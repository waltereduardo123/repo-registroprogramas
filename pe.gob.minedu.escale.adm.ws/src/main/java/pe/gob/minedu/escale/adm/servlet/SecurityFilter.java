package pe.gob.minedu.escale.adm.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.joda.time.DateTime;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;

import pe.gob.minedu.escale.adm.ejb.service.SesionServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.AccesoDTO;
import pe.gob.minedu.escale.common.rest.auth.util.AuthUtils;
import pe.gob.minedu.escale.common.util.LogUtil;

@Provider
public class SecurityFilter implements ContainerRequestFilter, ContainerResponseFilter {

	//////////////////////////////////////////////////////////////////////////////
	private static LogUtil log = new LogUtil(SecurityFilter.class.getName());
		
		@EJB
	    private transient SesionServiceLocal sesionServiceLocal = lookup(SesionServiceLocal.class);
	
	
	   @SuppressWarnings({ "unchecked" })
		private <T> T lookup(Class<T> clase) {
			try {
				String nombreSimple = clase.getSimpleName();
				Context c = new InitialContext();
				return (T) c.lookup(
						"java:global/pe.gob.minedu.escale.adm.ws-1.0.0-PRO/" + nombreSimple.replace("Local", "")
								+ "!pe.gob.minedu.escale.adm.ejb.service." + nombreSimple);
			} catch (NamingException ne) {
				if (log.isHabilitadoError()) {
					log.error(ne);
				}
				throw new RuntimeException(ne);
			}
		}  
	   
	/////////////////////////////////////////////////////////////////////////////
	
	/** Used for Script Nonce */
	private SecureRandom prng = null;
	
	@SuppressWarnings("unused")
	private static final String EXPIRE_ERROR_MSG = "El token a expirado", JWT_ERROR_MSG = "No se puede analizar el JWT",
			JWT_INVALID_MSG = "Invalido JWT token";

	
    
	
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext response) throws IOException {
	
		try {
 			this.prng = SecureRandom.getInstance("SHA1PRNG");
	
			// Add Script Nonce CSP Policy
			// --Generate a random number
		//String randomNum = new Integer(this.prng.nextInt()).toString();
 			String randomNumext=Integer.toHexString(new Integer(this.prng.nextInt()));
			//String randomNum =new Double(this.prng.nextGaussian()).toString();		
			
		
	
		String csp =  "frame-src 'self' https://www.google.com/recaptcha/ ;"+
				"style-src 'self' 'unsafe-inline' ;"+
				"font-src 'self'; "+
				"img-src 'self'; "+
				"media-src; "+
				"base-uri 'none'; "+
				"default-src 'nonce-"+ randomNumext +"' 'unsafe-inline' 'strict-dynamic' https: http:; "+
				"report-uri *://*.google.com https://www.bing.com/api/maps/mapcontrol?branch=release https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "+
				"connect-src 'self' ; "+
				"object-src 'self' 'unsafe-inline' ; "+
				"script-src 'self' https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "
				;
//	response.getHeaders().add( "X-FRAME-OPTIONS", "DENY" );
//	response.getHeaders().add( "X-Content-Type-Options", "nosniff" );
//	response.getHeaders().add( "Content-Security-Policy", "default-src 'self'; font-src; frame-src; img-src; media-src; object-src; report-uri; sandbox; script-src; style-src; upgrade-insecure-requests; connect-src 'none'; upgrade-insecure-requests" );
	response.getHeaders().add( "Content-Security-Policy", csp );
//	response.getHeaders().add( "X-XSS-Protection", "1; mode=block" );
	response.getHeaders().add( "Set-Cookie", "key=value; HttpOnly; SameSite=strict" );
	


		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR :  " + e );
			e.printStackTrace();
		}
	
	
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		@SuppressWarnings("unused")
		SecurityContext originalContext = requestContext.getSecurityContext();
		String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		String urlLogin = requestContext.getUriInfo().getPath().toString();
		boolean ctrlIntru=false;
		
		if (!urlLogin.equals("/usuario/validaLoginEscale") && !urlLogin.equals("/usuario/recuperarClaveUsuario")) {
			if (authHeader == null || authHeader.isEmpty() || authHeader.split(" ").length != 2) {
				requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build()); ////////////// aca se caeeeeeeee //////
			} else {
				JWTClaimsSet claimSet;
				try {////////////////////////////////////////////////////////////////////////
					claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
					
					
	
				////////////////////////////////////////////////////////////////////////////////	
					if(claimSet!=null){
					try {

						AccesoDTO adto=new AccesoDTO();
						adto.setUsuario(claimSet.toJSONObject().get("sub").toString());
						adto.setNavegador(String.valueOf(claimSet.toJSONObject().get("Browser")));
						adto.setHostcliente(String.valueOf(claimSet.toJSONObject().get("HostRemoto")));

						
						List<AccesoDTO> adtotwo=sesionServiceLocal.verificarNevegacionUserTupla(adto);
						
						List<AccesoDTO> lsadto=sesionServiceLocal.verificarNevegacionUserBrowseHost(adto);
						
						if(adtotwo.size()>0){
						
							if(lsadto.size()<=0  && !urlLogin.equals("/usuario/crearTkSesionMaster") && !urlLogin.equals("/usuario/registrarSesionNavegacion")){
									
				        			requestContext.abortWith(Response.status(Response.Status.METHOD_NOT_ALLOWED).build());
									ctrlIntru=true;
				        			
							}
						}
						/*
						
						
//						if(StringUtil.isNotNullOrBlank(adtotwo)){
						if(adtotwo.size()>0 && adtotwo!= null ){
//							if(adtotwo.size()==1 && adtotwo.get(0).getHostcliente()==null && adtotwo.get(0).getNavegador()==null && adtotwo.get(0).getIndAcc()==null ){
//								System.out.println("HAY UNA TUPLA, CON HOST NULL. ACTUALIZA LA TUPLA. LA PRIMERA VEZ");
//								adto.setNavegador(claimSet.toJSONObject().get("Browser").toString());
//								adto.setHostcliente(claimSet.toJSONObject().get("HostRemoto").toString());
//								adto.setIndAcc(IndicadorAccesoType.USER_HOST_BROWSE_IGUALES.getValue());
//								sesionServiceLocal.actualizaNevegacionUser(adto);
//							}
							
							if(adtotwo.size()==1 && adtotwo.get(0).getHostcliente()!=null && adtotwo.get(0).getNavegador()!=null && adtotwo.get(0).getIndAcc()!=null ){
								System.out.println("HAY UNA TUPLA. DEJALO PASAR");
//								adto.setNavegador(claimSet.toJSONObject().get("Browser").toString());
//								adto.setHostcliente(claimSet.toJSONObject().get("HostRemoto").toString());
//								adto.setIndAcc(IndicadorAccesoType.USER_HOST_BROWSE_IGUALES.getValue());
//								sesionServiceLocal.actualizaNevegacionUser(adto);
							}
							//|| adtotwo.get(0).getNavegador()!=claimSet.toJSONObject().get("Browser").toString()
							if(
									!adtotwo.get(0).getNavegador().equals(claimSet.toJSONObject().get("Browser").toString())
									 ){
								System.out.println("AVISAR QUE SE MATE LA SESION DEL INTRUSO");


								requestContext.abortWith(Response.status(Response.Status.METHOD_NOT_ALLOWED).build());
							
								ctrlIntru=true;
								
							}
													
						}
						else{	
							System.out.println("NO HAY TUPLA REGISTRADA :  "  );
						}
						
						*/
						
					}catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
						
					
			////////////////////////////////////////////////////////////////////////////////////////////		 
					if(!ctrlIntru){
							if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
								requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());////////////////////////////
							}
					}
				} catch (ParseException e) {
					requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
				} catch (JOSEException e) {
					requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
				}
			}
		}		
	}
}
