package pe.gob.minedu.escale.adm.ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.google.gson.Gson;

import pe.gob.minedu.escale.adm.business.exception.CorreoPersonalUsuarioExisteException;
import pe.gob.minedu.escale.adm.business.exception.UsuarioEntidadExisteException;
import pe.gob.minedu.escale.adm.business.exception.UsuarioEntidadPerfilExisteException;
import pe.gob.minedu.escale.adm.business.exception.UsuarioNoExisteException;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.business.type.TipoAccionAdministrarUsuario;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.ClaveServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.NotificacionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.ObjectServiceAdmLocal;
import pe.gob.minedu.escale.adm.ejb.service.SecuryTokenServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.SesionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.AccesoDTO;
import pe.gob.minedu.escale.adm.model.dto.ClaveDTO;
import pe.gob.minedu.escale.adm.model.dto.ModuloDTO;
import pe.gob.minedu.escale.adm.model.dto.OrganismoDTO;
import pe.gob.minedu.escale.adm.model.dto.OrganismoPerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PrivilegioDTO;
import pe.gob.minedu.escale.adm.model.dto.PrivilegioRolDTO;
import pe.gob.minedu.escale.adm.model.dto.RolDTO;
import pe.gob.minedu.escale.adm.model.dto.TokenSecuryDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoPKDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioRolDTO;
import pe.gob.minedu.escale.adm.model.ws.dto.AutenticacionDTO;
import pe.gob.minedu.escale.adm.model.ws.dto.NewUsuarioCompareDTO;
import pe.gob.minedu.escale.adm.model.ws.dto.UsuarioOrganismoWsDTO;
import pe.gob.minedu.escale.adm.vo.BusquedaUsuarioVO;
import pe.gob.minedu.escale.adm.vo.RecuperarClaveVO;
import pe.gob.minedu.escale.adm.vo.UsuarioCriteriaVO;
import pe.gob.minedu.escale.adm.vo.UsuarioLoginResultadoVO;
import pe.gob.minedu.escale.adm.vo.UsuarioSessionVO;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.business.state.UsuarioState;
import pe.gob.minedu.escale.common.constans.IndicadorConstant;
import pe.gob.minedu.escale.common.dto.rest.ConsultaJson;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.notify.NotificacionUtil;
import pe.gob.minedu.escale.common.rest.auth.util.AuthUtils;
import pe.gob.minedu.escale.common.rest.auth.util.Token;
import pe.gob.minedu.escale.common.rest.util.AesUtil;
import pe.gob.minedu.escale.common.rest.util.QueryParamURL;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.CollectionUtil;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.LogUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.CadenaInvalidaException;




/**
 * REST Web Service
 *
 * @author IMENDOZA
 */
@Path("/usuario")
@Stateless
public class UsuarioWs extends BaseBean{

/**
	 * 
	 */
	private static final long serialVersionUID = -9222475680284802826L;

//    @Context
//    private UriInfo context;
    
    /** El contexto. */
    private ServiceContext context = new ServiceContext();

    /** El servicio de administracion service local. */
    @EJB
    private transient AdministracionServiceLocal administracionServiceLocal = lookup(AdministracionServiceLocal.class);
    /** El servicio de usuario service local. */
    @EJB
    private transient UsuarioServiceLocal usuarioServiceLocal = lookup(UsuarioServiceLocal.class);
    
    @EJB
    private transient SesionServiceLocal sesionServiceLocal = lookup(SesionServiceLocal.class);
    
    @EJB
    private transient SecuryTokenServiceLocal securyTokenServiceLocal = lookup(SecuryTokenServiceLocal.class);
    
    
    @EJB
    private transient ClaveServiceLocal claveServiceLocal = lookup(ClaveServiceLocal.class);
    
    /** El objeto usuario detalle. */ 
    private UsuarioOrganismoDTO usuarioDetalle;
    
    /** El servicio de notificacion service local. */
    @EJB
    private  NotificacionServiceLocal notificacionServiceLocal = lookup(NotificacionServiceLocal.class);
    
	@EJB
	private ObjectServiceAdmLocal objectService = lookup(ObjectServiceAdmLocal.class);
    
    
    private BCryptPasswordEncoder passwordEncoder;
    
    private static String CREDENCIALES_INCORRECTAS = "31: Las credenciales son incorrectas"; 
    //private static String CREDENCIALES_INCORRECTAS_PASS_INVALIDO = "21:Credenciales incorrectas: contraseña no válida";
    private static String CREDENCIALES_INCORRECTAS_PASS_INVALIDO = "21:Usuario y/o contraseña incorrecta";
    // private static String CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE = "20:Credenciales incorrectas: No existe Usuario";
    private static String CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE = "20:Usuario y/o contraseña incorrecta";//werr 29/12/2017
    private static String CREDENCIALES_INCORRECTAS_EMAIL_INEXISTENTE = "23:Credenciales incorrectas: No existe email";
   // private static String CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE = "24:Credenciales incorrectas: No coincide el usuario y el email ";
    private static String CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE = "24:Usuario y/o correo incorrecto"; //werr 29/12/2017
    
    private static String CREDENCIALES_CORRECTAS_NO_ADMINISTRADOR = "32: Las credenciales son correctas pero el usuario no es principal";
    private static String CREDENCIALES_CORRECTAS_INHABILITADO_INTENTOS_FALLIDOS = "13:Credenciales correctas: Usuario inhabilitado por intentos fallidos";
   // private static String CREDENCIALES_CORRECTAS_INHABILITADO = "12:Credenciales correctas: Usuario inhabilitado";
    private static String CREDENCIALES_CORRECTAS_INHABILITADO = "Credenciales correctas: Usuario inhabilitado"; //werr 22/06/2018
    @SuppressWarnings("unused")
	private static String USUARIO_INEXISTENTE = "33: Todo lo anterior está bien pero, no encontró al usuario";
    private static String USUARIO_EXISTE_NO_ENTIDAD = "34: Todo lo anterior está bien pero el usuario solicitado no corresponde al ámbito permitido";
    private static String USUARIO_INHABILITADO = "14: Usuario inhabilitado";
    private static String USUARIO_EXISTE_CODOID = "30: Ya existe un usuario con el mismo número de documento";
    private static String USUARIO_INEXISTENTE_CODOID = "45: El número de documento no existe";
    private static String USUARIO_CODOID_INEXISTENTE = "34: El nombre de usuario o número de documento es nulo ";
    private static String ERROR_INTERNO = "22:Error Interno";
    private static String ACTUALIZACION_MIGRACION_EXITOSA = "40: Actualización por migración exitosa";
    private static String ACTUALIZACION_CONTRASENA_EXITOSA = "41: Actualización de contraseña exitosa";
    private static String REGISTRO_USUARIOORGANISMO_EXITOSA = "42: Se registro con éxito el usuario";
    private static String REGISTRO_USUARIOORGANISMO_SIN_EXITO = "43: Se registro sin éxito el usuario";
    private static String ACTUALIZAR_USUARIO_EXITOSA = "44: Se actualizo con éxito el usuario";
    @SuppressWarnings("unused")
	private static String BUSQUEDA_USUARIO_EXITOSA = "44: La búsqueda del usuario fue exitosa";
    private static String SESSION_USUARIO_EXISTENTE = "Para el usuario ingresado existe una sesión activa. Debe de cerrar la sesión anterior. Verifique";//werr 12/10/2018 45: 
    
    
    private Gson gson = new Gson();

	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;

	private Map<Object, Object> parametros;
    
	private String param,salt,iv;
	
	private int keySize = 128, iterationCount = 1000;
	
	private String passphrase= "0015975320171234";  
	
//	private String passphraseToken= "958463201000";
//	
//	private String passphraseTokenRequest= "958461101000";
	
	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);	;
	
	private static LogUtil log = new LogUtil(UsuarioWs.class.getName());
	
	
//	 /** El servicio usuario dao. */
//    @EJB
//    private UsuarioDAOLocal usuarioDAO;
	
    public UsuarioWs() {
    }

//    public void despliegue(){  correo personal
//    	String comando = "";
//    	ProcessBuilder p = new ProcessBuilder(comando);
//    }
 
   @SuppressWarnings("rawtypes")
   @POST
   @Path("/validaLoginEscale")
   @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
   public Response validaLoginEscale(String jsonEncrypted) {
       UsuarioDTO usuario = new UsuarioDTO();
       BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
       parametros = new HashMap<>();
       AutenticacionDTO aut = new AutenticacionDTO();
       String respEncryp;
       try {
    	   aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);			
           usuario = administracionServiceLocal.buscarUsuarioxOID(aut.getIdCodoId());
           if(usuario != null){
               if ("0".equals(aut.getIdEstado())) {
                   usuario.setEstado(UsuarioState.BLOQUEADO_INTENTOS_FALLIDOS.getKey());
                   usuarioServiceLocal.actualizarUsuario(usuario);                   
                   respuesta = new RespuestaDTO(false, "['"+CREDENCIALES_CORRECTAS_INHABILITADO_INTENTOS_FALLIDOS+"']", null);
                   respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
      			   return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
      				
               }else
               if(passwordEncoder.matches(aut.getPass(), usuario.getContrasena())){
                   if (usuario.getEstado().equals(UsuarioState.BLOQUEADO_INTENTOS_FALLIDOS.getKey())) {
                	   respuesta = new RespuestaDTO(false, "['"+CREDENCIALES_CORRECTAS_INHABILITADO_INTENTOS_FALLIDOS+"']", null);
                       respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
          			   return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();               
                       
                   }
                   if(usuario.getEstado().equals(UsuarioState.ACTIVO.getKey())){
                	/////////////////******************************//////////////////////*****************************////////////////////////////////
           
            		 //imendoza 20170126 inicio
                	   List<UsuarioLoginResultadoVO> listaUsuarioLogin = usuarioServiceLocal.buscarUsuarioLoginxDni(usuario.getCodigoOID());
                	   @SuppressWarnings("unused")
                	   List<OrganismoPerfilDTO> listaOrgnPrf = listaUsuarioLogin.stream()
																				.filter(p->(Objects.nonNull(p.getOrganismoPerfil()) && p.getOrganismoPerfil().getId()!=0L))
																				.map(UsuarioLoginResultadoVO::getOrganismoPerfil)
																				.collect(Collectors.toList()).stream()
													   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
													   			   											 .collect(Collectors.toList()); 
                	   List<OrganismoDTO> listaOrgn = listaUsuarioLogin.stream()
                			   											.filter(p-> (Objects.nonNull(p.getOrganismo()) && p.getOrganismo().getId()!=0L))
                			   											.map(UsuarioLoginResultadoVO::getOrganismo)
                			   											.collect(Collectors.toList()).stream()
							                			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
							                			   											 .collect(Collectors.toList());                	   
                	   //List<OrganismoDTO> distinctOrganismo = listaOrgn.stream().filter(CollectionUtil.distinctByKey(p -> p.getId())).collect(Collectors.toList());
                	   List<PerfilDTO> listaPrf = listaUsuarioLogin.stream()
																	.filter(p-> (Objects.nonNull(p.getPerfil()) && p.getPerfil().getId()!=0L))
																	.map(UsuarioLoginResultadoVO::getPerfil)
																	.collect(Collectors.toList()).stream()
										   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
										   			   											 .collect(Collectors.toList());
                	   List<RolDTO> listaRol = listaUsuarioLogin.stream()
																.filter(p-> (Objects.nonNull(p.getRol()) && p.getRol().getId()!=0L))
																.map(UsuarioLoginResultadoVO::getRol)
																.collect(Collectors.toList()).stream()
									   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
									   			   											 .collect(Collectors.toList());
                	   List<ModuloDTO> listaMod = listaUsuarioLogin.stream()
																	.filter(p-> (Objects.nonNull(p.getModulo()) && p.getModulo().getId()!=0L))
																	.map(UsuarioLoginResultadoVO::getModulo)
																	.collect(Collectors.toList()).stream()
										   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
										   			   											 .collect(Collectors.toList());                	   
                	   List<PrivilegioDTO> listaPrv = listaUsuarioLogin.stream()
																		.filter(p-> (Objects.nonNull(p.getPrivilegio()) && p.getPrivilegio().getId()!=0L))
																		.map(UsuarioLoginResultadoVO::getPrivilegio)
																		.collect(Collectors.toList()).stream()
											   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
											   			   											 .collect(Collectors.toList());
                	   List<PrivilegioRolDTO> listaPrvRol = listaUsuarioLogin.stream()
																				.filter(p-> (Objects.nonNull(p.getPrivilegioRol()) && p.getPrivilegioRol().getId()!=0L))
																				.map(UsuarioLoginResultadoVO::getPrivilegioRol)
																				.collect(Collectors.toList()).stream()
													   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getId()))
													   			   											 .collect(Collectors.toList());
                	   List<UsuarioRolDTO> listaUsuRol = listaUsuarioLogin.stream()
																				.filter(p-> Objects.nonNull(p.getUsuarioRol()))
																				.map(UsuarioLoginResultadoVO::getUsuarioRol)
																				.collect(Collectors.toList()).stream()
													   			   											 .filter(CollectionUtil.distinctByKey(p -> p.getUsuarioRolPK()))
													   			   											 .collect(Collectors.toList());
                	 //imendoza 20170126 fin
                               
                       usuario.setFechaCreacion(null);
                       Date ultimaFechaAcceso = new Date();
                       ultimaFechaAcceso = usuarioServiceLocal.ultimaFechadeActividad(usuario.getId());
                       usuario.setUltimaFechadeActividad(FechaUtil.obtenerFechaFormatoPersonalizado(ultimaFechaAcceso, "dd/MM/yyyy hh:mm:ss"));
                       UsuarioSessionVO usuarioSession = new UsuarioSessionVO();
                       usuarioSession.setCorreoPersonal(usuario.getEmail());
                       usuarioSession.setCargoInstitucional("Estadistico");                            
                       usuarioSession.setCorreoLogin(usuario.getEmail());
                       usuarioSession.setHostRemoto(aut.getIpHostRemoto());
                       usuarioSession.setIdEntidad(listaOrgn.get(0).getId());
                       usuarioSession.setIdPerfil1(listaPrf.get(0).getId());                            
                       usuarioSession.setIdUsuario(usuario.getId());
                       usuarioSession.setIndClaveTemporal(0);
                       usuarioSession.setNombreEntidad(listaOrgn.get(0).getNombreOrganismo());
                       usuarioSession.setNombreUsuario(usuario.getNombreCompleto());
                       usuarioSession.setOidUsuario(usuario.getCodigoOID());
                       usuarioSession.setUsuarioState(UsuarioState.ACTIVO);
                       administracionServiceLocal.guardarLogUsuIngreso(usuarioSession, usuarioSession.getHostRemoto());
	                   	usuario.setContrasena(null);
	                   	UsuarioDTO u = new UsuarioDTO();
	       				u.setId(usuario.getId());
	       				u.setNombres(usuario.getNombres());
	       				u.setApellidoPaterno(usuario.getApellidoPaterno());
	       				u.setApellidoMaterno(usuario.getApellidoMaterno());
	       				u.setNombreCompleto(usuario.getNombreCompleto());
	       				u.setTipoDocumento(usuario.getTipoDocumento());
	       				u.setDescDocumento(usuario.getDescDocumento());
	       				u.setEmail(usuario.getEmail());
	       				u.setEstado(usuario.getEstado());
	       				u.setCodigoOID(aut.getIdCodoId());//imendoza 20170302
	       				//imendoza 20170302 u.setCodigoOID(usuario.getCodigoOID());
	       				u.setUltimaFechadeActividad(usuario.getUltimaFechadeActividad());
	       				//imendoza 20170302 inicio
	       				u.setContrasena(aut.getPass());
	       				//imendoza 20170302 fin
	       				//imendoza 20170303 inicio
	       				Map<String,Object> mensajes = new HashMap<>();
	       				mensajes.put("msgItf", "Intento de acceso fallido");	       				
	       				//imendoza 20170303 fin
                        //Inicio Respuesta
                       	parametros.clear();
                       	

                       	
                       	///////////////////////////////////////////////IP AND BROWSE POR ACCESOS////////////////////////////////////////////////////////
                       	
                     	parametros.put("HostRemoto", aut.getIpHostRemoto());
                     	parametros.put("Browser", aut.getBrowse());
                       	
                       	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                       	
	       				parametros.put("Organismos", listaOrgn);
	       				parametros.put("UsuarioRol", listaUsuRol);
	       				parametros.put("Perfiles", listaPrf);
	       				parametros.put("Roles", listaRol);
	       				parametros.put("Privilegios", listaPrv);
	       				parametros.put("Modulos", listaMod);	       				
	       				parametros.put("Usuario", u);
	       				parametros.put("PrivilegioRol", listaPrvRol);
	       				parametros.put("CodigoOID", usuario.getCodigoOID());
	       				//imendoza 20170303 inicio
	       				parametros.put("Mensajes", mensajes);
	       				//imendoza 20170303 fin
	       				
	       				
	       				//final Token token = AuthUtils.createToken(request.getRemoteHost(), parametros);
	       				final Token token = AuthUtils.createToken(aut.getIpHostRemoto(), parametros);
	       				
	       				respuesta = new RespuestaDTO(true, "[]", token);
	                    
	       				respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
	       			 	
	       				return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
               				
               				
                        //Fin Respuesta

	       				
                   }else{
                   	respuesta = new RespuestaDTO(false, "['"+CREDENCIALES_CORRECTAS_INHABILITADO+"']", null);
                   	respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
                    return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Credenciales correctas: Usuario inhabilitado
                   }
               }else{
            	   parametros.put("noshow", false  );
            	   respuesta = new RespuestaDTO(false, "['"+CREDENCIALES_INCORRECTAS_PASS_INVALIDO+"']", parametros);               	
	               	return Response.status(Status.UNAUTHORIZED).entity(gson.toJson(respuesta).toString()).build();//Credenciales incorrectas: contrasena no valida                 	        
                //imendoza 20170213 return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Credenciales incorrectas: contrasena no valida
               }
           }else{
	          	//respuesta = new RespuestaDTO(false, "['"+CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE+"']", null);           	
        	   
        	   parametros.put("noshow", true  );  
        	   respuesta = new RespuestaDTO(false, "['"+CREDENCIALES_INCORRECTAS_PASS_INVALIDO+"']", parametros);               		
        	   return Response.status(Status.UNAUTHORIZED).entity(gson.toJson(respuesta).toString()).build();//Credenciales incorrectas: No existe Usuario           
          //imendoza 20170213 return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Credenciales incorrectas: No existe Usuario 
           }
       } catch (Exception ex) {
       	if (log.isHabilitadoError()) {
       		log.error(ex);
       	}
           parametros.put("ExceptionTipo", ex.toString());
           parametros.put("Exception", ex.getMessage());
           parametros.put("idCodoId", aut.getIdCodoId());
           parametros.put("ipHostRemoto", aut.getIpHostRemoto());
           parametros.put("idEstado", aut.getIdEstado());
           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
       }      
   }
    
   
   @SuppressWarnings("rawtypes")
   @POST
   @Path("/validaNavegacionSesion")
   @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
   public Response validaNavegacionSesion(String jsonEncrypted) {
//       UsuarioDTO usuario = new UsuarioDTO();
//       BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
       parametros = new HashMap<>();
       AutenticacionDTO aut = new AutenticacionDTO();
       String respEncryp="";
       try {
    	   aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);
        	AccesoDTO datosesion = new AccesoDTO();
        	datosesion.setUsuario(aut.getIdCodoId());
        

           	List<AccesoDTO> verifsesion=sesionServiceLocal.verificarNevegacionUserTupla(datosesion);
           	if(verifsesion.size()==0){          		
                			parametros.put("notrow", true);
                			respuesta = new RespuestaDTO(true, "[' ']", parametros);
	                        respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
	                        return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
                            	
            		}else if(verifsesion.size()==1){
            			parametros.put("notrow", false);   
            			respuesta = new RespuestaDTO(true, "[' ']", parametros);
                        respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
                        return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
            			}else{
                			respuesta = new RespuestaDTO(true, "['cualquiera']", parametros);
	                        respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
	                        return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();		                		
	                   } 
            		
         }catch (Exception ex) {
              	if (log.isHabilitadoError()) {
              		log.error(ex);
              	}
                  parametros.put("ExceptionTipo", ex.toString());
                  parametros.put("Exception", ex.getMessage());
                  respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
                  respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
                  return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
              }   
          }
    
    @PUT
    @Path("/actualizarDatosByMigracion")
    @Consumes({"application/json"})
    public Response actualizarDatosByMigracion(UsuarioDTO usuario) {        
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        try {
            UsuarioDTO usuarioExistente;
            if(usuario.getNombres()!= null && !usuario.getCodigoOID().isEmpty() && usuario.getContrasena().trim().length() >=8){                  
                
                usuarioExistente = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID());
                
                if (usuarioExistente == null) {
                    usuario.setUltimoUsuarioModificacion(usuario.getNombreCompleto());
                    usuario.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                    usuario.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());

                    usuario.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
                    usuarioServiceLocal.actualizarUsuario(usuario);
                    return Response.ok(ACTUALIZACION_MIGRACION_EXITOSA).build();
                }else if(usuarioExistente.getId().longValue()!= usuario.getId().longValue()){
                    return Response.ok(USUARIO_EXISTE_CODOID).build();
                }else if(usuarioExistente.getId().longValue() == usuario.getId().longValue()){
                    usuario.setUltimoUsuarioModificacion(usuario.getNombreCompleto());
                    usuario.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                    usuario.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());

                    usuario.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
                    usuarioServiceLocal.actualizarUsuario(usuario);
                   return Response.ok(ACTUALIZACION_MIGRACION_EXITOSA).build();
                }                                        
            }                        
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
        		log.error(ex);
        	}          
            return Response.ok(ERROR_INTERNO).build();
           
        }
        return Response.ok(ERROR_INTERNO).build();
        
    }

    

    @POST
    @Path("/buscadorUsuario")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response buscadorUsuario(UsuarioCriteriaVO criteria) {        
        List<BusquedaUsuarioVO> listaUsuario;
        try {
            listaUsuario = usuarioServiceLocal.buscarUsuario(context,criteria);
            
            JSONArray jsArray = new JSONArray();
            JSONObject job;
            for (BusquedaUsuarioVO busquedaUsuario : listaUsuario) {
                job = new JSONObject(busquedaUsuario);
                jsArray.put(job);
            }           
            return Response.ok(jsArray.toString()).build();
            
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
        		log.error(ex);
        	}
            return Response.ok(ERROR_INTERNO).build();
        }
        
    }
    
    @SuppressWarnings("rawtypes")
	@POST
    @Path("/recuperarClaveUsuario")
    public Response recuperarClaveUsuario(@QueryParam("idCodoId") String idCodoId, @QueryParam("email") String email) {
        
        UsuarioDTO usuario = new UsuarioDTO();        
        List<UsuarioDTO> dto = null;    
        try {            
            if (!StringUtil.isNotNullOrBlank(idCodoId)) {
                respuesta = new RespuestaDTO(true, "['"+CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE+"']", null);
                return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
            }
            if (!StringUtil.isNotNullOrBlank(email)) {
                respuesta = new RespuestaDTO(true, "['"+CREDENCIALES_INCORRECTAS_EMAIL_INEXISTENTE+"']", null);
                return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
            }
            usuario = usuarioServiceLocal.buscarUsuarioxOID(idCodoId);
            if (usuario == null) {
               // respuesta = new RespuestaDTO(true, "['"+CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE+"']", null);
            	  respuesta = new RespuestaDTO(true, "['"+CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE+"']", null);
            	return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
            }
            dto = usuarioServiceLocal.buscarUsuarioCorreo(idCodoId, email);
            if (dto == null) {
            	String emailCorrecto = usuario.getEmail();
            	emailCorrecto = emailCorrecto.replace(emailCorrecto.substring(emailCorrecto.indexOf("@")-4, emailCorrecto.indexOf("@")), "****");
            //    respuesta = new RespuestaDTO(true, "['"+CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE+", el email vinculado es: "+ emailCorrecto +"']", null);
                respuesta = new RespuestaDTO(true, "['"+CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE+"']", null); //werr 29/12/2017
            	return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
            }
          //imendoza 20170102  luego activar
            /*else{
                List<Object[]> data = usuarioServiceLocal.buscarUsuarioRecuperarClave(idCodoId);
                if (String.valueOf(data).equalsIgnoreCase("[]")) {
                    respuesta = new RespuestaDTO(true, "['"+USUARIO_INHABILITADO+"']", null);
                    return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
                }
            }
            */
          //imendoza 20170102 
            RecuperarClaveVO recuperarClaveVO = new RecuperarClaveVO();
            recuperarClaveVO.setCorreoElectronico(email);
            recuperarClaveVO.setFlagCapcha(false);
            recuperarClaveVO.setUsuario(idCodoId);
            administracionServiceLocal.recuperarClave(recuperarClaveVO);
            
            respuesta = new RespuestaDTO(true, "['"+ACTUALIZACION_CONTRASENA_EXITOSA+"']", null);
            return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
                                 
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
        		log.error(ex);
        	}
           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", null);
           return  Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build(); //Error Interno
        }        
    }
    
    @PUT
    @Path("/actualizarDatos")
    @Consumes({"application/json"})
    public Response actualizarDatos(UsuarioDTO usuario) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        try {
			UsuarioDTO usuarioExistente;
			usuarioExistente = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID()); 
            if(usuarioExistente.getNombres()!= null && !usuarioExistente.getCodigoOID().isEmpty()){                                  
                usuario.setUltimoUsuarioModificacion(usuario.getNombreCompleto());
                usuario.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                usuario.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());                
                if (StringUtil.isNotNullOrBlank(usuario.getContrasena())) {                    
                    usuario.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
                }else{
                    usuario.setContrasena(null);
                }                
                usuarioServiceLocal.actualizarUsuario(usuario);
            }else{
                return Response.ok(USUARIO_CODOID_INEXISTENTE).build();
            }
                                                                
            return Response.ok(usuario).build();

        } catch (Exception e) {
        	if (log.isHabilitadoError()) {
        		log.error(e);
        	}
            return Response.ok(ERROR_INTERNO).build();
        }
    }
    
    
    @SuppressWarnings("rawtypes")
	@PUT
    @Path("/actCeUsNew")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response actCeUsNew(final String jsonEncrypted) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        UsuarioDTO usuarioExistente;
        UsuarioDTO usuarioActualizar;
        parametros = new HashMap<>();
        boolean indExisteEmail=false;
        List<UsuarioDTO> usuTem=null;
        List<ClaveDTO> claveHistorica;  /////////////////////////********************************************************
        ClaveDTO clavedto=new ClaveDTO(); ////////////////////////////////************************************************
        Boolean indClaveHistorica=false; /////////////////////////////////************************************************
        Boolean indRegistro=false; /////////////////////////////////************************************************
        String respEncrypt=""; /////////////////////////////////************************************************
        try {        	
        	UsuarioDTO usuario = gson.fromJson(desencriptar(jsonEncrypted), UsuarioDTO.class);
			
        
        	
        	
        	////////////////////////////////////////////////////////////////////////////
        	
		        	usuarioExistente = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID());
		       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	
		        	
		        	int ctaKey=0;
		        	clavedto.setCodigo(usuario.getCodigoOID()); //////////////////////PRIMERA VERIF////////////////////////*********************************************
		        	claveHistorica= claveServiceLocal.verificarClaveUser(clavedto);  ///////////////////////////////////***************************************************
					///////////////////////////////////////////////REGISTRO DE HISTORICO DE CLAVES////////////////////////////////////////////////////////
		        	if(claveHistorica.size()==0){
		        		ClaveDTO clavedtoreg=new ClaveDTO();
						clavedtoreg.setCodigo(usuario.getCodigoOID());
						//usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(sesionExistente.getUsuario());  
						clavedtoreg.setClave(usuarioExistente.getContrasena());
						clavedtoreg.setTiempoVigencia(2);
						clavedtoreg.setUsuCrea(usuario.getCodigoOID());
						clavedtoreg.setFecCrea(FechaUtil.obtenerFechaActual());
			         	claveServiceLocal.registrarClaveHistorica(clavedtoreg);
		        	}
		        	ClaveDTO clavedtoreg=new ClaveDTO();
		        	ClaveDTO clavedtoregTem=new ClaveDTO();
		        	for(int i=0; i<claveHistorica.size();i++ ){
		        		//ClaveDTO clavedtoregTem=new ClaveDTO();	
		        		clavedtoregTem.setClave(claveHistorica.get(i).getClave());
		        		
		        		//if(!usuarioExistente.getContrasena().trim().equals(clavedtoregTem.getClave().trim())){
		        		 if(!passwordEncoder.matches(usuario.getContrasena(), clavedtoregTem.getClave())){	
					        		//ClaveDTO clavedtoreg=new ClaveDTO();
									clavedtoreg.setCodigo(usuario.getCodigoOID());
									//usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(sesionExistente.getUsuario());  
									clavedtoreg.setClave(usuarioExistente.getContrasena());
									clavedtoreg.setTiempoVigencia(2);
									clavedtoreg.setUsuCrea(usuario.getCodigoOID());
									clavedtoreg.setFecCrea(FechaUtil.obtenerFechaActual());
									indRegistro=true;
								//	claveServiceLocal.registrarClaveHistorica(clavedtoreg);
				        		
		        			}else{
		        				break;
		        			}
		        	}
		         	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		        	
	        		  if(passwordEncoder.matches(usuario.getContrasena(), clavedtoregTem.getClave())){
	        			  indClaveHistorica=true;
	        			 // break;
	        		  }else{			        			  
	        			  if(indRegistro)claveServiceLocal.registrarClaveHistorica(clavedtoreg);
	        			  indClaveHistorica=false;
	        		  }
		        	

		    		        	
		        	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					usuarioActualizar = new UsuarioDTO();
		            if(Objects.nonNull(usuarioExistente) && StringUtil.isNotNullOrBlank(usuarioExistente.getCodigoOID())){
					  
		            	
		            	if(passwordEncoder.matches(usuario.getClave(), usuarioExistente.getContrasena())){
		            		
		            		parametros.put("keyAnt", true);	
		            		respuesta = new RespuestaDTO(false, "['La nueva contraseña no debe de ser igual a la anterior']", parametros);
					    	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
		            	}
		            	else
		            	{
		            	
		            	
		            	
			            	if(!indClaveHistorica){
				            	
			            		parametros.put("isOk", false);	
			            		respuesta = new RespuestaDTO(true, "['']", parametros); 
				            	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			            		
			            		/*if(passwordEncoder.matches(usuario.getClave(), usuarioExistente.getContrasena())){
							    	usuarioActualizar.setCodigoOID(usuario.getCodigoOID());	
							    	
							    	
							    	
							    }
				            	else{
							    	respuesta = new RespuestaDTO(false, "['La contraseña actual no es válida']", parametros);
							    	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
							    }*/              
							  }else{
								  parametros.put("keyOld", true);	
								  respuesta = new RespuestaDTO(false, "['No puede usar una contraseña antigua']", parametros);
								  respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
							  } 
		            	
		            }
		            	
		            	
		            	
		            	
		            }else{
		            	parametros.put("keyInex", true);	
		            	respuesta = new RespuestaDTO(false, "['"+USUARIO_CODOID_INEXISTENTE+"']", parametros);  
		            	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
		            }
            
        														
            
            
        } catch (Exception e) {
        	parametros.clear();
        	if (log.isHabilitadoError()) {
				log.error(e);
			}
        	parametros.put("keyConf", true);	
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			//return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
        }
        //return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
        
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
        
    }
    
    
    
    
    
    
    
    @SuppressWarnings("rawtypes")
	@PUT
    @Path("/actualizarCe")
   // @Produces(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response actualizarCe(final String jsonEncrypted) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        UsuarioDTO usuarioExistente;
        UsuarioDTO usuarioActualizar;
        parametros = new HashMap<>();
        boolean indExisteEmail=false;
        List<UsuarioDTO> usuTem=null;
        List<ClaveDTO> claveHistorica;  /////////////////////////********************************************************
        ClaveDTO clavedto=new ClaveDTO(); ////////////////////////////////************************************************
        Boolean indClaveHistorica=false; /////////////////////////////////************************************************
        Boolean indRegistro=false; /////////////////////////////////************************************************
        String respEncrypt=""; /////////////////////////////////************************************************
        try {        	
        	UsuarioDTO usuario = gson.fromJson(desencriptar(jsonEncrypted), UsuarioDTO.class);
			
        	/////////////////////////////////////////////////////////////////////////////
        	String userId=usuario.getUserID();
        	
        	String tipCodoiUser=usuarioServiceLocal.obtenerTipoNumeroDocumento(Long.parseLong(userId));
        	String[] part=tipCodoiUser.split(",");
        	String codOIDok=part[1];
        	////////////////////////////////////////////////////////////////////////////
        	
        	if(codOIDok.trim().toUpperCase().equals(usuario.getCodigoOID().trim().toUpperCase())){
		        	usuarioExistente = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID());
		       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	
		        	
		        	int ctaKey=0;
		        	clavedto.setCodigo(usuario.getCodigoOID()); //////////////////////PRIMERA VERIF////////////////////////*********************************************
		        	claveHistorica= claveServiceLocal.verificarClaveUser(clavedto);  ///////////////////////////////////***************************************************
					///////////////////////////////////////////////REGISTRO DE HISTORICO DE CLAVES////////////////////////////////////////////////////////
		        	if(claveHistorica.size()==0){
		        		ClaveDTO clavedtoreg=new ClaveDTO();
						clavedtoreg.setCodigo(usuario.getCodigoOID());
						//usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(sesionExistente.getUsuario());  
						clavedtoreg.setClave(usuarioExistente.getContrasena());
						clavedtoreg.setTiempoVigencia(2);
						clavedtoreg.setUsuCrea(usuario.getCodigoOID());
						clavedtoreg.setFecCrea(FechaUtil.obtenerFechaActual());
			         	claveServiceLocal.registrarClaveHistorica(clavedtoreg);
		        	}
		        	ClaveDTO clavedtoreg=new ClaveDTO();
		        	ClaveDTO clavedtoregTem=new ClaveDTO();
		        	for(int i=0; i<claveHistorica.size();i++ ){
		        		//ClaveDTO clavedtoregTem=new ClaveDTO();	
		        		clavedtoregTem.setClave(claveHistorica.get(i).getClave());
		        		
		        		//if(!usuarioExistente.getContrasena().trim().equals(clavedtoregTem.getClave().trim())){
		        		 if(!passwordEncoder.matches(usuario.getContrasena(), clavedtoregTem.getClave())){	
					        		//ClaveDTO clavedtoreg=new ClaveDTO();
									clavedtoreg.setCodigo(usuario.getCodigoOID());
									//usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(sesionExistente.getUsuario());  
									clavedtoreg.setClave(usuarioExistente.getContrasena());
									clavedtoreg.setTiempoVigencia(2);
									clavedtoreg.setUsuCrea(usuario.getCodigoOID());
									clavedtoreg.setFecCrea(FechaUtil.obtenerFechaActual());
									indRegistro=true;
								//	claveServiceLocal.registrarClaveHistorica(clavedtoreg);
				        		
		        			}else{
		        				break;
		        			}
		        	}
		         	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		        	
	        		  if(passwordEncoder.matches(usuario.getContrasena(), clavedtoregTem.getClave())){
	        			  indClaveHistorica=true;
	        			 // break;
	        		  }else{			        			  
	        			  if(indRegistro)claveServiceLocal.registrarClaveHistorica(clavedtoreg);
	        			  indClaveHistorica=false;
	        		  }
		        	
//		        	clavedto.setCodigo(usuario.getCodigoOID()); //////////////////////////////////////////////*********************************************
//		        	claveHistorica= claveServiceLocal.verificarClaveUser(clavedto);  ///////////////////////////////////***************************************************
//		        		//verificar en lista existente
//		        		for(int j=0; j<claveHistorica.size();j++ ){
//			        		  ClaveDTO clavedtotem=new ClaveDTO();
//			        		  clavedtotem.setClave(claveHistorica.get(j).getClave());
//			        		  //indClaveHistorica
//			        		  if(passwordEncoder.matches(usuario.getContrasena(), clavedtotem.getClave())){
//			        			  indClaveHistorica=true;
//			        			  break;
//			        		  }else{			        			  
//			        			  if(indRegistro)claveServiceLocal.registrarClaveHistorica(clavedtoreg);
//			        			  indClaveHistorica=false;
//			        		  }
//			        	
//			        	
//		        		}
		        
		        	
		       	
		        	
		        	
		        	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					usuarioActualizar = new UsuarioDTO();
		            if(Objects.nonNull(usuarioExistente) && StringUtil.isNotNullOrBlank(usuarioExistente.getCodigoOID())){
					  if(!indClaveHistorica){
		            	
		            	if(passwordEncoder.matches(usuario.getClave(), usuarioExistente.getContrasena())){
					    	usuarioActualizar.setCodigoOID(usuario.getCodigoOID());
					    	
					    	//verificación del email ingresado
					    	if(usuario.getEmail().equals(usuarioExistente.getEmail())){
					    		usuarioActualizar.setEmail(usuario.getEmail());
					    		usuarioActualizar.setUltimoUsuarioModificacion(usuario.getCodigoOID());
				            	usuarioActualizar.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
				            	usuarioActualizar.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());                
				                if (StringUtil.isNotNullOrBlank(usuario.getContrasena())) {                    
				                	usuarioActualizar.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
				                }else{
				                	usuarioActualizar.setContrasena(null);
				                }
				                usuarioServiceLocal.actualizarUsuario(usuarioActualizar);
						    	respuesta = new RespuestaDTO(true, "['']", parametros);
						    	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
					    	}else{		
					    		////////////////////////////////////////////////////////////////////////////////
					    		//---buscar por correo-----------
					    		
					    		 
					    	        //get de correo registrados
					    	        
					    	        usuTem=usuarioServiceLocal.buscarCorreoPersonal(usuario.getEmail());
//					    	        if((usuTem != null) && usuTem.size()>0){
					    	        if(usuTem != null){
					    	        	indExisteEmail=true;
					    	        }
					    	        else{
					    	        		indExisteEmail=false;
					    	        		usuarioActualizar.setEmail(usuario.getEmail());
								    		usuarioActualizar.setUltimoUsuarioModificacion(usuario.getCodigoOID());
							            	usuarioActualizar.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
							            	usuarioActualizar.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());                
							                if (StringUtil.isNotNullOrBlank(usuario.getContrasena())) {                    
							                	usuarioActualizar.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
							                }else{
							                	usuarioActualizar.setContrasena(null);
							                }
					    	        		usuarioServiceLocal.actualizarUsuario(usuarioActualizar);
					    	        		respuesta = new RespuestaDTO(true, "['']", parametros);
					    	        		respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
					    	        }
					    		
					    		/////////////////////////////////////////////////////////////////////////////
					    	        if(indExisteEmail){
					    	        	   respuesta = new RespuestaDTO(false, "['El email ingresado ya se encuentra asignado a otro usuario. Verifique por favor.']", parametros);	
					    	        	   respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
					    	        }
//					    	        respuesta = new RespuestaDTO(false, "['El email ingresado ya se encuentra asignado a otro usuario. Verifique por favor.']", parametros);		
					    	
					    	}
					    	
			            		        
					    }else{
					    	respuesta = new RespuestaDTO(false, "['La contraseña actual no es válida']", parametros);
					    	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
					    }              
		            
		            
					  }else{
						  
						  respuesta = new RespuestaDTO(false, "['No puede usar una contraseña antigua']", parametros);
						  respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
					  } 
		            	
		            	
		            
		            
		            
		            
		            }else{
		            	respuesta = new RespuestaDTO(false, "['"+USUARIO_CODOID_INEXISTENTE+"']", parametros);  
		            	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
		            }
            
        	}else{
        				respuesta = new RespuestaDTO(false, "['Se detectó cambios sobre datos no permitidos a través de este formulario. Verifique los datos ingresados']", parametros);	
        				respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
        	}													
            
            
        } catch (Exception e) {
        	parametros.clear();
        	if (log.isHabilitadoError()) {
				log.error(e);
			}
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			//return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
        }
        //return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
        
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
        
    }
    

  
@SuppressWarnings("rawtypes")
@POST
@Path("/registroUsuarioOrganismo")
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
public Response registroUsuarioOrganismo(String jsonEncrypted) { 
    	boolean indicadorVEDU = false;
    	boolean existeCorreoPersonal = false;
    	UsuarioOrganismoDTO usuarioOrganismo;
    	String respEncrypt="";
    	UsuarioOrganismoWsDTO uorg=new UsuarioOrganismoWsDTO();  //usuarioOrganismoWsDTO
    	String jsonEncrypteddcInexus="";
    	String jsonEncrypteddcJodexus="";
    	NewUsuarioCompareDTO objInexus=null;
    	NewUsuarioCompareDTO objJodexus=null;
    	NewUsuarioCompareDTO objCampos=new NewUsuarioCompareDTO();
        try{
        	
        	
        	param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			uorg = gson.fromJson(objJsonString, UsuarioOrganismoWsDTO.class);
			
        	
        	
        	
//        	uorg = gson.fromJson(desencriptar(jsonEncrypted), UsuarioOrganismoWsDTO.class);	

        	
        	
        	
        	try{
        		jsonEncrypteddcInexus = aesUtil.descifrarBase64(uorg.getInexus().substring(9));
        		
        		objInexus=gson.fromJson(jsonEncrypteddcInexus, NewUsuarioCompareDTO.class);
        		
        		jsonEncrypteddcJodexus = aesUtil.descifrarBase64(uorg.getJodexus());
        		
        		objJodexus=gson.fromJson(jsonEncrypteddcJodexus, NewUsuarioCompareDTO.class);
        		
        		}catch(StringIndexOutOfBoundsException e){
        			throw new CadenaInvalidaException();
        		}catch(NullPointerException e){
        			throw new CadenaInvalidaException();
        		}

        	
        	if( objInexus.getIndModificacion()!=null ){
        		//SECURITY AL EDITAR
        		objCampos.setIdOrgPrf(objInexus.getIdOrgPrf());//--------------------------------------
        		objCampos.setNombre(objInexus.getNombre());/////////////////////////////
        		objCampos.setApellidoPaterno(objInexus.getApellidoPaterno());/////////////////////////
        		objCampos.setApellidoMaterno(objInexus.getApellidoMaterno());////////////////////
        		objCampos.setNombreCompleto(objInexus.getNombreCompleto());
        		objCampos.setEmail(objInexus.getEmail());//////////////////////////////////////
        		objCampos.setTipoDocumento(objInexus.getTipoDocumento());/////////////////
        		objCampos.setDocumento(objInexus.getDocumento());////////////////////////////
        		objCampos.setCodoid(objInexus.getCodoid());
        		objCampos.setEstado(objInexus.getEstado());
        		objCampos.setCelular(objInexus.getCelular());////////////////////////////////////
        		objCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
        		objCampos.setIdOrgan(objInexus.getIdOrgan());
        		objCampos.setEmailInstitucional(objInexus.getEmailInstitucional());//--------------------------
        		objCampos.setNrotflins(objInexus.getNrotflins());//------------------------------------
        		objCampos.setNroanxins(objInexus.getNroanxins());//----------------------------------
        		objCampos.setCargo(objInexus.getCargo());//----------------------------------------------
        		objCampos.setIndadm(objInexus.getIndadm());
        		objCampos.setFechaRegistro(objInexus.getFechaRegistro());
        		objCampos.setTiposolicitud(objInexus.getTiposolicitud());
        		objCampos.setIndExisteUsuario(objInexus.getIndExisteUsuario());
        		objCampos.setEstadoOrganizacion(objInexus.getEstadoOrganizacion());//---------------------------------
        		objCampos.setClave(objInexus.getClave());
        		objCampos.setIndModificacion(objInexus.getIndModificacion());
        		objCampos.setIdRol(objInexus.getIdRol());
        		objCampos.setIdPerfil(objInexus.getIdPerfil());//-----------------------------------------
        		
        		
        	}else{
        	//SECURITY AL REGISTRAR
        
        	objCampos.setIdUsuario(objInexus.getIdUsuario());
        	objCampos.setIdOrgPrf(objInexus.getIdOrgPrf());
        	objCampos.setNombre(objInexus.getNombre());
        	objCampos.setApellidoPaterno(objInexus.getApellidoPaterno());
        	objCampos.setApellidoMaterno(objInexus.getApellidoMaterno());
        	objCampos.setNombreCompleto(objInexus.getNombreCompleto());
        	objCampos.setEmail(objInexus.getEmail());
        	objCampos.setTipoDocumento(objInexus.getTipoDocumento());
        	objCampos.setDocumento(objInexus.getDocumento());
        	objCampos.setCodoid(objInexus.getCodoid());
        	objCampos.setEstado(objInexus.getEstado());
        	objCampos.setCelular(objInexus.getCelular());
        	objCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
        	objCampos.setIndactmgr(objInexus.getIndactmgr());
        	objCampos.setIdOrgan(objInexus.getIdOrgan());
        	objCampos.setIdRol(objInexus.getIdRol());
        	objCampos.setNrocelins(objInexus.getNrocelins());
        	objCampos.setEmailInstitucional(objInexus.getEmailInstitucional());
        	objCampos.setNrotflins(objInexus.getNrotflins());
        	objCampos.setNroanxins(objInexus.getNroanxins());
        	objCampos.setCargo(objInexus.getCargo());
        	objCampos.setIndadm(objInexus.getIndadm());
        	objCampos.setFechaRegistro(objInexus.getFechaRegistro());
        	objCampos.setTiposolicitud(objInexus.getTiposolicitud());
        	objCampos.setIdPerfil(objInexus.getIdPerfil());
        	objCampos.setIndExisteUsuario(objInexus.getIndExisteUsuario());
        	
        	}
        	if(!objInexus.equals(objJodexus)||!objInexus.equals(objCampos)||!objJodexus.equals(objCampos))
			{
				
        		throw new CadenaInvalidaException();
			
			}
    		else{
//    			parametros.put("xtu", true);
//    			respuesta = new RespuestaDTO(true, "['']", parametros);
//    			respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
    		
        	
        	
        	try {
            
//        	uorg = gson.fromJson(desencriptar(jsonEncrypted), UsuarioOrganismoWsDTO.class);	
        	
        	String flagCreacion = "";  //, String jsonEncrypted
            Integer solicitud = 0;
            UsuarioDTO usuarioDTO = new UsuarioDTO();
            UsuarioSessionVO usuarioSessionVO = new UsuarioSessionVO();
            usuarioSessionVO.setOidUsuario(objInexus.getUsuarioCreacion());
            usuarioSessionVO.setNombreUsuario(objInexus.getNombreCompleto());            
            if (StringUtil.isNotNullOrBlank(objInexus.getTiposolicitud())) {
                solicitud = objInexus.getTiposolicitud();
            }            
            switch(solicitud){
                case 1: flagCreacion = TipoAccionAdministrarUsuario.NUEVO_USUARIO.getValue();
                        usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(objInexus.getCodoid());
                        if (usuarioDTO != null) {
                        	respuesta = new RespuestaDTO(false, "['"+USUARIO_EXISTE_CODOID+"']", null);
                        	 respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
                 			 return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
                        	//return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
                        }
                        if (StringUtil.isNotNullOrBlank(objInexus.getEmail())) {
                        	existeCorreoPersonal = usuarioServiceLocal.existeCorreoPersonal(objInexus.getEmail(),objInexus.getCodoid());//Verifica si el correo personal es unico en todas las organizaciones.
                            if (existeCorreoPersonal) {
                         	   throw new CorreoPersonalUsuarioExisteException();
                            }
						}                        
                     break;
                case 2:
                case 3: flagCreacion = TipoAccionAdministrarUsuario.NUEVO_USUARIO.getValue();
//                		flagCreacion = TipoAccionAdministrarUsuario.MODIFICAR_USUARIO.getValue();
                		indicadorVEDU = true;
                	break;
                case 4: flagCreacion = TipoAccionAdministrarUsuario.MODIFICAR_USUARIO.getValue();
		                if (StringUtil.isNotNullOrBlank(objInexus.getEmail())) {
		                	existeCorreoPersonal = usuarioServiceLocal.existeCorreoPersonal(objInexus.getEmail(),objInexus.getCodoid());//Verifica si el correo personal es unico en todas las organizaciones.
		                    if (existeCorreoPersonal) {
		                 	   throw new CorreoPersonalUsuarioExisteException();
		                    }
						} 
                	break;
                default: flagCreacion = "Defecto";
                	break;
            }            
            usuarioDetalle = new UsuarioOrganismoDTO();
            usuarioDetalle.setUsuario(new UsuarioDTO());
            usuarioDetalle.getUsuario().setId(objInexus.getIdUsuario());// imendoza 20170117
            usuarioDetalle.getUsuario().setCodigoOID(objInexus.getCodoid().trim().toUpperCase());
            usuarioDetalle.getUsuario().setNombres(objInexus.getNombre().trim().toUpperCase());
            usuarioDetalle.getUsuario().setApellidoPaterno(objInexus.getApellidoPaterno().trim().toUpperCase());
            usuarioDetalle.getUsuario().setApellidoMaterno(objInexus.getApellidoMaterno().trim().toUpperCase());
            usuarioDetalle.getUsuario().setNombreCompleto(objInexus.getNombreCompleto().trim().toUpperCase());
            usuarioDetalle.getUsuario().setTipoDocumento(objInexus.getTipoDocumento().trim().toUpperCase());
            usuarioDetalle.getUsuario().setDescDocumento(objInexus.getDocumento().trim().toUpperCase());
            usuarioDetalle.getUsuario().setEmail(StringUtil.isNotNullOrBlank(objInexus.getEmail())?objInexus.getEmail().trim().toUpperCase():null);
            usuarioDetalle.getUsuario().setCelular(objInexus.getCelular().trim().toUpperCase());
//            usuarioDetalle.getUsuario().setFechaCreacion(FechaUtil.obtenerFechaActual());//imendoza
            if(StringUtil.isNotNullOrBlank(objInexus.getEstado())){
            	usuarioDetalle.getUsuario().setEstado(objInexus.getEstado());
            }
            usuarioDetalle.setTelefonoInstitucional(objInexus.getNrotflins().trim().toUpperCase());
            usuarioDetalle.setAnexoInstitucional(objInexus.getNroanxins().trim().toUpperCase());
            usuarioDetalle.setIndicadorAdministradorEntidad(objInexus.getIndadm());
            usuarioDetalle.setCargo(objInexus.getCargo());
            usuarioDetalle.setEmailInstitucional(objInexus.getEmailInstitucional());
            if(StringUtil.isNotNullOrBlank(objInexus.getEstadoOrganizacion())){
            	usuarioDetalle.setEstado(objInexus.getEstadoOrganizacion());
            }
            if (flagCreacion.equals(TipoAccionAdministrarUsuario.NUEVO_USUARIO.getValue()) && indicadorVEDU) {
               usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(objInexus.getCodoid());                
               usuarioDetalle.setUsuarioOrganismoPK(new UsuarioOrganismoPKDTO(usuarioDTO.getId(), objInexus.getIdOrgan()));
               usuarioDetalle.getUsuario().setId(usuarioDTO.getId());
               usuarioDetalle.getUsuario().setFechaCreacion(FechaUtil.obtenerFechaActual());//imendoza
	            if (objInexus.getEstado().equalsIgnoreCase(EstadoState.INACTIVO.getKey())) {
	                usuarioDetalle.setEstado(EstadoState.INACTIVO.getKey());
	                usuarioDetalle.getUsuario().setEstado(EstadoState.INACTIVO.getKey());                    
	            }else{
	                usuarioDetalle.setEstado(EstadoState.ACTIVO.getKey());
	                usuarioDetalle.getUsuario().setEstado(EstadoState.ACTIVO.getKey()); 
	            }
            }
            //imendoza 20170117 inicio
            if (flagCreacion.equals(TipoAccionAdministrarUsuario.MODIFICAR_USUARIO.getValue())){
            	 usuarioDetalle.setOrganismo(new OrganismoDTO(objInexus.getIdOrgan()));
            	 List<UsuarioRolDTO> listaUsRol = new ArrayList<UsuarioRolDTO>();
            	 if (Objects.isNull(objInexus.getIdUsuario()) && Objects.isNull(usuarioDetalle.getUsuario().getId())) {
            		 usuarioDetalle.getUsuario().setId(usuarioServiceLocal.verificarCodoid(objInexus.getCodoid()).getId());
            		 objInexus.setIdUsuario(usuarioDetalle.getUsuario().getId());
 				 }
            	 listaUsRol.add(new UsuarioRolDTO(objInexus.getIdUsuario(), objInexus.getIdOrgan(), objInexus.getIdPerfil(), objInexus.getIdRol()));
            	 usuarioDetalle.setListaUsuarioRol(listaUsRol);
            	 usuarioDetalle.setIndicadorAdministradorEntidad(0);
            	 usuarioDetalle.setOrganismoPerfil(new OrganismoPerfilDTO(objInexus.getIdOrgPrf()));
            	 PerfilDTO perfil = new PerfilDTO(objInexus.getIdPerfil());
            	 usuarioDetalle.getOrganismoPerfil().setPerfil(perfil);
            	 usuarioDetalle.setUsuarioOrganismoPK(new UsuarioOrganismoPKDTO(objInexus.getIdUsuario(), objInexus.getIdOrgan()));
                 usuarioDetalle.getUsuario().setUltimoUsuarioModificacion(usuarioSessionVO.getOidUsuario());
                 usuarioDetalle.getUsuario().setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                 usuarioDetalle.setUltimoUsuarioModificacion(usuarioSessionVO.getOidUsuario());
                 usuarioDetalle.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());   
                 if (StringUtil.isNotNullOrBlank(objInexus.getClave())) {
                	 BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                	 usuarioDetalle.getUsuario().setContrasena(passwordEncoder.encode(objInexus.getClave()));                	 
				 }
                 if(StringUtil.isNotNullOrBlank(objInexus.getIndModificacion())){
                	 switch (objInexus.getIndModificacion()) {// imendoza 20170125 1: cambio datos usuario 2: datos relacion org 3: ambos
						case "1":
							usuarioServiceLocal.actualizar(usuarioDetalle.getUsuario());
							break;
						case "2":
							usuarioServiceLocal.actualizar(usuarioDetalle);
							break;
						case "3":
							usuarioServiceLocal.actualizar(usuarioDetalle.getUsuario());
							usuarioServiceLocal.actualizar(usuarioDetalle);
							break;
						default:
							break;
						}
                 }                                 
                 respuesta = new RespuestaDTO(true, "['"+ACTUALIZAR_USUARIO_EXITOSA+"']", null);
                 respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
     			 return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
                 //return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
            }
            //imendoza 20170117 fin            
            usuarioDetalle.setOrganismoPerfil(new OrganismoPerfilDTO());
            usuarioDetalle.getOrganismoPerfil().setPerfil(new PerfilDTO(objInexus.getIdPerfil()));            
            RolDTO  rol = new RolDTO();
            rol.setId(objInexus.getIdRol());
            rol.setHidden(true);
            List<RolDTO> listaRol = new ArrayList<RolDTO>();
            listaRol.add(rol);            
           
            context.setLocale(context.getLocale());
            context.setUsuarioSessionVO(usuarioSessionVO);
            ServiceContext.setCurrent(context);            
//            UsuarioOrganismoDTO usuarioOrganismo;
            usuarioOrganismo = usuarioServiceLocal.administrarUsuarioRegistrar(usuarioDetalle, context, listaRol,objInexus.getIdOrgan(),flagCreacion,indicadorVEDU);            
            if (usuarioOrganismo.getUsuario().getId() == null && usuarioOrganismo.getOrganismo().getId() == null) {
            	respuesta = new RespuestaDTO(false, "['"+REGISTRO_USUARIOORGANISMO_SIN_EXITO+"']", null);
            	 respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
     			 return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
            	//return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
            }else{
                envioCorreoConfirmacion(usuarioOrganismo);
                respuesta = new RespuestaDTO(true, "['"+REGISTRO_USUARIOORGANISMO_EXITOSA+"']", null);
                respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
    			return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
               // return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
            }                 
        } catch (UsuarioEntidadExisteException | UsuarioEntidadPerfilExisteException | CorreoPersonalUsuarioExisteException e ){
        	if (log.isHabilitadoError()) {
        		log.error(e);
        	}
             respuesta = new RespuestaDTO(false, "['"+getErrorService().getErrorFor(e, ResourceUtil.obtenerLocaleSession()).getDefaultMessage()+"']", null);
             respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
             return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
            // return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build(); 
        }
        catch (Exception ex) {
        	if (log.isHabilitadoError()) {
        		log.error(ex);
        	}
            respuesta = new RespuestaDTO(true, "['"+ERROR_INTERNO+"']", null);
            respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build(); //error interno
            // return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();            
        
        }
        	
    		}
        	
        	
        }	
        catch(CadenaInvalidaException ex){
    		if (log.isHabilitadoError()) log.error(ex);

    	
    		parametros.put("rare", true);
    		respuesta = new RespuestaDTO(false, "['"+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() +"']", parametros);
    		respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
    		return Response.status(Status.NOT_ACCEPTABLE).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
    	}
        
        
        
    }
        
    @POST
    @Path("/obtenerDatosUsuario")    
    public String obtenerDatosUsuario(String url) {   
        UsuarioDTO usuarioAdmin;
        UsuarioDTO usuario; 
        passwordEncoder = new BCryptPasswordEncoder();
        String idCodoId,pass,idCodoIdBus;
        List<UsuarioOrganismoDTO> listaUsuarioOrganismoAdmin;
        List<UsuarioOrganismoDTO> listaUsuarioOrganismo;
        try {
            idCodoId = QueryParamURL.getParam(url, "idCodoId");
            pass = QueryParamURL.getParam(url, "pass");
            idCodoIdBus = QueryParamURL.getParam(url, "idCodoIdBus");            
            if (StringUtil.isNotNullOrBlank(idCodoId) 
                    && StringUtil.isNotNullOrBlank(pass)) {                                                
                usuarioAdmin = administracionServiceLocal.buscarUsuarioxOID(idCodoId);                
                if (passwordEncoder.matches(pass, usuarioAdmin.getContrasena())) {
                    if (StringUtil.isNotNullOrBlank(idCodoIdBus)) {
                        usuario = administracionServiceLocal.buscarUsuarioxOID(idCodoIdBus);
                        if (usuario == null) {
                            return USUARIO_INEXISTENTE_CODOID;
                        }else{
                            if(usuario.getEstado().equals(UsuarioState.ACTIVO.getKey())){
                                    listaUsuarioOrganismo = administracionServiceLocal.obtenerUsuarioOrganismoByidUsuario(usuario.getId());
                                    listaUsuarioOrganismoAdmin = administracionServiceLocal.obtenerUsuarioOrganismoByidUsuario(usuarioAdmin.getId());
                                    for (UsuarioOrganismoDTO usuarioOrganismoDTO : listaUsuarioOrganismo) {
                                        usuarioOrganismoDTO.setUsuario(null);          
                                        usuarioOrganismoDTO.setOrganismoPerfil(null);
                                            for (UsuarioRolDTO usuarioRolDTO : usuarioOrganismoDTO.getListaUsuarioRol()) {
                                                usuarioRolDTO.setUsuarioOrganismo(null);                                        
                                        }
                                    }
                                    usuario.setListaUsuarioOrganismo(listaUsuarioOrganismo);
                                    usuario.setFechaCreacion(null);
                                    usuario.setContrasena(null);
                                    for (UsuarioOrganismoDTO usuarioOrganismo : listaUsuarioOrganismoAdmin) {
                                        if (usuarioOrganismo.getIndicadorAdministradorEntidad() == 0) {
                                            return CREDENCIALES_CORRECTAS_NO_ADMINISTRADOR;
                                        }
                                        for (UsuarioOrganismoDTO usuarioOrganismoAdmin : listaUsuarioOrganismo) {
                                            if (!(usuarioOrganismo.getOrganismo().getId().equals(usuarioOrganismoAdmin.getOrganismo().getId()))) {
                                                return USUARIO_EXISTE_NO_ENTIDAD;
                                            }
                                        }
                                    }
                                    JSONArray jsArray = new JSONArray();
                                    JSONObject job;
                                    usuario.setContrasena(null);

                                    job = new JSONObject(usuario);
                                    jsArray.put(job);
                                    return  jsArray.toString();                                                              
                            }else{
                                return USUARIO_INHABILITADO;
                            }
                        }
                    }else{
                        return USUARIO_INEXISTENTE_CODOID;
                    }
                }else{
                    return CREDENCIALES_INCORRECTAS;
                }
            }           
            
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
        		log.error(ex);
        	}
            return ERROR_INTERNO;
        }
        return ERROR_INTERNO;
    }
    
    private void envioCorreoConfirmacion(UsuarioOrganismoDTO dto) throws Exception {
		try {
			List<String> destinatarios = new ArrayList<String>();
			destinatarios.add(dto.getUsuario().getEmail());
			Map<String, String> map = new HashMap<String, String>();
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_USUARIOACCESO, dto
					.getUsuario().getCodigoOID());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_FREGISTRO, FechaUtil
					.obtenerFechaFormatoSimple(dto.getUsuario()
							.getFechaCreacion()));
			map.put(NotificacionUtil.ELEMENTO_LABEL_USUARIO,
					dto.getIndicadorAdministradorEntidad().intValue() == IndicadorConstant.INDICADOR_ACTIVO ? NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO_ADM
							: NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO);
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_ENTIDAD, dto
					.getOrganismo().getNombreOrganismo().toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_DESTINATARIO, dto
					.getUsuario().getNombreCompleto()
					.toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_CLAVEACCESO, dto
					.getUsuario().getClave());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_PERFIL,
					dto.getOrganismoPerfil().getPerfil().getNombre());
			map.put(NotificacionUtil.ELEMENTO_CADUCIDAD_CONTRASENHA,
					this.administracionServiceLocal
							.getParametro(ParametroType.NUMERO_DIAS_CAMBIO_CONTRASENHA
									.getValue()));
			notificacionServiceLocal.enviarCorreoConfirmacionUsuario(
					NotificacionUtil.ASUNTO_NOTIFICACION_EMAIL_CREACION_USUARIO.toString(), destinatarios, map,
					NotificacionUtil.RUTA_TEMPLATES_CREA_USUARIO.toString(),
					null);
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
		}
	}
    
    @SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerDatos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerDatos(String jsonEncrypted) {
		List<Object> lista = new ArrayList<Object>();
		ConsultaJson consultaJson = new ConsultaJson();
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		try {
			//INICIO desencriptar			
//			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
//			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
//			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
//			
//			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
//			consultaJson = gson.fromJson(objJsonString, ConsultaJson.class);
			//FIN desencriptar
			consultaJson = gson.fromJson(desencriptar(jsonEncrypted), ConsultaJson.class);
			
			
			String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
					? consultaJson.getGroupFilter().toString() : "";
			lista = objectService.findByFirstMaxResultJson(consultaJson.obtenerFields(), valor, consultaJson.getTable(),
					consultaJson.getOrder(), consultaJson.getGroup(), consultaJson.getBatchSize(), consultaJson.getIndex());
			if (!lista.isEmpty()) {
				parametros.put("salt", salt);
				parametros.put("iv", iv);
				parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
				respuesta = new RespuestaDTO(true, "[]", parametros);
			} else if (lista.isEmpty()) {
				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", lista);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

    @SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerDatosDistinct")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerDatosDistinct(String jsonEncrypted) {
		List<Object> lista = new ArrayList<Object>();
		ConsultaJson consultaJson = new ConsultaJson();
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		try {
			//INICIO desencriptar			
//			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
//			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
//			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
//			
//			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
//			consultaJson = gson.fromJson(objJsonString, ConsultaJson.class);
			//FIN desencriptar ACTIVO
			consultaJson = gson.fromJson(desencriptar(jsonEncrypted), ConsultaJson.class);
			
			
			String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
					? consultaJson.getGroupFilter().toString() : "";
			lista = objectService.findByFirstMaxResultJsonDistinct(consultaJson.obtenerFields(), valor, consultaJson.getTable(),
					consultaJson.getOrder(), consultaJson.getGroup(), consultaJson.getBatchSize(), consultaJson.getIndex());
			if (!lista.isEmpty()) {
				parametros.put("salt", salt);
				parametros.put("iv", iv);
				parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
				respuesta = new RespuestaDTO(true, "[]", parametros);
			} else if (lista.isEmpty()) {
				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", lista);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}
    
    @SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerDatosUsuOrgPer")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerDatosUsuOrgPer(String jsonEncrypted) {
		List<Object> lista = new ArrayList<Object>(); 
//		List<Object> listaUpd = new ArrayList<Object>(); 
//		String estado="";
//		String estadoOrganizacion="";
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		ConsultaJson consultaJson = new ConsultaJson();
		try {
			consultaJson = gson.fromJson(desencriptar(jsonEncrypted), ConsultaJson.class);
			String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
					? consultaJson.getGroupFilter().toString() : "";
			if (StringUtil.isNotNullOrBlank(valor)) {
				lista = usuarioServiceLocal.buscarUsuarioOrganismoPerfilxDni(valor);
//				for(int i=0;i<lista.size();i++){
//					Object o=lista.get(i);
//					
//					if(o.equals("ACTIV"))o="ACTIVO";
//					if(o.equals("INACT"))o="INACTIVO";
//				}
			}			
			if (!lista.isEmpty()) {
				parametros.put("salt", salt);
				parametros.put("iv", iv);
				parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
				respuesta = new RespuestaDTO(true, "[]", parametros);
			} else if (lista.isEmpty()) {
				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", lista);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}
    //imendoza 20170110
//    @SuppressWarnings("rawtypes")
//	@POST
//	@Path("/obtenerDatosUsuOrgPer")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response obtenerDatosUsuOrgPer(String jsonEncrypted) {
//		List<Object> lista = new ArrayList<Object>();
//		String dni = "";
//		aesUtil = new AesUtil(keySize, iterationCount);	
//		parametros = new HashMap<>();
//		try {			
//			dni = desencriptar(jsonEncrypted);
//			if (StringUtil.isNotNullOrBlank(dni)) {
//				lista = usuarioServiceLocal.buscarUsuarioOrganismoPerfilxDni(dni);
//			}			
//			if (!lista.isEmpty()) {
//				parametros.put("salt", salt);
//				parametros.put("iv", iv);
//				parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
//				respuesta = new RespuestaDTO(true, "[]", parametros);
//			} else if (lista.isEmpty()) {
//				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
//			}
//		} catch (Exception e) {
//			if (log.isHabilitadoError()) {
//				log.error(e);
//			}
//			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", lista);
//			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
//		}
//		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
//	}
    
    @SuppressWarnings("rawtypes")
	@POST
	@Path("/validaCon")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validaCon(final String jsonEncrypted) {		
		UsuarioDTO usuarioClave = new UsuarioDTO();
		parametros = new HashMap<>();
		try {
			UsuarioDTO usuario = gson.fromJson(desencriptar(jsonEncrypted), UsuarioDTO.class);
		    usuarioClave = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID());
		    if (Objects.nonNull(usuarioClave)) {
		    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			    if(passwordEncoder.matches(usuario.getContrasena(), usuarioClave.getContrasena())){
			    	respuesta = new RespuestaDTO(true, "['']", parametros);		        
			    }else{
			    	respuesta = new RespuestaDTO(false, "['']", parametros);	
			    }
			}else{
				throw new UsuarioNoExisteException();
			}
		} catch (UsuarioNoExisteException er) {
			if (log.isHabilitadoError()) {
				log.error(er);
			}
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['"+ getErrorService().getErrorFor(er, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() +"']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();			
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", null);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}
    
    @SuppressWarnings("rawtypes")
	@POST
	@Path("/uescalet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response uescalet(String jsonEncrypted) {
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		try {		
			param = QueryParamURL.getParam(jsonEncrypted, "uescalet");	//uescalet: significa Token		
			
			
			sucess = AuthUtils.validarToken(param);			
			if (sucess) {
				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
//				parametros.put("chekstatetk", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
			}else{
//				parametros.put("chekstatetk", false);
				respuesta = new RespuestaDTO(false, "['']", parametros);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
//			parametros.put("chekstatetk", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}
    

	
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/getTokenAutorized")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getTokenAutorized(String jsonEncrypted) {
    	
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		try {		
			param = QueryParamURL.getParam(jsonEncrypted, "uescalet");	//uescalet: significa Token		
			
			
			sucess = AuthUtils.validarTokenAutorized(param);			
			if (sucess) {
				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
				parametros.put("chekstatetk", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
			}else{
				parametros.put("chekstatetk", false);
				respuesta = new RespuestaDTO(false, "['']", parametros);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			parametros.put("chekstatetk", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
    	
    }
    
    
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/getTokenRequest")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getTokenRequest(String jsonEncrypted) {
    	
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		try {		
			param = QueryParamURL.getParam(jsonEncrypted, "uescalet");	//uescalet: significa Token		
			
			
			sucess = AuthUtils.validarTokenRequest(param);			
			if (sucess) {
				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
				parametros.put("chekstatetk", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
			}else{
				parametros.put("chekstatetk", false);
				respuesta = new RespuestaDTO(false, "['']", parametros);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			parametros.put("chekstatetk", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
    	
    }
    
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/getRequestfl")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getRequestfl(String jsonEncrypted) {
    	
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		try {		
			param = QueryParamURL.getParam(jsonEncrypted, "uescalet");	//uescalet: significa Token		
			
			
			sucess = AuthUtils.validarTokenPdf(param);			
			if (sucess) {
				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
				parametros.put("chekstatetk", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
			}else{
				parametros.put("chekstatetk", false);
				respuesta = new RespuestaDTO(false, "['']", parametros);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			parametros.put("chekstatetk", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
    	
    }
    
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/getRequestForm")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getRequestForm(String jsonEncrypted) {
    	
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		TokenSecuryDTO dtotk=new TokenSecuryDTO();
		try {		
			param = QueryParamURL.getParam(jsonEncrypted, "uescalet");	//uescalet: significa Token		
			
			securyTokenServiceLocal.eliminarTokenForm(dtotk);
			
		//	sucess = AuthUtils.validarTokenFormulario(param);			
//			if (sucess) {
//				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
//				parametros.put("chekstatetk", true);
//				respuesta = new RespuestaDTO(true, "['']", parametros);
//			}else{
//				parametros.put("chekstatetk", false);
//				respuesta = new RespuestaDTO(false, "['']", parametros);
//			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			parametros.put("chekstatetk", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
    	
    }
    
    
    @SuppressWarnings("rawtypes")
   	@POST
   	@Path("/crearTokenAutorizedPerfil")
   	@Produces(MediaType.APPLICATION_JSON)  
   	public Response crearTokenAutorizedPerfil(String jsonEncrypted) {
   		aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		Boolean sucess;
   		AutenticacionDTO aut = new AutenticacionDTO();
   		String respEncryp;

   		
   		try {		
   			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
   			
   			aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);	
//            parametros.put("ipHostRemoto", aut.getIpHostRemoto());
            parametros.put("perfil", aut.getIdCodoId());
         //   parametros.put("identificador", passphraseToken); //
            parametros.put("identificador", passphrase); 
            parametros.put("Mensajes", mensajes);
            
    		final Token token = AuthUtils.createTokenAutorized(aut.getIpHostRemoto(), parametros);
    		
    		System.out.println("TOKEN RECIBIDO-->" + token.getToken() );
    		
    		respuesta = new RespuestaDTO(true, "[]", token);
        
    		respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
            
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

   		} catch (Exception ex) {
   	       	if (log.isHabilitadoError()) {
   	       		log.error(ex);
   	       	}
   	           parametros.put("ExceptionTipo", ex.toString());
   	           parametros.put("Exception", ex.getMessage());
   	           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
   	           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   	           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
   	       }  
   	}
    
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/registrarSesionNavegacion")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response registrarSesionNavegacion(String jsonEncrypted) {        
    	parametros = new HashMap<>();
        try {
        	AccesoDTO sesionExistente;
        	AccesoDTO sesionExisNavegHost=new AccesoDTO();
        	AccesoDTO sesionRegister=new AccesoDTO();
        	
        	UsuarioDTO usuarioDTO=new UsuarioDTO();
        	
        	List<AccesoDTO> listAceesos=new ArrayList<AccesoDTO>();
        	sesionExistente = gson.fromJson(desencriptar(jsonEncrypted), AccesoDTO.class);
        	if(sesionExistente.getToken()!= null && !sesionExistente.getToken().isEmpty() ){                  
        		sesionExistente.setNavegador(sesionExistente.getNavegador());///////////////////////
        		sesionExistente.setFecIniSesion(FechaUtil.obtenerFechaActual());
        		sesionExistente.setFecFinSesion(FechaUtil.sumarMinutosCalendar(FechaUtil.obtenerFechaActual(), 20)  );
        		sesionExistente.setUsuarioCreacion(sesionExistente.getUsuario());
        		
        		sesionExistente.setHostcliente(sesionExistente.getHostcliente());/////////////////////////
        		
        		//consultar por usuario
        		sesionExisNavegHost.setUsuario(sesionExistente.getUsuario());
//        		sesionExisNavegHost.setNavegador(sesionExistente.getNavegador());
//        		sesionExisNavegHost.setHostcliente(sesionExistente.getHostcliente());
        		listAceesos=sesionServiceLocal.verificarNevegacionUserTupla(sesionExisNavegHost);
        		if(listAceesos.size()>0){
        			System.out.println("VERIFICAR QUE LA TUPLA REGISTRADA LE CORRESPONDE A LA SESION: "  + listAceesos);
//        			//obtener navegador y host registrado en tupla
//		        			if(listAceesos.size()==1){
//		        				temBrowHost.setUsuario(listAceesos.get(0).getUsuario());
//		        				temBrowHost.setNavegador(listAceesos.get(0).getNavegador());
//		        				temBrowHost.setHostcliente(listAceesos.get(0).getHostcliente());
//		        				temBrowHost.setIndAcc(listAceesos.get(0).getIndAcc());
////		        			}
//		        			if(sesionExistente.getUsuario().equals(temBrowHost.getUsuario())&&sesionExistente.getNavegador().equals(temBrowHost.getNavegador())&&
//		        					sesionExistente.getHostcliente().equals(temBrowHost.getHostcliente())){
		        				//eliminar la existente 
		        				AccesoDTO delDTO=new AccesoDTO();
		        				delDTO.setUsuario(listAceesos.get(0).getUsuario());
		        				sesionServiceLocal.eliminarNevegacionUser(delDTO);
		        				//registrar la actual
		        				sesionServiceLocal.registrarNevegacionUser(sesionExistente);
		        				parametros.put("exito", true);
		        				parametros.put("dpnv", true);
		        				
		        				
//		    					///////////////////////////////////////////////REGISTRO DE HISTORICO DE CLAVES////////////////////////////////////////////////////////
//		    					ClaveDTO clavedto=new ClaveDTO();
//		    					clavedto.setCodigo(sesionExistente.getUsuario());
//		    					usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(sesionExistente.getUsuario());  
//		    					clavedto.setClave(usuarioDTO.getContrasena());
//		    					clavedto.setTiempoVigencia(2);
//		    					clavedto.setUsuCrea(String.valueOf(sesionExistente.getUsuario()));
//		    					clavedto.setFecCrea(FechaUtil.obtenerFechaActual());
//		    		         	claveServiceLocal.registrarClaveHistorica(clavedto);	
//		    		         	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		        				
//		        			}
		        			
        			
        		}else if(listAceesos.size()==0){
        			sesionRegister.setNavegador(sesionExistente.getNavegador());///////////////////////
        			sesionRegister.setFecIniSesion(FechaUtil.obtenerFechaActual());
        			sesionRegister.setFecFinSesion(FechaUtil.sumarMinutosCalendar(FechaUtil.obtenerFechaActual(), 20)  );
        			sesionRegister.setUsuarioCreacion(sesionExistente.getUsuario());
            		
        			sesionRegister.setHostcliente(sesionExistente.getHostcliente());/////////////////////////
        			
        			sesionServiceLocal.registrarNevegacionUser(sesionExistente);
            		parametros.put("exito", true);
            		
            		
//					///////////////////////////////////////////////REGISTRO DE HISTORICO DE CLAVES////////////////////////////////////////////////////////
//					ClaveDTO clavedto=new ClaveDTO();
//					clavedto.setCodigo(sesionExistente.getUsuario());
//					usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(sesionExistente.getUsuario());  
//					clavedto.setClave(usuarioDTO.getContrasena());
//					clavedto.setTiempoVigencia(2);
//					clavedto.setUsuCrea(String.valueOf(sesionExistente.getUsuario()));
//					clavedto.setFecCrea(FechaUtil.obtenerFechaActual());
//		         	claveServiceLocal.registrarClaveHistorica(clavedto);	
//		         	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            		
            		
            		
        		}
//
//        		if(temBrowHost.getUsuario()!=null&&temBrowHost.getNavegador()!=null&&temBrowHost.getHostcliente()!=null)
//		        		if((!temBrowHost.getNavegador().equals(sesionExistente.getNavegador()) || (!temBrowHost.getHostcliente().equals(sesionExistente.getHostcliente())))){
//		        			System.out.println("SE REGISTRA EL SIGUIENTE BROWSE O HOST INCORRECTO: "  );
//							AccesoDTO sesionExistentetwo=new AccesoDTO();
//												                      
//		//						sesionExistentetwo.setNavegador(claimSettwo.toJSONObject().get("Browser").toString());
//								sesionExistentetwo.setUsuario(temBrowHost.getUsuario());
//								sesionExistentetwo.setFecIniSesion(FechaUtil.obtenerFechaActual());
//								sesionExistentetwo.setFecFinSesion(FechaUtil.sumarMinutosCalendar(FechaUtil.obtenerFechaActual(), 20)  );
//								sesionExistentetwo.setUsuarioCreacion(sesionExistente.getUsuario());
//		//						sesionExistentetwo.setHostcliente(claimSettwo.toJSONObject().get("HostRemoto").toString());
//								sesionExistentetwo.setToken(sesionExistente.getToken());
//						//sesionExistente.setFechaCreacion(FechaUtil.obtenerFechaActual());
//								sesionServiceLocal.registrarNevegacionIntrusa(sesionExistentetwo);
//		        		}
        		

        	        		
            }
        	return Response.status(Status.OK).entity(gson.toJson(parametros)).build();
	
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
    			log.error(ex);
    		}
    		parametros.put("chekstatetk", false);
    		respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
    		return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
           
        }
      //  return Response.ok(ERROR_INTERNO).build();
    }
    
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/start")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
   public Response start(String jsonEncrypted) {          
    	aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		String respEncryp="";
   		String tkActual;
        try {
        	AccesoDTO cambioSelectExistente;
        	AccesoDTO cambioToken=new AccesoDTO();
        	AccesoDTO sesionRegister=new AccesoDTO();
        	AccesoDTO cabioTokenTemp=new AccesoDTO();
        	UsuarioDTO usuarioDTO=new UsuarioDTO();
        	
        	List<AccesoDTO> listAceesos=new ArrayList<AccesoDTO>();
        	List<AccesoDTO> listAceesosCombo=new ArrayList<AccesoDTO>();
        	cambioSelectExistente = gson.fromJson(desencriptar(jsonEncrypted), AccesoDTO.class);
        	if(cambioSelectExistente!= null ){                  
        		cambioToken.setUsuario(cambioSelectExistente.getUsuario());
        		cambioToken.setToken(cambioSelectExistente.getToken());
        		
        		//read token in seccion
        		sesionRegister.setUsuario(cambioSelectExistente.getUsuario());
        		listAceesos=sesionServiceLocal.verificarNevegacionUserTupla(sesionRegister);
        		if(listAceesos.size()>0){
        						tkActual=listAceesos.get(0).getToken();
        						tkActual=tkActual+"FF##FF";
        						//update token 
        						sesionRegister.setToken(tkActual);
		        				sesionServiceLocal.actualizarTokenCambioCombo(sesionRegister);
		        				parametros.put("tkp", tkActual );
		        				respuesta = new RespuestaDTO(true, "[]", parametros);
		        				respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));


        		}
        		
				//enviar token actualizado
//				cabioTokenTemp.setUsuario(cambioSelectExistente.getUsuario());
//				listAceesosCombo=sesionServiceLocal.verificarNevegacionUserTupla(cabioTokenTemp);
//				String tksend=listAceesosCombo.get(0).getToken();
//				parametros.put("tkp", tkActual );
//				respuesta = new RespuestaDTO(true, "[]", parametros);
//				respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   		
            }else{
            	
            }
        	
                		
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
//        	return Response.status(Status.OK).entity(gson.toJson(parametros)).build();
	
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
    			log.error(ex);
    		}
    		parametros.put("chekstatetk", false);
    		respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);

	        respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
	        return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
    		
    		//return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
           
        }
      //  return Response.ok(ERROR_INTERNO).build();
    }
    
    
    //ELIMINAR LA SESION DE NAVEGACION
    @POST
    @Path("/inNever")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response inNever(String jsonEncrypted) {        
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    	parametros = new HashMap<>();
    	
//    	String respEncryp;
        try {
        	AccesoDTO sesionExistente;
         	sesionExistente = gson.fromJson(desencriptar(jsonEncrypted), AccesoDTO.class);
        	if(sesionExistente.getToken()!= null && !sesionExistente.getToken().isEmpty() ){                 
        		sesionExistente.setUsuario(sesionExistente.getUsuario());
        		sesionExistente.setNavegador(sesionExistente.getNavegador());
        		sesionExistente.setHostcliente(sesionExistente.getHostcliente());
        		sesionServiceLocal.eliminarNevegacionUserHost(sesionExistente);
        		parametros.put("exito", true);
        		
        		
        		
        		
        		
            }        	
        	return Response.status(Status.OK).entity(gson.toJson(parametros)).build();
	
        } catch (Exception ex) {
        	if (log.isHabilitadoError()) {
    			log.error(ex);
    		}
    		parametros.put("chekstatetk", false);
    		respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio' ] ", parametros);
    		return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
           
        }
        
    }
    
    @SuppressWarnings("rawtypes")
   	@POST
   	@Path("/validarPerfilxCodoid")
   	@Produces(MediaType.APPLICATION_JSON)
   	public Response validarPerfilxCodoid(String jsonEncrypted) {
   		aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		Boolean sucess;
   		AutenticacionDTO aut = new AutenticacionDTO();
   		String respEncryp;

   		
   		try {		
   			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
   			
   			aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);	
   			List<UsuarioLoginResultadoVO> listaUsuarioLogin = usuarioServiceLocal.buscarUsuarioLoginxDni(aut.getIdCodoId());
   	        @SuppressWarnings("unused")
   	        List<PerfilDTO> listaPrf = listaUsuarioLogin.stream().filter(p-> (Objects.nonNull(p.getPerfil()) && p.getPerfil().getId()!=0L))
   																		.map(UsuarioLoginResultadoVO::getPerfil)
   																		.collect(Collectors.toList()).stream()
   											   			   				.filter(CollectionUtil.distinctByKey(p -> p.getId()))
   											   			   				.collect(Collectors.toList());
   	                	
   			
          //  usuarioSession.setIdPerfil1(listaPrf.get(0).getId());     
   			
   			
            parametros.put("perfil", aut.getIdCodoId());
           // parametros.put("identificador", passphraseToken); //
            parametros.put("identificador", passphrase); 
            parametros.put("Mensajes", mensajes);
            
    		final Token token = AuthUtils.createTokenAutorized(aut.getIpHostRemoto(), parametros);
    		
    		respuesta = new RespuestaDTO(true, "[]", token);
        
    		respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
            
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

   		} catch (Exception ex) {
   	       	if (log.isHabilitadoError()) {
   	       		log.error(ex);
   	       	}
   	           parametros.put("ExceptionTipo", ex.toString());
   	           parametros.put("Exception", ex.getMessage());
   	           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
   	           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   	           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
   	       }  
   	}
    
    
    @SuppressWarnings("rawtypes")
   	@POST
   	@Path("/tokenRequestxExpirate")
   	@Produces(MediaType.APPLICATION_JSON)
   	public Response tokenRequestxExpirate(String jsonEncrypted) {
   		aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		Boolean sucess;
   		AutenticacionDTO aut = new AutenticacionDTO(); //validarRequestxTokenExpirate
   		String respEncryp;

   		
   		try {		
   			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
   			
   			aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);	
   		
          //  usuarioSession.setIdPerfil1(listaPrf.get(0).getId());     
   			
   			
            parametros.put("perfil", aut.getPerfil());
            //parametros.put("codid", aut.getIdCodoId()); //
            parametros.put("idCodoId", aut.getIdCodoId()); //
            parametros.put("identificador", passphrase); 
            parametros.put("Mensajes", mensajes);
            
    		final Token token = AuthUtils.createTokenValPeticion(aut.getIpHostRemoto(), parametros);
    		
    		
    		
    		respuesta = new RespuestaDTO(true, "[]", token);
        
    		respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
            
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

   		} catch (Exception ex) {
   	       	if (log.isHabilitadoError()) {
   	       		log.error(ex);
   	       	}
   	           parametros.put("ExceptionTipo", ex.toString());
   	           parametros.put("Exception", ex.getMessage());
   	           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
   	           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   	           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
   	       }  
   	}
    
    
    @SuppressWarnings("rawtypes")
   	@POST
   	@Path("/crearTokenExpiratefl")
   	@Produces(MediaType.APPLICATION_JSON)
   	public Response crearTokenExpiratefl(String jsonEncrypted) {
   		aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		Boolean sucess;
   		AutenticacionDTO aut = new AutenticacionDTO(); //validarRequestxTokenExpirate
   		String respEncryp;

   		
   		try {		
   			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
   			
   			aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);	
            parametros.put("perfil", aut.getPerfil());
            parametros.put("codid", aut.getIdCodoId()); //
            parametros.put("identificador", passphrase); 
            parametros.put("Mensajes", mensajes);
            
            
    		final Token token = AuthUtils.createTokenValPDFPeticion(aut.getIpHostRemoto(), parametros);
    		
    		
    		
    		respuesta = new RespuestaDTO(true, "[]", token);
        
    		respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
            
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

   		} catch (Exception ex) {
   	       	if (log.isHabilitadoError()) {
   	       		log.error(ex);
   	       	}
   	           parametros.put("ExceptionTipo", ex.toString());
   	           parametros.put("Exception", ex.getMessage());
   	           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
   	           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   	           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
   	       }  
   	}
    
    
    @SuppressWarnings("rawtypes")
   	@POST
   	@Path("/crearTokenForm")
   	@Produces(MediaType.APPLICATION_JSON)
   	public Response crearTokenForm(String jsonEncrypted) {
   		aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		Boolean sucess;
   		AutenticacionDTO aut = new AutenticacionDTO(); 
   		String respEncryp;
   		TokenSecuryDTO tokenSecuryDTO=new TokenSecuryDTO();
   		try {		
   			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
   			
   			aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);	
            parametros.put("perfil", aut.getPerfil());
            parametros.put("codid", aut.getIdCodoId()); //
            parametros.put("identificador", passphrase); 
            parametros.put("Mensajes", mensajes);
            
            
    		final Token token = AuthUtils.createTokenValFormularios(aut.getIpHostRemoto(), parametros);
    		
    		// almacenar el token.
    		tokenSecuryDTO.setToken(token.getToken());
    		securyTokenServiceLocal.registrarTokenForm(tokenSecuryDTO);
    		
    		
    		respuesta = new RespuestaDTO(true, "[]", token);
        
    		respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
            
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

   		} catch (Exception ex) {
   	       	if (log.isHabilitadoError()) {
   	       		log.error(ex);
   	       	}
   	           parametros.put("ExceptionTipo", ex.toString());
   	           parametros.put("Exception", ex.getMessage());
   	           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
   	           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   	           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
   	       }  
   	}
    
    @SuppressWarnings("rawtypes")
   	@POST
   	@Path("/crearTkSesionMaster")
   	@Produces(MediaType.APPLICATION_JSON)
   	public Response crearTkSesionMaster(String jsonEncrypted) {
   		aesUtil = new AesUtil(keySize, iterationCount);	
   		parametros = new HashMap<>();
   		Boolean sucess;
   		AutenticacionDTO aut = new AutenticacionDTO(); 
   		String respEncryp;
   		try {		
   			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
   			
   			aut = gson.fromJson(desencriptar(jsonEncrypted), AutenticacionDTO.class);	
            parametros.put("CodigoOID", aut.getIdCodoId());
            parametros.put("HostRemoto", aut.getIpHostRemoto() );
            parametros.put("Browser", aut.getBrowse() );
            parametros.put("identificador", passphrase); 
            parametros.put("Mensajes", mensajes);
            
            
            
    		final Token token = AuthUtils.createTokenSesionMaster(aut.getIpHostRemoto(), parametros);
    		
    		respuesta = new RespuestaDTO(true, "[]", token);
        
    		respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
            
    		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

   		} catch (Exception ex) {
   	       	if (log.isHabilitadoError()) {
   	       		log.error(ex);
   	       	}
   	           parametros.put("ExceptionTipo", ex.toString());
   	           parametros.put("Exception", ex.getMessage());
   	           respuesta = new RespuestaDTO(false, "['"+ERROR_INTERNO+"']", parametros);
   	           respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
   	           return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();//Error Interno
   	       }  
   	}
    
    @SuppressWarnings("rawtypes")
    @POST
    @Path("/checksesionmt")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response checksesionmt(String jsonEncrypted) {
    //	AccesoDTO aut=new AccesoDTO();
    	
		aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		try {		
			param = QueryParamURL.getParam(jsonEncrypted, "uescalet");	//uescalet: significa Token		
			
			
			sucess = AuthUtils.validarTokenMaster(param);			
			if (sucess) {
				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
				parametros.put("chekstatetk", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
			}else{
				parametros.put("chekstatetk", false);
				respuesta = new RespuestaDTO(false, "['']", parametros);
			}
		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			parametros.put("chekstatetk", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
    	
    }
    
    

    @SuppressWarnings("rawtypes")
    @POST
    @Path("/checkswinmt")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response checkswinmt(String jsonEncrypted) {
//    	AutenticacionDTO aut=new AutenticacionDTO();
    	AccesoDTO aut=new AccesoDTO();
    	aesUtil = new AesUtil(keySize, iterationCount);	
		parametros = new HashMap<>();
		Boolean sucess;
		String respEncrypt;
		try {		
			aut = gson.fromJson(desencriptar(jsonEncrypted), AccesoDTO.class);
			           
				AccesoDTO sesionExisNavegHost=new AccesoDTO();
	        	List<AccesoDTO> listAceesos=new ArrayList<AccesoDTO>();
	        		sesionExisNavegHost.setUsuario(aut.getUsuario());
	        		listAceesos=sesionServiceLocal.verificarNevegacionUserTupla(sesionExisNavegHost);
	        		
//				for(int j=0;j<listAceesos.size();j++){		
	        		if(listAceesos.size()==0){
	        			List<AccesoDTO> listAceesostwo=new ArrayList<AccesoDTO>();
	        			AccesoDTO sesionExisNavegHosttwo=new AccesoDTO();
	        			sesionExisNavegHosttwo.setUsuario(aut.getUsuario());
	        			sesionExisNavegHosttwo.setNavegador(aut.getNavegador());
	        			sesionExisNavegHosttwo.setHostcliente(aut.getHostcliente());
	        			listAceesostwo=sesionServiceLocal.verificarNevegacionUserBrowseHost(sesionExisNavegHosttwo);
		        			if(listAceesostwo.size()==0){
		        				parametros.put("isInval", false);
								parametros.put("dpnv", false);
								parametros.put("mensaje","" );
	//							break;
		        			}
							
							
	        			}
	        		else if(listAceesos.size()==1){
//							parametros.put("isInval", true);
//							parametros.put("dpnv", false);
//							parametros.put("mensaje", SESSION_USUARIO_EXISTENTE);
						
	        			
	        			//////////////////////////////////////////////////////////////
	        			//eliminar la existente 
        				AccesoDTO delDTO=new AccesoDTO();
        				delDTO.setUsuario(listAceesos.get(0).getUsuario());
        				sesionServiceLocal.eliminarNevegacionUser(delDTO);
        				//registrar la actual
        				AccesoDTO insDTO=new AccesoDTO();
        				insDTO.setUsuario(listAceesos.get(0).getUsuario());
        				insDTO.setToken(aut.getToken());
        				insDTO.setFecIniSesion(FechaUtil.obtenerFechaActual());
        				insDTO.setFecFinSesion(FechaUtil.sumarMinutosCalendar(FechaUtil.obtenerFechaActual(), 20)  );
        				insDTO.setUsuarioCreacion(listAceesos.get(0).getUsuario());
                		insDTO.setHostcliente(aut.getHostcliente());
                		insDTO.setNavegador(aut.getNavegador());
                		insDTO.setIndAcc("1");
        				sesionServiceLocal.registrarNevegacionUser(insDTO);
	        			
	        			parametros.put("isInval", true);
						parametros.put("dpnv", true);
						parametros.put("mensaje", SESSION_USUARIO_EXISTENTE);
						
						
						//////////////////////////////////////////////////////////////
						
	        			}
	        			
	        			
					if(listAceesos.size()==1){
						if(aut.getUsuario().equals(listAceesos.get(0).getUsuario())&&aut.getNavegador().equals(listAceesos.get(0).getNavegador())&&
								aut.getHostcliente().equals(listAceesos.get(0).getHostcliente())	 ){
	        				//eliminar la existente 
	        				AccesoDTO delDTO=new AccesoDTO();
	        				delDTO.setUsuario(listAceesos.get(0).getUsuario());
	        				sesionServiceLocal.eliminarNevegacionUser(delDTO);
	        				//registrar la actual
	        				AccesoDTO insDTO=new AccesoDTO();
	        				insDTO.setUsuario(listAceesos.get(0).getUsuario());
	        				insDTO.setToken(aut.getToken());
	        				insDTO.setFecIniSesion(FechaUtil.obtenerFechaActual());
	        				insDTO.setFecFinSesion(FechaUtil.sumarMinutosCalendar(FechaUtil.obtenerFechaActual(), 20)  );
	        				insDTO.setUsuarioCreacion(listAceesos.get(0).getUsuario());
	                		
	        				sesionServiceLocal.registrarNevegacionUser(insDTO);
	        				parametros.put("isInval", true);
	        				
	        				parametros.put("exito", true);
	        				parametros.put("dpnv", true);
	        				parametros.put("mensaje","" );
//	        				break;
	        			}
						
					}
					
					////verificacion desde el filter

//					AccesoDTO adto=new AccesoDTO();
//					
//					List<AccesoDTO> lisFilterValidad=sesionServiceLocal.alertIntromision(adto);
//				}
////					System.out.println("AVISO DEL FILTER : "  + lisFilterValidad );
//					boolean alerta=sesionServiceLocal.verAlertIntromision();
//					System.out.println("LA ALERTA DEL FILTER ES : " + alerta );
				respuesta = new RespuestaDTO(true, "['']", parametros);

		} catch (Exception e) {
			if (log.isHabilitadoError()) {
				log.error(e);
			}
			parametros.put("chekstatenv", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
		   	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
//			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		
           	respEncrypt = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncrypt)).toString()).build();
		
//		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
    	
    }
    
    
    private String desencriptar(String jsonEncrypted){
    	param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
		salt = QueryParamURL.getParam(jsonEncrypted, "salt");
		iv = QueryParamURL.getParam(jsonEncrypted, "iv");		
		String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
    	return objJsonString;
    }
    @SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			Context c = new InitialContext();
			return (T) c.lookup(
					"java:global/pe.gob.minedu.escale.adm.ws-1.0.0-PRO/" + nombreSimple.replace("Local", "")
							+ "!pe.gob.minedu.escale.adm.ejb.service." + nombreSimple);
		} catch (NamingException ne) {
			if (log.isHabilitadoError()) {
				log.error(ne);
			}
			throw new RuntimeException(ne);
		}
	}
}