<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.security.SecureRandom"  %>
<%@ page import="java.security.NoSuchAlgorithmException"  %>
<%@ page import="java.net.MalformedURLException" %>
<%@ page import="java.net.URL" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>



<%! 
SecureRandom prng = null;
String randomNumext = "";
%>

<%
//String randomNumext = "";
String dominio="";
try{
	
	 URL aURL = new URL(request.getRequestURL().toString());
	 
	 dominio=aURL.getProtocol() +"://" + aURL.getAuthority();
	 
	//SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
	prng = SecureRandom.getInstance("SHA1PRNG");
	randomNumext = Integer.toHexString(new Integer(prng.nextInt()))+ Integer.toHexString(new Integer(prng.nextInt())).substring(0, 2);
	
}catch (NoSuchAlgorithmException e) {
    e.printStackTrace();
}
//	"unsafe-eval "  +  dominio + " ;"  +

String csp =
		"script-src  'strict-dynamic' 'nonce-"+ randomNumext +"' 'unsafe-inline' https: http: ; " +
		//"script-src  'strict-dynamic' 'nonce-"+ randomNumext +"' 'unsafe-inline' https: http:  'unsafe-eval' " + dominio + " ; " +
		"media-src 'self' blob:; " +
		"object-src 'none'  blob:; " +
		"base-uri 'none' ;" +
		"frame-src https://www.google.com ; " + 
		"child-src 'self'; " + 
		"report-uri "  +  dominio + " ;"  +
		//"unsafe-eval "  +  dominio + " ;"  +
		"img-src  https://www.google-analytics.com http://sigmed.minedu.gob.pe http://ecn.t0.tiles.virtualearth.net http://ecn.t1.tiles.virtualearth.net http://ecn.t2.tiles.virtualearth.net " + dominio + " http://ecn.t3.tiles.virtualearth.net ; " + 
		"style-src 'unsafe-inline' https://www.bing.com " + dominio + " http://sigmed.minedu.gob.pe ;" +
		"font-src " + dominio + " data: blob: http://sigmed.minedu.gob.pe ;" +
		//"connect-src http://10.36.136.152:8081/admws/rest/usuario/* http://10.36.136.152:8081/regprogramaws/* http://10.36.136.152:8081/regprogramaws/svt/inSoon http://10.36.136.152:8081/regprogramaws/svt/inMoon ;" + 
		"connect-src https://www.google-analytics.com " + dominio + " http://sigmed.minedu.gob.pe  http://escale.minedu.gob.pe; " ;
		//"connect-src 'self' ; ";
		
					
	String sessionid = request.getSession().getId();
	response.setHeader("X-Frame-Options", "DENY");
	response.setHeader("X-Content-Type-Options", "nosniff");
	response.setHeader("X-XSS-Protection", "1; mode=block");
	response.addHeader( "Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly ; SameSite=Strict ");
	response.addHeader( "Content-Security-Policy", csp );
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <title>Insert title here</title>  -->


<jsp:include page="index.xhtml"></jsp:include>
</head>
<body>
<%--  mi valor es : <%= randomNumext%>  --%>
</body>
</html>