package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "det_regpro_provedepa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProveedorDepartamento.findAll", query = "SELECT d FROM ProveedorDepartamento d")})
public class ProveedorDepartamento extends EntidadUtil implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected ProveedorDepartamentoPK proveedorDepartamentoPK;

    @Column(name = "D_FECCRE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Column(name = "C_USUCRE")
    private String usuarioCreacion;
    
    @Column(name = "D_FECUMO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaModificacion;
    
    @Column(name = "C_USUUMO")
    private String usuarioUltimaModificacion;
    
    @Column(name = "C_ESTADO")
    private String estado;
    
    @JoinColumn(name = "ID_REGION", referencedColumnName = "ID_REGION", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Regiones regiones;
    
    @JoinColumn(name = "N_ID_MAESTRO", referencedColumnName = "N_ID_MAESTRO", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Maestro maestro;

   

    public ProveedorDepartamentoPK getProveedorDepartamentoPK() {
		return proveedorDepartamentoPK;
	}

	public void setProveedorDepartamentoPK(ProveedorDepartamentoPK proveedorDepartamentoPK) {
		this.proveedorDepartamentoPK = proveedorDepartamentoPK;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Regiones getRegiones() {
		return regiones;
	}

	public void setRegiones(Regiones regiones) {
		this.regiones = regiones;
	}

	public Maestro getMaestro() {
		return maestro;
	}

	public void setMaestro(Maestro maestro) {
		this.maestro = maestro;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (proveedorDepartamentoPK != null ? proveedorDepartamentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ProveedorDepartamento)) {
            return false;
        }
        ProveedorDepartamento other = (ProveedorDepartamento) object;
        if ((this.proveedorDepartamentoPK == null && other.proveedorDepartamentoPK != null) || (this.proveedorDepartamentoPK != null && !this.proveedorDepartamentoPK.equals(other.proveedorDepartamentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.ProveedorDepartamento[ proveedorDepartamentoPK=" + proveedorDepartamentoPK + " ]";
    }
    
}
