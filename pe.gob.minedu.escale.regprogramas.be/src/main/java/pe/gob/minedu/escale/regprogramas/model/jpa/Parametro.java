package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_parametro")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Parametro.findAll", query = "SELECT p FROM Parametro p") })
public class Parametro extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_PARAMETRO")
	private Long idParametro;

	@Column(name = "C_CODPARAME")
	private String codigoParametro;

	@Column(name = "C_DESPARAM")
	private String descripcionParametro;

	@Column(name = "C_VALORPARAM")
	private String valorLetra;

	@Column(name = "N_VALORPARAM")
	private BigDecimal valorNumerico;

	@Column(name = "D_VALORPARAM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date valorFecha;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	public Parametro() {
	}

	public Parametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public Parametro(Long idParametro, String codigoParametro, String descripcionParametro, String valorLetra,
			BigDecimal valorNumerico, Date valorFecha, Date fechaCreacion, String usuarioCreacion,
			Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado) {
		super();
		this.idParametro = idParametro;
		this.codigoParametro = codigoParametro;
		this.descripcionParametro = descripcionParametro;
		this.valorLetra = valorLetra;
		this.valorNumerico = valorNumerico;
		this.valorFecha = valorFecha;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
	}

	public Parametro(Long idParametro, String codigoParametro, String descripcionParametro, String valorLetra,
			BigDecimal valorNumerico, Date valorFecha, String estado) {
		this.idParametro = idParametro;
		this.codigoParametro = codigoParametro;
		this.descripcionParametro = descripcionParametro;
		this.valorLetra = valorLetra;
		this.valorNumerico = valorNumerico;
		this.valorFecha = valorFecha;
		this.estado = estado;
	}

	public Long getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public String getCodigoParametro() {
		return codigoParametro;
	}

	public void setCodigoParametro(String codigoParametro) {
		this.codigoParametro = codigoParametro;
	}

	public String getDescripcionParametro() {
		return descripcionParametro;
	}

	public void setDescripcionParametro(String descripcionParametro) {
		this.descripcionParametro = descripcionParametro;
	}

	public String getValorLetra() {
		return valorLetra;
	}

	public void setValorLetra(String valorLetra) {
		this.valorLetra = valorLetra;
	}

	public BigDecimal getValorNumerico() {
		return valorNumerico;
	}

	public void setValorNumerico(BigDecimal valorNumerico) {
		this.valorNumerico = valorNumerico;
	}

	public Date getValorFecha() {
		return valorFecha;
	}

	public void setValorFecha(Date valorFecha) {
		this.valorFecha = valorFecha;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idParametro != null ? idParametro.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Parametro)) {
			return false;
		}
		Parametro other = (Parametro) object;
		if ((this.idParametro == null && other.idParametro != null)
				|| (this.idParametro != null && !this.idParametro.equals(other.idParametro))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Paramnetro[ idParametro=" + idParametro + " ]";
	}

}
