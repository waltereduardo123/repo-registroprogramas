package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Embeddable
public class SolicitudDocumentoPK extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;
	

    @Column(name = "N_ID_DOCUMENTO")
    private long idDocumento;

    @Column(name = "N_ID_SOLICITUD")
    private long idSolicitud;

    public SolicitudDocumentoPK() {
    }

    public SolicitudDocumentoPK(long idDocumento, long idSolicitud) {
        this.idDocumento = idDocumento;
        this.idSolicitud = idSolicitud;
    }

    public long getidDocumento() {
        return idDocumento;
    }

    public void setidDocumento(long idDocumento) {
        this.idDocumento = idDocumento;
    }

    public long getidSolicitud() {
        return idSolicitud;
    }

    public void setidSolicitud(long idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idDocumento;
        hash += (int) idSolicitud;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SolicitudDocumentoPK)) {
            return false;
        }
        SolicitudDocumentoPK other = (SolicitudDocumentoPK) object;
        if (this.idDocumento != other.idDocumento) {
            return false;
        }
        if (this.idSolicitud != other.idSolicitud) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.SolicitudDocumentoPK[ idDocumento=" + idDocumento + ", idSolicitud=" + idSolicitud + " ]";
    }
    
}
