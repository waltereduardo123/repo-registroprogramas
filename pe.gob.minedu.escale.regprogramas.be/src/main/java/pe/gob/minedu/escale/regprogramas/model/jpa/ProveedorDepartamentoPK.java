package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Embeddable
public class ProveedorDepartamentoPK extends EntidadUtil implements Serializable {
    
	private static final long serialVersionUID = 1L;
	   
    @Column(name = "ID_REGION")
    private String idRegion;
 
    @Column(name = "N_ID_MAESTRO")
    private long idMaestro;

    public ProveedorDepartamentoPK() {
		
	}

	public ProveedorDepartamentoPK(String idRegion, long idMaestro) {
		super();
		this.idRegion = idRegion;
		this.idMaestro = idMaestro;
	}

	public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public long getIdMaestro() {
		return idMaestro;
	}

	public void setIdMaestro(long idMaestro) {
		this.idMaestro = idMaestro;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegion != null ? idRegion.hashCode() : 0);
        hash += (int) idMaestro;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ProveedorDepartamentoPK)) {
            return false;
        }
        ProveedorDepartamentoPK other = (ProveedorDepartamentoPK) object;
        if ((this.idRegion == null && other.idRegion != null) || (this.idRegion != null && !this.idRegion.equals(other.idRegion))) {
            return false;
        }
        if (this.idMaestro != other.idMaestro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.ProveedorDepartamentoPK[ idRegion=" + idRegion + ", idMaestro=" + idMaestro + " ]";
    }
    
}
