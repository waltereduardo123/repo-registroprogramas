package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 * Entity implementation class for Entity: Corte
 *
 */
@Entity
@Table(name="tbl_regpro_corte")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Corte.findAll", query = "SELECT c FROM Corte c") })
@DynamicUpdate(value = true)
public class Corte extends EntidadUtil implements Serializable {
	
private static final long serialVersionUID = 1L;
	


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)    
	@Column(name = "N_ID_CORTE")
	private Integer idCorte;

	@Column(name = "C_NOMPERIODO")
	private String namePeriodo;

	@Column(name = "D_FEC_INIPERIODO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicioPeriodo;
	
	@Column(name = "D_FEC_FINPERIODO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFinPeriodo;
	
	@Column(name = "C_USUCREA")
	private String userCrea;
	
	@Column(name = "D_FECCREA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecCrea;
	
	
	@Column(name = "C_ESTADO")
	private String estado;

	
	
	public Corte() {
		super();
	}




	public Corte(Integer idCorte, String namePeriodo, Date fechaInicioPeriodo, Date fechaFinPeriodo, String userCrea,
			Date fecCrea, String estado) {
		super();
		this.idCorte = idCorte;
		this.namePeriodo = namePeriodo;
		this.fechaInicioPeriodo = fechaInicioPeriodo;
		this.fechaFinPeriodo = fechaFinPeriodo;
		this.userCrea = userCrea;
		this.fecCrea = fecCrea;
		this.estado = estado;
	}




	public Integer getIdCorte() 											{		return idCorte;}
	public void setIdCorte(Integer idCorte) 								{		this.idCorte = idCorte;}
	public String getNamePeriodo() 											{		return namePeriodo;}
	public void setNamePeriodo(String namePeriodo) 							{		this.namePeriodo = namePeriodo;}
	public Date getFechaInicioPeriodo() 									{		return fechaInicioPeriodo;}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) 				{		this.fechaInicioPeriodo = fechaInicioPeriodo;}
	public Date getFechaFinPeriodo() 										{		return fechaFinPeriodo;}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) 					{		this.fechaFinPeriodo = fechaFinPeriodo;}
	public String getUserCrea() 											{		return userCrea;}
	public void setUserCrea(String userCrea) 								{		this.userCrea = userCrea;}
	public Date getFecCrea() 												{		return fecCrea;}
	public void setFecCrea(Date fecCrea) 									{		this.fecCrea = fecCrea;}
	public String getEstado() 												{		return estado;}
	public void setEstado(String estado) 									{		this.estado = estado;}
	
	
   
}



