package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "det_regpro_solcam")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RevisionSolicitudCampos.findAll", query = "SELECT r FROM RevisionSolicitudCampos r") })
@DynamicUpdate(value = true)
public class RevisionSolicitudCampos extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_SOLCAM")
	private Long idSolcam;

	@Column(name = "C_ESTEVAL")
	private String estadoEvaluacion;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_PERFREV")
	private String perfilRevisor;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "N_ID_SOLICITUD", referencedColumnName = "N_ID_SOLICITUD")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Solicitud solicitud;

	@JoinColumn(name = "N_ID_CAMPOS", referencedColumnName = "N_ID_CAMPOS")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Campos campos;

	public RevisionSolicitudCampos() {
	}

	public RevisionSolicitudCampos(Long nIdSolcam) {
		this.idSolcam = nIdSolcam;
	}

	public RevisionSolicitudCampos(Long idSolcam, Date dFeccre, String cUsucre, String cEstado) {
		this.idSolcam = idSolcam;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
	}

	public RevisionSolicitudCampos(Long idSolcam, String estadoEvaluacion, Long idSolicitud, Long idCampos,
			String usuarioCreacion, String estado) {
		this.idSolcam = idSolcam;
		this.estadoEvaluacion = estadoEvaluacion;
		this.solicitud = Objects.isNull(solicitud) ? new Solicitud(idSolicitud) : solicitud;
		this.campos = Objects.isNull(campos) ? new Campos(idCampos) : campos;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public RevisionSolicitudCampos(Long idSolcam, String estadoEvaluacion, Long idSolicitud, Long idCampos,
			String usuarioCreacion, String estado, String perfilRevisor) {
		this.idSolcam = idSolcam;
		this.estadoEvaluacion = estadoEvaluacion;
		this.solicitud = Objects.isNull(solicitud) ? new Solicitud(idSolicitud) : solicitud;
		this.campos = Objects.isNull(campos) ? new Campos(idCampos) : campos;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		this.perfilRevisor = perfilRevisor;
	}

	public Long getIdSolcam() {
		return idSolcam;
	}

	public void setIdSolcam(Long idSolcam) {
		this.idSolcam = idSolcam;
	}

	public String getEstadoEvaluacion() {
		return estadoEvaluacion;
	}

	public void setEstadoEvaluacion(String estadoEvaluacion) {
		this.estadoEvaluacion = estadoEvaluacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public Campos getCampos() {
		return campos;
	}

	public void setCampos(Campos campos) {
		this.campos = campos;
	}

	public String getPerfilRevisor() {
		return perfilRevisor;
	}

	public void setPerfilRevisor(String perfilRevisor) {
		this.perfilRevisor = perfilRevisor;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idSolcam != null ? idSolcam.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof RevisionSolicitudCampos)) {
			return false;
		}
		RevisionSolicitudCampos other = (RevisionSolicitudCampos) object;
		if ((this.idSolcam == null && other.idSolcam != null)
				|| (this.idSolcam != null && !this.idSolcam.equals(other.idSolcam))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.RevisionSolicitudCampos[ idSolcam=" + idSolcam + " ]";
	}

}
