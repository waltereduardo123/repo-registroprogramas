package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_campos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Campos.findAll", query = "SELECT c FROM Campos c")})
public class Campos extends EntidadUtil implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column(name = "N_ID_CAMPOS")
    private Long idCampos;
    
    @Column(name = "C_NOMCAMP")
    private String nombreCampo;
    
    @Column(name = "C_CODCAMP")
    private String codigoCampo;
    
    @Column(name = "B_OBLIGATORIO")
    private Boolean obligatorio;
    
    @Column(name = "D_FECCRE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Column(name = "C_USUCRE")
    private String usuarioCreacion;
    
    @Column(name = "D_FECUMO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaModificacion;
    
    @Column(name = "C_USUUMO")
    private String usuarioUltimaModificacion;
  
    @Column(name = "C_ESTADO")
    private String estado;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campos", fetch = FetchType.LAZY)
    private List<RevisionSolicitudCampos> listaRevisionSolicitudCampos;

    public Campos() {
    }

    public Campos(Long idCampos) {
        this.idCampos = idCampos;
    }

    public Campos(Long idCampos, Date dFeccre, String cUsucre, String cEstado) {
        this.idCampos = idCampos;
        this.fechaCreacion = dFeccre;
        this.usuarioCreacion = cUsucre;
        this.estado = cEstado;
    }
    

    public Campos(Long idCampos, String nombreCampo, String codigoCampo, Boolean obligatorio, String estado) {
		this.idCampos = idCampos;
		this.nombreCampo = nombreCampo;
		this.codigoCampo = codigoCampo;
		this.obligatorio = obligatorio;
		this.estado = estado;
	}

	public Long getIdCampos() {
		return idCampos;
	}

	public void setIdCampos(Long idCampos) {
		this.idCampos = idCampos;
	}

	public String getNombreCampo() {
		return nombreCampo;
	}

	public void setNombreCampo(String nombreCampo) {
		this.nombreCampo = nombreCampo;
	}

	public String getCodigoCampo() {
		return codigoCampo;
	}

	public void setCodigoCampo(String codigoCampo) {
		this.codigoCampo = codigoCampo;
	}

	public Boolean getObligatorio() {
		return obligatorio;
	}

	public void setObligatorio(Boolean obligatorio) {
		this.obligatorio = obligatorio;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

	@XmlTransient
	public List<RevisionSolicitudCampos> getListaRevisionSolicitudCampos() {
		return listaRevisionSolicitudCampos;
	}

	public void setListaRevisionSolicitudCampos(List<RevisionSolicitudCampos> listaRevisionSolicitudCampos) {
		this.listaRevisionSolicitudCampos = listaRevisionSolicitudCampos;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idCampos != null ? idCampos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Campos)) {
            return false;
        }
        Campos other = (Campos) object;
        if ((this.idCampos == null && other.idCampos != null) || (this.idCampos != null && !this.idCampos.equals(other.idCampos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.Campos[ idCampos=" + idCampos + " ]";
    }
    
}
