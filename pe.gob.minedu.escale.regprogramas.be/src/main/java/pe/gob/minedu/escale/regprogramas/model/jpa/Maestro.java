package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_maestro")
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "Maestro.findAll", query = "SELECT m FROM Maestro m")})
public class Maestro extends EntidadUtil implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Column(name = "N_ID_MAESTRO")
    private Long idMaestro;
    
    @Column(name = "C_CODGRUP")
    private String codigoAgrupacion;
    
    @Column(name = "C_NOM")
    private String nombreMaestro;

    @Column(name = "C_DESC")
    private String descripcionMaestro;
    
    @Column(name = "C_CODITEM")
    private String codigoItem;

    @Column(name = "D_FECCRE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "C_USUCRE")
    private String usuarioCreacion;
    
    @Column(name = "D_FECUMO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaModificacion;
    
    @Column(name = "C_USUUMO")
    private String usuarioUltimaModificacion;

    @Column(name = "C_ESTADO")
    private String estado;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "maestro", fetch = FetchType.LAZY)
    private List<ProveedorDepartamento> proveedorDepartamento;

    @OneToMany(mappedBy = "maestroPadre", fetch = FetchType.LAZY)
    private List<Maestro> listaMaestro;
    
    @JoinColumn(name = "N_ID_MAESTROPADRE", referencedColumnName = "N_ID_MAESTRO")
    @ManyToOne(fetch = FetchType.EAGER)
    private Maestro maestroPadre;

    public Maestro() {
    }

    public Maestro(Long idMaestro) {
        this.idMaestro = idMaestro;
    }

    

    public Maestro(Long idMaestro, String codigoAgrupacion, String nombreMaestro, String descripcionMaestro,
			String codigoItem, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado, List<ProveedorDepartamento> proveedorDepartamento,
			List<Maestro> listaMaestro, Maestro maestroPadre) {
		super();
		this.idMaestro = idMaestro;
		this.codigoAgrupacion = codigoAgrupacion;
		this.nombreMaestro = nombreMaestro;
		this.descripcionMaestro = descripcionMaestro;
		this.codigoItem = codigoItem;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.proveedorDepartamento = proveedorDepartamento;
		this.listaMaestro = listaMaestro;
		this.maestroPadre = maestroPadre;
	}
    
    public Maestro(Long idMaestro, String codigoAgrupacion, String nombreMaestro, String descripcionMaestro,
			String codigoItem) {
		this.idMaestro = idMaestro;
		this.codigoAgrupacion = codigoAgrupacion;
		this.nombreMaestro = nombreMaestro;
		this.descripcionMaestro = descripcionMaestro;
		this.codigoItem = codigoItem;
	}

	public Long getIdMaestro() {
		return idMaestro;
	}

	public void setIdMaestro(Long idMaestro) {
		this.idMaestro = idMaestro;
	}

	public String getCodigoAgrupacion() {
		return codigoAgrupacion;
	}

	public void setCodigoAgrupacion(String codigoAgrupacion) {
		this.codigoAgrupacion = codigoAgrupacion;
	}

	public String getNombreMaestro() {
		return nombreMaestro;
	}

	public void setNombreMaestro(String nombreMaestro) {
		this.nombreMaestro = nombreMaestro;
	}

	public String getDescripcionMaestro() {
		return descripcionMaestro;
	}

	public void setDescripcionMaestro(String descripcionMaestro) {
		this.descripcionMaestro = descripcionMaestro;
	}

	public String getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@XmlTransient
	public List<ProveedorDepartamento> getProveedorDepartamento() {
		return proveedorDepartamento;
	}

	public void setProveedorDepartamento(List<ProveedorDepartamento> proveedorDepartamento) {
		this.proveedorDepartamento = proveedorDepartamento;
	}

	@XmlTransient
	public List<Maestro> getListaMaestro() {
		return listaMaestro;
	}

	public void setListaMaestro(List<Maestro> listaMaestro) {
		this.listaMaestro = listaMaestro;
	}

	public Maestro getMaestroPadre() {
		return maestroPadre;
	}

	public void setMaestroPadre(Maestro maestroPadre) {
		this.maestroPadre = maestroPadre;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idMaestro != null ? idMaestro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Maestro)) {
            return false;
        }
        Maestro other = (Maestro) object;
        if ((this.idMaestro == null && other.idMaestro != null) || (this.idMaestro != null && !this.idMaestro.equals(other.idMaestro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.Maestro[ idMaestro=" + idMaestro + " ]";
    }
    
}
