package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_coord")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "CoordinadorDreUgel.findAll", query = "SELECT c FROM CoordinadorDreUgel c") })
public class CoordinadorDreUgel extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_COORD")
	private Long idCoord;

	@Column(name = "C_USUCOORD")
	private String usuarioCoordinador;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO")
	@ManyToOne(fetch = FetchType.LAZY)
	private DreUgel dreUgel;

	public CoordinadorDreUgel() {
	}

	public CoordinadorDreUgel(Long idCoord) {
		this.idCoord = idCoord;
	}	

	public CoordinadorDreUgel(Long idCoord, String usuarioCoordinador, Date fechaCreacion, String usuarioCreacion,
			Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado, DreUgel dreUgel) {
		super();
		this.idCoord = idCoord;
		this.usuarioCoordinador = usuarioCoordinador;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.dreUgel = dreUgel;
	}

	public Long getIdCoord() {
		return idCoord;
	}

	public void setIdCoord(Long idCoord) {
		this.idCoord = idCoord;
	}

	public String getUsuarioCoordinador() {
		return usuarioCoordinador;
	}

	public void setUsuarioCoordinador(String usuarioCoordinador) {
		this.usuarioCoordinador = usuarioCoordinador;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public DreUgel getDreUgel() {
		return dreUgel;
	}

	public void setDreUgel(DreUgel dreUgel) {
		this.dreUgel = dreUgel;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idCoord != null ? idCoord.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof CoordinadorDreUgel)) {
			return false;
		}
		CoordinadorDreUgel other = (CoordinadorDreUgel) object;
		if ((this.idCoord == null && other.idCoord != null)
				|| (this.idCoord != null && !this.idCoord.equals(other.idCoord))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.CoordinadorDreUgel[ idCoord=" + idCoord + " ]";
	}

}
