package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;
import pe.gob.minedu.escale.common.util.StringUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_programa")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Programa.findAll", query = "SELECT p FROM Programa p") })
@DynamicUpdate(value = true)
public class Programa extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_PROGRAMA")
	private Long idPrograma;

	@Column(name = "C_CODMOD")
	private String codigoModular;

	@JoinColumn(name = "N_ID_TIPPROG")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoPrograma;

	@Column(name = "C_NOMPROG")
	private String nombrePrograma;

	@Column(name = "N_PROLAT")
	private BigDecimal latitudPrograma;

	@Column(name = "N_PROLON")
	private BigDecimal longitudPrograma;

	@JoinColumn(name = "N_ID_TIPGEST")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoGestion;

	@JoinColumn(name = "N_ID_TIPDEPE")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoDependencia;

	@JoinColumn(name = "N_ID_TIPEGES")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoGestionEducativa;

	@JoinColumn(name = "CODOOII", referencedColumnName = "CODIGO")
	@ManyToOne(fetch = FetchType.EAGER)
	private DreUgel dreUgel;

	@JoinColumn(name = "N_ID_TIPTURN")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoTurno;

	@JoinColumn(name = "N_ID_TIPCJES")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoContinuidadJornadaEscolar;

	@JoinColumn(name = "N_ID_TIPVIA")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoVia;

	@Column(name = "C_DIRNOMVIA")
	private String nombreVia;

	@Column(name = "C_DIRNUMVIA")
	private String numeroVia;

	@Column(name = "C_DIRMZ")
	private String manzana;

	@Column(name = "C_DIRLOTE")
	private String lote;

	@JoinColumn(name = "N_ID_TIPLOCD")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoLocalidad;

	@Column(name = "C_DIRLOCD")
	private String localidad;

	@Column(name = "C_DIRETAP")
	private String etapa;

	@Column(name = "C_DIRSECT")
	private String sector;

	@Column(name = "C_DIRZONA")
	private String zona;

	@Column(name = "C_DIROTRO")
	private String otraDireccion;

	@Column(name = "C_DIRREFE")
	private String referenciaDireccion;

	@JoinColumn(name = "CODGEO", referencedColumnName = "ID_DISTRITO")
	@ManyToOne(fetch = FetchType.EAGER)
	private Distritos distrito;

	@Column(name = "C_CODCCPP")
	private String codigoCentroPoblado;

	@Column(name = "C_NOMCCPP")
	private String nombreCentroPoblado;

	@Column(name = "N_CCPPLAT")
	private BigDecimal latitudCentroPoblado;

	@Column(name = "N_CCPPLON")
	private BigDecimal longitudCentroPoblado;

	@Column(name = "C_CSEMC")
	private String codigoServicioEduMasCercano;

	@Column(name = "C_NOMSEMC")
	private String nombreServicioEduMasCercano;

	@Column(name = "N_SEMCLAT")
	private BigDecimal latitudServicioEduMasCercano;

	@Column(name = "N_SEMCLON")
	private BigDecimal longitudServicioEduMasCercano;

	@JoinColumn(name = "N_ID_TIPPRAG")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoProveedorAgua;

	@Column(name = "C_OTRPRAGUA")
	private String otroProveedorAgua;

	@Column(name = "C_SUMAGUA")
	private String suministroAgua;

	@JoinColumn(name = "N_ID_TIPPREN")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoProveedorEnergia;

	@Column(name = "C_OTRPRENER")
	private String otroProveedorEnergia;

	@Column(name = "C_SUMENER")
	private String suministroEnergia;

	@Column(name = "COD_AREA")
	private String area;

	@Column(name = "C_COD_AREASIG")
	private String codigoAreaSig;

	@Column(name = "NZOOM")
	private Long zoom;

	@Column(name = "MCENSO")
	private String marcoCensal;

	@Column(name = "MARCOCD")
	private String marcocd;

	@JoinColumn(name = "N_ID_TIPSITU")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSituacion;

	@Column(name = "D_FECREPROG")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacionPrograma;

	@Column(name = "D_FECIEPROG")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCierrePrograma;

	@Column(name = "D_FERENPROG")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRenovacionPrograma;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "N_ID_TIPESTADO")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoEstado;  

	@Column(name = "C_GEOHASH")
	private String geoHash;

	@JoinColumn(name = "N_ID_TIPRESO")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoResolucion;

	@Column(name = "C_NRODOC")
	private String numeroDocumento;

	@Column(name = "D_FECDOCU")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaDocumento;

	@Column(name = "C_CODDOCU")
	private String codigoDocumento;

	public Programa() {
	}

	public Programa(Long idPrograma) {
		this.idPrograma = idPrograma;
	}

	public Programa(Long idPrograma, String codigoModular, Maestro tipoPrograma, String nombrePrograma,
			BigDecimal latitudPrograma, BigDecimal longitudPrograma, Maestro tipoGestion, Maestro tipoDependencia,
			Maestro tipoGestionEducativa, DreUgel dreUgel, Maestro tipoTurno, Maestro tipoContinuidadJornadaEscolar,
			Maestro tipoVia, String nombreVia, String numeroVia, String manzana, String lote, Maestro tipoLocalidad,
			String localidad, String etapa, String sector, String zona, String otraDireccion,
			String referenciaDireccion, Distritos distrito, String codigoCentroPoblado, String nombreCentroPoblado,
			BigDecimal latitudCentroPoblado, BigDecimal longitudCentroPoblado, String codigoServicioEduMasCercano,
			String nombreServicioEduMasCercano, BigDecimal latitudServicioEduMasCercano,
			BigDecimal longitudServicioEduMasCercano, Maestro tipoProveedorAgua, String otroProveedorAgua,
			String suministroAgua, Maestro tipoProveedorEnergia, String otroProveedorEnergia, String suministroEnergia,
			String area, String codigoAreaSig, Long zoom, String marcoCensal, String marcocd, Maestro tipoSituacion,
			Date fechaCreacionPrograma, Date fechaCierrePrograma, Date fechaRenovacionPrograma, Date fechaCreacion,
			String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado,
			Maestro tipoEstado, String geoHash, Maestro tipoResolucion, String numeroDocumento, Date fechaDocumento,
			String codigoDocumento) {
		super();
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.latitudPrograma = latitudPrograma;
		this.longitudPrograma = longitudPrograma;
		this.tipoGestion = tipoGestion;
		this.tipoDependencia = tipoDependencia;
		this.tipoGestionEducativa = tipoGestionEducativa;
		this.dreUgel = dreUgel;
		this.tipoTurno = tipoTurno;
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
		this.tipoVia = tipoVia;
		this.nombreVia = nombreVia;
		this.numeroVia = numeroVia;
		this.manzana = manzana;
		this.lote = lote;
		this.tipoLocalidad = tipoLocalidad;
		this.localidad = localidad;
		this.etapa = etapa;
		this.sector = sector;
		this.zona = zona;
		this.otraDireccion = otraDireccion;
		this.referenciaDireccion = referenciaDireccion;
		this.distrito = distrito;
		this.codigoCentroPoblado = codigoCentroPoblado;
		this.nombreCentroPoblado = nombreCentroPoblado;
		this.latitudCentroPoblado = latitudCentroPoblado;
		this.longitudCentroPoblado = longitudCentroPoblado;
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
		this.tipoProveedorAgua = tipoProveedorAgua;
		this.otroProveedorAgua = otroProveedorAgua;
		this.suministroAgua = suministroAgua;
		this.tipoProveedorEnergia = tipoProveedorEnergia;
		this.otroProveedorEnergia = otroProveedorEnergia;
		this.suministroEnergia = suministroEnergia;
		this.area = area;
		this.codigoAreaSig = codigoAreaSig;
		this.zoom = zoom;
		this.marcoCensal = marcoCensal;
		this.marcocd = marcocd;
		this.tipoSituacion = tipoSituacion;
		this.fechaCreacionPrograma = fechaCreacionPrograma;
		this.fechaCierrePrograma = fechaCierrePrograma;
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.tipoEstado = tipoEstado;
		this.geoHash = geoHash;
		this.tipoResolucion = tipoResolucion;
		this.numeroDocumento = numeroDocumento;
		this.fechaDocumento = fechaDocumento;
		this.codigoDocumento = codigoDocumento;
	}

	public Programa(Long idPrograma, String codigoModular, Maestro tipoPrograma, String nombrePrograma,
			BigDecimal latitudPrograma, BigDecimal longitudPrograma, Maestro tipoGestion, Maestro tipoDependencia,
			Maestro tipoGestionEducativa, Maestro tipoTurno, Maestro tipoContinuidadJornadaEscolar, String marcoCensal,
			Date fechaCreacion, String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.latitudPrograma = latitudPrograma;
		this.longitudPrograma = longitudPrograma;
		this.tipoGestion = tipoGestion;
		this.tipoDependencia = tipoDependencia;
		this.tipoGestionEducativa = tipoGestionEducativa;
		this.tipoTurno = tipoTurno;
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
		this.marcoCensal = marcoCensal;
		this.fechaCreacion = fechaCreacion;
		this.estado = estado;
	}

	public Programa(Long idPrograma, String codigoModular, String nombrePrograma, String usuarioCreacion,
			String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public Programa(Long idPrograma, String codigoModular, String nombrePrograma, Date fechaCreacionPrograma,
			Date fechaCierrePrograma, Date fechaRenovacionPrograma, String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.fechaCreacionPrograma = fechaCreacionPrograma;
		this.fechaCierrePrograma = fechaCierrePrograma;
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
		this.estado = estado;
	}

	public Programa(Long idPrograma, String codigoModular, String nombrePrograma, Long idTipoSituacion,
			String tipoSituacion, String codooii, Date fechaCreacionPrograma, Date fechaCierrePrograma,
			Date fechaRenovacionPrograma, String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.tipoSituacion = claseNullAContruir(Maestro.class, this.tipoSituacion, idTipoSituacion);
		getTipoSituacion().setDescripcionMaestro(StringUtil.isNotNullOrBlank(tipoSituacion) ? tipoSituacion : "");
		getDreUgel().setCodigo(StringUtil.isNotNullOrBlank(codooii) ? codooii : "");
		this.fechaCreacionPrograma = fechaCreacionPrograma;
		this.fechaCierrePrograma = fechaCierrePrograma;
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
		this.estado = estado;
	}

	// Se agrega constructor, por item del maestro werr  08112017
	public Programa(Long idPrograma, String codigoModular, String nombrePrograma, Long idTipoSituacion,
			String tipoSituacion, String tipoSituacionItem, String codooii, Date fechaCreacionPrograma, Date fechaCierrePrograma,
			Date fechaRenovacionPrograma, String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.tipoSituacion = claseNullAContruir(Maestro.class, this.tipoSituacion, idTipoSituacion);
		getTipoSituacion().setDescripcionMaestro(StringUtil.isNotNullOrBlank(tipoSituacion) ? tipoSituacion : "");
		getTipoSituacion().setCodigoItem(StringUtil.isNotNullOrBlank(tipoSituacionItem) ? tipoSituacionItem : "");
		getDreUgel().setCodigo(StringUtil.isNotNullOrBlank(codooii) ? codooii : "");
		this.fechaCreacionPrograma = fechaCreacionPrograma;
		this.fechaCierrePrograma = fechaCierrePrograma;
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
		this.estado = estado;
	}
	

	
	public Long getIdPrograma() {
		return idPrograma;
	}

	public void setIdPrograma(Long idPrograma) {
		this.idPrograma = idPrograma;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public BigDecimal getLatitudPrograma() {
		return latitudPrograma;
	}

	public void setLatitudPrograma(BigDecimal latitudPrograma) {
		this.latitudPrograma = latitudPrograma;
	}

	public BigDecimal getLongitudPrograma() {
		return longitudPrograma;
	}

	public void setLongitudPrograma(BigDecimal longitudPrograma) {
		this.longitudPrograma = longitudPrograma;
	}

	public DreUgel getDreUgel() {
		if (dreUgel == null) {
			dreUgel = new DreUgel();
		}
		return dreUgel;
	}

	public void setDreUgel(DreUgel dreUgel) {
		this.dreUgel = dreUgel;
	}

	public String getNombreVia() {
		return nombreVia;
	}

	public void setNombreVia(String nombreVia) {
		this.nombreVia = nombreVia;
	}

	public String getNumeroVia() {
		return numeroVia;
	}

	public void setNumeroVia(String numeroVia) {
		this.numeroVia = numeroVia;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getOtraDireccion() {
		return otraDireccion;
	}

	public void setOtraDireccion(String otraDireccion) {
		this.otraDireccion = otraDireccion;
	}

	public String getReferenciaDireccion() {
		return referenciaDireccion;
	}

	public void setReferenciaDireccion(String referenciaDireccion) {
		this.referenciaDireccion = referenciaDireccion;
	}

	public Distritos getDistrito() {
		return distrito;
	}

	public void setDistrito(Distritos distrito) {
		this.distrito = distrito;
	}

	public String getCodigoCentroPoblado() {
		return codigoCentroPoblado;
	}

	public void setCodigoCentroPoblado(String codigoCentroPoblado) {
		this.codigoCentroPoblado = codigoCentroPoblado;
	}

	public String getNombreCentroPoblado() {
		return nombreCentroPoblado;
	}

	public void setNombreCentroPoblado(String nombreCentroPoblado) {
		this.nombreCentroPoblado = nombreCentroPoblado;
	}

	public String getCodigoServicioEduMasCercano() {
		return codigoServicioEduMasCercano;
	}

	public void setCodigoServicioEduMasCercano(String codigoServicioEduMasCercano) {
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
	}

	public String getNombreServicioEduMasCercano() {
		return nombreServicioEduMasCercano;
	}

	public void setNombreServicioEduMasCercano(String nombreServicioEduMasCercano) {
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
	}

	public String getOtroProveedorAgua() {
		return otroProveedorAgua;
	}

	public void setOtroProveedorAgua(String otroProveedorAgua) {
		this.otroProveedorAgua = otroProveedorAgua;
	}

	public String getSuministroAgua() {
		return suministroAgua;
	}

	public void setSuministroAgua(String suministroAgua) {
		this.suministroAgua = suministroAgua;
	}

	public String getOtroProveedorEnergia() {
		return otroProveedorEnergia;
	}

	public void setOtroProveedorEnergia(String otroProveedorEnergia) {
		this.otroProveedorEnergia = otroProveedorEnergia;
	}

	public String getSuministroEnergia() {
		return suministroEnergia;
	}

	public void setSuministroEnergia(String suministroEnergia) {
		this.suministroEnergia = suministroEnergia;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCodigoAreaSig() {
		return codigoAreaSig;
	}

	public void setCodigoAreaSig(String codigoAreaSig) {
		this.codigoAreaSig = codigoAreaSig;
	}

	public Long getZoom() {
		return zoom;
	}

	public void setZoom(Long zoom) {
		this.zoom = zoom;
	}

	public String getMarcoCensal() {
		return marcoCensal;
	}

	public void setMarcoCensal(String marcoCensal) {
		this.marcoCensal = marcoCensal;
	}

	public String getMarcocd() {
		return marcocd;
	}

	public void setMarcocd(String marcocd) {
		this.marcocd = marcocd;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public Maestro getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(Maestro tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public Maestro getTipoGestion() {
		return tipoGestion;
	}

	public void setTipoGestion(Maestro tipoGestion) {
		this.tipoGestion = tipoGestion;
	}

	public Maestro getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(Maestro tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}

	public Maestro getTipoGestionEducativa() {
		return tipoGestionEducativa;
	}

	public void setTipoGestionEducativa(Maestro tipoGestionEducativa) {
		this.tipoGestionEducativa = tipoGestionEducativa;
	}

	public Maestro getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(Maestro tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	public Maestro getTipoContinuidadJornadaEscolar() {
		return tipoContinuidadJornadaEscolar;
	}

	public void setTipoContinuidadJornadaEscolar(Maestro tipoContinuidadJornadaEscolar) {
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
	}

	public Maestro getTipoVia() {
		return tipoVia;
	}

	public void setTipoVia(Maestro tipoVia) {
		this.tipoVia = tipoVia;
	}

	public Maestro getTipoLocalidad() {
		return tipoLocalidad;
	}

	public void setTipoLocalidad(Maestro tipoLocalidad) {
		this.tipoLocalidad = tipoLocalidad;
	}

	public Maestro getTipoProveedorAgua() {
		return tipoProveedorAgua;
	}

	public void setTipoProveedorAgua(Maestro tipoProveedorAgua) {
		this.tipoProveedorAgua = tipoProveedorAgua;
	}

	public Maestro getTipoProveedorEnergia() {
		return tipoProveedorEnergia;
	}

	public void setTipoProveedorEnergia(Maestro tipoProveedorEnergia) {
		this.tipoProveedorEnergia = tipoProveedorEnergia;
	}

	public Maestro getTipoSituacion() {
		if (tipoSituacion == null) {
			tipoSituacion = new Maestro(0L);
		}
		return tipoSituacion;
	}

	public void setTipoSituacion(Maestro tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public Date getFechaCreacionPrograma() {
		return fechaCreacionPrograma;
	}

	public void setFechaCreacionPrograma(Date fechaCreacionPrograma) {
		this.fechaCreacionPrograma = fechaCreacionPrograma;
	}

	public Date getFechaCierrePrograma() {
		return fechaCierrePrograma;
	}

	public void setFechaCierrePrograma(Date fechaCierrePrograma) {
		this.fechaCierrePrograma = fechaCierrePrograma;
	}

	public Date getFechaRenovacionPrograma() {
		return fechaRenovacionPrograma;
	}

	public void setFechaRenovacionPrograma(Date fechaRenovacionPrograma) {
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
	}

	public BigDecimal getLatitudCentroPoblado() {
		return latitudCentroPoblado;
	}

	public void setLatitudCentroPoblado(BigDecimal latitudCentroPoblado) {
		this.latitudCentroPoblado = latitudCentroPoblado;
	}

	public BigDecimal getLongitudCentroPoblado() {
		return longitudCentroPoblado;
	}

	public void setLongitudCentroPoblado(BigDecimal longitudCentroPoblado) {
		this.longitudCentroPoblado = longitudCentroPoblado;
	}

	public BigDecimal getLatitudServicioEduMasCercano() {
		return latitudServicioEduMasCercano;
	}

	public void setLatitudServicioEduMasCercano(BigDecimal latitudServicioEduMasCercano) {
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
	}

	public BigDecimal getLongitudServicioEduMasCercano() {
		return longitudServicioEduMasCercano;
	}

	public void setLongitudServicioEduMasCercano(BigDecimal longitudServicioEduMasCercano) {
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
	}

	public String getGeoHash() {
		return geoHash;
	}

	public void setGeoHash(String geoHash) {
		this.geoHash = geoHash;
	}

	public Maestro getTipoResolucion() {
		return tipoResolucion;
	}

	public void setTipoResolucion(Maestro tipoResolucion) {
		this.tipoResolucion = tipoResolucion;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public Maestro getTipoEstado() {
		return tipoEstado;
	}

	public void setTipoEstado(Maestro tipoEstado) {
		this.tipoEstado = tipoEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idPrograma != null ? idPrograma.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Programa)) {
			return false;
		}
		Programa other = (Programa) object;
		if ((this.idPrograma == null && other.idPrograma != null)
				|| (this.idPrograma != null && !this.idPrograma.equals(other.idPrograma))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Programa[ idPrograma=" + idPrograma + " ]";
	}

	@SuppressWarnings({ "unchecked" })
	public <T> T claseNullAContruir(Class<T> clase, Object obj, Object id) {
		T objT = null;
		try {
			if (Objects.isNull(obj)) {
				// String className = ((ParameterizedType)
				// obj.getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
				// Class<?> clazz = Class.forName(className);
				T clazz = clase.newInstance();
				if (clazz instanceof Maestro) {
					Maestro obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new Maestro(0L);
					} else {
						obj2 = new Maestro((Long) id);
					}
					objT = (T) obj2;
				} else if (clazz instanceof Distritos) {
					Distritos obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new Distritos("0");
					} else {
						obj2 = new Distritos((String) id);
					}
					objT = (T) obj2;
				} else if (clazz instanceof DreUgel) {
					DreUgel obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new DreUgel("0");
					} else {
						obj2 = new DreUgel((String) id);
					}
					objT = (T) obj2;
				}
			} else {
				objT = (T) obj;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return objT;
	}
}
