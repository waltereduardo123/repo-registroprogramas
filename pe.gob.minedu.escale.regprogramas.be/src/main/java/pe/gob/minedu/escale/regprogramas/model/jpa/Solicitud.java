package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;
import pe.gob.minedu.escale.common.util.StringUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_solicitud")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Solicitud.findAll", query = "SELECT s FROM Solicitud s") })
@DynamicUpdate(value = true)
public class Solicitud extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_SOLICITUD")
	private Long idSolicitud;

	@Column(name = "C_CODSOLI")
	private String codSolicitud;

	@JoinColumn(name = "N_ID_TIPSOLI")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSolicitud;

	@Column(name = "C_CODMOD")
	private String codigoModular;

	@JoinColumn(name = "N_ID_TIPPROG")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoPrograma;

	@Column(name = "C_NOMPROG")
	private String nombrePrograma;

	@Column(name = "N_PROLAT")
	private BigDecimal latitudPrograma;

	@Column(name = "N_PROLON")
	private BigDecimal longitudPrograma;

	@JoinColumn(name = "N_ID_TIPGEST")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoGestion;

	@JoinColumn(name = "N_ID_TIPDEPE")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoDependencia;

	@JoinColumn(name = "N_ID_TIPEGES")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoGestionEducativa;

	@JoinColumn(name = "N_ID_TIPTURN")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoTurno;

	@JoinColumn(name = "N_ID_TIPCJES")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoContinuidadJornadaEscolar;

	@JoinColumn(name = "N_ID_TIPVIA")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoVia;
	
	
	@Column(name = "C_DIRNOMVIA")
	private String nombreVia;

	@Column(name = "C_DIRNUMVIA")
	private String numeroVia;

	@Column(name = "C_DIRMZ")
	private String manzana;

	@Column(name = "C_DIRLOTE")
	private String lote;

	@JoinColumn(name = "N_ID_TIPLOCD")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoLocalidad;

	@Column(name = "C_DIRLOCD")
	private String localidad;

	@Column(name = "C_DIRETAP")
	private String etapa;

	@Column(name = "C_DIRSECT")
	private String sector;

	@Column(name = "C_DIRZONA")
	private String zona;

	@Column(name = "C_DIROTRO")
	private String otraDireccion;

	@Column(name = "C_DIRREFE")
	private String referenciaDireccion;

	@Column(name = "C_CODCCPP")
	private String codigoCentroPoblado;

	@Column(name = "C_NOMCCPP")
	private String nombreCentroPoblado;
	
	@Column(name = "C_COD_AREA")
	private String codigoArea;

	@Column(name = "C_COD_AREASIG")
	private String codigoAreaSig;
	
	@Column(name = "N_CCPPLAT")
	private BigDecimal latitudCentroPoblado;

	@Column(name = "N_CCPPLON")
	private BigDecimal longitudCentroPoblado;

	@Column(name = "C_CSEMC")
	private String codigoServicioEduMasCercano;

	@Column(name = "C_NOMSEMC")
	private String nombreServicioEduMasCercano;

	@Column(name = "N_SEMCLAT")
	private BigDecimal latitudServicioEduMasCercano;

	@Column(name = "N_SEMCLON")
	private BigDecimal longitudServicioEduMasCercano;

	@JoinColumn(name = "N_ID_TIPPRAG")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoProveedorAgua;

	@Column(name = "C_OTRPRAGUA")
	private String otroProveedorAgua;

	@Column(name = "C_SUMAGUA")
	private String suministroAgua;

	@JoinColumn(name = "N_ID_TIPPREN")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoProveedorEnergia;

	@Column(name = "C_OTRPRENER")
	private String otroProveedorEnergia;

	@Column(name = "C_SUMENER")
	private String suministroEnergia;

	@JoinColumn(name = "N_ID_TIPSITU")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSituacionSolicitud;

	@Column(name = "C_GEOHASH")
	private String geoHash;

	@Column(name = "C_USUENV")
	private String usuarioEnvio;

	@Column(name = "C_NOMUSUENV")
	private String nombreUsuarioEnvio;

	@Column(name = "D_FECENV")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEnvio;

	@Column(name = "C_USUMODI")
	private String usuarioModificacion;

	@Column(name = "C_NOMUSUMODI")
	private String nombreUsuarioModificacion;

	@Column(name = "D_FECMODI")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;

	@Column(name = "C_USUREVI")
	private String usuarioRevision;

	@Column(name = "C_NOMUSUREVI")
	private String nombreUsuarioRevision;

	@Column(name = "D_FECREVI")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRevision;

	@Column(name = "C_USUREVS")
	private String usuarioRevisionSig;

	@Column(name = "C_NOMUSUREVS")
	private String nombreUsuarioRevisionSig;

	@Column(name = "D_FECREVS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRevisionSig;

	@Column(name = "D_FECATEN")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAtencion;

	@JoinColumn(name = "N_ID_TIPSITUREV")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSituacionRevision;

	@JoinColumn(name = "N_ID_TIPSITUREVSIG")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSituacionRevisionSig;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "N_ID_TIPSITUPROG")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSituacionPrograma;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.LAZY)
	private List<SolicitudDocumento> listaSolicitudDocumento;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.LAZY)
	private List<RevisionSolicitudCampos> listaRevisionSolicitudCampos;

	// @JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO", insertable
	// = false, updatable = false)
	@JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO")
	@ManyToOne(fetch = FetchType.EAGER)
	private DreUgel dreUgel;

	// @JoinColumn(name = "ID_DISTRITO", referencedColumnName = "ID_DISTRITO",
	// insertable = false, updatable = false)
	@JoinColumn(name = "ID_DISTRITO", referencedColumnName = "ID_DISTRITO")
	@ManyToOne(fetch = FetchType.EAGER)
	private Distritos distrito;

	@OneToMany(mappedBy = "solicitud", fetch = FetchType.LAZY)
	private List<AccionSolicitud> listaAccionSolicitud;

	@Column(name = "C_INDULT")
	private String indicadorUltimo;
	
	
	@JoinColumn(name = "N_ID_TIPOPERIODO")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro periodoRegistrado;
	
	@Transient
	private int count;

	public Solicitud() {
	}

	public Solicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}	
	

	public Solicitud(Long idSolicitud, String codSolicitud, Maestro tipoSolicitud, String codigoModular,
			Maestro tipoPrograma, String nombrePrograma, BigDecimal latitudPrograma, BigDecimal longitudPrograma,
			Maestro tipoGestion, Maestro tipoDependencia, Maestro tipoGestionEducativa, Maestro tipoTurno,
			Maestro tipoContinuidadJornadaEscolar, Maestro tipoVia, String nombreVia, String numeroVia, String manzana,
			String lote, Maestro tipoLocalidad, String localidad, String etapa, String sector, String zona,
			String otraDireccion, String referenciaDireccion, String codigoCentroPoblado, String nombreCentroPoblado,
			String codigoArea, String codigoAreaSig, BigDecimal latitudCentroPoblado, BigDecimal longitudCentroPoblado,
			String codigoServicioEduMasCercano, String nombreServicioEduMasCercano,
			BigDecimal latitudServicioEduMasCercano, BigDecimal longitudServicioEduMasCercano,
			Maestro tipoProveedorAgua, String otroProveedorAgua, String suministroAgua, Maestro tipoProveedorEnergia,
			String otroProveedorEnergia, String suministroEnergia, Maestro tipoSituacionSolicitud, String geoHash,
			String usuarioEnvio, String nombreUsuarioEnvio, Date fechaEnvio, String usuarioModificacion,
			String nombreUsuarioModificacion, Date fechaModificacion, String usuarioRevision,
			String nombreUsuarioRevision, Date fechaRevision, String usuarioRevisionSig,
			String nombreUsuarioRevisionSig, Date fechaRevisionSig, Date fechaAtencion, Maestro tipoSituacionRevision,
			Maestro tipoSituacionRevisionSig, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado, Maestro tipoSituacionPrograma,
			List<SolicitudDocumento> listaSolicitudDocumento,
			List<RevisionSolicitudCampos> listaRevisionSolicitudCampos, DreUgel dreUgel, Distritos distrito,
			List<AccionSolicitud> listaAccionSolicitud, int count) {
		super();
		this.idSolicitud = idSolicitud;
		this.codSolicitud = codSolicitud;
		this.tipoSolicitud = tipoSolicitud;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.latitudPrograma = latitudPrograma;
		this.longitudPrograma = longitudPrograma;
		this.tipoGestion = tipoGestion;
		this.tipoDependencia = tipoDependencia;
		this.tipoGestionEducativa = tipoGestionEducativa;
		this.tipoTurno = tipoTurno;
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
		this.tipoVia = tipoVia;
		this.nombreVia = nombreVia;
		this.numeroVia = numeroVia;
		this.manzana = manzana;
		this.lote = lote;
		this.tipoLocalidad = tipoLocalidad;
		this.localidad = localidad;
		this.etapa = etapa;
		this.sector = sector;
		this.zona = zona;
		this.otraDireccion = otraDireccion;
		this.referenciaDireccion = referenciaDireccion;
		this.codigoCentroPoblado = codigoCentroPoblado;
		this.nombreCentroPoblado = nombreCentroPoblado;
		this.codigoArea = codigoArea;
		this.codigoAreaSig = codigoAreaSig;
		this.latitudCentroPoblado = latitudCentroPoblado;
		this.longitudCentroPoblado = longitudCentroPoblado;
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
		this.tipoProveedorAgua = tipoProveedorAgua;
		this.otroProveedorAgua = otroProveedorAgua;
		this.suministroAgua = suministroAgua;
		this.tipoProveedorEnergia = tipoProveedorEnergia;
		this.otroProveedorEnergia = otroProveedorEnergia;
		this.suministroEnergia = suministroEnergia;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.geoHash = geoHash;
		this.usuarioEnvio = usuarioEnvio;
		this.nombreUsuarioEnvio = nombreUsuarioEnvio;
		this.fechaEnvio = fechaEnvio;
		this.usuarioModificacion = usuarioModificacion;
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
		this.fechaModificacion = fechaModificacion;
		this.usuarioRevision = usuarioRevision;
		this.nombreUsuarioRevision = nombreUsuarioRevision;
		this.fechaRevision = fechaRevision;
		this.usuarioRevisionSig = usuarioRevisionSig;
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
		this.fechaRevisionSig = fechaRevisionSig;
		this.fechaAtencion = fechaAtencion;
		this.tipoSituacionRevision = tipoSituacionRevision;
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.tipoSituacionPrograma = tipoSituacionPrograma;
		this.listaSolicitudDocumento = listaSolicitudDocumento;
		this.listaRevisionSolicitudCampos = listaRevisionSolicitudCampos;
		this.dreUgel = dreUgel;
		this.distrito = distrito;
		this.listaAccionSolicitud = listaAccionSolicitud;
		this.count = count;
	}

	public Solicitud(Long idSolicitud, String codSolicitud, Maestro tipoSolicitud, String codigoModular,
			Maestro tipoPrograma, String nombrePrograma, Date fechaCreacion, String usuarioCreacion, String estado,
			DreUgel dreUgel) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = codSolicitud;
		this.tipoSolicitud = tipoSolicitud;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		this.dreUgel = dreUgel;
	}

	public Solicitud(Long idSolicitud, String codSolicitud, String codigoModular, String nombrePrograma,
			Date fechaCreacion, String usuarioCreacion, String estado) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = codSolicitud;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public Solicitud(Long idSolicitud, String codSolicitud, String codigoModular, String nombrePrograma,
			String tipoSolicitud, Long tipoSolicitudIdMaestro, String tipoSituacionSolicitud, Date fechaCreacion,
			String usuarioCreacion, String estado, Long tipoSituacionProgramaIdMaestro, String indicadorUltimo) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = StringUtil.isNotNullOrBlank(codSolicitud) ? codSolicitud : "";
		this.codigoModular = StringUtil.isNotNullOrBlank(codigoModular) ? codigoModular : "";
		this.nombrePrograma = StringUtil.isNotNullOrBlank(nombrePrograma) ? nombrePrograma : "";
		if (this.tipoSolicitud == null)
			this.tipoSolicitud = new Maestro(tipoSolicitudIdMaestro);
		this.tipoSolicitud.setDescripcionMaestro(StringUtil.isNotNullOrBlank(tipoSolicitud) ? tipoSolicitud : "");
		if (this.tipoSituacionSolicitud == null)
			this.tipoSituacionSolicitud = new Maestro(0L);
		this.tipoSituacionSolicitud.setDescripcionMaestro(
				StringUtil.isNotNullOrBlank(tipoSituacionSolicitud) ? tipoSituacionSolicitud : "");
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		if (this.tipoSituacionPrograma == null)
			this.tipoSituacionPrograma = new Maestro(tipoSituacionProgramaIdMaestro);
	}

	public Solicitud(Long idSolicitud, String codSolicitud, String codigoModular, String nombrePrograma,
			String tipoSolicitud, Long tipoSolicitudIdMaestro, String tipoSituacionSolicitud, Date fechaCreacion,
			String usuarioCreacion, String estado, Long tipoSituacionProgramaIdMaestro, String indicadorUltimo, Long tipoSituacionSolicitudIdMaestro,
			String tipoSituacionSolicitudCodigoItem, Long idTipoSituacionRevisionPadron, Long idTipoSituacionRevisionSig ) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = StringUtil.isNotNullOrBlank(codSolicitud) ? codSolicitud : "";
		this.codigoModular = StringUtil.isNotNullOrBlank(codigoModular) ? codigoModular : "";
		this.nombrePrograma = StringUtil.isNotNullOrBlank(nombrePrograma) ? nombrePrograma : "";
		if (this.tipoSolicitud == null)
			this.tipoSolicitud = new Maestro(tipoSolicitudIdMaestro);
		this.tipoSolicitud.setDescripcionMaestro(StringUtil.isNotNullOrBlank(tipoSolicitud) ? tipoSolicitud : "");
		this.tipoSituacionSolicitud = Objects.isNull(tipoSituacionSolicitudIdMaestro)? new Maestro(0L):new Maestro(tipoSituacionSolicitudIdMaestro);
		
		this.tipoSituacionSolicitud.setDescripcionMaestro(
				StringUtil.isNotNullOrBlank(tipoSituacionSolicitud) ? tipoSituacionSolicitud : "");
		this.tipoSituacionSolicitud.setCodigoItem(StringUtil.isNotNullOrBlank(tipoSituacionSolicitudCodigoItem) ? tipoSituacionSolicitudCodigoItem : "");
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		if (this.tipoSituacionPrograma == null)
			this.tipoSituacionPrograma = new Maestro(tipoSituacionProgramaIdMaestro);
		this.tipoSituacionRevision = claseNullAContruir(Maestro.class, this.tipoSituacionRevision, idTipoSituacionRevisionPadron);
		this.tipoSituacionRevisionSig = claseNullAContruir(Maestro.class, this.tipoSituacionRevisionSig, idTipoSituacionRevisionSig);
	}
	
	public Solicitud(Long idSolicitud, String codSolicitud, String codigoModular, String nombrePrograma,
			String tipoSolicitud, Long idTipoSolicitud, String tipoSituacionSolicitud, Date fechaCreacion,
			String usuarioCreacion, String estado, Long idTipoContinuidadJornadaEscolar, Long idTipoDependencia,
			Long idTipoGestion, Long idTipoGestionEducativa, Long idTipoLocalidad, Long idTipoPrograma,
			Long idTipoProveedorAgua, Long idTipoProveedorEnergia, Long idTipoSituacionSolicitud, Long idTipoTurno,
			Long idTipoVia, String idDistrito, String idDreUgel) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = StringUtil.isNotNullOrBlank(codSolicitud) ? codSolicitud : "";
		this.codigoModular = StringUtil.isNotNullOrBlank(codigoModular) ? codigoModular : "";
		this.nombrePrograma = StringUtil.isNotNullOrBlank(nombrePrograma) ? nombrePrograma : "";

		this.tipoSolicitud.setDescripcionMaestro(StringUtil.isNotNullOrBlank(tipoSolicitud) ? tipoSolicitud : "");
		if (this.tipoSituacionSolicitud == null)
			this.tipoSituacionSolicitud = new Maestro(0L);

		this.tipoContinuidadJornadaEscolar = claseNullAContruir(Maestro.class, this.tipoContinuidadJornadaEscolar,
				idTipoContinuidadJornadaEscolar);
		this.tipoDependencia = claseNullAContruir(Maestro.class, this.tipoDependencia, idTipoDependencia);
		this.tipoGestion = claseNullAContruir(Maestro.class, this.tipoGestion, idTipoGestion);
		this.tipoGestionEducativa = claseNullAContruir(Maestro.class, this.tipoGestionEducativa,
				idTipoGestionEducativa);
		this.tipoLocalidad = claseNullAContruir(Maestro.class, this.tipoLocalidad, idTipoLocalidad);
		this.tipoPrograma = claseNullAContruir(Maestro.class, this.tipoPrograma, idTipoPrograma);
		this.tipoProveedorAgua = claseNullAContruir(Maestro.class, this.tipoProveedorAgua, idTipoProveedorAgua);
		this.tipoProveedorEnergia = claseNullAContruir(Maestro.class, this.tipoProveedorEnergia,
				idTipoProveedorEnergia);
		this.tipoSituacionSolicitud = claseNullAContruir(Maestro.class, this.tipoSituacionSolicitud,
				idTipoSituacionSolicitud);
		this.tipoSolicitud = claseNullAContruir(Maestro.class, this.tipoSolicitud, idTipoSolicitud);
		this.tipoTurno = claseNullAContruir(Maestro.class, this.tipoTurno, idTipoTurno);
		this.tipoVia = claseNullAContruir(Maestro.class, this.tipoVia, idTipoVia);
		this.distrito = claseNullAContruir(Distritos.class, this.distrito, idDistrito);
		this.dreUgel = claseNullAContruir(DreUgel.class, this.dreUgel, idDreUgel);

		this.tipoSituacionSolicitud.setDescripcionMaestro(
				StringUtil.isNotNullOrBlank(tipoSituacionSolicitud) ? tipoSituacionSolicitud : "");
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	@SuppressWarnings("deprecation")
	public Solicitud(Long idSolicitud, String codigo, Date fechaCreacion, String tipoSolicitudDesc,
			String nombrePrograma, String tipoProgramaDesc, String codigoModular, String tipoSituacionSolicitudDesc,
			String nombreUsuarioRevision, String nombreUsuarioRevisionSig, String usuarioRevision,
			String usuarioRevisionSig, String tipoSituacionRevisionSigDesc, String tipoSituacionRevisionDesc) {
		this.idSolicitud = idSolicitud;
		this.dreUgel = claseNullAContruir(DreUgel.class, this.dreUgel, codigo);
		this.fechaCreacion = Objects.isNull(fechaCreacion) ? new Date(9999, 99, 99) : fechaCreacion;
		this.tipoSolicitud = claseNullAContruir(Maestro.class, this.tipoSolicitud, null);
		this.tipoSolicitud.setDescripcionMaestro(tipoSolicitudDesc);
		this.nombrePrograma = StringUtil.isNotNullOrBlank(nombrePrograma) ? nombrePrograma : "";
		this.tipoPrograma = claseNullAContruir(Maestro.class, this.tipoPrograma, null);
		this.tipoPrograma.setDescripcionMaestro(tipoProgramaDesc);
		this.codigoModular = StringUtil.isNotNullOrBlank(codigoModular) ? codigoModular : "";
		this.tipoSituacionSolicitud = claseNullAContruir(Maestro.class, this.tipoSituacionSolicitud, null);
		this.tipoSituacionSolicitud.setDescripcionMaestro(tipoSituacionSolicitudDesc);
		this.nombreUsuarioRevision = Objects.isNull(nombreUsuarioRevision) ? "" : nombreUsuarioRevision;
		this.nombreUsuarioRevisionSig = Objects.isNull(nombreUsuarioRevisionSig) ? "" : nombreUsuarioRevisionSig;
		this.usuarioRevision = Objects.isNull(usuarioRevision) ? "" : usuarioRevision;
		this.usuarioRevisionSig = Objects.isNull(usuarioRevisionSig) ? "" : usuarioRevisionSig;
		this.tipoSituacionRevisionSig = claseNullAContruir(Maestro.class, this.tipoSituacionRevisionSig, null);
		this.tipoSituacionRevisionSig.setDescripcionMaestro(tipoSituacionRevisionSigDesc);
		this.tipoSituacionRevision = claseNullAContruir(Maestro.class, this.tipoSituacionRevision, null);
		this.tipoSituacionRevision.setDescripcionMaestro(tipoSituacionRevisionDesc);
	}

	@SuppressWarnings("deprecation")
	public Solicitud(Long idSolicitud, String codigo, Date fechaCreacion, String tipoSolicitudDesc,
			String nombrePrograma, String tipoProgramaDesc, String codigoModular, String tipoSituacionSolicitudDesc,
			String nombreUsuarioRevision, String nombreUsuarioRevisionSig, String usuarioRevision,
			String usuarioRevisionSig, String tipoSituacionRevisionSigDesc, String tipoSituacionRevisionDesc, String indicadorUltimo, Date fechaModificacion) {
		this.idSolicitud = idSolicitud;
		this.dreUgel = claseNullAContruir(DreUgel.class, this.dreUgel, codigo);
		this.fechaCreacion = Objects.isNull(fechaCreacion) ? new Date(9999, 99, 99) : fechaCreacion;
		this.tipoSolicitud = claseNullAContruir(Maestro.class, this.tipoSolicitud, null);
		this.tipoSolicitud.setDescripcionMaestro(tipoSolicitudDesc);
		this.nombrePrograma = StringUtil.isNotNullOrBlank(nombrePrograma) ? nombrePrograma : "";
		this.tipoPrograma = claseNullAContruir(Maestro.class, this.tipoPrograma, null);
		this.tipoPrograma.setDescripcionMaestro(tipoProgramaDesc);
		this.codigoModular = StringUtil.isNotNullOrBlank(codigoModular) ? codigoModular : "";
		this.tipoSituacionSolicitud = claseNullAContruir(Maestro.class, this.tipoSituacionSolicitud, null);
		this.tipoSituacionSolicitud.setDescripcionMaestro(tipoSituacionSolicitudDesc);
		this.nombreUsuarioRevision = Objects.isNull(nombreUsuarioRevision) ? "" : nombreUsuarioRevision;
		this.nombreUsuarioRevisionSig = Objects.isNull(nombreUsuarioRevisionSig) ? "" : nombreUsuarioRevisionSig;
		this.usuarioRevision = Objects.isNull(usuarioRevision) ? "" : usuarioRevision;
		this.usuarioRevisionSig = Objects.isNull(usuarioRevisionSig) ? "" : usuarioRevisionSig;
		this.tipoSituacionRevisionSig = claseNullAContruir(Maestro.class, this.tipoSituacionRevisionSig, null);
		this.tipoSituacionRevisionSig.setDescripcionMaestro(tipoSituacionRevisionSigDesc);
		this.tipoSituacionRevision = claseNullAContruir(Maestro.class, this.tipoSituacionRevision, null);
		this.tipoSituacionRevision.setDescripcionMaestro(tipoSituacionRevisionDesc);
		this.indicadorUltimo = indicadorUltimo;
		this.fechaModificacion = Objects.isNull(fechaModificacion) ? null : fechaModificacion;
	}
	
	
	
	
	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getCodSolicitud() {
		return codSolicitud;
	}

	public void setCodSolicitud(String codSolicitud) {
		this.codSolicitud = codSolicitud;
	}

	public Maestro getTipoSolicitud() {
		// if (tipoSolicitud==null) {
		// tipoSolicitud = new Maestro(0L);
		// }
		return tipoSolicitud;
	}

	public void setTipoSolicitud(Maestro tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public Maestro getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(Maestro tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public BigDecimal getLatitudPrograma() {
		return latitudPrograma;
	}

	public void setLatitudPrograma(BigDecimal latitudPrograma) {
		this.latitudPrograma = latitudPrograma;
	}

	public BigDecimal getLongitudPrograma() {
		return longitudPrograma;
	}

	public void setLongitudPrograma(BigDecimal longitudPrograma) {
		this.longitudPrograma = longitudPrograma;
	}

	public Maestro getTipoGestion() {
		return tipoGestion;
	}

	public void setTipoGestion(Maestro tipoGestion) {
		this.tipoGestion = tipoGestion;
	}

	public Maestro getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(Maestro tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}

	public Maestro getTipoGestionEducativa() {
		return tipoGestionEducativa;
	}

	public void setTipoGestionEducativa(Maestro tipoGestionEducativa) {
		this.tipoGestionEducativa = tipoGestionEducativa;
	}

	public Maestro getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(Maestro tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	public Maestro getTipoContinuidadJornadaEscolar() {
		return tipoContinuidadJornadaEscolar;
	}

	public void setTipoContinuidadJornadaEscolar(Maestro tipoContinuidadJornadaEscolar) {
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
	}

	public Maestro getTipoVia() {
		return tipoVia;
	}

	public void setTipoVia(Maestro tipoVia) {
		this.tipoVia = tipoVia;
	}

	public String getNombreVia() {
		return nombreVia;
	}

	public void setNombreVia(String nombreVia) {
		this.nombreVia = nombreVia;
	}

	public String getNumeroVia() {
		return numeroVia;
	}

	public void setNumeroVia(String numeroVia) {
		this.numeroVia = numeroVia;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Maestro getTipoLocalidad() {
		return tipoLocalidad;
	}

	public void setTipoLocalidad(Maestro tipoLocalidad) {
		this.tipoLocalidad = tipoLocalidad;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getOtraDireccion() {
		return otraDireccion;
	}

	public void setOtraDireccion(String otraDireccion) {
		this.otraDireccion = otraDireccion;
	}

	public String getReferenciaDireccion() {
		return referenciaDireccion;
	}

	public void setReferenciaDireccion(String referenciaDireccion) {
		this.referenciaDireccion = referenciaDireccion;
	}

	public String getCodigoCentroPoblado() {
		return codigoCentroPoblado;
	}

	public void setCodigoCentroPoblado(String codigoCentroPoblado) {
		this.codigoCentroPoblado = codigoCentroPoblado;
	}

	public String getNombreCentroPoblado() {
		return nombreCentroPoblado;
	}

	public void setNombreCentroPoblado(String nombreCentroPoblado) {
		this.nombreCentroPoblado = nombreCentroPoblado;
	}

	public BigDecimal getLatitudCentroPoblado() {
		return latitudCentroPoblado;
	}

	public void setLatitudCentroPoblado(BigDecimal latitudCentroPoblado) {
		this.latitudCentroPoblado = latitudCentroPoblado;
	}

	public BigDecimal getLongitudCentroPoblado() {
		return longitudCentroPoblado;
	}

	public void setLongitudCentroPoblado(BigDecimal longitudCentroPoblado) {
		this.longitudCentroPoblado = longitudCentroPoblado;
	}

	public String getCodigoServicioEduMasCercano() {
		return codigoServicioEduMasCercano;
	}

	public void setCodigoServicioEduMasCercano(String codigoServicioEduMasCercano) {
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
	}

	public String getNombreServicioEduMasCercano() {
		return nombreServicioEduMasCercano;
	}

	public void setNombreServicioEduMasCercano(String nombreServicioEduMasCercano) {
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
	}

	public BigDecimal getLatitudServicioEduMasCercano() {
		return latitudServicioEduMasCercano;
	}

	public void setLatitudServicioEduMasCercano(BigDecimal latitudServicioEduMasCercano) {
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
	}

	public BigDecimal getLongitudServicioEduMasCercano() {
		return longitudServicioEduMasCercano;
	}

	public void setLongitudServicioEduMasCercano(BigDecimal longitudServicioEduMasCercano) {
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
	}

	public Maestro getTipoProveedorAgua() {
		return tipoProveedorAgua;
	}

	public void setTipoProveedorAgua(Maestro tipoProveedorAgua) {
		this.tipoProveedorAgua = tipoProveedorAgua;
	}

	public String getOtroProveedorAgua() {
		return otroProveedorAgua;
	}

	public void setOtroProveedorAgua(String otroProveedorAgua) {
		this.otroProveedorAgua = otroProveedorAgua;
	}

	public String getSuministroAgua() {
		return suministroAgua;
	}

	public void setSuministroAgua(String suministroAgua) {
		this.suministroAgua = suministroAgua;
	}

	public Maestro getTipoProveedorEnergia() {
		// if (tipoProveedorEnergia==null) {
		// tipoProveedorEnergia = new Maestro(0L);
		// }
		return tipoProveedorEnergia;
	}

	public void setTipoProveedorEnergia(Maestro tipoProveedorEnergia) {
		this.tipoProveedorEnergia = tipoProveedorEnergia;
	}

	public String getOtroProveedorEnergia() {
		return otroProveedorEnergia;
	}

	public void setOtroProveedorEnergia(String otroProveedorEnergia) {
		this.otroProveedorEnergia = otroProveedorEnergia;
	}

	public String getSuministroEnergia() {
		return suministroEnergia;
	}

	public void setSuministroEnergia(String suministroEnergia) {
		this.suministroEnergia = suministroEnergia;
	}

	public Maestro getTipoSituacionSolicitud() {
		// if (tipoSituacionSolicitud==null) {
		// tipoSituacionSolicitud = new Maestro();
		// }
		return tipoSituacionSolicitud;
	}

	public void setTipoSituacionSolicitud(Maestro tipoSituacionSolicitud) {
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
	}

	public String getUsuarioEnvio() {
		return usuarioEnvio;
	}

	public void setUsuarioEnvio(String usuarioEnvio) {
		this.usuarioEnvio = usuarioEnvio;
	}

	public String getNombreUsuarioEnvio() {
		return nombreUsuarioEnvio;
	}

	public void setNombreUsuarioEnvio(String nombreUsuarioEnvio) {
		this.nombreUsuarioEnvio = nombreUsuarioEnvio;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getNombreUsuarioModificacion() {
		return nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioRevision() {
		return usuarioRevision;
	}

	public void setUsuarioRevision(String usuarioRevision) {
		this.usuarioRevision = usuarioRevision;
	}

	public String getNombreUsuarioRevision() {
		return nombreUsuarioRevision;
	}

	public void setNombreUsuarioRevision(String nombreUsuarioRevision) {
		this.nombreUsuarioRevision = nombreUsuarioRevision;
	}

	public Date getFechaRevision() {
		return fechaRevision;
	}

	public void setFechaRevision(Date fechaRevision) {
		this.fechaRevision = fechaRevision;
	}

	public String getUsuarioRevisionSig() {
		return usuarioRevisionSig;
	}

	public void setUsuarioRevisionSig(String usuarioRevisionSig) {
		this.usuarioRevisionSig = usuarioRevisionSig;
	}

	public String getNombreUsuarioRevisionSig() {
		return nombreUsuarioRevisionSig;
	}

	public void setNombreUsuarioRevisionSig(String nombreUsuarioRevisionSig) {
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
	}

	public Date getFechaRevisionSig() {
		return fechaRevisionSig;
	}

	public void setFechaRevisionSig(Date fechaRevisionSig) {
		this.fechaRevisionSig = fechaRevisionSig;
	}

	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@XmlTransient
	public List<SolicitudDocumento> getListaSolicitudDocumento() {
		return listaSolicitudDocumento;
	}

	public void setListaSolicitudDocumento(List<SolicitudDocumento> listaSolicitudDocumento) {
		this.listaSolicitudDocumento = listaSolicitudDocumento;
	}

	@XmlTransient
	public List<RevisionSolicitudCampos> getListaRevisionSolicitudCampos() {
		return listaRevisionSolicitudCampos;
	}

	public void setListaRevisionSolicitudCampos(List<RevisionSolicitudCampos> listaRevisionSolicitudCampos) {
		this.listaRevisionSolicitudCampos = listaRevisionSolicitudCampos;
	}

	public DreUgel getDreUgel() {
		return dreUgel;
	}

	public void setDreUgel(DreUgel dreUgel) {
		this.dreUgel = dreUgel;
	}

	public Distritos getDistrito() {
		return distrito;
	}

	public void setDistrito(Distritos distrito) {
		this.distrito = distrito;
	}

	@XmlTransient
	public List<AccionSolicitud> getListaAccionSolicitud() {
		return listaAccionSolicitud;
	}

	public void setListaAccionSolicitud(List<AccionSolicitud> listaAccionSolicitud) {
		this.listaAccionSolicitud = listaAccionSolicitud;
	}

	public String getGeoHash() {
		return geoHash;
	}

	public void setGeoHash(String geoHash) {
		this.geoHash = geoHash;
	}

	public String getCodigoArea() {
		return codigoArea;
	}

	public void setCodigoArea(String codigoArea) {
		this.codigoArea = codigoArea;
	}		

	public String getCodigoAreaSig() {
		return codigoAreaSig;
	}

	public void setCodigoAreaSig(String codigoAreaSig) {
		this.codigoAreaSig = codigoAreaSig;
	}

	public Maestro getTipoSituacionPrograma() {
		return tipoSituacionPrograma;
	}

	public void setTipoSituacionPrograma(Maestro tipoSituacionPrograma) {
		this.tipoSituacionPrograma = tipoSituacionPrograma;
	}

	public Maestro getTipoSituacionRevision() {
		return tipoSituacionRevision;
	}

	public void setTipoSituacionRevision(Maestro tipoSituacionRevision) {
		this.tipoSituacionRevision = tipoSituacionRevision;
	}

	public Maestro getTipoSituacionRevisionSig() {
		return tipoSituacionRevisionSig;
	}

	public void setTipoSituacionRevisionSig(Maestro tipoSituacionRevisionSig) {
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}	

	public String getIndicadorUltimo() {
		return indicadorUltimo;
	}

	public void setIndicadorUltimo(String indicadorUltimo) {
		this.indicadorUltimo = indicadorUltimo;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idSolicitud != null ? idSolicitud.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Solicitud)) {
			return false;
		}
		Solicitud other = (Solicitud) object;
		if ((this.idSolicitud == null && other.idSolicitud != null)
				|| (this.idSolicitud != null && !this.idSolicitud.equals(other.idSolicitud))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Solicitud[ idSolicitud=" + idSolicitud + " ]";
	}

	@SuppressWarnings({ "unchecked" })
	public <T> T claseNullAContruir(Class<T> clase, Object obj, Object id) {
		T objT = null;
		try {
			if (Objects.isNull(obj)) {
				// String className = ((ParameterizedType)
				// obj.getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
				// Class<?> clazz = Class.forName(className);
				T clazz = clase.newInstance();
				if (clazz instanceof Maestro) {
					Maestro obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new Maestro(0L);
					} else {
						obj2 = new Maestro((Long) id);
					}
					objT = (T) obj2;
				} else if (clazz instanceof Distritos) {
					Distritos obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new Distritos("0");
					} else {
						obj2 = new Distritos((String) id);
					}
					objT = (T) obj2;
				} else if (clazz instanceof DreUgel) {
					DreUgel obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new DreUgel("0");
					} else {
						obj2 = new DreUgel((String) id);
					}
					objT = (T) obj2;
				}
			} else {
				objT = (T) obj;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return objT;
	}

	public Maestro getPeriodoRegistrado() {
		return periodoRegistrado;
	}

	public void setPeriodoRegistrado(Maestro periodoRegistrado) {
		this.periodoRegistrado = periodoRegistrado;
	}


	
	
	
	
}
