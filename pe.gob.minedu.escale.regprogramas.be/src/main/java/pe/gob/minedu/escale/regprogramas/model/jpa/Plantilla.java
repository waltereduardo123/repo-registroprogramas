package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_plantilla")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Plantilla.findAll", query = "SELECT p FROM Plantilla p") })
public class Plantilla extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_PLANTILLA")
	private Long idPlantilla;

	@Column(name = "C_NOMPLANTILLA")
	private String nombreplantilla;

	@JoinColumn(name = "N_ID_TIPPLANTILLA")
	@OneToOne(fetch = FetchType.LAZY)
	private Maestro tipoPlantilla;

	@Column(name = "C_DESCPLANTILLA")
	private String descripcionPlantilla;

	@Column(name = "C_CODARCH")
	private String codigoArchivo;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	public Plantilla() {
	}

	public Plantilla(Long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public Plantilla(Long idPlantilla, String nombreplantilla, Maestro tipoPlantilla, String descripcionPlantilla,
			String codigoArchivo, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado) {
		super();
		this.idPlantilla = idPlantilla;
		this.nombreplantilla = nombreplantilla;
		this.tipoPlantilla = tipoPlantilla;
		this.descripcionPlantilla = descripcionPlantilla;
		this.codigoArchivo = codigoArchivo;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
	}

	public Long getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(Long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public String getNombreplantilla() {
		return nombreplantilla;
	}

	public void setNombreplantilla(String nombreplantilla) {
		this.nombreplantilla = nombreplantilla;
	}

	public Maestro getTipoPlantilla() {
		return tipoPlantilla;
	}

	public void setTipoPlantilla(Maestro tipoPlantilla) {
		this.tipoPlantilla = tipoPlantilla;
	}

	public String getDescripcionPlantilla() {
		return descripcionPlantilla;
	}

	public void setDescripcionPlantilla(String descripcionPlantilla) {
		this.descripcionPlantilla = descripcionPlantilla;
	}

	public String getCodigoArchivo() {
		return codigoArchivo;
	}

	public void setCodigoArchivo(String codigoArchivo) {
		this.codigoArchivo = codigoArchivo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idPlantilla != null ? idPlantilla.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Plantilla)) {
			return false;
		}
		Plantilla other = (Plantilla) object;
		if ((this.idPlantilla == null && other.idPlantilla != null)
				|| (this.idPlantilla != null && !this.idPlantilla.equals(other.idPlantilla))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Plantilla[ idPlantilla=" + idPlantilla + " ]";
	}

}
