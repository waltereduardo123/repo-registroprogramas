package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "dre_ugel")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "DreUgel.findAll", query = "SELECT d FROM DreUgel d") })
public class DreUgel extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CODIGO")
	private String codigo;

	@Column(name = "NOMBREOI")
	private String nombreoi;

	@Column(name = "TIPO")
	private String tipo;

	@Column(name = "IN_VERNA")
	private Short inVerna;

	@Column(name = "POINT_X")
	private BigDecimal pointX;

	@Column(name = "POINT_Y")
	private BigDecimal pointY;

	@Column(name = "ZOOM")
	private Short zoom;

	@OneToMany(mappedBy = "dreUgel", fetch = FetchType.LAZY)
	private List<CoordinadorDreUgel> listaCoordinadorDreUgel;

	@OneToMany(mappedBy = "dreUgel", fetch = FetchType.LAZY)
	private List<Solicitud> listaSolicitud;
	
	@OneToMany(mappedBy = "dreUgel", fetch = FetchType.LAZY)
	private List<Programa> listaPrograma;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "dreUgel", fetch = FetchType.LAZY)
	private List<DreGeo> listaDreGeo;

	public DreUgel() {
	}

	public DreUgel(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombreoi() {
		return nombreoi;
	}

	public void setNombreoi(String nombreoi) {
		this.nombreoi = nombreoi;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Short getInVerna() {
		return inVerna;
	}

	public void setInVerna(Short inVerna) {
		this.inVerna = inVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}
	
	@XmlTransient
	public List<CoordinadorDreUgel> getListaCoordinadorDreUgel() {
		return listaCoordinadorDreUgel;
	}

	public void setListaCoordinadorDreUgel(List<CoordinadorDreUgel> listaCoordinadorDreUgel) {
		this.listaCoordinadorDreUgel = listaCoordinadorDreUgel;
	}
	
	@XmlTransient
	public List<Solicitud> getListaSolicitud() {
		return listaSolicitud;
	}

	public void setListaSolicitud(List<Solicitud> listaSolicitud) {
		this.listaSolicitud = listaSolicitud;
	}
	
	@XmlTransient
	public List<DreGeo> getListaDreGeo() {
		return listaDreGeo;
	}

	public void setListaDreGeo(List<DreGeo> listaDreGeo) {
		this.listaDreGeo = listaDreGeo;
	}
	
	 

	public List<Programa> getListaPrograma() {
		return listaPrograma;
	}

	@XmlTransient
	public void setListaPrograma(List<Programa> listaPrograma) {
		this.listaPrograma = listaPrograma;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DreUgel)) {
			return false;
		}
		DreUgel other = (DreUgel) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.DreUgel[ codigo=" + codigo + " ]";
	}

}
