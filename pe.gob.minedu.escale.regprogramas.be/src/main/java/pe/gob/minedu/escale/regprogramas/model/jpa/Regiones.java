package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name="regiones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regiones.findAll", query = "SELECT r FROM Regiones r")})
public class Regiones extends EntidadUtil implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID_REGION")
    private String idRegion;
   
    @Column(name="REGION")
    private String region;
    
    @Column(name = "IND_VERNA")
    private Short indVerna;
    
    @Column(name = "POINT_X")
    private BigDecimal pointX;
    
    @Column(name = "POINT_Y")
    private BigDecimal pointY;
    
    @Column(name = "ZOOM")
    private Short zoom;
       
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "regiones", fetch = FetchType.LAZY)
    private List<ProveedorDepartamento> listaProveedorDepartamento;
    
    
    @OneToMany(mappedBy = "regiones", fetch = FetchType.LAZY)
    private List<Provincias> listaProvincia;

    public Regiones() {
    }   

    public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Short getIndVerna() {
		return indVerna;
	}

	public void setIndVerna(Short indVerna) {
		this.indVerna = indVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	@XmlTransient
	public List<Provincias> getListaProvincia() {
		return listaProvincia;
	}

	public void setListaProvincia(List<Provincias> listaProvincia) {
		this.listaProvincia = listaProvincia;
	}

	@XmlTransient
	public List<ProveedorDepartamento> getListaProveedorDepartamento() {
		return listaProveedorDepartamento;
	}

	public void setListaProveedorDepartamento(List<ProveedorDepartamento> listaProveedorDepartamento) {
		this.listaProveedorDepartamento = listaProveedorDepartamento;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegion != null ? idRegion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Regiones)) {
            return false;
        }
        Regiones other = (Regiones) object;
        if ((this.idRegion == null && other.idRegion != null) || (this.idRegion != null && !this.idRegion.equals(other.idRegion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.Regiones[ idRegion=" + idRegion + " ]";
    }
    
}
