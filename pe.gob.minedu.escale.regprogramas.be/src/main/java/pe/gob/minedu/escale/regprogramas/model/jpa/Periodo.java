package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

/**
 * Entity implementation class for Entity: Periodo
 *
 */
@Entity
@Table(name="tbl_regpro_periodo")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Periodo.findAll", query = "SELECT p FROM Periodo p") })
@DynamicUpdate(value = true)
public class Periodo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)    
	@Column(name = "N_ID_PERIODO")
	private Integer idPeriodo;

	@Column(name = "C_NOMPERIODO")
	private String namePeriodo;

	@Column(name = "D_FEC_INIPERIODO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicioPeriodo;
	
	@Column(name = "D_FEC_FINPERIODO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFinPeriodo;
	
	@Column(name = "D_FEC_FINANIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFinAnio;
	
	@Column(name = "C_CODGRUP")
	private String codGrupo;
	
	@Column(name = "C_CODITEM")
	private String codItem;
	
	@Column(name = "C_USUCREA")
	private String userCrea;
	
	@Column(name = "D_FECCREA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecCrea;
	
	
	@Column(name = "C_ESTADO")
	private String estado;

	
	@Transient
	private int count;
	
	public Periodo() {
		super();
	}

	public Periodo(Integer idPeriodo, String namePeriodo, Date fechaInicioPeriodo, Date fechaFinPeriodo,
			Date fechaFinAnio, String codGrupo, String codItem, String userCrea, Date fecCrea, String estado) {
		super();
		this.idPeriodo = idPeriodo;
		this.namePeriodo = namePeriodo;
		this.fechaInicioPeriodo = fechaInicioPeriodo;
		this.fechaFinPeriodo = fechaFinPeriodo;
		this.fechaFinAnio = fechaFinAnio;
		this.codGrupo = codGrupo;
		this.codItem = codItem;
		this.userCrea = userCrea;
		this.fecCrea = fecCrea;
		this.estado = estado;
	}

	
	
	public Periodo(Integer idPeriodo, String namePeriodo, Date fechaInicioPeriodo, Date fechaFinPeriodo,
			Date fechaFinAnio, String codGrupo, String codItem, String estado) {
		super();
		this.idPeriodo = idPeriodo;
		this.namePeriodo = namePeriodo;
		this.fechaInicioPeriodo = fechaInicioPeriodo;
		this.fechaFinPeriodo = fechaFinPeriodo;
		this.fechaFinAnio = fechaFinAnio;
		this.codGrupo = codGrupo;
		this.codItem = codItem;
		this.estado = estado;
	}

	public Integer getIdPeriodo() 																{		return idPeriodo;}
	public void setIdPeriodo(Integer idPeriodo) 												{		this.idPeriodo = idPeriodo;}
	public String getNamePeriodo() 																{		return namePeriodo;}
	public void setNamePeriodo(String namePeriodo) 												{		this.namePeriodo = namePeriodo;}
	public Date getFechaInicioPeriodo() 														{		return fechaInicioPeriodo;}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) 									{		this.fechaInicioPeriodo = fechaInicioPeriodo;}
	public Date getFechaFinPeriodo() 															{		return fechaFinPeriodo;}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) 										{		this.fechaFinPeriodo = fechaFinPeriodo;}
	public Date getFechaFinAnio() 																{		return fechaFinAnio;}
	public void setFechaFinAnio(Date fechaFinAnio) 												{		this.fechaFinAnio = fechaFinAnio;}
	public String getCodGrupo() 																{		return codGrupo;}
	public void setCodGrupo(String codGrupo) 													{		this.codGrupo = codGrupo;}
	public String getCodItem() 																	{		return codItem;}
	public void setCodItem(String codItem) 														{		this.codItem = codItem;}
	public String getUserCrea() 																{		return userCrea;}
	public void setUserCrea(String userCrea) 													{		this.userCrea = userCrea;}
	public Date getFecCrea() 																	{		return fecCrea;}
	public void setFecCrea(Date fecCrea) 														{		this.fecCrea = fecCrea;}
	public String getEstado() 																	{		return estado;}
	public void setEstado(String estado) 														{		this.estado = estado;}

	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}	
	
   
}


