package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "codmod_asig")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "CodigoModular.findAll", query = "SELECT a FROM CodigoModular a") })
@DynamicUpdate(value = true)
public class CodigoModular extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "codmod")
	private String codigoModular;

	@Column(name = "asignado")
	private String estadoAsignado;

	@Column(name = "codooii_asig")
	private String codoiiAsignado;

	@Column(name = "usuario_asig")
	private String usuarioAsignacion;

	@Column(name = "fecha_asig")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAsignacion;

	public CodigoModular() {
	}

	public CodigoModular(Long id) {
		this.id = id;
	}

	public CodigoModular(Long id, String codigoModular, String estadoAsignado, String codoiiAsignado,
			String usuarioAsignacion, Date fechaAsignacion) {
		super();
		this.id = id;
		this.codigoModular = codigoModular;
		this.estadoAsignado = estadoAsignado;
		this.codoiiAsignado = codoiiAsignado;
		this.usuarioAsignacion = usuarioAsignacion;
		this.fechaAsignacion = fechaAsignacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getEstadoAsignado() {
		return estadoAsignado;
	}

	public void setEstadoAsignado(String estadoAsignado) {
		this.estadoAsignado = estadoAsignado;
	}

	public String getCodoiiAsignado() {
		return codoiiAsignado;
	}

	public void setCodoiiAsignado(String codoiiAsignado) {
		this.codoiiAsignado = codoiiAsignado;
	}

	public String getUsuarioAsignacion() {
		return usuarioAsignacion;
	}

	public void setUsuarioAsignacion(String usuarioAsignacion) {
		this.usuarioAsignacion = usuarioAsignacion;
	}

	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodigoModular other = (CodigoModular) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.CodigoModular[ id	=" + id + " ]";
	}

}
