package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Embeddable
public class DreGeoPK extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "CODIGO")
	private String codigo;

	@Column(name = "ID_DISTRITO")
	private String idDistrito;

	public DreGeoPK() {
	}

	public DreGeoPK(String codigo, String idDistrito) {
		this.codigo = codigo;
		this.idDistrito = idDistrito;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		hash += (idDistrito != null ? idDistrito.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DreGeoPK)) {
			return false;
		}
		DreGeoPK other = (DreGeoPK) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		if ((this.idDistrito == null && other.idDistrito != null)
				|| (this.idDistrito != null && !this.idDistrito.equals(other.idDistrito))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.DreGeoPK[ codigo=" + codigo + ", idDistrito=" + idDistrito
				+ " ]";
	}

}
