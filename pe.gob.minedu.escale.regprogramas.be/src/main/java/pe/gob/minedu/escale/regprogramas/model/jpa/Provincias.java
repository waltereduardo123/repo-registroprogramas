package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name="provincias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provincias.findAll", query = "SELECT p FROM Provincias p")})
public class Provincias extends EntidadUtil implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id           
    @Column(name = "ID_PROVINCIA")
    private String idProvincia;
      
    @Column(name="PROVINCIA")
    private String provincia;
    
    @Column(name = "IND_VERNA")
    private Short indVerna;
        
    @Column(name = "POINT_X")
    private BigDecimal pointX;
    
    @Column(name = "POINT_Y")
    private BigDecimal pointY;
    
    @Column(name="ZOOM")
    private Short zoom;
    
    @JoinColumn(name = "ID_REGION", referencedColumnName = "ID_REGION")
    @ManyToOne(fetch = FetchType.EAGER)
    private Regiones regiones;
    
    @OneToMany(mappedBy = "provincias", fetch = FetchType.LAZY)
    private List<Distritos> listaDistrito;

    public Provincias() {
    }

    public Provincias(String idProvincia) {
        this.idProvincia = idProvincia;
    }

    

    public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public Short getIndVerna() {
		return indVerna;
	}

	public void setIndVerna(Short indVerna) {
		this.indVerna = indVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	public Regiones getRegiones() {
		return regiones;
	}

	public void setRegiones(Regiones regiones) {
		this.regiones = regiones;
	}

	@XmlTransient
	public List<Distritos> getListaDistrito() {
		return listaDistrito;
	}

	public void setListaDistrito(List<Distritos> listaDistrito) {
		this.listaDistrito = listaDistrito;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idProvincia != null ? idProvincia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Provincias)) {
            return false;
        }
        Provincias other = (Provincias) object;
        if ((this.idProvincia == null && other.idProvincia != null) || (this.idProvincia != null && !this.idProvincia.equals(other.idProvincia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.gob.minedu.escale.regprogramas.be.Provincias[ idProvincia=" + idProvincia + " ]";
    }
    
}
