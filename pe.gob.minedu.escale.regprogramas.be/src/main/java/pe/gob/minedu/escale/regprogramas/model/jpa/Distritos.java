package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "distritos")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Distritos.findAll", query = "SELECT d FROM Distritos d") })
public class Distritos extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_DISTRITO")
	private String idDistrito;

	@Column(name = "DISTRITO")
	private String distrito;

	@Column(name = "MUNICIP")
	private Short municip;

	@Column(name = "IND_VERNA")
	private Short indVerna;

	@Column(name = "POINT_X")
	private BigDecimal pointX;

	@Column(name = "POINT_Y")
	private BigDecimal pointY;

	@Column(name = "ZOOM")
	private Short zoom;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "distritos", fetch = FetchType.LAZY)
	private List<DreGeo> listaDreGeo;

	@JoinColumn(name = "ID_PROVINCIA", referencedColumnName = "ID_PROVINCIA")
	@ManyToOne(fetch = FetchType.EAGER)
	private Provincias provincias;

	@OneToMany(mappedBy = "distrito", fetch = FetchType.LAZY)
	private List<Solicitud> listaSolicitud;

	@OneToMany(mappedBy = "distrito", fetch = FetchType.LAZY)
	private List<Programa> listaPrograma;

	public Distritos() {
	}

	public Distritos(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public Short getMunicip() {
		return municip;
	}

	public void setMunicip(Short municip) {
		this.municip = municip;
	}

	public Short getIndVerna() {
		return indVerna;
	}

	public void setIndVerna(Short indVerna) {
		this.indVerna = indVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	@XmlTransient
	public List<DreGeo> getListaDreGeo() {
		return listaDreGeo;
	}

	public void setListaDreGeo(List<DreGeo> listaDreGeo) {
		this.listaDreGeo = listaDreGeo;
	}

	public Provincias getProvincias() {
		return provincias;
	}

	public void setProvincias(Provincias provincias) {
		this.provincias = provincias;
	}

	@XmlTransient
	public List<Solicitud> getListaSolicitud() {
		return listaSolicitud;
	}

	public void setListaSolicitud(List<Solicitud> listaSolicitud) {
		this.listaSolicitud = listaSolicitud;
	}

	@XmlTransient
	public List<Programa> getListaPrograma() {
		return listaPrograma;
	}

	public void setListaPrograma(List<Programa> listaPrograma) {
		this.listaPrograma = listaPrograma;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idDistrito != null ? idDistrito.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Distritos)) {
			return false;
		}
		Distritos other = (Distritos) object;
		if ((this.idDistrito == null && other.idDistrito != null)
				|| (this.idDistrito != null && !this.idDistrito.equals(other.idDistrito))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Distritos[ idDistrito=" + idDistrito + " ]";
	}

}
