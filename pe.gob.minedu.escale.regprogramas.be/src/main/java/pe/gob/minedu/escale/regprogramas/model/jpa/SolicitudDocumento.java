package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "det_regpro_sol_doc")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "SolicitudDocumento.findAll", query = "SELECT s FROM SolicitudDocumento s") })
@DynamicUpdate(value = true)
public class SolicitudDocumento extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected SolicitudDocumentoPK solicitudDocumentoPK;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "N_ID_SOLICITUD", referencedColumnName = "N_ID_SOLICITUD", insertable = false, updatable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Solicitud solicitud;

	@JoinColumn(name = "N_ID_DOCUMENTO", referencedColumnName = "N_ID_DOCUMENTO", insertable = false, updatable = false)
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Documento documento;

	public SolicitudDocumento() {
	}

	public SolicitudDocumento(SolicitudDocumentoPK solicitudDocumentoPK) {
		this.solicitudDocumentoPK = solicitudDocumentoPK;
	}

	public SolicitudDocumento(SolicitudDocumentoPK solicitudDocumentoPK, Date fechaCreacion, String usuarioCreacion,
			String estado) {
		this.solicitudDocumentoPK = solicitudDocumentoPK;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public SolicitudDocumento(long nIdDocumento, long nIdSolicitud) {
		this.solicitudDocumentoPK = new SolicitudDocumentoPK(nIdDocumento, nIdSolicitud);
	}

	public SolicitudDocumentoPK getSolicitudDocumentoPK() {
		return solicitudDocumentoPK;
	}

	public void setSolicitudDocumentoPK(SolicitudDocumentoPK solicitudDocumentoPK) {
		this.solicitudDocumentoPK = solicitudDocumentoPK;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (solicitudDocumentoPK != null ? solicitudDocumentoPK.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SolicitudDocumento)) {
			return false;
		}
		SolicitudDocumento other = (SolicitudDocumento) object;
		if ((this.solicitudDocumentoPK == null && other.solicitudDocumentoPK != null)
				|| (this.solicitudDocumentoPK != null
						&& !this.solicitudDocumentoPK.equals(other.solicitudDocumentoPK))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.SolicitudDocumento[ solicitudDocumentoPK=" + solicitudDocumentoPK
				+ " ]";
	}

}
