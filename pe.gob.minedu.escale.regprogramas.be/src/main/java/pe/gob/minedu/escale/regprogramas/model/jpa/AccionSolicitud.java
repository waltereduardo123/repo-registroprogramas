package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.EntidadUtil;
import pe.gob.minedu.escale.common.util.StringUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_accsoli")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "AccionSolicitud.findAll", query = "SELECT a FROM AccionSolicitud a") })
public class AccionSolicitud extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_ACCIONSOLI")
	private Long idAccionSolicitud;

	@JoinColumn(name = "N_ID_TIPSITU")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoSituacionSolicitud;

	@Column(name = "C_OBS")
	private String observacion;
	
	@Column(name = "C_PERFREV")
	private String perfilRevisor;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaultimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "N_ID_SOLICITUD", referencedColumnName = "N_ID_SOLICITUD")
	@ManyToOne(fetch = FetchType.LAZY)
	private Solicitud solicitud;

	public AccionSolicitud() {
	}

	public AccionSolicitud(Long idAccionSolicitud) {
		this.idAccionSolicitud = idAccionSolicitud;
	}

	public AccionSolicitud(Long idAccionSolicitud, Maestro nIdTipaccs, Date dFeccre, String cUsucre, String cEstado) {
		this.idAccionSolicitud = idAccionSolicitud;
		this.tipoSituacionSolicitud = nIdTipaccs;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
	}

	public AccionSolicitud(Long idAccionSolicitud, Maestro tipoSituacionSolicitud, String observacion,
			Date fechaCreacion, String usuarioCreacion, Date fechaultimaModificacion, String usuarioUltimaModificacion,
			String estado, Solicitud solicitud) {
		super();
		this.idAccionSolicitud = idAccionSolicitud;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.observacion = observacion;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaultimaModificacion = fechaultimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.solicitud = solicitud;
	}	

	public AccionSolicitud(Long idAccionSolicitud, String observacion, Date fechaCreacion, String usuarioCreacion,
			String estado) {
		super();
		this.idAccionSolicitud = idAccionSolicitud;
		this.observacion = observacion;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public AccionSolicitud(Long idAccionSolicitud, String observacion, Date fechaCreacion, String usuarioCreacion,
			String estado, Long idTipoSituacionSolicitud, String tipoSituacionSolicitud ) {
		super();
		this.idAccionSolicitud = idAccionSolicitud;
		this.observacion = observacion;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		this.tipoSituacionSolicitud = claseNullAContruir(Maestro.class, this.tipoSituacionSolicitud,
				idTipoSituacionSolicitud);
		this.tipoSituacionSolicitud.setDescripcionMaestro(
				StringUtil.isNotNullOrBlank(tipoSituacionSolicitud) ? tipoSituacionSolicitud : "");
	}
	
	public Long getIdAccionSolicitud() {
		return idAccionSolicitud;
	}

	public void setIdAccionSolicitud(Long idAccionSolicitud) {
		this.idAccionSolicitud = idAccionSolicitud;
	}

	public Maestro getTipoSituacionSolicitud() {
		return tipoSituacionSolicitud;
	}

	public void setTipoSituacionSolicitud(Maestro tipoSituacionSolicitud) {
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaultimaModificacion() {
		return fechaultimaModificacion;
	}

	public void setFechaultimaModificacion(Date fechaultimaModificacion) {
		this.fechaultimaModificacion = fechaultimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public String getPerfilRevisor() {
		return perfilRevisor;
	}

	public void setPerfilRevisor(String perfilRevisor) {
		this.perfilRevisor = perfilRevisor;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idAccionSolicitud != null ? idAccionSolicitud.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AccionSolicitud)) {
			return false;
		}
		AccionSolicitud other = (AccionSolicitud) object;
		if ((this.idAccionSolicitud == null && other.idAccionSolicitud != null)
				|| (this.idAccionSolicitud != null && !this.idAccionSolicitud.equals(other.idAccionSolicitud))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.AccionSolicitud[ idAccionSolicitud=" + idAccionSolicitud + " ]";
	}
	
	@SuppressWarnings({ "unchecked" })
	public <T> T claseNullAContruir(Class<T> clase, Object obj, Object id) {
		T objT = null;
		try {
			if (Objects.isNull(obj)) {
				T clazz = clase.newInstance();
				if (clazz instanceof Maestro) {
					Maestro obj2 = null;
					if (Objects.isNull(id)) {
						obj2 = new Maestro(0L);
					} else {
						obj2 = new Maestro((Long) id);
					}
					objT = (T) obj2;
				}
			} else {
				objT = (T) obj;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return objT;
	}
}
