package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_documento")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d") })
@DynamicUpdate(value = true)
public class Documento extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_DOCUMENTO")
	private Long idDocumento;

	@JoinColumn(name = "N_ID_TIPDOCU")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoDocumento;

	@JoinColumn(name = "N_ID_TIPRESO")
	@OneToOne(fetch = FetchType.EAGER)
	private Maestro tipoDocumentoResolucion;

	@Column(name = "C_NRODOC")
	private String nroDocumento;

	@Column(name = "D_FECDOCU")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaDocumento;

	@Column(name = "C_CODDOCU")
	private String codigoDocumento;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@Column(name = "CODOOII")
	private String codooii;

	@Transient
	private String indCambioArchivos;

	@Transient
	private String indCambioArchivoCroquis;

	@Transient
	private String indCambioDocumentoExistente;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "documento", fetch = FetchType.LAZY)
	private List<SolicitudDocumento> listaSolicitudDocumento;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "documento", fetch = FetchType.LAZY)
	private List<Archivo> listaArchivo;

	public Documento() {
	}

	public Documento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public Documento(Long idDocumento, Date fechaCreacion, String usuarioCreacion, String estado, String codooii,
			String codigoDocumento) {
		this.idDocumento = idDocumento;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		this.codooii = codooii;
		this.codigoDocumento = codigoDocumento;
	}

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public Maestro getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Maestro tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodooii() {
		return codooii;
	}

	public void setCodooii(String codooii) {
		this.codooii = codooii;
	}

	public Maestro getTipoDocumentoResolucion() {
		return tipoDocumentoResolucion;
	}

	public void setTipoDocumentoResolucion(Maestro tipoDocumentoResolucion) {
		this.tipoDocumentoResolucion = tipoDocumentoResolucion;
	}

	@XmlTransient
	public List<Archivo> getListaArchivo() {
		return listaArchivo;
	}

	public void setListaArchivo(List<Archivo> listaArchivo) {
		this.listaArchivo = listaArchivo;
	}

	@XmlTransient
	public List<SolicitudDocumento> getListaSolicitudDocumento() {
		return listaSolicitudDocumento;
	}

	public void setListaSolicitudDocumento(List<SolicitudDocumento> listaSolicitudDocumento) {
		this.listaSolicitudDocumento = listaSolicitudDocumento;
	}

	public String getIndCambioArchivos() {
		return indCambioArchivos;
	}

	public void setIndCambioArchivos(String indCambioArchivos) {
		this.indCambioArchivos = indCambioArchivos;
	}

	public String getIndCambioArchivoCroquis() {
		return indCambioArchivoCroquis;
	}

	public void setIndCambioArchivoCroquis(String indCambioArchivoCroquis) {
		this.indCambioArchivoCroquis = indCambioArchivoCroquis;
	}

	public String getIndCambioDocumentoExistente() {
		return indCambioDocumentoExistente;
	}

	public void setIndCambioDocumentoExistente(String indCambioDocumentoExistente) {
		this.indCambioDocumentoExistente = indCambioDocumentoExistente;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idDocumento != null ? idDocumento.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Documento)) {
			return false;
		}
		Documento other = (Documento) object;
		if ((this.idDocumento == null && other.idDocumento != null)
				|| (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Documento[ idDocumento=" + idDocumento + " ]";
	}

}
