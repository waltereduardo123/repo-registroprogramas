package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "dre_geo")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "DreGeo.findAll", query = "SELECT d FROM DreGeo d") })
public class DreGeo  extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	protected DreGeoPK dreGeoPK;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "ID_DISTRITO", referencedColumnName = "ID_DISTRITO", insertable = false, updatable = false)
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Distritos distritos;

	@JoinColumn(name = "CODIGO", referencedColumnName = "CODIGO", insertable = false, updatable = false)
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private DreUgel dreUgel;

	public DreGeo() {
	}

	public DreGeo(DreGeoPK dreGeoPK) {
		this.dreGeoPK = dreGeoPK;
	}

	public DreGeo(DreGeoPK dreGeoPK, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado, Distritos distritos, DreUgel dreUgel) {
		super();
		this.dreGeoPK = dreGeoPK;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.distritos = distritos;
		this.dreUgel = dreUgel;
	}

	public DreGeoPK getDreGeoPK() {
		return dreGeoPK;
	}

	public void setDreGeoPK(DreGeoPK dreGeoPK) {
		this.dreGeoPK = dreGeoPK;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Distritos getDistritos() {
		return distritos;
	}

	public void setDistritos(Distritos distritos) {
		this.distritos = distritos;
	}

	public DreUgel getDreUgel() {
		return dreUgel;
	}

	public void setDreUgel(DreUgel dreUgel) {
		this.dreUgel = dreUgel;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dreGeoPK != null ? dreGeoPK.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof DreGeo)) {
			return false;
		}
		DreGeo other = (DreGeo) object;
		if ((this.dreGeoPK == null && other.dreGeoPK != null)
				|| (this.dreGeoPK != null && !this.dreGeoPK.equals(other.dreGeoPK))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.b.DreGeo[ dreGeoPK=" + dreGeoPK + " ]";
	}

}
