package pe.gob.minedu.escale.regprogramas.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.DynamicUpdate;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
@Entity
@Table(name = "tbl_regpro_archivo")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Archivo.findAll", query = "SELECT a FROM Archivo a") })
@DynamicUpdate(value = true)
public class Archivo extends EntidadUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "N_ID_ARCHIVO")
	private Long idArchivo;

	@JoinColumn(name = "N_ID_TIPARCH")
	@OneToOne(fetch = FetchType.LAZY)
	private Maestro tipoArchivo;

	@Column(name = "C_NOMARCH")
	private String nombreArchivo;

	@Column(name = "C_CODARCH")
	private String codigoArchivo;

	@Column(name = "D_FECCRE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "C_USUCRE")
	private String usuarioCreacion;

	@Column(name = "D_FECUMO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimaModificacion;

	@Column(name = "C_USUUMO")
	private String usuarioUltimaModificacion;

	@Column(name = "C_ESTADO")
	private String estado;

	@JoinColumn(name = "N_ID_DOCUMENTO", referencedColumnName = "N_ID_DOCUMENTO")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Documento documento;

	public Archivo() {
	}

	public Archivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public Archivo(Long idArchivo, Maestro tipoArchivo, String nombreArchivo, Date fechaCreacion,
			String usuarioCreacion, String estado) {
		this.idArchivo = idArchivo;
		this.tipoArchivo = tipoArchivo;
		this.nombreArchivo = nombreArchivo;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}
	
	public Archivo(Long idArchivo, String nombreArchivo, String codigoArchivo, Date fechaCreacion,
			String usuarioCreacion, String estado) {
		this.idArchivo = idArchivo;
		this.nombreArchivo = nombreArchivo;
		this.codigoArchivo = codigoArchivo;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public Maestro getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(Maestro tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getCodigoArchivo() {
		return codigoArchivo;
	}

	public void setCodigoArchivo(String codigoArchivo) {
		this.codigoArchivo = codigoArchivo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@XmlTransient
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idArchivo != null ? idArchivo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Archivo)) {
			return false;
		}
		Archivo other = (Archivo) object;
		if ((this.idArchivo == null && other.idArchivo != null)
				|| (this.idArchivo != null && !this.idArchivo.equals(other.idArchivo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.be.Archivo[ idArchivo=" + idArchivo + " ]";
	}

}
