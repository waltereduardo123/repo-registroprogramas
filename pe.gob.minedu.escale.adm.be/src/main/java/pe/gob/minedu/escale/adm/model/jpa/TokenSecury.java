package pe.gob.minedu.escale.adm.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

/**
 * Entity implementation class for Entity: TokenSecury
 * 
 */
@Entity
@Table(name = "tbl_adm_tksecury")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "TokenSecury.findAll", query = "SELECT a FROM TokenSecury a") })
@DynamicUpdate(value = true)
public class TokenSecury implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "N_ID_TOKEN")
	private Long idToken;

	@Basic(optional = false)
	@Column(name = "C_TOKEN")
	private String token;

	public TokenSecury() {
		super();
	}

	public TokenSecury(Long idToken, String token) {
		super();
		this.idToken = idToken;
		this.token = token;
	}

	public Long getIdToken() {
		return idToken;
	}

	public void setIdToken(Long idToken) {
		this.idToken = idToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
