package pe.gob.minedu.escale.adm.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

/**
 * Entity implementation class for Entity: Clave
 *
 */

@Entity
@Table(name = "tbl_adm_clave_hist")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Clave.findAll", query = "SELECT c FROM Clave c"),
				@NamedQuery(name = "Clave.findByUsuario", query = "SELECT c FROM Clave c WHERE c.codigo = :codigo")
		})
@DynamicUpdate(value = true)
public class Clave implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="N_ID_CLAVE")
	private Long idClave;
	
	
	@Basic(optional = false)
	@Column(name="C_CODIGO")
	private String codigo;  
	
	@Basic(optional = false)
	@Column(name="C_CLAVE")
	private String clave;
	
	
	@Basic(optional = false)
	@Column(name="C_TIEMPO_VIGENCIA")
	private Integer tiempoVigencia;
	
	@Basic(optional = false)
	@Column(name="C_USUCREA")
	private String usuCrea;
	
	
	@Basic(optional = false)
	@Column(name="D_FECCREA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecCrea;
	
	/** La clave. */
	@Transient
	private String keyClave;
	
	public Clave() {
		super();
	}



	public Clave(Long idClave, String codigo, String clave, Integer tiempoVigencia, String usuCrea, Date fecCrea,
			String keyClave) {
		super();
		this.idClave = idClave;
		this.codigo = codigo;
		this.clave = clave;
		this.tiempoVigencia = tiempoVigencia;
		this.usuCrea = usuCrea;
		this.fecCrea = fecCrea;
		this.keyClave = keyClave;
	}



	public Integer getTiempoVigencia() {
		return tiempoVigencia;
	}

	public void setTiempoVigencia(Integer tiempoVigencia) {
		this.tiempoVigencia = tiempoVigencia;
	}

	public Clave(Long idClave, String codigo, String clave, String usuCrea, Date fecCrea, String keyClave) {
		super();
		this.idClave = idClave;
		this.codigo = codigo;
		this.clave = clave;
		this.usuCrea = usuCrea;
		this.fecCrea = fecCrea;
		this.keyClave = keyClave;
	}





	public String getKeyClave() {
		return keyClave;
	}

	public void setKeyClave(String keyClave) {
		this.keyClave = keyClave;
	}

	public Long getIdClave() {
		return idClave;
	}

	public void setIdClave(Long idClave) {
		this.idClave = idClave;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getUsuCrea() {
		return usuCrea;
	}

	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}

	public Date getFecCrea() {
		return fecCrea;
	}

	public void setFecCrea(Date fecCrea) {
		this.fecCrea = fecCrea;
	}
	
	
	
	
   
}
