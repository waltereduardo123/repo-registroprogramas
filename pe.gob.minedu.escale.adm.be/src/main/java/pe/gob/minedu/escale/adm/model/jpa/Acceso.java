package pe.gob.minedu.escale.adm.model.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.DynamicUpdate;

/**
 * Entity implementation class for Entity: Sesion
 *
 */
@Entity
@Table(name = "tbl_adm_acceso")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Acceso.findAll", query = "SELECT a FROM Acceso a"),
				@NamedQuery(name = "Acceso.findByUsuario", query = "SELECT a FROM Acceso a WHERE a.usuario = :usuario"),
				@NamedQuery(name = "Acceso.findByFecFinSesion", query = "SELECT a FROM Acceso a WHERE a.fecFinSesion = :fecFinSesion")
		})
@DynamicUpdate(value = true)
public class Acceso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name="N_ID_SESION")
	private Long idSesion;
	
	@Basic(optional = false)
	@Column(name="C_NAVEGADOR")
	private String navegador;  
	
	@Basic(optional = false)
	@Column(name="C_USUARIO")
	private String usuario;
	
	@Basic(optional = false)
	@Column(name="C_TOKEN")
	private String token;
	
	@Basic(optional = false)
	@Column(name="HOST_CLIENTE")
	private String hostcliente;  
	
	@Basic(optional = false)
	@Column(name="IND_ACC")
	private String indAcc;
	
	@Basic(optional = false)
	@Column(name="D_FEC_INISESION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecIniSesion;
	
	
	@Basic(optional = false)
	@Column(name="D_FEC_FINSESION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecFinSesion;
	
	
	@Basic(optional = false)
	@Column(name = "C_USUCREA")
	private String usuarioCreacion;
	
	@Basic(optional = false)
	@Column(name = "D_FECCREA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;
	
	@Basic(optional = false)
	@Column(name = "C_ESTADO")
	private String estado;
	
	public Acceso() {
		super();
	}
	

	public Acceso(Long idSesion, String navegador, String usuario, String token, String hostcliente, String indAcc,
			Date fecIniSesion, Date fecFinSesion, String usuarioCreacion, Date fechaCreacion, String estado) {
		super();
		this.idSesion = idSesion;
		this.navegador = navegador;
		this.usuario = usuario;
		this.token = token;
		this.hostcliente = hostcliente;
		this.indAcc = indAcc;
		this.fecIniSesion = fecIniSesion;
		this.fecFinSesion = fecFinSesion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaCreacion = fechaCreacion;
		this.estado = estado;
	}













	public Long getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(Long idSesion) {
		this.idSesion = idSesion;
	}

	public String getNavegador() {
		return navegador;
	}

	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getFecIniSesion() {
		return fecIniSesion;
	}

	public void setFecIniSesion(Date fecIniSesion) {
		this.fecIniSesion = fecIniSesion;
	}

	public Date getFecFinSesion() {
		return fecFinSesion;
	}

	public void setFecFinSesion(Date fecFinSesion) {
		this.fecFinSesion = fecFinSesion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}



	public String getHostcliente() {
		return hostcliente;
	}



	public void setHostcliente(String hostcliente) {
		this.hostcliente = hostcliente;
	}


	public String getIndAcc() {
		return indAcc;
	}


	public void setIndAcc(String indAcc) {
		this.indAcc = indAcc;
	}
	
	
	
	
   
}
