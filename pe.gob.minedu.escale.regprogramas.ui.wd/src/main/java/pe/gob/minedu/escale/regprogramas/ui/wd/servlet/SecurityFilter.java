package pe.gob.minedu.escale.regprogramas.ui.wd.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import javax.ws.rs.ext.Provider;


@Provider
public class SecurityFilter implements ContainerRequestFilter, ContainerResponseFilter {
	
	/** Used for Script Nonce */
	private SecureRandom prng = null;
	
	@SuppressWarnings("unused")
	private static final String EXPIRE_ERROR_MSG = "El token a expirado", JWT_ERROR_MSG = "No se puede analizar el JWT",
			JWT_INVALID_MSG = "Invalido JWT token";

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext response) throws IOException {
		
		try {
			this.prng = SecureRandom.getInstance("SHA1PRNG");
	
			// Add Script Nonce CSP Policy
			// --Generate a random number
		//String randomNum = new Integer(this.prng.nextInt()).toString();
			String randomNumext=Integer.toHexString(new Integer(this.prng.nextInt()));
		//String randomNum =new Double(this.prng.nextGaussian()).toString();	
			
			String csp =  "frame-src 'self' https://www.google.com/recaptcha/ ;"+
					"style-src 'self' 'unsafe-inline' ;"+
					"font-src 'self'; "+
					"img-src 'self'; "+
					"media-src; "+
					"base-uri 'none'; "+
					"default-src 'nonce-"+ randomNumext +"' 'unsafe-inline' 'strict-dynamic' https: http:; "+
					"report-uri *://*.google.com https://www.bing.com/api/maps/mapcontrol?branch=release https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "+
					"connect-src 'self' ; "+
					"object-src 'self' 'unsafe-inline' ; "+
					"script-src 'self' https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "
					;
//		response.getHeaders().add( "X-FRAME-OPTIONS", "DENY" );
//		response.getHeaders().add( "X-Content-Type-Options", "nosniff" );
//		response.getHeaders().add( "Content-Security-Policy", "default-src 'self'; font-src; frame-src; img-src; media-src; object-src; report-uri; sandbox; script-src; style-src; upgrade-insecure-requests; connect-src 'none'; upgrade-insecure-requests" );
		response.getHeaders().add( "Content-Security-Policy", csp );
//		response.getHeaders().add( "X-XSS-Protection", "1; mode=block" );
		response.getHeaders().add( "Set-Cookie", "key=value; HttpOnly; SameSite=strict" );
		
		
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		// TODO Auto-generated method stub
		
	}


}
