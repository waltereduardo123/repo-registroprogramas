package pe.gob.minedu.escale.adm.wd.utils;

public class ConstanteWeb {

	/** Metodo Constructor */
	public ConstanteWeb() {}
	
	public static final String REPOSITORIO_DATOS = "repositorioDatos";
	
	/** RUTAS DE PROPIEDAEDS UI*/
	public static final String CODIGO_SELECCIONE = "";
	public static final Integer VALOR_COMBO=0;
	
	public static final String VALOR_SELECCIONE = "      -SELECCIONE-      ";
	public static final String VALOR_TODOS = " ---TODOS---      ";
	public static final String PROPERTIES="escale-ui";
	/** RUTAS DE RECURSOS EXTERNOS*/
	public static final String PROPERTIES_FS="resources/escale-recursos";
	public static final String FS_CSS="escale.ui.filesystem.recursos.css";
	public static final String FS_JS="escale.ui.filesystem.recursos.js";
	public static final String FS_IMAGENES="escale.ui.filesystem.recursos.imagenes";
	
	
	/**	RUTA DE ARCHIVOS DPF **/
	public static final String FS_PERSONALES="escale.ui.filesystem.recursos.pdf";
	public static final String REPORTE_PDF="reportePDF";
	
	
	public static final Integer TIPO_USUARIO_INTERNO=1;
	
	
	/** Objectos en sesion **/
	public static final String CAPACITACION="detallesCapacitacionDTO";

	public static final String USUARIO="usuario";
	
	
	
	/** Vavlores Estaticos*/
	public static final String VALOR_CERO = "0";
	public static final String VALOR_UNO = "1";
	
	/** Opciones SI No*/
	public static final String ETIQUETA_SI = "Si";
	public static final String VALOR_SI = "1";
	public static final String ETIQUETA_NO = "No";
	public static final String VALOR_NO = "0";
	
	
	/** Codigo captcha*/
	public static final String CODIGO_CAPTCHA_NOCOICIDE="escale.ui.codigo.captcha";
	public static final String CODIGO_CAPTCHA_CORRECTO="escale.ui.codigo.captcha.correcto";
	public static final String CODIGO_CAPTCHA_INCORRECTO="escale.ui.codigo.captcha.incorrecto";
	
	
}
