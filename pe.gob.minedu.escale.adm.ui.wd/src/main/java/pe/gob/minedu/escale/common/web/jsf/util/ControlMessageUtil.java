package pe.gob.minedu.escale.common.web.jsf.util;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import pe.gob.minedu.escale.adm.vo.ErrorVO;
import pe.gob.minedu.escale.common.business.ErrorService;
import pe.gob.minedu.escale.common.exception.BusinessException;
import pe.gob.minedu.escale.common.util.LogUtil;
import pe.gob.minedu.escale.resources.LanguageResources;


/**
 *
 * Clase Utilitaria para el manejo de excepciones y mensajes en los ManagedBean.
 *
 */
public class ControlMessageUtil implements Serializable {
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La Constante GENERIC_MESSAGE. */
	private static final String GENERIC_MESSAGE = "adm.exception.generic";
		
	/** El error service. */
	private ErrorService errorService;
	
	/** EL objeto  class. */
	@SuppressWarnings("rawtypes")
	private Class clazz;
	
	/** El  log util class. */
	private LogUtil logClass;
	
	/** El  log util message. */
	@SuppressWarnings("unused")
	private LogUtil logMessage = new LogUtil(ControlMessageUtil.class.getName());
	
	/** Properties Message **/
	private static final String propertiesMessage = "resources.escale-messages-resources";
	
	/**
	 * Constructor de la clase ControMessageUtil.
	 *
	 * @param errorService the error service
	 * @param clazz the clazz
	 */
	@SuppressWarnings("rawtypes")
	public ControlMessageUtil(ErrorService errorService, Class clazz) {
		this.errorService = errorService;
		this.clazz = clazz;
		logClass = new LogUtil(this.clazz.getName());
	}
	
	/**
	 * Sirve para imprimir mensajes y uso del Log.
	 *
	 * @param exception the exception
	 * @param severity the severity
	 * @return mensaje
	 */
	public String imprimirMessage(Exception exception, Severity severity) {
		String mensajeGenerico = "";
		if (exception != null && severity != null) {
			if (isBusinessException(exception)) {
				ErrorVO errorVO = errorService.getErrorFor(exception, FacesUtil.getLocale());
				mensajeGenerico = errorVO.getDefaultMessage();
				FacesMessageUtil.addMessage(FacesContext.getCurrentInstance(),
						severity, errorVO.getDefaultMessage());
				imprimirLog(mensajeGenerico, exception, severity);
			} else {
				mensajeGenerico = getMessage(GENERIC_MESSAGE);
				FacesMessageUtil.addMessage(FacesContext.getCurrentInstance(),
						severity, mensajeGenerico);
				imprimirLog(mensajeGenerico, exception, severity);
			}
		}
		return mensajeGenerico;
	}
	
	/**
	 * Sirve para imprimir mensajes y uso del Log.
	 *
	 * @param mensaje the mensaje
	 * @param exception the exception
	 * @param severity the severity
	 */
	public void imprimirMessage(String mensaje, Exception exception, Severity severity) {
		if (exception != null && severity != null) {
			FacesMessageUtil.addMessage(FacesContext.getCurrentInstance(),
					severity, mensaje);
			imprimirLog(mensaje, exception, severity);
		}
	}
	
	/**
	 * Sirve para imprimir mensajes y uso del Log.
	 *
	 * @param mensaje the mensaje
	 * @param severity the severity
	 */
	public void imprimirMessage(String mensaje, Severity severity) {
		FacesMessageUtil.addMessage(FacesContext.getCurrentInstance(),
				severity, mensaje);
		imprimirLog(mensaje, severity);
	}
	
	/**
	 * Sirve para imprimir mensajes(A partir de un clave del properties escale-messages-resources.properties)
	 * y uso del Log.
	 *
	 * @param key the key
	 * @param severity the severity
	 * @return mensaje
	 */
	public String imprimirMessageClave(String key, Severity severity) {
		String mensaje = getMessage(key);
		FacesMessageUtil.addMessage(FacesContext.getCurrentInstance(),
				severity, mensaje);
		imprimirLog(mensaje, severity);                
		return mensaje;
	}
	
	/**
	 * Sirve para imprimir mensajes(A partir de un clave del properties escale-messages-resources.properties)
	 * y uso del Log.
	 *
	 * @param key the key
	 * @param severity the severity
	 * @param parametros the parametros
	 * @return the string
	 */
	public String imprimirMessageClave(String key, Severity severity, String... parametros) {
		String mensaje = getMessage(key, parametros);
		FacesMessageUtil.addMessage(FacesContext.getCurrentInstance(),
				severity, mensaje);
		imprimirLog(mensaje, severity);
		return mensaje;
	}
	
	/**
	 * Imprimir log.
	 *
	 * @param mensaje the mensaje
	 * @param exception the exception
	 * @param severity the severity
	 */
	public void imprimirLog(String mensaje, Exception exception, Severity severity) {
		if (severity.equals(FacesMessage.SEVERITY_ERROR)) {
			logClass.error(FacesUtil.getContext(), mensaje, exception);
		} else if (severity.equals(FacesMessage.SEVERITY_WARN)) {
			logClass.warn(FacesUtil.getContext(), mensaje, exception);
		} else if (severity.equals(FacesMessage.SEVERITY_INFO)) {
			logClass.info(FacesUtil.getContext(), mensaje, exception);
		}
	}
	
	/**
	 * Imprimir log.
	 *
	 * @param mensaje the mensaje
	 * @param severity the severity
	 */
	public void imprimirLog(String mensaje, Severity severity) {
		if (severity.equals(FacesMessage.SEVERITY_ERROR)) {
			logClass.error(FacesUtil.getContext(), mensaje);
		} else if (severity.equals(FacesMessage.SEVERITY_WARN)) {
			logClass.warn(FacesUtil.getContext(), mensaje);
		} else if (severity.equals(FacesMessage.SEVERITY_INFO)) {
			logClass.info(FacesUtil.getContext(), mensaje);
		}
	}
	
	/**
	 * Imprimir log.
	 *
	 * @param ex el ex
	 * @param severity el severity
	 */
	public void imprimirLog(Exception ex, Severity severity) {
		if (severity.equals(FacesMessage.SEVERITY_ERROR)) {
			logClass.error(FacesUtil.getContext(), ex);
		} else if (severity.equals(FacesMessage.SEVERITY_WARN)) {
			logClass.warn(FacesUtil.getContext(), ex);
		} else if (severity.equals(FacesMessage.SEVERITY_INFO)) {
			logClass.info(FacesUtil.getContext(), ex);
		}
	}
	
	/**
	 * Verifica si el error es de tipo BusinessException.
	 *
	 * @param object the object
	 * @return true, if is business exception
	 */
	@SuppressWarnings("unused")
	private boolean isBusinessException(Object object) {
		try {
        	BusinessException be = (BusinessException) object;
        	return true;
        } catch (Exception e) {
        	return false;
        }
	}
	
	/**
	 * Obtiene el message a partir un key del properties.
	 *
	 * @param key el key
	 * @return message
	 */
	public String getMessage(String key) {
		return getMessageResource(key);
	}
	
	/**
	 * Obtiene el message a partir un key del properties.
	 *
	 * @param key el key
	 * @param parametro el parametro
	 * @return message
	 */
	public String getMessage(String key, String parametro) {
		return getMessageResource(key, parametro);
	}
	
	/**
	 * Obtiene el message a partir un key del properties.
	 *
	 * @param key el key
	 * @param parametros el parametros
	 * @return message
	 */
	public String getMessage(String key, String... parametros) {
		return getMessageResource(key, parametros);
	}
	
	
	/**
	 * Obtiene message resource.
	 *
	 * @param key el key
	 * @return message resource
	 */
	private String getMessageResource(String key) {
		return getLanguageResources().getString(key);
	}
	
	/**
	 * Obtiene message resource.
	 *
	 * @param key el key
	 * @param parametro el parametro
	 * @return message resource
	 */
	private String getMessageResource(String key, String parametro) {
		return getLanguageResources().getString(key, parametro);
	}
	
	/**
	 * Obtiene message resource.
	 *
	 * @param key el key
	 * @param parametros el parametro
	 * @return message resource
	 */
	private String getMessageResource(String key, String... parametros) {
		return getLanguageResources().getString(key, parametros);
	}
	
	/**
	 * Obtiene resource.
	 *
	 * @return resource
	 */
	private LanguageResources getLanguageResources() {
		ResourceBundle resource;
		Locale locale = FacesUtil.getLocale();
		if (locale != null) {
			resource = ResourceBundle.getBundle(propertiesMessage, locale);
		} else {
			resource = ResourceBundle.getBundle(propertiesMessage);
		}
		return LanguageResources.getInstance(resource);
	}
}