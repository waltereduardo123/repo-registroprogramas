package pe.gob.minedu.escale.adm.web.util;

/**
 * Constantes de Mensajes Administracion.
 */
public class ConstantesUtil {
	
	public static final String MENSAJE_NRO_TRAMITE = "adm.message.nrotramite";
	public static final String MENSAJE_EXITO_GUARDAR = "adm.message.confirmacion.guardar";
	public static final String MENSAJE_EXITO_MODIFICAR_CLAVE_TEMPORAL = "adm.message.confirmacion.modificarClaveTemporal";
	public static final String MENSAJE_ALERTA_CAMBIO_CONTRASENHA_INICIAL = "adm.label.modificarclave.diasAlertaCambioContrasenaInicial";
	public static final String MENSAJE_REQUISITOS_CLAVE = "adm.label.modificarclave.requisitosNuevaContrasena";
	public static final String MENSAJE_REQUISITOS_CLAVE_INICIAL = "adm.label.modificarclave.requisitosNuevaContrasenaInicial";
	public static final String MENSAJE_EXITO_RECUPERAR_CLAVE = "adm.message.confirmacion.recuperarClave";
	public static final String MENSAJE_EXITO_RECUPERAR_CLAVE_BLOQUEADO = "adm.message.aviso.recuperarClaveBloqueado";
	public static final String MENSAJE_EXITO_ELIMINAR = "adm.message.confirmacion.eliminar";
	public static final String MENSAJE_EXITO_MODIFICAR = "adm.message.confirmacion.modifico";
	public static final String MENSAJE_LISTA_VACIA = "adm.grilla.accion.vacia";
	public static final String MENSAJE_EJM_ENTERO = "adm.ejm.entero";
	public static final String MENSAJE_EJM_DECIMAL = "adm.ejm.decimal";
	public static final String MENSAJE_EJM_TEXTO = "adm.ejm.texto";
	public static final String MENSAJE_EJM_FECHA = "adm.ejm.fecha";
	public static final String MENSAJE_EJM_FECHA_HORA = "adm.ejm.fechaHora";
	public static final String MENSAJE_FECHA = "common.message.fechaIniciomayorfechaFin";
	public static final String MENSAJE_FECHAD = "common.message.fechaDesdemayorfechaHasta";
	public static final String MENSAJE_FECHA_NECESARIO = "common.message.fecha.necesario";
	public static final String MENSAJE_ASIGNACION_LISTA_VACIA_ROL = "adm.message.asignacion.lista.vacia.rol";
	public static final String MENSAJE_ASIGNACION_LISTA_VACIA_PRIVILEGIO = "adm.message.asignacion.lista.vacia.privilegio";
	public static final String MENSAJE_SELECCION_REGISTRO = "adm.message.seleccion.registro";
	public static final String MENSAJE_ARCHIVO_EXTENCION_INVALIDA = "adm.piedepagina.message.archivoExtensionInvalida";
	public static final String MENSAJE_ROL_ASOCIADO_ENTIDAD_ACTIVO = "adm.rol.asociado.entidad.activo";
	public static final String MENSAJA_PERFIL_ASOCIADO_ENTIDAD_ACTIVO = "adm.perfil.asociado.entidad.activo" ;
	public static final String MENSAJE_SELECCIONAR_LUPA = "adm.validar.persona.existente";
	public static final String DNI_EXISTE = "adm.dni.existe";
	public static final String MENSAJE_EXITO_REGISTRO_ACCESODIRECTO = "adm.accesodirecto.message.registroAccesoDirecto";
	public static final String MENSAJE_EXITO_MODIFICACION_ACCESODIRECTO = "adm.accesodirecto.message.modificacionAccesoDirecto";
	public static final String MENSAJE_EXITO_ASIGNAR_ACCESODIRECTO = "adm.accesodirecto.message.asignarAccesos";
	public static final String MENSAJE_ERROR_TAMANIO_IMAGEN = "adm.accesodirecto.message.tamanio";
	public static final String MENSAJE_ERROR_ACCESO_ASIGNADO = "adm.accesodirecto.message.accesoAsignado";
	public static final String MENSAJE_ERROR_CAMPO_OBLIGATORIO = "adm.message.campo.obligatorio";
	public static final String MENSAJE_ERROR_TIEMPO_MINUTOS = "adm.message.parametro.tiempo.minutos";
	public static final String MENSAJE_ERROR_TIEMPO_SEGUNDOS = "adm.message.parametro.tiempo.segundos";
	public static final String MENSAJE_ERROR_MINIMO_UN_CAMPO_OBLIGATORIO = "adm.message.minimo.campo.obligatorio";
	public static final String MENSAJE_SAVE_EXITO = "adm.message.save.exito";
	public static final String MENSAJE_ERROR_FORMATO_ENTERO = "adm.message.error.formato.entero";
	public static final String MENSAJE_ERROR_FORMATO_DECIMAL = "adm.message.error.formato.decimal";
	public static final String MENSAJE_ERROR_FORMATO_DECIMAL_ERROR = "adm.message.error.formato.decimal.negativo";
	public static final String MENSAJE_ERROR_FORMATO_TOTAL_PERSONAS = "adm.message.error.total.personas";
	public static final String MENSAJE_ERROR_UNCAMPO_NECESARIO = "adm.message.uncampo.necesario";
	public static final String MENSAJE_ERROR_UN_ROL = "adm.accesodirecto.message.unrol";
	public static final String MENSAJE_ERROR_UN_ACD = "adm.accesodirecto.message.unaccesodirecto";
	public static final String MENSAJE_ERROR_UN_REGISTRO = "adm.message.unregistro";
	public static final String MENSAJE_ERROR_UN_COMBO = "adm.message.uncombo";
	public static final String MENSAJE_ERROR_ACCESO_DIRECTO_FECHA = "adm.accesodirecto.label.fechaaccion";
	public static final String MENSAJE_FALTA_ICONO = "adm.accesodirecto.message.faltaIcono";
	public static final String MENSAJE_ARCHIVO_INV = "adm.accesodirecto.message.archivoInvalido";
	public static final String MENSAJE_TAMANIO_ICONO = "adm.accesodirecto.message.tamanioExcedido";
	public static final String MENSAJE_EXTENSION_ICONO = "adm.accesodirecto.message.extensionIcono";
	public static final String ROL_ADMINISTRADOR_PORTAL  = "ADMINISTRADOR PORTAL";
	public static final String MENSAJE_NOMBRE_YA_REGISTRADO = "adm.message.nombre.existe";
	public static final String MENSAJE_CAMPO_OBLIGATORIO_HISTORICO_ENTIDAD = "adm.message.campo.obligatorio";
	public static final String MENSAJE_BORRADOR_ENTIDAD_ELIMINADO = "adm.message.entidad.borrador.eliminado";
	public static final String ERROR_NO_EXISTE_BORRADOR = "adm.message.entidad.noexiste.borrador";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_CONTACTO = "adm.message.entidad.contacto.obligatorio";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_PRESUPUESTALES = "adm.message.entidad.presupuestales.obligatorio";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_CLASIFICACION = "adm.message.entidad.clasificacion.obligatorio";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_PERSONAL = "adm.message.entidad.personal.obligatorio";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_ADICIONAL = "adm.message.entidad.adicional.obligatorio";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_ADMINISTRADOR = "adm.message.entidad.administrador.obligatorio";
	public static final String ERROR_NOMBRE_ARCHIVO_REPETIDO = "adm.message.entidad.personal.nombreArchivoRepetido";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_PERFIL = "adm.message.entidad.perfil.obligatorio";
	public static final String ERROR_FORMATO_ENTIDAD_GENERAL = "adm.message.entidad.general.formato";
	public static final String ERROR_FORMATO_ENTIDAD_CONTACTO = "adm.message.entidad.contacto.formato";
	public static final String ERROR_FORMATO_ENTIDAD_PRESUPUESTALES = "adm.message.entidad.presupuestales.formato";
	public static final String ERROR_FORMATO_ENTIDAD_CLASIFICACION = "adm.message.entidad.clasificacion.formato";
	public static final String ERROR_FORMATO_ENTIDAD_PERSONAL = "adm.message.entidad.personal.formato";
	public static final String ERROR_FORMATO_ENTIDAD_ADICIONAL = "adm.message.entidad.adicional.formato";
	public static final String EXITO_ENTIDAD_GENERAL = "adm.message.entidad.general.exito";
	public static final String EXITO_ENTIDAD_CONTACTO = "adm.message.entidad.contacto.exito";
	public static final String EXITO_ENTIDAD_PRESUPUESTALES = "adm.message.entidad.presupuestales.exito";
	public static final String EXITO_ENTIDAD_CLASIFICACION = "adm.message.entidad.clasificacion.exito";
	public static final String EXITO_ENTIDAD_PERSONAL = "adm.message.entidad.personal.exito";
	public static final String EXITO_ENTIDAD_ADICIONAL = "adm.message.entidad.adicional.exito";
	public static final String EXITO_ENTIDAD_REGISTRO = "adm.message.entidad.registro.exito";
	public static final String ERROR_ENTIDAD_REGISTRO = "adm.message.entidad.registro.error";
	public static final String ERROR_ENTIDAD_REGISTRO_ADMINISTRADOR = "adm.message.entidad.registro.admin.error";
	public static final String EXITO_ENTIDAD_MODIFICACION = "adm.message.entidad.modificacion.exito";
	public static final String ERROR_ENTIDAD_MODIFICACION = "adm.message.entidad.modificacion.error";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_CRITERIA = "adm.message.entidad.criteria.onligatorio";
	public static final String MENSAJE_ARCHIVO_INVALIDO = "adm.message.entidad.archivoInvalido";
	public static final String MENSAJE_ARCHIVO_EXCEDIDO = "adm.message.entidad.archivoExcedido";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA_NOTIFICACION = "adm.message.entidad.archivoExtensionInvalidaNotificacion";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA = "adm.message.entidad.archivoExtensionInvalida";
	public static final String MENSAJE_ARCHIVO_VACIO = "adm.message.entidad.archivoVacio";
	public static final String MENSAJE_FORMATO_DATO = "adm.message.entidad.formatoDato";
	public static final String MENSAJE_DNI_DUPLICADO = "adm.message.entidad.dniduplicado";
	public static final String MENSAJE_EMAIL_DUPLICADO = "adm.message.entidad.emailduplicado";
	public static final String MENSAJE_EMAIL_FORMATO_INVALIDO = "adm.message.entidad.emailformatoinvalido";
	public static final String MENSAJE_RUC_NO_EXISTE = "adm.message.entidad.rucnoexiste";
	public static final String MENSAJE_DNI_NO_EXISTE = "adm.message.entidad.dninoexiste";
	public static final String MENSAJE_USER_NOPERTENEC_ENTIDAD = "adm.message.entidad.userNoPertenece";
	public static final String MENSAJE_USER_YA_ES_ADMINISTRADOR = "adm.message.entidad.userYaEsAdmin";
	public static final String MENSAJE_USER_NO_EXISTE_EN_SISTEMA = "adm.message.entidad.userNoExiste";
	public static final String MENSAJE_USER_EXISTE_EN_SISTEMA = "adm.message.entidad.userExiste";
	public static final String MENSAJE_LISTA_UO_NO_EXISTE = "adm.message.entidad.listaUOnoexiste";
	public static final String MENSAJE_UO_EXISTENTE = "adm.message.entidad.uoexiste";
	public static final String MENSAJE_TLC_EXISTENTE = "adm.message.entidad.tlcexiste";
	public static final String MENSAJE_UO_EXISTE_IGUAL = "adm.message.entidad.uoexisteconelmismodetalle";
	public static final String MENSAJE_RUC_ES_PERSONA_NATURAL = "adm.message.entidad.rucPersonaNatural"; 
	public static final String MENSAJE_FECHA_OBLIGATORIO = "adm.message.entidad.fechaBusquedaObligatorio";
	public static final String MENSAJE_BUSQUEDA_ENTIDAD_NO_RESULTADO = "common.message.noresult";
	public static final String FORMATO_DNI_INVALIDO = "adm.message.entidad.dniinvalido";
	public static final String MENSAJE_BUSQUEDA_REPORTE_ENTIDAD_NO_RESULTADO = "common.message.noresult";
	public static final String MENSAJE_FECHA_RANGO_SUPERIOR = "adm.message.entidad.fechaRangoSuperior";
	public static final String MENSAJE_REPORTE_ENTIDAD_EXITO = "adm.message.reporteEntidad.exito";
	public static final String MENSAJE_ERROR_ENTIDAD_EXISTENTE = "adm.message.entidad.existente";
	public static final int DNI_SIZE = 8;
	public static final int RUC_SIZE = 11;
	public static final String MENSAJE_FORMATO_SOLO_NUMERICO = "adm.message.error.formato.solo.numerico";
	public static final String MENSAJE_SELECCIONAR_PRIMERO_ENTIDAD = "adm.message.error.seleccionar.primero.entidad";
	public static final String ERROR_ACCION_USUARIO_OBLIGATORIO = "adm.message.accionusuario.criterio.obligatorio";
	public static final String ERROR_ACCION_USUARIO_NO_RESULTADO = "adm.message.accionusuario.noresultado";
	public static final String ERROR_OBLIGATORIO_CONFIGURACION_ROL = "adm.message.config.rol.obligatorio";
	public static final String ERROR_OBLIGATORIO_CONFIGURACION_GRUPO = "adm.ptnot.config.grupo.obligatorio";
	public static final String ERROR_CAMPOS_OBLIGATORIOS = "adm.message.notificacion.envio.camposObligatorios";
	public static final String ERROR_DESTINATARIOS_REQUERIDOS = "adm.message.notificacion.error.nodestinatarios";
	public static final String MENSAJE_EXITO_NOTIFICACION_ENVIAR = "adm.message.notificacion.envio.exito";
	public static final String MENSAJE_ROL_YA_ASIGNADO = "adm.message.notificacion.configura.rolexiste";
	public static final String MENSAJE_ARCHIVO_EXISTENTE_NOTI = "adm.message.notificacion.envio.archivoExistente";
	public static final String ERROR_OBLIGATORIO_USUARIO_GENERAL = "adm.message.usuario.general.obligatorio";
	public static final String MENSAJE_EXITO_USUARIO_GUARDAR = "adm.message.usuario.confirmacion.guardar";
	public static final String MENSAJE_EXITO_USUARIO_MODIFICAR = "adm.message.usuario.confirmacion.modifico";
	public static final String MENSAJE_EXITO_USUARIO_DESACTIVAR = "adm.message.usuario.confirmacion.desactivar";
	public static final String MENSAJE_EXITO_USUARIO_GUARDAR_ADMIN = "adm.message.usuario.confirmacion.guardar.admin";
	public static final String ERROR_FORMATO_EMAIL = "adm.message.usuario.general.error.email";
	public static final String ERROR_REGISTRO_PERFIL = "adm.message.usuario.general.error.perfil";
	public static final String ERROR_REGISTRO_PERFIL_INACTIVO = "adm.message.usuario.general.error.perfil.inactivo";
	public static final String ERROR_REGISTRO_EMAIL_DUPLICATE_LIFERAY = "adm.message.usuario.general.error.email.liferay";
	public static final String ERROR_REGISTRO_USUARIO = "adm.message.usuario.general.error.registro";
	public static final String ERROR_REGISTRO_USER_DNI = "adm.message.usuario.general.error.registroDNI";
	public static final String ERROR_REGISTRO_USUARIO_COMPLETAR_FORMULARIO = "adm.message.usuario.general.error.completarFormulario";
	public static final String ERROR_REGISTRO_USUARIO_COMPLETAR_FORMULARIO_INGRESAR = "adm.message.usuario.general.error.completarFormularioIngresar";
	public static final String ERROR_REGISTRO_USUARIO_INGRESAR_ENTIDAD = "adm.message.usuario.organismo.seleccione";
	public static final String ERROR_REGISTRO_USUARIO_LIFERAY = "adm.message.usuario.general.error.registro.liferay";	
	public static final String MENSAJE_PREVIO_REGISTRO_USUARIO = "adm.message.usuario.general.verificacion.anterior";
	public static final String MENSAJE_ORGANISMO_INACTIVO = "adm.message.usuario.organismo.inactivo";
	public static final String MENSAJE_EMAIL_EXISTE = "adm.usuario.label.email.registrado";
	public static final String MENSAJE_ERROR_REGISTRO_EVALUACION = "adm.message.resgitroEvaluacion.registro.error";
	public static final String MENSAJE_ERROR_REGISTRO_EVALUACION_ENTIDAD_INACTIVA = "adm.message.resgitroEvaluacion.registro.entidad.inactiva.error";
	public static final String MENSAJE_ERROR_FECHA_ENVIO = "adm.buscarSolicitud.label.fieldset.fechaEnvio";
	public static final String MENSAJE_ERROR_FECHA_ATENCION = "adm.buscarSolicitud.label.fieldset.fechaAtencion";
	public static final String MENSAJE_SOLICITUD_NRO_CORRELATIVO_NO_EXISTE = "adm.solicitud.error.registro.solicitud.correlativo";
	public static final String MENSAJE_ERROR_REGISTRO_USUARIO_DESACTIVACION = "adm.message.usuario.general.error.servicioSuspendido";
	public static final String MENSAJE_REGISTRO_DATOS_ADMINISTRADOR = "adm.solicitudDesacAdminBean.label.registrar.datos.administrador";
	public static final String MENSAJE_ERROR_REGISTRO_SOLICITUD = "adm.message.error.completar.registro.solicitud";
	public static final String MENSAJE_ERROR_REGISTRO_SOLICITANTE = "adm.message.error.registro.solicitante";
	public static final String MENSAJE_ERROR_DATOS_REEMPLAZANTES = "adm.message.error.completar.datos.reemplazante";
	public static final String MENSAJE_ERROR_DATOS_ADMINISTRADOR = "adm.message.error.completar.datos.administrador";
	public static final String MENSAJE_ERROR_USUARIO_REEMPLAZANTE_INACTIVO = "adm.message.error.usuario.reemplazante.inactivo";
	public static final String MENSAJE_ERROR_REGISTRO_DATOS_ENTIDAD = "adm.message.error.completar.registro.datos.entidad";
	public static final String MENSAJE_ERROR_ENTIDAD_EXISTE_SOLICITUD = "adm.message.error.entidad.existe.solicitud";
	public static final String MENSAJE_ERROR_FORMATO_FAX = "common.message.validator.faxInvalido";
	public static final String MENSAJE_ERROR_FORMATO_TELEFONO = "common.message.validator.telefonoInvalido";
	public static final String MENSAJE_ERROR_TIPO_NOACT = "NOACT";
	public static final String MENSAJE_ERROR_SOLICITUD = "adm.solicitudModificacionEntidad.error.solicitud.mod.entidad";
	public static final String MENSAJE_ERROR_SOLICITUD_MOD_ENTIDAD = "adm.solicitudModificacionEntidad.error.modificacion.entidad";
	public static final String MENSAJE_ERROR_FORMATO_CORREO = "adm.message.entidad.emailformatoinvalidoGeneral";
	public static final String MENSAJE_ERROR_DOCCUMENTOADJUNTO_MODIFICACION = "adm.message.documentoNoAdjunto.modificacion";
	public static final String MENSAJE_EXISTE_SOLICITUD_PENDIENTE_EVALUACION = "adm.solicitudModificacionEntidad.existe.solicitud.pendiente";
	public static final String MENSAJE_ENTIDAD_DOCUMENTO_SUSTENTO_TITULAR = "adm.message.entidad.documento.sustento.titular";
	public static final String MENSAJE_ENTIDAD_DOCUMENTO_SUSTENTO_CREACION = "adm.message.entidad.documento.sustento.creacion";
	public static final String MENSAJE_ENTIDAD_DOCUMENTO_SUSTENTO_JEFE = "adm.message.entidad.documento.sustento.jefe";
	public static final String MENSAJE_ENTIDAD_DOCUMENTO_SUSTENTO_CREACION_JEFE = "adm.message.entidad.documento.sustento.creacion.jefe";
	public static final String MENSAJE_ENTIDAD_DOCUMENTO_SUSTENTO_CREACION_JEFE_INFOR = "adm.message.entidad.documento.sustento.creacion.jefe.infor";
	public static final String MENSAJE_ENTIDAD_DOCUMENTO_SUSTENTO_CREACIN_INFOR = "adm.message.entidad.documento.sustento.creacion.infor";
	public static final String MENSAJE_ERROR_CONTRASENA_IGUAL_AL_ANTERIOR = "adm.message.modificarclave.contrasena.igual.anterior";
        public static final String MENSAJE_ERROR_CONTRASENA_DISTINTA_AL_ANTERIOR = "adm.message.modificarclave.contrasena.distinta.anterior";
                
	public static final String MENSAJE_ERROR_DEBE_SELECCIONAR_ROL = "adm.usuario.nuevo.seleccionar.rol";
	public static final String MENSAJE_INGRESAR_RAZON_SOCIAL = "adm.message.entidad.razon.social";
	public static final String MENSAJE_INGRESAR_DIRECCION_FISCAL = "adm.message.entidad.direccion.fiscal";
	public static final String MENSAJE_VALIDAR_RUC = "adm.message.entidad.validar.ruc";
	public static final String MENSAJE_ROLES_YA_AGREGADOS = "adm.message.seleccion.registro.yaagregados";
	public static final String MENSAJE_ERROR_AMBITO = "adm.message.validacion.ambito";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA_IMAGE = "portal.upload.aviso.image";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA_DOC = "portal.upload.aviso.documento";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA_TXT = "portal.upload.aviso.text";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA_ALL = "portal.upload.aviso.all";
	public static final String MENSAJE_ARCHIVO_EXTENSIONINVALIDA_ICON = "portal.upload.aviso.icon";
	public static final String MENSAJE_EXITO_GENERAR_CLAVE = "adm.message.confirmacion.generar.clave";
	public static final String ERROR_GENERAR_CLAVE = "adm.message.error.generar.clave";
	public static final String MENSAJE_GUARDO_BORRADOR = "adm.message.error.guardar.borrador";
	public static final String MENSAJE_ACTUALIZO_BORRADOR = "adm.message.error.actualizar.borrador";
	public static final String ERROR_OBLIGATORIO_ENTIDAD_GENERAL = "adm.message.entidad.general.obligatorio";
	public static final String MENSAJE_INGRESE_PUNTO_NOTIFICACION = "adm.ptnot.type.ingresePuntoNotificacion";
	public static final String MENSAJE_INGRESE_GRUPO_NOTIFICACION = "adm.ptnot.type.ingreseGrupoNotificacion";
	public static final String MENSAJE_ERROR_ENVIO_CORREO = "adm.message.notificacion.envio.fallido";
	public static final String ERROR_ROL_ASIGNADO_ELIMINAR = "adm.expcetion.negocio.rolAsignadoEliminar";
	public static final String ERROR_MENSAJE_ENTIDAD_NO_TIENE_UNIDAD_OPERATIVA = "adm.message.usuario.entidad.unidadOperativa.noTiene";
	
	/*-*********************************************************/
	/*-******** MODULO DE SEURIDAD********/
	/*-*********************************************************/
	public static final String MESSAGE_ERROR_CREDENTIALS = "login.error.credencialesInvalidas";
        
        /** La contante MESSAGE_ERROR_USER_INACTIVO. */
        public static final String MESSAGE_ERROR_USER_INACTIVO = "login.error.usarioInactivo";

        /** La contante MESSAGE_ERROR_USER_BLOQUEADO_INTENTOS. */
        public static final String MESSAGE_ERROR_USER_BLOQUEADO_INTENTOS = "login.error.sesionBloqueoIntentosFallidos";

        /** La contante MESSAGE_ERROR_USER_BLOQUEADO_CADUCIDAD. */
        public static final String MESSAGE_ERROR_USER_BLOQUEADO_CADUCIDAD = "login.error.sesionBloqueoCaducada";

        /** La contante MESSAGE_ERROR_USER_BLOQUEADO_INACTIVIDAD. */
        public static final String MESSAGE_ERROR_USER_BLOQUEADO_INACTIVIDAD = "login.error.sesionBloqueoExpirada";

        /** La contante MESSAGE_ERROR_CAPTCHA_INCORRECTO. */
        public static final String MESSAGE_ERROR_CAPTCHA_INCORRECTO = "pe.gob.minedu.escale.administracion.business.exception.CaptchaIncorrectoException";

        /** La contante MESSAGE_ERROR_AUTENTICACION. */
        public static final String MESSAGE_ERROR_AUTENTICACION = "login.error.autenticacion";

        /** La contante MESSAGE_ERROR_SESSION_ACTIVA. */
        public static final String MESSAGE_ERROR_SESSION_ACTIVA = "login.error.sesionActiva";

        /** La contante MESSAGE_ERROR_SESSION_POR_INICIAR. */
        public static final String MESSAGE_ERROR_SESSION_POR_INICIAR = "login.error.sesionPorIniciar";

        /** La contante MESSAGE_ERROR_SESSION_INICIADA. */
        public static final String MESSAGE_ERROR_SESSION_INICIADA = "login.error.sesionIniciado";
        
        
        public static final String MESSAGE_ERROR_USUARIO_INGRESE = "login.error.usuarioIngrese";
        
        public static final String MESSAGE_ERROR_CLAVE_INGRESE = "login.error.claseIngrese";
        
        
        
        
        
}