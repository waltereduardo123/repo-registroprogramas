package pe.gob.minedu.escale.common.web.jsf.util;


import java.security.Principal;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.util.LogUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.SERVICE_CONTEXT;

public class FacesUtil {

	/** El log. */
	private static LogUtil log = new LogUtil(FacesUtil.class.getName());
	
	/**
	 * Obtiene context path.
	 *
	 * @return context path
	 */
	public static String getContextPath() {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getRequestContextPath();
	}

	/**
	 * Obtiene context.
	 *
	 * @return context
	 */
	public static ServiceContext getContext() {
		return (ServiceContext) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(SERVICE_CONTEXT);
	}

	/**
	 * Obtiene session map.
	 *
	 * @return session map
	 */
	public static Map<String, Object> getSessionMap() {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap();
	}

	/**
	 * Obtiene faces context.
	 *
	 * @return faces context
	 */
	public static synchronized FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	/**
	 * Obtiene bean.
	 *
	 * @param nombre el nombre
	 * @return bean
	 */
	public static synchronized Object getBean(String nombre) {
		FacesContext facesContext = getFacesContext();
		return facesContext.getELContext().getELResolver().getValue(facesContext.getELContext(), null,nombre);
	}
	
	/**
	 * Comprueba si es logged in.
	 *
	 * @return true, si es logged in
	 */
    public boolean isLoggedIn() {
        return !getPrincipalName().trim().equals("");
    }

    /**
     * Obtiene principal name.
     *
     * @return principal name
     */
    public static synchronized String getPrincipalName() {
        try {
            return getExternalContext().getUserPrincipal().getName();
        } catch (Exception ex) {
        	log.error(ex);
            return "";
        }
    }
    
    /**
     * Obtiene external context.
     *
     * @return external context
     */
    public static synchronized ExternalContext getExternalContext() {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getExternalContext();
    }

    /**
     * Obtiene user name.
     *
     * @return user name
     */
    public static synchronized String getUserName() {
        return getPrincipal().getName();
    }
    
    /**
     * Obtiene principal.
     *
     * @return principal
     */
    public static synchronized Principal getPrincipal() {
        return getExternalContext().getUserPrincipal();

    }

    /**
     * Obtiene remote address.
     *
     * @return remote address
     */
    public static synchronized String getRemoteAddress() {
        return getRequest().getRemoteAddr();
    }
    
    /**
     * Obtiene request.
     *
     * @return request
     */
    public static synchronized HttpServletRequest getRequest() {
        ExternalContext ctx;
        try {
            ctx = FacesContext.getCurrentInstance().getExternalContext();
            return (HttpServletRequest) ctx.getRequest();
        } catch (NullPointerException ex) {
        	log.error(ex);
            return null;
        }
    }
    
    /**
     * Obtiene response.
     *
     * @return response
     */
    public static synchronized HttpServletResponse getResponse() {
        ExternalContext ctx;
        try {
            ctx = FacesContext.getCurrentInstance().getExternalContext();
            return (HttpServletResponse) ctx.getResponse();
        } catch (Exception ex) {
        	log.error(ex);
            return null;
        }
    }

	/**
	 * Error.
	 *
	 * @param msg el msg
	 */
	public static synchronized void error(String msg) {
		FacesContext fc = FacesContext.getCurrentInstance();
		if (fc != null) {
			FacesMessage facesMsg;
			facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
			fc.addMessage(null, facesMsg);
		}
	}
	
	/**
	 * Info.
	 *
	 * @param msg el msg
	 */
	public static synchronized void info(String msg) {
            FacesMessage facesMsg;
            facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage("successInfo", facesMsg);
        }
	
	/* Metodo que retorna el real path de la aplicacion-
	 * fecha de creacion : 04 05 11
	 * */
	/**
	 * Obtiene real path.
	 *
	 * @param path el path
	 * @return real path
	 */
	public static String getRealPath(String path) {  
		
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		return ctx.getRealPath(path);

	}
	
	/**
	 * Obtiene locale.
	 *
	 * @return locale
	 */
	public static Locale getLocale() {
		 Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
         Locale localeSession = ResourceUtil.obtenerLocaleSession();
         if (localeSession != null) {
             locale = localeSession;
         }
		return locale ;
	}
	
}
