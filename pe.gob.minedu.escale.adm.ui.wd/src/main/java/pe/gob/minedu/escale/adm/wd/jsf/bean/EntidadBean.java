package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import pe.gob.minedu.escale.adm.business.type.FechaAccionType;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.business.type.TipoAgrupadorSeguridad;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.OrganismoServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.OrganismoDTO;
import pe.gob.minedu.escale.adm.model.dto.OrganismoPerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoDTO;
import pe.gob.minedu.escale.adm.vo.DetalleEntidadVO;
import pe.gob.minedu.escale.adm.vo.EntidadCriteriaVO;
import pe.gob.minedu.escale.adm.vo.SelectVO;
import pe.gob.minedu.escale.adm.vo.UsuarioSessionVO;
import pe.gob.minedu.escale.adm.web.util.ValidacionUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.FactoryBean;
import pe.gob.minedu.escale.common.exception.BusinessException;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FormatterUtil;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;
import pe.gob.minedu.escale.common.web.jsf.util.FacesUtil;
import pe.gob.minedu.escale.common.web.util.ConstantesUtil;


@ManagedBean(name="entidadBean")
@SessionScoped
public class EntidadBean extends BaseBean implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 521190247338957589L;

	/** El servicio usuario service local. */
	@EJB
	private transient UsuarioServiceLocal usuarioServiceLocal;
	
	
	/** El servicio de administracion service local. */
	@EJB
	private transient AdministracionServiceLocal administracionServiceLocal;
        
        /** El servicio de organismo service local. */
	@EJB
	private transient OrganismoServiceLocal organismoServiceLocal;

	/** La Constante SI. */
	private static final String SI = "SI";

	/** La Constante EXITO. */
	private static final String EXITO = "success";
	
	/** La Constante FALLO. */
	private static final String FALLO = "fail";
	
	/** La Constante OPERACION_CANCELADA. */
	private static final String OPERACION_CANCELADA = "operacionCancelada";
	
	/** La Constante RETURN_FSME. */
	private static final String RETURN_FSME = "formularioSolicitudModificacionEntidad";
	
	/** La Constante VIEW_MODIFICAR_ENTIDAD. */
	private static final String VIEW_MODIFICAR_ENTIDAD = "formularioModificarEntidad";
	
	/** La Constante MODIFICACION_FALLO. */
	private static final String MODIFICACION_FALLO = "modificacionError";

//	
	/** La Constante FLAG_OK. */
	private static final String FLAG_OK = "OK";
	
	/** La Constante FLAG_NOK. */
	private static final String FLAG_NOK = "NOK";
	

	/** La Constante INDICADOR_ACTIVO. */
	private static final int INDICADOR_ACTIVO = 1;
	
	/** El flag modificar entidad valido. */
	private boolean modificarEntidadValido = false;

	
	/** El contexto. */
	private ServiceContext context;
	
	/** El control message util. */
        private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), EntidadBean.class);   
   
        
        /** El objeto usuario organismo. */
	private UsuarioOrganismoDTO usuarioOrganismo;
	
	/** El tipo accion. */
	private FechaAccionType tipoAccion;
	
	/** El objeto entidad criteria. */
	private EntidadCriteriaVO entidadCriteria;
        
        /** El codigo organismo. */
	private Long codigoOrganismo;
        
        /** La cantidad de filas. */
	private int cantFilas = 0;
        
        /** El flag mostrar boton regresar. */
	private boolean mostrarBotonRegresar;
        
        /** El mensaje busqueda entidad. */
	private String mensajeBusquedaEntidad;
        
        /** EL mensaje fecha buscar entidad. */
	private String mensajeFechaBuscarEntidad;
        
        /** La lista entidad. */
	private List<OrganismoDTO> listaEntidad = new ArrayList<OrganismoDTO>();
        
        /** El flag valida click. */
	private boolean validaClick;
        
        /** El flag external. */
	private String flagExternal = null;
        
        /** El flag entidad saved session. */
	private boolean flagEntidadSavedSession = false;
        
        /** El call buscar entidad modificar entidad. */
	private String callBuscarEntidadModificarEntidad;
        
        /** El call buscar historico entidad ver historico entidad. */
	private String callBuscarHistoricoEntidadVerHistoricoEntidad;
        
        /** El codigo entidad. */
	private Long codigoEntidad;
        
        /** La lista estado. */
	private List<SelectItem> listaEstado;
        
        /** El languaje estado. */
	private String languajeEstado = "";
        
        /** La lista accion. */
	private List<SelectItem> listaAccion;
        
        /** El languaje accion. */
	private String languajeAccion = "";
        
        /** El flag indicador ambito entidad. */
	private boolean indicadorAmbitoEntidad = false;
        
        /** EL size busqueda. */
	private int sizeBusqueda = 0;
        
        /** EL size entidades encontradas. */
	private int sizeEntidadesEncontradas;
        
        //imendoza
        private OrganismoDTO organismo;
        
        /** El call entidad buscar perfil. */
	private String callEntidadBuscarPerfil;
        
        /** La lista organismo perfil. */
	private List<OrganismoPerfilDTO> listaOrganismoPerfil = new ArrayList<OrganismoPerfilDTO>();
                
        /** La cantidad perfil asignado. */
	private int cantidadPerfilAsignado;
        
        /** La lista usuario organismo. */
	private List<UsuarioOrganismoDTO> listaUsuarioOrganismo = new ArrayList<UsuarioOrganismoDTO>();
        
        /** El objeto entidad persist. */
	private OrganismoDTO entidadPersist = null;
        
        /** El call buscar entidad detalle entidad. */
	private String callBuscarEntidadDetalleEntidad;
        
        /** El objeto detalle entidad. */
	private DetalleEntidadVO detalleEntidad = null;
        
        /** El flag deshabilitar formulario entidad. */
	private boolean deshabilitarFormularioEntidad = true;
        
        
        /** El flag estado entidad. */
	private boolean flagEstadoEntidad = true; // Atributo que determina el estado actual de la entidad
        
        /** El flag usuario administrador. */
	private boolean flagUsuarioAdministrador = false; // Atributo que determina
	// si se apertura el formulario de asignacion de usuario administrador
        
        /** El flag activacion entidad. */
	private boolean activacionEntidad = false;
        
        /** La variable generales completado. */
	private String generalesCompletado = null;
        
	/**
	 * Instancia un nuevo entidad bean.
	 */
	public EntidadBean() {
		
	}
	
	/**
	 * Post construct.
	 */
	@PostConstruct
	public void postConstruct() {
		try {
			context = FacesUtil.getContext();
//			setEntidad(FactoryBean.getBean(EntidadDTO.class));
			setEntidadCriteria(new EntidadCriteriaVO());
//			setCantidadPerfilAsignado(0);
			setUsuarioOrganismo(FactoryBean.getBean(UsuarioOrganismoDTO.class));
			setCantFilas(FormatterUtil.toIntPrimivite(administracionServiceLocal.getParametro(ParametroType.CANTIDAD_FILAS_DT_ADM.getValue())));
//			setListaEntidad(new ArrayList<EntidadDTO>());
//			limpiarErrorFormularioEntidad();	
			this.setMostrarBotonRegresar(false);
                        //imendoza
                        setOrganismo(FactoryBean.getBean(OrganismoDTO.class));
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	
	/**
	 * Error obligatorio entidad general.
	 *
	 */
	public  void errorObligatorioEntidadGeneral() {
		controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.ERROR_OBLIGATORIO_ENTIDAD_GENERAL, FacesMessage.SEVERITY_ERROR);
	}

	/**
	 * Error obligatorio entidad perfil.
	 *
	 */
	public void errorObligatorioEntidadPerfil() {
		controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.ERROR_OBLIGATORIO_ENTIDAD_PERFIL, FacesMessage.SEVERITY_ERROR);
	}

	/**
	 * Cancelar operacion.
	 *
	 * @return the string
	 */
	public String cancelarOperacion() {
		// limpiar campos eliminar session de ManagedBean
		Map<String, Object> session = FacesUtil.getSessionMap();
		session.remove("entidadBean");
		session.remove("usuarioBean");
		if (getUsuarioOrganismo() != null && getUsuarioOrganismo().getUsuario() != null) {
			getUsuarioOrganismo().getUsuario().setNombreCompleto("");
		}
		setUsuarioOrganismo(null);
		return OPERACION_CANCELADA;
	}

	/**
	 * Iniciar registrar entidad.
	 *
	 * @param event el event
	 * @throws Exception the exception
	 */
	public void iniciarRegistrarEntidad(ActionEvent event) throws Exception {
		try {
			setListaOrganismoPerfil(new ArrayList<OrganismoPerfilDTO>());			
                        setOrganismo(FactoryBean.getBean(OrganismoDTO.class));
			PerfilDTO perfil = administracionServiceLocal.obtenerPerfil(
					context,
					TipoAgrupadorSeguridad.PERFIL_FUNCIONARIO_ENTIDAD
							.getValue());
                        //Se elimina, ya que se debe agregar los perfiles seleccionados y no por defecto el del registrador.
//			OrganismoPerfilDTO organismoPerfil = new OrganismoPerfilDTO();
//			organismoPerfil.setPerfil(perfil);
//			getListaOrganismoPerfil().add(organismoPerfil);
//			setCantidadPerfilAsignado(1);
			Map<String, Object> session = FacesUtil.getSessionMap();
			session.remove("usuarioBean");
                        
			if (getUsuarioOrganismo() != null && getUsuarioOrganismo().getUsuario() != null) {
				getUsuarioOrganismo().setUsuario(FactoryBean.getBean(UsuarioDTO.class));
			}

			setUsuarioOrganismo(null);
			
                        
                        
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Registrar entidad.
	 *
	 * @return the string
	 * @throws BusinessException the business exception
	 */
        //imendoza
	public String registrarEntidad() throws BusinessException {
		context.setLocale(FacesUtil.getLocale());
		String resultado = "";
		try {
			getOrganismo().setListaOrganismoPerfil(getListaOrganismoPerfil());
			getOrganismo().setListaOrganismoPerfil(getListaOrganismoPerfil());

                        resultado = EXITO;
			if (resultado != null) {
				if (resultado.equalsIgnoreCase(EXITO)) {
					if (context != null && getOrganismo() != null) {
						setEntidadPersist(organismoServiceLocal.crearEntidad(context, getOrganismo()));						
						Date fec = new Date();
						DateFormat anhioFormat = new SimpleDateFormat("yyyy");
						String anhio = anhioFormat.format(fec);
						getListaUsuarioOrganismo().add(getUsuarioOrganismo());
						controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_EXITO_GUARDAR, FacesMessage.SEVERITY_INFO);										
						resultado = "registroExitoso";
						setMostrarBotonRegresar(true);
					} else {
						controlMessageUtil.imprimirMessage(controlMessageUtil.getMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_ENTIDAD_EXISTENTE), FacesMessage.SEVERITY_ERROR);
						resultado = "registroFallo";
					}
				}
			} else {
				resultado = "registroFallo";
			}
		} catch (BusinessException be) {
			controlMessageUtil.imprimirMessage(be, FacesMessage.SEVERITY_ERROR);
			getOrganismo().setId(null);
			resultado = "registroFallo";
		} catch (Exception e) {
			controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.ERROR_ENTIDAD_REGISTRO, FacesMessage.SEVERITY_ERROR);			
			if (getOrganismo() != null) {
				getOrganismo().setId(null);
			}
			resultado = "registroFallo";
		}
		return resultado;
	}

	/**
	 * Buscar entidad.
	 *
	 * @param event el event
	 * @throws Exception the exception
	 */
	public void buscarEntidad(ActionEvent event) throws Exception {
		context.setLocale(FacesUtil.getLocale());
		try {
			setListaEntidad(null);
			String resultado = validarFormularioBuscarEntidad(event);			
			if (resultado != null) {
				if (resultado.equalsIgnoreCase(EXITO)) {										
					getEntidadCriteria().setFechaAccionHasta(ValidacionUtil.SumarHorasCalendar(getEntidadCriteria().getFechaAccionHasta()));
					getEntidadCriteria().setIdEntidad(isIndicadorAmbitoEntidad() ? context.getUsuarioSessionVO().getIdEntidad() : null);
					setListaEntidad(organismoServiceLocal.buscarEntidad(context,getEntidadCriteria()));
					setSizeBusqueda(getListaEntidad().size());					
					if (getListaEntidad().size() == 0) {
						setMensajeBusquedaEntidad(controlMessageUtil.getMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_BUSQUEDA_ENTIDAD_NO_RESULTADO));
					}
					setSizeEntidadesEncontradas(getListaEntidad().size());
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Visualizar entidad.
	 *
	 * @param event el event
	 */
	public void visualizarEntidad(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("idEntidad");
			Long codigoEntidad = (Long) component.getValue();
			if (codigoEntidad != null) {
				setDetalleEntidad(organismoServiceLocal.visualizarEntidad(
						context, codigoEntidad));
				cargaFormularioDetalleEntidad(getDetalleEntidad().getEntidad());
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Iniciar modificar entidad.
	 *
	 * @param event el event
	 */
	public void iniciarModificarEntidad(ActionEvent event) {
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("idEntidadMod");
			Long codigoEntidad = (Long) component.getValue();
			setOrganismo(organismoServiceLocal.verDetalleEntidad(codigoEntidad));
			Map<String,Object> sessionMap = FacesUtil.getSessionMap();
			sessionMap.put("idEntidad", getOrganismo().getId());
			// Seteando el flag segun el estado para habilitar el formulario de usuario
			if (getOrganismo().getEstado() != null) {
				if (getOrganismo().getEstado().equalsIgnoreCase(
						EstadoState.INACTIVO.getKey())) {
					setFlagEstadoEntidad(false);
				}
			}			
			limpiarCriteriosBuscarEntidad(event);
			cargaFormularioModificacionEntidad(getOrganismo());
			setMostrarBotonRegresar(false);
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Carga formulario modificacion entidad.
	 *
	 * @param entidad el entidad
	 */
	public void cargaFormularioModificacionEntidad(OrganismoDTO entidad) {
		cargaFormularioEntidad(entidad, true, true);
	}

	/**
	 * Carga formulario detalle entidad.
	 *
	 * @param entidad el entidad
	 */
	public void cargaFormularioDetalleEntidad(OrganismoDTO entidad) {
		cargaFormularioEntidad(entidad, false, true);
	}

	/**
	 * Carga formulario entidad.
	 *
	 * @param entidad el entidad
	 * @param isModificacion el is modificacion
	 * @param isSolicitudModificacion el is solicitud modificacion
	 */
	private void cargaFormularioEntidad(OrganismoDTO entidad,
			boolean isModificacion, boolean isSolicitudModificacion) {
		try {
			setCodigoOrganismo(entidad.getId());
			if (isModificacion) {
				cargaIndicadores();
			}
			if (isSolicitudModificacion) {
				isModificacion = true;
			}
			
			if (isSolicitudModificacion) {
//				cargaFormularioAdministradorEntidad(entidad);
				cargaFormularioPerfilEntidad(entidad);
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Carga indicadores.
	 */
	public void cargaIndicadores() {	
		setDeshabilitarFormularioEntidad(false);
	}

	/**
	 * Carga formulario perfil entidad.
	 *
	 * @param entidad el entidad
	 */
	public void cargaFormularioPerfilEntidad(OrganismoDTO entidad) {
		if (entidad.getListaOrganismoPerfil() != null) {
			setListaOrganismoPerfil(entidad.getListaOrganismoPerfil());
			setCantidadPerfilAsignado(entidad.getListaOrganismoPerfil().size());
		}
		
	}


	/**
	 * Modificar entidad.
	 *
	 * @return the string
	 */
	public String modificarEntidad() {
		context.setLocale(FacesUtil.getLocale());
		String resultado = "";
		try {			
			resultado = validarFormularioEntidad(false,true);
			if (resultado != null) {
				if (resultado.equalsIgnoreCase(EXITO)) {
					organismoServiceLocal.modificarEntidad(context, getOrganismo());
					controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_EXITO_MODIFICAR, FacesMessage.SEVERITY_INFO);
					setMostrarBotonRegresar(true);
					resultado = MODIFICACION_FALLO;
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
			resultado = MODIFICACION_FALLO;
		}
		return resultado;
	}

	/**
	 * Establece el ear flag external.
	 *
	 * @param event el new ear flag external
	 */
	public void setearFlagExternal(ActionEvent event) {
		try {
			context.setLocale(FacesUtil.getLocale());
			limpiarCriteriosBuscarEntidad(null);
			setValidaClick(true);
	
			UIParameter component = null;
			// Se obtiene el Flag
			component = (UIParameter) event.getComponent().findComponent(
					"flagExternalEntidad");
			if (component == null) {
				component = (UIParameter) event.getComponent().findComponent(
						"flagExternalEntidadPerfil");
			}
                        
			if (component.getValue() != null) {
				setFlagExternal((component.getValue().toString()));
			}
			//Se asignan valores a los atributos necesarios
			if (getFlagExternal() != null) {
				if (getFlagExternal()
						.equalsIgnoreCase(ConstantesUtil.CALL_ENTIDAD_TO_USUARIO)) {
					Map<String,Object> session = FacesUtil.getSessionMap();
					if (session.get("objectUsuarioOrganismo") != null) {
						session.remove("objectUsuarioOrganismo");
					}
				}
				if (getFlagExternal()
						.equalsIgnoreCase(ConstantesUtil.CALL_SOLICITUD_MODIFICACION_TO_ENTIDAD)) {
					UIParameter entidadSavedSession = (UIParameter) event
							.getComponent()
							.findComponent("flagEntidadSavedSession");
					if (entidadSavedSession.getValue() != null) {
						setFlagEntidadSavedSession((Boolean) (entidadSavedSession
								.getValue()));
					}
					UsuarioSessionVO usuario = context.getUsuarioSessionVO();
					setCodigoEntidad(usuario.getIdEntidad());
				}
	
				if (getFlagExternal()
						.equalsIgnoreCase(ConstantesUtil.CALL_REPORTE_ACTIVIDAD_USUARIO_TO_ENTIDAD)) {
					UsuarioSessionVO usuario = context.getUsuarioSessionVO();
					setCodigoEntidad(usuario.getIdEntidad());
				}
				if (getFlagExternal()
						.equalsIgnoreCase(ConstantesUtil.CALL_HISTORICO_ENTIDAD_TO_ENTIDAD)) {
					limpiarCriteriosBuscarEntidad(event);
				}
				if (getFlagExternal()
						.equalsIgnoreCase(getCallBuscarEntidadModificarEntidad())) {
					iniciarModificarEntidad(event);
				}
				if (getFlagExternal()
						.equalsIgnoreCase(getCallBuscarEntidadDetalleEntidad())) {
					visualizarEntidad(event);
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Establece el ear flag external detalle entidad.
	 *
	 * @param event el new ear flag external detalle entidad
	 */
	public void setearFlagExternalDetalleEntidad(ActionEvent event) {
		try {
			UIParameter component = null;
			// Se obtiene el Flag
			component = (UIParameter) event.getComponent().findComponent(
					"flagExternalDetalleEntidad");
			if (component.getValue() != null) {
				setFlagExternal((component.getValue().toString()));
			}
			if (getFlagExternal() != null) {
				if (getFlagExternal()
						.equalsIgnoreCase(getCallBuscarEntidadDetalleEntidad())) {
					visualizarEntidad(event);
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Agregar perfil.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String agregarPerfil() throws Exception {
            String retorno = StringUtils.EMPTY;
		try {
			PerfilBean perfil = (PerfilBean) FacesUtil.getBean("perfilBean");
			PerfilDTO bean;
			OrganismoPerfilDTO tipoResul;
			List<PerfilDTO> listaPerfiles = perfil.getLista();
			boolean blInsertar = true;
			boolean blMarcado = false;

			for (int i = 0; i < listaPerfiles.size(); i++) {
				bean = (PerfilDTO) listaPerfiles.get(i);
				// Si tiene algun registro marcado
				if (bean.isHidden()) {
					blMarcado = true;
					break;
				}
			}
			if (blMarcado) {
				for (int i = 0; i < listaPerfiles.size(); i++) {
					bean = (PerfilDTO) listaPerfiles.get(i);
					// Si el registro esta marcado
					if (bean.isHidden()) {
						blInsertar = true;
						for (int j = 0; j < getListaOrganismoPerfil().size(); j++) {
							tipoResul = getListaOrganismoPerfil().get(j);
							if (bean.getId() != null) {
								if (bean.getId().equals(
										tipoResul.getPerfil().getId())) {
									blInsertar = false;
									break;
								}
							}
						}
						// Listo pa insertar
						if (blInsertar) {
							tipoResul = new OrganismoPerfilDTO();
							tipoResul.setPerfil(bean);
							getListaOrganismoPerfil().add(tipoResul);

						}
					}
				}
			} else {
				controlMessageUtil.imprimirMessage(controlMessageUtil.getMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_UN_REGISTRO), FacesMessage.SEVERITY_ERROR);
				return "";
			}
			setCantidadPerfilAsignado(0);                        
			setCantidadPerfilAsignado(getListaOrganismoPerfil().size());
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
                if (getFlagExternal() != null) {
                    if(getFlagExternal().equalsIgnoreCase(getCallBuscarEntidadModificarEntidad())){
                         retorno = "volverModificarEntidad";
                    }
                }else{
                    retorno = "volverEntidadPerfil";
                }
		return retorno;
	}
//
	/**
	 * Eliminar elemento perfil entidad.
	 *
	 * @param event el event
	 * @throws Exception the exception
	 */
	public void eliminarElementoPerfilEntidad(ActionEvent event)
			throws Exception {
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("elementoPerfil");
			int idUO = Integer.parseInt(component.getValue().toString());
			for (int i = 0; i < getListaOrganismoPerfil().size(); i++) {
				if (idUO == i + 1) {
					getListaOrganismoPerfil().remove(i);
				}
			}
			setCantidadPerfilAsignado(getListaOrganismoPerfil().size());
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Validar formulario entidad.
	 *
	 * @param isCreacion el is creacion
	 * @param isSolicitud el is solicitud
	 * @return the string
	 */
	public String validarFormularioEntidad(boolean isCreacion, boolean isSolicitud) {
		String resultado = "";
		resultado = validarFormularioDatosGenerales();
		if (resultado != null) {			
                    if (resultado.equals(EXITO)) {
                            if (!isSolicitud) {
                                resultado = validarFormularioAsignarPerfilEntidad();
                            }
                            if (resultado.equals(EXITO)) {
                                return EXITO;
                            } else {
                                return FALLO;
                            }
                    } else {
                            return FALLO;
                    }
                } 		
		return FALLO;
	}

	/**
	 * Validar formulario datos generales.
	 *
	 * @return the string
	 */
	public String validarFormularioDatosGenerales() {
		context.setLocale(FacesUtil.getLocale());
		int contError = 0;		

		if (getOrganismo().getNombreOrganismo() == null) {			
			contError++;
		} else {
			try {
				organismoServiceLocal.validarNombreEntidad(getOrganismo());
			} catch (BusinessException e) {				
				controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
				setGeneralesCompletado(FLAG_NOK);
				return FALLO;
			} catch (Exception e) {
				controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
			}
		}
				
		if (contError > 0) {
			errorObligatorioEntidadGeneral();
			setGeneralesCompletado(FLAG_NOK);
			return FALLO;
		} 
		
		return EXITO;
		
	}

	/**
	 * Validar formulario asignar perfil entidad.
	 *
	 * @return the string
	 */
	public String validarFormularioAsignarPerfilEntidad() {
		if (getCantidadPerfilAsignado() > 0) {
			return EXITO;
		} else {
			errorObligatorioEntidadPerfil();
			return FALLO;
		}
	}

	/**
	 * Limpiar criterios buscar entidad.
	 *
	 * @param event el event
	 */
	public void limpiarCriteriosBuscarEntidad(ActionEvent event) {
		this.getEntidadCriteria().setAccion("");				
		this.getEntidadCriteria().setEstado("");
		this.getEntidadCriteria().setFechaAccionDesde(null);
		this.getEntidadCriteria().setFechaAccionHasta(null);
		this.getEntidadCriteria().setNombre("");		
		this.getEntidadCriteria().setIdDre("");		
                this.getEntidadCriteria().setIdUgel("");	
		
		setMensajeFechaBuscarEntidad(null);
		setMensajeBusquedaEntidad(null);
		setListaEntidad(null);
		
	}

	/**
	 * Validar formulario buscar entidad.
	 *
	 * @param event el event
	 * @return the string
	 */
	public String validarFormularioBuscarEntidad(ActionEvent event) {		
		setMensajeFechaBuscarEntidad(null);		
		setMensajeBusquedaEntidad("");
	
		if (StringUtils.isBlank(getEntidadCriteria().getNombre())								
				&& StringUtils.isBlank(getEntidadCriteria().getIdDre())	
                                && StringUtils.isBlank(getEntidadCriteria().getIdUgel())
				&& StringUtils.isBlank(getEntidadCriteria().getEstado())
				&& StringUtils.isBlank(getEntidadCriteria().getAccion())							
		) {			
			controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_MINIMO_UN_CAMPO_OBLIGATORIO, FacesMessage.SEVERITY_ERROR);
			return FALLO ;			
		}			
		if (getEntidadCriteria().getFechaAccionDesde() != null
				&& getEntidadCriteria().getFechaAccionHasta() != null) {
			if (getEntidadCriteria().getFechaAccionDesde().after(
					getEntidadCriteria().getFechaAccionHasta())) {
				controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_FECHA, FacesMessage.SEVERITY_ERROR);
				return FALLO;
			}
		}
		return EXITO;
	}

	/**
	 * Validar visualizar formulario usuario.
	 *
	 * @param event el event
	 */
	public void validarVisualizarFormularioUsuario(ValueChangeEvent event) {
		if (event != null) {
			String newValue = (String) event.getNewValue();
			getOrganismo().setEstado(newValue);
			if ((!isFlagEstadoEntidad()) && (getOrganismo().getEstado() != null)
					&& (getOrganismo().getEstado().equalsIgnoreCase(EstadoState.ACTIVO.getKey()))) {
				setFlagUsuarioAdministrador(true);
				setActivacionEntidad(true);
			} else {
				setFlagUsuarioAdministrador(false);
			}
		}
	}

    public FechaAccionType getTipoAccion() {
        return tipoAccion;
    }

    public void setTipoAccion(FechaAccionType tipoAccion) {
        this.tipoAccion = tipoAccion;
    }

    public EntidadCriteriaVO getEntidadCriteria() {
        return entidadCriteria;
    }

    public void setEntidadCriteria(EntidadCriteriaVO entidadCriteria) {
        this.entidadCriteria = entidadCriteria;
    }

    public UsuarioOrganismoDTO getUsuarioOrganismo() {
        return usuarioOrganismo;
    }

    public void setUsuarioOrganismo(UsuarioOrganismoDTO usuarioOrganismo) {
        this.usuarioOrganismo = usuarioOrganismo;
    }

    public int getCantFilas() {
        return cantFilas;
    }

    public void setCantFilas(int cantFilas) {
        this.cantFilas = cantFilas;
    }

    public Long getCodigoOrganismo() {
        return codigoOrganismo;
    }

    public void setCodigoOrganismo(Long codigoOrganismo) {
        this.codigoOrganismo = codigoOrganismo;
    }

    public boolean isMostrarBotonRegresar() {
        return mostrarBotonRegresar;
    }

    public void setMostrarBotonRegresar(boolean mostrarBotonRegresar) {
        this.mostrarBotonRegresar = mostrarBotonRegresar;
    }

    public String getMensajeBusquedaEntidad() {
        return mensajeBusquedaEntidad;
    }

    public void setMensajeBusquedaEntidad(String mensajeBusquedaEntidad) {
        this.mensajeBusquedaEntidad = mensajeBusquedaEntidad;
    }

    public String getMensajeFechaBuscarEntidad() {
        return mensajeFechaBuscarEntidad;
    }

    public void setMensajeFechaBuscarEntidad(String mensajeFechaBuscarEntidad) {
        this.mensajeFechaBuscarEntidad = mensajeFechaBuscarEntidad;
    }
        
    

    public List<OrganismoDTO> getListaEntidad() {
            return listaEntidad;
    }


    public void setListaEntidad(List<OrganismoDTO> listaEntidad) {
            this.listaEntidad = listaEntidad;
    }

    public boolean isValidaClick() {
        return validaClick;
    }

    public void setValidaClick(boolean validaClick) {
        this.validaClick = validaClick;
    }

    public String getFlagExternal() {
        return flagExternal;
    }

    public void setFlagExternal(String flagExternal) {
        this.flagExternal = flagExternal;
    }

    public boolean isFlagEntidadSavedSession() {
        return flagEntidadSavedSession;
    }

    public void setFlagEntidadSavedSession(boolean flagEntidadSavedSession) {
        this.flagEntidadSavedSession = flagEntidadSavedSession;
    }

    public String getCallBuscarEntidadModificarEntidad() {
        callBuscarEntidadModificarEntidad = ConstantesUtil.CALL_BUSCAR_ENTIDAD_TO_MODIFICAR_ENTIDAD;
        return callBuscarEntidadModificarEntidad;
    }

    public void setCallBuscarEntidadModificarEntidad(String callBuscarEntidadModificarEntidad) {
        this.callBuscarEntidadModificarEntidad = callBuscarEntidadModificarEntidad;
    }

    public String getCallBuscarHistoricoEntidadVerHistoricoEntidad() {
        return callBuscarHistoricoEntidadVerHistoricoEntidad;
    }

    public void setCallBuscarHistoricoEntidadVerHistoricoEntidad(String callBuscarHistoricoEntidadVerHistoricoEntidad) {
        this.callBuscarHistoricoEntidadVerHistoricoEntidad = callBuscarHistoricoEntidadVerHistoricoEntidad;
    }

    public Long getCodigoEntidad() {
        return codigoEntidad;
    }

    public void setCodigoEntidad(Long codigoEntidad) {
        this.codigoEntidad = codigoEntidad;
    }
    
    /**
    * Obtiene lista estado.
    *
    * @return lista estado
    */
   public List<SelectItem> getListaEstado() {
           if (!languajeEstado.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) {
                   languajeEstado = FacesUtil.getLocale().getLanguage();
                   listaEstado = new ArrayList<SelectItem>();
                   List<SelectVO> estadosEntidad = EstadoState.getList(FacesUtil.getLocale());

                   if (estadosEntidad != null) {
                           for (SelectVO estadoEntidad : estadosEntidad) {
                                   listaEstado.add(new SelectItem(estadoEntidad.getId(),
                                                   estadoEntidad.getValue()));
                           }
                   }
           }
           return listaEstado;
   }

   /**
    * Establece el lista estado.
    *
    * @param listaEstado el new lista estado
    */
   public void setListaEstado(List<SelectItem> listaEstado) {
           this.listaEstado = listaEstado;
   }

    public String getLanguajeEstado() {
        return languajeEstado;
    }

    public void setLanguajeEstado(String languajeEstado) {
        this.languajeEstado = languajeEstado;
    }
   
   /**
    * Obtiene lista accion.
    *
    * @return lista accion
    */
    public List<SelectItem> getListaAccion() {
           if (!languajeAccion.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) {
                   languajeAccion = FacesUtil.getLocale().getLanguage();
                   listaAccion = new ArrayList<SelectItem>();
                   List<SelectVO> accionesFecha = FechaAccionType.getList(FacesUtil.getLocale());
                   if (accionesFecha != null) {
                           for (SelectVO accionFecha : accionesFecha) {
                                   listaAccion.add(new SelectItem(accionFecha.getId(),
                                                   accionFecha.getValue()));
                           }
                   }
           }
           return listaAccion;
    }

    /**
    * Establece el lista accion.
    *
    * @param listaAccion el new lista accion
    */
    public void setListaAccion(List<SelectItem> listaAccion) {
           this.listaAccion = listaAccion;
    }

    public String getLanguajeAccion() {
        return languajeAccion;
    }

    public void setLanguajeAccion(String languajeAccion) {
        this.languajeAccion = languajeAccion;
    }

    public boolean isIndicadorAmbitoEntidad() {
        return indicadorAmbitoEntidad;
    }

    public void setIndicadorAmbitoEntidad(boolean indicadorAmbitoEntidad) {
        this.indicadorAmbitoEntidad = indicadorAmbitoEntidad;
    }

    public int getSizeBusqueda() {
        return sizeBusqueda;
    }

    public void setSizeBusqueda(int sizeBusqueda) {
        this.sizeBusqueda = sizeBusqueda;
    }

    public int getSizeEntidadesEncontradas() {
        return sizeEntidadesEncontradas;
    }

    public void setSizeEntidadesEncontradas(int sizeEntidadesEncontradas) {
        this.sizeEntidadesEncontradas = sizeEntidadesEncontradas;
    }

    /**
    * Obtiene time zone.
    *
    * @return time zone
    */
    public TimeZone getTimeZone() {
         return TimeZone.getDefault();
    }

    public OrganismoDTO getOrganismo() {
        return organismo;
    }

    public void setOrganismo(OrganismoDTO organismo) {
        this.organismo = organismo;
    }
    
    /**
    * Obtiene call entidad buscar perfil.
    *
    * @return call entidad buscar perfil
    */
   public String getCallEntidadBuscarPerfil() {
           callEntidadBuscarPerfil = ConstantesUtil.CALL_CREAR_ENTIDAD_TO_BUSCAR_PEFIL;
           return callEntidadBuscarPerfil;
   }

   /**
    * Establece el call entidad buscar perfil.
    *
    * @param callEntidadBuscarPerfil el new call entidad buscar perfil
    */
   public void setCallEntidadBuscarPerfil(String callEntidadBuscarPerfil) {
           this.callEntidadBuscarPerfil = callEntidadBuscarPerfil;
   }
   
   /**
    * Obtiene lista organismo perfil.
    *
    * @return lista organismo perfil
    */
   public List<OrganismoPerfilDTO> getListaOrganismoPerfil() {
           return listaOrganismoPerfil;
   }

   /**
    * Establece el lista organismo perfil.
    *
    * @param listaOrganismoPerfil el new lista organismo perfil
    */
   public void setListaOrganismoPerfil(
                   List<OrganismoPerfilDTO> listaOrganismoPerfil) {
           this.listaOrganismoPerfil = listaOrganismoPerfil;
   }
    /**
    * Obtiene cantidad perfil asignado.
    *
    * @return cantidad perfil asignado
    */
   public int getCantidadPerfilAsignado() {
           return cantidadPerfilAsignado;
   }

   /**
    * Establece el cantidad perfil asignado.
    *
    * @param cantidadPerfilAsignado el new cantidad perfil asignado
    */
   public void setCantidadPerfilAsignado(int cantidadPerfilAsignado) {
           this.cantidadPerfilAsignado = cantidadPerfilAsignado;
   }   
   
   /**
    * Obtiene lista usuario organismo.
    *
    * @return lista usuario organismo
    */
   public List<UsuarioOrganismoDTO> getListaUsuarioOrganismo() {
           return listaUsuarioOrganismo;
   }

   /**
    * Establece el lista usuario organismo.
    *
    * @param listaUsuarioOrganismo el new lista usuario organismo
    */
   public void setListaUsuarioOrganismo(
                   List<UsuarioOrganismoDTO> listaUsuarioOrganismo) {
           this.listaUsuarioOrganismo = listaUsuarioOrganismo;
   }
   
    /**
    * Obtiene entidad persist.
    *
    * @return entidad persist
    */
   public OrganismoDTO getEntidadPersist() {
           return entidadPersist;
   }

   /**
    * Establece el entidad persist.
    *
    * @param entidadPersist el new entidad persist
    */
   public void setEntidadPersist(OrganismoDTO entidadPersist) {
           this.entidadPersist = entidadPersist;
   }   
   

    
  
    
    /**
     * Obtiene call buscar entidad detalle entidad.
     *
     * @return call buscar entidad detalle entidad
     */
    public String getCallBuscarEntidadDetalleEntidad() {
            callBuscarEntidadDetalleEntidad = ConstantesUtil.CALL_BUSCAR_ENTIDAD_TO_DETALLE_ENTIDAD;
            return callBuscarEntidadDetalleEntidad;
    }

    /**
     * Establece el call buscar entidad detalle entidad.
     *
     * @param callBuscarEntidadDetalleEntidad el new call buscar entidad detalle entidad
     */
    public void setCallBuscarEntidadDetalleEntidad(
                    String callBuscarEntidadDetalleEntidad) {
            this.callBuscarEntidadDetalleEntidad = callBuscarEntidadDetalleEntidad;
    }
    
    /**
     * Obtiene detalle entidad.
     *
     * @return detalle entidad
     */
    public DetalleEntidadVO getDetalleEntidad() {
            return detalleEntidad;
    }

    /**
     * Establece el detalle entidad.
     *
     * @param detalleEntidad el new detalle entidad
     */
    public void setDetalleEntidad(DetalleEntidadVO detalleEntidad) {
            this.detalleEntidad = detalleEntidad;
    }

    
    /**
     * Comprueba si es deshabilitar formulario entidad.
     *
     * @return true, si es deshabilitar formulario entidad
     */
    public boolean isDeshabilitarFormularioEntidad() {
            return deshabilitarFormularioEntidad;
    }

    /**
     * Establece el deshabilitar formulario entidad.
     *
     * @param deshabilitarFormularioEntidad el new deshabilitar formulario entidad
     */
    public void setDeshabilitarFormularioEntidad(
                    boolean deshabilitarFormularioEntidad) {
            this.deshabilitarFormularioEntidad = deshabilitarFormularioEntidad;
    }
    
    /**
     * Comprueba si es flag estado entidad.
     *
     * @return true, si es flag estado entidad
     */
    public boolean isFlagEstadoEntidad() {
            return flagEstadoEntidad;
    }

    /**
     * Establece el flag estado entidad.
     *
     * @param flagEstadoEntidad el new flag estado entidad
     */
    public void setFlagEstadoEntidad(boolean flagEstadoEntidad) {
            this.flagEstadoEntidad = flagEstadoEntidad;
    }
    
    /**
     * Comprueba si es flag usuario administrador.
     *
     * @return true, si es flag usuario administrador
     */
    public boolean isFlagUsuarioAdministrador() {
            return flagUsuarioAdministrador;
    }

    /**
     * Establece el flag usuario administrador.
     *
     * @param flagUsuarioAdministrador el new flag usuario administrador
     */
    public void setFlagUsuarioAdministrador(boolean flagUsuarioAdministrador) {
            this.flagUsuarioAdministrador = flagUsuarioAdministrador;
    }
    
    /**
     * Comprueba si es activacion entidad.
     *
     * @return true, si es activacion entidad
     */
    public boolean isActivacionEntidad() {
            return activacionEntidad;
    }

    /**
     * Establece el activacion entidad.
     *
     * @param activacionEntidad el new activacion entidad
     */
    public void setActivacionEntidad(boolean activacionEntidad) {
            this.activacionEntidad = activacionEntidad;
    }
    
    /**
     * Obtiene generales completado.
     *
     * @return generales completado
     */
    public String getGeneralesCompletado() {
        return generalesCompletado;
    }

    /**
     * Establece el generales completado.
     *
     * @param generalesCompletado el new generales completado
     */
    public void setGeneralesCompletado(String generalesCompletado) {
         this.generalesCompletado = generalesCompletado;
    }

}