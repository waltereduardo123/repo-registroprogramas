package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import pe.gob.minedu.escale.adm.business.type.FechaAccionType;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.ModuloDTO;
import pe.gob.minedu.escale.adm.model.dto.PrivilegioDTO;
import pe.gob.minedu.escale.adm.model.dto.PrivilegioRolDTO;
import pe.gob.minedu.escale.adm.model.dto.RolDTO;
import pe.gob.minedu.escale.adm.vo.ErrorVO;
import pe.gob.minedu.escale.adm.vo.PrivilegioVO;
import pe.gob.minedu.escale.adm.vo.RolCriteriaVO;
import pe.gob.minedu.escale.adm.vo.RolNuevoVO;
import pe.gob.minedu.escale.adm.vo.RolVO;
import pe.gob.minedu.escale.adm.vo.SelectVO;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.adm.web.util.ValidacionUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.CondicionState;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.FactoryBean;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.CollectionUtil;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.FormatterUtil;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;
import pe.gob.minedu.escale.common.web.jsf.util.FacesUtil;


@ManagedBean(name="rolBean")
@SessionScoped
public class RolBean extends BaseBean implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1856611006974467531L;
	
	/** El servicio de administracion service local. */
	@EJB
	private transient AdministracionServiceLocal administracionServiceLocal;
	
	/** La Constante EN. */
	private static final String EN = "EN";

	/** La Constante ROLBEAN_LIST_PRIVILEGIO. */
	private static final String ROLBEAN_LIST_PRIVILEGIO = "ROLBEAN_LIST_PRIVILEGIO";

	/** La Constante GUARDAR_NUEVO_ROL. */
	private static final String GUARDAR_NUEVO_ROL = "guardarNuevoRol";

	/** La Constante EDITAR. */
	private static final String EDITAR = "EDITAR";

	/** La Constante NUEVO. */
	private static final String NUEVO = "NUEVO";

	/** La Constante PERFILBEAN_LIST_ROL. */
	private static final String PERFILBEAN_LIST_ROL = "PERFILBEAN_LIST_ROL";

	/** La Constante INPUT_ERROR. */
	private static final String INPUT_ERROR = "inputError";
	
	/** El contexto. */
	private ServiceContext context;
	
	/** El objeto rol criteria vo. */
	private RolCriteriaVO vo;
	
	/** El objeto rol dto. */
	private RolDTO dto;
	
	/** El objeto rol dto seleccionado. */
	private RolDTO rolDTOSeleccionado;
	
	/** El objeto rol nuevo vo. */
	private RolNuevoVO voN;
	
	/** El id. */
	private Long id;
	
	/** El size. */
	private int size;
	
	/** El size priv. */
	private int sizePriv;
	
	/** El id perfil. */
	private Long idPerfil;
	
	/** El objeto error. */
	private ErrorVO error;
	
	/** El nombre anterior. */
	private String nombreAnterior;
	
	/** La lista de estados. */
	private List<SelectItem> estados;
	
	/** La lista de modulos. */
	private List<SelectItem> modulos;
	
	/** La lista de acciones. */
	private List<SelectItem> acciones;
	
	/** La lista de indicadores adm. */
	private List<SelectItem> indicadoresAdm;
	
	/** La lista rol dto. */
	private List<RolDTO> lista;
	
	/** La lista rol vo . */
	private List<RolVO> listaR;
	
	/** El vector id priv. */
	private Vector<Long> idpriv;
	
	/** La lista privilegio vo 1. */
	private List<PrivilegioVO> listP1;
	
	/** La list privilegio vo 2. */
	private List<PrivilegioVO> listP2;
	
	/** La lista privilegio dto. */
	private List<PrivilegioDTO> listaPrivilegioDTO;
	
	/** El flag vis list perfil. */
	private boolean visListPerfil = false;
	
	/** El flag vis list perfil o. */
	private boolean visListPerfilO = true;
	
	/** La vis list perfil nuevo. */
	private boolean visListPerfilNuevo = false;
	
	/** El flag vis list perfil editar. */
	private boolean visListPerfilEditar = false;
	
	/** El flag vis list rol nuevo. */
	private boolean visListRolNuevo = false;
	
	/** El flag vis list rol editar. */
	private boolean visListRolEditar = false;
	
	/** El size perfil. */
	private int sizePerfil = 0;
	
	/** El size privilegio. */
	private int sizePrivilegio = 0;
	
	/** El size acceso directo. */
	private int sizeAccesoDirecto = 0;
	
	/** El mensaje lista vacia. */
	private String mensajelistavacia;
	
	/** El style txt nombre. */
	private String styleTxtNombre;
	
	/** El style txt modulo. */
	private String styleTxtModulo;
	
	/** El style txt descripcion. */
	private String styleTxtDescripcion;
	
	/** El style txt estado. */
	private String styleTxtEstado;
	
	/** El style txt finicio. */
	private String styleTxtFinicio;
	
	/** El style txt ffin. */
	private String styleTxtFfin;
	
	/** El hidden. */
	private String hidden;
	
	/** El flag external. */
	private String flagExternal;
	
	/** El flag vis list perfil nuevo cancelar. */
	private boolean visListPerfilNuevoCancelar = false;
	
	/** El flag vis list perfil editar cancelar. */
	private boolean visListPerfilEditarCancelar = false;
	
	/** El flag vis button nuevo. */
	private boolean visButtonNuevo = true;
	
	/** El flag ver check. */
	private boolean verCheck = false;
	
	/** El flag ver opciones. */
	private boolean verOpciones;
	
	/** El flag aceptar cambios. */
	private boolean aceptarCambios = true;
	
	/** El indicador tipo fecha. */
	private String indicadorTipoFecha = "";
	
	/** La lista estado filtrado. */
	private List<SelectItem> estadoFiltrado;
	
	/** El page actual. */
	private String pageActual;
	
	/** El flag estado seleccion. */
	private boolean estadoSeleccion = false;
	
	/** La cantidad de filas. */
	private int cantFilas = 0;
	
	/** El lenguaje indicadores adm. */
	private String languajeIndicadoresAdm = "";
	
	/** El lenguaje estados. */
	private String languajeEstados = "";
	
	/** El lenguaje acciones. */
	private String languajeAcciones = "";
	
	/** El id privilegio. */
	private Long idprivilegio;

	/** El lenguaje estado filtrado. */
	private String languajeEstadoFiltrado = "";
	
	/** El control message util. */
        private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), RolBean.class);   
    
        /**
	 * Instancia un nuevo rol bean.
	 */
	public RolBean() {
	}
	
	/**
	 * Post construct.
	 */
	@PostConstruct
	public void postConstruct() {
		context = FacesUtil.getContext();

		if (FacesUtil.getBean("perfilBean") != null) {
			PerfilBean bean = (PerfilBean) FacesUtil.getBean("perfilBean");
			this.setVisListPerfilNuevoCancelar(bean.isVisListPerfilNuevo());
			this.setVisListPerfilEditarCancelar(bean.isVisListPerfilEditar());
			this.setVisButtonNuevo(false);
		}

		mensajelistavacia = null;
		try {
			vo = FactoryBean.getBean(RolCriteriaVO.class);
			voN = FactoryBean.getBean(RolNuevoVO.class);
			dto = FactoryBean.getBean(RolDTO.class);
			listP1 = new ArrayList<PrivilegioVO>();
			lista = new ArrayList<RolDTO>();
			listaR = new ArrayList<RolVO>();
			listaPrivilegioDTO = new ArrayList<PrivilegioDTO>(); 
			cantFilas = FormatterUtil.toIntPrimivite(administracionServiceLocal.getParametro(ParametroType.CANTIDAD_FILAS_DT_ADM.getValue()));
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}		
	}
	
	/* FACES MENSAJES*/
	/**
	 * Mensaje error campo obligatorio.
	 *
	 */
	public  void mensajeErrorCampoObligatorio() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ERROR_CAMPO_OBLIGATORIO, FacesMessage.SEVERITY_ERROR);
	}
	
	/**
	 * Mensaje asignacion lista vacia rol.
	 *
	 */
	public  void mensajeAsignacionListaVaciaRol() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ASIGNACION_LISTA_VACIA_ROL, FacesMessage.SEVERITY_ERROR);
	}

	/**
	 * Mensaje asignacion lista vacia privilegio.
	 *
	 */
	public  void mensajeAsignacionListaVaciaPrivilegio() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ASIGNACION_LISTA_VACIA_PRIVILEGIO, FacesMessage.SEVERITY_ERROR);
	}
	
	/**
	 * Mensaje exito guardar.
	 *
	 */
	public  void mensajeExitoGuardar() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_EXITO_GUARDAR, FacesMessage.SEVERITY_INFO);
	}
	
	/**
	 * Mensaje exito modificar.
	 *
	 */
	public  void mensajeExitoModificar() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_EXITO_MODIFICAR, FacesMessage.SEVERITY_INFO);
	}
	
	/*MENSAJE DE ERROR PASANDO COMO PARAMETRO EL ConstantesUtil */
	/**
	 * Error.
	 *
	 * @param key el key
	 */
	public  void error(String key) {
		controlMessageUtil.imprimirMessageClave(key, FacesMessage.SEVERITY_ERROR);
	}
	
	/**
	 * Mensaje perfil activo.
	 *
	 */
	public  void mensajePerfilActivo() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ROL_ASOCIADO_ENTIDAD_ACTIVO, FacesMessage.SEVERITY_ERROR);
	}
	/**
	 * Validate buscar.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateBuscar() {
		boolean rs = false;
		if (FormatterUtil.toString(vo.getNombre()) == null) {
			setStyleTxtNombre(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtNombre("");
		}
		return rs;
	}

	/**
	 * Validate fecha.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateFecha() {
		boolean rs = false;
		return rs;
	}

	/**
	 * Adds the list rol.
	 */
	public void addListRol() {
		try {
			PerfilBean perfil = (PerfilBean) FacesUtil.getBean("perfilBean");
			RolBean rol = (RolBean) FacesUtil.getBean("rolBean");
			if (rol != null) {
				if (perfil != null) {
					if (perfil.getHidden() == null) {
						setVerCheck(true);
						setVisButtonNuevo(false);
						setVisListPerfil(false);
						setVerOpciones(false);
						return;
					}
	
				}
	
			}
			if (perfil != null) {
	
				if (perfil.getHidden() != null
						&& perfil.getHidden().equals(PERFILBEAN_LIST_ROL)) {
					if (perfil.getDireccionar().equals(NUEVO)) {
						this.setVisListPerfilNuevo(true);
						this.setVisListPerfilEditar(false);
					} else if (perfil.getDireccionar().equals(EDITAR)) {
						this.setVisListPerfilNuevo(false);
						this.setVisListPerfilEditar(true);
					}
					this.setVisListPerfil(true);
					this.setVisListPerfilO(false);
					this.setVerCheck(false);
				} else {
					this.setVisListPerfil(false);
					this.setVisListPerfilO(true);
					this.setVerCheck(false);
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Buscar rol.
	 *
	 * @param ev el ev
	 */
	public void buscarRol(ActionEvent ev) {
		try {
			context.setLocale(FacesUtil.getLocale());
			setVisListPerfilNuevo(false);
			if (!validateBuscar()) {
				if (!validateFecha()) {
					size = 0;
					this.estadoSeleccion = false;
					
					addListRol();
					vo.setFechaAccionHasta(ValidacionUtil.SumarHorasCalendar(vo
							.getFechaAccionHasta()));					
					if (vo.getAccion() != null) {
					    this.indicadorTipoFecha = vo.getAccion();
					}
					String accion = vo.getAccion();
					lista = administracionServiceLocal.buscarRol(context, vo, FacesUtil.getLocale());
					vo.setAccion(accion);
					RolDTO bean = null;
					listaR = new ArrayList<RolVO>();
					for (int i = 0; i < lista.size(); i++) {
						bean = (RolDTO) lista.get(i);
						RolVO b = new RolVO();
						b.setId(bean.getId());
						b.setNombre(bean.getNombre());
						if (bean.getDescripcion() == null) {
							b.setDescripcion("");
						} else {
							b.setDescripcion(bean.getDescripcion());
						}
						b.setModulo(bean.getModulo().getNombre());
						b.setEstado(bean.getEstado());
						b.setEsadm(bean.getIndicadorAdministrador());
						b.setFechaCreacion(bean.getFechaCreacion());
						b.setUltimaFechaActivacion(bean.getUltimaFechaActivacion());
						b.setUltimaFechaDesactivacion(bean.getUltimaFechaDesactivacion());
						b.setUltimaFechaModificacion(bean.getUltimaFechaModificacion());
						b.setAccion(bean.getAccion());
						b.setFechaAccion(bean.getFechaAccion());
						listaR.add(b);
					}
					size = listaR.size();
					
					mensajelistavacia = controlMessageUtil.getMessage(ConstantesUtil.MENSAJE_LISTA_VACIA);
					
				} else {
					mensajelistavacia = null;
					setListaR(null);
					size = 0;
				}
			} else {
				mensajelistavacia = null;
				setListaR(null);
				size = 0;
				controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ERROR_CAMPO_OBLIGATORIO, FacesMessage.SEVERITY_ERROR);
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	//buscar rol2 este metodo solo se usa la opcion seguridad --> administrar rol
	public void buscarRol2(ActionEvent ev) {
		try {
			context.setLocale(FacesUtil.getLocale());
			setVisListPerfilNuevo(false);
				if (!validateFecha()) {
					size = 0;
					this.estadoSeleccion = false;
					
					addListRol();
					vo.setFechaAccionHasta(ValidacionUtil.SumarHorasCalendar(vo
							.getFechaAccionHasta()));					
					if (vo.getAccion() != null) {
					    this.indicadorTipoFecha = vo.getAccion();
					}
					String accion = vo.getAccion();
					lista = administracionServiceLocal.buscarRol(context, vo, FacesUtil.getLocale());
					vo.setAccion(accion);
					RolDTO bean = null;
					listaR = new ArrayList<RolVO>();
					for (int i = 0; i < lista.size(); i++) {
						bean = (RolDTO) lista.get(i);
						RolVO b = new RolVO();
						b.setId(bean.getId());
						b.setNombre(bean.getNombre());
						if (bean.getDescripcion() == null) {
							b.setDescripcion("");
						} else {
							b.setDescripcion(bean.getDescripcion());
						}
						b.setModulo(bean.getModulo().getNombre());
						b.setEstado(bean.getEstado());
						b.setEsadm(bean.getIndicadorAdministrador());
						b.setFechaCreacion(bean.getFechaCreacion());
						b.setUltimaFechaActivacion(bean.getUltimaFechaActivacion());
						b.setUltimaFechaDesactivacion(bean.getUltimaFechaDesactivacion());
						b.setUltimaFechaModificacion(bean.getUltimaFechaModificacion());
						b.setAccion(bean.getAccion());
						b.setFechaAccion(bean.getFechaAccion());
						listaR.add(b);
					}
					size = listaR.size();
					
					mensajelistavacia = controlMessageUtil.getMessage(ConstantesUtil.MENSAJE_LISTA_VACIA);
					
				} else {
					mensajelistavacia = null;
					setListaR(null);
					size = 0;
				}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	/**
	 * Limpiar size detalle.
	 */
	private void limpiarSizeDetalle() {
		sizePerfil = 0;
		sizePrivilegio = 0;
		sizeAccesoDirecto = 0;
	}

	/**
	 * Detalle.
	 *
	 * @param event el event
	 */
	public void detalle(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		limpiarSizeDetalle();
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("id");
			id = Long.parseLong(component.getValue().toString());
			dto = administracionServiceLocal.obtenerRol(context, id);

			sizePerfil = dto.getListaPerfilRol().size();
			sizePrivilegio = dto.getListaPrivilegioRol().size();

		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}

	}

	/**
	 * Cancelar.
	 *
	 * @param event el event
	 */
	public void cancelar(ActionEvent event) {
		limpiarStyleTxt();
		this.mensajelistavacia = null;
		vo = new RolCriteriaVO();
		limpiarStyleTxt();
		mensajelistavacia = "";
		this.setListaR(null);
		size = 0;
		listaPrivilegioDTO = new ArrayList<PrivilegioDTO>();
		sizePriv = 0;
	}

	/**
	 * Nuevo.
	 *
	 * @return the string
	 */
	public String nuevo() {
		voN = new RolNuevoVO();
		limpiar();
		this.setListP1(null);
		limpiarStyleTxt();
		sizePriv = 0;
		aceptarCambios = true;
		return "nuevoRol";
	}

	/**
	 * Limpiar campos.
	 *
	 * @param ev el ev
	 */
	public void limpiarCampos(ActionEvent ev) {
		vo = new RolCriteriaVO();
		limpiarStyleTxt();
		mensajelistavacia = "";
		this.setListaR(null);
		size = 0;
		limpiarBotonAdicional();
	}

	/**
	 * Limpiar boton adicional.
	 */
	private void limpiarBotonAdicional() {
		visListPerfilNuevo = false;
		visListPerfilEditar = false;
	}

	/**
	 * Limpiar.
	 */
	public void limpiar() {
		dto = new RolDTO();
		this.setLista(null);

	}

	/**
	 * Limpiar nuevo.
	 */
	public void limpiarNuevo() {
		voN = new RolNuevoVO();
		listaPrivilegioDTO = new ArrayList<PrivilegioDTO>();
	}
	/**
	 * Guardar nuevo rol.
	 *
	 * @param ev el ev
	 */
	public void guardarNuevoRol(ActionEvent ev) {
		context.setLocale(FacesUtil.getLocale());
		UIParameter uip = (UIParameter) ev.getComponent().findComponent("saveRol");
		flagExternal = uip.getValue().toString();
		if (!validate()) {
			if (!validateListaPrivilegio()) {
				limpiarStyleTxt();
				try {
					dto.setFechaCreacion(FechaUtil.obtenerFechaActual());
					dto.setNombre(voN.getNombre().trim().toUpperCase());
					if (voN.getDescripcion() != null) {
						dto.setDescripcion(voN.getDescripcion().trim().toUpperCase());
					}
					dto.setIndicadorAdministrador(voN.getIndicadorAdministrador());
                                        dto.setModulo(new ModuloDTO());
					dto.getModulo().setId(voN.getModulo().getId());

					administracionServiceLocal.crearRol(context, dto);
		
					limpiarNuevo();
					
					mensajeExitoGuardar();
					
					aceptarCambios = false;

				} catch (Exception e) {
					controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
				}
			} else {
				mensajeAsignacionListaVaciaPrivilegio();
			}
		} else {
			mensajeErrorCampoObligatorio();
		}
	}

	/**
	 * Limpiar style txt.
	 */
	public void limpiarStyleTxt() {
		setStyleTxtNombre(null);
		setStyleTxtDescripcion(null);
		setStyleTxtModulo(null);
		setStyleTxtEstado(null);
		setStyleTxtFinicio(null);
		setStyleTxtFfin(null);
	}

	/**
	 * Validate.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validate() {
		boolean rs = false;
		if (voN.getNombre() == null) {
			setStyleTxtNombre(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtNombre("");
		}
		if (voN.getModulo().getId() == -1) {
			setStyleTxtModulo(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtModulo("");
		}
		return rs;
	}

	/**
	 * Validate lista privilegio.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateListaPrivilegio() {
		boolean rs = false;
		return rs;
		
	}

	/**
	 * Validate editar.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateEditar() {
		boolean rs = false;
		if (dto.getNombre() == null) {
			setStyleTxtNombre(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtNombre("");
		}
		if (dto.getModulo().getId() == null) {
			setStyleTxtModulo(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtModulo("");
		}
		if (dto.getEstado() == null) {
			setStyleTxtEstado(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtEstado("");
		}
		return rs;
	}

	/**
	 * Cancelar.
	 */
	public void cancelar() {
		limpiar();
		limpiarStyleTxt();
	}
	

	/**
	 * Eliminar seleccion.
	 *
	 * @param ev el ev
	 */
	public void eliminarSeleccion(ActionEvent ev) {
		try {
			UIParameter component = (UIParameter) ev.getComponent().findComponent("eliminarPriv");
			Long codigoPriv = Long.parseLong(component.getValue().toString());
			PrivilegioDTO privDTO;
			for (int i = 0; i < listaPrivilegioDTO.size(); i++) {
				privDTO = listaPrivilegioDTO.get(i);
				if (privDTO.getId().equals(codigoPriv)) {
					listaPrivilegioDTO.remove(i);
				}
			}
			this.sizePriv = listaPrivilegioDTO.size();
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	
	/**
	 * Eliminar seleccion2.
	 *
	 * @param ev el ev
	 */
	public void eliminarSeleccion2(ActionEvent ev) {
		try {
			UIParameter component = (UIParameter) ev.getComponent().findComponent("eliminarPriv");
			Long codigoPriv = Long.parseLong(component.getValue().toString());
			
			PrivilegioVO priVO;
			for (int i = 0; i < listP1.size(); i++) {
				priVO = listP1.get(i);
				if (priVO.getId().equals(codigoPriv)) {
					listP1.remove(i);
				}
			}
			sizePriv = listP1.size();
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	
	/**
	 * Editar.
	 *
	 * @param event el event
	 */
	public void editar(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("id");
			id = Long.parseLong(component.getValue().toString());
			dto = (RolDTO) administracionServiceLocal.obtenerRol(context, id);
			PrivilegioRolDTO bean = null;
			listP1 = new ArrayList<PrivilegioVO>();
			for (int i = 0; i < dto.getListaPrivilegioRol().size(); i++) {
				bean = (PrivilegioRolDTO) dto.getListaPrivilegioRol().get(i);
				PrivilegioVO priv = new PrivilegioVO();

				priv.setId(bean.getPrivilegio().getId());
				priv.setNombre(bean.getPrivilegio().getNombre());
				priv.setDescripcion(bean.getPrivilegio().getDescripcion());
				priv.setModulo(bean.getPrivilegio().getModulo().getNombre());
//				priv.setAccion(bean.getPrivilegio().getAccion());
				priv.setEstado(bean.getPrivilegio().getEstadoPrivilegio()
						.getValue());
				listP1.add(priv);

			}
			CollectionUtil.ordenador(false, listP1, "nombre");
			sizePriv = listP1.size();
			setNombreAnterior(dto.getNombre());
			aceptarCambios = true;
			buscarRol2(event); //cambio hecho en desarrollo por gtintaya
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Guardar editar rol.
	 *
	 * @param ev el ev
	 */
	public void guardarEditarRol(ActionEvent ev) {
		context.setLocale(FacesUtil.getLocale());
		UIParameter uip = (UIParameter) ev.getComponent().findComponent("editRol");
		flagExternal = uip.getValue().toString();
		if (!validateEditar()) {
			if (!validateListaPrivilegio()) {
				limpiarStyleTxt();
				if (!validacionRolActivo()) {
					try {
					dto.setNombre(dto.getNombre().trim().toUpperCase());
					if (dto.getDescripcion() != null) {
						dto.setDescripcion(dto.getDescripcion().trim().toUpperCase());
					}
					dto.setNombre(dto.getNombre().toUpperCase());
					dto.setFechaCreacion(FechaUtil.obtenerFechaActual());
					administracionServiceLocal.modificarRol(context, dto);

					mensajeExitoModificar();

					aceptarCambios = false;
				} catch (Exception e) {
					controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
				}
				} else {
					mensajePerfilActivo();  // cuadno el perfil se encuentra asociado a una entidad no se puede desactivar
				}
			} else {
				mensajeAsignacionListaVaciaPrivilegio();
			}
		} else {
			mensajeErrorCampoObligatorio();
		}
	}



	/**
	 * Obtiene diferences.
	 *
	 * @param <T> el tipo generico
	 * @param lst1 el lst1
	 * @param lst2 el lst2
	 * @return diferences
	 */
	public static <T> List<T> getDiferences(List<T> lst1, List<T> lst2) {
		List<T> lst3 = new ArrayList<T>(lst1);
		lst3.retainAll(lst2);
		List<T> lst4 = new ArrayList<T>(lst2);
		lst4.removeAll(lst3);
		return lst4;
	}

	/**
	 * Establece el ear rol.
	 *
	 * @param event el new ear rol
	 */
	public void setearRol(ActionEvent event) {
		try {
			UIParameter component0 = (UIParameter) event.getComponent().findComponent("flagExternalRol");
			if (component0 != null) {
				if (component0.getValue() != null) {
					flagExternal = component0.getValue().toString(); //flagExternal = CN
				}
			}
			// Se asignan valores a los atributos necesarios
			// Se asiga el idPerfil proveniente de ADMINISTRACION DE USUARIOS

			UIParameter component = (UIParameter) event.getComponent().findComponent("editId");
			if (component != null) {
				if (component.getValue() != null) {
					idPerfil = Long.parseLong(component.getValue().toString());
					vo.setIdPerfil(idPerfil);
				}
			}

		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Ver seleccion multiple.
	 *
	 * @param event el event
	 */
	public void verSeleccionMultiple(ActionEvent event) {
		RolBean rol = (RolBean) FacesUtil.getBean("rolBean");
		rol.limpiar();
		setFlagExternal(EN);
		listaR = new ArrayList<RolVO>();limpiarCampos(null);
		this.estadoSeleccion = false;
	}

	/**
	 * Limpiar fecha inicio.
	 *
	 * @param ev el ev
	 */
	public void limpiarFechaInicio(ActionEvent ev) {
		this.vo.setFechaAccionDesde(null);
	}

	/**
	 * Limpiar fecha fin.
	 *
	 * @param ev el ev
	 */
	public void limpiarFechaFin(ActionEvent ev) {
		this.vo.setFechaAccionHasta(null);
	}

	/**
	 * Validacion rol activo.
	 *
	 * @return true, en caso de exito
	 */
	public boolean validacionRolActivo() {
		if (dto.getEstado().equalsIgnoreCase(EstadoState.INACTIVO.getKey())) {
			return administracionServiceLocal.validarRolActivo(dto);
		} else {
			return false;
		}
	}

	/**
	 * Seleccionar todo.
	 *
	 * @param event el event
	 */
	public void seleccionarTodo(ValueChangeEvent event) {	
		try {
			if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
				event.setPhaseId(PhaseId.INVOKE_APPLICATION);
				event.queue();
				return;
				}
				if (event != null) {
					
					this.pageActual = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
					.get("frmRol:valorPageCount");
					int pagina = 0;
					if (this.pageActual == null) {
						pagina = 1;
					} else {
						pagina = Integer.valueOf(this.pageActual);
					}
					
					estadoSeleccion = (Boolean) event.getNewValue();
					
					for (int i = 0; i < 10; i++) {
						if (((pagina - 1) * 10 + i) < listaR.size()) {
						RolVO rolVo = listaR.get(((pagina - 1) * 10) + i);
						rolVo.setHidden((Boolean) event.getNewValue());
	
						listaR.set(((pagina - 1) * 10) + i, rolVo);
						}
	
					}					
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	
	/**
	 * Limpiar seleccion todo.
	 *
	 * @param ev el ev
	 */
	public void limpiarSeleccionTodo(ActionEvent ev) {
		this.setEstadoSeleccion(false);
	}
	
	//get y set
	/**
	 * Obtiene rol dto seleccionado.
	 *
	 * @return rol dto seleccionado
	 */
	public RolDTO getRolDTOSeleccionado() {
		return rolDTOSeleccionado;
	}

	/**
	 * Establece el rol dto seleccionado.
	 *
	 * @param rolDTOSeleccionado el new rol dto seleccionado
	 */
	public void setRolDTOSeleccionado(RolDTO rolDTOSeleccionado) {
		this.rolDTOSeleccionado = rolDTOSeleccionado;
	}

	/**
	 * Obtiene lista privilegio dto.
	 *
	 * @return lista privilegio dto
	 */
	public List<PrivilegioDTO> getListaPrivilegioDTO() {
		return listaPrivilegioDTO;
	}

	/**
	 * Establece el lista privilegio dto.
	 *
	 * @param listaPrivilegioDTO el new lista privilegio dto
	 */
	public void setListaPrivilegioDTO(List<PrivilegioDTO> listaPrivilegioDTO) {
		this.listaPrivilegioDTO = listaPrivilegioDTO;
	}

	/**
	 * Comprueba si es ver check.
	 *
	 * @return true, si es ver check
	 */
	public boolean isVerCheck() {
		return verCheck;
	}

	/**
	 * Establece el ver check.
	 *
	 * @param verCheck el new ver check
	 */
	public void setVerCheck(boolean verCheck) {
		this.verCheck = verCheck;
	}

	/**
	 * Comprueba si es ver opciones.
	 *
	 * @return true, si es ver opciones
	 */
	public boolean isVerOpciones() {
		return verOpciones;
	}

	/**
	 * Establece el ver opciones.
	 *
	 * @param verOpciones el new ver opciones
	 */
	public void setVerOpciones(boolean verOpciones) {
		this.verOpciones = verOpciones;
	}

	/**
	 * Obtiene style txt finicio.
	 *
	 * @return style txt finicio
	 */
	public String getStyleTxtFinicio() {
		return styleTxtFinicio;
	}


	/**
	 * Establece el style txt finicio.
	 *
	 * @param styleTxtFinicio el new style txt finicio
	 */
	public void setStyleTxtFinicio(String styleTxtFinicio) {
		this.styleTxtFinicio = styleTxtFinicio;
	}

	/**
	 * Obtiene style txt ffin.
	 *
	 * @return style txt ffin
	 */
	public String getStyleTxtFfin() {
		return styleTxtFfin;
	}

	/**
	 * Establece el style txt ffin.
	 *
	 * @param styleTxtFfin el new style txt ffin
	 */
	public void setStyleTxtFfin(String styleTxtFfin) {
		this.styleTxtFfin = styleTxtFfin;
	}

	/**
	 * Comprueba si es vis button nuevo.
	 *
	 * @return true, si es vis button nuevo
	 */
	public boolean isVisButtonNuevo() {
		return visButtonNuevo;
	}

	/**
	 * Establece el vis button nuevo.
	 *
	 * @param visButtonNuevo el new vis button nuevo
	 */
	public void setVisButtonNuevo(boolean visButtonNuevo) {
		this.visButtonNuevo = visButtonNuevo;
	}

	/**
	 * Comprueba si es vis list perfil nuevo cancelar.
	 *
	 * @return true, si es vis list perfil nuevo cancelar
	 */
	public boolean isVisListPerfilNuevoCancelar() {
		return visListPerfilNuevoCancelar;
	}

	/**
	 * Establece el vis list perfil nuevo cancelar.
	 *
	 * @param visListPerfilNuevoCancelar el new vis list perfil nuevo cancelar
	 */
	public void setVisListPerfilNuevoCancelar(boolean visListPerfilNuevoCancelar) {
		this.visListPerfilNuevoCancelar = visListPerfilNuevoCancelar;
	}

	/**
	 * Comprueba si es vis list perfil editar cancelar.
	 *
	 * @return true, si es vis list perfil editar cancelar
	 */
	public boolean isVisListPerfilEditarCancelar() {
		return visListPerfilEditarCancelar;
	}

	/**
	 * Establece el vis list perfil editar cancelar.
	 *
	 * @param visListPerfilEditarCancelar el new vis list perfil editar cancelar
	 */
	public void setVisListPerfilEditarCancelar(
			boolean visListPerfilEditarCancelar) {
		this.visListPerfilEditarCancelar = visListPerfilEditarCancelar;
	}

	/**
	 * Obtiene vo n.
	 *
	 * @return vo n
	 */
	public RolNuevoVO getVoN() {
		return voN;
	}

	/**
	 * Establece el vo n.
	 *
	 * @param voN el new vo n
	 */
	public void setVoN(RolNuevoVO voN) {
		this.voN = voN;
	}

	/**
	 * Obtiene mensajelistavacia.
	 *
	 * @return mensajelistavacia
	 */
	public String getMensajelistavacia() {
		return mensajelistavacia;
	}

	/**
	 * Establece el mensajelistavacia.
	 *
	 * @param mensajelistavacia el new mensajelistavacia
	 */
	public void setMensajelistavacia(String mensajelistavacia) {
		this.mensajelistavacia = mensajelistavacia;
	}

	/**
	 * Obtiene size priv.
	 *
	 * @return size priv
	 */
	public int getSizePriv() {
		return sizePriv;
	}

	/**
	 * Establece el size priv.
	 *
	 * @param sizePriv el new size priv
	 */
	public void setSizePriv(int sizePriv) {
		this.sizePriv = sizePriv;
	}

	/**
	 * Obtiene lista r.
	 *
	 * @return lista r
	 */
	public List<RolVO> getListaR() {
		return listaR;
	}

	/**
	 * Establece el lista r.
	 *
	 * @param listaR el new lista r
	 */
	public void setListaR(List<RolVO> listaR) {
		this.listaR = listaR;
	}

	/**
	 * Obtiene error.
	 *
	 * @return error
	 */
	public ErrorVO getError() {
		return error;
	}

	/**
	 * Establece el error.
	 *
	 * @param error el new error
	 */
	public void setError(ErrorVO error) {
		this.error = error;
	}

	/**
	 * Obtiene idpriv.
	 *
	 * @return idpriv
	 */
	public Vector<Long> getIdpriv() {
		return idpriv;
	}

	/**
	 * Establece el idpriv.
	 *
	 * @param idpriv el new idpriv
	 */
	public void setIdpriv(Vector<Long> idpriv) {
		this.idpriv = idpriv;
	}

	/**
	 * Obtiene style txt modulo.
	 *
	 * @return style txt modulo
	 */
	public String getStyleTxtModulo() {
		return styleTxtModulo;
	}

	/**
	 * Establece el style txt modulo.
	 *
	 * @param styleTxtModulo el new style txt modulo
	 */
	public void setStyleTxtModulo(String styleTxtModulo) {
		this.styleTxtModulo = styleTxtModulo;
	}

	/**
	 * Obtiene style txt descripcion.
	 *
	 * @return style txt descripcion
	 */
	public String getStyleTxtDescripcion() {
		return styleTxtDescripcion;
	}

	/**
	 * Establece el style txt descripcion.
	 *
	 * @param styleTxtDescripcion el new style txt descripcion
	 */
	public void setStyleTxtDescripcion(String styleTxtDescripcion) {
		this.styleTxtDescripcion = styleTxtDescripcion;
	}

	/**
	 * Obtiene style txt estado.
	 *
	 * @return style txt estado
	 */
	public String getStyleTxtEstado() {
		return styleTxtEstado;
	}

	/**
	 * Establece el style txt estado.
	 *
	 * @param styleTxtEstado el new style txt estado
	 */
	public void setStyleTxtEstado(String styleTxtEstado) {
		this.styleTxtEstado = styleTxtEstado;
	}

	/**
	 * Obtiene dto.
	 *
	 * @return dto
	 */
	public RolDTO getDto() {
		return dto;
	}

	/**
	 * Establece el dto.
	 *
	 * @param dto el new dto
	 */
	public void setDto(RolDTO dto) {
		this.dto = dto;
	}

	/**
	 * Obtiene time zone.
	 *
	 * @return time zone
	 */
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	/**
	 * Obtiene id.
	 *
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id.
	 *
	 * @param id el new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * Obtiene indicadores adm.
	 *
	 * @return indicadores adm
	 */
	public List<SelectItem> getIndicadoresAdm() {
		try {
			if (!languajeIndicadoresAdm.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) 
			{
				languajeIndicadoresAdm = FacesUtil.getLocale().getLanguage();
				indicadoresAdm = new ArrayList<SelectItem>();
				List<SelectVO> lista = CondicionState.getList(FacesUtil.getLocale());
				if (lista == null) {
					lista = CondicionState.getList(null);
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						indicadoresAdm
								.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);	
		}
		return indicadoresAdm;
	}

	/**
	 * Establece el indicadores adm.
	 *
	 * @param indicadoresAdm el new indicadores adm
	 */
	public void setIndicadoresAdm(List<SelectItem> indicadoresAdm) {
		this.indicadoresAdm = indicadoresAdm;
	}

	/**
	 * Obtiene size.
	 *
	 * @return size
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Obtiene estados.
	 *
	 * @return estados
	 */
	public List<SelectItem> getEstados() {
		try {
			if (!languajeEstados.equalsIgnoreCase(FacesUtil.getLocale().getLanguage()))  {
				languajeEstados = FacesUtil.getLocale().getLanguage();
				estados = new ArrayList<SelectItem>();
				List<SelectVO> lista = EstadoState.getList(FacesUtil.getLocale());
				if (lista == null) {
					lista = EstadoState.getList(null);
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						estados.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return estados;
	}

	/**
	 * Obtiene modulos.
	 *
	 * @return modulos
	 */
	public List<SelectItem> getModulos() {
		try {
			if (modulos == null) {
				modulos = new ArrayList<SelectItem>();
				List<ModuloDTO> lista = null;
				if (lista == null) {
					lista = new ArrayList<ModuloDTO>();
						lista = administracionServiceLocal.listarModulos();
				}
				if (lista != null) {
					for (ModuloDTO moduloDTO : lista) {
						modulos.add(new SelectItem(moduloDTO.getId(), moduloDTO
								.getNombre()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return modulos;
	}
	
	/**
	 * Obtiene acciones.
	 *
	 * @return acciones
	 */
	public List<SelectItem> getAcciones() {
		try {
			if (!languajeAcciones.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) {
				languajeAcciones = FacesUtil.getLocale().getLanguage();
				acciones = new ArrayList<SelectItem>();
				List<SelectVO> lista = FechaAccionType.getList(FacesUtil.getLocale());
				if (lista == null) {
					lista = FechaAccionType.getList(null);
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						acciones.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return acciones;
	}

	/**
	 * Obtiene style txt nombre.
	 *
	 * @return style txt nombre
	 */
	public String getStyleTxtNombre() {
		return styleTxtNombre;
	}

	/**
	 * Obtiene vo.
	 *
	 * @return vo
	 */
	public RolCriteriaVO getVo() {
		return vo;
	}

	/**
	 * Establece el vo.
	 *
	 * @param vo el new vo
	 */
	public void setVo(RolCriteriaVO vo) {
		this.vo = vo;
	}

	/**
	 * Establece el size.
	 *
	 * @param size el new size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Establece el estados.
	 *
	 * @param estados el new estados
	 */
	public void setEstados(List<SelectItem> estados) {
		this.estados = estados;
	}

	/**
	 * Establece el modulos.
	 *
	 * @param modulos el new modulos
	 */
	public void setModulos(List<SelectItem> modulos) {
		this.modulos = modulos;
	}

	/**
	 * Establece el acciones.
	 *
	 * @param acciones el new acciones
	 */
	public void setAcciones(List<SelectItem> acciones) {
		this.acciones = acciones;
	}

	/**
	 * Obtiene lista.
	 *
	 * @return lista
	 */
	public List<RolDTO> getLista() {
		return lista;
	}

	/**
	 * Establece el lista.
	 *
	 * @param lista el new lista
	 */
	public void setLista(List<RolDTO> lista) {
		this.lista = lista;
	}

	/**
	 * Establece el style txt nombre.
	 *
	 * @param styleTxtNombre el new style txt nombre
	 */
	public void setStyleTxtNombre(String styleTxtNombre) {
		this.styleTxtNombre = styleTxtNombre;
	}

	
	/**
	 * Comprueba si es vis list perfil nuevo.
	 *
	 * @return true, si es vis list perfil nuevo
	 */
	public boolean isVisListPerfilNuevo() {
		return visListPerfilNuevo;
	}

	/**
	 * Establece el vis list perfil nuevo.
	 *
	 * @param visListPerfilNuevo el new vis list perfil nuevo
	 */
	public void setVisListPerfilNuevo(boolean visListPerfilNuevo) {
		this.visListPerfilNuevo = visListPerfilNuevo;
	}

	/**
	 * Comprueba si es vis list perfil editar.
	 *
	 * @return true, si es vis list perfil editar
	 */
	public boolean isVisListPerfilEditar() {
		return visListPerfilEditar;
	}

	/**
	 * Establece el vis list perfil editar.
	 *
	 * @param visListPerfilEditar el new vis list perfil editar
	 */
	public void setVisListPerfilEditar(boolean visListPerfilEditar) {
		this.visListPerfilEditar = visListPerfilEditar;
	}

	/**
	 * Comprueba si es vis list perfil o.
	 *
	 * @return true, si es vis list perfil o
	 */
	public boolean isVisListPerfilO() {
		return visListPerfilO;
	}

	/**
	 * Establece el vis list perfil o.
	 *
	 * @param visListPerfilO el new vis list perfil o
	 */
	public void setVisListPerfilO(boolean visListPerfilO) {
		this.visListPerfilO = visListPerfilO;
	}

	/**
	 * Comprueba si es vis list perfil.
	 *
	 * @return true, si es vis list perfil
	 */
	public boolean isVisListPerfil() {
		return visListPerfil;
	}

	/**
	 * Establece el vis list perfil.
	 *
	 * @param visListPerfil el new vis list perfil
	 */
	public void setVisListPerfil(boolean visListPerfil) {
		this.visListPerfil = visListPerfil;
	}	

	/**
	 * Obtiene size perfil.
	 *
	 * @return size perfil
	 */
	public int getSizePerfil() {
		return sizePerfil;
	}

	/**
	 * Establece el size perfil.
	 *
	 * @param sizePerfil el new size perfil
	 */
	public void setSizePerfil(int sizePerfil) {
		this.sizePerfil = sizePerfil;
	}

	/**
	 * Obtiene size acceso directo.
	 *
	 * @return size acceso directo
	 */
	public int getSizeAccesoDirecto() {
		return sizeAccesoDirecto;
	}

	/**
	 * Establece el size acceso directo.
	 *
	 * @param sizeAccesoDirecto el new size acceso directo
	 */
	public void setSizeAccesoDirecto(int sizeAccesoDirecto) {
		this.sizeAccesoDirecto = sizeAccesoDirecto;
	}

	/**
	 * Obtiene size privilegio.
	 *
	 * @return size privilegio
	 */
	public int getSizePrivilegio() {
		return sizePrivilegio;
	}

	/**
	 * Establece el size privilegio.
	 *
	 * @param sizePrivilegio el new size privilegio
	 */
	public void setSizePrivilegio(int sizePrivilegio) {
		this.sizePrivilegio = sizePrivilegio;
	}

	/**
	 * Obtiene hidden.
	 *
	 * @return hidden
	 */
	public String getHidden() {
		return hidden;
	}

	/**
	 * Establece el hidden.
	 *
	 * @param hidden el new hidden
	 */
	public void setHidden(String hidden) {
		this.hidden = hidden;
	}
	
	/**
	 * Obtiene idprivilegio.
	 *
	 * @return idprivilegio
	 */
	public Long getIdprivilegio() {
		return idprivilegio;
	}

	/**
	 * Establece el idprivilegio.
	 *
	 * @param idprivilegio el new idprivilegio
	 */
	public void setIdprivilegio(Long idprivilegio) {
		this.idprivilegio = idprivilegio;
	}

	/** La direccionar. */
	private String direccionar;

	/**
	 * Obtiene direccionar.
	 *
	 * @return direccionar
	 */
	public String getDireccionar() {
		return direccionar;
	}

	/**
	 * Establece el direccionar.
	 *
	 * @param direccionar el new direccionar
	 */
	public void setDireccionar(String direccionar) {
		this.direccionar = direccionar;
	}

	
	/**
	 * Comprueba si es vis list rol nuevo.
	 *
	 * @return true, si es vis list rol nuevo
	 */
	public boolean isVisListRolNuevo() {
		return visListRolNuevo;
	}

	/**
	 * Establece el vis list rol nuevo.
	 *
	 * @param visListRolNuevo el new vis list rol nuevo
	 */
	public void setVisListRolNuevo(boolean visListRolNuevo) {
		this.visListRolNuevo = visListRolNuevo;
	}

	/**
	 * Comprueba si es vis list rol editar.
	 *
	 * @return true, si es vis list rol editar
	 */
	public boolean isVisListRolEditar() {
		return visListRolEditar;
	}

	/**
	 * Establece el vis list rol editar.
	 *
	 * @param visListRolEditar el new vis list rol editar
	 */
	public void setVisListRolEditar(boolean visListRolEditar) {
		this.visListRolEditar = visListRolEditar;
	}	

	/**
	 * Obtiene list p1.
	 *
	 * @return list p1
	 */
	public List<PrivilegioVO> getListP1() {
		return listP1;
	}

	/**
	 * Establece el list p1.
	 *
	 * @param listP1 el new list p1
	 */
	public void setListP1(List<PrivilegioVO> listP1) {
		this.listP1 = listP1;
	}

	/**
	 * Obtiene list p2.
	 *
	 * @return list p2
	 */
	public List<PrivilegioVO> getListP2() {
		return listP2;
	}

	/**
	 * Establece el list p2.
	 *
	 * @param listP2 el new list p2
	 */
	public void setListP2(List<PrivilegioVO> listP2) {
		this.listP2 = listP2;
	}

	/**
	 * Obtiene id perfil.
	 *
	 * @return id perfil
	 */
	public Long getIdPerfil() {
		return idPerfil;
	}

	/**
	 * Establece el id perfil.
	 *
	 * @param idPerfil el new id perfil
	 */
	public void setIdPerfil(Long idPerfil) {
		this.idPerfil = idPerfil;
	}

	/**
	 * Obtiene nombre anterior.
	 *
	 * @return nombre anterior
	 */
	public String getNombreAnterior() {
		return nombreAnterior;
	}

	/**
	 * Establece el nombre anterior.
	 *
	 * @param nombreAnterior el new nombre anterior
	 */
	public void setNombreAnterior(String nombreAnterior) {
		this.nombreAnterior = nombreAnterior;
	}
	
	/**
	 * Obtiene flag external.
	 *
	 * @return flag external
	 */
	public String getFlagExternal() {
		return flagExternal;
	}

	/**
	 * Establece el flag external.
	 *
	 * @param flagExternal el new flag external
	 */
	public void setFlagExternal(String flagExternal) {
		this.flagExternal = flagExternal;
	}
	
	/**
	 * Comprueba si es aceptar cambios.
	 *
	 * @return true, si es aceptar cambios
	 */
	public boolean isAceptarCambios() {
		return aceptarCambios;
	}

	/**
	 * Establece el aceptar cambios.
	 *
	 * @param aceptarCambios el new aceptar cambios
	 */
	public void setAceptarCambios(boolean aceptarCambios) {
		this.aceptarCambios = aceptarCambios;
	}

	/**
	 * Obtiene indicador tipo fecha.
	 *
	 * @return indicador tipo fecha
	 */
	public String getIndicadorTipoFecha() {
		return indicadorTipoFecha;
	}


	/**
	 * Establece el indicador tipo fecha.
	 *
	 * @param indicadorTipoFecha el new indicador tipo fecha
	 */
	public void setIndicadorTipoFecha(String indicadorTipoFecha) {
		this.indicadorTipoFecha = indicadorTipoFecha;
	}
	
	/**
	 * Obtiene estado filtrado.
	 *
	 * @return estado filtrado
	 */
	public List<SelectItem> getEstadoFiltrado() {
		try {
			if (!languajeEstadoFiltrado.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) {
				languajeEstadoFiltrado = FacesUtil.getLocale().getLanguage();
				estadoFiltrado = new ArrayList<SelectItem>();
				List<SelectVO> lista = EstadoState.getList(FacesUtil.getLocale());
				if (lista.size() != 0) {
				estadoFiltrado.add(new SelectItem(lista.get(0).getId(), lista.get(0).getValue()));
			   }
			}	
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return estadoFiltrado;
	}

	/**
	 * Establece el estado filtrado.
	 *
	 * @param estadoFiltrado el new estado filtrado
	 */
	public void setEstadoFiltrado(List<SelectItem> estadoFiltrado) {	
		this.estadoFiltrado = estadoFiltrado;
	}

	/**
	 * Obtiene page actual.
	 *
	 * @return page actual
	 */
	public String getPageActual() {
		return pageActual;
	}

	/**
	 * Establece el page actual.
	 *
	 * @param pageActual el new page actual
	 */
	public void setPageActual(String pageActual) {
		this.pageActual = pageActual;
	}

	/**
	 * Comprueba si es estado seleccion.
	 *
	 * @return true, si es estado seleccion
	 */
	public boolean isEstadoSeleccion() {
		return estadoSeleccion;
	}

	/**
	 * Establece el estado seleccion.
	 *
	 * @param estadoSeleccion el new estado seleccion
	 */
	public void setEstadoSeleccion(boolean estadoSeleccion) {
		this.estadoSeleccion = estadoSeleccion;
	}

	/**
	 * Obtiene cant filas.
	 *
	 * @return cant filas
	 */
	public int getCantFilas() {
		return cantFilas;
	}

	/**
	 * Establece el cant filas.
	 *
	 * @param cantFilas el new cant filas
	 */
	public void setCantFilas(int cantFilas) {
		this.cantFilas = cantFilas;
	}	
}