package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import pe.gob.minedu.escale.adm.business.exception.UsuarioEntidadExisteException;
import pe.gob.minedu.escale.adm.business.type.FechaAccionType;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.business.type.TipoAccionAdministrarUsuario;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.NotificacionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.OrganismoDTO;
import pe.gob.minedu.escale.adm.model.dto.OrganismoPerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilRolDTO;
import pe.gob.minedu.escale.adm.model.dto.PrivilegioDTO;
import pe.gob.minedu.escale.adm.model.dto.RolDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioRolDTO;
import pe.gob.minedu.escale.adm.vo.BusquedaUsuarioVO;
import pe.gob.minedu.escale.adm.vo.SelectVO;
import pe.gob.minedu.escale.adm.vo.UsuarioCriteriaVO;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.CondicionState;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.business.state.UsuarioState;
import pe.gob.minedu.escale.common.constans.IndicadorConstant;
import pe.gob.minedu.escale.common.dto.FactoryBean;
import pe.gob.minedu.escale.common.exception.ApplicationException;
import pe.gob.minedu.escale.common.exception.BusinessException;
import pe.gob.minedu.escale.common.notify.NotificacionUtil;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;
import pe.gob.minedu.escale.common.web.jsf.util.FacesUtil;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_BUSCAR_SOLICITUD_DESACTIVACION_ADMINISTRADOR;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_ENTIDAD_TO_USUARIO;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_MODIFICAR_ENTIDAD_TO_USUARIO;

@ManagedBean(name="usuarioBean")
@SessionScoped
public class UsuarioBean extends BaseBean implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 5523915987703096617L;

	/** El servicio de administracion service local. */
	@EJB
	private transient AdministracionServiceLocal administracionServiceLocal;

	/** El servicio usuario service local. */
	@EJB
	private  UsuarioServiceLocal usuarioServiceLocal;

	/** El servicio de notificacion service local. */
	@EJB
	private  NotificacionServiceLocal notificacionServiceLocal;

	/** La Constante FORMULARIO_USUARIO. */
	private static final String FORMULARIO_USUARIO = "formularioUsuario";

        /** La Constante FORMULARIO_BUSCAR_USUARIO. */
	@SuppressWarnings("unused")
	private static final String FORMULARIO_BUSCAR_USUARIO = "verGestionUsuario";

	/** La Constante OBJECT_INDICADOR_VEDU. */
	private static final String OBJECT_INDICADOR_VEDU = "objectIndicadorVEDU";

	/** La Constante INPUT_ERROR_NO_UPPER. */
	@SuppressWarnings("unused")
	private static final String INPUT_ERROR_NO_UPPER = "inputErrorNoUpper";

	/** La Constante OBJECT_USUARIO_ORGANISMO. */
	private static final String OBJECT_USUARIO_ORGANISMO = "objectUsuarioOrganismo";

        /** La Constante CALL_DESACTIVAR_USUARIO. */
	public static final String CALL_DESACTIVAR_USUARIO = "DEU";

	/** La Constante INPUT_ERROR. */
	@SuppressWarnings("unused")
	private static final String INPUT_ERROR = "inputError";

	/** El objeto usuario criteria. */
	private UsuarioCriteriaVO criteria = new UsuarioCriteriaVO();

	/** El objeto usuario detalle. */
	private UsuarioOrganismoDTO usuarioDetalle;

	/** El objeto usuario detalle inicial temporal. */
	@SuppressWarnings("unused")
	private UsuarioOrganismoDTO usuarioDetalleInicialTemporal;
        
	/** El codigo perfil. */
	@SuppressWarnings("unused")
	private String codigoPerfil;
        
	/** El codigo pliego. */
	@SuppressWarnings("unused")
	private String codigoPliego;

	/** El codigo estado. */
	@SuppressWarnings("unused")
	private String codigoEstado;

	/** El tipo documento. */
	@SuppressWarnings("unused")
	private String tipoDocumento;

	/** La fecha accion desde. */
	@SuppressWarnings("unused")
	private Date fechaAccionDesde;

	/** La fecha accion hasta. */
	private Date fechaAccionHasta;

	/** El mensaje error. */
	private String mensajeError = null;

	/** La lista tipo documento. */
	private List<SelectItem> listaTipoDocumento;

	/** La lista perfil. */
	private List<SelectItem> listaPerfil;

	/** La lista rol. */
	private List<SelectItem> listaRol;
	/** La lista estado. */
	private List<SelectItem> listaEstado;

        /** La lista de indicadores adm. */
	private List<SelectItem> indicadoresAdm;
        
	/** La lista estado usuario organismo. */
	private List<SelectItem> listaEstadoUsuarioOrganismo;
        
	/** La lista acciones. */
	private List<SelectItem> listaAcciones;

	/** El flag. */
	@SuppressWarnings("unused")
	private String flag;

	/** La lista usuario. */
	private List<BusquedaUsuarioVO> listaUsuario;

	/** La lista rol dto. */
	private List<UsuarioRolDTO> listaRolDTO = new ArrayList<UsuarioRolDTO>();

	/** La lista privilegio dto. */
	private List<PrivilegioDTO> listaPrivilegioDTO = new ArrayList<PrivilegioDTO>();

	/** La lista privilegio dto memory. */
	private List<PrivilegioDTO> listaPrivilegioDTOMemory; // mgj

	/** La lista rol dto memory. */
	private List<UsuarioRolDTO> listaRolDTOMemory;

	/** El id perfil. */
	@SuppressWarnings("unused")
	private String idPerfil;

	/** El style perfil. */
	@SuppressWarnings("unused")
	private String stylePerfil;

	/** El style descripcion. */
	@SuppressWarnings("unused")
	private String styleDescripcion;

	/** El style rol. */
	@SuppressWarnings("unused")
	private String styleRol;

	/** El call detalle entidad detalle usuario. */
	@SuppressWarnings("unused")
	private String callDetalleEntidadDetalleUsuario;

	/** El call detalle historio entidad detalle usuario. */
	@SuppressWarnings("unused")
	private String callDetalleHistorioEntidadDetalleUsuario;

	/** El call buscar historico usuario detalle usuario. */
	@SuppressWarnings("unused")
	private String callBuscarHistoricoUsuarioDetalleUsuario;

	/** El id organismo. */
	private Long idOrganismo;

	/** El id persona. */
	private Long idPersona;

	/** El nombre organismo. */
	private String nombreOrganismo;

	/** El style nombre usuario. */
	@SuppressWarnings("unused")
	private String styleNombreUsuario;

	/** El flag indicador ambito. */
	private boolean indicadorAmbito = true;

	/** El flag indicador vedu. */
	private boolean indicadorVEDU;

	/** El mensaje lista vacia. */
	private String mensajelistavacia;

	/** El mensaje dni. */
	private String mensajeDNI;

	/** El mensaje fecha. */
	private String mensajeFecha;

	/** EL nombre perfil. */
	private String nombrePerfil;

	/** El flag deshabilitar. */
	private boolean deshabilitar = false;

	/** El flag creacion. */
	private String flagCreacion;

	/** El usuario state. */
	@SuppressWarnings("unused")
	private UsuarioState usuarioState;

	/** El estado default. */
	@SuppressWarnings("unused")
	private String estadoDefault;

	/** El flag external. */
	private String flagExternal = null;

	/** El flag session. */
	private boolean flagSession = false;

	/** El contexto. */
    private ServiceContext context = new ServiceContext();

	/** El size. */
	private int size;

	/** El flag ver check. */
	private boolean verCheck = false;

	/** El flag validar email. */
	@SuppressWarnings("unused")
	private boolean validarEmail = false;

	/** El flag resgitro exitoso. */
	private boolean flagResgitroExitoso;
	/** El size roles. */
	private int sizeRoles;

	/** EL size privilegios. */
	@SuppressWarnings("unused")
	private int sizePrivilegios;


	/** El estado desccripcion. */
	private String estadoDesccripcion;

	/** El flag editar usuario. */
	private boolean editarUsuario = false;

	/** La lista estado filtrado. */
	private List<SelectItem> estadoFiltrado;

	/** El indicador estado usuario. */
	private String indicadorEstadoUsuario = "";

	/** El flag indicador seleccionar todo. */
	private boolean indicadorSeleccionarTodo = false;

	/** El flag indicador seleccionar todo rol. */
	private boolean indicadorSeleccionarTodoRol = false;

	/** La paguina actual. */
	private String pageActual;

	/** El flag valida click. */
	private boolean validaClick;
	
	/** El flag bloqueado intentos fallidos. */
	private boolean bloqueadoIntentosFallidos = true;

	/** El flag bloqueado expiracion. */
	private boolean bloqueadoExpiracion = false;

	/** El flag bloqueado caducidad. */
	private boolean bloqueadoCaducidad = false;

	/** La cantidad filas. */
	private int cantFilas = 0;

	/** El lenguaje estado. */
	private String languajeEstado = "";
        
        /** El lenguaje indicadores adm. */
	private String languajeIndicadoresAdm = "";

	/** El flag deshabilitar borr. */
	private boolean deshabilitarBorr;

	/** El lenguaje estado usuario. */
	private String languajeEstadoUsuario = "";

	/** El lenguaje acciones. */
	private String languajeAcciones = "";

	/** El lenguaje estado filtrado. */
	private String languajeEstadoFiltrado = "";

	/** El control message util. */
	private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(
			getErrorService(), UsuarioBean.class);

       /** El flag indicador requerido. */
	private boolean indicadorRequerido = true;


        private String flagTipoDocumento = "";
	/**
	 * Instancia un nuevo usuario bean.
	 */
	public UsuarioBean() {
	}

	/**
	 * Post construct.
	 */
	@PostConstruct
	public void postConstruct() {
		try {
                        usuarioDetalle = new UsuarioOrganismoDTO();
//			inicializarValores();
//			flagResgitroExitoso = true;
			context = FacesUtil.getContext();
//			usuarioDetalle = FactoryBean.getBean(UsuarioOrganismoDTO.class);
//			idOrganismo = context.getUsuarioSessionVO().getIdEntidad();
//			UsuarioSessionVO usuario = context.getUsuarioSessionVO();
//			indicadorAmbito = false;
//			if ((TipoAgrupadorSeguridad.PERFIL_FUNCIONARIO_MINEDU.getValue()
//					.compareTo(usuario.getIdPerfil1()) == 0)) {
//				for (Long idRol : usuario.getRoles()) {
//					if (TipoAgrupadorSeguridad.ROL_ADMINISTRADOR_ENTIDAD_MINEDU
//							.getValue().compareTo(idRol) == 0
//							|| TipoAgrupadorSeguridad.ROL_ADMINISTRADOR_SISTEMA
//									.getValue().compareTo(idRol) == 0
//							|| TipoAgrupadorSeguridad.ROL_ADMINISTRADOR_USUARIOS
//									.getValue().compareTo(idRol) == 0) {
//						indicadorAmbito = true;
//						break;
//					}
//				}
//			}
			//req 114 - 12/2013
//			for (Long idRol : usuario.getRoles()) {
//				if (TipoAgrupadorSeguridad.ROL_ADMINISTRADOR_ENTIDAD
//						.getValue().compareTo(idRol) == 0){
//					setHasRolAdministradorEntidad(true);
//				}
//				if (TipoAgrupadorSeguridad.ROL_ADMINISTRADOR_ENTIDAD_MINEDU
//						.getValue().compareTo(idRol) == 0){
//					setHasRolAdministracionRec(true);
//				}
//			}
//			cantFilas = FormatterUtil.toIntPrimivite(administracionServiceLocal
//					.getParametro(ParametroType.CANTIDAD_FILAS_DT_ADM
//							.getValue()));
//			this.inicializarDepartamentos();

		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}


	/**
	 * Inicializar valores no dni.
	 */
	public void inicializarValoresNoDNI() {
		try {
			if (usuarioDetalle.getUsuario() == null) {
				usuarioDetalle.setUsuario(new UsuarioDTO());
			}
			usuarioDetalle.getUsuario().setId(null);
			usuarioDetalle.getUsuario().setId(null);
			usuarioDetalle.getUsuario().setNombres(StringUtils.EMPTY);
                        usuarioDetalle.getUsuario().setApellidoPaterno(StringUtils.EMPTY);
			usuarioDetalle.getUsuario().setApellidoMaterno(StringUtils.EMPTY);
			usuarioDetalle.getUsuario().setEmail(StringUtils.EMPTY);

		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Cargar rol.
	 *
	 * @param event
	 *            el event
	 */
	public void cargarRol(ValueChangeEvent event) {
		if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
			event.setPhaseId(PhaseId.INVOKE_APPLICATION);
			event.queue();
			return;
		}
		try {
			if (event != null) {
				String data = (String) event.getNewValue();
				if (data != null && !data.equals("-1")) {
					listaRol = new ArrayList<SelectItem>();
					List<RolDTO> listaRolDTO = administracionServiceLocal
							.listaRolesxPerfil(Long.valueOf(data));
					for (RolDTO dto : listaRolDTO) {
						listaRol.add(new SelectItem(
								String.valueOf(dto.getId()), String.valueOf(dto
										.getNombre())));

					}
				} else {
					if (listaRol != null) {
						listaRol.clear();
					}

				}
			}
		} catch (Exception e) {
			mensajeError = controlMessageUtil.imprimirMessage(e,
					FacesMessage.SEVERITY_ERROR);
		}
	}
//
	/**
	 * Limpiar fecha inicio.
	 *
	 * @param ev
	 *            el ev
	 */
	public void limpiarFechaInicio(ActionEvent ev) {
		this.criteria.setFechaDesdeAccion(null);
	}

	/**
	 * Limpiar fecha fin.
	 *
	 * @param ev
	 *            el ev
	 */
	public void limpiarFechaFin(ActionEvent ev) {
		this.criteria.setFechaHastaAccion(null);
	}

	/**
	 * Procesar busqueda.
	 *
	 * @param event
	 *            el event
	 */
	public void procesarBusqueda(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		this.editarUsuario = false;
		size = 0;
		try {
			if (!validateBuscar()) {
                                listaUsuario = usuarioServiceLocal.buscarUsuario(context,
						criteria);
				size = listaUsuario.size();
				if (size == 0) {
					mensajelistavacia = controlMessageUtil
							.getMessage(ConstantesUtil.MENSAJE_LISTA_VACIA);
				}

			} else {
				size = 0;
				controlMessageUtil
						.imprimirMessageClave(
								ConstantesUtil.MENSAJE_ERROR_MINIMO_UN_CAMPO_OBLIGATORIO,
								FacesMessage.SEVERITY_ERROR);
			}
			this.indicadorSeleccionarTodo = false;
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Limpiar busqueda.
	 *
	 * @param event
	 *            el event
	 */
	public void limpiarBusqueda(ActionEvent event) {
		try {
			this.editarUsuario = false;
			size = 0;
			sizePrivilegios = 0;
			sizeRoles = 0;
			limpiarErrorBusquedaUsuario();
			setMensajeDNI(""); // mensaje de existencia de dni y/o continuidad
								// del registro del usuario.
			setMensajeError(""); // mensaje de error de formato del nro de dni.
			setValidaClick(false); // bandera q indica q se dio click a la lupa
			setDeshabilitar(false); // deshabilita campos de texto

			usuarioDetalle = this.cargarUsuarioOrganismoDTO();
			criteria = new UsuarioCriteriaVO();
			if (flagExternal != null) {
				if (flagExternal.equalsIgnoreCase(CALL_BUSCAR_SOLICITUD_DESACTIVACION_ADMINISTRADOR) || flagExternal.equalsIgnoreCase(CALL_DESACTIVAR_USUARIO)) {
					criteria.setEntidad(FacesUtil.getContext().getUsuarioSessionVO().getNombreEntidad());
				}
			}

			if (listaUsuario != null) {
				listaUsuario.clear();
				mensajelistavacia = StringUtils.EMPTY;
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Limpiar error busqueda usuario.
	 */
	public void limpiarErrorBusquedaUsuario() {
		this.styleNombreUsuario = "";
	}

	/**
	 * Nuevo usuario.
	 *
	 * @param event
	 *            el event
	 * @throws Exception
	 *             the exception
	 */
	public void nuevoUsuario(ActionEvent event) throws Exception {
		try {
			this.editarUsuario = false;
                        setFlagCreacion(TipoAccionAdministrarUsuario.NUEVO_USUARIO
					.getValue());
			reestablecerValores();
			establecerValoresNuevo();
			if (indicadorAmbito) {
				idOrganismo = null;
				this.nombreOrganismo = StringUtils.EMPTY;
			} else {
				idOrganismo = context.getUsuarioSessionVO().getIdEntidad();
				this.nombreOrganismo = context.getUsuarioSessionVO().getNombreEntidad();
				sizePrivilegios = 0;
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Detalle usuario.
	 *
	 * @param event
	 *            el event
	 */
	public void detalleUsuario(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		this.editarUsuario = false;
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("editPersona");
			idPersona = Long.parseLong(component.getValue().toString());

			UIParameter component2 = (UIParameter) event.getComponent()
					.findComponent("editOrganismo");
			idOrganismo = Long.parseLong(component2.getValue().toString());

			flag = "";
			usuarioDetalle = usuarioServiceLocal.obtenerUsuarioDetalle(context,
					idPersona, idOrganismo);
			bloqueadoCaducidad = false;
			bloqueadoExpiracion = false;
			bloqueadoIntentosFallidos = false;
			if (usuarioDetalle.getUsuario().getEstado()
					.equals(UsuarioState.BLOQUEADO_CADUCIDAD.getKey())) {
				this.bloqueadoCaducidad = true;
			} else if (usuarioDetalle.getUsuario().getEstado()
					.equals(UsuarioState.BLOQUEADO_EXPIRADO.getKey())) {
				this.bloqueadoExpiracion = true;
			} else if (usuarioDetalle.getUsuario().getEstado()
					.equals(UsuarioState.BLOQUEADO_INTENTOS_FALLIDOS.getKey())) {
				this.bloqueadoIntentosFallidos = true;
			}
			if (bloqueadoCaducidad || bloqueadoExpiracion
					|| bloqueadoIntentosFallidos) {
				Date fechaBloqueo = this.administracionServiceLocal
						.fechaBloqueo(usuarioDetalle.getUsuario().getId());
				usuarioDetalle.getUsuario().setUltimaFechaModificacion(
						fechaBloqueo);
			}
			listaRolDTO = administracionServiceLocal
					.listarUsuarioRolxUsuarioxOrganismo(idPersona, idOrganismo);
			// Obtenemos los privilegios
			if (listaPrivilegioDTO != null) {
				listaPrivilegioDTO.clear();
			}
			if (listaRolDTO != null) {
				for (UsuarioRolDTO dto : listaRolDTO) {
					List<PrivilegioDTO> lstPrivilegios = new ArrayList<PrivilegioDTO>();
					lstPrivilegios = administracionServiceLocal
							.listaPrivilegioxRol(dto.getPerfilRol().getRol()
									.getId());
					for (PrivilegioDTO privilegio : lstPrivilegios) {
						if (!listaPrivilegioDTO.contains(privilegio)) {
							listaPrivilegioDTO.add(privilegio);
						}
					}
				}

				sizeRoles = listaRolDTO.size();
			}

			if (listaPrivilegioDTO != null) {
				sizePrivilegios = listaPrivilegioDTO.size();
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Editar usuario.
	 *
	 * @param event
	 *            el event
	 */
	public void editarUsuario(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		try {
			// Normalizando los estilos de los inputs
			mensajeDNI = StringUtils.EMPTY;
			mensajeError = StringUtils.EMPTY;
			setFlagCreacion(TipoAccionAdministrarUsuario.MODIFICAR_USUARIO
					.getValue());
			setValidaClick(true);
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("editPersona");
			idPersona = Long.parseLong(component.getValue().toString());
			UIParameter component2 = (UIParameter) event.getComponent()
					.findComponent("editOrganismo");
			idOrganismo = Long.parseLong(component2.getValue().toString());
			if (this.flagExternal == null) {
				flagResgitroExitoso = true;
			}
			usuarioDetalle = usuarioServiceLocal.obtenerUsuario(context,
					idPersona, idOrganismo);
                        usuarioDetalle.getUsuario().setCodigoOIDAnterior(usuarioDetalle.getUsuario().getCodigoOID());
			this.setEstadoDesccripcion(EstadoState.get(
					usuarioDetalle.getEstado()).getDescription(
					FacesUtil.getLocale()));
			nombreOrganismo = usuarioDetalle.getOrganismo()
					.getNombreOrganismo();
			nombrePerfil = usuarioDetalle.getOrganismoPerfil().getPerfil()
					.getNombre();

			listaRolDTOMemory = new ArrayList<UsuarioRolDTO>();
			listaRolDTO = new ArrayList<UsuarioRolDTO>();
			listaRolDTO = administracionServiceLocal
					.listarUsuarioRolPerfilxUsuarioxOrganismo(idPersona,
							idOrganismo);
			for (UsuarioRolDTO roldto : listaRolDTO) {
				roldto.setHidden(true);
			}
			listaRolDTOMemory.addAll(listaRolDTO);
			List<RolDTO> listaRolPerfil = administracionServiceLocal
					.listaRolesxPerfil(usuarioDetalle.getOrganismoPerfil()
							.getPerfil().getId());
			for (RolDTO rol : listaRolPerfil) {
				boolean ind = false;
				for (UsuarioRolDTO roldto : listaRolDTO) {
					if (rol.getId().equals(
							roldto.getPerfilRol().getRol().getId())) {
						ind = true;
					}
				}
				if (!ind) {
					UsuarioRolDTO usuarioRolDTO = new UsuarioRolDTO();
					usuarioRolDTO.setPerfilRol(new PerfilRolDTO());
					usuarioRolDTO.getPerfilRol().setRol(rol);
					listaRolDTOMemory.add(usuarioRolDTO);
				}
			}
			listaRolDTO = new ArrayList<UsuarioRolDTO>();
			listaRolDTO.addAll(listaRolDTOMemory);
			// Si el usuario seleccionado se encuentra activo en ralacion con
			// organismo no puede pasar a inactivo
			if (usuarioDetalle.getEstado().equalsIgnoreCase(
					EstadoState.ACTIVO.getKey())) {
				this.indicadorEstadoUsuario = ""
						+ IndicadorConstant.INDICADOR_ACTIVO + ""; // "1";
			} else {
				this.indicadorEstadoUsuario = "";
			}
			this.sizeRoles = listaRolDTO.size();
			this.sizePrivilegios = listaPrivilegioDTO.size();
			this.editarUsuario = true;
			for (UsuarioRolDTO roldto : listaRolDTO) {
				if (!roldto.isHidden()) {
					this.indicadorSeleccionarTodo = false;
					break;
				} else {
					this.indicadorSeleccionarTodo = true;
				}
			}
			if (usuarioDetalle.getOrganismo().getEstado()
					.equals(EstadoState.INACTIVO.getKey())) {
				controlMessageUtil.imprimirMessageClave(
						ConstantesUtil.MENSAJE_ORGANISMO_INACTIVO,
						FacesMessage.SEVERITY_WARN);
			}

		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}


	/**
	 * Filtrar rol.
	 *
	 * @param event
	 *            el event
	 */

	public void filtrarRol(ValueChangeEvent event) {
		try {
			listaRolDTO.size();
			listaPrivilegioDTO.size();
			UIParameter uip = (UIParameter) event.getComponent().findComponent(
					"rolIdFiltro");
			String rolIdFiltro = uip.getValue().toString();
			boolean flagCheck = false;
			Long idRol = 0L;
			for (UsuarioRolDTO rol : listaRolDTO) {
				if (rol.getPerfilRol().getRol().getId().toString()
						.equalsIgnoreCase(rolIdFiltro)) {
					flagCheck = rol.isHidden();
					idRol = rol.getPerfilRol().getRol().getId();
					break;
				}
			}
                        if (flagCheck) {
				// quito
				for (Iterator<UsuarioRolDTO> it = listaRolDTO.iterator(); it.hasNext();) {
					UsuarioRolDTO dto = it.next();
					if (dto.getPerfilRol().getRol().getId()== idRol) {
						it.remove();
					}
				}
			}

			this.sizeRoles = listaRolDTO.size();
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Valida roles seleccion.
	 *
	 * @return true, en caso de exito
	 */
	public boolean validaRolesSeleccion() {
		for (UsuarioRolDTO rolDTO : listaRolDTO) {
			if (rolDTO.isHidden()) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Guardar usuario.
	 *
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	
	public String guardarUsuario() throws Exception {
		context.setLocale(FacesUtil.getLocale());
		String paginaRetorno = null;
		UsuarioOrganismoDTO dtoReturn = null;
		boolean flagCreacionUsuario = false;
		String msjeValidaFormato = "";
                //prueba
                validaClick=true;
		try {
			if (!validaClick) {
				setMensajeDNI("");
				controlMessageUtil.imprimirMessageClave(
						ConstantesUtil.MENSAJE_PREVIO_REGISTRO_USUARIO,
						FacesMessage.SEVERITY_ERROR);
//				setStyleNroDoc(INPUT_ERROR_NO_UPPER);
				return "";
			} else {
				setMensajeDNI("");
			}

                        msjeValidaFormato = null;
			if (msjeValidaFormato == null) {
				if (validaRolesSeleccion()) {                                   
					UsuarioDTO usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(
                                                flagCreacion.equals(TipoAccionAdministrarUsuario.NUEVO_USUARIO.getValue())?
                                                        usuarioDetalle.getUsuario().getCodigoOID():
                                                        usuarioDetalle.getUsuario().getCodigoOIDAnterior());
					if (usuarioDTO != null) {
						if (usuarioDTO.getId() != null) {
							indicadorVEDU = true;
						} else if (usuarioDTO != null && usuarioDTO.getId() != null) {
							usuarioDetalle.getUsuario().setId(usuarioDTO.getId());
							indicadorVEDU = false;
						} else {
							indicadorVEDU = false;
						}
					} else {
						usuarioDetalle.getUsuario().setId(null);
						indicadorVEDU = false;
					}
					setMensajeDNI(null);
//					setStyleNroDoc("");
					usuarioDetalle
							.getUsuario()
							.setNombres(
									usuarioDetalle.getUsuario().getNombres().trim().toUpperCase());
					usuarioDetalle
							.getUsuario()
                                                        .setApellidoMaterno(
									usuarioDetalle.getUsuario().getApellidoMaterno().trim().toUpperCase());
                                        usuarioDetalle
							.getUsuario()
                                                        .setApellidoPaterno(
									usuarioDetalle.getUsuario().getApellidoPaterno().trim().toUpperCase());
					usuarioDetalle
							.getUsuario()
							.setEmail(
									usuarioDetalle.getUsuario().getEmail().trim().toUpperCase());
//					usuarioDetalle.setEmailInstitucional(usuarioDetalle.getEmailInstitucional().trim().toUpperCase());

					String nombreCompleto = usuarioDetalle
							.getUsuario()
							.getNombres()
							.concat(" ")
							.concat(usuarioDetalle.getUsuario().getApellidoPaterno())
                                                        .concat(" ")
							.concat(usuarioDetalle.getUsuario().getApellidoMaterno());
					usuarioDetalle.getUsuario().setNombreCompleto(nombreCompleto);


					if (flagExternal != null
							&& (flagExternal.equals(CALL_ENTIDAD_TO_USUARIO) || flagExternal
									.equals(CALL_MODIFICAR_ENTIDAD_TO_USUARIO))) {
						Map<String, Object> session = FacesUtil.getSessionMap();
						session.put(OBJECT_USUARIO_ORGANISMO, usuarioDetalle);
						session.put(OBJECT_INDICADOR_VEDU, indicadorVEDU ? 1
								: 0);
						controlMessageUtil
								.imprimirMessageClave(
										ConstantesUtil.MENSAJE_EXITO_USUARIO_GUARDAR_ADMIN,
										FacesMessage.SEVERITY_INFO);
						paginaRetorno = FORMULARIO_USUARIO;
						flagSession = true;

						/************************************************************************/
						if (flagExternal.equals(CALL_ENTIDAD_TO_USUARIO)) {
							setDeshabilitar(true);
							setDeshabilitarBorr(true);
						}
						/************************************************************************/
					} else {//
						if (flagCreacion
								.equals(TipoAccionAdministrarUsuario.NUEVO_USUARIO
										.getValue())) {
							List<Object[]> lista = new ArrayList<Object[]>();
//							List<Object[]> lista = usuarioServiceLocal
//									.buscarUsuarioDNIEntidad(usuarioDetalle
//											.getUsuario()
//											.getCodigoOID(), idOrganismo);
							if (lista != null && !lista.isEmpty()) {
								controlMessageUtil.imprimirMessageClave(
										ConstantesUtil.ERROR_REGISTRO_USER_DNI,
										FacesMessage.SEVERITY_ERROR);
								return FORMULARIO_USUARIO;
							}
						}
						// aqui es
						List<RolDTO> listaRolTemp = new ArrayList<RolDTO>();
						for (UsuarioRolDTO usuarioRol : listaRolDTO) {
							usuarioRol.getPerfilRol().getRol()
									.setHidden(usuarioRol.isHidden());
							listaRolTemp
									.add(usuarioRol.getPerfilRol().getRol());
						}
						dtoReturn = usuarioServiceLocal
								.administrarUsuarioRegistrar(usuarioDetalle,
										context, listaRolTemp, idOrganismo,
										flagCreacion, indicadorVEDU);
						flagCreacionUsuario = dtoReturn == null ? false : true;
						String msg = flagCreacion
								.equals(TipoAccionAdministrarUsuario.NUEVO_USUARIO
										.getValue()) ? ConstantesUtil.MENSAJE_EXITO_GUARDAR
								: ConstantesUtil.MENSAJE_EXITO_MODIFICAR;

						if (!indicadorVEDU
								&& (flagCreacion
										.equals(TipoAccionAdministrarUsuario.NUEVO_USUARIO
												.getValue()) || flagCreacion
										.equals(TipoAccionAdministrarUsuario.USUARIO_ADMINISTRADOR_ENTIDAD
												.getValue()))) {
							// Envio de correo electronico
							envioCorreoConfirmacion(dtoReturn);
						} else if (!flagCreacion
								.equals(TipoAccionAdministrarUsuario.NUEVO_USUARIO
										.getValue())) {
//							envioCorreoModificacion(dtoReturn);
						}
						controlMessageUtil.imprimirMessageClave(msg,
								FacesMessage.SEVERITY_INFO);
						// limpiarBusqueda(null);
						setMensajeDNI(""); // mensaje de existencia de dni y/o
											// continuidad del registro del
											// usuario.
						setMensajeError(""); // mensaje de error de formato del
												// nro de dni.
						setValidaClick(false); // bandera q indica q se dio
												// click a la lupa
						setDeshabilitar(false); // deshabilita campos de texto
						paginaRetorno = FORMULARIO_USUARIO;
//                                                paginaRetorno = FORMULARIO_BUSCAR_USUARIO;

						if (this.flagExternal == null) {
							flagResgitroExitoso = false;
						}
					}
				} else {
					controlMessageUtil.imprimirMessageClave(
							ConstantesUtil.ERROR_REGISTRO_USUARIO + ": Verificar los roles asignados",
							FacesMessage.SEVERITY_ERROR);
					paginaRetorno = FORMULARIO_USUARIO;
				}
			}
//			else {
//				controlMessageUtil.imprimirMessage(msjeValidaFormato,
//						FacesMessage.SEVERITY_ERROR);
//				paginaRetorno = FORMULARIO_USUARIO;
//			}
		} catch (UsuarioEntidadExisteException ueee) {
			controlMessageUtil.imprimirMessageClave(
					ConstantesUtil.ERROR_REGISTRO_USER_DNI,
					FacesMessage.SEVERITY_ERROR);
		} catch (BusinessException be) {
			controlMessageUtil.imprimirMessage(be, FacesMessage.SEVERITY_ERROR);
		} catch (ApplicationException ae) {
			controlMessageUtil.imprimirMessageClave(
					ConstantesUtil.ERROR_REGISTRO_USUARIO,
					FacesMessage.SEVERITY_ERROR);
		} catch (Exception e) {
			controlMessageUtil.imprimirMessageClave(
					ConstantesUtil.ERROR_REGISTRO_USUARIO,
					FacesMessage.SEVERITY_ERROR);
			// Se elimina el usuario
			if (flagCreacionUsuario) {
				usuarioServiceLocal.deleteUserCascade(dtoReturn.getUsuario()
						.getId(), dtoReturn.getOrganismo().getId());
			}
		}
		return paginaRetorno;
	}


	/**
	 * Envio correo confirmacion.
	 *
	 * @param dto
	 *            el dto
	 * @throws Exception
	 *             the exception
	 */
	private void envioCorreoConfirmacion(UsuarioOrganismoDTO dto)
			throws Exception {
		try {
			List<String> destinatarios = new ArrayList<String>();
			destinatarios.add(dto.getUsuario().getEmail());
//			destinatarios.add(dto.getEmailInstitucional());
			Map<String, String> map = new HashMap<String, String>();
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_USUARIOACCESO, dto
					.getUsuario().getCodigoOID());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_FREGISTRO, FechaUtil
					.obtenerFechaFormatoSimple(dto.getUsuario()
							.getFechaCreacion()));
			map.put(NotificacionUtil.ELEMENTO_LABEL_USUARIO,
					dto.getIndicadorAdministradorEntidad().intValue() == IndicadorConstant.INDICADOR_ACTIVO ? NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO_ADM
							: NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO);
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_ENTIDAD, dto
					.getOrganismo().getNombreOrganismo().toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_DESTINATARIO, dto
					.getUsuario().getNombreCompleto()
					.toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_CLAVEACCESO, dto
					.getUsuario().getClave());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_PERFIL,
					this.nombrePerfil);
			map.put(NotificacionUtil.ELEMENTO_CADUCIDAD_CONTRASENHA,
					this.administracionServiceLocal
							.getParametro(ParametroType.NUMERO_DIAS_CAMBIO_CONTRASENHA
									.getValue()));
			notificacionServiceLocal.enviarCorreoConfirmacionUsuario(
					NotificacionUtil.ASUNTO_NOTIFICACION_EMAIL_CREACION_USUARIO
							.toString(), destinatarios, map,
					NotificacionUtil.RUTA_TEMPLATES_CREA_USUARIO.toString(),
					null); // usuario_clave.ftl*/
		} catch (Exception e) {
			controlMessageUtil.imprimirLog(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Envio correo modificacion.
	 *
	 * @param dto
	 *            el dto
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unused")
	private void envioCorreoModificacion(UsuarioOrganismoDTO dto)
			throws Exception {
		try {
			List<String> destinatarios = new ArrayList<String>();
			destinatarios.add(dto.getUsuario().getEmail());
//			destinatarios.add(dto.getEmailInstitucional());

			Map<String, String> map = new HashMap<String, String>();
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_FREGISTRO, FechaUtil
					.obtenerFechaFormatoSimple(dto.getUsuario()
							.getUltimaFechaModificacion()));
			map.put(NotificacionUtil.ELEMENTO_LABEL_USUARIO,
					dto.getIndicadorAdministradorEntidad().intValue() == IndicadorConstant.INDICADOR_ACTIVO ? NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO_ADM
							: NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO);
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_USUARIOACCESO, dto
					.getUsuario().getCodigoOID());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_ENTIDAD, dto
					.getOrganismo().getNombreOrganismo());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_DESTINATARIO, dto
					.getUsuario().getNombreCompleto()
					.toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_CADUCIDAD_CONTRASENHA,
					this.administracionServiceLocal
							.getParametro(ParametroType.NUMERO_DIAS_CAMBIO_CONTRASENHA
									.getValue()));
			notificacionServiceLocal
					.enviarCorreoConfirmacionUsuario(
							NotificacionUtil.ASUNTO_NOTIFICACION_EMAIL_MODIFICACION_USUARIO
									.toString(), destinatarios, map,
							NotificacionUtil.RUTA_TEMPLATES_MODIFICA_USUARIO
									.toString(), null);
		} catch (Exception e) {
			controlMessageUtil.imprimirLog(e, FacesMessage.SEVERITY_ERROR);
		}
	}


	/**
	 * Generar clave.
	 *
	 * @param event
	 *            el event
	 * @throws Exception
	 *             the exception
	 */
	public void generarClave(ActionEvent event) throws Exception {
		if (this.administracionServiceLocal.generarClave(usuarioDetalle)) {
			controlMessageUtil.imprimirMessageClave(
					ConstantesUtil.MENSAJE_EXITO_GENERAR_CLAVE,
					FacesMessage.SEVERITY_INFO);
		} else {
			controlMessageUtil.imprimirMessageClave(
					ConstantesUtil.ERROR_GENERAR_CLAVE,
					FacesMessage.SEVERITY_ERROR);
		}
	}
//
	/**
	 * Cancelar.
	 *
	 * @return the string
	 */
	public String cancelar() {
		limpiarBusqueda(null);
		return "regresar";
	}

	/**
	 * Reestablecer valores.
	 *
	 * @throws Exception
	 *             the exception
	 */
	private void reestablecerValores() throws Exception {
		try {
			flag = "";
			nombrePerfil = StringUtils.EMPTY;
			idPerfil = null;
			idPersona = null;
			validarEmail = false;
			indicadorVEDU = false;
			flagResgitroExitoso = true;
			usuarioDetalle = FactoryBean.getBean(UsuarioOrganismoDTO.class);
			criteria = FactoryBean.getBean(UsuarioCriteriaVO.class);
			if (indicadorAmbito) {
				nombreOrganismo = StringUtils.EMPTY;
				idOrganismo = null;
			}
			if (listaUsuario != null) {
				listaUsuario.clear();
			}
			if (listaRolDTO != null) {
				listaRolDTO.clear();
			}
			if (listaPrivilegioDTO != null) {
				listaPrivilegioDTO.clear();
			}
			mensajeDNI = StringUtils.EMPTY;
			mensajeError = StringUtils.EMPTY;
			if (this.flagExternal == null) {
				flagResgitroExitoso = true;
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}


	/**
	 * Establecer valores nuevo.
	 */
	private void establecerValoresNuevo() {
		try {
			if (usuarioDetalle.getUsuario() == null) {
				usuarioDetalle.setUsuario(new UsuarioDTO());
			}
			if (usuarioDetalle.getUsuario()== null) {
				usuarioDetalle.setUsuario(new UsuarioDTO());
			}
//			usuarioDetalle.getUsuario().setTipoDocumento(TipoDocumentoType.DNI.getKey());
			usuarioDetalle.setEstado(UsuarioState.ACTIVO.getKey());
			idOrganismo = context.getUsuarioSessionVO().getIdEntidad();
			setNombreOrganismo(context.getUsuarioSessionVO().getNombreEntidad());
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Volver perfil.
	 *
	 * @param event
	 *            el event
	 * @throws Exception
	 *             the exception
	 */
	public void volverPerfil(ActionEvent event) throws Exception {
		context.setLocale(FacesUtil.getLocale());
		try {
			listaPrivilegioDTOMemory = new ArrayList<PrivilegioDTO>(); // mgj
			sizePrivilegios = 0;
			sizeRoles = 0;
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("editId");
			Long idPerfil = Long.valueOf(component.getValue().toString());
			PerfilDTO perfilDTO = administracionServiceLocal.obtenerPerfil(
					context, idPerfil);
			usuarioDetalle.getOrganismoPerfil().getPerfil()
					.setId(perfilDTO.getId());
			nombrePerfil = perfilDTO.getNombre();
			// Se limpian las listas de roles y privilegios
			listaRolDTO = new ArrayList<UsuarioRolDTO>();
			listaPrivilegioDTO = new ArrayList<PrivilegioDTO>();

			// Cargamos los roles asociados
			List<RolDTO> listaRolTempDTO = administracionServiceLocal
					.listaRolesxPerfil(idPerfil);
			if (listaRolTempDTO != null) {
				sizeRoles = listaRolTempDTO.size();
				// Cargamos los privilegios
				for (RolDTO dto : listaRolTempDTO) {
					UsuarioRolDTO usuarioRolDTO = new UsuarioRolDTO();
					usuarioRolDTO.setPerfilRol(new PerfilRolDTO());
					usuarioRolDTO.getPerfilRol().setRol(dto);
					usuarioRolDTO.setHidden(true);
					listaRolDTO.add(usuarioRolDTO);

				}
			} else {
				sizeRoles = 0;
			}
			this.indicadorSeleccionarTodoRol = sizeRoles > 0;
			if (listaPrivilegioDTO != null) {
				sizePrivilegios = listaPrivilegioDTO.size();
				this.listaPrivilegioDTOMemory.addAll(listaPrivilegioDTO); // mgj
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Volver entidad.
	 *
	 * @param event
	 *            el event
	 * @throws Exception
	 *             the exception
	 */
	public void volverEntidad(ActionEvent event) throws Exception {
		try {
			UIParameter component = (UIParameter) event.getComponent().findComponent("entidadId");
			if (component.getValue() != null) {
				idOrganismo = Long.valueOf(component.getValue().toString());
			}
			UIParameter component2 = (UIParameter) event.getComponent().findComponent("entidadNombre");
			if (component2.getValue() != null) {
				nombreOrganismo = (component2.getValue().toString());
			}
                        this.indicadorRequerido=true;
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Validate buscar.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateBuscar() {
		boolean rs = false;
		mensajeFecha = "";
		if (!StringUtil.isNotNullOrBlank(criteria.getEntidad())
				&& !StringUtil.isNotNullOrBlank(criteria.getNombreUsuario())
				&& !StringUtil.isNotNullOrBlank(criteria.getPerfil())
				&& !StringUtil.isNotNullOrBlank(criteria.getNumeroDocumentoIdentidad())
				&& !StringUtil.isNotNullOrBlank(criteria.getRol())
				&& !StringUtil.isNotNullOrBlank(criteria.getEstadoUsuario())
				&& !StringUtil.isNotNullOrBlank(criteria.getEstadoUsuarioOrganismo())
				&& !StringUtil.isNotNullOrBlank(criteria.getAccion())
				) {
			rs = true;
		}


		return rs;
	}

	/**
	 * Limpiar fecha inicio accion.
	 *
	 * @param ev
	 *            el ev
	 */
	public void limpiarFechaInicioAccion(ActionEvent ev) {
		this.setFechaAccionDesde(null);
	}

	/**
	 * Limpiar fecha fin accion.
	 *
	 * @param ev
	 *            el ev
	 */
	public void limpiarFechaFinAccion(ActionEvent ev) {
		this.setFechaAccionHasta(null);
	}

	/**
	 * Seleccionar todo.
	 *
	 * @param event
	 *            el event
	 */
	public void seleccionarTodo(ValueChangeEvent event) {
		if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
			event.setPhaseId(PhaseId.INVOKE_APPLICATION);
			event.queue();
			return;
		}
		try {
			if (event != null) {
				this.pageActual = FacesContext.getCurrentInstance()
						.getExternalContext().getRequestParameterMap()
						.get("frmUsuario:valorPageCount");
				int pagina = 0;
				if (this.pageActual == null) {
					pagina = 1;
				} else {
					pagina = Integer.valueOf(this.pageActual);
				}
				indicadorSeleccionarTodo = (Boolean) event.getNewValue();
				for (int i = 0; i < 10; i++) {
					if (((pagina - 1) * 10 + i) < listaUsuario.size()) {
						BusquedaUsuarioVO usuarioVO = listaUsuario
								.get(((pagina - 1) * 10) + i);
						usuarioVO.setHidden((Boolean) event.getNewValue());
						listaUsuario.set(((pagina - 1) * 10) + i, usuarioVO);
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Seleccionar todo rol.
	 *
	 * @param event
	 *            el event
	 */
	public void seleccionarTodoRol(ValueChangeEvent event) {
		if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
			event.setPhaseId(PhaseId.INVOKE_APPLICATION);
			event.queue();
			return;
		}
		try {
			if (event != null) {
				for (UsuarioRolDTO roldto : listaRolDTO) {
					roldto.setHidden((Boolean) event.getNewValue());
				}
				listaPrivilegioDTO.clear();
				indicadorSeleccionarTodoRol = (Boolean) event.getNewValue();
				setIndicadorSeleccionarTodo((Boolean) event.getNewValue());
				if ((Boolean) event.getNewValue()) {
					for (PrivilegioDTO pdto : listaPrivilegioDTOMemory) {
						listaPrivilegioDTO.add(pdto);
					}
				}
				sizePrivilegios = listaPrivilegioDTO.size();
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}


	/**
	 * Cargar usuario organismo dto.
	 *
	 * @return the usuario organismo dto
	 */
	private UsuarioOrganismoDTO cargarUsuarioOrganismoDTO() {
		UsuarioOrganismoDTO usuarioOrganismo = new UsuarioOrganismoDTO();
		usuarioOrganismo.setOrganismo(new OrganismoDTO());
		usuarioOrganismo.setOrganismoPerfil(new OrganismoPerfilDTO());
		usuarioOrganismo.getOrganismoPerfil().setPerfil(new PerfilDTO());
		return usuarioOrganismo;
	}

	/**
	 * Establece el fecha accion desde.
	 *
	 * @param fechaAccionDesde
	 *            el new fecha accion desde
	 */
	public void setFechaAccionDesde(Date fechaAccionDesde) {
		this.fechaAccionDesde = fechaAccionDesde;
	}

	/**
	 * Obtiene fecha accion hasta.
	 *
	 * @return fecha accion hasta
	 */
	public Date getFechaAccionHasta() {
		return fechaAccionHasta;
	}

	/**
	 * Establece el fecha accion hasta.
	 *
	 * @param fechaAccionHasta
	 *            el new fecha accion hasta
	 */
	public void setFechaAccionHasta(Date fechaAccionHasta) {
		this.fechaAccionHasta = fechaAccionHasta;
	}

	/**
	 * Obtiene mensaje error.
	 *
	 * @return mensaje error
	 */
	public String getMensajeError() {
		return mensajeError;
	}

	/**
	 * Establece el mensaje error.
	 *
	 * @param mensajeError
	 *            el new mensaje error
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	/** La languaje tipo documento. */
	private String languajeTipoDocumento = "";

	/**
	 * Obtiene lista tipo documento.
	 *
	 * @return lista tipo documento
	 */
	public List<SelectItem> getListaTipoDocumento() {
		try {
			if (!languajeTipoDocumento.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) {
				languajeTipoDocumento = FacesUtil.getLocale().getLanguage();
				listaTipoDocumento = new ArrayList<SelectItem>();
				List<SelectVO> tiposDocumento = TipoDocumentoType.getList(FacesUtil.getLocale());

				if (tiposDocumento != null) {
					for (SelectVO tipoDocumento : tiposDocumento) {
                                            listaTipoDocumento.add(new SelectItem(tipoDocumento.getId(), tipoDocumento.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return listaTipoDocumento;
	}

	/**
	 * Obtiene lista perfil.
	 *
	 * @return lista perfil
	 */
	public List<SelectItem> getListaPerfil() {
		listaPerfil = new ArrayList<SelectItem>();
		try {
			List<Object[]> listaPerfiles = administracionServiceLocal.listarPerfiles();
			if (listaPerfiles != null) {
				for (Object[] obj : listaPerfiles) {
					listaPerfil.add(new SelectItem(String.valueOf(obj[0]),
							String.valueOf(obj[1])));
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return listaPerfil;
	}


	/**
	 * Obtiene lista rol.
	 *
	 * @return lista rol
	 */
	public List<SelectItem> getListaRol() {
		return listaRol;
	}

	/**
	 * Establece el lista rol.
	 *
	 * @param listaRol
	 *            el new lista rol
	 */
	public void setListaRol(List<SelectItem> listaRol) {
		this.listaRol = listaRol;
	}

        public String getFlagTipoDocumento() {
            return flagTipoDocumento;
        }

        public void setFlagTipoDocumento(String flagTipoDocumento) {
            this.flagTipoDocumento = flagTipoDocumento;
        }

        
        

	/**
	 * Obtiene time zone.
	 *
	 * @return time zone
	 */
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}


	/**
	 * Obtiene usuario detalle.
	 *
	 * @return usuario detalle
	 */
	public UsuarioOrganismoDTO getUsuarioDetalle() {
		return usuarioDetalle;
	}

	/**
	 * Establece el usuario detalle.
	 *
	 * @param usuarioDetalle
	 *            el new usuario detalle
	 */
	public void setUsuarioDetalle(UsuarioOrganismoDTO usuarioDetalle) {
		this.usuarioDetalle = usuarioDetalle;
	}

	/**
	 * Obtiene lista usuario.
	 *
	 * @return lista usuario
	 */
	public List<BusquedaUsuarioVO> getListaUsuario() {
		return listaUsuario;
	}

	/**
	 * Obtiene lista rol dto.
	 *
	 * @return lista rol dto
	 */
	public List<UsuarioRolDTO> getListaRolDTO() {
		return listaRolDTO;
	}

	/**
	 * Establece el lista rol dto.
	 *
	 * @param listaRolDTO
	 *            el new lista rol dto
	 */
	public void setListaRolDTO(List<UsuarioRolDTO> listaRolDTO) {
		this.listaRolDTO = listaRolDTO;
	}

        public boolean isIndicadorRequerido() {
            return indicadorRequerido;
        }

        public void setIndicadorRequerido(boolean indicadorRequerido) {
            this.indicadorRequerido = indicadorRequerido;
        }

        
	/**
	 * Obtiene lista estado.
	 *
	 * @return lista estado
	 */
	public List<SelectItem> getListaEstado() {
		try {
			if (!languajeEstado.equalsIgnoreCase(FacesUtil.getLocale()
					.getLanguage())) {
				languajeEstado = FacesUtil.getLocale().getLanguage();
				listaEstado = new ArrayList<SelectItem>();
				List<SelectVO> estadosUsuario = UsuarioState.getList(FacesUtil
						.getLocale());
				if (estadosUsuario != null) {
					for (SelectVO estadoUsuario : estadosUsuario) {
						listaEstado.add(new SelectItem(estadoUsuario.getId(),
								estadoUsuario.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return listaEstado;
	}

        /**
	 * Obtiene indicadores adm.
	 *
	 * @return indicadores adm
	 */
	public List<SelectItem> getIndicadoresAdm() {
		try {
			if (!languajeIndicadoresAdm.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) 
			{
				languajeIndicadoresAdm = FacesUtil.getLocale().getLanguage();
				indicadoresAdm = new ArrayList<SelectItem>();
				List<SelectVO> lista = CondicionState.getList(FacesUtil.getLocale());
				if (lista == null) {
					lista = CondicionState.getList(null);
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						indicadoresAdm.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);	
		}
		return indicadoresAdm;
	}
	/**
	 * Obtiene lista estado usuario organismo.
	 *
	 * @return lista estado usuario organismo
	 */
	public List<SelectItem> getListaEstadoUsuarioOrganismo() {
		try {
			if (!languajeEstadoUsuario.equalsIgnoreCase(FacesUtil.getLocale()
					.getLanguage())) {
				languajeEstadoUsuario = FacesUtil.getLocale().getLanguage();
				listaEstadoUsuarioOrganismo = new ArrayList<SelectItem>();
				List<SelectVO> estadosEntidad = EstadoState.getList(FacesUtil
						.getLocale());
				if (estadosEntidad != null) {
					for (SelectVO estadoEntidad : estadosEntidad) {
						listaEstadoUsuarioOrganismo
								.add(new SelectItem(estadoEntidad.getId(),
										estadoEntidad.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return listaEstadoUsuarioOrganismo;
	}


	/**
	 * Comprueba si es valida click.
	 *
	 * @return true, si es valida click
	 */
	public boolean isValidaClick() {
		return validaClick;
	}

	/**
	 * Establece el valida click.
	 *
	 * @param validaClick
	 *            el new valida click
	 */
	public void setValidaClick(boolean validaClick) {
		this.validaClick = validaClick;
	}

	/**
	 * Obtiene mensajelistavacia.
	 *
	 * @return mensajelistavacia
	 */
	public String getMensajelistavacia() {
		return mensajelistavacia;
	}

	/**
	 * Establece el mensajelistavacia.
	 *
	 * @param mensajelistavacia
	 *            el new mensajelistavacia
	 */
	public void setMensajelistavacia(String mensajelistavacia) {
		this.mensajelistavacia = mensajelistavacia;
	}

	/**
	 * Obtiene nombre perfil.
	 *
	 * @return nombre perfil
	 */
	public String getNombrePerfil() {
		return nombrePerfil;
	}

	/**
	 * Establece el nombre perfil.
	 *
	 * @param nombrePerfil
	 *            el new nombre perfil
	 */
	public void setNombrePerfil(String nombrePerfil) {
		this.nombrePerfil = nombrePerfil;
	}

	/**
	 * Obtiene nombre organismo.
	 *
	 * @return nombre organismo
	 */
	public String getNombreOrganismo() {
		return nombreOrganismo;
	}

	/**
	 * Establece el nombre organismo.
	 *
	 * @param nombreOrganismo
	 *            el new nombre organismo
	 */
	public void setNombreOrganismo(String nombreOrganismo) {
		this.nombreOrganismo = nombreOrganismo;
	}

	/**
	 * Obtiene id organismo.
	 *
	 * @return id organismo
	 */
	public Long getIdOrganismo() {
		return idOrganismo;
	}

	/**
	 * Establece el id organismo.
	 *
	 * @param idOrganismo
	 *            el new id organismo
	 */
	public void setIdOrganismo(Long idOrganismo) {
		this.idOrganismo = idOrganismo;
	}

	/**
	 * Comprueba si es deshabilitar.
	 *
	 * @return true, si es deshabilitar
	 */
	public boolean isDeshabilitar() {
		return deshabilitar;
	}

	/**
	 * Establece el deshabilitar.
	 *
	 * @param deshabilitar
	 *            el new deshabilitar
	 */
	public void setDeshabilitar(boolean deshabilitar) {
		this.deshabilitar = deshabilitar;
	}

	/**
	 * Obtiene flag creacion.
	 *
	 * @return flag creacion
	 */
	public String getFlagCreacion() {
		return flagCreacion;
	}

	/**
	 * Establece el flag creacion.
	 *
	 * @param flagCreacion
	 *            el new flag creacion
	 */
	public void setFlagCreacion(String flagCreacion) {
		this.flagCreacion = flagCreacion;
	}

	/**
	 * Obtiene flag external.
	 *
	 * @return flag external
	 */
	public String getFlagExternal() {
		return flagExternal;
	}

	/**
	 * Establece el flag external.
	 *
	 * @param flagExternal
	 *            el new flag external
	 */
	public void setFlagExternal(String flagExternal) {
		this.flagExternal = flagExternal;
	}


	/**
	 * Comprueba si es flag session.
	 *
	 * @return true, si es flag session
	 */
	public boolean isFlagSession() {
		return flagSession;
	}

	/**
	 * Establece el flag session.
	 *
	 * @param flagSession
	 *            el new flag session
	 */
	public void setFlagSession(boolean flagSession) {
		this.flagSession = flagSession;
	}

	/**
	 * Comprueba si es ver check.
	 *
	 * @return true, si es ver check
	 */
	public boolean isVerCheck() {
		return verCheck;
	}


	/**
	 * Establece el ver check.
	 *
	 * @param verCheck
	 *            el new ver check
	 */
	public void setVerCheck(boolean verCheck) {
		this.verCheck = verCheck;
	}

	/**
	 * Obtiene mensaje dni.
	 *
	 * @return mensaje dni
	 */
	public String getMensajeDNI() {
		return mensajeDNI;
	}

	/**
	 * Establece el mensaje dni.
	 *
	 * @param mensajeDNI
	 *            el new mensaje dni
	 */
	public void setMensajeDNI(String mensajeDNI) {
		this.mensajeDNI = mensajeDNI;
	}

	/**
	 * Obtiene mensaje fecha.
	 *
	 * @return mensaje fecha
	 */
	public String getMensajeFecha() {
		return mensajeFecha;
	}

	/**
	 * Establece el mensaje fecha.
	 *
	 * @param mensajeFecha
	 *            el new mensaje fecha
	 */
	public void setMensajeFecha(String mensajeFecha) {
		this.mensajeFecha = mensajeFecha;
	}


	/**
	 * Comprueba si es flag resgitro exitoso.
	 *
	 * @return true, si es flag resgitro exitoso
	 */
	public boolean isFlagResgitroExitoso() {
		return flagResgitroExitoso;
	}

	/**
	 * Establece el flag resgitro exitoso.
	 *
	 * @param flagResgitroExitoso
	 *            el new flag resgitro exitoso
	 */
	public void setFlagResgitroExitoso(boolean flagResgitroExitoso) {
		this.flagResgitroExitoso = flagResgitroExitoso;
	}

	/**
	 * Obtiene estado desccripcion.
	 *
	 * @return estado desccripcion
	 */
	public String getEstadoDesccripcion() {
		return estadoDesccripcion;
	}

	/**
	 * Establece el estado desccripcion.
	 *
	 * @param estadoDesccripcion
	 *            el new estado desccripcion
	 */
	public void setEstadoDesccripcion(String estadoDesccripcion) {
		this.estadoDesccripcion = estadoDesccripcion;
	}

	/**
	 * Obtiene lista acciones.
	 *
	 * @return lista acciones
	 */
	public List<SelectItem> getListaAcciones() {
		try {
			if (!languajeAcciones.equalsIgnoreCase(FacesUtil.getLocale()
					.getLanguage())) {
				languajeAcciones = FacesUtil.getLocale().getLanguage();
				listaAcciones = new ArrayList<SelectItem>();
				List<SelectVO> listaAccionesVO = FechaAccionType
						.getList(FacesUtil.getLocale());
				for (SelectVO vo : listaAccionesVO) {
					SelectItem item = new SelectItem();
					item.setValue(vo.getId());
					item.setLabel(vo.getValue());
					this.listaAcciones.add(item);
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return listaAcciones;
	}

	/**
	 * Establece el editar usuario.
	 *
	 * @param editarUsuario
	 *            el new editar usuario
	 */
	public void setEditarUsuario(boolean editarUsuario) {
		this.editarUsuario = editarUsuario;
	}

	/**
	 * Comprueba si es editar usuario.
	 *
	 * @return true, si es editar usuario
	 */
	public boolean isEditarUsuario() {
		return editarUsuario;
	}

	/**
	 * Obtiene estado filtrado.
	 *
	 * @return estado filtrado
	 */
	public List<SelectItem> getEstadoFiltrado() {
		try {
			if (!languajeEstadoFiltrado.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) {
				languajeEstadoFiltrado = FacesUtil.getLocale().getLanguage();
				estadoFiltrado = new ArrayList<SelectItem>();
				List<SelectVO> lista = EstadoState.getList(FacesUtil.getLocale());
                                if (lista == null) {
					lista = EstadoState.getList(null);
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						estadoFiltrado.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
//				if (lista.size() != 0) {					
//					for(SelectVO sVO:lista){
//						estadoFiltrado.add(new SelectItem(sVO.getId(),sVO.getValue()));
//					}
//
//					estadoFiltrado.add(new SelectItem(lista.get(0).getId(),lista.get(0).getValue()));
//				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return estadoFiltrado;
	}

	/**
	 * Establece el estado filtrado.
	 *
	 * @param estadoFiltrado
	 *            el new estado filtrado
	 */
	public void setEstadoFiltrado(List<SelectItem> estadoFiltrado) {
		this.estadoFiltrado = estadoFiltrado;
	}

	/**
	 * Obtiene indicador estado usuario.
	 *
	 * @return indicador estado usuario
	 */
	public String getIndicadorEstadoUsuario() {
		return indicadorEstadoUsuario;
	}

	/**
	 * Establece el indicador estado usuario.
	 *
	 * @param indicadorEstadoUsuario
	 *            el new indicador estado usuario
	 */
	public void setIndicadorEstadoUsuario(String indicadorEstadoUsuario) {
		this.indicadorEstadoUsuario = indicadorEstadoUsuario;
	}

	/**
	 * Comprueba si es indicador seleccionar todo.
	 *
	 * @return true, si es indicador seleccionar todo
	 */
	public boolean isIndicadorSeleccionarTodo() {
		return indicadorSeleccionarTodo;
	}

	/**
	 * Establece el indicador seleccionar todo.
	 *
	 * @param indicadorSeleccionarTodo
	 *            el new indicador seleccionar todo
	 */
	public void setIndicadorSeleccionarTodo(boolean indicadorSeleccionarTodo) {
		this.indicadorSeleccionarTodo = indicadorSeleccionarTodo;
	}

	/**
	 * Comprueba si es indicador seleccionar todo rol.
	 *
	 * @return true, si es indicador seleccionar todo rol
	 */
	public boolean isIndicadorSeleccionarTodoRol() {
		return indicadorSeleccionarTodoRol;
	}

	/**
	 * Establece el indicador seleccionar todo rol.
	 *
	 * @param indicadorSeleccionarTodoRol
	 *            el new indicador seleccionar todo rol
	 */
	public void setIndicadorSeleccionarTodoRol(
			boolean indicadorSeleccionarTodoRol) {
		this.indicadorSeleccionarTodoRol = indicadorSeleccionarTodoRol;
	}

	/**
	 * Obtiene cant filas.
	 *
	 * @return cant filas
	 */
	public int getCantFilas() {
		return cantFilas;
	}

	/**
	 * Establece el cant filas.
	 *
	 * @param cantFilas
	 *            el new cant filas
	 */
	public void setCantFilas(int cantFilas) {
		this.cantFilas = cantFilas;
	}


	/**
	 * Establece el deshabilitar borr.
	 *
	 * @param deshabilitarBorr
	 *            el new deshabilitar borr
	 */
	public void setDeshabilitarBorr(boolean deshabilitarBorr) {
		this.deshabilitarBorr = deshabilitarBorr;
	}

	/**
	 * Comprueba si es deshabilitar borr.
	 *
	 * @return true, si es deshabilitar borr
	 */
	public boolean isDeshabilitarBorr() {
		return deshabilitarBorr;
	}

	/**
	 * Obtiene creacion usuario.
	 *
	 * @return creacion usuario
	 */
	public String getCreacionUsuario() {
		return pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_CREAR_USUARIO;
	}

	/**
	 * Obtiene modificacion usuario.
	 *
	 * @return modificacion usuario
	 */
	public String getModificacionUsuario() {
		return pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_MODIFICAR_USUARIO;
	}

       /**
	 * Obtiene criteria.
	 *
	 * @return criteria
	 */
	public UsuarioCriteriaVO getCriteria() {
		return criteria;
	}

	/**
	 * Establece el criteria.
	 *
	 * @param criteria
	 *            el new criteria
	 */
	public void setCriteria(UsuarioCriteriaVO criteria) {
		this.criteria = criteria;
	}

//        public void mostrarPerfil() {
//		          System.out.println("Boton mostrarPerfil()");
//	}

        public void actualizarUsuariosClaveByMigracion (ActionEvent event) {
            boolean exito;
            try {
                   exito = administracionServiceLocal.encriptarClavesByMigracion();
			if (exito) {
                            controlMessageUtil
						.imprimirMessageClave(
								"EXITO: Se realizo la encriptacion de las claves de los usuarios por Migracion. ",
								FacesMessage.SEVERITY_ERROR);
			} else {
				controlMessageUtil
						.imprimirMessageClave(
								"ERROR: No se realizo la encriptacion de las claves de los usuarios por Migracion. ",
								FacesMessage.SEVERITY_ERROR);
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
        
        public void inactivarRequeridos(){
            this.indicadorRequerido = false;
        }
        
        public void seleccionarTipoDocummento(ValueChangeEvent event) {	
//            System.out.println(event.getNewValue().toString());
            UIOutput component = (UIOutput) event.getComponent().findComponent("tipoDocumento");
            String id = component.getValue().toString();
//            System.out.println(id);
            if (id.equals(TipoDocumentoType.DNI.getKey())) {
                flagTipoDocumento = TipoDocumentoType.DNI.getKey();
            }
            if (id.equals(TipoDocumentoType.CARNE_EXTRANJERIA.getKey())) {
                flagTipoDocumento = TipoDocumentoType.CARNE_EXTRANJERIA.getKey();
            }
            if (id.equals(TipoDocumentoType.PASAPORTE.getKey())) {
                flagTipoDocumento = TipoDocumentoType.PASAPORTE.getKey();
            }
            if (id.equals(TipoDocumentoType.RUC.getKey())) {
                flagTipoDocumento = TipoDocumentoType.RUC.getKey();
            }
//            if (usuarioDetalle.getUsuario().getTipoDocumento().equals(TipoDocumentoType.OTRO.getKey())) {
//                flagTipoDocumento = TipoDocumentoType.OTRO.getKey();
//            }
            
        }
}