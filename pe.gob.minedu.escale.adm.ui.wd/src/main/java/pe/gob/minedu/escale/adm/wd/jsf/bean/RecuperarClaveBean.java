package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import pe.gob.minedu.escale.adm.business.exception.UsuarioEstaDesactivadoException;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.SeguridadServiceRemote;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.vo.RecuperarClaveVO;
import pe.gob.minedu.escale.adm.web.util.ADMConstantes;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;

@ManagedBean(name="recuperarClaveBean")
@SessionScoped
public class RecuperarClaveBean extends BaseBean implements Serializable {

    /** La Constante serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** El servicio administracion service local. */
    @EJB
    private transient AdministracionServiceLocal administracionServiceLocal;

    /** El servicio usuario service local. */
    @EJB
    private transient UsuarioServiceLocal usuarioServiceLocal;

    /** La Constante INPUT_ERROR_NO_UPPER. */
    private static final String INPUT_ERROR_NO_UPPER = "inputErrorNoUpper";

    /** La variable answer. */
    private String answer;
    
    /** La contrase�a antigua. */
    private String contrasenaAntigua;
    
    /** La contrase�a nueva. */
    private String contrasenaNueva;
    
    /** El correo electronico persona. */
    private String correoElectronicoPer;
    
    /** El style usuario. */
    private String styleUsuario;
    
    /** El style correo. */
    private String styleCorreo;
    
    /** El style codigo. */
    private String styleCodigo;
    
    /** El countador. */
    private int count = 0;
    
    /** El key. */
    private String key;
    
    /** El login. */
    private String login;
    
    /** El mensaje. */
    private String mensaje;
    
    /** El mensaje error. */
    private String mensajeError = null;
    
    /** El flag mostrar captcha. */
    private boolean mostrarCaptcha = false;
    
    /** El password. */
    private String password;
    
    /** La prueba captcha. */
    private String pruebaCaptcha;
    
    /** El flag remember me. */
    private boolean rememberMe;
    
    /** La variable repetir nueva contrase�a. */
    private String repetirNuevaContrasena;
    
    /** El hash map user temp. */
    private HashMap userTemp = new HashMap();
    
    /** El usuario asignado. */
    private String usuarioAsignado;
    
    /** El flag usuario bloqueado. */
    private boolean usuarioBloqueado;
    
    /** El arreglo de byte name image. */
    private byte[] nameImageByte = null; // Se garantiza que su inicio sea en blanco

    /** El control message util. */
    private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), RecuperarClaveBean.class);   

    /**
     * Instancia un nuevo recuperar clave bean.
     */
    public RecuperarClaveBean() {
    }
    
    /**
     * Post construct.
     */
    @PostConstruct
    public void postConstruct() {
            try {
            init();
            this.mensajeError = null;
            this.setMostrarCaptcha(false);            
            HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            Object flagBlocked = session.getAttribute(SeguridadServiceRemote.USER_BLOCKED);
            if (flagBlocked != null) {
                    usuarioBloqueado = (Boolean)flagBlocked;
            }
            } catch (Exception e) {
                    controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
            }
    }

    /**
     * Mensaje exito recuperar clave.
     *
     */
    
    public  void mensajeExitoRecuperarClave() {
    	controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_EXITO_RECUPERAR_CLAVE, FacesMessage.SEVERITY_INFO);
    }
    
    /**
     * Mensaje error campo obligatorio.
     *
     */
    public  void mensajeErrorCampoObligatorio() {
    	controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_CAMPO_OBLIGATORIO, FacesMessage.SEVERITY_ERROR);
    }
    
    /**
     * Mensaje usuario no existe.
     *
     */
    public void mensajeUsuarioNoExiste() {
    	controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_USER_NO_EXISTE_EN_SISTEMA, FacesMessage.SEVERITY_ERROR);
    }
    
    /**
     * Mensaje recuperar clave bloqueado.
     *
     */
    public  void mensajeRecuperarClaveBloqueado() {
    	controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_EXITO_RECUPERAR_CLAVE, FacesMessage.SEVERITY_INFO);	
    }
    
    /**
     * Cancelar cambio clave.
     *
     * @return the string
     */
    public String cancelarCambioClave() {
        // validamos la nueva contrase�a
        return ADMConstantes.MODIFICAR_CLAVE_USUARIO;
    }

    /**
     * Confirmar cambio clave.
     *
     * @return the string
     */
    public String confirmarCambioClave() {
        // validamos la nueva contrase�a
        mensajeError = null;
        return ADMConstantes.MODIFICAR_CLAVE_TEMPORAL;
    }
    /**
	 * Limpiar campos.
	 *
	 * @return the string
	 */
    public String limpiarCampos() {        
        this.usuarioAsignado = "";
        this.correoElectronicoPer = "";
//        this.pruebaCaptcha = "";
        return null;
        
    }

    /**
     * Modificar contrasena.
     *
     * @return the string
     */
    public String modificarContrasena() {
        // validamos la nueva contrase�a
        mensajeError = null;
        return ADMConstantes.MODIFICAR_CLAVE_TEMPORAL;
    }

    /**
     * Recuperar clave usuario.
     *
     * @return the string
     */
    public String recuperarClaveUsuario() {
        mensajeError = "";
        return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
    }
    /**
     * Validar campos.
     *
     * @return true, en caso de exito
     */
    public boolean validarCampos() {
    	boolean var = false;
    	try {
	    	if (StringUtils.isBlank(usuarioAsignado)) {
				//sbObligatorios.append("Nro.Doc, ");
				setStyleUsuario(INPUT_ERROR_NO_UPPER);
				var = true;
			} else {
				setStyleUsuario("");
			}
	    	if (StringUtils.isBlank(correoElectronicoPer)) {
				//sbObligatorios.append("Nro.Doc, ");
				setStyleCorreo(INPUT_ERROR_NO_UPPER);
				var = true;
			} else {
				setStyleCorreo("");
			}

    	} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
    	
    	return var;
    }

    /**
     * Validar recuperar clave.
     *
     * @return the string
     */
    public String validarRecuperarClave() {
    	try {
	        boolean flagCaptcha = false;
	        if (validarCampos()) {
	        	mensajeErrorCampoObligatorio();	            
	            return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
	        }
	        
	        UsuarioDTO userDTO = usuarioServiceLocal.buscarUsuarioxOID(usuarioAsignado);
	        if (userDTO == null) {
	        	mensajeUsuarioNoExiste();
	        	flagCaptcha = true;		        
	        	return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
	        }

	        //Validar al UsuarioOrganismo si es que esta activo.
	        List<Object[]> data = usuarioServiceLocal.buscarUsuarioRecuperarClave(getUsuarioAsignado());
	        if (String.valueOf(data).equalsIgnoreCase("[]")) {
	        	throw new UsuarioEstaDesactivadoException();
	        }

	        RecuperarClaveVO recuperarClaveVO = new RecuperarClaveVO();
	        recuperarClaveVO.setCorreoElectronico(getCorreoElectronicoPer());
	        recuperarClaveVO.setFlagCapcha(flagCaptcha);
	        recuperarClaveVO.setUsuario(getUsuarioAsignado());
	        
            administracionServiceLocal.recuperarClave(recuperarClaveVO);
            mensajeExitoRecuperarClave();
            limpiarCampos();
            HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            session.setAttribute(SeguridadServiceRemote.USER_BLOCKED, false);
            session.setAttribute(SeguridadServiceRemote.CAPTCHA_HABILITADO, Boolean.FALSE);//desabilitar el capcha si es necesario
            usuarioBloqueado = false;
            //no mostrar dos veces el mensaje de exito el el gobal messaje
            
        } catch (UsuarioEstaDesactivadoException e) {        	
                controlMessageUtil.imprimirMessageClave(ConstantesUtil.MESSAGE_ERROR_USER_INACTIVO, FacesMessage.SEVERITY_ERROR);
        }catch (Exception e) {
        	controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
                
        }
        return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
    }

    /**
     * Inits the.
     */
    private void init() {
        login = "";
        password = "";
        mensajeError = "";
        contrasenaAntigua = "";
        contrasenaNueva = "";
        repetirNuevaContrasena = "";
        usuarioAsignado = "";
        correoElectronicoPer = "";
    }
   
    
    //get y set
    /**
     * Obtiene answer.
     *
     * @return answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Obtiene contrasena antigua.
     *
     * @return contrasena antigua
     */
    public String getContrasenaAntigua() {
        return contrasenaAntigua;
    }

    /**
     * Obtiene contrasena nueva.
     *
     * @return contrasena nueva
     */
    public String getContrasenaNueva() {
        return contrasenaNueva;
    }

    /**
     * Obtiene correo electronico per.
     *
     * @return correo electronico per
     */
    public String getCorreoElectronicoPer() {
        return correoElectronicoPer;
    }

    /**
     * Obtiene count.
     *
     * @return count
     */
    public int getCount() {
        return count;
    }

    /**
     * Obtiene key.
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Obtiene login.
     *
     * @return login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Obtiene mensaje.
     *
     * @return mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Obtiene mensaje error.
     *
     * @return mensaje error
     */
    public String getMensajeError() {
        return mensajeError;
    }

    /**
     * Obtiene password.
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Obtiene prueba captcha.
     *
     * @return prueba captcha
     */
    public String getPruebaCaptcha() {
        return pruebaCaptcha;
    }

    /**
     * Obtiene repetir nueva contrasena.
     *
     * @return repetir nueva contrasena
     */
    public String getRepetirNuevaContrasena() {
        return repetirNuevaContrasena;
    }

    /**
     * Obtiene user temp.
     *
     * @return user temp
     */
    public HashMap getUserTemp() {
        return userTemp;
    }

    /**
     * Obtiene usuario asignado.
     *
     * @return usuario asignado
     */
    public String getUsuarioAsignado() {
        return usuarioAsignado;
    }

    /**
     * Comprueba si es mostrar captcha.
     *
     * @return true, si es mostrar captcha
     */
    public boolean isMostrarCaptcha() {
        return mostrarCaptcha;
    }

    /**
     * Comprueba si es remember me.
     *
     * @return true, si es remember me
     */
    public boolean isRememberMe() {
        return rememberMe;
    }

    /**
     * Obtiene style usuario.
     *
     * @return style usuario
     */
    public String getStyleUsuario() {
		return styleUsuario;
	}

	/**
	 * Establece el style usuario.
	 *
	 * @param styleUsuario el new style usuario
	 */
	public void setStyleUsuario(String styleUsuario) {
		this.styleUsuario = styleUsuario;
	}

	/**
	 * Obtiene style correo.
	 *
	 * @return style correo
	 */
	public String getStyleCorreo() {
		return styleCorreo;
	}

	/**
	 * Establece el style correo.
	 *
	 * @param styleCorreo el new style correo
	 */
	public void setStyleCorreo(String styleCorreo) {
		this.styleCorreo = styleCorreo;
	}

	/**
	 * Obtiene style codigo.
	 *
	 * @return style codigo
	 */
	public String getStyleCodigo() {
		return styleCodigo;
	}

	/**
	 * Establece el style codigo.
	 *
	 * @param styleCodigo el new style codigo
	 */
	public void setStyleCodigo(String styleCodigo) {
		this.styleCodigo = styleCodigo;
	}

    /**
     * Establece el answer.
     *
     * @param answer el new answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     * Establece el contrasena antigua.
     *
     * @param contrasenaAntigua el new contrasena antigua
     */
    public void setContrasenaAntigua(String contrasenaAntigua) {
        this.contrasenaAntigua = contrasenaAntigua;
    }

    /**
     * Establece el contrasena nueva.
     *
     * @param contrasenaNueva el new contrasena nueva
     */
    public void setContrasenaNueva(String contrasenaNueva) {
        this.contrasenaNueva = contrasenaNueva;
    }

    /**
     * Establece el correo electronico per.
     *
     * @param correoElectronicoPer el new correo electronico per
     */
    public void setCorreoElectronicoPer(String correoElectronicoPer) {
        this.correoElectronicoPer = correoElectronicoPer;
    }

    /**
     * Establece el count.
     *
     * @param count el new count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Establece el key.
     *
     * @param key el new key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Establece el login.
     *
     * @param login el new login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Establece el mensaje.
     *
     * @param mensaje el new mensaje
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * Establece el mensaje error.
     *
     * @param mensajeError el new mensaje error
     */
    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    /**
     * Establece el mostrar captcha.
     *
     * @param mostrarCaptcha el new mostrar captcha
     */
    public void setMostrarCaptcha(boolean mostrarCaptcha) {
        this.mostrarCaptcha = mostrarCaptcha;
    }

    /**
     * Establece el password.
     *
     * @param password el new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Establece el prueba captcha.
     *
     * @param pruebaCaptcha el new prueba captcha
     */
    public void setPruebaCaptcha(String pruebaCaptcha) {
        this.pruebaCaptcha = pruebaCaptcha;
    }

    /**
     * Establece el remember me.
     *
     * @param rememberMe el new remember me
     */
    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    /**
     * Establece el repetir nueva contrasena.
     *
     * @param repetirNuevaContrasena el new repetir nueva contrasena
     */
    public void setRepetirNuevaContrasena(String repetirNuevaContrasena) {
        this.repetirNuevaContrasena = repetirNuevaContrasena;
    }

    /**
     * Establece el user temp.
     *
     * @param userTemp el new user temp
     */
    public void setUserTemp(HashMap userTemp) {
        this.userTemp = userTemp;
    }

    /**
     * Establece el usuario asignado.
     *
     * @param usuarioAsignado el new usuario asignado
     */
    public void setUsuarioAsignado(String usuarioAsignado) {
        this.usuarioAsignado = usuarioAsignado;
    }

    /**
     * Obtiene name image byte.
     *
     * @return name image byte
     */
    public byte[] getNameImageByte() {
        return nameImageByte;
    }

    /**
     * Establece el name image byte.
     *
     * @param nameImageByte el new name image byte
     */
    public void setNameImageByte(byte[] nameImageByte) {
        this.nameImageByte = nameImageByte;
    }

	/**
	 * Comprueba si es usuario bloqueado.
	 *
	 * @return true, si es usuario bloqueado
	 */
	public boolean isUsuarioBloqueado() {
		return usuarioBloqueado;
	}

	/**
	 * Establece el usuario bloqueado.
	 *
	 * @param usuarioBloqueado el new usuario bloqueado
	 */
	public void setUsuarioBloqueado(boolean usuarioBloqueado) {
		this.usuarioBloqueado = usuarioBloqueado;
	}
    
}