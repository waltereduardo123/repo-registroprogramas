/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.adm.ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.business.type.TipoAccionAdministrarUsuario;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.NotificacionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.OrganismoPerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.RolDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoPKDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioRolDTO;
import pe.gob.minedu.escale.adm.model.ws.dto.BandejaUsuarioDTO;
import pe.gob.minedu.escale.adm.vo.BusquedaUsuarioVO;
import pe.gob.minedu.escale.adm.vo.RecuperarClaveVO;
import pe.gob.minedu.escale.adm.vo.UsuarioCriteriaVO;
import pe.gob.minedu.escale.adm.vo.UsuarioSessionVO;
import pe.gob.minedu.escale.adm.wd.utils.UBaseManage;
import pe.gob.minedu.escale.adm.ws.util.QueryParamURL;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.business.state.UsuarioState;
import pe.gob.minedu.escale.common.constans.IndicadorConstant;
import pe.gob.minedu.escale.common.notify.NotificacionUtil;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.StringUtil;

/**
 * REST Web Service
 *
 * @author IMENDOZA
 */
@Path("/usuario")
@Stateless
public class UsuarioWs extends UBaseManage{

    
    /** El contexto. */
    private ServiceContext context = new ServiceContext();

    /** El servicio de administracion service local. */
    @EJB
    private transient AdministracionServiceLocal administracionServiceLocal;
    /** El servicio de usuario service local. */
    @EJB
    private transient UsuarioServiceLocal usuarioServiceLocal;
    
    /** El objeto usuario detalle. */
    private UsuarioOrganismoDTO usuarioDetalle;
    
    /** El servicio de notificacion service local. */
    @EJB
    private  NotificacionServiceLocal notificacionServiceLocal;
    
    
    private BCryptPasswordEncoder passwordEncoder;
    
    private static String CREDENCIALES_INCORRECTAS = "31: Las credenciales son incorrectas";
    private static String CREDENCIALES_INCORRECTAS_PASS_INVALIDO = "21:Credenciales incorrectas: contrasena no valida";
    private static String CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE = "20:Credenciales incorrectas: No existe Usuario";
    private static String CREDENCIALES_INCORRECTAS_EMAIL_INEXISTENTE = "23:Credenciales incorrectas: No existe email";
    private static String CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE = "24:Credenciales incorrectas: No coincide el email y el usuario";
    
    
    private static String CREDENCIALES_CORRECTAS_NO_ADMINISTRADOR = "32: Las credenciales son correctas pero el usuario no es principal";
    private static String CREDENCIALES_CORRECTAS_INHABILITADO_INTENTOS_FALLIDOS = "13:Credenciales correctas: Usuario inhabilitado por intetos fallidos";
    private static String CREDENCIALES_CORRECTAS_INHABILITADO = "12:Credenciales correctas: Usuario inhabilitado";
    private static String USUARIO_INEXISTENTE = "33: Todo lo anterior está bien pero, no encontró al usuario";
    private static String USUARIO_EXISTE_NO_ENTIDAD = "34: Todo lo anterior está bien pero el usuario solicitado no corresponde al ámbito permitido";
    private static String USUARIO_INHABILITADO = "14: Usuario inhabilitado";
    private static String USUARIO_EXISTE_CODOID = "30: Ya existe un usuario con el mismo numero de documento";
    private static String USUARIO_INEXISTENTE_CODOID = "45: El numero de documento no existe";
    private static String USUARIO_CODOID_INEXISTENTE = "34: El nombre de usuario o numero de documento es nulo ";
    private static String ERROR_INTERNO = "22:Error Interno";
    private static String ACTUALIZACION_MIGRACION_EXITOSA = "40: Actualizacion por migracion exitosa";
    private static String ACTUALIZACION_CONTRASENA_EXITOSA = "41: Actualizacion de contrasena exitosa";
    private static String REGISTRO_USUARIOORGANISMO_EXITOSA = "42: Se registro con exito el usuario";
    private static String REGISTRO_USUARIOORGANISMO_SIN_EXITO = "43: Se registro sin exito el usuario";
    private static String BUSQUEDA_USUARIO_EXITOSA = "44: La busqueda del usuario fue exitosa";
    
    
    /**
     * Creates a new instance of UsuarioWs
     */
    public UsuarioWs() {
    }



    
    @POST
    @Path("/validaLoginEscale")
    public String validaLoginEscale(String url) {
        UsuarioDTO usuario;
        List<UsuarioOrganismoDTO> listaUsuarioOrganismo;
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        try {
            usuario = administracionServiceLocal.buscarUsuarioxOID(QueryParamURL.getParam(url, "idCodoId"));
            if(usuario != null){
                if ("0".equals(QueryParamURL.getParam(url, "idEstado"))) {
                    usuario.setEstado(UsuarioState.BLOQUEADO_INTENTOS_FALLIDOS.getKey());
                    usuarioServiceLocal.actualizarUsuario(usuario);
                    return  CREDENCIALES_CORRECTAS_INHABILITADO_INTENTOS_FALLIDOS;
                }else
                if(passwordEncoder.matches(QueryParamURL.getParam(url, "pass"), usuario.getContrasena())){
                    if (usuario.getEstado().equals(UsuarioState.BLOQUEADO_INTENTOS_FALLIDOS.getKey())) {
                        return  CREDENCIALES_CORRECTAS_INHABILITADO_INTENTOS_FALLIDOS;
                    }
                    if(usuario.getEstado().equals(UsuarioState.ACTIVO.getKey())){
                                listaUsuarioOrganismo = administracionServiceLocal.obtenerUsuarioOrganismoByidUsuario(usuario.getId());
                                int cont = 0;
                                for (UsuarioOrganismoDTO usuarioOrganismoDTO : listaUsuarioOrganismo) {                                    
                                    usuarioOrganismoDTO.setUsuario(null);          
                                    usuarioOrganismoDTO.setOrganismoPerfil(null);
                                        for (UsuarioRolDTO usuarioRolDTO : usuarioOrganismoDTO.getListaUsuarioRol()) {
                                            usuarioRolDTO.setUsuarioOrganismo(null);                                        
                                    }
                                    if (usuarioOrganismoDTO.getEstado().equals(EstadoState.ACTIVO.getKey())) {
                                        cont++;
                                    }
                                }
                                if (cont<=0) {
                                    return  CREDENCIALES_CORRECTAS_INHABILITADO;//Credenciales correctas: Usuario inhabilitado
                                }

                                usuario.setListaUsuarioOrganismo(listaUsuarioOrganismo);



                                usuario.setFechaCreacion(null);

                                Date ultimaFechaAcceso = new Date();
                                ultimaFechaAcceso = usuarioServiceLocal.ultimaFechadeActividad(usuario.getId());
                                usuario.setUltimaFechadeActividad(FechaUtil.obtenerFechaFormatoPersonalizado(ultimaFechaAcceso, "dd/MM/yyyy hh:mm:ss"));
                                UsuarioSessionVO usuarioSession = new UsuarioSessionVO();

                                usuarioSession.setCorreoPersonal(usuario.getEmail());
                                usuarioSession.setCargoInstitucional(usuario.getListaUsuarioOrganismo().get(0).getCargo());                            
                                usuarioSession.setCorreoLogin(usuario.getEmail());
                                usuarioSession.setHostRemoto(obtieneIPCliente());
//                                usuarioSession.setHostRemoto(QueryParamURL.getParam(url, "ipHostRemoto"));
                                usuarioSession.setIdEntidad(usuario.getListaUsuarioOrganismo().get(0).getOrganismo().getId());
                                usuarioSession.setIdPerfil1(usuario.getListaUsuarioOrganismo().get(0).getListaUsuarioRol().get(0).getPerfilRol().getPerfil().getId());                            
                                usuarioSession.setIdUsuario(usuario.getId());
                                usuarioSession.setIndClaveTemporal(0);
                                usuarioSession.setNombreEntidad(usuario.getListaUsuarioOrganismo().get(0).getOrganismo().getNombreOrganismo());
                                usuarioSession.setNombreUsuario(usuario.getNombreCompleto());
                                usuarioSession.setOidUsuario(usuario.getCodigoOID());
                                usuarioSession.setUsuarioState(UsuarioState.ACTIVO);



                                administracionServiceLocal.guardarLogUsuIngreso(usuarioSession, usuarioSession.getHostRemoto());



                                JSONArray jsArray = new JSONArray();
                                JSONObject job;
                                usuario.setContrasena(null);

                                job = new JSONObject(usuario);
                                jsArray.put(job);
                                return  jsArray.toString(); 

                    }else{
                        return  CREDENCIALES_CORRECTAS_INHABILITADO;//Credenciales correctas: Usuario inhabilitado
                    }     

                }else{
                    return CREDENCIALES_INCORRECTAS_PASS_INVALIDO;//Credenciales incorrectas: contrasena no valida
                }
            }else{
                return  CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE;//Credenciales incorrectas: No existe Usuario 
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);
            return  ERROR_INTERNO; //Error Interno
        }  
        
        
    }
    
    @PUT
    @Path("/actualizarDatosByMigracion")
    @Consumes({"application/json"})
    public Response actualizarDatosByMigracion(UsuarioDTO usuario) {        
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        try {
            UsuarioDTO usuarioExistente;
            if(usuario.getNombres()!= null && !usuario.getCodigoOID().isEmpty() && usuario.getContrasena().trim().length() >=8){                  
                
                usuarioExistente = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID());
                
                if (usuarioExistente == null) {
                    usuario.setUltimoUsuarioModificacion(usuario.getNombreCompleto());
                    usuario.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                    usuario.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());

                    usuario.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
                    usuarioServiceLocal.actualizarUsuario(usuario);
                    return Response.ok(ACTUALIZACION_MIGRACION_EXITOSA).build();
                }else if(usuarioExistente.getId().longValue()!= usuario.getId().longValue()){
                    return Response.ok(USUARIO_EXISTE_CODOID).build();
                }else if(usuarioExistente.getId().longValue() == usuario.getId().longValue()){
                    usuario.setUltimoUsuarioModificacion(usuario.getNombreCompleto());
                    usuario.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                    usuario.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());

                    usuario.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
                    usuarioServiceLocal.actualizarUsuario(usuario);
                   return Response.ok(ACTUALIZACION_MIGRACION_EXITOSA).build();
                }                                        
            }                        
        } catch (Exception ex) {
            Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);            
            return Response.ok(ERROR_INTERNO).build();
           
        }
        return Response.ok(ERROR_INTERNO).build();
        
    }

    

    @POST
    @Path("/buscadorUsuario")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response buscadorUsuario(UsuarioCriteriaVO criteria) {        
        List<BusquedaUsuarioVO> listaUsuario;
        try {
            listaUsuario = usuarioServiceLocal.buscarUsuario(context,criteria);
            
            JSONArray jsArray = new JSONArray();
            JSONObject job;
            for (BusquedaUsuarioVO busquedaUsuario : listaUsuario) {
                job = new JSONObject(busquedaUsuario);
                jsArray.put(job);
            }           
            return Response.ok(jsArray.toString()).build();
            
        } catch (Exception ex) {
            Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);
            return Response.ok(ERROR_INTERNO).build();
        }
        
    }
    
    @POST
    @Path("/recuperarClaveUsuario")
    public String recuperarClaveUsuario(String url) {
        
        UsuarioDTO usuario;        
        List<UsuarioDTO> dto = null;    
        try {            
            if (!StringUtil.isNotNullOrBlank(QueryParamURL.getParam(url, "idCodoId"))) {
                return  CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE;
            }
            if (!StringUtil.isNotNullOrBlank(QueryParamURL.getParam(url, "email"))) {
                return  CREDENCIALES_INCORRECTAS_EMAIL_INEXISTENTE;
            }
            usuario = usuarioServiceLocal.buscarUsuarioxOID(QueryParamURL.getParam(url, "idCodoId"));
            if (usuario == null) {
                return  CREDENCIALES_INCORRECTAS_USUARIO_INEXISTENTE;
            }            

            dto = usuarioServiceLocal.buscarUsuarioCorreo(QueryParamURL.getParam(url, "idCodoId"), QueryParamURL.getParam(url, "email"));
            if (dto == null) {
                return  CREDENCIALES_INCORRECTAS_USUARIO_EMAIL_INEXISTENTE;
            }else{
                List<Object[]> data = usuarioServiceLocal.buscarUsuarioRecuperarClave(QueryParamURL.getParam(url, "idCodoId"));
                if (String.valueOf(data).equalsIgnoreCase("[]")) {
                    return  USUARIO_INHABILITADO;
                }
            }
            RecuperarClaveVO recuperarClaveVO = new RecuperarClaveVO();
            recuperarClaveVO.setCorreoElectronico(QueryParamURL.getParam(url, "email"));
            recuperarClaveVO.setFlagCapcha(false);
            recuperarClaveVO.setUsuario(QueryParamURL.getParam(url, "idCodoId"));
            administracionServiceLocal.recuperarClave(recuperarClaveVO);
            return  ACTUALIZACION_CONTRASENA_EXITOSA;
                                 
        } catch (Exception ex) {
               Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);
               return  ERROR_INTERNO; //Error Interno
        }        
    }
    
    @PUT
    @Path("/actualizarDatos")
    @Consumes({"application/json"})
    public Response actualizarDatos(UsuarioDTO usuario) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        try {
            UsuarioDTO usuarioExistente;
            
            if(usuario.getNombres()!= null && !usuario.getCodigoOID().isEmpty()){                  
                
                usuarioExistente = administracionServiceLocal.buscarUsuarioxOID(usuario.getCodigoOID());                                
                
                usuario.setUltimoUsuarioModificacion(usuario.getNombreCompleto());
                usuario.setUltimaFechaModificacion(FechaUtil.obtenerFechaActual());
                usuario.setUltimaModificacionPass(FechaUtil.obtenerFechaActual());
                if (StringUtil.isNotNullOrBlank(usuario.getContrasena())) {                    
                    usuario.setContrasena(passwordEncoder.encode(usuario.getContrasena()));
                }else{
                    usuario.setContrasena(null);
                }                
                usuarioServiceLocal.actualizarUsuario(usuario);
            }else{
                return Response.ok(USUARIO_CODOID_INEXISTENTE).build();
            }
                                                                
            return Response.ok(usuario).build();

        } catch (Exception e) {
            return Response.ok(ERROR_INTERNO).build();
        }
    }
    
     @PUT
    @Path("/registroUsuarioOrganismo")
    @Consumes({"application/json"})
    public Response registroUsuarioOrganismo(BandejaUsuarioDTO bandejaUsuarioDTO) {                        
        try {
            String flagCreacion = "";
            Integer solicitud = 0;
            UsuarioDTO usuarioDTO = new UsuarioDTO();
            if (StringUtil.isNotNullOrBlank(bandejaUsuarioDTO.getNTiposolicitud())) {
                solicitud = bandejaUsuarioDTO.getNTiposolicitud();
            }                         
            switch(solicitud){
                case 1: flagCreacion = TipoAccionAdministrarUsuario.NUEVO_USUARIO.getValue();
                        usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(bandejaUsuarioDTO.getCCodoid());
                        if (usuarioDTO != null) {
                            return Response.ok(USUARIO_EXISTE_CODOID).build();
                        }
                     break;
                case 2:
                case 3: flagCreacion = TipoAccionAdministrarUsuario.MODIFICAR_USUARIO.getValue();                        
                break;
                default: flagCreacion = "Defecto";
               break;
            }
            
            
            usuarioDetalle = new UsuarioOrganismoDTO();
            usuarioDetalle.setUsuario(new UsuarioDTO());
            usuarioDetalle.getUsuario().setCodigoOID(bandejaUsuarioDTO.getCCodoid().trim().toUpperCase());
            usuarioDetalle.getUsuario().setNombres(bandejaUsuarioDTO.getCNombre().trim().toUpperCase());
            usuarioDetalle.getUsuario().setApellidoPaterno(bandejaUsuarioDTO.getCAppt().trim().toUpperCase());
            usuarioDetalle.getUsuario().setApellidoMaterno(bandejaUsuarioDTO.getCApmt().trim().toUpperCase());
            usuarioDetalle.getUsuario().setNombreCompleto(bandejaUsuarioDTO.getCNomcom().trim().toUpperCase());
            usuarioDetalle.getUsuario().setTipoDocumento(bandejaUsuarioDTO.getCTipdoc().trim().toUpperCase());
            usuarioDetalle.getUsuario().setDescDocumento(bandejaUsuarioDTO.getCDocum().trim().toUpperCase());
            usuarioDetalle.getUsuario().setEmail(bandejaUsuarioDTO.getCEmail().trim().toUpperCase());            
            usuarioDetalle.setTelefonoInstitucional(bandejaUsuarioDTO.getCNrotflins().trim().toUpperCase());
            usuarioDetalle.setAnexoInstitucional(bandejaUsuarioDTO.getCNroanxins().trim().toUpperCase());
            if (flagCreacion.equals(TipoAccionAdministrarUsuario.MODIFICAR_USUARIO.getValue())) {
               usuarioDTO = usuarioServiceLocal.verificarExistenciaUsuario(bandejaUsuarioDTO.getCCodoid()); 
               usuarioDetalle.setUsuarioOrganismoPK(new UsuarioOrganismoPKDTO(usuarioDTO.getId(), bandejaUsuarioDTO.getNIdOrgan()));
               usuarioDetalle.getUsuario().setId(usuarioDTO.getId());
                if (bandejaUsuarioDTO.getCEstado().equalsIgnoreCase(EstadoState.INACTIVO.getKey())) {
                    usuarioDetalle.setEstado(EstadoState.INACTIVO.getKey());
                    usuarioDetalle.getUsuario().setEstado(EstadoState.INACTIVO.getKey());                    
                }else{
                    usuarioDetalle.setEstado(EstadoState.ACTIVO.getKey());
                    usuarioDetalle.getUsuario().setEstado(EstadoState.ACTIVO.getKey()); 
                }
            }
            
            
            
            usuarioDetalle.setOrganismoPerfil(new OrganismoPerfilDTO());
            usuarioDetalle.getOrganismoPerfil().setPerfil(new PerfilDTO(bandejaUsuarioDTO.getNIdPerfil()));            

            RolDTO  rol = new RolDTO();
            rol.setId(bandejaUsuarioDTO.getNIdRol());
            rol.setHidden(true);
            List<RolDTO> listaRol = new ArrayList<RolDTO>();
            listaRol.add(rol);
            
            boolean indicadorVEDU = false;
            UsuarioSessionVO usuarioSessionVO = new UsuarioSessionVO();
            usuarioSessionVO.setOidUsuario(bandejaUsuarioDTO.getCUsucre());
            usuarioSessionVO.setNombreUsuario(bandejaUsuarioDTO.getCNomcom());
            context.setLocale(context.getLocale());
            context.setUsuarioSessionVO(usuarioSessionVO);
            ServiceContext.setCurrent(context);
            
            UsuarioOrganismoDTO usuarioOrganismo;
            usuarioOrganismo = usuarioServiceLocal.administrarUsuarioRegistrar(usuarioDetalle, context,listaRol,bandejaUsuarioDTO.getNIdOrgan(),flagCreacion,indicadorVEDU);
            
            if (usuarioOrganismo.getUsuario().getId() == null && usuarioOrganismo.getOrganismo().getId() == null) {                
                return Response.ok(REGISTRO_USUARIOORGANISMO_SIN_EXITO).build();
            }else{
                envioCorreoConfirmacion(usuarioOrganismo);
                return Response.ok(REGISTRO_USUARIOORGANISMO_EXITOSA).build();
            }                 
        } catch (Exception ex) {
            Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);
            return Response.ok(ERROR_INTERNO).build();
        }
    }
    
    @POST
    @Path("/obtenerDatosUsuario")    
    public String obtenerDatosUsuario(String url) {   
        UsuarioDTO usuarioAdmin;
        UsuarioDTO usuario; 
        passwordEncoder = new BCryptPasswordEncoder();
        String idCodoId,pass,idCodoIdBus;
        List<UsuarioOrganismoDTO> listaUsuarioOrganismoAdmin;
        List<UsuarioOrganismoDTO> listaUsuarioOrganismo;
        try {
            idCodoId = QueryParamURL.getParam(url, "idCodoId");
            pass = QueryParamURL.getParam(url, "pass");
            idCodoIdBus = QueryParamURL.getParam(url, "idCodoIdBus");
            
            if (StringUtil.isNotNullOrBlank(idCodoId) 
                    && StringUtil.isNotNullOrBlank(pass)) {                                                
                usuarioAdmin = administracionServiceLocal.buscarUsuarioxOID(idCodoId);
                
                if (passwordEncoder.matches(pass, usuarioAdmin.getContrasena())) {
                    if (StringUtil.isNotNullOrBlank(idCodoIdBus)) {
                        usuario = administracionServiceLocal.buscarUsuarioxOID(idCodoIdBus);
                        if (usuario == null) {
                            return USUARIO_INEXISTENTE_CODOID;
                        }else{
                            if(usuario.getEstado().equals(UsuarioState.ACTIVO.getKey())){
                                    listaUsuarioOrganismo = administracionServiceLocal.obtenerUsuarioOrganismoByidUsuario(usuario.getId());
                                    listaUsuarioOrganismoAdmin = administracionServiceLocal.obtenerUsuarioOrganismoByidUsuario(usuarioAdmin.getId());
                                    for (UsuarioOrganismoDTO usuarioOrganismoDTO : listaUsuarioOrganismo) {
                                        usuarioOrganismoDTO.setUsuario(null);          
                                        usuarioOrganismoDTO.setOrganismoPerfil(null);
                                            for (UsuarioRolDTO usuarioRolDTO : usuarioOrganismoDTO.getListaUsuarioRol()) {
                                                usuarioRolDTO.setUsuarioOrganismo(null);                                        
                                        }
                                    }

                                    usuario.setListaUsuarioOrganismo(listaUsuarioOrganismo);
                                    usuario.setFechaCreacion(null);
                                    usuario.setContrasena(null);

                                    for (UsuarioOrganismoDTO usuarioOrganismo : listaUsuarioOrganismoAdmin) {
                                        if (usuarioOrganismo.getIndicadorAdministradorEntidad() == 0) {
                                            return CREDENCIALES_CORRECTAS_NO_ADMINISTRADOR;
                                        }
                                        for (UsuarioOrganismoDTO usuarioOrganismoAdmin : listaUsuarioOrganismo) {
                                            if (!(usuarioOrganismo.getOrganismo().getId().equals(usuarioOrganismoAdmin.getOrganismo().getId()))) {
                                                return USUARIO_EXISTE_NO_ENTIDAD;
                                            }
                                        }
                                    }

                                    JSONArray jsArray = new JSONArray();
                                    JSONObject job;
                                    usuario.setContrasena(null);

                                    job = new JSONObject(usuario);
                                    jsArray.put(job);
                                    return  jsArray.toString();                                                              
                            }else{
                                return USUARIO_INHABILITADO;
                            }
                        }
                    }else{
                        return USUARIO_INEXISTENTE_CODOID;
                    }
                }else{
                    return CREDENCIALES_INCORRECTAS;
                }
            }
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);
            return ERROR_INTERNO;
        }
        return ERROR_INTERNO;
    }
    
    private void envioCorreoConfirmacion(UsuarioOrganismoDTO dto) throws Exception {
		try {
			List<String> destinatarios = new ArrayList<String>();
			destinatarios.add(dto.getUsuario().getEmail());
			Map<String, String> map = new HashMap<String, String>();
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_USUARIOACCESO, dto
					.getUsuario().getCodigoOID());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_FREGISTRO, FechaUtil
					.obtenerFechaFormatoSimple(dto.getUsuario()
							.getFechaCreacion()));
			map.put(NotificacionUtil.ELEMENTO_LABEL_USUARIO,
					dto.getIndicadorAdministradorEntidad().intValue() == IndicadorConstant.INDICADOR_ACTIVO ? NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO_ADM
							: NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO);
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_ENTIDAD, dto
					.getOrganismo().getNombreOrganismo().toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_DESTINATARIO, dto
					.getUsuario().getNombreCompleto()
					.toUpperCase());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_CLAVEACCESO, dto
					.getUsuario().getClave());
			map.put(NotificacionUtil.ELEMENTO_PLANTILLA_PERFIL,
					dto.getOrganismoPerfil().getPerfil().getNombre());
			map.put(NotificacionUtil.ELEMENTO_CADUCIDAD_CONTRASENHA,
					this.administracionServiceLocal
							.getParametro(ParametroType.NUMERO_DIAS_CAMBIO_CONTRASENHA
									.getValue()));
			notificacionServiceLocal.enviarCorreoConfirmacionUsuario(
					NotificacionUtil.ASUNTO_NOTIFICACION_EMAIL_CREACION_USUARIO.toString(), destinatarios, map,
					NotificacionUtil.RUTA_TEMPLATES_CREA_USUARIO.toString(),
					null);
		} catch (Exception ex) {                    
                    Logger.getLogger(UsuarioWs.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}