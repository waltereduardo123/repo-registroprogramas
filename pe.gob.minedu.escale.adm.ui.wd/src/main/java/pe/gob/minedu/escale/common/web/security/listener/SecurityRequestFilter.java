package pe.gob.minedu.escale.common.web.security.listener;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class SecurityRequestFilter implements Filter {

    /** La local request. */
    private static ThreadLocal<HttpServletRequest> localRequest = new ThreadLocal<HttpServletRequest>();

    /** La Constante REQUEST_ENCODING. */
    private static final String REQUEST_ENCODING = "UTF-8";

    /**
     * Obtiene request.
     *
     * @return request
     */
    public static HttpServletRequest getRequest() {
    	return localRequest.get();
    }

    /**
     * Obtiene session.
     *
     * @return session
     */
    public static HttpSession getSession() {
        HttpServletRequest request = localRequest.get();
        return (request != null) ? request.getSession() : null;
    }


    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    	
    	servletRequest.setCharacterEncoding(REQUEST_ENCODING);
     	servletResponse.setCharacterEncoding(REQUEST_ENCODING);
     	if (servletRequest instanceof HttpServletRequest) {
        	localRequest.set((HttpServletRequest) servletRequest);
        }
        try {
        	HttpServletRequest request = (HttpServletRequest) servletRequest;
                HttpSession session = request.getSession(false);
        	if (session != null) {        	        		
        		session.setAttribute("ultimoAccesoSistema",new Date());
        		
        	}
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            localRequest.remove();
        }
       
		
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy() {
    }
}