package pe.gob.minedu.escale.common.web.security;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 *
 * @author IMENDOZA
 */
public class EscaleCallbackHandler implements CallbackHandler {

	String name;
	String password;

	public EscaleCallbackHandler(String name, String password) {
		this.name = name;
		this.password = password;
	}

        @Override
	public void handle(Callback[] callbacks) throws UnsupportedCallbackException {		
            for (Callback callback : callbacks) {
                if (callback instanceof NameCallback) {
                    NameCallback nameCallback = (NameCallback) callback;
                    nameCallback.setName(name);
                } else if (callback instanceof PasswordCallback) {
                    PasswordCallback passwordCallback = (PasswordCallback) callback;
                    passwordCallback.setPassword(password.toCharArray());
                } else {
                    throw new UnsupportedCallbackException(callback, "La  presentacion Callback esta sin apoyo");
                }
            }
	}

}
