package pe.gob.minedu.escale.common.web.jsf.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;


public class FacesMessageUtil {

	/**
	 * Adds the message.
	 *
	 * @param facesContext the faces context
	 * @param severity the severity
	 * @param message the message
	 */
	public static void addMessage(FacesContext facesContext, Severity severity,
			String message) {
		FacesMessage facesMessage = new FacesMessage(severity, message, null);
		facesContext.addMessage(null, facesMessage);
	}

}
