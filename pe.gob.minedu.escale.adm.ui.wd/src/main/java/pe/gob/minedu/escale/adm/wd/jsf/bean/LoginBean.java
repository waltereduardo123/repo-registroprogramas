package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.security.auth.login.CredentialExpiredException;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import pe.gob.minedu.escale.adm.business.exception.CuentaBloqueadaIntentosFallidosException;
import pe.gob.minedu.escale.adm.business.exception.CuentaUsuarioCaducadaException;
import pe.gob.minedu.escale.adm.business.exception.CuentaUsuarioExpiradaException;
import pe.gob.minedu.escale.adm.business.exception.UsuarioNoExisteException;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.SeguridadServiceRemote;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.vo.UsuarioSessionVO;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;
import pe.gob.minedu.escale.common.web.security.EscaleCallbackHandler;
import pe.gob.minedu.escale.common.web.security.ModuloAutenticacion;
import pe.gob.minedu.escale.common.web.security.listener.SecurityRequestFilter;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.SERVICE_CONTEXT;
import pe.gob.minedu.escale.ctx.SistemaContextBean;



@ManagedBean(name ="loginBean")
@SessionScoped
public class LoginBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private UsuarioDTO usuario;
	private String fechaSistema;

        /** El contexto. */
        private ServiceContext context;
	/** Variable estatica que identifica el log de la clase */
	static final Logger log = Logger.getLogger(LoginBean.class);

        /** El servicio de administracion service local. */
        @EJB
        private transient AdministracionServiceLocal administracionServiceLocal;
 
        /** El control message util. */
        private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), LoginBean.class);  

	/**
	 * @return the usuario
	 */
	public UsuarioDTO getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fechaSistema
	 */
	public String getFechaSistema() {
		return fechaSistema;
	}

	/**
	 * @param fechaSistema
	 *            the fechaSistema to set
	 */
	public void setFechaSistema(String fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	@PostConstruct
	public void iniciar() {
		this.usuario = new UsuarioDTO();
	}

	/**
	 * Metodo que facilita ingresar al sistema
	 * @return rutaNavegacion, tipo String
        * @throws java.lang.Exception
	 */
	public String ingresarSistema() throws Exception {
		String rutaNavegacion = null;
		HttpSession miSession = null;
                rutaNavegacion = SistemaContextBean.VISTA_LIENZO_GENERAL;
                boolean exitoAutenticacion = false;
                HttpServletRequest request = SecurityRequestFilter.getRequest();
                HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);

                String username = "";
                String password = "";
                
                context = (ServiceContext)session.getAttribute(SERVICE_CONTEXT);
                if (context == null) {
                    context = new ServiceContext();
                    session.setAttribute(SERVICE_CONTEXT, context);                    
                }
                try {
                    session.setAttribute(SeguridadServiceRemote.USER_BLOCKED, false);
                    if(!StringUtil.isNotNullOrBlank(usuario.getCodigoOID()) && !StringUtil.isNotNullOrBlank(usuario.getClave())){
                        controlMessageUtil.imprimirMessageClave(ConstantesUtil.MESSAGE_ERROR_CREDENTIALS, FacesMessage.SEVERITY_INFO);
                        return null;
                    }
                    
                    if(!StringUtil.isNotNullOrBlank(usuario.getCodigoOID())){
                        controlMessageUtil.imprimirMessageClave(ConstantesUtil.MESSAGE_ERROR_USUARIO_INGRESE, FacesMessage.SEVERITY_INFO);
                        return null;
                    }
                    if(!StringUtil.isNotNullOrBlank(usuario.getClave())){
                        controlMessageUtil.imprimirMessageClave(ConstantesUtil.MESSAGE_ERROR_CLAVE_INGRESE, FacesMessage.SEVERITY_INFO);
                        return null;
                    }
                    
                    
                    username = this.usuario.getCodigoOID().trim();
                    password = this.usuario.getClave().trim();
                    EscaleCallbackHandler  handler  = new EscaleCallbackHandler(username, password);
                    ModuloAutenticacion autenticacion = new ModuloAutenticacion();
                    autenticacion.inicializar(null, handler, null, null);
                    exitoAutenticacion = autenticacion.login();
                    if (exitoAutenticacion) {  
                        registraSesionActiva(username, request, session);
                        session.removeAttribute(SeguridadServiceRemote.ERROR_MESSAGE);
                        session.removeAttribute(SeguridadServiceRemote.USER_BLOCKED);
                        
                        ServiceContext contexto = (ServiceContext)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SeguridadServiceRemote.SESSION_VAR);
			UsuarioSessionVO usuarioContexto = contexto.getUsuarioSessionVO();
                        
                        administracionServiceLocal.guardarLogUsuIngreso(usuarioContexto, usuarioContexto.getHostRemoto());
                        
                        usuario.setNombreCompleto(usuarioContexto.getNombreUsuario());
                        rutaNavegacion=SistemaContextBean.VISTA_LIENZO_GENERAL;
                        
                        
                    }else{
                        return null;
                    }
                        
                    
                     
                } catch (LoginException ex) {
                    controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_USER_EXISTE_EN_SISTEMA, FacesMessage.SEVERITY_INFO);
                    return null;
                }catch (UsuarioNoExisteException ue) {
                    controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_USER_EXISTE_EN_SISTEMA, FacesMessage.SEVERITY_INFO);
		}
                
                return rutaNavegacion;
        }

        /**
	 * Registrar sesion activa
	 *
	 * @param username the username
	 * @param request the request
	 * @param session the session
	 * @return the int
	 * @throws Exception the exception
	 */
        private int registraSesionActiva(String username, HttpServletRequest request, HttpSession session) throws Exception {
            int indicadorClaveTemporal = 0;           
            List<SelectItem> listaOrganismo = new ArrayList<SelectItem>();
            try {
                if (username != null) {
                    UsuarioSessionVO usuarioSessionVO = null;
                    List<UsuarioSessionVO> usuarioSessionVOList = administracionServiceLocal.getUsuarioSessionxOID(username,  request.getRemoteHost());
                    if (usuarioSessionVOList != null && !usuarioSessionVOList.isEmpty())
                    {
                            usuarioSessionVO = usuarioSessionVOList.get(0);
                            usuarioSessionVO.setHostRemoto(request.getRemoteHost());
                            indicadorClaveTemporal = usuarioSessionVO.getIndClaveTemporal();
                            if (usuarioSessionVOList.size() > 1) {
                                List<Object[]> listaOrganismos = administracionServiceLocal.buscarOrganismoxOID(username);
                                if (listaOrganismos != null && listaOrganismos.size() > 0) {
                                    for (Object[] obj : listaOrganismos) {
                                        listaOrganismo.add(new SelectItem(Long.valueOf(obj[0].toString()), String.valueOf(obj[1])));
                                    }
                                    session.setAttribute(SeguridadServiceRemote.LIST_ORGS_VAR, listaOrganismo);
                                }
                                    usuarioSessionVO.setIdEntidad(null);
                        } else {
                            session.setAttribute(SeguridadServiceRemote.ID_ENTIDAD, usuarioSessionVO.getIdEntidad());
                        }
                        context.setLocale(context.getLocale());
                        context.setUsuarioSessionVO(usuarioSessionVO);
                        ServiceContext.setCurrent(context);
                        session.setAttribute(SeguridadServiceRemote.SESSION_VAR, context);
                    } else {
                            throw new CredentialExpiredException(SeguridadServiceRemote.MESSAGE_ERROR_USER_INACTIVO);
                    }
                }
            } catch (Exception e) {
                log.warn(e);
                    if (e instanceof CredentialExpiredException) {
                            throw (CredentialExpiredException) e;
                    }                    
                    if (e instanceof CuentaUsuarioCaducadaException) {
                            throw (CuentaUsuarioCaducadaException)e;
                    }
                    if (e instanceof CuentaUsuarioExpiradaException) {
                            throw (CuentaUsuarioExpiradaException)e;
                    }
                    if (e instanceof CuentaBloqueadaIntentosFallidosException) {
                            throw (CuentaBloqueadaIntentosFallidosException)e;
                    }
                    java.util.logging.Logger.getLogger(LoginBean.class.getName()).log(Level.WARNING, null, e);

            }
        return indicadorClaveTemporal;
            
        }
	
	/**
	 * permite cerrar la sesion de un usuario
	 * @return rutaNavegacion, tipo String
	 */
	public String cerrarSesion() {
		@SuppressWarnings("unused")
		String rutaNavegacion = null;
		try {
			HttpSession miSession = (HttpSession) FacesContext
					.getCurrentInstance().getExternalContext()
					.getSession(false);
			if (miSession != null) {
				miSession.invalidate();
			}			
		} catch (Exception e) {
			log.error("Problemas al cerrar sesion del sistema ESCALE", e);
		}
		return rutaNavegacion = SistemaContextBean.VISTA_LOGIN;
	}
	

}
