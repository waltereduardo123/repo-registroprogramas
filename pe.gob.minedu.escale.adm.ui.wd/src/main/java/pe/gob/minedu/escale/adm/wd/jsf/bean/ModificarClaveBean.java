package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.NotificacionServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.vo.ModificarClaveVO;
import pe.gob.minedu.escale.adm.vo.RecuperarClaveVO;
import pe.gob.minedu.escale.adm.vo.UsuarioSessionVO;
import pe.gob.minedu.escale.adm.web.util.ADMConstantes;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.adm.web.util.ValidacionUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.exception.BusinessException;
import pe.gob.minedu.escale.common.notify.NotificacionUtil;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;
import pe.gob.minedu.escale.common.web.jsf.util.FacesUtil;


@ManagedBean(name="modificarClaveBean")
@SessionScoped
public class ModificarClaveBean extends BaseBean implements Serializable {
	
    /** La Constante serialVersionUID. */
    private static final long serialVersionUID = -123453558210881593L;

    /** El servicio administracion service local. */
    @EJB
    private transient AdministracionServiceLocal administracionServiceLocal;
    
    /** El servicio notificacion service local. */
    @EJB
    private transient NotificacionServiceLocal notificacionServiceLocal;
    
    /** La variable answer. */
    private String answer;
    
    /** El contexto. */
    private ServiceContext context;
    
    /** La contrase�a antigua. */
    private String contrasenaAntigua;
    
    /** La contrase�a nueva. */
    private String contrasenaNueva;
    
    /** EL correo electronico. */
    private String correoElectronicoPer;
    
    /** EL countador. */
    private int count = 0;
    
    /** El key. */
    private String key;
    
    /** El login. */
    private String login;
    
    /** El mensaje. */
    private String mensaje;
    
    /** El mensaje error. */
    private String mensajeError = null;
    
    /** El flag mostrar captcha. */
    private boolean mostrarCaptcha = false;
    
    /** El nombre image. */
    private String nameImage = null;
    
    /** El password. */
    private String password;
    
    /** La prueba captcha. */
    private String pruebaCaptcha;
    
    /** El flag remember me. */
    private boolean rememberMe;
    
    /** La variable repetir nueva contrasena. */
    private String repetirNuevaContrasena;
    
    /** El usuario asignado. */
    private String usuarioAsignado;
    
    /** El usuario asignado. */
    private String requisitosClave;
    
    /** El usuario asignado. */
    private String requisitosClaveInicial;
    
	/** El control message util. */
    private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), ModificarClaveBean.class);    
    
    /** El objeto locale. */
    private Locale locale;
    
    /**
     * Instancia un nuevo modificar clave bean.
     */
    public ModificarClaveBean() {
        
    }
    
    /**
     * Post construct.
     */
	@PostConstruct
	public void postConstruct() {
            try {
                init();
                this.mensajeError = null;
                this.setMostrarCaptcha(false);
                context = FacesUtil.getContext();
                locale = context.getLocale();
                iniciarMensajes();
            } catch (Exception ex) {
                    controlMessageUtil.imprimirLog(ex, FacesMessage.SEVERITY_ERROR);
            }
	}
	
	
	/**
	 * Gets the cambio idioma.
	 *
	 * @return the cambio idioma
	 */
	public String getCambioIdioma() {
		try {
			if (!locale.equals(context.getLocale())) {
		        this.iniciarMensajes();
		        locale = context.getLocale();
			}
		} catch (Exception ex) {
			controlMessageUtil.imprimirLog(ex, FacesMessage.SEVERITY_ERROR);
		}
		return null;
	}
	
	/**
	 * Sets the cambio idioma.
	 *
	 * @param cambioIdioma the new cambio idioma
	 */
	public void setCambioIdioma(String cambioIdioma) {
		//para evitarProblemas de renderizado
	}
	
	/**
	 * Iniciar mensajes.
	 */
	private void iniciarMensajes() throws Exception {
            String minimun = administracionServiceLocal.getParametro(ParametroType.MINIMO_CARACTERES_CONTRASENA.getValue());
            String maximun = administracionServiceLocal.getParametro(ParametroType.MAXIMO_CARACTERES_CONTRASENA.getValue());
            requisitosClave = controlMessageUtil.getMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_REQUISITOS_CLAVE, minimun, maximun);
            requisitosClaveInicial = controlMessageUtil.getMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_REQUISITOS_CLAVE_INICIAL, minimun, maximun);
	}

    /**
     * Cancelar cambio clave.
     *
     * @return the string
     */
    public String cancelarCambioClave() {
        // validamos la nueva contrase�a
        return ADMConstantes.MODIFICAR_CLAVE_USUARIO;
    }

    /**
     * Confirmar cambio clave.
     *
     * @return the string
     */
    public String confirmarCambioClave() {
        // validamos la nueva contrase�a
        mensajeError = null;
        return ADMConstantes.MODIFICAR_CLAVE_TEMPORAL;
    }

    /**
     * Modificar contrasena.
     *
     * @return the string
     */
    public String modificarContrasena() {
        mensajeError = null;
        return ADMConstantes.MODIFICAR_CLAVE_TEMPORAL;
    }

    /**
     * Recuperar clave usuario.
     *
     * @return the string
     */
    public String recuperarClaveUsuario() {
        mensajeError = "";
        return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
    }
    /**
     * Validar nueva contrasena.
     *
     * @return the string
     */
    public String validarNuevaContrasena() {
    	context.setLocale(FacesUtil.getLocale());
        String[] values = { getContrasenaAntigua(), getContrasenaNueva(),
                getRepetirNuevaContrasena() };
        if (ValidacionUtil.checkEmptyOrBlanck(values)) {
            mensajeError = controlMessageUtil.getMessage(
                    ConstantesUtil.MENSAJE_ERROR_CAMPO_OBLIGATORIO);
            controlMessageUtil.imprimirMessage(mensajeError, FacesMessage.SEVERITY_ERROR);
            return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
        }
        // validamos la nueva contrasena
        UsuarioSessionVO usuario = context.getUsuarioSessionVO();
        ModificarClaveVO modificarClave = new ModificarClaveVO();
        modificarClave.setCoid(usuario.getOidUsuario());
        modificarClave.setUsuarioCorreo(usuario.getCorreoPersonal());
//        modificarClave.setUsuarioCorreo(usuario.getCorreoLogin());
        modificarClave.setContrasenaAnterior(getContrasenaAntigua());
        modificarClave.setContrasenaNueva(getContrasenaNueva());
        modificarClave.setContrasenaNuevaDos(getRepetirNuevaContrasena());
        try {
                UsuarioDTO usuarioClave = new UsuarioDTO();
                usuarioClave = administracionServiceLocal.buscarUsuarioxOID(usuario.getOidUsuario());
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                if(!(passwordEncoder.matches(modificarClave.getContrasenaAnterior(), usuarioClave.getContrasena()))){
                    controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_CONTRASENA_DISTINTA_AL_ANTERIOR, FacesMessage.SEVERITY_ERROR);
                    return ADMConstantes.MODIFICAR_CLAVE_USUARIO;
                }
                
        } catch (Exception e) {
            controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR); 
        }
        
        try {
            administracionServiceLocal.modificarClave(context.getUsuarioSessionVO(),modificarClave);
            controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_EXITO_MODIFICAR_CLAVE_TEMPORAL, FacesMessage.SEVERITY_INFO);
            this.envioCorreoModificacion(usuario, getContrasenaNueva());
            setRememberMe(true);
        } catch (BusinessException be) {
            controlMessageUtil.imprimirMessage(be, FacesMessage.SEVERITY_ERROR);            
        } catch (Exception e) {
              controlMessageUtil.imprimirMessageClave(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_CONTRASENA_IGUAL_AL_ANTERIOR, FacesMessage.SEVERITY_ERROR);
//            controlMessageUtil.imprimirMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_CONTRASENA_IGUAL_AL_ANTERIOR, FacesMessage.SEVERITY_ERROR); 
//            if (e.getMessage().equalsIgnoreCase(pe.gob.minedu.escale.common.web.util.ConstantesUtil.CODIGO_ERROR_53_LDAP)) {
//            	controlMessageUtil.imprimirMessage(pe.gob.minedu.escale.adm.web.util.ConstantesUtil.MENSAJE_ERROR_CONTRASENA_IGUAL_AL_ANTERIOR, FacesMessage.SEVERITY_ERROR); 
//            }
        }
        return ADMConstantes.MODIFICAR_CLAVE_USUARIO;
    }
    
    /**
     * Envio correo modificacion.
     *
     * @param usuario el usuario
     * @param contrasenhaNueva el contrasenha nueva
     * @throws Exception the exception
     */
    private void envioCorreoModificacion(UsuarioSessionVO usuario, String contrasenhaNueva)
	throws Exception {

		List<String> destinatarios = new ArrayList<String>();
//		destinatarios.add(usuario.getCorreoLogin());
		destinatarios.add(usuario.getCorreoPersonal());		
		Map<String, String> map = new HashMap<String, String>();
		map.put(NotificacionUtil.ELEMENTO_PLANTILLA_FREGISTRO, 
				FechaUtil.obtenerFechaFormatoSimple(FechaUtil.obtenerFechaActual()));
		map.put(NotificacionUtil.ELEMENTO_LABEL_USUARIO, usuario.getIndicadorAdministradorEntidad() == 1
				? NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO_ADM
				: NotificacionUtil.ELEMENTO_LABEL_TITLE_USUARIO);
		map.put(NotificacionUtil.ELEMENTO_PLANTILLA_USUARIOACCESO, usuario.getOidUsuario());
		map.put(NotificacionUtil.ELEMENTO_PLANTILLA_ENTIDAD, 
				usuario.getNombreEntidad());
		map.put(NotificacionUtil.ELEMENTO_PLANTILLA_DESTINATARIO, usuario.getNombreUsuario());
		map.put(NotificacionUtil.ELEMENTO_PLANTILLA_CLAVEACCESO, contrasenhaNueva);
		map.put(NotificacionUtil.ELEMENTO_CADUCIDAD_CONTRASENHA, this.administracionServiceLocal.getParametro(ParametroType.NUMERO_DIAS_CAMBIO_CONTRASENHA.getValue()));
		
		notificacionServiceLocal.enviarCorreoConfirmacionUsuario(
				NotificacionUtil.ASUNTO_NOTIFICACION_EMAIL_MODIFICACION_CLAVE	//CLAVE DE ACCESO AL ESCALE
						.toString(), destinatarios, map,
				NotificacionUtil.RUTA_TEMPLATES_RECUPERA_CLAVE.toString(), null);	//usuario_clave.ftl*/
	}

    /**
     * Validar recuperar clave.
     *
     * @return the string
     */
    public String validarRecuperarClave() {
        String[] values = { getUsuarioAsignado(), getCorreoElectronicoPer(),getPruebaCaptcha() };
//        boolean flagCaptcha = false;
        if (ValidacionUtil.checkEmptyOrBlanck(values)) {
            mensajeError = controlMessageUtil.getMessage(ConstantesUtil.MENSAJE_ERROR_CAMPO_OBLIGATORIO);
            return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
        }
//        if (pruebaCaptcha.equalsIgnoreCase(answer)) {
//            flagCaptcha = true;
//        } else {
//            flagCaptcha = false;
//        }
        RecuperarClaveVO recuperarClaveVO = new RecuperarClaveVO();
        recuperarClaveVO.setCorreoElectronico(getCorreoElectronicoPer());
//        recuperarClaveVO.setFlagCapcha(flagCaptcha);
        recuperarClaveVO.setUsuario(getUsuarioAsignado());
        try {
            administracionServiceLocal.recuperarClave(recuperarClaveVO);
            controlMessageUtil.imprimirMessage(ConstantesUtil.MENSAJE_EXITO_MODIFICAR, FacesMessage.SEVERITY_INFO);
        } catch (Exception e2) {
        	controlMessageUtil.imprimirMessage(e2, FacesMessage.SEVERITY_ERROR);
        	
        }
        return ADMConstantes.RECUPERAR_CLAVE_USUARIO;
    }

    /**
     * Inits the.
     */
    private void init() {
        login = "";
        password = "";
        mensajeError = "";
        contrasenaAntigua = "";
        contrasenaNueva = "";
        repetirNuevaContrasena = "";
        usuarioAsignado = "";
        correoElectronicoPer = "";
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        if (nameImage != null) {
            ValidacionUtil.borrarArchivo(nameImage);
        }
        super.finalize();
    }
    
    /**
     * Limpiar.
     */
    public void limpiar() {
    	  init();
    }
    //get y set
    /**
     * Obtiene answer.
     *
     * @return answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Obtiene contrasena antigua.
     *
     * @return contrasena antigua
     */
    public String getContrasenaAntigua() {
        return contrasenaAntigua;
    }

    /**
     * Obtiene contrasena nueva.
     *
     * @return contrasena nueva
     */
    public String getContrasenaNueva() {
        return contrasenaNueva;
    }

    /**
     * Obtiene correo electronico per.
     *
     * @return correo electronico per
     */
    public String getCorreoElectronicoPer() {
        return correoElectronicoPer;
    }

    /**
     * Obtiene count.
     *
     * @return count
     */
    public int getCount() {
        return count;
    }

    /**
     * Obtiene key.
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Obtiene login.
     *
     * @return login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Obtiene mensaje.
     *
     * @return mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Obtiene mensaje error.
     *
     * @return mensaje error
     */
    public String getMensajeError() {
        return mensajeError;
    }

//    /**
//     * Obtiene name image.
//     *
//     * @return name image
//     */
//    public String getNameImage() {
//    	try {
//	        ValidacionUtil.setFacesContext(FacesUtil.getFacesContext());
//	        if (nameImage != null) {
//	            ValidacionUtil.borrarArchivo(nameImage);
//	        }
//	        String[] data = ValidacionUtil.createImage();
//	        answer = data[0];
//	        setNameImage(data[1]);
//    	} catch (Exception e) {
//    		controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
//		}
//        return nameImage;
//    }

    /**
     * Obtiene password.
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Obtiene prueba captcha.
     *
     * @return prueba captcha
     */
    public String getPruebaCaptcha() {
        return pruebaCaptcha;
    }

    /**
     * Obtiene repetir nueva contrasena.
     *
     * @return repetir nueva contrasena
     */
    public String getRepetirNuevaContrasena() {
        return repetirNuevaContrasena;
    }

    /**
     * Obtiene usuario asignado.
     *
     * @return usuario asignado
     */
    public String getUsuarioAsignado() {
        return usuarioAsignado;
    }

    /**
     * Comprueba si es mostrar captcha.
     *
     * @return true, si es mostrar captcha
     */
    public boolean isMostrarCaptcha() {
        return mostrarCaptcha;
    }

    /**
     * Comprueba si es remember me.
     *
     * @return true, si es remember me
     */
    public boolean isRememberMe() {
        return rememberMe;
    }

    /**
     * Establece el answer.
     *
     * @param answer el new answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     * Establece el context.
     *
     * @param context el new context
     */
    public void setContext(ServiceContext context) {
        this.context = context;
    }

    /**
     * Establece el contrasena antigua.
     *
     * @param contrasenaAntigua el new contrasena antigua
     */
    public void setContrasenaAntigua(String contrasenaAntigua) {
        this.contrasenaAntigua = contrasenaAntigua;
    }

    /**
     * Establece el contrasena nueva.
     *
     * @param contrasenaNueva el new contrasena nueva
     */
    public void setContrasenaNueva(String contrasenaNueva) {
        this.contrasenaNueva = contrasenaNueva;
    }

    /**
     * Establece el correo electronico per.
     *
     * @param correoElectronicoPer el new correo electronico per
     */
    public void setCorreoElectronicoPer(String correoElectronicoPer) {
        this.correoElectronicoPer = correoElectronicoPer;
    }

    /**
     * Establece el count.
     *
     * @param count el new count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Establece el key.
     *
     * @param key el new key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Establece el login.
     *
     * @param login el new login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Establece el mensaje.
     *
     * @param mensaje el new mensaje
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * Establece el mensaje error.
     *
     * @param mensajeError el new mensaje error
     */
    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    /**
     * Establece el mostrar captcha.
     *
     * @param mostrarCaptcha el new mostrar captcha
     */
    public void setMostrarCaptcha(boolean mostrarCaptcha) {
        this.mostrarCaptcha = mostrarCaptcha;
    }

    /**
     * Establece el name image.
     *
     * @param nameImage el new name image
     */
    public void setNameImage(String nameImage) {
        this.nameImage = nameImage;
    }

    /**
     * Establece el password.
     *
     * @param password el new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Establece el prueba captcha.
     *
     * @param pruebaCaptcha el new prueba captcha
     */
    public void setPruebaCaptcha(String pruebaCaptcha) {
        this.pruebaCaptcha = pruebaCaptcha;
    }

    /**
     * Establece el remember me.
     *
     * @param rememberMe el new remember me
     */
    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    /**
     * Establece el repetir nueva contrasena.
     *
     * @param repetirNuevaContrasena el new repetir nueva contrasena
     */
    public void setRepetirNuevaContrasena(String repetirNuevaContrasena) {
        this.repetirNuevaContrasena = repetirNuevaContrasena;
    }

    /**
     * Establece el usuario asignado.
     *
     * @param usuarioAsignado el new usuario asignado
     */
    public void setUsuarioAsignado(String usuarioAsignado) {
        this.usuarioAsignado = usuarioAsignado;
    }

	/**
	 * Obtiene requisitos clave.
	 *
	 * @return requisitos clave
	 */
	public String getRequisitosClave() {
		return requisitosClave;
	}

	/**
	 * Establece el requisitos clave.
	 *
	 * @param requisitosClave el new requisitos clave
	 */
	public void setRequisitosClave(String requisitosClave) {
		this.requisitosClave = requisitosClave;
	}

	/**
	 * Obtiene requisitos clave inicial.
	 *
	 * @return requisitos clave inicial
	 */
	public String getRequisitosClaveInicial() {
		return requisitosClaveInicial;
	}

	/**
	 * Establece el requisitos clave inicial.
	 *
	 * @param requisitosClaveInicial el new requisitos clave inicial
	 */
	public void setRequisitosClaveInicial(String requisitosClaveInicial) {
		this.requisitosClaveInicial = requisitosClaveInicial;
	}
}