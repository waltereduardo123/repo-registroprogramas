package pe.gob.minedu.escale.adm.wd.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import pe.gob.minedu.escale.adm.business.type.FechaAccionType;
import pe.gob.minedu.escale.adm.business.type.ParametroType;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.PerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilRolDTO;
import pe.gob.minedu.escale.adm.model.dto.RolDTO;
import pe.gob.minedu.escale.adm.vo.ErrorVO;
import pe.gob.minedu.escale.adm.vo.PerfilCriteriaVO;
import pe.gob.minedu.escale.adm.vo.RolVO;
import pe.gob.minedu.escale.adm.vo.SelectVO;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.adm.web.util.ValidacionUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.FactoryBean;
import pe.gob.minedu.escale.common.exception.BusinessException;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.FormatterUtil;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;
import pe.gob.minedu.escale.common.web.jsf.util.FacesMessageUtil;
import pe.gob.minedu.escale.common.web.jsf.util.FacesUtil;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_CREAR_ENTIDAD_TO_BUSCAR_PEFIL;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_CREAR_USUARIO;
import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.CALL_MODIFICAR_USUARIO;



@ManagedBean(name="perfilBean")
@SessionScoped
public class PerfilBean extends BaseBean implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 3231783511281901593L;
	
	/** El servicio de administracion service local. */
	@EJB
	private transient AdministracionServiceLocal administracionServiceLocal;
	
	/** La Constante EDITAR. */
	private static final String EDITAR = "EDITAR";

	/** La Constante ROL_BEAN. */
	private static final String ROL_BEAN = "rolBean";

	/** La Constante NUEVO. */
	private static final String NUEVO = "NUEVO";

	/** La Constante PERFILBEAN_LIST_ROL. */
	private static final String PERFILBEAN_LIST_ROL = "PERFILBEAN_LIST_ROL";

	/** La Constante INPUT_ERROR. */
	private static final String INPUT_ERROR = "inputError";
	
	/** El contexto. */
	private ServiceContext context;
	
	/** El objeto perfil dto. */
	private PerfilDTO dto;
	
	/** El objeto perfil criteria vo. */
	private PerfilCriteriaVO vo;
	
	/** La lista perfil dto. */
	private List<PerfilDTO> lista;
	
	/** La lista perfil rol dto inicial. */
	private List<PerfilRolDTO> listaPerfilRolDTOInicial;
	
	/** El id. */
	private Long id;
	
	/** La mensaje lista vacia. */
	private String mensajelistavacia;
	
	/** El style txt nombre. */
	private String styleTxtNombre;
	
	/** El style txt accion. */
	private String styleTxtAccion;
	
	/** El style txt descripcion. */
	private String styleTxtDescripcion;
	
	/** El style txt estado. */
	private String styleTxtEstado;
	
	/** El style txt finicio. */
	private String styleTxtFinicio;
	
	/** El style txt ffin. */
	private String styleTxtFfin;
	
	/** La lista acciones. */
	private List<SelectItem> acciones;
	
	/** La lista estados. */
	private List<SelectItem> estados;
	
	/** La lista estado filtrado. */
	private List<SelectItem> estadoFiltrado;
	
	/** El hidden. */
	private String hidden;
	
	/** La lista list rol. */
	private List<RolDTO> listRol;
	
	/** El page actual. */
	private String pageActual;
	
	/** El size. */
	private int size;
	
	/** El objeto error. */
	private ErrorVO error = null;
	
	/** La variable direccionar. */
	private String direccionar;
	
	/** El flag external. */
	private String flagExternal;
	
	/** El flag vis list perfil nuevo. */
	private boolean visListPerfilNuevo = false;
	
	/** El flag vis list perfil editar. */
	private boolean visListPerfilEditar = false;
	
	/** El flag ver check. */
	private boolean verCheck = false;
	
	/** El id organismo external. */
	private Long idOrganismoExternal = null;
	
	/** El indicador seleccion todo. */
	private boolean indicadorSeleccionTodo = false;
	
	/** La cantidad filas. */
	private int cantFilas = 0;
	
	/** El languaje acciones. */
	private String languajeAcciones = "";
	
	/** La lista r1. */
	private List<RolVO> listR1 = new ArrayList<RolVO>();
	
	/** La lista r2. */
	private List<RolVO> listR2;
	
	/** El size detalle perfil. */
	private int sizeDetallePerfil = 0;
	
	/** El nombre anterior. */
	private String nombreAnterior;

	/** El lenguaje estado filtrado. */
	private String lenguajeEstadoFiltrado = "";
	
	/** El lenguaje estados. */
	private String lenguajeEstados = "";
	
	/** El control message util. */
        private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), PerfilBean.class);   

        /** El indicador si la edicion fue exitosa */
	private boolean edicionExitosa = false;
	/**
	 * Instancia un nuevo perfil bean.
	 */
	public PerfilBean() {
	}
	
	/**
	 * Post construct.
	 */
	@PostConstruct
	public void postConstruct() {
		context = FacesUtil.getContext(); //ServiceContext
		mensajelistavacia = null;
		try {
			dto = FactoryBean.getBean(PerfilDTO.class);
			vo = FactoryBean.getBean(PerfilCriteriaVO.class);
			lista = new ArrayList<PerfilDTO>();
			listR1 = new ArrayList<RolVO>();
			listRol = new ArrayList<RolDTO>();			
			cantFilas = FormatterUtil.toIntPrimivite(administracionServiceLocal.getParametro(ParametroType.CANTIDAD_FILAS_DT_ADM.getValue()));
			
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	
	/* FACES MENSAJES*/
	/**
	 * Mensaje error campo obligatorio.
	 *
	 */
	public  void mensajeErrorCampoObligatorio() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ERROR_CAMPO_OBLIGATORIO, FacesMessage.SEVERITY_ERROR);
	}
	
	/**
	 * Mensaje error minimo un campo.
	 *
	 */
	public  void mensajeErrorMinimoUnCampo() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ERROR_MINIMO_UN_CAMPO_OBLIGATORIO, FacesMessage.SEVERITY_ERROR);
    }
	
	/**
	 * Mensaje asignacion lista vacia rol.
	 *
	 */
	public  void mensajeAsignacionListaVaciaRol() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_ASIGNACION_LISTA_VACIA_ROL, FacesMessage.SEVERITY_ERROR);
	}

	
	/**
	 * Mensaje exito guardar.
	 *
	 */
	public  void mensajeExitoGuardar() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_EXITO_GUARDAR, FacesMessage.SEVERITY_INFO);	
	}
	
	/**
	 * Mensaje exito modificar.
	 *
	 */
	public void mensajeExitoModificar( ) {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_EXITO_MODIFICAR, FacesMessage.SEVERITY_INFO);
	}
	
	/**
	 * Mensaje error nombre ya registrado.
	 *
	 */
	public  void mensajeErrorNombreYaRegistrado() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_NOMBRE_YA_REGISTRADO, FacesMessage.SEVERITY_ERROR);
	}
	/*MENSAJE DE ERROR PASANDO COMO PARAMETRO EL ConstantesUtil */
	/**
	 * Error.
	 *
	 * @param msg el msg
	 */
	public  void error(String msg) {
		controlMessageUtil.imprimirMessageClave(msg, FacesMessage.SEVERITY_ERROR);
	}
	
	/**
	 * Validate buscar.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateBuscar() {
		boolean rs = false;
		if (FormatterUtil.toString(vo.getNombre()) == null && FormatterUtil.toString(vo.getDescripcion()) == null
				&& FormatterUtil.toString(vo.getAccion()) == null && FormatterUtil.toString(vo.getEstado()) == null) {
			rs = true;
		}
		return rs;
	}

	/**
	 * Validate fecha.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateFecha() {
		boolean rs = false;
		return rs;
	}

	/**
	 * Limpiar campos.
	 *
	 * @param ev el ev
	 */
	public void limpiarCampos(ActionEvent ev) {
		limpiarCampos();
	}

	/**
	 * Limpiar campos.
	 */
	private void limpiarCampos() {
		vo = new PerfilCriteriaVO();
		limpiar();
		limpiarStyleTxt();
		size = 0;
	}

	/**
	 * Limpiar style txt.
	 */
	public void limpiarStyleTxt() {
		setStyleTxtNombre(null);
		setStyleTxtDescripcion(null);
		setStyleTxtEstado(null);
	}

	/**
	 * Limpiar.
	 */
	public void limpiar() {
		dto = new PerfilDTO();
		setMensajelistavacia("");
		this.setLista(null);
		this.setListR1(null);
                this.setEdicionExitosa(false);
	}
	
	/*Limpiar campos de Busqueda y volver al registro de Usuario*/
	/**
	 * Volver usuario registro.
	 *
	 * @return the string
	 */
	public String volverUsuarioRegistro() {
		limpiar();
		if (flagExternal.equals(CALL_CREAR_USUARIO)) {
			return "volverUsuarioRegistro";
		} else if (flagExternal.equals(CALL_MODIFICAR_USUARIO)) {
			return "volverUsuarioEdicion";			
		}
		return "";
		
	}
	

	/**
	 * Buscar perfil.
	 *
	 * @param ev el ev
	 */
	public void buscarPerfil(ActionEvent ev) {
		context.setLocale(FacesUtil.getLocale());
		if (lista != null) {
			lista.clear();
		}
		if (!validateBuscar()) {
			if (!validateFecha()) {
				size = 0;
				try {
					vo.setFechaAccionHasta(ValidacionUtil.SumarHorasCalendar(vo.getFechaAccionHasta()));
					if (idOrganismoExternal !=  null) {
						if (idOrganismoExternal == 0) {
							idOrganismoExternal = null;
						}
					}
					this.indicadorSeleccionTodo = false;		
					vo.setIdOrganismo(idOrganismoExternal);
					String estadoAnterior = vo.getAccion();
					lista = administracionServiceLocal.buscarPerfil(context, vo);
					size = lista.size();
					mensajelistavacia = controlMessageUtil.getMessage(ConstantesUtil.MENSAJE_LISTA_VACIA);
					vo.setAccion(estadoAnterior);
				} catch (Exception e) {
					controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
				}
			} else {
				mensajelistavacia = null;
				setLista(null);
				size = 0;
			}
		} else {
			mensajelistavacia = null;
			setLista(null);
			size = 0;
			vo = new PerfilCriteriaVO();
			mensajeErrorMinimoUnCampo();
		}
	}

	/**
	 * Cancelar.
	 *
	 * @param ev el ev
	 */
	public void cancelar(ActionEvent ev) {
		limpiarStyleTxt();
		limpiarCampos(ev);
	}

	/**
	 * Adds the list rol nuevo.
	 *
	 * @return the string
	 */
	public String addListRolNuevo() {
		setHidden(PERFILBEAN_LIST_ROL);
		setDireccionar(NUEVO);
		this.setVisListPerfilNuevo(true);
		this.setVisListPerfilEditar(false);
		Map<String,Object> session = FacesUtil.getSessionMap();
		session.remove(ROL_BEAN);
		return "addListRol";
	}

	/**
	 * Adds the list rol editar.
	 *
	 * @return the string
	 */
	public String addListRolEditar() {
		setHidden(PERFILBEAN_LIST_ROL);
		setDireccionar(EDITAR);
		this.setVisListPerfilNuevo(false);
		this.setVisListPerfilEditar(true);
		Map<String,Object> session = FacesUtil.getSessionMap();
		session.remove(ROL_BEAN);
		return "addListRol";
	}

	/**
	 * Validate.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validate() {
		boolean rs = false;
		if (dto.getNombre() == null) {
			setStyleTxtNombre(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtNombre("");
		}
		return rs;
	}

	/**
	 * Validate lista rol.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateListaRol() {
		boolean rs = false;
		if (listR1 == null || listR1.size() == 0) {
			rs = true;
		}
		return rs;
	}

	/**
	 * Validate editar.
	 *
	 * @return true, en caso de exito
	 */
	private boolean validateEditar() {
		boolean rs = false;
		if (dto.getNombre() == null) {
			setStyleTxtNombre(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtNombre("");
		}
		if (dto.getEstado() == null) {
			setStyleTxtEstado(INPUT_ERROR);
			rs = true;
		} else {
			setStyleTxtEstado("");
		}
		return rs;
	}

	/**
	 * Guardar nuevo perfil.
	 *
	 * @param ev el ev
	 */
	public void guardarNuevoPerfil(ActionEvent ev) {
		context.setLocale(FacesUtil.getLocale());
		if (!validate()) {
			if (!validateListaRol()) {
				limpiarStyleTxt();
				try {

					List<PerfilRolDTO> listRol = new ArrayList<PerfilRolDTO>();
					PerfilRolDTO bean = null;
					RolVO vo;
					for (int i = 0; i < listR1.size(); i++) {
						vo = (RolVO) listR1.get(i);
						bean = FactoryBean.getBean(PerfilRolDTO.class);
						bean.getPerfilRolPK().setIdRol(vo.getId());
						bean.getRol().setNombre(vo.getNombre());
						listRol.add(bean);

					}
					dto.setNombre(dto.getNombre().trim().toUpperCase());
					if (dto.getDescripcion() != null) {
						dto.setDescripcion(dto.getDescripcion().trim().toUpperCase());
					}
					dto.setFechaCreacion(FechaUtil.obtenerFechaActual());
					dto.setListaPerfilRol(listRol);

					administracionServiceLocal.crearPerfil(context, dto);				
					
					mensajeExitoGuardar();

					limpiar();

				} catch (Exception e) {
					controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
				}
			} else {
				error(ConstantesUtil.MENSAJE_ASIGNACION_LISTA_VACIA_ROL);
			}
		} else {
			mensajeErrorCampoObligatorio();
		}
	}

	/**
	 * Detalle.
	 *
	 * @param event el event
	 */
	public void detalle(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		sizeDetallePerfil = 0;
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("id");
			id = Long.parseLong(component.getValue().toString());
			dto = administracionServiceLocal.obtenerPerfil(context, id);
			sizeDetallePerfil = dto.getListaPerfilRol().size();
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}

	}
	
	/**
	 * Eliminar seleccion.
	 *
	 * @param ev el ev
	 */
	public void eliminarSeleccion(ActionEvent ev) {
		UIParameter component = (UIParameter) ev.getComponent().findComponent(
				"eliminarId");
		id = Long.parseLong(component.getValue().toString());
		RolVO bean;
		try {
			for (int i = 0; i < listR1.size(); i++) {
				bean = (RolVO) listR1.get(i);
				if (bean.getId().equals(id)) {
					if (!administracionServiceLocal.verificarRolEnUso(dto.getId(),bean.getId())) {
						listR1.remove(i);
						size = listR1.size();
					} else {
						throw new BusinessException(ConstantesUtil.ERROR_ROL_ASIGNADO_ELIMINAR, bean.getNombre(), dto.getNombre());
					}
				}
			}
		}  catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	
	/**
	 * Eliminar seleccion nuevo.
	 *
	 * @param ev el ev
	 */
	public void eliminarSeleccionNuevo(ActionEvent ev) {
		try {
			UIParameter component = (UIParameter) ev.getComponent().findComponent(
					"eliminarId");
			id = Long.parseLong(component.getValue().toString());
			RolVO bean;
			
			for (int i = 0; i < listR1.size(); i++) {
				bean = (RolVO) listR1.get(i);
				if (bean.getId().equals(id)) {
						  listR1.remove(i);
						  size = listR1.size();
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}
	/**
	 * Editar.
	 *
	 * @param event el event
	 */
	public void editar(ActionEvent event) {
		context.setLocale(FacesUtil.getLocale());
		try {
			UIParameter component = (UIParameter) event.getComponent()
					.findComponent("id");
			id = Long.parseLong(component.getValue().toString());
			dto = (PerfilDTO) administracionServiceLocal.obtenerPerfil(context, id);

			// GUardamos la configuracion anterior del perfil
			listaPerfilRolDTOInicial = dto.getListaPerfilRol();
			
			PerfilRolDTO bean = null;
			listR1 = new ArrayList<RolVO>();
			for (int i = 0; i < dto.getListaPerfilRol().size(); i++) {
				bean = (PerfilRolDTO) dto.getListaPerfilRol().get(i);
				RolVO rol = new RolVO();

				rol.setId(bean.getRol().getId());
				rol.setNombre(bean.getRol().getNombre());
				rol.setDescripcion(bean.getRol().getDescripcion());
				rol.setModulo(bean.getRol().getModulo().getNombre());
				rol.setEstado(bean.getEstado());
				rol.setEsadm(bean.getRol().getIndicadorAdministrador());

				listR1.add(rol);

			}
			size = listR1.size();
			setNombreAnterior(dto.getNombre());
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}

	}

	/**
	 * Guardar editar perfil.
	 *
	 * @param ev el ev
	 */
	public void guardarEditarPerfil(ActionEvent ev) {
		context.setLocale(FacesUtil.getLocale());
		if (!validateEditar()) {
			if (!validateListaRol()) {
				limpiarStyleTxt();
				
				if (!validacionPerfilActivo()) {
				//if (true) {
				try {
					List<PerfilRolDTO> listRol = new ArrayList<PerfilRolDTO>();
					PerfilRolDTO bean = null;
					RolVO vo;
					for (int i = 0; i < listR1.size(); i++) {
						vo = (RolVO) listR1.get(i);
						bean = FactoryBean.getBean(PerfilRolDTO.class);
						bean.getPerfilRolPK().setIdPerfil(dto.getId());
						bean.getPerfilRolPK().setIdRol(vo.getId());
						bean.getRol().setNombre(vo.getNombre());
						bean.getRol().setId(vo.getId());
						listRol.add(bean);

					}
					dto.setNombre(dto.getNombre().trim().toUpperCase());
					if (dto.getDescripcion() != null) {
						dto.setDescripcion(dto.getDescripcion().trim().toUpperCase());
					}
					dto.setListaPerfilRol(listRol);
					administracionServiceLocal.modificarPerfil(context, dto);
					listaPerfilRolDTOInicial = administracionServiceLocal.obtenerPerfil(context, dto.getId()).getListaPerfilRol();

					mensajeExitoModificar();

					buscarPerfil(ev);
                                        
                                        edicionExitosa = true;
                                        
				} catch (BusinessException e1) {
					error = getErrorService().getErrorFor(e1,
							FacesUtil.getLocale());
					FacesMessageUtil.addMessage(FacesContext
							.getCurrentInstance(), FacesMessage.SEVERITY_ERROR,
							error.getDefaultMessage());										
				} catch (Exception e) {
					controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
					
				}
				} else {
						mensajePerfilActivo(); 
					
				}
			} else {
				mensajeAsignacionListaVaciaRol();
				
			}
		} else {
			mensajeErrorCampoObligatorio();
		}
	}

	/**
	 * Agregar.
	 *
	 * @return the string
	 */
	public String agregar() {
		String path = "";
		try {
			RolBean rol = (RolBean) FacesUtil.getBean(ROL_BEAN);
			RolVO bean;
			int conta = 0;
			if (listR1 != null && !listR1.isEmpty()) {
				listR2 = new ArrayList<RolVO>();
				listR2 = getDiferences(listR1, rol.getListaR());
				for (int i = 0; i < listR2.size(); i++) {
					bean = (RolVO) listR2.get(i);
					if (bean.isHidden()) {
						listR1.add(bean);
						conta++;
					}
				}
			} else {
				listR1 = new ArrayList<RolVO>();
				for (int i = 0; i < rol.getListaR().size(); i++) {
					bean = (RolVO) rol.getListaR().get(i);
					if (bean.isHidden()) {
						listR1.add(bean);
						conta++;
					}
				}
			}
			this.size = listR1.size();
			
			if (conta > 0) {
				if (isVisListPerfilNuevo()) {
					path = "regresarNuevo";
				}
				if (isVisListPerfilEditar()) {
					path = "regresarEditar";
				}
			} else {
				controlMessageUtil.imprimirMessage(ConstantesUtil.MENSAJE_ROLES_YA_AGREGADOS, FacesMessage.SEVERITY_ERROR);
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return path;
	}

	/**
	 * Nuevo.
	 *
	 * @return the string
	 */
	public String nuevo() {
		limpiar();
		limpiarStyleTxt();
		return "nuevoPerfil";
	}

	/**
	 * Obtiene diferences.
	 *
	 * @param <T> el tipo generico
	 * @param lst1 el lst1
	 * @param lst2 el lst2
	 * @return diferences
	 */
	public static <T> List<T> getDiferences(List<T> lst1, List<T> lst2) {
		List<T> lst3 = new ArrayList<T>(lst1);
		lst3.retainAll(lst2);
		List<T> lst4 = new ArrayList<T>(lst2);
		lst4.removeAll(lst3);
		return lst4;
	}

	/**
	 * Establece el ear perfil.
	 *
	 * @param event el new ear perfil
	 */
	public void setearPerfil(ActionEvent event) {
		try {
			UIParameter component0 = (UIParameter) event.getComponent()
					.findComponent("flagExternalPerfil");
			if (component0.getValue() != null) {
				flagExternal = component0.getValue().toString();
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Establece el ear flag external.
	 *
	 * @param event el new ear flag external
	 */
	public void setearFlagExternal(ActionEvent event) {
		// Se obtiene el Flag
		try {
			UIParameter component = (UIParameter) event.getComponent().findComponent("flagExternalPerfil");
			if (component.getValue() != null) {
				flagExternal = (component.getValue().toString());
			}
	
			if (flagExternal.equals(CALL_CREAR_ENTIDAD_TO_BUSCAR_PEFIL)) {
				verCheck = true;
				lista = null;
				limpiarCampos(event);
			}
	
			if (flagExternal.equals(CALL_CREAR_USUARIO) || flagExternal.equals(CALL_MODIFICAR_USUARIO)) {
				component = (UIParameter) event.getComponent().findComponent("flagExternalOrganismo");
				if (component.getValue() != null) {
					idOrganismoExternal = (Long) component.getValue();
					limpiarCampos();
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}

	}
	
	/**
	 * Regresar buscar.
	 *
	 * @return the string
	 */
	public String regresarBuscar() {
		String retorno = StringUtils.EMPTY;
		if (this.flagExternal.equalsIgnoreCase(CALL_CREAR_USUARIO)) { //"CU"
			retorno = "volverUsuarioRegistro";
		}
		if (this.flagExternal
				.equalsIgnoreCase(CALL_CREAR_ENTIDAD_TO_BUSCAR_PEFIL)) {
			retorno = "volverEntidadPerfil";
		}
		return retorno;
	}

	/**
	 * Cancelar list rol.
	 *
	 * @param ev el ev
	 */
	public void cancelarListRol(ActionEvent ev) {

	}

	/**
	 * Ver seleccion multiple.
	 *
	 * @param event el event
	 */
	public void verSeleccionMultiple(ActionEvent event) {
		try {
			//PortletRequest request = PortalHelper.getPortletRequest();
			//PortletSession session = request.getPortletSession();	
			PerfilBean  perfil = (PerfilBean) FacesUtil.getBean("perfilBean");limpiar();
			perfil.limpiar() ;	
			perfil.size = 0;
			setVerCheck(true);
			this.indicadorSeleccionTodo = false;
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * Limpiar fecha inicio.
	 *
	 * @param ev el ev
	 */
	public void limpiarFechaInicio(ActionEvent ev) {
		this.vo.setFechaAccionDesde(null);
	}

	/**
	 * Limpiar fecha fin.
	 *
	 * @param ev el ev
	 */
	public void limpiarFechaFin(ActionEvent ev) {
		this.vo.setFechaAccionHasta(null);
	}

	/**
	 * Eliminar.
	 *
	 * @param ev el ev
	 */
	public void eliminar(ActionEvent ev) {
		context.setLocale(FacesUtil.getLocale());
		try {
			UIParameter component = (UIParameter) ev.getComponent()
					.findComponent("id");
			id = Long.parseLong(component.getValue().toString());
			dto.setId(id);
			administracionServiceLocal.eliminarPerfil(context, dto);
			controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_EXITO_ELIMINAR, FacesMessage.SEVERITY_INFO);
			
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}

	}
	/**
	 * Validacion perfil activo.
	 *
	 * @return true, en caso de exito
	 */
	public boolean validacionPerfilActivo() {
		if (dto.getEstado().equalsIgnoreCase(EstadoState.INACTIVO.getKey())) {
			return administracionServiceLocal.validarPerfilActivo(dto);
		} else {
			return false;
		}
		
	}
	
	/**
	 * Mensaje perfil activo.
	 *
	 */
	public  void mensajePerfilActivo() {
		controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJA_PERFIL_ASOCIADO_ENTIDAD_ACTIVO, FacesMessage.SEVERITY_ERROR);
	}
	/**
	 * Seleccionar todo.
	 *
	 * @param event el event
	 */
	public void seleccionarTodo(ValueChangeEvent event) {
		try {
			if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
				event.setPhaseId(PhaseId.INVOKE_APPLICATION);
				event.queue();
				return;
				}
				if (event != null) {					
					this.pageActual = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
					.get("frmRol:valorPageCount");
					int pagina = 0;
					
					if (this.pageActual == null) {
						pagina = 1;
					} else {
						pagina = Integer.valueOf(this.pageActual);
					}
					
					indicadorSeleccionTodo = (Boolean) event.getNewValue();
					
					for (int i = 0; i < 10; i++) {
						if (((pagina - 1) * 10 + i) < lista.size()) {
						PerfilDTO prfDto = lista.get(((pagina - 1) * 10) + i);
						prfDto.setHidden((Boolean) event.getNewValue());
	
						lista.set(((pagina - 1) * 10) + i, prfDto);
						}
	
				   }
				
		     }
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}		
	}
	
	/**
	 * Limpiar seleccion todo.
	 *
	 * @param ev el ev
	 */
	public void limpiarSeleccionTodo(ActionEvent ev) {
		this.setIndicadorSeleccionTodo(false);
	}
	
	//get y set
	/**
	 * Obtiene style txt finicio.
	 *
	 * @return style txt finicio
	 */
	public String getStyleTxtFinicio() {
		return styleTxtFinicio;
	}

	/**
	 * Establece el style txt finicio.
	 *
	 * @param styleTxtFinicio el new style txt finicio
	 */
	public void setStyleTxtFinicio(String styleTxtFinicio) {
		this.styleTxtFinicio = styleTxtFinicio;
	}

	/**
	 * Obtiene style txt ffin.
	 *
	 * @return style txt ffin
	 */
	public String getStyleTxtFfin() {
		return styleTxtFfin;
	}

	/**
	 * Establece el style txt ffin.
	 *
	 * @param styleTxtFfin el new style txt ffin
	 */
	public void setStyleTxtFfin(String styleTxtFfin) {
		this.styleTxtFfin = styleTxtFfin;
	}

	/**
	 * Comprueba si es vis list perfil nuevo.
	 *
	 * @return true, si es vis list perfil nuevo
	 */
	public boolean isVisListPerfilNuevo() {
		return visListPerfilNuevo;
	}

	/**
	 * Establece el vis list perfil nuevo.
	 *
	 * @param visListPerfilNuevo el new vis list perfil nuevo
	 */
	public void setVisListPerfilNuevo(boolean visListPerfilNuevo) {
		this.visListPerfilNuevo = visListPerfilNuevo;
	}

	/**
	 * Comprueba si es vis list perfil editar.
	 *
	 * @return true, si es vis list perfil editar
	 */
	public boolean isVisListPerfilEditar() {
		return visListPerfilEditar;
	}

	/**
	 * Establece el vis list perfil editar.
	 *
	 * @param visListPerfilEditar el new vis list perfil editar
	 */
	public void setVisListPerfilEditar(boolean visListPerfilEditar) {
		this.visListPerfilEditar = visListPerfilEditar;
	}

	/**
	 * Comprueba si es ver check.
	 *
	 * @return true, si es ver check
	 */
	public boolean isVerCheck() {
		return verCheck;
	}

	/**
	 * Establece el ver check.
	 *
	 * @param verCheck el new ver check
	 */
	public void setVerCheck(boolean verCheck) {
		this.verCheck = verCheck;
	}

	/**
	 * Obtiene list rol.
	 *
	 * @return list rol
	 */
	public List<RolDTO> getListRol() {
		return listRol;
	}

	/**
	 * Establece el list rol.
	 *
	 * @param listRol el new list rol
	 */
	public void setListRol(List<RolDTO> listRol) {
		this.listRol = listRol;
	}

	/**
	 * Obtiene id.
	 *
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el id.
	 *
	 * @param id el new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Obtiene direccionar.
	 *
	 * @return direccionar
	 */
	public String getDireccionar() {
		return direccionar;
	}

	/**
	 * Establece el direccionar.
	 *
	 * @param direccionar el new direccionar
	 */
	public void setDireccionar(String direccionar) {
		this.direccionar = direccionar;
	}

	/**
	 * Obtiene hidden.
	 *
	 * @return hidden
	 */
	public String getHidden() {
		return hidden;
	}

	/**
	 * Establece el hidden.
	 *
	 * @param hidden el new hidden
	 */
	public void setHidden(String hidden) {
		this.hidden = hidden;
	}

	/**
	 * Obtiene size.
	 *
	 * @return size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Establece el size.
	 *
	 * @param size el new size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Obtiene error.
	 *
	 * @return error
	 */
	public ErrorVO getError() {
		return error;
	}

	/**
	 * Establece el error.
	 *
	 * @param error el new error
	 */
	public void setError(ErrorVO error) {
		this.error = error;
	}

	/**
	 * Obtiene lista.
	 *
	 * @return lista
	 */
	public List<PerfilDTO> getLista() {
		return lista;
	}

	/**
	 * Establece el lista.
	 *
	 * @param lista el new lista
	 */
	public void setLista(List<PerfilDTO> lista) {
		this.lista = lista;
	}

	/**
	 * Obtiene vo.
	 *
	 * @return vo
	 */
	public PerfilCriteriaVO getVo() {
		return vo;
	}

	/**
	 * Establece el vo.
	 *
	 * @param vo el new vo
	 */
	public void setVo(PerfilCriteriaVO vo) {
		this.vo = vo;
	}

	/**
	 * Obtiene time zone.
	 *
	 * @return time zone
	 */
	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	/**
	 * Obtiene style txt nombre.
	 *
	 * @return style txt nombre
	 */
	public String getStyleTxtNombre() {
		return styleTxtNombre;
	}

	/**
	 * Obtiene mensajelistavacia.
	 *
	 * @return mensajelistavacia
	 */
	public String getMensajelistavacia() {
		return mensajelistavacia;
	}

	/**
	 * Establece el mensajelistavacia.
	 *
	 * @param mensajelistavacia el new mensajelistavacia
	 */
	public void setMensajelistavacia(String mensajelistavacia) {
		this.mensajelistavacia = mensajelistavacia;
	}

	/**
	 * Establece el style txt nombre.
	 *
	 * @param styleTxtNombre el new style txt nombre
	 */
	public void setStyleTxtNombre(String styleTxtNombre) {
		this.styleTxtNombre = styleTxtNombre;
	}

	/**
	 * Obtiene style txt accion.
	 *
	 * @return style txt accion
	 */
	public String getStyleTxtAccion() {
		return styleTxtAccion;
	}

	/**
	 * Establece el style txt accion.
	 *
	 * @param styleTxtAccion el new style txt accion
	 */
	public void setStyleTxtAccion(String styleTxtAccion) {
		this.styleTxtAccion = styleTxtAccion;
	}

	/**
	 * Obtiene style txt descripcion.
	 *
	 * @return style txt descripcion
	 */
	public String getStyleTxtDescripcion() {
		return styleTxtDescripcion;
	}

	/**
	 * Establece el style txt descripcion.
	 *
	 * @param styleTxtDescripcion el new style txt descripcion
	 */
	public void setStyleTxtDescripcion(String styleTxtDescripcion) {
		this.styleTxtDescripcion = styleTxtDescripcion;
	}

	/**
	 * Obtiene style txt estado.
	 *
	 * @return style txt estado
	 */
	public String getStyleTxtEstado() {
		return styleTxtEstado;
	}

	/**
	 * Establece el style txt estado.
	 *
	 * @param styleTxtEstado el new style txt estado
	 */
	public void setStyleTxtEstado(String styleTxtEstado) {
		this.styleTxtEstado = styleTxtEstado;
	}

	/**
	 * Obtiene id organismo external.
	 *
	 * @return id organismo external
	 */
	public Long getIdOrganismoExternal() {
		return idOrganismoExternal;
	}

	/**
	 * Establece el id organismo external.
	 *
	 * @param idOrganismoExternal el new id organismo external
	 */
	public void setIdOrganismoExternal(Long idOrganismoExternal) {
		this.idOrganismoExternal = idOrganismoExternal;
	}

	/**
	 * Obtiene dto.
	 *
	 * @return dto
	 */
	public PerfilDTO getDto() {
		return dto;
	}

	/**
	 * Establece el dto.
	 *
	 * @param dto el new dto
	 */
	public void setDto(PerfilDTO dto) {
		this.dto = dto;
	}
	
	/**
	 * Obtiene acciones.
	 *
	 * @return acciones
	 */
	public List<SelectItem> getAcciones() {
		try {
			if (!languajeAcciones.equalsIgnoreCase(FacesUtil.getLocale().getLanguage()))  {
				languajeAcciones = FacesUtil.getLocale().getLanguage();
				acciones = new ArrayList<SelectItem>();
				List<SelectVO> lista = FechaAccionType.getList(FacesUtil.getLocale());
				if (lista == null) {
					try {
						lista = FechaAccionType.getList(null);
					} catch (Exception e) {
						controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
					}
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						acciones.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return acciones;
	}

	/**
	 * Obtiene flag external.
	 *
	 * @return flag external
	 */
	public String getFlagExternal() {
		return flagExternal;
	}

	/**
	 * Establece el flag external.
	 *
	 * @param flagExternal el new flag external
	 */
	public void setFlagExternal(String flagExternal) {
		this.flagExternal = flagExternal;
	}

	/**
	 * Establece el acciones.
	 *
	 * @param acciones el new acciones
	 */
	public void setAcciones(List<SelectItem> acciones) {
		this.acciones = acciones;
	}
	
	/**
	 * Obtiene estados.
	 *
	 * @return estados
	 */
	public List<SelectItem> getEstados() {
		try {
			if (!lenguajeEstados.equalsIgnoreCase(FacesUtil.getLocale().getLanguage()))  {
				lenguajeEstados = FacesUtil.getLocale().getLanguage();
				estados = new ArrayList<SelectItem>();
				List<SelectVO> lista = EstadoState.getList(FacesUtil.getLocale());
				if (lista == null) {
					try {
						lista = EstadoState.getList(null);
					} catch (Exception e) {
						controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
					}
				}
				if (lista != null) {
					for (SelectVO vo : lista) {
						estados.add(new SelectItem(vo.getId(), vo.getValue()));
					}
				}
			}
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return estados;
	}

	/**
	 * Establece el estados.
	 *
	 * @param estados el new estados
	 */
	public void setEstados(List<SelectItem> estados) {
		this.estados = estados;
	}

	/**
	 * Obtiene size detalle perfil.
	 *
	 * @return size detalle perfil
	 */
	public int getSizeDetallePerfil() {
		return sizeDetallePerfil;
	}

	/**
	 * Establece el size detalle perfil.
	 *
	 * @param sizeDetallePerfil el new size detalle perfil
	 */
	public void setSizeDetallePerfil(int sizeDetallePerfil) {
		this.sizeDetallePerfil = sizeDetallePerfil;
	}

	/**
	 * Obtiene list r1.
	 *
	 * @return list r1
	 */
	public List<RolVO> getListR1() {
		return listR1;
	}

	/**
	 * Establece el list r1.
	 *
	 * @param listR1 el new list r1
	 */
	public void setListR1(List<RolVO> listR1) {
		this.listR1 = listR1;
	}

	/**
	 * Obtiene list r2.
	 *
	 * @return list r2
	 */
	public List<RolVO> getListR2() {
		return listR2;
	}

	/**
	 * Establece el list r2.
	 *
	 * @param listR2 el new list r2
	 */
	public void setListR2(List<RolVO> listR2) {
		this.listR2 = listR2;
	}

	/**
	 * Obtiene lista perfil rol dto inicial.
	 *
	 * @return lista perfil rol dto inicial
	 */
	public List<PerfilRolDTO> getListaPerfilRolDTOInicial() {
		return listaPerfilRolDTOInicial;
	}

	/**
	 * Establece el lista perfil rol dto inicial.
	 *
	 * @param listaPerfilRolDTOInicial el new lista perfil rol dto inicial
	 */
	public void setListaPerfilRolDTOInicial(
			List<PerfilRolDTO> listaPerfilRolDTOInicial) {
		this.listaPerfilRolDTOInicial = listaPerfilRolDTOInicial;
	}
	
	/**
	 * Obtiene nombre anterior.
	 *
	 * @return nombre anterior
	 */
	public String getNombreAnterior() {
		return nombreAnterior;
	}

	/**
	 * Establece el nombre anterior.
	 *
	 * @param nombreAnterior el new nombre anterior
	 */
	public void setNombreAnterior(String nombreAnterior) {
		this.nombreAnterior = nombreAnterior;
	}
	
	/**
	 * Obtiene estado activo.
	 *
	 * @return estado activo
	 */
	public SelectItem getEstadoActivo() {
		return new SelectItem(EstadoState.ACTIVO.getKey(), EstadoState.ACTIVO.getDescription(FacesUtil.getLocale()));
	}

	/**
	 * Comprueba si es indicador seleccion todo.
	 *
	 * @return true, si es indicador seleccion todo
	 */
	public boolean isIndicadorSeleccionTodo() {
		return indicadorSeleccionTodo;
	}

	/**
	 * Establece el indicador seleccion todo.
	 *
	 * @param indicadorSeleccionTodo el new indicador seleccion todo
	 */
	public void setIndicadorSeleccionTodo(boolean indicadorSeleccionTodo) {
		this.indicadorSeleccionTodo = indicadorSeleccionTodo;
	}
	/**
	 * Obtiene estado filtrado.
	 *
	 * @return estado filtrado
	 */
	public List<SelectItem> getEstadoFiltrado() {
		try {
			if (!lenguajeEstadoFiltrado.equalsIgnoreCase(FacesUtil.getLocale().getLanguage())) 
			{
				lenguajeEstadoFiltrado = FacesUtil.getLocale().getLanguage();
				estadoFiltrado = new ArrayList<SelectItem>();
				List<SelectVO> lista = EstadoState.getList(FacesUtil.getLocale());
				if (lista.size() != 0) {
				estadoFiltrado.add(new SelectItem(lista.get(0).getId(), lista.get(0).getValue()));
				}
			}	
		} catch (Exception e) {
			controlMessageUtil.imprimirMessage(e, FacesMessage.SEVERITY_ERROR);
		}
		return estadoFiltrado;
	}

	/**
	 * Establece el estado filtrado.
	 *
	 * @param estadoFiltrado el new estado filtrado
	 */
	public void setEstadoFiltrado(List<SelectItem> estadoFiltrado) {
		this.estadoFiltrado = estadoFiltrado;
	}

	/**
	 * Obtiene page actual.
	 *
	 * @return page actual
	 */
	public String getPageActual() {
		return pageActual;
	}

	/**
	 * Establece el page actual.
	 *
	 * @param pageActual el new page actual
	 */
	public void setPageActual(String pageActual) {
		this.pageActual = pageActual;
	}

	/**
	 * Obtiene cant filas.
	 *
	 * @return cant filas
	 */
	public int getCantFilas() {
		return cantFilas;
	}

	/**
	 * Establece el cant filas.
	 *
	 * @param cantFilas el new cant filas
	 */
	public void setCantFilas(int cantFilas) {
		this.cantFilas = cantFilas;
	}

        public boolean isEdicionExitosa() {
            return edicionExitosa;
        }

        public void setEdicionExitosa(boolean edicionExitosa) {
            this.edicionExitosa = edicionExitosa;
        }
        
        
}