package pe.gob.minedu.escale.ctx;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;




@ManagedBean(name = "sistemaContextBean")
@ApplicationScoped
public class SistemaContextBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String VISTA_LIENZO_GENERAL = "verLienzoGeneral";
	public static final String VISTA_LOGIN = "verBandejaLogin";
        public static final String VISTA_GEUSU = "verGestionUsuario";
        public static final String VISTA_REGUSU = "verRegistroUsuario";
        public static final String VISTA_GEPERF = "verGestionPerfil";
        public static final String VISTA_GEROL = "verGestionRol";
        public static final String VISTA_GERENT = "verGestionEntidad";
        
        
        
        
	
	public static final String VISTA_CAMBIAR_CLAVE = "verCambiarClave";
	

	public static final String PAGINA_RETORNAR = "return";

	
	public static final String PAGINA_INICIO = "/inicio.xhtml";
	public static final String PAGINA_LOGIN = "/login.xhtml";
	public static final String PAGINA_JAVAX = "/javax.faces.resource";
        public static final String PAGINA_RECUPERA_CLAVE = "/paginas/recuperarClaveUsuario/recuperarClaveUsuario.xhtml";
        
	
	

	public static final String[] urlPermitidaSinSesion = new String[] {
			"/inicio.xhtml", "/login.xhtml", "/javax.faces.resource","/recuperarClaveUsuario.xhtml" };


	
	
	/**
	 * @return the vistaLienzoGeneral
	 */
	public static String getVistaLienzoGeneral() {
		return VISTA_LIENZO_GENERAL;
	}

	
	
	
	

	/**
	 * @return the vistaLogin
	 */
	public static String getVistaLogin() {
		return VISTA_LOGIN;
	}

	/**
	 * @return the paginaInicio
	 */
	public static String getPaginaInicio() {
		return PAGINA_INICIO;
	}

	/**
	 * @return the paginaLogin
	 */
	public static String getPaginaLogin() {
		return PAGINA_LOGIN;
	}

	/**
	 * @return the paginaJavax
	 */
	public static String getPaginaJavax() {
		return PAGINA_JAVAX;
	}

	/**
	 * @return the urlpermitidasinsesion
	 */
	public static String[] getUrlpermitidasinsesion() {
		return urlPermitidaSinSesion;
	}

	
	
	
	public static String vistaCambiarClave(){
		return VISTA_CAMBIAR_CLAVE;
	}
	
	

	

	public SistemaContextBean() {

	}

	
	/**
	 * Metodo que permite obtener el valor del objeto VISTA_CAMBIAR_CLAVE
	 * @return VISTA_CAMBIAR_CLAVE, tipo String
	 */
	public static String getVistaCambiarClave() {
		return VISTA_CAMBIAR_CLAVE;
	}

	

	/**
	 * Metodo que permite obtener el valor del objeto PAGINA_RETORNAR
	 * @return PAGINA_RETORNAR, tipo String
	 */
	public static String paginaRetornar() {
		return PAGINA_RETORNAR;
	}

	
	
        
        
        
        /**
	 * @return the vistaGeusu
	 */
	public String vistaGeusu() throws Exception {
		return VISTA_GEUSU;
	}
        
        /**
	 * @return the vistaRegusu
	 */
	public String vistaRegusu() {
		return VISTA_REGUSU;
	}

        public String vistaGeperf() {
            return VISTA_GEPERF;
        }
        
        public String vistaGerol() {
            return VISTA_GEROL;
        }
        
        public String vistaGeent() {
            return VISTA_GERENT;
        }

}	
