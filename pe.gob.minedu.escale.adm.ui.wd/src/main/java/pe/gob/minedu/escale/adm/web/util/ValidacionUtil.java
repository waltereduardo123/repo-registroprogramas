package pe.gob.minedu.escale.adm.web.util;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import pe.gob.minedu.escale.common.util.LogUtil;

public class ValidacionUtil {

	/** La Constante SEGUNDO. */
	private static final int SEGUNDO = 59;

	/** La Constante MINUTO. */
	private static final int MINUTO = 59;

	/** La Constante HORA. */
	private static final int HORA = 23;

	/** La Constante MILISECOND. */
	private static final int MILISEGUNDO = 999;

	// private static final String EXTENSION_JPG = "jpg";

	/** El objeto faces context. */
	private static Object facesContext;

	/** El log. */
	private static LogUtil log = new LogUtil(ValidacionUtil.class.getName());

	/** El log. */
	// private static LogUtil log = new
	// LogUtil(DataExportExcel.class.getName());
	//
	/**
	 * Borrar archivo.
	 *
	 * @param name
	 *            el name
	 */
	public static void borrarArchivo(String name) {
		File fil = new File(getRealPath() + name);
		if (!fil.delete()) {
			log.info("Archivo temporal no eliminado: " + name);
		}
	}

	/**
	 * validamos los campos obligatorios Check empty or blanck.
	 *
	 * @param values
	 *            el values
	 * @return true, en caso de exito
	 */
	public static boolean checkEmptyOrBlanck(String[] values) {
		boolean mensaje = false;
		for (int i = 0; i < values.length; i++) {
			if (StringUtils.isBlank(values[i]) || StringUtils.isEmpty(values[i])) {
				mensaje = true;
				break;
			}
		}
		return mensaje;
	}

	/**
	 * validamos que como minimo ingrese un parametro de busqueda Check only one
	 * parameter.
	 *
	 * @param values
	 *            el values
	 * @return true, en caso de exito
	 */
	public static boolean checkOnlyOneParameter(String[] values) {
		boolean mensaje = true;
		for (int i = 0; i < values.length; i++) {
			if (!StringUtils.isBlank(values[i]) && !StringUtils.isEmpty(values[i])
					&& !values[i].equalsIgnoreCase("null")) {
				mensaje = false;
				break;
			}
		}
		return mensaje;
	}

	/**
	 * Crear imagen.
	 *
	 * @param currentFile
	 *            el current file
	 * @param ruta
	 *            el ruta
	 * @return the string
	 */
	// public static String crearImagen(FileInfo currentFile, String ruta) {
	// try {
	// final BufferedImage bufferedImage = ImageIO
	// .read(new ByteArrayInputStream(FileHelper
	// .getBytesFromFile(currentFile.getFile())));
	//
	// String nombreArchivo = currentFile.getFileName();
	// String ext = nombreArchivo.substring(
	// nombreArchivo.indexOf(".") + 1, nombreArchivo.length());
	// String namecanonico = ruta + currentFile.getFileName();
	// File file = new File(namecanonico);
	// ImageIO.write(bufferedImage, ext, file);
	//
	// } catch (Exception e) {
	// log.error(e);
	// }
	// return "";
	// }

	/**
	 * Obtener imagen ruta fisica.
	 *
	 * @param ruta
	 *            el ruta
	 * @return the byte[]
	 */
	// public static byte[] obtenerImagenRutaFisica(String ruta) {
	// ByteArrayOutputStream baos = null;
	// byte[] bytes = null;
	// FileInputStream fis = null;
	// try {
	// ImagenVo data = null;
	// File file = new File(ruta);
	// if (file.exists()) {
	// fis = new FileInputStream(file);
	// byte[] buf = new byte[1024];
	// baos = new ByteArrayOutputStream();
	// for (int readNum; (readNum = fis.read(buf)) != -1;) {
	// baos.write(buf, 0, readNum);
	// }
	// bytes = baos.toByteArray();
	// }
	// } catch (IOException ex) {
	// log.error(ex);
	// }
	// return bytes;
	// }

	/**
	 * Creates the image.
	 *
	 * @return the string[]
	 */
	// public static String[] createImage() {
	// String answer = null;
	// Random trnd = new Random();
	// nl.captcha.Captcha captcha = new nl.captcha.Captcha.Builder(150, 40)
	// .addBackground(new GradiatedBackgroundProducer()).addText(
	// new DefaultTextProducer(6)).build();
	//
	// BufferedImage captchaImage;
	// captchaImage = captcha.getImage();
	// answer = captcha.getAnswer();
	//
	// String ext = EXTENSION_JPG;
	// int NombreTmp = trnd.nextInt(10000);
	// String namecanonico = "images/imageTemp/" + NombreTmp + "." + ext;
	// File file = new File(getRealPath() + namecanonico);
	// try {
	// ImageIO.write(captchaImage, ext, file);
	// } catch (IOException ioe) {
	// log.error(ioe);
	// }
	//
	// String[] data = { answer, "/" + namecanonico };
	// return data;
	// }

	/**
	 * Creates the image byte.
	 *
	 * @return the imagen vo
	 */
	// public static ImagenVo createImageByte() {
	// String answer = null;
	// ImagenVo data = null;
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// byte[] bytesOut = null;
	// Random trnd = new Random();
	// nl.captcha.Captcha captcha = new nl.captcha.Captcha.Builder(150, 40)
	// .addBackground(new GradiatedBackgroundProducer()).addText(
	// new DefaultTextProducer(6)).gimp().build();
	//
	// BufferedImage captchaImage;
	// captchaImage = captcha.getImage();
	// answer = captcha.getAnswer();
	// String ext = EXTENSION_JPG;
	// try {
	// baos = new ByteArrayOutputStream();
	// data = new ImagenVo();
	// ImageIO.write(captchaImage, ext, baos);
	// data.setNombreImagenByte(baos.toByteArray());
	// data.setRespuesta(answer);
	// } catch (IOException ioe) {
	// log.error(ioe);
	// }
	//
	// return data;
	// }

	/**
	 * Obtiene real path.
	 *
	 * @return real path
	 */
	public static String getRealPath() {

		Object ctx = facesContext;
		String path = "/";
		if (ctx instanceof ServletContext) {
			path = ((ServletContext) ctx).getRealPath("/");
		}
		// else {
		// path = ((PortletContext) ctx).getRealPath("/");
		// }
		return path;
	}

	/**
	 * Establece el faces context.
	 *
	 * @param ctx
	 *            el new faces context
	 */
	public static void setFacesContext(Object ctx) {
		facesContext = ctx;
	}

	/**
	 * Sumar horas calendar.
	 *
	 * @param date
	 *            el date
	 * @return the date
	 */
	public static Date SumarHorasCalendar(Date date) {
		if (date == null) {
			return null;
		} else {
			Calendar cale = Calendar.getInstance();
			cale.setTime(date);
			cale.add(Calendar.HOUR, HORA);
			cale.add(Calendar.MINUTE, MINUTO);
			cale.add(Calendar.SECOND, SEGUNDO);
			cale.add(Calendar.MILLISECOND, MILISEGUNDO);
			return cale.getTime();
		}
	}

	/**
	 * Validacion imagen.
	 *
	 * @param currentFile
	 *            el current file
	 * @return the string
	 */
	// public static String validacionImagen(final FileInfo currentFile) {
	// String resultado = "";
	// try {
	// final BufferedImage bufferedImage = ImageIO
	// .read(new ByteArrayInputStream(FileHelper
	// .getBytesFromFile(currentFile.getFile())));
	// if (bufferedImage.getWidth() > 100
	// || bufferedImage.getHeight() > 100) {
	// resultado = "E";
	// }
	// } catch (Exception e) {
	// log.error(e);
	// }
	// return resultado;
	// }

	/**
	 * Validar dni.
	 *
	 * @param dni
	 *            el dni
	 * @return true, en caso de exito
	 */
	public static boolean validarDNI(String dni) {
		boolean error = false;
		if (dni != null) {
			if (dni.length() != ConstantesUtil.DNI_SIZE) {
				error = true;
			}
		}
		return error;
	}

	/**
	 * Verificar numerico.
	 *
	 * @param in
	 *            el in
	 * @return true, en caso de exito
	 */
	/**
	 * @param in
	 * @return
	 */
	public static boolean verificarNumerico(String in) {

		try {
			Integer.parseInt(in);
		} catch (NumberFormatException ex) {
			log.warn(ex);
			return false;
		}
		return true;
	}
}
