
package pe.gob.minedu.escale.common.web.session.filter;

import static pe.gob.minedu.escale.common.web.util.ConstantesUtil.SERVICE_CONTEXT;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pe.gob.minedu.escale.adm.vo.UsuarioSessionVO;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.ctx.SistemaContextBean;

/**
 *
 * @author IMENDOZA
 */
/**
 * Servlet Filter implementation class SessionUrlFilter
 */
@WebFilter(description = "Clase para filtrar las url segun sesion.", urlPatterns = { "*.xhtml" })
public class SessionUrlFilter implements Filter {
	FilterConfig filterConfig;

        /** El contexto. */
        private ServiceContext context;
        
	/**
	 * Default constructor.
	 */
	public SessionUrlFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
        @Override
	public void destroy() {
		this.filterConfig = null;
	}

	/**
     * @param request
     * @param response
     * @param chain
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
        @Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(true);
//                ServiceContext contexto = (ServiceContext)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SeguridadServiceRemote.SESSION_VAR);
//                UsuarioSessionVO usuarioContexto = contexto.getUsuarioSessionVO();
		String requestUrl = req.getRequestURL().toString();

		String[] urlPermitidaSinSesion = new String[] {
				SistemaContextBean.PAGINA_INICIO,
				SistemaContextBean.PAGINA_LOGIN,
				SistemaContextBean.PAGINA_JAVAX,
                                SistemaContextBean.PAGINA_RECUPERA_CLAVE };

		boolean redireccionarPeticion = false;
//		if (session.getAttribute("usuarioSessionVO") == null) {
                context = (ServiceContext)session.getAttribute(SERVICE_CONTEXT);
                if (context == null) {
                    context = new ServiceContext();
                    session.setAttribute(SERVICE_CONTEXT, context);                    
                }
//                ServiceContext contexto = (ServiceContext)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SeguridadServiceRemote.SESSION_VAR);
                UsuarioSessionVO usuarioContexto = context.getUsuarioSessionVO();
                if (usuarioContexto == null) {
			redireccionarPeticion = true;
			for (String item : urlPermitidaSinSesion) {
				if (requestUrl.contains(item)) {
					redireccionarPeticion = false;
					break;
				}
			}
		} else {
			redireccionarPeticion = false;
		}
		if (redireccionarPeticion) {
			res.sendRedirect(req.getContextPath() + SistemaContextBean.PAGINA_LOGIN);
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
     * @param filterConfig
     * @throws javax.servlet.ServletException
	 * @see Filter#init(FilterConfig)
	 */
        @Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	
}
