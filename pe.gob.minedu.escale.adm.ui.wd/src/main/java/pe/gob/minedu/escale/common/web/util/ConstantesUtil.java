package pe.gob.minedu.escale.common.web.util;

import pe.gob.minedu.escale.common.web.jsf.util.FacesUtil;






public class ConstantesUtil {
	
	/** La Constante PORTLETSESSION_ATTR_MAXIMIZEURL. */
	public static final String PORTLETSESSION_ATTR_MAXIMIZEURL = "maxUrl";
	
	/** La Constante PORTLETSESSION_ATTR_MINIMIZEURL. */
	public static final String PORTLETSESSION_ATTR_MINIMIZEURL = "minUrl";
	
	/** La Constante PORTLETSESSION_ATTR_NORMALURL. */
	public static final String PORTLETSESSION_ATTR_NORMALURL = "normalUrl";

	/** La Constante CALL_CREAR_USUARIO. */
	public static final String CALL_CREAR_USUARIO = "CU";
	
	/** La Constante CALL_MODIFICAR_USUARIO. */
	public static final String CALL_MODIFICAR_USUARIO = "MU";
	
	/** La Constante CALL_ENTIDAD_TO_USUARIO. */
	public static final String CALL_ENTIDAD_TO_USUARIO = "EU";
	
	/** La Constante CALL_MODIFICAR_ENTIDAD_TO_USUARIO. */
	public static final String CALL_MODIFICAR_ENTIDAD_TO_USUARIO = "MEU";
	
	/** La Constante CALL_SOLICITUD_TO_USUARIO. */
	public static final String CALL_SOLICITUD_TO_USUARIO = "SU";
	
	/** La Constante CALL_USUARIO_ROL. */
	public static final String CALL_USUARIO_ROL = "UR";
	
	/** La Constante CALL_ASIGNARAC_ROL. */
	public static final String CALL_ASIGNARAC_ROL = "AR";
	
	/** La Constante CALL_SOLICITUD_DESACTIVACION_TO_ENTIDAD. */
	public static final String CALL_SOLICITUD_DESACTIVACION_TO_ENTIDAD = "SDE";
	
	/** La Constante CALL_SOLICITUD_MODIFICACION_TO_ENTIDAD. */
	public static final String CALL_SOLICITUD_MODIFICACION_TO_ENTIDAD = "SME";
	
	/** La Constante CALL_REGISTRAR_SOLICITUD_ENTIDAD. */
	public static final String CALL_REGISTRAR_SOLICITUD_ENTIDAD = "RSE";
	
	/** La Constante CALL_SOLICITUD_DESACTIVACION_TO_USUARIO. */
	public static final String CALL_SOLICITUD_DESACTIVACION_TO_USUARIO = "SDE";
	
	/** La Constante CALL_REPORTE_ACTIVIDAD_USUARIO_TO_ENTIDAD. */
	public static final String CALL_REPORTE_ACTIVIDAD_USUARIO_TO_ENTIDAD = "AUE";
	
	/** La Constante CALL_HISTORICO_ENTIDAD_TO_ENTIDAD. */
	public static final String CALL_HISTORICO_ENTIDAD_TO_ENTIDAD = "HEE";
	
	/** La Constante CALL_REPORTE_ACTIVIDAD_USUARIO_TO_USUARIO. */
	public static final String CALL_REPORTE_ACTIVIDAD_USUARIO_TO_USUARIO = "RAU";
	
	/** La Constante CALL_CREAR_ENTIDAD_TO_BUSCAR_PEFIL. */
	public static final String CALL_CREAR_ENTIDAD_TO_BUSCAR_PEFIL = "CEBP";
	
	/** La Constante CALL_DETALLE_SOLICITUD_ENTIDAD_TO_ENTIDAD. */
	public static final String CALL_DETALLE_SOLICITUD_ENTIDAD_TO_ENTIDAD = "DSEE";
	
	/** La Constante CALL_CREAR_ENTIDAD_ADMINISTRAR_PERFIL. */
	public static final String CALL_CREAR_ENTIDAD_ADMINISTRAR_PERFIL = "CEAP";
	
	/** La Constante CALL_MODIFICAR_ENTIDAD_ADMINISTRAR_PERFIL. */
	public static final String CALL_MODIFICAR_ENTIDAD_ADMINISTRAR_PERFIL = "MEAP";
	
	/** La Constante CALL_ADMINISTRAR_PERFIL_TO_ENTIDAD. */
	public static final String CALL_ADMINISTRAR_PERFIL_TO_ENTIDAD = "APE";
	
	/** La Constante CALL_BUSCAR_ENTIDAD_TO_MODIFICAR_ENTIDAD. */
	public static final String CALL_BUSCAR_ENTIDAD_TO_MODIFICAR_ENTIDAD = "BEME";
	
	/** La Constante CALL_BUSCAR_ENTIDAD_TO_DETALLE_ENTIDAD. */
	public static final String CALL_BUSCAR_ENTIDAD_TO_DETALLE_ENTIDAD = "BEDE";
	
	/** La Constante CALL_BUSCAR_SOLICITUD_MODIFICACION_ENTIDAD. */
	public static final String CALL_BUSCAR_SOLICITUD_MODIFICACION_ENTIDAD = "BSME";
	
	/** La Constante CALL_DETALLE_ENTIDAD_TO_DETALLE_USUARIO. */
	public static final String CALL_DETALLE_ENTIDAD_TO_DETALLE_USUARIO = "DEDU";
	
	/** La Constante CALL_DETALLE_HISTORICO_ENTIDAD_TO_DETALLE_USUARIO. */
	public static final String CALL_DETALLE_HISTORICO_ENTIDAD_TO_DETALLE_USUARIO = "DHEDU";
	
	/** La Constante CALL_BUSCAR_HISTORICO_USUARIO_TO_DETALLE_USUARIO. */
	public static final String CALL_BUSCAR_HISTORICO_USUARIO_TO_DETALLE_USUARIO = "BHUDU";
	
	/** La Constante CALL_BUSCAR_HISTORICO_ENTIDAD_TO_DETALLE_HISTORICO_ENTIDAD. */
	public static final String CALL_BUSCAR_HISTORICO_ENTIDAD_TO_DETALLE_HISTORICO_ENTIDAD = "BHEDHE";
	
	/** La Constante CALL_BUSCAR_SOLICITUD. */
	public static final String CALL_BUSCAR_SOLICITUD = "BS";
	
	/** La Constante CALL_BUSCAR_SOLICITUD_DESACTIVACION_ADMINISTRADOR. */
	public static final String CALL_BUSCAR_SOLICITUD_DESACTIVACION_ADMINISTRADOR = "BSDA";
	
	/** La Constante CALL_DESACTIVAR_USUARIO. */
	public static final String CALL_DESACTIVAR_USUARIO = "DEU";
	
		
	/** La Constante CALL_EXTENSION_XLSX. */
	public static final String CALL_EXTENSION_XLSX = "xlsx";
	
	/** La Constante CALL_SIZE_XLSX. */
	public static final long CALL_SIZE_XLSX = 9769;

	/** La Constante SIZE_PAGINACION_BUSQUEDA. */
	public static final int SIZE_PAGINACION_BUSQUEDA = 10;
	
	
	/** La Constante ACCION_NUEVO_DOC_NOV. */
	public static final String ACCION_NUEVO_DOC_NOV = "NN";
	
	/** La Constante ACCION_NUEVO_DOC_FAQ. */
	public static final String ACCION_NUEVO_DOC_FAQ = "FD";
	
	/** La Constante ACCION_NUEVO_DOC_PIE_PAGINA. */
	public static final String ACCION_NUEVO_DOC_PIE_PAGINA = "FPDP";
	
	/** La Constante ACCION_NUEVO_DOC_DOCUMENTO. */
	public static final String ACCION_NUEVO_DOC_DOCUMENTO = "RND";
	
	/** La Constante ACCION_EDITAR_DOC_DOCUMENTO. */
	public static final String ACCION_EDITAR_DOC_DOCUMENTO = "RED";
	
	/** La Constante ACCION_NUEVO_DOC_ESTADISTICA. */
	public static final String ACCION_NUEVO_DOC_ESTADISTICA = "JN";
	
	/** La Constante ACCION_EDITAR_DOC_ESTADISTICA. */
	public static final String ACCION_EDITAR_DOC_ESTADISTICA = "JE";
	
	/** La Constante ACCION_NUEVO_DOC_BANNER. */
	public static final String ACCION_NUEVO_DOC_BANNER = "NBAN";
	
	/** La Constante ACCION_EDITAR_DOC_BANNER. */
	public static final String ACCION_EDITAR_DOC_BANNER = "EBAN";

	/** La Constante DOWNLOAD_PATH. */
	public static final String DOWNLOAD_PATH = FacesUtil.getContextPath() + "/download";
	
	/** La Constante IMAGE_SERVLET_PATH. */
	public static final String IMAGE_SERVLET_PATH = FacesUtil.getContextPath() + "/Image";
	
	/** La Constante SERVICE_CONTEXT. */
	public static final String SERVICE_CONTEXT = "ServiceContext";
	
	
	/** La Constante MENSAJE_ERROR_CAMPO_OBLIGATORIO. */
	public static final String MENSAJE_ERROR_CAMPO_OBLIGATORIO = "Ingresar campos obligatorios";
	
	/** La Constante PRIVILEGIO_CREAR_ENTIDAD. */
	public static final String PRIVILEGIO_CREAR_ENTIDAD = "CREAR ENTIDAD";
	
	/** La Constante IMAGEN_JPG. */
	public static final String IMAGEN_JPG = "JPG";
	
	/** La Constante IMAGEN_JPEG. */
	public static final String IMAGEN_JPEG = "JPEG";
	
	/** La Constante ACCION_REGRESAR_SOLICITUD_DESACTIVACION_ADMINISTRADOR. */
	public static final String ACCION_REGRESAR_SOLICITUD_DESACTIVACION_ADMINISTRADOR = "volverSolicitudDesc";
	
	/** La Constante ACCION_BUSCAR_ACCESO. */
	public static final String ACCION_BUSCAR_ACCESO = "AA";

}