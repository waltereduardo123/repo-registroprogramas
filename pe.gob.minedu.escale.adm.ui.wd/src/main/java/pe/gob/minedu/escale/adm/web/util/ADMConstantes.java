package pe.gob.minedu.escale.adm.web.util;


public class ADMConstantes {
		
		/** La Constante MODIFICAR_CLAVE_USUARIO. */
		public static final String MODIFICAR_CLAVE_USUARIO = "modificarContrasena";	
		
		/** La Constante RECUPERAR_CLAVE_USUARIO. */
		public static final String RECUPERAR_CLAVE_USUARIO = "recuperarClaveUsuario";	
		
		/** La Constante MENSAJE_CAMBIO_CLAVE_TEMPORAL. */
		public static final String MENSAJE_CAMBIO_CLAVE_TEMPORAL = "mensajeCambioClaveTemporal";	
		
		/** La Constante MODIFICAR_CLAVE_TEMPORAL. */
		public static final String MODIFICAR_CLAVE_TEMPORAL = "modificarContrasenaTemporal";	
		
		
		//////CU06 REGISTRAR SOLICITUD
		/** La Constante VER_REGISTRAR_SOLICITUD. */
		public static final String VER_REGISTRAR_SOLICITUD = "verRegistrarSolicitud";
		
		/** La Constante REGRESAR_VENTANA_SOLICITUD_DESAC_ADM. */
		public static final String REGRESAR_VENTANA_SOLICITUD_DESAC_ADM = "regresar";
		
		/** La Constante REGRESAR_MODIFICACION_ENTIDAD. */
		public static final String REGRESAR_MODIFICACION_ENTIDAD = "regresarModificacionEntidad";
		
		/** La Constante VER_REGISTRAR_RESULTADO_EVALUACION. */
		public static final String VER_REGISTRAR_RESULTADO_EVALUACION = "verRegistrarResultadoEvaluacion";
		
		/** La Constante VER_VENTANA_SATISFACTORIO. */
		public static final String VER_VENTANA_SATISFACTORIO = "satisfactorio";
		
		/** La Constante REGRESAR_VENTANA_BUSCAR_SOLICITUD. */
		public static final String REGRESAR_VENTANA_BUSCAR_SOLICITUD = "regresar";
		
		/** La Constante REGRESAR_VENTANA_SOLICITUD_REGISTRO_MODIFICACION_ENTIDAD. */
		public static final String REGRESAR_VENTANA_SOLICITUD_REGISTRO_MODIFICACION_ENTIDAD = "regresarModificacionEntidad";
		
		/** La Constante NUMERO_DE_FORMULARIO. */
		public static final String NUMERO_DE_FORMULARIO = "NUMFORM";

}
