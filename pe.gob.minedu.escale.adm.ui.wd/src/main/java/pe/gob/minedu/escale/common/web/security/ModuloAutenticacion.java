package pe.gob.minedu.escale.common.web.security;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import pe.gob.minedu.escale.adm.business.exception.UsuarioNoExisteException;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioOrganismoDTO;
import pe.gob.minedu.escale.adm.wd.jsf.bean.UsuarioBean;
import pe.gob.minedu.escale.adm.web.util.ConstantesUtil;
import pe.gob.minedu.escale.common.business.ServiceContext;
import pe.gob.minedu.escale.common.business.state.UsuarioState;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.web.jsf.util.ControlMessageUtil;

/**
 *
 * @author IMENDOZA
 */
public class ModuloAutenticacion extends BaseBean implements LoginModule {

	private static final long serialVersionUID = -2556289430303973932L;
	@SuppressWarnings("unused")
	private Subject sujeto = null;
    private EscaleCallbackHandler escaleCallbackHandler = null;
    @SuppressWarnings("unused")
	private CallbackHandler callBackHandler = null;
    @SuppressWarnings({ "rawtypes", "unused" })
	private Map sharedState;
    @SuppressWarnings({ "rawtypes", "unused" })
	private Map options;
    private boolean succeeded = false;
     /** El contexto. */
    @SuppressWarnings("unused")
	private ServiceContext context = new ServiceContext();

    private UsuarioDTO usuario = new UsuarioDTO();
    
    @SuppressWarnings("unused")
	private UsuarioBean usuarioBean;
    
     /** El control message util. */
    private ControlMessageUtil controlMessageUtil = new ControlMessageUtil(getErrorService(), ModuloAutenticacion.class);  
    
    /** El servicio de administracion service local. */
    @EJB
    private  AdministracionServiceLocal administracionServiceLocal = lookupModuloAutenticacion();

    @Override
    public void initialize(Subject sujeto, CallbackHandler callBackHandler,
                    Map<String, ?> estados, Map<String, ?> opciones) {
            this.sujeto = sujeto;
            this.callBackHandler = callBackHandler;
            succeeded = false;
    }

    public void inicializar(Subject sujeto, EscaleCallbackHandler escaleCallbackHandler,
                    Map<String, ?> estados, Map<String, ?> opciones) {
            this.sujeto = sujeto;
            this.escaleCallbackHandler = escaleCallbackHandler;
            succeeded = false;
    }
    
    @Override
    public boolean login() throws LoginException {        
        if (escaleCallbackHandler == null) {
                throw new LoginException("El 'escaleCallbackHandler' es nulo");
        }
        String name  = escaleCallbackHandler.name;
        String password  = escaleCallbackHandler.password;
        try {            
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); 

            usuario = administracionServiceLocal.buscarUsuarioxOID(name);
            if(usuario != null){
                if(passwordEncoder.matches(password, usuario.getContrasena())){
                    List<UsuarioOrganismoDTO> listaUsuarioOrganismo = administracionServiceLocal.obtenerUsuarioOrganismoByidUsuario(usuario.getId());
                    String modulo = listaUsuarioOrganismo.get(0).getListaUsuarioRol().get(0).getPerfilRol().getRol().getModulo().getNombre();
                    if (modulo.equalsIgnoreCase("ADMINISTRACION")) {
                        if (usuario.getEstado().equals(UsuarioState.BLOQUEADO_INTENTOS_FALLIDOS.getKey())) {
                        throw new FailedLoginException("13:Credenciales correctas: Usuario inhabilitado por intetos fallidos");
                        }
                        if(usuario.getEstado().equals(UsuarioState.ACTIVO.getKey())){
                            succeeded = true;
                            return succeeded;
                        }
                    }else{
                        throw new UsuarioNoExisteException();
                    }
                    
                }
            }else{
                 throw new FailedLoginException();
            }
        } catch (UsuarioNoExisteException ue) {
                    controlMessageUtil.imprimirMessageClave(ConstantesUtil.MENSAJE_USER_NO_EXISTE_EN_SISTEMA, FacesMessage.SEVERITY_INFO);
	} catch (FailedLoginException ue) {
                    controlMessageUtil.imprimirMessageClave(ConstantesUtil.MESSAGE_ERROR_CREDENTIALS, FacesMessage.SEVERITY_INFO);
	} catch (Exception ex) {
            java.util.logging.Logger.getLogger(ModuloAutenticacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return succeeded;
    }

    @Override
    public boolean commit() throws LoginException {
            return true;
    }

    @Override
    public boolean abort() throws LoginException {
            return true;
    }

    @Override
    public boolean logout() throws LoginException {
            this.sujeto = null;
            this.callBackHandler = null;
            return false;
    }
    
    private AdministracionServiceLocal lookupModuloAutenticacion() {
            try {
                Context c = new InitialContext();
                return (AdministracionServiceLocal) c.lookup("java:global/pe.gob.minedu.escale.adm.ui.wd-1.0.0-PRO/AdministracionService!pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal");
            } catch (NamingException ne) {
                throw new RuntimeException(ne);
            }
        }

}
