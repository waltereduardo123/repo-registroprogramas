/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.minedu.escale.adm.wd.utils;

import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 *
 * @author IMENDOZA
 */
public class LifeCycleListener implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent event) {
//        System.out.println("END PHASE " + event.getPhaseId());

        if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            for (Iterator<FacesMessage> iter = event.getFacesContext().getMessages(); iter.hasNext();) {
                FacesMessage msg = iter.next();
//                System.out.println(msg.getSummary());
            }
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {
//        System.out.println("START PHASE " + event.getPhaseId());

        if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            for (Iterator<FacesMessage> iter = event.getFacesContext().getMessages(); iter.hasNext();) {
                FacesMessage msg = iter.next();
//                System.out.println(msg.getSummary());
            }
        }
    }

    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }

    
}