package pe.gob.minedu.escale.adm.ejb.dao;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.adm.model.jpa.UsuarioOrganismoDesactivacion;

@Local
public interface UsuarioOrganismoDesactivacionDAOLocal extends StatelessCRUDServices<Long, UsuarioOrganismoDesactivacion> {
	
    /**
     * Save.
     *
     * @param entity el entity
     */
    void save(UsuarioOrganismoDesactivacion entity) ;
}
