package pe.gob.minedu.escale.adm.ejb.dao;

import java.util.List;

import javax.ejb.Local;


import pe.gob.minedu.escale.adm.model.jpa.Clave;
import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;

@Local
public interface ClaveDAOLocal extends StatelessCRUDServices<Long, Clave> {

	void saveClave(Clave entity);
	void updateClave(Clave entity);
	void deleteClave(Clave entity);
   
   public Clave registrarClave(Clave entity);
   public Clave findById(Long idClave);
   public List<Clave> listClaveRegistrada(String idUsuario);
	
}
