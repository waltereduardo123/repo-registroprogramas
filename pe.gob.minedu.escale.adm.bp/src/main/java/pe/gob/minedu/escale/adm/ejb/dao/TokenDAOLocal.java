package pe.gob.minedu.escale.adm.ejb.dao;


import pe.gob.minedu.escale.adm.model.jpa.Acceso;
import pe.gob.minedu.escale.adm.model.jpa.TokenSecury;
import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;

import java.util.List;

import javax.ejb.Local;

@Local
public interface TokenDAOLocal extends StatelessCRUDServices<Long, TokenSecury> {
	
	void saveTokenSecury(TokenSecury entity);
	void updateTokenSecury(TokenSecury entity);
	void deleteTokenSecury();
	public List<TokenSecury> listTokenRegistrado();
	
  
  
}
