package pe.gob.minedu.escale.adm.ejb.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.ejb.dao.ClaveDAOLocal;
import pe.gob.minedu.escale.adm.model.jpa.Clave;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.LogUtil;

@Stateless
public class ClaveDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Clave> implements ClaveDAOLocal {

    @SuppressWarnings("unused")
	private static LogUtil log = new LogUtil(ClaveDAOImpl.class.getName());
    
    private Session sess;

	private SessionFactory sessionFactoria;
    
	

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	/** El em. */
    @PersistenceContext(unitName = "EJBAdministracionPU")
    private EntityManager em;
	
	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveClave(Clave entity) {
		em.persist(entity);
		em.flush();	
	}

	@Override
	public void updateClave(Clave entity) {
		Query query = em.createQuery("update from Clave set c_clave = :clave  where c_codigo = :codigo");
		query.setParameter("codigo", entity.getCodigo());
		query.setParameter("clave", entity.getClave());
		query.executeUpdate();
		
	}

	@Override
	public void deleteClave(Clave entity) {
		em.remove(entity);
	}

	@Override
	public Clave registrarClave(Clave entity) {
		   em.persist(entity);
	       em.flush();
	    return entity;
	}

	@Override
	public Clave findById(Long idClave) {
		return em.find(Clave.class, idClave );
	}

 	
	@Override
	public List<Clave> listClaveRegistrada(String idusuario) {
		List<Clave> li = new ArrayList<Clave>();
		Clave clave = null;
		StringBuilder sbQuery = new StringBuilder();
		    
		  try {
			  	sbQuery.append("SELECT clave ");
			    sbQuery.append("FROM Clave clave ");
			    sbQuery.append("WHERE 1=1 ");
			    sbQuery.append("AND clave.codigo = :idusuario ");
			    
			    Query query = em.createQuery(sbQuery.toString());
			    
			    query.setParameter("idusuario", idusuario);
			  
			  li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
	}



}
