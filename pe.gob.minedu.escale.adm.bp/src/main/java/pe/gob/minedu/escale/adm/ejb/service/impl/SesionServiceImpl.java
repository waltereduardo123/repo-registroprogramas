package pe.gob.minedu.escale.adm.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.business.exception.UsuarioEstaDesactivadoException;
import pe.gob.minedu.escale.adm.business.type.IndicadorAccesoType;
import pe.gob.minedu.escale.adm.ejb.dao.SesionDAOLocal;
import pe.gob.minedu.escale.adm.ejb.service.SesionServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.AccesoDTO;
import pe.gob.minedu.escale.adm.model.jpa.Acceso;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.LogUtil;


@PermitAll
@Stateless(name = "SesionService", mappedName = "ejb/SesionService")
public class SesionServiceImpl implements SesionServiceLocal {

	/** El log. */
    private static LogUtil log = new LogUtil(SesionServiceImpl.class.getName());
    
    /** El servicio sesion dao. */
	
    @EJB
    private SesionDAOLocal sesionDAO;
    
    private List<AccesoDTO> alertIntromision;
	
	@Override
	public AccesoDTO registrarNevegacionUser(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		
		try {
			sesion.setIdSesion(seDTO.getIdSesion());
			sesion.setUsuario(seDTO.getUsuario());
			sesion.setToken(seDTO.getToken());
			sesion.setFecIniSesion(seDTO.getFecIniSesion());
			sesion.setFecFinSesion(seDTO.getFecFinSesion());
			sesion.setUsuarioCreacion(seDTO.getUsuarioCreacion());
			sesion.setFechaCreacion(FechaUtil.obtenerFechaActual());
			
			
			sesion.setNavegador(seDTO.getNavegador());/////////////
 
			sesion.setHostcliente(seDTO.getHostcliente());/////////////////////////
			sesion.setIndAcc("0");
			
			sesion.setEstado("1");
			sesion=sesionDAO.registrarSesion(sesion);
			sesdto=ConversorHelper.convertir(AccesoDTO.class, sesion);
			
		} catch (Exception e) {
            log.error(e);            
            if (e instanceof UsuarioEstaDesactivadoException ) {
                throw new UsuarioEstaDesactivadoException();
            }			                                 
            throw new Exception(e);
        }
		
		return sesdto;
	}
	@Override
	public AccesoDTO registrarNevegacionIntrusa(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		
		try {
			sesion.setIdSesion(seDTO.getIdSesion());
			sesion.setNavegador(seDTO.getNavegador());
			sesion.setUsuario(seDTO.getUsuario());
			sesion.setToken(seDTO.getToken());
			sesion.setHostcliente(seDTO.getHostcliente());
			sesion.setIndAcc(IndicadorAccesoType.BROWSE_DIFERENTES.getValue());
			sesion.setFecIniSesion(seDTO.getFecIniSesion());
			sesion.setFecFinSesion(seDTO.getFecFinSesion());
			sesion.setUsuarioCreacion(seDTO.getUsuarioCreacion());
			sesion.setFechaCreacion(FechaUtil.obtenerFechaActual());
			sesion.setEstado("1");
			sesion=sesionDAO.registrarSesion(sesion);
			sesdto=ConversorHelper.convertir(AccesoDTO.class, sesion);
			
		} catch (Exception e) {
            log.error(e);            
            if (e instanceof UsuarioEstaDesactivadoException ) {
                throw new UsuarioEstaDesactivadoException();
            }			                                 
            throw new Exception(e);
        }
		
		return sesdto;
	}
	
	@Override
	public AccesoDTO eliminarNevegacionUser(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());
		
		sesionDAO.deleteAllSesionbyUsuario(seDTO.getUsuario());
		
		return sesdto;
	}
	
	@Override
	public AccesoDTO eliminarNevegacionUserHost(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());
		sesion.setNavegador(seDTO.getNavegador());
		sesion.setHostcliente(seDTO.getHostcliente());
		sesionDAO.deleteSesionUserBrowHost(seDTO.getUsuario(), seDTO.getNavegador() , seDTO.getHostcliente());
		return sesdto;
	}
	
	@Override
	public void eliminarNevegacionUserxIdSesion(AccesoDTO seDTO) throws Exception {
		sesionDAO.deleteSesionbyUsuarioId(seDTO.getUsuario(), seDTO.getIdSesion());
	}
	
	
	@Override
	public AccesoDTO actualizaNevegacionUser(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());
		sesion.setNavegador(seDTO.getNavegador());
		sesion.setHostcliente(seDTO.getHostcliente());
		sesion.setIndAcc(seDTO.getIndAcc());
		sesionDAO.updateSesion(sesion);
		
		return sesdto;
	}
	
	@Override
	public AccesoDTO actualizaNevegacionUserxId(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());
		sesion.setIdSesion(seDTO.getIdSesion());
		sesion.setNavegador(seDTO.getNavegador());
		sesion.setHostcliente(seDTO.getHostcliente());
		sesion.setIndAcc(seDTO.getIndAcc());
		sesionDAO.updateSesionUsuarioxId(sesion);
		
		return sesdto;
	}
	@Override
	public AccesoDTO actualizarTokenCambioCombo(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());
		sesion.setToken(seDTO.getToken());
		sesionDAO.updateSesionToken(sesion);
		return sesdto;
	}
	@Override
	public  AccesoDTO verificarNevegacionUser(AccesoDTO seDTO) throws Exception {
		AccesoDTO sesdto=new AccesoDTO();	
		try {
			//Acceso sesion=new Acceso();			
			//sesion.setUsuario(seDTO.getUsuario());
			List<Acceso> listAcceso=sesionDAO.listSesionRegistrada(seDTO.getUsuario());
			 if(listAcceso!=null && listAcceso.size()>0){
				 sesdto=ConversorHelper.convertir(AccesoDTO.class, listAcceso.get(0));
			 }else{
				 sesdto.setUsuario("ok");
			 } 
                        
        } catch (Exception e) {
            log.error(e);            
            if (e instanceof UsuarioEstaDesactivadoException ) {
                throw new UsuarioEstaDesactivadoException();
            }			                                 
            throw new Exception(e);
        }
		return sesdto;
	}
	@Override
	public List<AccesoDTO> verificarNevegacionUserTupla(AccesoDTO seDTO) throws Exception {
		List<AccesoDTO> sesdtolist=new ArrayList<AccesoDTO>();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());			
		try {
			List<Acceso> listAcceso=sesionDAO.listSesionRegistradaUser(seDTO.getUsuario());
				for(int i=0;i<listAcceso.size();i++){
					AccesoDTO sesdto=new AccesoDTO();
					sesdto=ConversorHelper.convertir(AccesoDTO.class, listAcceso.get(i));
					sesdtolist.add(sesdto);
				}				
                        
        } catch (Exception e) {

        }
		 	
		return sesdtolist;
	}
	
	@Override
	public List<AccesoDTO> alertIntromision(AccesoDTO seDTO) throws Exception {
		List<AccesoDTO> sesdtolist=new ArrayList<AccesoDTO>();		
		try {		
			if(seDTO!=null){
				sesdtolist.add(seDTO); 
				setAlertIntromision(sesdtolist);
			}
			
				          
        } catch (Exception e) {
        }
		return sesdtolist;
	}
	
	@Override
	public boolean verAlertIntromision() throws Exception {
		boolean isok=false;
		System.out.println("GET LISTADO : " + getAlertIntromision() );
		
		if(getAlertIntromision().size()==0){
				isok= false;
			}else{
				isok= true;
			}
		return isok;
	}
	
	@Override
	public List<AccesoDTO> verificarNevegacionUserIndAcion(AccesoDTO seDTO) throws Exception {
		List<AccesoDTO> sesdtolist=new ArrayList<AccesoDTO>();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());			
		try {
			List<Acceso> listAcceso=sesionDAO.listSesionRegistradaUserxIndAcc(seDTO.getUsuario(), seDTO.getIndAcc());
				for(int i=0;i<listAcceso.size();i++){
					AccesoDTO sesdto=new AccesoDTO();
					sesdto=ConversorHelper.convertir(AccesoDTO.class, listAcceso.get(i));
					sesdtolist.add(sesdto);
				}				
                        
        } catch (Exception e) {

        }
		 	
		return sesdtolist;
	}
	
	@Override
	public List<AccesoDTO> verificarNevegacionxIdSession(AccesoDTO seDTO) throws Exception {
		List<AccesoDTO> sesdtolist=new ArrayList<AccesoDTO>();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());			
		try {
			List<Acceso> listAcceso=sesionDAO.listSesionRegistradaxIdsesion(seDTO.getIdSesion());
				for(int i=0;i<listAcceso.size();i++){
					AccesoDTO sesdto=new AccesoDTO();
					sesdto=ConversorHelper.convertir(AccesoDTO.class, listAcceso.get(i));
					sesdtolist.add(sesdto);
				}				

                        
        } catch (Exception e) {

        }
		 	
		return sesdtolist;
		
	}
	
	@Override
	public List<AccesoDTO> verificarNevegacionUserBrowseHost(AccesoDTO seDTO) throws Exception {
		List<AccesoDTO> sesdtolist=new ArrayList<AccesoDTO>();
		Acceso sesion=new Acceso();
		sesion.setUsuario(seDTO.getUsuario());			
		try {
			List<Acceso> listAcceso=sesionDAO.listSesionRegUserBrowseHost(seDTO.getUsuario(), seDTO.getNavegador(), seDTO.getHostcliente());
				for(int i=0;i<listAcceso.size();i++){
					AccesoDTO sesdto=new AccesoDTO();
					sesdto=ConversorHelper.convertir(AccesoDTO.class, listAcceso.get(i));
					sesdtolist.add(sesdto);
				}				                        
        } catch (Exception e) {
                      
        }	
		return sesdtolist;
	}
	
	
	public List<AccesoDTO> getAlertIntromision() {
		return alertIntromision;
	}
	
	public void setAlertIntromision(List<AccesoDTO> alertIntromision) {
		this.alertIntromision = alertIntromision;
	}




}
