package pe.gob.minedu.escale.adm.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.business.exception.UsuarioEstaDesactivadoException;
import pe.gob.minedu.escale.adm.ejb.dao.TokenDAOLocal;
import pe.gob.minedu.escale.adm.ejb.service.SecuryTokenServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.AccesoDTO;
import pe.gob.minedu.escale.adm.model.dto.TokenSecuryDTO;
import pe.gob.minedu.escale.adm.model.jpa.Acceso;
import pe.gob.minedu.escale.adm.model.jpa.TokenSecury;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.util.LogUtil;


@PermitAll
@Stateless(name = "SecuryTokenService", mappedName = "ejb/SecuryTokenService")
public class SecuryTokenServiceImpl implements SecuryTokenServiceLocal {

	/** El log. */
    private static LogUtil log = new LogUtil(SecuryTokenServiceImpl.class.getName());
    
    /** El servicio sesion dao. */
	
    @EJB
    private TokenDAOLocal sesionDAO;
    


	

	

	
//	@Override
//	public TokenSecuryDTO actualizarTokenSecutityForm(TokenSecuryDTO seDTO) throws Exception {
//		TokenSecuryDTO sesdto=new TokenSecuryDTO();
//		TokenSecury sesion=new TokenSecury();
//		sesion.setToken(seDTO.getToken());
//		sesionDAO.updateTokenSecury(sesion);
//		return sesdto;
//	}


	@Override
	public TokenSecuryDTO verificarTokenForm(TokenSecuryDTO seDTO) throws Exception {
		TokenSecuryDTO sesdto=new TokenSecuryDTO();	
		try {

			List<TokenSecury> listAcceso=sesionDAO.listTokenRegistrado();
			 if(listAcceso!=null && listAcceso.size()>0){
				 sesdto=ConversorHelper.convertir(TokenSecuryDTO.class, listAcceso.get(0));
			 }else{
				 sesdto.setToken("no");
			 } 
                        
        } catch (Exception e) {
            log.error(e);            
            if (e instanceof UsuarioEstaDesactivadoException ) {
                throw new UsuarioEstaDesactivadoException();
            }			                                 
            throw new Exception(e);
        }
		return sesdto;
	}


	@Override
	public List<TokenSecuryDTO> listVerificarTokenForm() throws Exception {
		List<TokenSecuryDTO> sesdtolist=new ArrayList<TokenSecuryDTO>();
//		TokenSecury sesion=new TokenSecury();
//		sesion.setToken(seDTO.getToken());			
		try {
			List<TokenSecury> listAcceso=sesionDAO.listTokenRegistrado();
			System.out.println("test : "  + listAcceso );	
			if(listAcceso.size()>0) {
						for(int i=0;i<listAcceso.size();i++){
								TokenSecuryDTO sesdto=new TokenSecuryDTO();
								sesdto=ConversorHelper.convertir(TokenSecuryDTO.class, listAcceso.get(i));
								sesdtolist.add(sesdto);
							}
			}else {
				TokenSecuryDTO sesdtoEr=new TokenSecuryDTO();
				sesdtoEr.setToken("err");
				sesdtolist.add(sesdtoEr);
			}
                        
        } catch (Exception e) {

        }
		 	
		return sesdtolist;
	}


	@Override
	public TokenSecuryDTO registrarTokenForm(TokenSecuryDTO seDTO) throws Exception {
		TokenSecuryDTO sesdto=new TokenSecuryDTO();
		TokenSecury sesion=new TokenSecury();
		
		try {
			sesion.setIdToken(sesdto.getIdToken());
			sesion.setToken(seDTO.getToken());
		
		
			sesionDAO.saveTokenSecury(sesion);
//			sesdto=ConversorHelper.convertir(TokenSecuryDTO.class, sesion);
			
		} catch (Exception e) {
            log.error(e);            
            if (e instanceof UsuarioEstaDesactivadoException ) {
                throw new UsuarioEstaDesactivadoException();
            }			                                 
            throw new Exception(e);
        }
		
		return sesdto;
	}


	@Override
	public TokenSecuryDTO eliminarTokenForm(TokenSecuryDTO seDTO) throws Exception {
		TokenSecuryDTO sesdto=new TokenSecuryDTO();
		TokenSecury sesion=new TokenSecury();
//		sesion.setIdToken(seDTO.getIdToken());
		
		sesionDAO.deleteTokenSecury();
		
		return sesdto;
	}




}
