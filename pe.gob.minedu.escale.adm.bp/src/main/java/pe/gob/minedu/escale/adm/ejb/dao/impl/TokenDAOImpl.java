package pe.gob.minedu.escale.adm.ejb.dao.impl;

import java.util.ArrayList;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.ejb.dao.TokenDAOLocal;
import pe.gob.minedu.escale.adm.model.jpa.TokenSecury;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.LogUtil;

@Stateless
public class TokenDAOImpl extends AbstractJtaStatelessCRUDServices<Long, TokenSecury> implements TokenDAOLocal {

	/** El log. */
	@SuppressWarnings("unused")
	private static LogUtil log = new LogUtil(TokenDAOImpl.class.getName());

	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	/** El em. */
	@PersistenceContext(unitName = "EJBAdministracionPU")
	private EntityManager em;

	/**
	 * Instantiates a new usuario dao impl.
	 */
	public TokenDAOImpl() {
	}

	@Override
	public void saveTokenSecury(TokenSecury entity) {
		em.persist(entity);
		em.flush();
	}

	@Override
	public void updateTokenSecury(TokenSecury entity) {
		Query query = em.createQuery("update from TokenSecury set c_token = :token ");
		query.setParameter("token", entity.getToken());
		query.executeUpdate();

	}

	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}

	@Override
	public void deleteTokenSecury() {
		Query query = em.createQuery("delete from TokenSecury ");
		query.executeUpdate();
	}

	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TokenSecury> listTokenRegistrado() {

		List<TokenSecury> li = new ArrayList<TokenSecury>();
		StringBuilder sbQuery = new StringBuilder();
		    
		  try {
			  	sbQuery.append("SELECT a ");
			    sbQuery.append("FROM TokenSecury a ");
			    sbQuery.append("WHERE 1=1 ");

			    Query query = em.createQuery(sbQuery.toString());
		
			  
			    li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
	}

}
