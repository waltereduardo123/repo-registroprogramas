package pe.gob.minedu.escale.adm.ejb.service;


import java.util.List;

import pe.gob.minedu.escale.adm.model.dto.AccesoDTO;

public interface SesionServiceLocal {

	   AccesoDTO registrarNevegacionUser(AccesoDTO seDTO)  throws Exception;
	   AccesoDTO registrarNevegacionIntrusa(AccesoDTO seDTO)  throws Exception;
	   public  AccesoDTO verificarNevegacionUser(AccesoDTO seDTO)  throws Exception;
	   public  List<AccesoDTO> verificarNevegacionUserTupla(AccesoDTO seDTO)  throws Exception;
	   public  List<AccesoDTO> verificarNevegacionUserIndAcion(AccesoDTO seDTO)  throws Exception;
	   public  List<AccesoDTO> verificarNevegacionxIdSession(AccesoDTO seDTO)  throws Exception;
	   public  List<AccesoDTO> verificarNevegacionUserBrowseHost(AccesoDTO seDTO)  throws Exception;
	   AccesoDTO eliminarNevegacionUser(AccesoDTO seDTO) throws Exception; 
	   AccesoDTO eliminarNevegacionUserHost(AccesoDTO seDTO) throws Exception; 
	   void eliminarNevegacionUserxIdSesion(AccesoDTO seDTO) throws Exception; 
	   AccesoDTO actualizaNevegacionUser(AccesoDTO seDTO) throws Exception; 
	   AccesoDTO actualizaNevegacionUserxId(AccesoDTO seDTO) throws Exception; 
	   AccesoDTO actualizarTokenCambioCombo(AccesoDTO seDTO) throws Exception;
	   List<AccesoDTO> alertIntromision(AccesoDTO seDTO) throws Exception;
	   boolean verAlertIntromision() throws Exception;
}
