package pe.gob.minedu.escale.adm.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.business.exception.ClaveHistoricaException;
import pe.gob.minedu.escale.adm.ejb.dao.ClaveDAOLocal;

import pe.gob.minedu.escale.adm.ejb.service.ClaveServiceLocal;

import pe.gob.minedu.escale.adm.model.dto.ClaveDTO;
import pe.gob.minedu.escale.adm.model.jpa.Clave;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.util.LogUtil;

@PermitAll
@Stateless(name = "ClaveService", mappedName = "ejb/ClaveService")
public class ClaveServiceImpl implements ClaveServiceLocal {

	/** El log. */
    private static LogUtil log = new LogUtil(ClaveServiceImpl.class.getName());
    
    /** El servicio clave dao. */
	
    @EJB
    private ClaveDAOLocal claveDAO;
    
    
    private List<ClaveDTO> alertIntromision;


	@Override
	public ClaveDTO registrarClaveHistorica(ClaveDTO seDTO) throws Exception {
		ClaveDTO sesdto=new ClaveDTO();
		Clave clave=new Clave();
		
		try {
			clave.setIdClave(seDTO.getIdClave());
			clave.setCodigo(seDTO.getCodigo());
			clave.setClave(seDTO.getClave());
			clave.setTiempoVigencia(seDTO.getTiempoVigencia());
			clave.setFecCrea(seDTO.getFecCrea());
			clave.setUsuCrea(seDTO.getUsuCrea());
	
			clave=claveDAO.registrarClave(clave);
			sesdto=ConversorHelper.convertir(ClaveDTO.class, clave);
			
		} catch (Exception e) {
            log.error(e);            
            if (e instanceof ClaveHistoricaException ) {
                throw new ClaveHistoricaException();
            }			                                 
            throw new Exception(e);
        }
		
		return sesdto;
	}

	@Override
	public List<ClaveDTO> verificarClaveUser(ClaveDTO seDTO) throws Exception {
		List<ClaveDTO> sesdto=new ArrayList<ClaveDTO>();	
		try {
			List<Clave> listClave=claveDAO.listClaveRegistrada(seDTO.getCodigo());
			 if(listClave!=null && listClave.size()>0){
				 sesdto=ConversorHelper.convertirTodo(ClaveDTO.class, listClave);
			 }else{
				// sesdto.setCodigo("ok");
			 } 
                        
        } catch (Exception e) {
            log.error(e);            
            if (e instanceof ClaveHistoricaException ) {
                throw new ClaveHistoricaException();
            }			                                 
            throw new Exception(e);
        }
		return sesdto;
		
		
	}

}
