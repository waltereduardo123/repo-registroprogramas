package pe.gob.minedu.escale.adm.ejb.service;

import java.util.List;

import pe.gob.minedu.escale.adm.model.dto.TokenSecuryDTO;

public interface SecuryTokenServiceLocal {

	TokenSecuryDTO registrarTokenForm(TokenSecuryDTO seDTO) throws Exception;

	public TokenSecuryDTO verificarTokenForm(TokenSecuryDTO seDTO) throws Exception;

	public List<TokenSecuryDTO> listVerificarTokenForm() throws Exception;

	TokenSecuryDTO eliminarTokenForm(TokenSecuryDTO seDTO) throws Exception;

}
