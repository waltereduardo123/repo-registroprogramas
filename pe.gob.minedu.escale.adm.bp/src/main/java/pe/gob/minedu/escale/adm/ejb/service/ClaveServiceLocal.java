package pe.gob.minedu.escale.adm.ejb.service;

import java.util.List;


import pe.gob.minedu.escale.adm.model.dto.ClaveDTO;

public interface ClaveServiceLocal {
	

	   ClaveDTO registrarClaveHistorica(ClaveDTO seDTO)  throws Exception;
	   public  List<ClaveDTO> verificarClaveUser(ClaveDTO seDTO)  throws Exception;
	   
	
}
