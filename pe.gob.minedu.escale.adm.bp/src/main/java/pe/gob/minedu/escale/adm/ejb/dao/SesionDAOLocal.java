package pe.gob.minedu.escale.adm.ejb.dao;

import pe.gob.minedu.escale.adm.model.jpa.Acceso;
import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SesionDAOLocal extends StatelessCRUDServices<Long, Acceso> {
	
	

	
	//void inciarSessionFactory();

	void saveSesion(Acceso entity);
	void updateSesion(Acceso entity);
	void updateSesionUsuarioxId(Acceso entity);
	void updateSesionToken(Acceso entity);
	void deleteSesion(Acceso entity);
	
  
   //Acceso deleteSesionObj(Long idUsuario);
   public Acceso registrarSesion(Acceso entity);
   
   Acceso findById(Long idUsuario);
   
   void deleteAllSesionbyUsuario(String idusuario);
   void deleteSesionbyUsuarioId(String idusuario,Long idSesion);
   void deleteSesionUserBrowHost(String idusuario,String browser, String hostCliente);
   public List<Acceso> listSesionRegistrada(String idusuario);
   public List<Acceso> listSesionRegistradaUser(String idusuario);
   public List<Acceso> listSesionRegistradaxIdsesion(Long sesion);
   public List<Acceso> listSesionRegistradaUserxIndAcc(String idusuario, String indAcc);
   public List<Acceso> listSesionRegUserBrowseHost(String idusuario , String browse, String host);
 //  List<Acceso> listSesion();
}
