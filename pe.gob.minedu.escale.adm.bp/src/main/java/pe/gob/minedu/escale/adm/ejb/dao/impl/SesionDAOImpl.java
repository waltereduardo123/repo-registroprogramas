package pe.gob.minedu.escale.adm.ejb.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.hibernate.Session;
import org.hibernate.SessionFactory;


import pe.gob.minedu.escale.adm.ejb.dao.SesionDAOLocal;

import pe.gob.minedu.escale.adm.model.jpa.Acceso;

import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.LogUtil;

@Stateless
public class SesionDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Acceso> implements SesionDAOLocal {
	 
	  /** El log. */
    @SuppressWarnings("unused")
	private static LogUtil log = new LogUtil(SesionDAOImpl.class.getName());
	
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}
	
	/** El em. */
    @PersistenceContext(unitName = "EJBAdministracionPU")
    private EntityManager em;
	
    /**
     * Instantiates a new usuario dao impl.
     */
    public SesionDAOImpl() {
    }

    
    /*
	private Session sess;
	private SessionFactory sessionFactoria;
	
	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}*/
	
	@Override
	public void saveSesion(Acceso entity) {
		em.persist(entity);
		em.flush();
	}


	
	@Override
	public void updateSesion(Acceso entity) {
		Query query = em.createQuery("update from Acceso set c_navegador = :navegador , host_cliente = :cliente ,IND_ACC= :indacc  where usuario = :idUsuario");
		query.setParameter("idUsuario", entity.getUsuario());
		query.setParameter("navegador", entity.getNavegador());
		query.setParameter("cliente", entity.getHostcliente());
		query.setParameter("indacc", entity.getIndAcc());
		query.executeUpdate();

	}
	
	@Override
	public void updateSesionUsuarioxId(Acceso entity) {
		Query query = em.createQuery("update from Acceso set c_navegador = :navegador , host_cliente = :cliente ,IND_ACC= :indacc  where usuario = :idUsuario and idSesion = :idSesion ");
		query.setParameter("idUsuario", entity.getUsuario());
		query.setParameter("idSesion", entity.getIdSesion());
		query.setParameter("navegador", entity.getNavegador());
		query.setParameter("cliente", entity.getHostcliente());
		query.setParameter("indacc", entity.getIndAcc());
		query.executeUpdate();
		
	}

	@Override
	public void updateSesionToken(Acceso entity) {
		Query query = em.createQuery("update from Acceso set c_token = :token  where usuario = :idUsuario ");
		query.setParameter("idUsuario", entity.getUsuario());
		query.setParameter("token", entity.getToken());
		query.executeUpdate();
		
	}
	
	public void inciarSessionFactory() {
  		EntityManager em = getEntityManager();
  		setSess(em.unwrap(Session.class));
  		setSessionFactoria(getSess().getSessionFactory());
  	}
	

  	
	@Override
	public void deleteSesion(Acceso entity) {
	 		em.remove(entity);
	}


	@Override
	public void deleteAllSesionbyUsuario(String idusuario) {
		Query query = em.createQuery("delete from Acceso where usuario = :idUsuario");
		query.setParameter("idUsuario", idusuario);
		query.executeUpdate();
	}
	
	@Override
	public void deleteSesionbyUsuarioId(String idusuario, Long idSesion) {
		Query query = em.createQuery("delete from Acceso where usuario = :idUsuario and idSesion = :idSesion ");
		query.setParameter("idUsuario", idusuario);
		query.setParameter("idSesion", idSesion);
		query.executeUpdate();
		
	}
	
	@Override
	public void deleteSesionUserBrowHost(String idusuario, String browser, String hostCliente) {
		Query query = em.createQuery("delete from Acceso where usuario = :idUsuario and  navegador = :navegador and hostcliente = :hostcliente ");
		query.setParameter("idUsuario", idusuario);  
		query.setParameter("navegador", browser);
		query.setParameter("hostcliente", hostCliente);
		query.executeUpdate();
		
	}
	
//	 @Override
//	    public void deleteUsuario(Long idUsuario) {
//	       em.remove(findById(idUsuario));
//	    }

	@Override
	 public Acceso findById(Long idUsuario) {
	        return em.find(Acceso.class, idUsuario );
	    }
	
	@Override
	public Acceso registrarSesion(Acceso entity) {
		   em.persist(entity);
	        em.flush();
	        return entity;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Acceso> listSesionRegistrada(String idusuario) {
//		  Query query = em.createQuery("select a from Acceso a where a.usuario = :idUsuario and a.fecFinSesion > :fechafin");
		List<Acceso> li = new ArrayList<Acceso>();
		Acceso acceso = null;
		  StringBuilder sbQuery = new StringBuilder();
		    
		  try {
			  sbQuery.append("SELECT a ");
			    sbQuery.append("FROM Acceso a ");
			    sbQuery.append("WHERE 1=1 ");
			    sbQuery.append("AND a.usuario = :idUsuario ");
			    sbQuery.append("AND a.fecFinSesion > :fechafin ");

			    Query query = em.createQuery(sbQuery.toString());
			    
			  query.setParameter("idUsuario", idusuario);
			  query.setParameter("fechafin", new Date(), TemporalType.TIMESTAMP);
			  
			  li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
	}
	
  
    



	@Override
	public EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Acceso> listSesionRegistradaUser(String idusuario) {
		List<Acceso> li = new ArrayList<Acceso>();
		  StringBuilder sbQuery = new StringBuilder();
		    
		  try {
			  sbQuery.append("SELECT a ");
			    sbQuery.append("FROM Acceso a ");
			    sbQuery.append("WHERE 1=1 ");
			    sbQuery.append("AND a.usuario = :idUsuario ");
			    Query query = em.createQuery(sbQuery.toString());
			    
			    query.setParameter("idUsuario", idusuario);
			  
			  li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Acceso> listSesionRegistradaUserxIndAcc(String idusuario , String indAcc) {
		List<Acceso> li = new ArrayList<Acceso>();
		  StringBuilder sbQuery = new StringBuilder();
		    
		  try {
			  sbQuery.append("SELECT a ");
			    sbQuery.append("FROM Acceso a ");
			    sbQuery.append("WHERE 1=1 ");
			    sbQuery.append("AND a.usuario = :idUsuario ");
			    sbQuery.append("AND a.indAcc = :indAccc ");
			    Query query = em.createQuery(sbQuery.toString());
			    
			    query.setParameter("idUsuario", idusuario);
			    query.setParameter("indAccc", indAcc);
			  
			  li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Acceso> listSesionRegistradaxIdsesion(Long sesion) {
		
		List<Acceso> li = new ArrayList<Acceso>();
		  StringBuilder sbQuery = new StringBuilder();
		    
		  try {
			  sbQuery.append("SELECT a ");
			    sbQuery.append("FROM Acceso a ");
			    sbQuery.append("WHERE 1=1 ");
			    sbQuery.append("AND a.idSesion = :isesion ");
			    Query query = em.createQuery(sbQuery.toString());
			    
			    query.setParameter("isesion", sesion);
			  
			  li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
		
	}
	
	@Override
	public List<Acceso> listSesionRegUserBrowseHost(String idusuario, String browse, String host) {
		List<Acceso> li = new ArrayList<Acceso>();
		Acceso acceso = null;
		  StringBuilder sbQuery = new StringBuilder();
		  try {
			  	sbQuery.append("SELECT a ");
			    sbQuery.append("FROM Acceso a ");
			    sbQuery.append("WHERE 1=1 ");
			    sbQuery.append("AND a.usuario = :idUsuario ");  
			    sbQuery.append("AND a.navegador = :browse ");
			    sbQuery.append("AND a.hostcliente = :host ");
			    Query query = em.createQuery(sbQuery.toString());
			    
			    query.setParameter("idUsuario", idusuario);
			    query.setParameter("browse", browse);
			    query.setParameter("host", host);
			  
			  li = query.getResultList();
	                        
	        } catch (Exception e) {
	            log.error(e);
	        }
		  
		  return li;
	}





	

	

	



}
