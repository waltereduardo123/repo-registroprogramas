package pe.gob.minedu.escale.regprograma.business.exception;

import pe.gob.minedu.escale.common.exception.BusinessException;

public class CadenaInvalidaException extends BusinessException {
	  /** La Constante serialVersionUID. */
    private static final long serialVersionUID = -3706220390397773731L;
    
    /** La Constante ERROR_KEY. */
    private static final String ERROR_KEY = CadenaInvalidaException.class.getName();

    
    public CadenaInvalidaException() {
        super(ERROR_KEY);
    }
    
    public CadenaInvalidaException(String parameter) {
        super(ERROR_KEY, parameter);
    }
}
