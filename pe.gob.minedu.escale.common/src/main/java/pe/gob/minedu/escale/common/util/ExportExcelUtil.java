package pe.gob.minedu.escale.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;

import pe.gob.minedu.escale.common.notify.NotificacionUtil;

public class ExportExcelUtil {

	private List<Object[]> listaExportar;
	private List<String> listaExportarCabecera;
	static String MINEDU = "MINISTERIO DE EDUCACIÓN";
	static String REPORTE = "REPORTE :";
	static String FECHA_REPORTE = "Fecha de creación : ";
	static String HORA_REPORTE = "Hora de creación : ";
	static String USER_CREACION = "Usuario : ";
	private String nombreReporte;
	HttpServletResponse response;

	/** La Constante PE_GOB_MINEDU_ESCALE_RESOURCES_NOTIFICACION_PROPERTIES. */
	private static final String PE_GOB_MINEDU_ESCALE_RESOURCES_NOTIFICACION_PROPERTIES = "/pe/gob/minedu/escale/resources/notificacion.properties";
	
	Properties propiedades = new Properties();
	InputStream in = getClass().getResourceAsStream("/pe/gob/minedu/escale/resources/notificacion.properties");
	
	/** La Constante RUTA_TEMPLATES. */
	public static final StringBuffer RUTA_TEMPLATES = new StringBuffer();
	/** La properties. */
	private static Properties properties = null;
	
	
	static {
		try {
			properties = getPropiedades();	
		} catch (Exception e) {
			//log.error(e);
		}
	}
	
	/**
	 * Obtiene propiedades.
	 *
	 * @return propiedades
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Se�ales de que una excepci�n de E / S se ha producido.
	 */
	private static Properties getPropiedades() throws FileNotFoundException, IOException {
		Properties propiedades = new Properties();	
		
		propiedades.load(NotificacionUtil.class.getClassLoader().getResourceAsStream(PE_GOB_MINEDU_ESCALE_RESOURCES_NOTIFICACION_PROPERTIES));

		return propiedades;
	}
	
	public ExportExcelUtil() {

	}

	public ExportExcelUtil(List<Object[]> listaExportar, List<String> listaExportarCabecera, String nombreReporte) {
		super();
		this.listaExportar = listaExportar;
		this.listaExportarCabecera = listaExportarCabecera;
		this.nombreReporte = nombreReporte;
	}

	public ExportExcelUtil(List<Object[]> listaExportar, List<String> listaExportarCabecera, String nombreReporte,
			HttpServletResponse response) {
		super();
		this.listaExportar = listaExportar;
		this.listaExportarCabecera = listaExportarCabecera;
		this.nombreReporte = nombreReporte;
		this.response = response;
	}

	public List<Object[]> getListaExportar() {
		return listaExportar;
	}

	public void setListaExportar(List<Object[]> listaExportar) {
		this.listaExportar = listaExportar;
	}

	public List<String> getListaExportarCabecera() {
		return listaExportarCabecera;
	}

	public void setListaExportarCabecera(List<String> listaExportarCabecera) {
		this.listaExportarCabecera = listaExportarCabecera;
	}

	public static String getMINEDU() {
		return MINEDU;
	}

	public static void setMINEDU(String mINEDU) {
		MINEDU = mINEDU;
	}

	public static String getREPORTE() {
		return REPORTE;
	}

	public static void setREPORTE(String rEPORTE) {
		REPORTE = rEPORTE;
	}

	public static String getFECHA_REPORTE() {
		return FECHA_REPORTE;
	}

	public static void setFECHA_REPORTE(String fECHA_REPORTE) {
		FECHA_REPORTE = fECHA_REPORTE;
	}

	public String getNombreReporte() {
		return nombreReporte;
	}

	public void setNombreReporte(String nombreReporte) {
		this.nombreReporte = nombreReporte;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

//	HSSFWorkbook wb = new HSSFWorkbook();
//	   2:   HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
//	   3:   HSSFClientAnchor anchor;
//	   4:   anchor = new HSSFClientAnchor(500,100,600,200,(short)0,0,(short)1,0);
//	   5:   anchor.setAnchorType( 2 );
//	   6:   HSSFPicture picture =  patriarch.createPicture(anchor, loadPicture(files, wb ));
	
	@SuppressWarnings({ "resource", "deprecation" })
	public void doExport(List<Object[]> dataList, String usuario) {
		//if (dataList != null && !dataList.isEmpty()) {
		if (dataList != null) {
			HSSFWorkbook workBook = new HSSFWorkbook();
			HSSFSheet sheet = workBook.createSheet();
			// Estilo
			HSSFCellStyle style = workBook.createCellStyle();
			HSSFFont font = workBook.createFont();
			font.setFontName(HSSFFont.FONT_ARIAL);
			//	font.setFontHeightInPoints((short) 10);
			font.setFontHeightInPoints((short) 8);
			font.setBold(true);
			style.setFont(font);
			// style.setFillPattern(CellStyle.ALIGN_FILL);
			style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
			style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
			style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
			style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);

			HSSFRow mineduRow = sheet.createRow(0);
			
			//sheet.setColumnWidth(0, 1000);
			
			mineduRow.createCell((short) 0).setCellValue(MINEDU);
			
//			for (int x = 0; x < sheet.getRow(0).getPhysicalNumberOfCells(); x++) {
//		        results.autoSizeColumn(x);
//			}
			
			
			//jl mineduRow.getCell(0).setCellStyle(style);
			HSSFRow tituloRow = sheet.createRow(1);
		//	tituloRow.createCell((short) 0).setCellValue(nombreReporte);
			String nameCreador=usuario;
			String [] nameVec=nameCreador.split(" ");
			String [] nameVecEnd;
			String nameVecEnd_ = "";
			String formatNameUser_="";
			String formatNameUser="";
			String formatNameUserEnd="";
			for(int x=0; x<nameVec.length; x++){
				formatNameUser=nameVec[x].substring(0,1).toUpperCase() + nameVec[x].substring(1).toLowerCase() ;
				
				formatNameUser=formatNameUser + " " + formatNameUser_ ;
				formatNameUser_=formatNameUser;
			}
			nameVecEnd=formatNameUser_.split(" ");
			int numNom=nameVecEnd.length;
			for(int  i=0;i<nameVecEnd.length;i++){
	
				if(numNom==3)formatNameUserEnd=nameVecEnd[nameVecEnd.length -1 ] + " "  +  nameVecEnd[nameVecEnd.length - 2] + " " + nameVecEnd[0];
				if(numNom==4)formatNameUserEnd=nameVecEnd[nameVecEnd.length -1 ] + " "  +  nameVecEnd[nameVecEnd.length - 2] + " " + nameVecEnd[nameVecEnd.length - 3] + " "  + nameVecEnd[0];
				if(numNom==5)formatNameUserEnd=nameVecEnd[nameVecEnd.length -1 ] + " "  +  nameVecEnd[nameVecEnd.length - 2] + " " + nameVecEnd[nameVecEnd.length - 3] + " " + nameVecEnd[nameVecEnd.length - 4] + " " + nameVecEnd[0];
				if(numNom==6)formatNameUserEnd=nameVecEnd[nameVecEnd.length -1 ] + " "  +  nameVecEnd[nameVecEnd.length - 2] + " " + nameVecEnd[nameVecEnd.length - 3] + " " + nameVecEnd[nameVecEnd.length - 4] + " " + nameVecEnd[nameVecEnd.length - 5] +  " " + nameVecEnd[0];
				
				
			}
			
			
			tituloRow.createCell((short) 0).setCellValue(USER_CREACION + formatNameUserEnd );
			//tituloRow.createCell((short) 0).setCellValue(USER_CREACION + nameVecEnd_ );
			//jl tituloRow.getCell(0).setCellStyle(style);
			HSSFRow fechaRow = sheet.createRow(2);
//			fechaRow.createCell((short) 0).setCellValue(FECHA_REPORTE + FechaUtil
//					.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "dd/MM/yyyy HH:mm:ss"));
			fechaRow.createCell((short) 0).setCellValue(FECHA_REPORTE + FechaUtil
					.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "dd/MM/yyyy"));
			//jl fechaRow.getCell(0).setCellStyle(style);
			
			HSSFRow horaRow = sheet.createRow(3);
			horaRow.createCell((short) 0).setCellValue(HORA_REPORTE + FechaUtil
					.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "HH:mm:ss"));
			//jl horaRow.getCell(0).setCellStyle(style);
			//////////////////////////////////////////////////////////////
			
			
			CellRangeAddress region = CellRangeAddress.valueOf("A1:C4");
			for(int i=region.getFirstRow();i<=region.getLastRow();i++){
			    Row row = sheet.getRow(i);

			    for(int j=region.getFirstColumn();j<region.getLastColumn();j++){
			        if(row.getCell(j) == null )
			        	row.createCell((short) j).setCellStyle(style);
			        else
			        	row.getCell(j).setCellStyle(style);
			    	
			    	//Cell cell = row.getCell(j);
			        //cell.setCellStyle(style);
			    }
			}
			
			sheet.addMergedRegion(CellRangeAddress.valueOf("A1:B1"));
			sheet.addMergedRegion(CellRangeAddress.valueOf("A2:B2"));
			sheet.addMergedRegion(CellRangeAddress.valueOf("A3:B3"));
			sheet.addMergedRegion(CellRangeAddress.valueOf("A4:B4"));
			
			
			InputStream inputStream;
			try {
			//	inputStream = new FileInputStream("logo.png");
				Properties propiedades = new Properties();
				propiedades.load(in);
				String pathFile = propiedades.getProperty("cabecera.reporte.excel.logo.ruta");
				in.close();
				
				FileInputStream input;
				File file = new File(pathFile.trim()+File.separator+"logo.jpg");
				if(file.exists()){
					input = new FileInputStream(pathFile.trim()+File.separator+"logo.jpg");
				}else{
					input = new FileInputStream(pathFile.trim()+File.separator+"NO_FILE.jpg");
				}
				inputStream=input;
//				String ruta=RUTA_TEMPLATES.append(properties.getProperty("cabecera.reporte.excel.logo.ruta")).toString();
//			      inputStream = new BufferedInputStream(new FileInputStream(ruta+"logo.png"));
			//in
				setimagen(workBook, sheet, nombreReporte,inputStream );
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//////////////////////////////////////////////

			HSSFRow headingRow = sheet.createRow(8);

			int i = 0;
			for (String cabecera : listaExportarCabecera) {
				headingRow.createCell((short) i).setCellValue(cabecera.toString());
				headingRow.getCell(i).setCellStyle(style);
				i++;
			}

			int z = 8; // La fila siguiente a la lista de cabecera
			HSSFRow row;
			HSSFCell cell;
			for (Object[] obj : dataList) {
				row = sheet.createRow(z + 1);
				for (int y = 0; y < obj.length; y++) {
					cell = row.createCell((short) y);
					if (obj[y] instanceof Date)
						cell.setCellValue(
								FechaUtil.obtenerFechaFormatoPersonalizado((Date) obj[y], "dd/MM/yyyy HH:mm:ss"));
					else if (obj[y] instanceof Boolean)
						cell.setCellValue((Boolean) obj[y]);
					else if (obj[y] instanceof Double)
						cell.setCellValue((Double) obj[y]);
					else if (obj[y] instanceof Integer)
						cell.setCellValue((Integer) obj[y]);
					else if (obj[y] == null)
						cell.setCellValue("");
					else
						cell.setCellValue("" + obj[y]);
				}
				z++;
			}

			if (dataList.size()<50000) {
				for (int r = 0; r < 50; r++) {
					sheet.autoSizeColumn(r);
				}
			}
		

			// String file = "D:/Files/exportarTabla.xls";

			try {
				// FileOutputStream fos = new FileOutputStream(file);
				ServletOutputStream stream = response.getOutputStream();
				workBook.write(stream);
				// fos.flush();
				// workBook.write(response.getOutputStream());
				
				response.getOutputStream().flush();
				// fos.close();
//				response.getOutputStream().close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.out.println("directorio invalido");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error al crear el archivo");
			}

		}
	}
	

	
	public void setimagen(HSSFWorkbook workbook, HSSFSheet sheet, String name, InputStream file) {
        try {
            Font whiteFont = workbook.createFont();
            whiteFont.setColor(IndexedColors.BLUE.index);
           // whiteFont.setFontHeightInPoints((short) 20.00);
            whiteFont.setFontHeightInPoints((short) 9.00);
            whiteFont.setBold(true);
            
        HSSFCellStyle cellheader = workbook.createCellStyle();
        cellheader.setAlignment(HSSFCellStyle.ALIGN_LEFT);
        cellheader.setWrapText(true);
        cellheader.setFont(whiteFont);

//        //NOMBRE DEL REPORTE   //HSSFRow
//        HSSFRow row = sheet.createRow(4);
//        row.setHeightInPoints((float) 29.00);
//        Cell cell = row.createCell((short) 4);
//        sheet.addMergedRegion(CellRangeAddress.valueOf("$E$2:$G$2"));
        HSSFRow row = sheet.createRow(4);
        row.setHeightInPoints((float) 29.00);
        Cell cell = row.createCell((short) 4);
        
     //   sheet.addMergedRegion(CellRangeAddress.valueOf("$D$4:$F$4"));
        
        cell.setCellValue(new HSSFRichTextString(name.toUpperCase()));
        
      //  cell.setCellStyle(cellheader);
        
               
        
        //Get the contents of an InputStream as a byte[].
        byte[] bytes = IOUtils.toByteArray(file);
        //Adds a picture to the workbook
        int pictureIdx = workbook.addPicture(bytes, workbook.PICTURE_TYPE_JPEG ); //workbook.PICTURE_TYPE_PNG
        //close the input stream
        //Returns an object that handles instantiating concrete classes
        CreationHelper helper = workbook.getCreationHelper();
        //Creates the top-level drawing patriarch.
        Drawing drawing = sheet.createDrawingPatriarch();
        //Create an anchor that is attached to the worksheet
        //ClientAnchor anchor = helper.createClientAnchor();
        ClientAnchor anchor = helper.createClientAnchor();
        //set top-left corner for the image
//        anchor.setDx1(1023);
//        anchor.setDy1(0);
        //anchor.setDx2(1023);
//        anchor.setDx2(0);
//        anchor.setDy2(0);
        anchor.setCol1(9);
        anchor.setRow1(0);
//        anchor.setCol2(3);
//        anchor.setRow2(1);
        //Creates a picture
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        //Reset the image to the original size
        pict.resize(3d);
        //sheet.createFreezePane(0, 1, 0, 1);
        sheet.createFreezePane(0, 9);
    } catch (Exception ex) {
           ex.printStackTrace();
    }

}
	
	
}