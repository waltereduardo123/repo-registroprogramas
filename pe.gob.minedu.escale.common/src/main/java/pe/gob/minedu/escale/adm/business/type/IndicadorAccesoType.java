package pe.gob.minedu.escale.adm.business.type;

public enum IndicadorAccesoType {
	USER_HOST_BROWSE_IGUALES("0"),
	BROWSE_DIFERENTES("1"),
	HOST_DIFERENTE("2"),
	HOST_BROWSE_DEFIRENTE("3"),
	ROW_PIRATA_TWO("4");

	/** EL value. */
	private String value;

	private IndicadorAccesoType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	
	
	
}
