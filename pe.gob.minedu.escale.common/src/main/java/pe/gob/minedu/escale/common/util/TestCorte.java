package pe.gob.minedu.escale.common.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestCorte {

	
	
	public static void main(String[] args) {
//	FechaUtil futil = null;
//	 
//	
//	String fini1="18-09-2018";
//	SimpleDateFormat sdfi1=new SimpleDateFormat("dd-MM-yyyy");
//	String ffin1="18-10-2018";
//	SimpleDateFormat sdff1=new SimpleDateFormat("dd-MM-yyyy");
//	String fini2="19-08-2018";
//	SimpleDateFormat sdfi2=new SimpleDateFormat("dd-MM-yyyy");
//	String ffin2="18-09-2018";
//	SimpleDateFormat sdff2=new SimpleDateFormat("dd-MM-yyyy");
//	try {
//	//	System.out.println("COMPARACION DE FECHA-<" +fini2.compareTo(ffin1) );
//		System.out.println("VERIFICA NO SOLAPAMIENTO--> " + verificaNoSolapamiento( sdfi1.parse(fini1),
//				sdff1.parse(ffin1),sdfi2.parse(fini2),sdff2.parse(ffin2) ));
//		
//	} catch (ParseException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}

	
		Date fecha=sumarMinutosCalendar(new Date(),20);
		
		System.out.println("fecha cambiada :--> " + fecha );
		
    }
	
	 public static Date sumarMinutosCalendar(Date date, int minuto) {
	     Date fechFormated;   
		 if (date == null) {
	                return null;
	        } else {
	                Calendar cale = Calendar.getInstance();
	                cale.setTime(date);
	                cale.add(Calendar.HOUR, 0);
	                cale.add(Calendar.MINUTE, minuto);
	                cale.add(Calendar.SECOND, 0);
	                cale.add(Calendar.MILLISECOND, 0);
	  
	                fechFormated=cale.getTime();
	                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	                Date fecFormat = new Date();
					try {
						fecFormat = dt.parse(dt.format(fechFormated));
						System.out.println("fecha x  " + fecFormat );
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                
	                
	                return fecFormat;//cale.getTime();
	        }
	
	 
	 }
/*
 * Metodo que obtiene el tamaño del bloque. Resta las dos fechas involucradas
 * 
 * */

	public static int getTamanoRango(Date fechaInicio, Date fechaFin){
		FechaUtil futil = null;
		int numDiasRango = 0;
		try{
			numDiasRango=futil.restarFechas(fechaInicio, fechaFin);
		}catch(Exception e){
			
		}
		
			return numDiasRango;	
	}

/*
 * Metodo que verifica si existe solapamiento entre dos periodos de fechas.
 * Return true si no existe solapamiento
 */
	public static boolean verificaNoSolapamiento(Date fechaInicio,Date fechaFin,Date fechaInicioPost, Date fechaFinPost){
		int tamPeriodoOne=0;
		int tamPeriodoTwo=0;
		boolean isOk=false;
		boolean isOkBloquePost=false;
		try{
		
			//1. Obtener tamaños de rango
			tamPeriodoOne=getTamanoRango(fechaInicio, fechaFin);
			tamPeriodoTwo=getTamanoRango(fechaInicioPost, fechaFinPost);
			//2. verificar si bloque dos esta despues de bloque uno
			if(fechaInicioPost.after(fechaFin)&&(fechaInicioPost.compareTo(fechaFin)>0||fechaInicioPost.compareTo(fechaFin)<0)){ 
				System.out.println("PRIMER FILTRO OK");
				isOk=true;
				isOkBloquePost=false;
			}else{
				System.out.println("PRIMER FILTRO PELIGRO");
				isOk=false;
				isOkBloquePost=true;
			}
			//3. verificar si fecha de inicio de bloque 2 esta antes de fecha fin de bloque 1
			if(isOkBloquePost){
				System.out.println("LA FECHA DE INICIO DEL BLOQUE DOS, ESTA ANTES DE LA FECHA DE FIN DEL BLOQUE UNO");
				if(fechaInicioPost.before(fechaFin)){
				System.out.println("ENTRAMOS EN TRUE");
				//La fecha de fin del bloque dos, debe de estar antes de la fecha de inicio del bloque uno
						if(fechaFinPost.before(fechaInicio)&&(fechaFinPost.compareTo(fechaInicio)>0||fechaFinPost.compareTo(fechaInicio)<0) ){
							isOk=true;
						}else{
							System.out.println("ENTRAMOS EN FALSE POR FECHA FIN BLOQUE DOS IGUAL A FECHA INICIO BLOQUE UNO");
							isOk=false;
						}
				
//					isOk=true;
				
				}else{
					System.out.println("ENTRAMOS EN FALSE");
					isOk=false;
				}
			}	
				
			
		}catch(Exception e){
			
		}
		
		
		
		return isOk;
	}
	
	
}
