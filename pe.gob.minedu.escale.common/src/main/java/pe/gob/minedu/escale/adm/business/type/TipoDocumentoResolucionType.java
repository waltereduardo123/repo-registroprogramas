package pe.gob.minedu.escale.adm.business.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import pe.gob.minedu.escale.adm.vo.SelectVO;
import pe.gob.minedu.escale.common.util.ResourceUtil;

public enum TipoDocumentoResolucionType {

	CODIGO_AGRUPACION("7", "cnf.tipodocumentoresolucion.type.codigoAgrupacion"),
    
    /** Corresponde a la constante que determina el nombre del tipo de resolucion. */
    NOMBRE("RESMIN", "cnf.tipodocumentoresolucion.type.nombre"),
    

    /** Corresponde a la constante que determina a la descripcion del tipo de resolucion. */
    DESCRIPCION("DES", "cnf.tipodocumentoresolucion.type.descrip"),
	
	OTROUE("ODUE", "cnf.tipodocumentoresolucion.type.otroue");

    /** La Constante BUNDLE_NAME. */
    private final static String BUNDLE_NAME = "pe.gob.minedu.escale.resources.escale-adm-type";
    
    /** La Constante list. */
    private static final List<TipoDocumentoResolucionType> list = new ArrayList<TipoDocumentoResolucionType>();

    static {
        for (TipoDocumentoResolucionType s : EnumSet.allOf(TipoDocumentoResolucionType.class)) {
            list.add(s);
        }
    }
    

    
    
    /** El key. */
    private String key;
    
    /** EL value. */
    private String value;

    /**
     * Instancia un nuevo tipo documento type.
     *
     * @param key el key
     * @param value el value
     */
    private TipoDocumentoResolucionType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Obtiene key.
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Obtiene value.
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * Obtiene description.
     *
     * @param locale Par&aacute;metro de tipo Locale que determina la localidad.
     * @return Retorna un valor de tipo String con la descripci&oacute;n del
     * estado.
     */
    public String getDescription(Locale locale) {
            return ResourceUtil.getString(locale, BUNDLE_NAME, this.getValue());
    }

    /**
     * Obtiene list.
     *
     * @param locale el locale
     * @return list
     */
    public static List<SelectVO> getList(Locale locale) {
        List<SelectVO> rList = new ArrayList<SelectVO>();
        for (TipoDocumentoResolucionType s : list) {
            SelectVO select = new SelectVO();
            select.setId(s.getKey());
            select.setValue(ResourceUtil.getString(locale, BUNDLE_NAME, s.getValue()));
            rList.add(select);
        }

        return rList;
    }
	
	
}
