package pe.gob.minedu.escale.regprograma.business.exception;

import pe.gob.minedu.escale.common.exception.BusinessException;

public class PeriodoNoRegistradoException extends BusinessException {

    /** La Constante serialVersionUID. */
    private static final long serialVersionUID = -3706220390881034565L;
    
    /** La Constante ERROR_KEY. */
    private static final String ERROR_KEY = PeriodoNoRegistradoException.class.getName();

    
    public PeriodoNoRegistradoException() {
        super(ERROR_KEY);
    }
    
    public PeriodoNoRegistradoException(String parameter) {
        super(ERROR_KEY, parameter);
    }
	
}
