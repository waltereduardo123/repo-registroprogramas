package pe.gob.minedu.escale.common.dto.rest;

public class ControlAvanceFilter {
	private String codoii;
	private String tipo;
	private String periodo;
	
	
	
	public String getCodoii() {
		return codoii;
	}
	public void setCodoii(String codoii) {
		this.codoii = codoii;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	
	
	
}
