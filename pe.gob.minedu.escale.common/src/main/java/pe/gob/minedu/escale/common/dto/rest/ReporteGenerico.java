package pe.gob.minedu.escale.common.dto.rest;

import java.util.List;

import pe.gob.minedu.escale.common.dto.rest.ConsultaJson;

public class ReporteGenerico {

	private ConsultaJson consultaJson;
	private String nombreReporte;
	private String usuarioCreador;
	private String actor;
	private String chapter;
	private String main;
	
	
	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	private List<String> listaCabecera;

	public ReporteGenerico() {

	}

	public ReporteGenerico(ConsultaJson consultaJson, String nombreReporte, List<String> listaCabecera) {
		this.consultaJson = consultaJson;
		this.nombreReporte = nombreReporte;
		this.listaCabecera = listaCabecera;
	}
	
	
	
	public ReporteGenerico(ConsultaJson consultaJson, String nombreReporte, String usuarioCreador, String actor,
			String chapter, String main, List<String> listaCabecera) {
		super();
		this.consultaJson = consultaJson;
		this.nombreReporte = nombreReporte;
		this.usuarioCreador = usuarioCreador;
		this.actor = actor;
		this.chapter = chapter;
		this.main = main;
		this.listaCabecera = listaCabecera;
	}

	public ConsultaJson getConsultaJson() {
		return consultaJson;
	}

	public void setConsultaJson(ConsultaJson consultaJson) {
		this.consultaJson = consultaJson;
	}

	public String getNombreReporte() {
		return nombreReporte;
	}

	public void setNombreReporte(String nombreReporte) {
		this.nombreReporte = nombreReporte;
	}

	public List<String> getListaCabecera() {
		return listaCabecera;
	}

	public void setListaCabecera(List<String> listaCabecera) {
		this.listaCabecera = listaCabecera;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getChapter() {
		return chapter;
	}

	public void setChapter(String chapter) {
		this.chapter = chapter;
	}

	
}
