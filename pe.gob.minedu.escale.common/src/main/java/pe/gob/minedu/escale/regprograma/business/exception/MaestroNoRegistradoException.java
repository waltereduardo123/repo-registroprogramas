package pe.gob.minedu.escale.regprograma.business.exception;

import pe.gob.minedu.escale.common.exception.BusinessException;

public class MaestroNoRegistradoException extends BusinessException {

	   /** La Constante serialVersionUID. */
    private static final long serialVersionUID = -3706220390881034565L;
    
    /** La Constante ERROR_KEY. */
    private static final String ERROR_KEY = MaestroNoRegistradoException.class.getName();

    
    public MaestroNoRegistradoException() {
        super(ERROR_KEY);
    }
    
    public MaestroNoRegistradoException(String parameter) {
        super(ERROR_KEY, parameter);
    }
	
}
