package pe.gob.minedu.escale.common.dto.rest;

public class TipoPeriodoFilter {
	private String namePeriodo;
	private String fecIni;
	private String fecFin;
	private String nameUser;
	
	
	
	public String getNamePeriodo() {
		return namePeriodo;
	}
	public void setNamePeriodo(String namePeriodo) {
		this.namePeriodo = namePeriodo;
	}
	public String getFecIni() {
		return fecIni;
	}
	public void setFecIni(String fecIni) {
		this.fecIni = fecIni;
	}
	public String getFecFin() {
		return fecFin;
	}
	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}
	public String getNameUser() {
		return nameUser;
	}
	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}
	
	
	
}
