package pe.gob.minedu.escale.regprograma.business.exception;

import pe.gob.minedu.escale.common.exception.BusinessException;

public class ProgramaNoSincronizadoException extends BusinessException {
	
    /** La Constante serialVersionUID. */
    private static final long serialVersionUID = -3706220390788034544L;
    
    /** La Constante ERROR_KEY. */
    private static final String ERROR_KEY = ProgramaNoSincronizadoException.class.getName();

    
    public ProgramaNoSincronizadoException() {
        super(ERROR_KEY);
    }
    
    public ProgramaNoSincronizadoException(String parameter) {
        super(ERROR_KEY, parameter);
    }

}
