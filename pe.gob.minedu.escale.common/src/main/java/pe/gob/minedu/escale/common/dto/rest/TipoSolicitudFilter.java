package pe.gob.minedu.escale.common.dto.rest;

import java.util.List;

public class TipoSolicitudFilter {
	private String codSolicitud;
	private String tipoAccion;
	private String tipoSolicitud;
	
	///Revision de la solicitud
	private String nomUsuario;
	private String dniUsuario;
	private String perfilUsuario;
	
	//Aprobar la solicitud
	private String obsSolicitud;
	private String codArea;
	private String codCPoblado;
	private String codAreaSig;
	 
	//Deshacer Observacion
	private String ctrBoton;
	
	//Seguridad
	private String      isIntruso;
    private String		islegal;
    
    //alterar cadenas
    private String inexus;
    private String jodexus;
	
    private String init; // privilegio que le corresponde al perfil del usuario
    
	public String getInit() {
		return init;
	}
	public void setInit(String init) {
		this.init = init;
	}
	public TipoSolicitudFilter(String codSolicitud, String tipoAccion, String tipoSolicitud, String nomUsuario,
			String dniUsuario, String perfilUsuario, String obsSolicitud, String codArea, String codCPoblado,
			String codAreaSig, String ctrBoton, String isIntruso, String islegal, String inexus, String jodexus) {
		super();
		this.codSolicitud = codSolicitud;
		this.tipoAccion = tipoAccion;
		this.tipoSolicitud = tipoSolicitud;
		this.nomUsuario = nomUsuario;
		this.dniUsuario = dniUsuario;
		this.perfilUsuario = perfilUsuario;
		this.obsSolicitud = obsSolicitud;
		this.codArea = codArea;
		this.codCPoblado = codCPoblado;
		this.codAreaSig = codAreaSig;
		this.ctrBoton = ctrBoton;
		this.isIntruso = isIntruso;
		this.islegal = islegal;
		this.inexus = inexus;
		this.jodexus = jodexus;
	}
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}
	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	public TipoSolicitudFilter() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCodSolicitud() {
		return codSolicitud;
	}
	public void setCodSolicitud(String codSolicitud) {
		this.codSolicitud = codSolicitud;
	}
	public String getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getDniUsuario() {
		return dniUsuario;
	}
	public void setDniUsuario(String dniUsuario) {
		this.dniUsuario = dniUsuario;
	}
	public String getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	public String getObsSolicitud() {
		return obsSolicitud;
	}
	public void setObsSolicitud(String obsSolicitud) {
		this.obsSolicitud = obsSolicitud;
	}
	public String getCodArea() {
		return codArea;
	}
	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}
	public String getCodCPoblado() {
		return codCPoblado;
	}
	public void setCodCPoblado(String codCPoblado) {
		this.codCPoblado = codCPoblado;
	}
	public String getCodAreaSig() {
		return codAreaSig;
	}
	public void setCodAreaSig(String codAreaSig) {
		this.codAreaSig = codAreaSig;
	}
	public String getCtrBoton() {
		return ctrBoton;
	}
	public void setCtrBoton(String ctrBoton) {
		this.ctrBoton = ctrBoton;
	}
	public String getIsIntruso() {
		return isIntruso;
	}
	public void setIsIntruso(String isIntruso) {
		this.isIntruso = isIntruso;
	}
	public String getIslegal() {
		return islegal;
	}
	public void setIslegal(String islegal) {
		this.islegal = islegal;
	}
	public String getInexus() {
		return inexus;
	}
	public void setInexus(String inexus) {
		this.inexus = inexus;
	}
	public String getJodexus() {
		return jodexus;
	}
	public void setJodexus(String jodexus) {
		this.jodexus = jodexus;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codArea == null) ? 0 : codArea.hashCode());
		result = prime * result + ((codAreaSig == null) ? 0 : codAreaSig.hashCode());
		result = prime * result + ((codCPoblado == null) ? 0 : codCPoblado.hashCode());
		result = prime * result + ((codSolicitud == null) ? 0 : codSolicitud.hashCode());
		result = prime * result + ((ctrBoton == null) ? 0 : ctrBoton.hashCode());
		result = prime * result + ((dniUsuario == null) ? 0 : dniUsuario.hashCode());
		result = prime * result + ((inexus == null) ? 0 : inexus.hashCode());
		result = prime * result + ((init == null) ? 0 : init.hashCode());
		result = prime * result + ((isIntruso == null) ? 0 : isIntruso.hashCode());
		result = prime * result + ((islegal == null) ? 0 : islegal.hashCode());
		result = prime * result + ((jodexus == null) ? 0 : jodexus.hashCode());
		result = prime * result + ((nomUsuario == null) ? 0 : nomUsuario.hashCode());
		result = prime * result + ((obsSolicitud == null) ? 0 : obsSolicitud.hashCode());
		result = prime * result + ((perfilUsuario == null) ? 0 : perfilUsuario.hashCode());
		result = prime * result + ((tipoAccion == null) ? 0 : tipoAccion.hashCode());
		result = prime * result + ((tipoSolicitud == null) ? 0 : tipoSolicitud.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoSolicitudFilter other = (TipoSolicitudFilter) obj;
		if (codArea == null) {
			if (other.codArea != null)
				return false;
		} else if (!codArea.equals(other.codArea))
			return false;
		if (codAreaSig == null) {
			if (other.codAreaSig != null)
				return false;
		} else if (!codAreaSig.equals(other.codAreaSig))
			return false;
		if (codCPoblado == null) {
			if (other.codCPoblado != null)
				return false;
		} else if (!codCPoblado.equals(other.codCPoblado))
			return false;
		if (codSolicitud == null) {
			if (other.codSolicitud != null)
				return false;
		} else if (!codSolicitud.equals(other.codSolicitud))
			return false;
		if (ctrBoton == null) {
			if (other.ctrBoton != null)
				return false;
		} else if (!ctrBoton.equals(other.ctrBoton))
			return false;
		if (dniUsuario == null) {
			if (other.dniUsuario != null)
				return false;
		} else if (!dniUsuario.equals(other.dniUsuario))
			return false;
		if (inexus == null) {
			if (other.inexus != null)
				return false;
		} else if (!inexus.equals(other.inexus))
			return false;
		if (init == null) {
			if (other.init != null)
				return false;
		} else if (!init.equals(other.init))
			return false;
		if (isIntruso == null) {
			if (other.isIntruso != null)
				return false;
		} else if (!isIntruso.equals(other.isIntruso))
			return false;
		if (islegal == null) {
			if (other.islegal != null)
				return false;
		} else if (!islegal.equals(other.islegal))
			return false;
		if (jodexus == null) {
			if (other.jodexus != null)
				return false;
		} else if (!jodexus.equals(other.jodexus))
			return false;
		if (nomUsuario == null) {
			if (other.nomUsuario != null)
				return false;
		} else if (!nomUsuario.equals(other.nomUsuario))
			return false;
		if (obsSolicitud == null) {
			if (other.obsSolicitud != null)
				return false;
		} else if (!obsSolicitud.equals(other.obsSolicitud))
			return false;
		if (perfilUsuario == null) {
			if (other.perfilUsuario != null)
				return false;
		} else if (!perfilUsuario.equals(other.perfilUsuario))
			return false;
		if (tipoAccion == null) {
			if (other.tipoAccion != null)
				return false;
		} else if (!tipoAccion.equals(other.tipoAccion))
			return false;
		if (tipoSolicitud == null) {
			if (other.tipoSolicitud != null)
				return false;
		} else if (!tipoSolicitud.equals(other.tipoSolicitud))
			return false;
		return true;
	}
	
	
	
	
}
