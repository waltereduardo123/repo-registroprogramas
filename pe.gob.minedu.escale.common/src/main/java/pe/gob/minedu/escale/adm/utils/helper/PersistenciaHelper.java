package pe.gob.minedu.escale.adm.utils.helper;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.minedu.escale.common.util.LogUtil;


public class PersistenciaHelper {
	private static final Logger LOG = LoggerFactory.getLogger(PersistenciaHelper.class);
  

    /**
     * Obtiene single result.
     *
     * @param <T> el tipo generico
     * @param clazz el clazz
     * @param query el query
     * @return single result
     */
    @SuppressWarnings("unchecked")
    public static final <T>T getSingleResult(Class<T> clazz, Query query) {
            try {
                    return (T)query.getSingleResult();
            } catch (NoResultException e) {
                    return null;	
            }catch (Exception e) {
            			LOG.error("Error producido :" + e);
                       return null;
              }
    }

}
