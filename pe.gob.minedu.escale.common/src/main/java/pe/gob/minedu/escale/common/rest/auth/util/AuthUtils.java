package pe.gob.minedu.escale.common.rest.auth.util;


import java.text.ParseException;
import java.util.Map;


import org.joda.time.DateTime;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import pe.gob.minedu.escale.common.util.FechaUtil;

public final class AuthUtils { 

	private static final JWSHeader JWT_HEADER = new JWSHeader(JWSAlgorithm.HS256);
	private static final JWSHeader JWT_HEADER_AUTORIZED = new JWSHeader(JWSAlgorithm.HS256);
	private static final JWSHeader JWT_HEADER_REQUEST = new JWSHeader(JWSAlgorithm.HS256);
	private static final String TOKEN_SECRET = "adsiinwonderlandadsiinwonderland";
//	private static final String TOKEN_SECRET_AUTORIZED = "adsiinwonderlandadsiinwonderlond";
//	private static final String TOKEN_SECRET_REQUEST = "adsiinwonderlandadsiinwonderlonf";
	public static final String AUTH_HEADER_KEY = "Authorization";
		
	

	
	public static String getSubject(String authHeader) throws ParseException, JOSEException {
		return decodeToken(authHeader).getSubject();
	}

	public static JWTClaimsSet decodeToken(String authHeader) throws ParseException, JOSEException {
		SignedJWT signedJWT = SignedJWT.parse(getSerializedToken(authHeader));
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
			return signedJWT.getJWTClaimsSet();
		} else {
			throw new JOSEException("Verificación de firma fallida");
		}
	}

	public static Boolean validarToken(String authHeader) throws ParseException, JOSEException{
		SignedJWT signedJWT = SignedJWT.parse(authHeader);
//		JWTClaimsSet claimSet= signedJWT.getJWTClaimsSet();
		
		Boolean respuesta;
//		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET)) && new DateTime(claimSet.getExpirationTime()).isAfter(DateTime.now())) {
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
			respuesta = true;
		} else {
			respuesta = false;
		}		
		return respuesta;
	}
	
	public static Boolean validarTokenMaster(String authHeader) throws ParseException, JOSEException{
		SignedJWT signedJWT = SignedJWT.parse(authHeader);
		JWTClaimsSet claimSet= signedJWT.getJWTClaimsSet();
		
		Boolean respuesta;
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET)) && new DateTime(claimSet.getExpirationTime()).isAfter(DateTime.now())) {
			respuesta = true;
		} else {
			respuesta = false;
		}		
		return respuesta;
	}
	

	
	public static Boolean validarTokenAutorized(String authHeader) throws ParseException, JOSEException{
		SignedJWT signedJWT = SignedJWT.parse(authHeader);
		Boolean respuesta;
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
			respuesta = true;
		} else {
			respuesta = false;
		}		
		return respuesta;
	}
	//////////////////////////////////
	public static Boolean validarTokenRequest(String authHeader) throws ParseException, JOSEException{
		SignedJWT signedJWT = SignedJWT.parse(authHeader);
		
		JWTClaimsSet claimSet= signedJWT.getJWTClaimsSet();
	
		Boolean respuesta;
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET)) && new DateTime(claimSet.getExpirationTime()).isAfter(DateTime.now())) {
		//if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
			respuesta = true;
		} else {
			respuesta = false;
		}		
		return respuesta;
	}
	
	public static Boolean validarTokenPdf(String authHeader) throws ParseException, JOSEException{
		SignedJWT signedJWT = SignedJWT.parse(authHeader);
		//JWTClaimsSet claimSet= signedJWT.getJWTClaimsSet();
		Boolean respuesta;

		//walter
		//if (signedJWT.verify(new MACVerifier(TOKEN_SECRET)) && new DateTime(claimSet.getExpirationTime()).isAfter(DateTime.now())) {
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
			respuesta = true;
		} else {
			respuesta = false;
		}	
				
		return respuesta;
	}
	
	public static Boolean validarTokenFormulario(String authHeader) throws ParseException, JOSEException{
		SignedJWT signedJWT = SignedJWT.parse(authHeader);
		JWTClaimsSet claimSet= signedJWT.getJWTClaimsSet();
		Boolean respuesta = false;
		
		boolean x=signedJWT.verify(new MACVerifier(TOKEN_SECRET));
		boolean y=new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now());
		
		System.out.println("DATOS A VALDAR : "  +   x + " ///  " + y  );
		
		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET)) && new DateTime(claimSet.getExpirationTime()).isAfter(DateTime.now() )
				) 
		{
//		if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
			respuesta = true; //////////////
		}
		else if(signedJWT.verify(new MACVerifier(TOKEN_SECRET)) && new DateTime(claimSet.getExpirationTime()).isBefore(80000)) {
			respuesta=false;
		} 
//		else {
//			respuesta = false; //////////////
//		}	
		return respuesta;
	}
	
	public static Token createToken(String host, Map<Object, Object> parametros) throws JOSEException {
		JWTClaimsSet claim = new JWTClaimsSet.Builder()
				.subject(String.valueOf(parametros.get("CodigoOID")))
				.issuer(host)
				.issueTime(FechaUtil.obtenerFechaActual())
//				.expirationTime(DateTime.now().plusMinutes(1).toDate())
				.expirationTime(DateTime.now().plusHours(2).toDate())
//				.expirationTime(DateTime.now().plusDays(1).toDate())
				.claim("Usuario", parametros.get("Usuario"))
				.claim("Organismos", parametros.get("Organismos"))
				.claim("UsuarioRol", parametros.get("UsuarioRol"))
				.claim("Perfiles", parametros.get("Perfiles"))
				.claim("Roles", parametros.get("Roles"))
				.claim("Privilegios", parametros.get("Privilegios"))
				.claim("PrivilegioRol", parametros.get("PrivilegioRol"))
				.claim("HostRemoto", parametros.get("HostRemoto"))
				.claim("Browser", parametros.get("Browser"))
				//imendoza 20170303 inicio
				.claim("Mensajes", parametros.get("Mensajes"))
				//imendoza 20170303 fin  
				.build();

		JWSSigner signer = new MACSigner(TOKEN_SECRET);
		SignedJWT jwt = new SignedJWT(JWT_HEADER, claim);
		jwt.sign(signer);

		return new Token(jwt.serialize());
	}
	/////////////////////////////////////////////////////////////////////////////////
	public static Token createTokenValPeticion(String host, Map<Object, Object> parametros) throws JOSEException {
		JWTClaimsSet claim = new JWTClaimsSet.Builder()
				//.subject(String.valueOf(parametros.get("codid")))
				.subject(String.valueOf(parametros.get("idCodoId")))
				.subject(String.valueOf(parametros.get("perfil")))
				.issuer(host)
				.issueTime(FechaUtil.obtenerFechaActual())
				.expirationTime(DateTime.now().plusSeconds(45).toDate())// actual
				//.expirationTime(DateTime.now().plusSeconds(300).toDate())
				.claim("Mensajes", parametros.get("Mensajes"))
				.build();
	

		JWSSigner signer = new MACSigner(TOKEN_SECRET);
		SignedJWT jwt = new SignedJWT(JWT_HEADER_REQUEST, claim);
		jwt.sign(signer);
		
		return new Token(jwt.serialize());
	}
	public static Token createTokenValPDFPeticion(String host, Map<Object, Object> parametros) throws JOSEException {
		JWTClaimsSet claim = new JWTClaimsSet.Builder()
				.subject(String.valueOf(parametros.get("CodigoOID")))
				.issuer(host)
				.issueTime(FechaUtil.obtenerFechaActual())
				//.expirationTime(DateTime.now().plusSeconds(30).toDate())//actual
				.expirationTime(DateTime.now().plusSeconds(45).toDate()) 
				//.expirationTime(DateTime.now().plusMinutes(2).toDate())
				.claim("Mensajes", parametros.get("Mensajes"))
				.build();
	

		JWSSigner signer = new MACSigner(TOKEN_SECRET);
		SignedJWT jwt = new SignedJWT(JWT_HEADER_REQUEST, claim);
		jwt.sign(signer);
		
		return new Token(jwt.serialize());
	}
	public static Token createTokenValFormularios(String host, Map<Object, Object> parametros) throws JOSEException {
		JWTClaimsSet claim = new JWTClaimsSet.Builder()
				.subject(String.valueOf(parametros.get("CodigoOID")))
				.issuer(host)
				.issueTime(FechaUtil.obtenerFechaActual())
				.expirationTime(DateTime.now().plusMillis(80000).toDate()) 
				.claim("Mensajes", parametros.get("Mensajes"))
				.build();
	
		JWSSigner signer = new MACSigner(TOKEN_SECRET);
		SignedJWT jwt = new SignedJWT(JWT_HEADER_REQUEST, claim);
		jwt.sign(signer);
		
		return new Token(jwt.serialize());
	}
	
	
	public static Token createTokenSesionMaster(String host, Map<Object, Object> parametros) throws JOSEException {
		JWTClaimsSet claim = new JWTClaimsSet.Builder()
				.subject(String.valueOf(parametros.get("CodigoOID")))
				.issuer(host)
				.issueTime(FechaUtil.obtenerFechaActual())
				.expirationTime(DateTime.now().plusHours(4).toDate())////////// 
//				.expirationTime(DateTime.now().plusMinutes(10).toDate())
				.claim("HostRemoto", parametros.get("HostRemoto"))
				.claim("Browser", parametros.get("Browser"))
				.claim("Mensajes", parametros.get("Mensajes"))
				.build();
	

		JWSSigner signer = new MACSigner(TOKEN_SECRET);
		SignedJWT jwt = new SignedJWT(JWT_HEADER_REQUEST, claim);
		jwt.sign(signer);
		
		return new Token(jwt.serialize());
	}
	
	
	
	
	public static Token createTokenAutorized(String host, Map<Object, Object> parametros) throws JOSEException {
		JWTClaimsSet claim = new JWTClaimsSet.Builder()
				.subject(String.valueOf(parametros.get("CodigoOID")))
				.issuer(host)
				.issueTime(FechaUtil.obtenerFechaActual())
				.expirationTime(DateTime.now().plusHours(8).toDate())
				.claim("Mensajes", parametros.get("Mensajes"))
				.build();
		
		
		JWSSigner signer = new MACSigner(TOKEN_SECRET);
		SignedJWT jwt = new SignedJWT(JWT_HEADER_AUTORIZED, claim);
		jwt.sign(signer);
		
		return new Token(jwt.serialize());
	}
	

	
	public static String getSerializedToken(String authHeader) {
		return authHeader.split(" ")[1];
	}
}
