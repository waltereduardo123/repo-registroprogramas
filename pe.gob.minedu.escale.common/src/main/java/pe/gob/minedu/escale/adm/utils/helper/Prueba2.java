package pe.gob.minedu.escale.adm.utils.helper;

public class Prueba2 {
	private String nombre;
	private String telefono;
	
	public Prueba2() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "Prueba2 [nombre=" + nombre + ", telefono=" + telefono + "]";
	}
	
	
}
