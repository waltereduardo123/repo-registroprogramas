package pe.gob.minedu.escale.common.dto.rest;

public class ServletDTO {

	private String nombrePrograma;
	private String tipoPrograma;
	
	
	
	
	public ServletDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getNombrePrograma() {
		return nombrePrograma;
	}
	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}
	public String getTipoPrograma() {
		return tipoPrograma;
	}
	public void setTipoPrograma(String tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}
	
	
	
}
