package pe.gob.minedu.escale.common.rest.auth.util;

public class Token {

	String token;

	public Token(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}
}