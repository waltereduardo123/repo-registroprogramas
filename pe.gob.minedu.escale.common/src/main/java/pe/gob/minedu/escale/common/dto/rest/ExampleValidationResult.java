package pe.gob.minedu.escale.common.dto.rest;

public class ExampleValidationResult {
	
	private Boolean success;
	private Integer indel;
	
	
	
	public ExampleValidationResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExampleValidationResult(Boolean success) {
		super();
		this.success = success;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Integer getIndel() {
		return indel;
	}

	public void setIndel(Integer indel) {
		this.indel = indel;
	}
	
	
		
}
