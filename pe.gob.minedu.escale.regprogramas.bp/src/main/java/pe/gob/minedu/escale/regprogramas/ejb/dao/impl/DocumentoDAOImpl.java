package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.DocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;

@Stateless
public class DocumentoDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Documento> implements DocumentoDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate

	public EntityManager getEntityManager() {
		return em;
	}
	
	public DocumentoDAOImpl() {
	}

	public void save(Documento entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(Documento entity) {
		inciarSessionFactory();
		Documento sol = getSess().get(Documento.class, entity.getIdDocumento());
		try {
			ConversorHelper.fusionaPropiedades(entity, sol);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(sol);
	}

	public void delete(Documento entity) {
		em.remove(entity);
	}

	@SuppressWarnings("unchecked")
	public List<Documento> findByIdDocumento(Long idDocumento) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_documento m ");
		sbQuery.append("WHERE m.N_ID_DOCUMENTO = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Documento.class);

		query.setParameter(1, idDocumento);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Documento> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM tbl_regpro_documento");
			query = em.createNativeQuery(sbQuery.toString(), Documento.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}

	public Documento registrar(Documento entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@Override
	public Documento registrarDocumentoConArchivo(Documento entity) {
		em.persist(entity);
//		for (Archivo archivo : entity.getListaArchivo()) {
//			
//		}
		return null;
	}
	

	@Override
	public List<Documento> findByFirstMaxResult(int batchSize, int index) {
		List<Documento> listaDocumento = findAll(Documento.class, batchSize, index);
		return listaDocumento;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Documento> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new Documento(s.idDocumento,s.fechaCreacion,"
				+ " s.usuarioCreacion,s.estado,s.codooii,s.codigoDocumento) "
				+ " FROM Documento s ");
		sbQuery.append(" WHERE 1=1 ");		
		if (parameters.containsKey("codooii")) {
			sbQuery.append(" AND upper(s.codooii) = :codooii ");
		}
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND s.estado = :estado ");
		}

		List<Documento> listaDocumento = find(sbQuery.toString(), batchSize, index, parameters);
		return listaDocumento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Documento> buscarDocumento(String codooii, String tipoResolucion, String numDocumento,
			String fechaDocumento) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" Select * from tbl_regpro_documento d ");
		sbQuery.append(" WHERE d.N_ID_TIPRESO=?1 ");
		sbQuery.append(" AND d.CODOOII= ?2 ");
		sbQuery.append(" AND  TRIM(LEADING '0' FROM d.C_NRODOC) = ?3 ");
		sbQuery.append(" AND DATE_FORMAT(d.D_FECDOCU, '%d/%m/%Y') = DATE_FORMAT(STR_TO_DATE(?4, '%Y-%m-%d'), '%d/%m/%Y')");
		
		Map<Integer, String> parametros = new HashMap<Integer, String>();
		parametros.put(1,tipoResolucion);
		parametros.put(2, codooii);
		parametros.put(3, numDocumento);
		parametros.put(4, fechaDocumento);
		
		List<Documento> listaDocumento = nativeFind(sbQuery.toString(), parametros, Documento.class);
		
		return listaDocumento;
	}
	
	//mejorar codigo y generalizarlo
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	//mejorar codigo y generalizarlo
}
