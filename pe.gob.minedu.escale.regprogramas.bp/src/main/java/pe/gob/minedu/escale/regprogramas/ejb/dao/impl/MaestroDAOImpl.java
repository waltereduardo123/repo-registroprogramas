package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.MaestroDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Maestro;


@Stateless
public class MaestroDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Maestro> implements MaestroDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	public MaestroDAOImpl() {
	}


	public void save(Maestro entity) {
		em.persist(entity);
		em.flush();
	}


	public void update(Maestro entity) {
		em.merge(entity);
	}


	public void delete(Maestro entity) {
		em.remove(entity);
	}

	public Maestro findById(Long id) {
		return em.find(Maestro.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Maestro> buscarMaestrosxPadre(Long id) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("SELECT * FROM tbl_regpro_maestro m ");
		sbQuery.append("WHERE m.N_ID_MAESTROPADRE = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(),Maestro.class);
        
		query.setParameter(1, id);
        query.setParameter(2, EstadoState.ACTIVO.getValue());
        
        
        return query.getResultList();
	}


	@SuppressWarnings("unchecked")
	public List<Maestro> buscarMaestrosxGrupo(String codigoAgrupacion) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("select new Maestro(m.idMaestro,m.codigoAgrupacion, m.nombreMaestro, m.descripcionMaestro, m.codigoItem) ");
        sbQuery.append("FROM Maestro m ");
        sbQuery.append("WHERE m.estado = :estado ");
        sbQuery.append("AND m.codigoAgrupacion = :codigoAgrupacion ");

        Query query = em.createQuery(sbQuery.toString(),Maestro.class);

        query.setParameter("estado", EstadoState.ACTIVO.getValue());
        query.setParameter("codigoAgrupacion", codigoAgrupacion);
        
        return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Maestro> buscarMaestrosxGrupoItem(String codigoAgrupacion,String item) {
	StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("select new Maestro(m.idMaestro,m.codigoAgrupacion, m.nombreMaestro, m.descripcionMaestro, m.codigoItem) ");
        sbQuery.append("FROM Maestro m ");
        sbQuery.append("WHERE m.estado = :estado ");
        sbQuery.append("AND m.codigoAgrupacion = :codigoAgrupacion ");
        sbQuery.append("AND m.codigoItem = :item ");
        
        Query query = em.createQuery(sbQuery.toString(),Maestro.class);

        query.setParameter("estado", EstadoState.ACTIVO.getValue());
        query.setParameter("codigoAgrupacion", codigoAgrupacion);
        query.setParameter("item", item);
        
        return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Maestro> buscarMaestrosxNombre(String nombreMaestro) {
		StringBuilder sbQuery = new StringBuilder();
		
        sbQuery.append("select new Maestro(m.idMaestro,m.codigoAgrupacion, m.nombreMaestro, m.descripcionMaestro, m.codigoItem) ");
        sbQuery.append("FROM Maestro m ");
        sbQuery.append(" where upper(m.nombreMaestro) like :nombreMaestro ");
        
        Query query = em.createQuery(sbQuery.toString(),Maestro.class);
        
        query.setParameter("nombreMaestro", "%".concat(nombreMaestro.toUpperCase()).concat("%"));
        
        return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Maestro> buscarMaestrosxEstado(String estado) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("select new Maestro(m.idMaestro,m.codigoAgrupacion, m.nombreMaestro, m.descripcionMaestro, m.codigoItem) ");
        sbQuery.append("FROM Maestro m ");
        sbQuery.append("WHERE m.estado = :estado ");
        
        Query query = em.createQuery(sbQuery.toString());

		query.setParameter("estado", estado);
		
        return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Maestro> findAll() {
        StringBuilder sbQuery = new StringBuilder();

    	sbQuery.append("SELECT * FROM tbl_regpro_maestro m ");
    	sbQuery.append("WHERE m.C_ESTADO = ?1 ");
    	
    	Query query = em.createNativeQuery(sbQuery.toString(),Maestro.class);
        
        query.setParameter(1, EstadoState.ACTIVO.getValue());
        
            

        return query.getResultList();
	}

	public Maestro registrar(Maestro entity) {
		em.persist(entity);
        em.flush();
        return entity;
	}


	public EntityManager getEntityManager() {
		return em;
	}


	@SuppressWarnings("unchecked")
	public String getMaxItemporGrupo(String codigoAgrupacion) {
		
			String periodomax= "";

			StringBuilder sbQuery = new StringBuilder();
			
			sbQuery.append("SELECT MAX(C_CODITEM) FROM tbl_regpro_maestro m ");
			sbQuery.append("WHERE m.c_codgrup = ?1  ");
			sbQuery.append("AND m.C_ESTADO = ?2 ");
			Query query = em.createNativeQuery(sbQuery.toString());
	        
			query.setParameter(1, codigoAgrupacion);
	        query.setParameter(2, EstadoState.ACTIVO.getValue());
	        
	        Object result = query.getSingleResult();
	        if(result != null)
	        	periodomax = result.toString();
	        	
	        return periodomax;
		
        
//		 StringBuilder sbQuery = new StringBuilder();
//
//	    	sbQuery.append("SELECT MAX(m.codigoItem) FROM Maestro m ");
//	    	sbQuery.append("WHERE m.estado = :estado ");
//	    	sbQuery.append("AND m.codigoAgrupacion = :codigoAgrupacion ");
//	    	
//	    	Query query = em.createNativeQuery(sbQuery.toString(),Maestro.class);
//	        
//	        query.setParameter("estado", EstadoState.ACTIVO.getValue());
//	        query.setParameter("codigoAgrupacion", codigoAgrupacion);
//	            
//
//	        return query.getResultList();
        
        
        
        
	}




	
}
