package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudDocumentoServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoPKDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumento;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumentoPK;

@PermitAll
@Stateless(name = "SolicitudDocumentoService", mappedName = "ejb/SolicitudDocumentoService")
public class SolicitudDocumentoServiceImpl implements SolicitudDocumentoServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private SolicitudDocumentoDAOLocal solicitudDocumentoDAO;

	@Override
	public SolicitudDocumentoDTO buscarSolicitudxId(SolicitudDocumentoPKDTO idSolicitud) throws Exception {
		SolicitudDocumentoPK solicitudDocumentoPK = ConversorHelper.convertir(SolicitudDocumentoPK.class, idSolicitud);
		SolicitudDocumento solicitudDocumento = solicitudDocumentoDAO.findById(solicitudDocumentoPK);
		SolicitudDocumentoDTO solicitudDocumentoDTO = ConversorHelper.convertir(SolicitudDocumentoDTO.class, solicitudDocumento);
		return solicitudDocumentoDTO;
	}

	@Override
	public SolicitudDTO registrarSolicitudNuevo(SolicitudDocumentoDTO solicitud) throws Exception {
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudDocumentoDTO> findByIdSolicitud(Long id) throws Exception {
		List<SolicitudDocumentoDTO> lista = new ArrayList<SolicitudDocumentoDTO>();
		List<SolicitudDocumento> list = solicitudDocumentoDAO.findByIdSolicitud(id);
		lista = ConversorHelper.convertirTodo(SolicitudDocumentoDTO.class, list);
		return lista;
	}

	

}
