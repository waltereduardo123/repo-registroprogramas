package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;



import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;



import org.hibernate.Session;
import org.hibernate.SessionFactory;


import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.CorteDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Corte;


@Stateless
public class CorteDAOImpl extends AbstractJtaStatelessCRUDServices<Long,Corte> implements CorteDAOLocal {
	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate
	
	public EntityManager getEntityManager() {
		return em;
	}
	
	@Override
	public void save(Corte entity) {
		em.persist(entity);
		//em.flush();
	}

	@Override
	public Corte registrar(Corte entity) {
		em.persist(entity);
		//em.flush();
		return entity;
	}





}
