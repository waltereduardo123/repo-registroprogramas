package pe.gob.minedu.escale.regprogramas.ejb.dao;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Corte;

/**
 * 
 * @author WARODRIGUEZ
 *
 */

@Local
public interface CorteDAOLocal extends StatelessCRUDServices<Long, Corte> {
	public void save(Corte entity);
	Corte registrar(Corte entity);
}
