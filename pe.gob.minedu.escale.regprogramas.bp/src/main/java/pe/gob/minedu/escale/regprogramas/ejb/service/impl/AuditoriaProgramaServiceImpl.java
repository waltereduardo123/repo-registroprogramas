package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.AuditoriaProgramaNoActualizadoException;
import pe.gob.minedu.escale.regprograma.business.exception.AuditoriaProgramaNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.AuditoriaProgramaNoRegistradoException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AuditoriaProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.AuditoriaProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.AuditoriaPrograma;


@PermitAll
@Stateless(name = "AuditoriaProgramaService", mappedName = "ejb/AuditoriaProgramaService")
public class AuditoriaProgramaServiceImpl implements AuditoriaProgramaServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private AuditoriaProgramaDAOLocal auditoriaProgramaDAO;

	@Override
	public AuditoriaProgramaDTO registrarProgramaNuevo(AuditoriaProgramaDTO programa) throws Exception {
		try {
			AuditoriaPrograma entity = ConversorHelper.convertir(AuditoriaPrograma.class, programa);
			entity = auditoriaProgramaDAO.registrar(entity);
			programa = ConversorHelper.convertir(AuditoriaProgramaDTO.class, entity);
		} catch (Exception e) {
			throw new AuditoriaProgramaNoRegistradoException(programa.getCodigoModular());
		}
		return programa;
	}

	@Override
	public AuditoriaProgramaDTO findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditoriaProgramaDTO> buscarProgramasxCodooii(char mander) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditoriaProgramaDTO> buscarProgramasxNombre(String nombrePrograma) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditoriaProgramaDTO> buscarProgramasxEstado(String estado) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditoriaProgramaDTO> listarProgramas() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AuditoriaProgramaDTO> listarProgramasByFirstMaxResult(int batchSize, int index) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<AuditoriaProgramaDTO> listarProgramasFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<AuditoriaPrograma> listaPrograma = auditoriaProgramaDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<AuditoriaProgramaDTO> listaAuditoriaProgramaDTO = ConversorHelper.convertirTodo(AuditoriaProgramaDTO.class, listaPrograma);
		return listaAuditoriaProgramaDTO;
	}

	@Override
	public AuditoriaProgramaDTO actualizar(AuditoriaProgramaDTO programa) throws Exception {
		AuditoriaPrograma entity = ConversorHelper.convertir(AuditoriaPrograma.class, programa);
		try {
			auditoriaProgramaDAO.update(entity);
		} catch (Exception e) {
			throw new AuditoriaProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity.getCodigoModular())?entity.getCodigoModular():"No hay codigo modular" );
		}		
		AuditoriaProgramaDTO entityDTO = ConversorHelper.convertir(AuditoriaProgramaDTO.class, entity);
		return entityDTO;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public AuditoriaProgramaDTO buscarProgramaxCodigoModular(String codigoModular) throws Exception {
		List<AuditoriaPrograma> listaPrograma = new ArrayList<AuditoriaPrograma>();
		List<AuditoriaProgramaDTO> lista  = new ArrayList<AuditoriaProgramaDTO>();
		listaPrograma = auditoriaProgramaDAO.buscarProgramasxCodigoModular(codigoModular);
		if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
			lista = ConversorHelper.convertirTodo(AuditoriaProgramaDTO.class, listaPrograma);			
		}else{
			throw new AuditoriaProgramaNoExisteException();
		}		
		return lista.stream().max(Comparator.comparing(AuditoriaProgramaDTO::getIdPrograma)).get();
	}

	

}
