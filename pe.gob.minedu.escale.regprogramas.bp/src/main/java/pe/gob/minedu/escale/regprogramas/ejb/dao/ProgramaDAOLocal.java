package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Programa;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface ProgramaDAOLocal extends StatelessCRUDServices<Long, Programa> {

	void save(Programa entity);
	void update(Programa entity);
	void delete(Programa entity);
	Programa registrar(Programa entity);
	Programa findById(Long id);
	List<Programa> buscarProgramasxCodooii(char mander);
	List<Programa> buscarProgramasxNombre(String nombrePrograma);
	List<Programa> buscarProgramasxEstado(String estado);
	List<Programa> findAll();
	List<Programa> findByFirstMaxResult(int batchSize, int index);
	@SuppressWarnings("rawtypes")
	List<Programa> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
	List<Programa> findByResultProgramasReinicio(Map parameters);// wer 15112017
	List<String> findByResultProgramasSincroniza(Map parameters);// wer 22022018
	List<Programa> findByResultProgramasSincronizaUpdate(Map parameters);// wer 22022018
	List<Object> findProgramasByCentroPoblado(Map parameters);// wer 22022018
	List<Programa> buscarCodigoModular(String codigoModular);
	List<Programa> buscarProgramasxCodigoModular(String codigoModular);
	List<Programa> buscarProgramasCodigoModular(String codigoModular);//werr
	List<Programa> buscarProgramasSituacionxCodigoModular(String codigoModular);
}
