package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.SerializationUtils;

import pe.gob.minedu.escale.adm.business.type.TipoArchivoType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.business.type.TipoPeriodoType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionRevisionType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.AccionSolicitudNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.ArchivoSolicitudNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.CodigoSolicitudRepetidoException;
import pe.gob.minedu.escale.regprograma.business.exception.DocumentoNoActualizadoException;
import pe.gob.minedu.escale.regprograma.business.exception.DocumentoSolicitudNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoActualizadaException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoRegistradaException;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AccionSolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ArchivoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.DocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.RevisionSolicitudCamposDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ArchivosBandejaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ObservacionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.SolicitudBandejaDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.AccionSolicitud;
import pe.gob.minedu.escale.regprogramas.model.jpa.Archivo;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;
import pe.gob.minedu.escale.regprogramas.model.jpa.Maestro;
import pe.gob.minedu.escale.regprogramas.model.jpa.Programa;
import pe.gob.minedu.escale.regprogramas.model.jpa.RevisionSolicitudCampos;
import pe.gob.minedu.escale.regprogramas.model.jpa.Solicitud;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumento;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumentoPK;

@PermitAll
@Stateless(name = "SolicitudService", mappedName = "ejb/SolicitudService")
public class SolicitudServiceImpl implements SolicitudServiceLocal {

	private static final String AGRUP_MAESTRO_PERIODO = "34";
	

	
	/** El servicio usuario dao. */
	@EJB
	private ArchivoDAOLocal archivoDAO;

	@EJB
	private DocumentoDAOLocal documentoDAO;

	@EJB
	private SolicitudDAOLocal solicitudDAO;

	@EJB
	private AccionSolicitudDAOLocal accionSolicitudDAO;

	@EJB
	private SolicitudDocumentoDAOLocal solicitudDocumentoDAO;
	
	@EJB
	private ProgramaDAOLocal programaDAO;	
	
	@EJB
	private RevisionSolicitudCamposDAOLocal  revisionSolicitudCamposDAO;
	
	@EJB
	private DataGeneralCache dataGeneralCache;
	
	private List<MaestroDTO> listaTipoSolicitud = new ArrayList<MaestroDTO>();
	
	private List<MaestroDTO> listaTipoSituacionPrograma = new ArrayList<MaestroDTO>();
	
	private List<MaestroDTO> listaTipoDocumento;
	
	private List<MaestroDTO> listaTipoSituacionSolicitud;
	
	private List<MaestroDTO> listaTipoSituacionRevision;
	
	private List<MaestroDTO> listaTipoArchivo = new ArrayList<MaestroDTO>();
	
	private List<MaestroDTO> listaTipoPeriodo = new ArrayList<MaestroDTO>();
	
	@EJB(beanName="MaestroService")
	private transient MaestroServiceLocal maestroService;
	
//	private Map<Object, Object> parametros;
	
	@PostConstruct
	public void iniciar() {
		listaTipoSolicitud = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionPrograma = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSituacionProgramaType.CODIGO_AGRUPACION.getKey());
		listaTipoDocumento = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoDocumentoType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionSolicitud = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionRevision = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSituacionRevisionType.CODIGO_AGRUPACION.getKey());
		listaTipoArchivo = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoArchivoType.CODIGO_AGRUPACION.getKey());
		listaTipoPeriodo=dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoPeriodoType.CODIGO_AGRUPACION.getKey());
		
	}
	
//	@SuppressWarnings("unused")
//	private Map<Object, Object> parametros;

	// AccionSolicitud

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> listarSolicitud() throws Exception {
		List<Solicitud> lista = solicitudDAO.findAll();
		List<SolicitudDTO> listSolicitud = ConversorHelper.convertirTodo(SolicitudDTO.class, lista);
		return listSolicitud;
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> buscarSolicitudxId(Long idSolicitud) throws Exception {
		List<Solicitud> lista = solicitudDAO.findByIdSolicitud(idSolicitud);
		List<SolicitudDTO> listSolicitud = ConversorHelper.convertirTodo(SolicitudDTO.class, lista);
		return listSolicitud;
	}

	public SolicitudDTO registrarSolicitudNuevo(SolicitudDTO solicitud, DocumentoDTO documento) throws Exception {
		try{			
//		    if (!solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
//																					  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey().toString()))
//																					  .findFirst()
//																					  .get()
//																					  .getIdMaestro())
//		    		&& !solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
//																							  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey().toString()))
//																							  .findFirst()
//																							  .get()
//																							  .getIdMaestro())) {
			 if (!solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro())) {
				List<Programa> listaPrograma = programaDAO.buscarCodigoModular(solicitud.getCodigoModular().toString());
				Programa programa = listaPrograma.get(0);
				programa.setTipoSituacion(new Maestro(listaTipoSituacionPrograma.stream()
																				  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.ENREVISION.getKey().toString()))
																				  .findFirst()
																				  .get()
																				  .getIdMaestro()));
				programaDAO.update(programa);
			}
		    if (solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
																					  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey().toString()))
																					  .findFirst()
																					  .get().getIdMaestro())) {
				SolicitudDTO sol = new SolicitudDTO();
				sol.setUsuarioModificacion(solicitud.getUsuarioCreacion());
				sol.setFechaModificacion(FechaUtil.obtenerFechaActual());
				ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();
				observacionSolicitud.setObservacion("Anulado por registro de cierre");
				gestionAccionSolicitud(listaTipoSituacionSolicitud.stream()
																  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
																  .findFirst()
																  .get()
																  .getIdMaestro()
																  .toString(),
																  "",
																  observacionSolicitud,
																  solicitud.getCodigoModular(),
																  sol,
																  true);			
				
				
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		//obtener le item de la agrupacion
	//	List<MaestroDTO> maestroMax=maestroService.getMaxItemGrupoMaestro("34");
		String item=maestroService.getMaxItemGrupoMaestro(AGRUP_MAESTRO_PERIODO).getCodigoItem();
		//buscar por codigo de grupo y codigo de item
		//maestroService.buscarMaestrosxGrupoItem("34", item).get(0).getIdMaestro();
		MaestroDTO mdto=new MaestroDTO();
		mdto.setIdMaestro(maestroService.buscarMaestrosxGrupoItem(AGRUP_MAESTRO_PERIODO, item).get(0).getIdMaestro());
		//Se agrega el periodo de la solicitud werr 20112017
//		solicitud.setPeriodoRegistrado(new MaestroDTO(listaTipoPeriodo.stream()
//				  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2018.getKey().toString()))
//				  .findFirst()
//				  .get().getIdMaestro()));
		solicitud.setPeriodoRegistrado(mdto);

		
		
	
		// registro Solicitud		
		Solicitud solicitudNuevo = ConversorHelper.convertir(Solicitud.class, solicitud);
		solicitudNuevo = solicitudDAO.registrar(solicitudNuevo);
		
		
		// registro accionsolciitud
		try {
			for (AccionSolicitudDTO accionSolicitudDTO : solicitud.getListaAccionSolicitud()) {
				AccionSolicitud accionSolicitud = ConversorHelper.convertir(AccionSolicitud.class, accionSolicitudDTO);
				accionSolicitud.setSolicitud(solicitudNuevo);
				accionSolicitudDAO.registrar(accionSolicitud);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		// registro AccionSolicitud
		// registro Documento
		Documento documentoNuevo = ConversorHelper.convertir(Documento.class, documento);
		Documento documentoViejo = null;		
		List<Documento> listaDocumento = new ArrayList<Documento>();
		String numeroDocumento = documentoNuevo.getNroDocumento().replaceFirst ("^0*", "");
		if(documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())) {		
			listaDocumento  = documentoDAO.buscarDocumento(documentoNuevo.getCodooii(), 
					documentoNuevo.getTipoDocumentoResolucion().getIdMaestro().toString(), 
					numeroDocumento, 
					FechaUtil.obtenerFechaFormatoPersonalizado(documentoNuevo.getFechaDocumento(), "yyyy-MM-dd"));
			documentoNuevo = listaDocumento.get(0);
			listaDocumento.clear();
		}else		
			if(documento.getIndCambioArchivos().equals(EstadoState.ACTIVO.getValue())) {
				listaDocumento = documentoDAO.buscarDocumento(documentoNuevo.getCodooii(), 
						documentoNuevo.getTipoDocumentoResolucion().getIdMaestro().toString(), 
						numeroDocumento, 
						FechaUtil.obtenerFechaFormatoPersonalizado(documentoNuevo.getFechaDocumento(), "yyyy-MM-dd"));
				documentoViejo = listaDocumento.get(0);				
				documentoNuevo = listaDocumento.get(0);
				listaDocumento.clear();
			}else
				if(documento.getIndCambioArchivos().equals("2")) { //Nuevo registro
					documentoNuevo = documentoDAO.registrar(documentoNuevo);					
				}
			listaDocumento.add(documentoNuevo);
		
		
		
		// registro archivo
		try {
			for (ArchivoDTO archivoDTO : documento.getListaArchivo()) {
//				if (archivoDTO.getTipoArchivo().getIdMaestro().longValue() == Long
//						.valueOf(listaTipoDocumento.stream()
//								  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
//								  .findFirst()
//								  .get()
//								  .getIdMaestro()).longValue()) {
				if (archivoDTO.getTipoArchivo().getIdMaestro().equals(listaTipoDocumento.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro())) {			
					DocumentoDTO documentoCroquis = new DocumentoDTO();
					documentoCroquis = (DocumentoDTO) SerializationUtils.clone(documento);
					documentoCroquis.getListaArchivo().clear();
					documentoCroquis.getListaArchivo().add(archivoDTO);
					documentoCroquis.setCodigoDocumento(
							TipoDocumentoType.CROQUIS.getDescription(ResourceUtil.obtenerLocaleSession()));
					documentoCroquis.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro()));
					documentoCroquis.setTipoDocumentoResolucion(new MaestroDTO(0L));
					Documento documentoNuevoCroquis = ConversorHelper.convertir(Documento.class, documentoCroquis);
					documentoNuevoCroquis = documentoDAO.registrar(documentoNuevoCroquis);
					listaDocumento.add(documentoNuevoCroquis);
					archivoDTO.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
																			  .filter(p-> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
					Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
					archivo.setDocumento(documentoNuevoCroquis);
					archivoDAO.registrar(archivo);

				} else {
					Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
					if (StringUtil.isNotNullOrBlank(documento.getIndCambioArchivos())){
						if (documento.getIndCambioArchivos().equals(EstadoState.ACTIVO.getValue())) {
							for(Archivo ar : documentoViejo.getListaArchivo()){
								ar.setEstado(EstadoState.INACTIVO.getValue());
								archivoDAO.update(ar);
							}
						}
						if(!documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())){
							archivo.setDocumento(documentoNuevo);
							archivoDAO.registrar(archivo);
						}
					}												
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		// registro SolicitudDocumento
		for (Documento doc : listaDocumento) {
			SolicitudDocumentoPK pk = new SolicitudDocumentoPK(doc.getIdDocumento(), solicitudNuevo.getIdSolicitud());
			SolicitudDocumento solicitudDocumento = new SolicitudDocumento();
			solicitudDocumento.setSolicitudDocumentoPK(pk);
			solicitudDocumento.setFechaCreacion(FechaUtil.obtenerFechaActual());
			solicitudDocumento.setUsuarioCreacion(documento.getUsuarioCreacion());
			solicitudDocumento.setEstado(EstadoState.ACTIVO.getValue());
			solicitudDocumento = solicitudDocumentoDAO.registrarSolicitudDocumentoNuevo(solicitudDocumento);
		}

		SolicitudDTO solicitudNuevoDTO = ConversorHelper.convertir(SolicitudDTO.class, solicitudNuevo);

		return solicitudNuevoDTO;
	}

	public List<SolicitudDTO> listarSolicitudByFirstMaxResult(int batchSize, int index) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<SolicitudDTO> listarSolicitudFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<Solicitud> listaSolicitud = solicitudDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<SolicitudDTO> listaSolicitudDTO = ConversorHelper.convertirTodo(SolicitudDTO.class, listaSolicitud);
		return listaSolicitudDTO;
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> buscarSolicitudCarga(String nombrePrograma, String codigoModular,
			String tipoSolicitud) throws Exception {
		List<Solicitud> lista = solicitudDAO.findByNameProgramaCodooii(nombrePrograma, codigoModular, tipoSolicitud);
		List<SolicitudDTO> listSolicitud = ConversorHelper.convertirTodo(SolicitudDTO.class, lista);
		return listSolicitud;
	}

	
	@SuppressWarnings("unchecked")
	public List<SolicitudDTO> buscarSolicitud(String nombrePrograma, String codooii, String codigoModular, String tipoSolicitud)
			throws Exception {
		List<Solicitud> lista = solicitudDAO.findByNombreProgramaCodooii(nombrePrograma, codooii, codigoModular, tipoSolicitud);
		List<SolicitudDTO> listSolicitud = ConversorHelper.convertirTodo(SolicitudDTO.class, lista);
		return listSolicitud;
	}

	public SolicitudDTO actualizar(SolicitudDTO solicitud) throws Exception,SolicitudNoRegistradaException {
		Solicitud entity = ConversorHelper.convertir(Solicitud.class, solicitud);
		try {
			solicitudDAO.update(entity);
		} catch (Exception e) {
			throw new SolicitudNoActualizadaException();
		}		
//		SolicitudDTO entityDTO = ConversorHelper.convertir(SolicitudDTO.class, entity);
		return solicitud;
	}

	public SolicitudDTO obtenerSolicitudById(Long id) throws Exception {
		Solicitud entity = solicitudDAO.findById(id);
		SolicitudDTO entityDTO = ConversorHelper.convertir(SolicitudDTO.class, entity);
		return entityDTO;
	}
	
	@SuppressWarnings("rawtypes")
	public RespuestaDTO gestionAccionSolicitud(String tipoSituacionSolicitud, String codSolicitud,  ObservacionSolicitudDTO observacionSolicitud, String codigoModular, SolicitudDTO solicitud,
			Boolean indAnularSolicitudxCierre)
			throws Exception {		
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		AccionSolicitudDTO accionSolicitud = null;
		MaestroDTO tipoSituacionSolicitudMaestro = null;
		Map<Object, Object> parametros = new HashMap<>();
		RespuestaDTO respuesta;
		List<AccionSolicitudDTO> listaAccionSolicitud = new ArrayList<AccionSolicitudDTO>();
		try {			
			
			boolean isTipoDeSituacionSolicitud = listaTipoSituacionSolicitud.stream().anyMatch(p -> p.getIdMaestro().equals(Long.valueOf(tipoSituacionSolicitud)));
			
			
			if (isTipoDeSituacionSolicitud) {
				tipoSituacionSolicitudMaestro = new MaestroDTO(Long.valueOf(tipoSituacionSolicitud));
				if (StringUtil.isNotNullOrBlank(codSolicitud))parametros.put("codSolicitud", codSolicitud.toUpperCase(ResourceUtil.obtenerLocaleSession()));
				if (StringUtil.isNotNullOrBlank(codigoModular))parametros.put("codigoModular", codigoModular);
				parametros.put("estado", EstadoState.ACTIVO.getValue());
				listSolicitud = listarSolicitudFirstMaxResultParametros(100, 0, parametros);
				//Si el tipo de solicitud es de ANULACION
				if (tipoSituacionSolicitud.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(pa-> pa.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()
																			  .toString())) {
					//Filtrar todas aquellas solicitudes que sean distintas de ANULADAS y APROBADAS, ya que luego podrian ser anuladas.
					listSolicitud = listSolicitud.stream()
								 				 .filter(p-> !p.getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
																																			  .filter(pa-> pa.getCodigoItem().equals(TipoSituacionSolicitudType.APROBADO.getKey().toString()))
																																			  .findFirst()
																																			  .get()
																																			  .getIdMaestro())
								 						 &&
									 						!p.getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
																																			  .filter(pe-> pe.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
																																			  .findFirst()
																																			  .get()
																																			  .getIdMaestro())
								 						 )
						 				 		 .collect(Collectors.toList());
				}				
				parametros.remove("codSolicitud");
				if (Objects.nonNull(listSolicitud)) {
					if (!listSolicitud.isEmpty()) {
						if (listSolicitud.size() > 0 && listSolicitud.size() == 1) {
							for (SolicitudDTO s : listSolicitud) {
								if (Objects.nonNull(solicitud)) {
									ConversorHelper.fusionaPropiedades(solicitud, s);
								}								
								accionSolicitud = new AccionSolicitudDTO();
								accionSolicitud.setSolicitud(new SolicitudDTO(s.getIdSolicitud()));
								accionSolicitud.setTipoSituacionSolicitud(tipoSituacionSolicitudMaestro);
								if (Objects.nonNull(observacionSolicitud)) {
									if (StringUtil.isNotNullOrBlank(observacionSolicitud.getObservacion()))accionSolicitud.setObservacion(observacionSolicitud.getObservacion());
									if (StringUtil.isNotNullOrBlank(observacionSolicitud.getPerfil()))accionSolicitud.setPerfilRevisor(observacionSolicitud.getPerfil());
								}								
								accionSolicitud.setEstado(EstadoState.ACTIVO.getValue().toString());
								if(StringUtil.isNotNullOrBlank(s.getUsuarioModificacion())){
									accionSolicitud.setUsuarioCreacion(s.getUsuarioModificacion()); 
								}else{
									accionSolicitud.setUsuarioCreacion("system");
								}								
								accionSolicitud.setFechaCreacion(FechaUtil.obtenerFechaActual());
								listaAccionSolicitud.add(accionSolicitud);															
								if (tipoSituacionSolicitud.equals(listaTipoSituacionSolicitud.stream()
																							  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
																							  .findFirst()
																							  .get()
																							  .getIdMaestro()
																							  .toString())) {
//									parametros.clear();
//									if (StringUtil.isNotNullOrBlank(codigoModular))parametros.put("codigoModular", codigoModular);
//									parametros.put("estado", EstadoState.ACTIVO.getValue());
//									List<SolicitudDTO> listaSolicitud = new ArrayList<SolicitudDTO>();
//									listaSolicitud = listarSolicitudFirstMaxResultParametros(5, 0, parametros);
//									Long idMaestroTipoSituacionPrograma = null;
//									if (Objects.nonNull(listaSolicitud) && !listaSolicitud.isEmpty()) {
//										if (listaSolicitud.size()>=1) {
//											idMaestroTipoSituacionPrograma = listaTipoSituacionPrograma.stream()
//																						  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.ENREVISION.getKey().toString()))
//																						  .findFirst()
//																						  .get()
//																						  .getIdMaestro();
//										}else{
//											idMaestroTipoSituacionPrograma = s.getTipoSituacionPrograma().getIdMaestro();
//										}
//									}else{
//										
//									}
									Long idMaestroTipoSituacionPrograma = null;
									if (Objects.nonNull(indAnularSolicitudxCierre) && indAnularSolicitudxCierre) {
										idMaestroTipoSituacionPrograma = listaTipoSituacionPrograma.stream()
																									  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.ENREVISION.getKey().toString()))
																									  .findFirst()
																									  .get()
																									  .getIdMaestro();
									}else{
										idMaestroTipoSituacionPrograma = s.getTipoSituacionPrograma().getIdMaestro();
									}
									
									
									List<Programa> listaPrograma = programaDAO.buscarCodigoModular(s.getCodigoModular().toString());
									if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
										Programa programa = listaPrograma.get(0);
										programa.setTipoSituacion(new Maestro(idMaestroTipoSituacionPrograma));
										programaDAO.update(programa);
									}
								}														
							}							
						} else {
							throw new CodigoSolicitudRepetidoException(listSolicitud.get(0).getCodSolicitud());							
						}
						listSolicitud.forEach(s->{
							s.setTipoSituacionSolicitud(new MaestroDTO(Long.valueOf(tipoSituacionSolicitud)));	
							s.setFechaModificacion(FechaUtil.obtenerFechaActual());
						});
						try {
							for (SolicitudDTO s : listSolicitud) {
								actualizar(s);
							}
							for (AccionSolicitudDTO a : listaAccionSolicitud) {
								try {
									AccionSolicitud entity = ConversorHelper.convertir(AccionSolicitud.class, a);
									accionSolicitudDAO.registrar(entity);
								} catch (Exception e) {
									throw new AccionSolicitudNoRegistradoException();
								}								
							}
						} catch (Exception e) {
							throw new SolicitudNoActualizadaException();
						}
					}
				}				
				parametros.keySet().removeIf(e -> (e == "estado" || e == "codSolicitud"));
				parametros.put("succes", true);
				parametros.put("solicitudActualizada", true);
				parametros.put("accionSolicitudCreado", true);
				respuesta = new RespuestaDTO(true, "[]", parametros);
			} else {
				parametros.put("succes", true);
				parametros.put("solicitudActualizada", false);
				parametros.put("accionSolicitudCreado", false);
				parametros.put("isTipoDeSituacionSolicitud", isTipoDeSituacionSolicitud);
				respuesta = new RespuestaDTO(false, "['El tipo de situacion de la solicitud no es valido']",
						parametros);
			}			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			parametros.put("succes", false);
			parametros.put("solicitudActualizada", false);
			parametros.put("accionSolicitudCreado", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema con el servicio.']", parametros);
			return respuesta;
		}
		return respuesta;
	}
		
	public List<SolicitudBandejaDTO> bandejaRevision(int index, int batchSize, String jpqlRules, String jpqlOrder) throws Exception {
		List<Solicitud> lista = new ArrayList<Solicitud>();
//		//imendoza 20170102  lista =	solicitudDAO.bandejaRevision(index, batchSize, jpqlRules,null);
		lista =	solicitudDAO.bandejaRevision(index, batchSize, jpqlRules,jpqlOrder);
		List<SolicitudBandejaDTO> listaRetorno = new ArrayList<SolicitudBandejaDTO>();
		List<SolicitudDocumento> listaSolicitudDocumento = new ArrayList<SolicitudDocumento>();		
		Documento docu = new Documento();
		List<Archivo> listArchivo = new ArrayList<Archivo>();		
		SolicitudBandejaDTO solArch;
		for (Solicitud solicitud : lista) {
//			solArch = new SolicitudBandejaDTO(solicitud.getIdSolicitud(), solicitud.getDreUgel().getCodigo(), solicitud.getFechaCreacion(), 
//					solicitud.getTipoSolicitud().getDescripcionMaestro(), solicitud.getNombrePrograma(), 
//					solicitud.getTipoPrograma().getDescripcionMaestro(), solicitud.getCodigoModular(), 
//					solicitud.getTipoSituacionSolicitud().getDescripcionMaestro(), solicitud.getNombreUsuarioRevision(), 
//					solicitud.getNombreUsuarioRevisionSig(), null, null, null, null,
//					solicitud.getUsuarioRevision(), solicitud.getUsuarioRevisionSig(),
//					solicitud.getCount());
			solArch = new SolicitudBandejaDTO(solicitud.getIdSolicitud(), solicitud.getDreUgel().getCodigo(), solicitud.getFechaCreacion(), 
					solicitud.getTipoSolicitud().getDescripcionMaestro(), solicitud.getNombrePrograma(), 
					solicitud.getTipoPrograma().getDescripcionMaestro(), solicitud.getCodigoModular(), 
					solicitud.getTipoSituacionSolicitud().getDescripcionMaestro(), solicitud.getNombreUsuarioRevision(), 
					solicitud.getNombreUsuarioRevisionSig(), null, null, null, null,
					solicitud.getUsuarioRevision(), solicitud.getUsuarioRevisionSig(), solicitud.getTipoSituacionRevisionSig().getDescripcionMaestro(),
					solicitud.getTipoSituacionRevision().getDescripcionMaestro(), solicitud.getCount(), solicitud.getIndicadorUltimo(), solicitud.getFechaModificacion());
			listaSolicitudDocumento = solicitudDocumentoDAO.findByIdSolicitud(solicitud.getIdSolicitud());
			for (SolicitudDocumento solicitudDocumento : listaSolicitudDocumento) {
				
				docu = solicitudDocumento.getDocumento();
				
				if (docu.getEstado().equals(EstadoState.ACTIVO.getValue())) {
					if (docu.getTipoDocumento().getIdMaestro().equals(listaTipoDocumento.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.RESOLUCION.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro())) {
						solArch.setIdDocumento(docu.getIdDocumento());
						solArch.setCodigoDocumento(docu.getCodigoDocumento());					
						listArchivo = docu.getListaArchivo();
						List<ArchivosBandejaDTO> listAr = new ArrayList<ArchivosBandejaDTO>();
						for (Archivo archivo : listArchivo) {
							if (archivo.getEstado().equals(EstadoState.ACTIVO.getValue())) {
								ArchivosBandejaDTO archivosBandeja = new ArchivosBandejaDTO(archivo.getIdArchivo(), 
										archivo.getNombreArchivo(), archivo.getCodigoArchivo(), archivo.getEstado());
								if (!(listAr.stream()
										.anyMatch((p) -> p.getIdArchivo().equals(archivosBandeja.getIdArchivo())))) {
									listAr.add(archivosBandeja);
								}
							}												
						}
						solArch.setListaArchivoResoluciones(listAr);
					}
					if (docu.getTipoDocumento().getIdMaestro().equals(listaTipoDocumento.stream()
							  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
							  .findFirst()
							  .get().getIdMaestro())) {	
						listArchivo = docu.getListaArchivo();
						Archivo arc = new Archivo();
						arc = listArchivo.get(0);
						ArchivosBandejaDTO ar = new ArchivosBandejaDTO(arc.getIdArchivo(), arc.getNombreArchivo(), arc.getCodigoArchivo(), arc.getEstado());					
						solArch.setArchivoCroquis(ar);
					}
				}
			}
//			listaSolicitudDocumento.addAll(solicitud.getListaSolicitudDocumento());
//			listaSolicitudDocumento2.addAll(listaSolicitudDocumento);
			listaRetorno.add(solArch);
		}
		//imendoza 20170102 
//		if (StringUtil.isNotNullOrBlank(jpqlOrder)) {
//			Method getMetodo = null;
//			getMetodo.invoke(SolicitudBandejaDTO.class);
//			listaRetorno.stream().sorted(
//                    comparing(SolicitudBandejaDTO::getCodigoDocumento)
//                   .thenComparing(reverseOrder(comparing(SolicitudBandejaDTO::codigoDocumento))))
//                   .collect(Collectors.toList());
//		}		
		return listaRetorno;
	}

	public List<MaestroDTO> getListaTipoSolicitud() {
		return listaTipoSolicitud;
	}

	public void setListaTipoSolicitud(List<MaestroDTO> listaTipoSolicitud) {
		this.listaTipoSolicitud = listaTipoSolicitud;
	}

	public List<MaestroDTO> getListaTipoSituacionPrograma() {
		return listaTipoSituacionPrograma;
	}

	public void setListaTipoSituacionPrograma(List<MaestroDTO> listaTipoSituacionPrograma) {
		this.listaTipoSituacionPrograma = listaTipoSituacionPrograma;
	}

	@Override
	public SolicitudDTO registrarSolicitudModificacion(SolicitudDTO solicitud, DocumentoDTO documento) throws Exception {
//		parametros = new HashMap<>();
		//PENDIENTE REVISION solo al estado de revisor OBSERVADO - INICIO
		Solicitud sol = solicitudDAO.findById(solicitud.getIdSolicitud());
		if (Objects.nonNull(sol)) {
			if (sol.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																								  .filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
																								  .findFirst()
																								  .get()
																								  .getIdMaestro())) {
				solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
																							  .filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
																							  .findFirst()
																							  .get()
																							  .getIdMaestro()));
				solicitud.setUsuarioRevision("");
				solicitud.setNombreUsuarioRevision("");
			}
			if (sol.getTipoSituacionRevisionSig().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																								  .filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
																								  .findFirst()
																								  .get()
																								  .getIdMaestro())) {
				solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
																								  .filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
																								  .findFirst()
																								  .get()
																								  .getIdMaestro()));
				solicitud.setUsuarioRevisionSig("");
				solicitud.setNombreUsuarioRevisionSig("");
			}			
		}
		//PENDIENTE REVISION solo al estado de revisor OBSERVADO - FIN
		// registro Solicitud		
		Solicitud solicitudNuevo = ConversorHelper.convertir(Solicitud.class, solicitud);	
		solicitud = actualizar(solicitud);
			
		// INICIO registro AccionSolicitud
		if (Objects.nonNull(solicitud.getListaAccionSolicitud()) && !solicitud.getListaAccionSolicitud().isEmpty()) {
			try {
				for (AccionSolicitudDTO accionSolicitudDTO : solicitud.getListaAccionSolicitud()) {
					AccionSolicitud accionSolicitud = ConversorHelper.convertir(AccionSolicitud.class, accionSolicitudDTO);
					accionSolicitud.setSolicitud(solicitudNuevo);
					accionSolicitudDAO.registrar(accionSolicitud);
				}
			} catch (Exception e) {
				throw new AccionSolicitudNoRegistradoException();			
			}
		}				
		// FIN registro AccionSolicitud
		// registro Documento
		Documento documentoNuevo = ConversorHelper.convertir(Documento.class, documento);
		Documento documentoViejo = null;
		Documento documentoViejoXIndCambioArchivos = null;
		SolicitudDocumento solDoc = null;
		SolicitudDocumento solDocAnt = null;
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoViejo = new ArrayList<Documento>();
		List<SolicitudDocumento> listaSolicitudDocumentoViejo = new ArrayList<SolicitudDocumento>();
		List<Archivo> listArch = null;
		try {
			//Administracion de documento Resolucion
			switch (documento.getIndCambioArchivos()){
			case "1"://Se cambia archivos, mas no documento
				documentoNuevo = buscarResolucion(documentoNuevo);
				documentoViejoXIndCambioArchivos = (Documento) SerializationUtils.clone(documentoNuevo);
				listArch = archivoDAO.buscarArchivo(documentoViejoXIndCambioArchivos.getIdDocumento());
				documentoViejoXIndCambioArchivos.setListaArchivo(listArch);
				break;
			case "2"://Nuevo registro
				solDoc = buscarSolicitudDocumentoByTipo(solicitud.getIdSolicitud(), TipoDocumentoType.RESOLUCION.getKey());				
				listaSolicitudDocumentoViejo.add((SolicitudDocumento) SerializationUtils.clone(solDoc));
				documentoViejo = ((SolicitudDocumento) SerializationUtils.clone(solDoc)).getDocumento();
				documentoNuevo = documentoDAO.registrar(documentoNuevo);
				listaDocumento.add(documentoNuevo);
				break;
			case "3"://Se cambia documento a otro existente, sin cambio de archivos
				documentoNuevo = buscarResolucion(documentoNuevo);
				solDocAnt = buscarSolicitudDocumentoByTipoYDocumento(solicitud.getIdSolicitud(), TipoDocumentoType.RESOLUCION.getKey(), documentoNuevo);
				if(Objects.nonNull(solDocAnt)){
					documentoNuevo.setIndCambioDocumentoExistente("1");//Indica que la relacion SolicitudDocumento ya existe, y solo se debe de actualziar de 0 a 1: Activo
				}
				solDoc = buscarSolicitudDocumentoByTipo(solicitud.getIdSolicitud(), TipoDocumentoType.RESOLUCION.getKey());				
				listaSolicitudDocumentoViejo.add((SolicitudDocumento) SerializationUtils.clone(solDoc));
				documentoViejo = ((SolicitudDocumento) SerializationUtils.clone(solDoc)).getDocumento();
				listaDocumento.add(documentoNuevo);
				break;
			case "4"://Se cambia documento a otro existente, con cambio de archivos
				documentoNuevo = buscarResolucion(documentoNuevo);
				solDocAnt = buscarSolicitudDocumentoByTipoYDocumento(solicitud.getIdSolicitud(), TipoDocumentoType.RESOLUCION.getKey(), documentoNuevo);
				if(Objects.nonNull(solDocAnt)){
					documentoNuevo.setIndCambioDocumentoExistente("1");//Indica que la relacion SolicitudDocumento ya existe, y solo se debe de actualziar de 0 a 1: Activo
				}
				documentoViejoXIndCambioArchivos = (Documento) SerializationUtils.clone(documentoNuevo);
				listArch = archivoDAO.buscarArchivo(documentoViejoXIndCambioArchivos.getIdDocumento());
				documentoViejoXIndCambioArchivos.setListaArchivo(listArch);
				solDoc = buscarSolicitudDocumentoByTipo(solicitud.getIdSolicitud(), TipoDocumentoType.RESOLUCION.getKey());				
				listaSolicitudDocumentoViejo.add((SolicitudDocumento) SerializationUtils.clone(solDoc));
				documentoViejo = ((SolicitudDocumento) SerializationUtils.clone(solDoc)).getDocumento();
				listaDocumento.add(documentoNuevo);
				break;
			default:
				break;
			}
			//Administracion de documento Croquis
			if (documento.getIndCambioArchivoCroquis().equals(EstadoState.ACTIVO.getValue())){
				solDoc = buscarSolicitudDocumentoByTipo(solicitud.getIdSolicitud(), TipoDocumentoType.CROQUIS.getKey());
				if (Objects.nonNull(solDoc)) {
					listaSolicitudDocumentoViejo.add((SolicitudDocumento) SerializationUtils.clone(solDoc));
					documentoViejo = ((SolicitudDocumento) SerializationUtils.clone(solDoc)).getDocumento();
					listArch = archivoDAO.buscarArchivo(documentoViejo.getIdDocumento());
					documentoViejo.setListaArchivo(listArch);
					listaDocumentoViejo.add(documentoViejo);
				}				
			}
			//Desactivar documentos viejos
			try {
				if (Objects.nonNull(listaSolicitudDocumentoViejo) && !listaSolicitudDocumentoViejo.isEmpty()) {
					for (SolicitudDocumento solDocv: listaSolicitudDocumentoViejo) {
						solDocv.setEstado(EstadoState.INACTIVO.getValue());
						solDocv.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
						solDocv.setUsuarioUltimaModificacion(solicitud.getUsuarioModificacion());
						solicitudDocumentoDAO.update(solDocv);							
					}
				}
				if (Objects.nonNull(listaDocumentoViejo) && !listaDocumentoViejo.isEmpty()) {
					for (Documento docv: listaDocumentoViejo) {
						docv.setEstado(EstadoState.INACTIVO.getValue());
						docv.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
						docv.setUsuarioUltimaModificacion(solicitud.getUsuarioModificacion());
						documentoDAO.update(docv);							
						for(Archivo ar1 : docv.getListaArchivo()){
							ar1.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
							ar1.setUsuarioUltimaModificacion(solicitud.getUsuarioModificacion());
							ar1.setEstado(EstadoState.INACTIVO.getValue());
							archivoDAO.update(ar1);
						}
					}
				}					
			} catch (Exception e) {
				throw new DocumentoNoActualizadoException();
			}				
			//Registro archivo
			try {
				for (ArchivoDTO archivoDTO : documento.getListaArchivo()) {
					if (archivoDTO.getTipoArchivo().getIdMaestro().equals(listaTipoDocumento.stream()
																							  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
																							  .findFirst()
																							  .get()
																							  .getIdMaestro())) {
						DocumentoDTO documentoCroquis = new DocumentoDTO();
						documentoCroquis = (DocumentoDTO) SerializationUtils.clone(documento);
						documentoCroquis.getListaArchivo().clear();
						documentoCroquis.getListaArchivo().add(archivoDTO);
						documentoCroquis.setCodigoDocumento(
								TipoDocumentoType.CROQUIS.getDescription(ResourceUtil.obtenerLocaleSession()));
						documentoCroquis.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
																							  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
																							  .findFirst()
																							  .get()
																							  .getIdMaestro()));
						documentoCroquis.setTipoDocumentoResolucion(new MaestroDTO(0L));
						Documento documentoNuevoCroquis = ConversorHelper.convertir(Documento.class, documentoCroquis);
						documentoNuevoCroquis = documentoDAO.registrar(documentoNuevoCroquis);
						listaDocumento.add(documentoNuevoCroquis);
						archivoDTO.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
																				  .filter(p-> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString()))
																				  .findFirst()
																				  .get()
																				  .getIdMaestro()));
						Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
						archivo.setDocumento(documentoNuevoCroquis);
						archivoDAO.registrar(archivo);
					} else {
						Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
						if (StringUtil.isNotNullOrBlank(documento.getIndCambioArchivos()) && 
								!documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())) {
							for (Documento docV : listaDocumentoViejo) {
								for(Archivo ar : docV.getListaArchivo()){
									ar.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
									ar.setUsuarioUltimaModificacion(solicitud.getUsuarioModificacion());
									ar.setEstado(EstadoState.INACTIVO.getValue());
									archivoDAO.update(ar);
								}
							}
							if (Objects.nonNull(documentoViejoXIndCambioArchivos)) {																
								for(Archivo ar1 : documentoViejoXIndCambioArchivos.getListaArchivo()){
									ar1.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
									ar1.setUsuarioUltimaModificacion(solicitud.getUsuarioModificacion());
									ar1.setEstado(EstadoState.INACTIVO.getValue());
									archivoDAO.update(ar1);
								}								
							}							
						}
						archivo.setDocumento(documentoNuevo);
						archivoDAO.registrar(archivo);				
					}
				}
			} catch (Exception e) {
				throw new ArchivoSolicitudNoRegistradoException();
			}
			// registro SolicitudDocumento
			for (Documento doc : listaDocumento) {
				SolicitudDocumentoPK pk = new SolicitudDocumentoPK(doc.getIdDocumento(), solicitudNuevo.getIdSolicitud());
				SolicitudDocumento solicitudDocumento = new SolicitudDocumento();				
				if (StringUtil.isNotNullOrBlank(doc.getIndCambioDocumentoExistente())) {
					solicitudDocumento.setSolicitudDocumentoPK(pk);
					solicitudDocumento.setEstado(EstadoState.ACTIVO.getValue());
					solicitudDocumento.setUsuarioUltimaModificacion(documento.getUsuarioCreacion());
					solicitudDocumento.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
					solicitudDocumentoDAO.update(solicitudDocumento);
				}else{
					solicitudDocumento.setSolicitudDocumentoPK(pk);
					solicitudDocumento.setFechaCreacion(FechaUtil.obtenerFechaActual());
					solicitudDocumento.setUsuarioCreacion(documento.getUsuarioCreacion());
					solicitudDocumento.setEstado(EstadoState.ACTIVO.getValue());
					solicitudDocumento = solicitudDocumentoDAO.registrarSolicitudDocumentoNuevo(solicitudDocumento);
				}
			}			
		} catch (Exception e) {
			throw new DocumentoSolicitudNoRegistradoException();
		}
		if (solicitud.getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.SUSTENTADO.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro())) {
			//Desactivar todos los campos observados de la solicitud
			List<RevisionSolicitudCampos> listaRevisionSolicitudCampos  = new ArrayList<RevisionSolicitudCampos>();
			
			listaRevisionSolicitudCampos = revisionSolicitudCamposDAO.findByIdSolicitud(solicitud.getIdSolicitud());
			if (Objects.nonNull(listaRevisionSolicitudCampos) && !listaRevisionSolicitudCampos.isEmpty()) {
				for (RevisionSolicitudCampos rev : listaRevisionSolicitudCampos) {
					rev.setEstado(EstadoState.INACTIVO.getValue());
					rev.setEstadoEvaluacion(EstadoState.INACTIVO.getValue());
					rev.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
					rev.setUsuarioModificacion(solicitud.getUsuarioModificacion());
					revisionSolicitudCamposDAO.update(rev);					
				}
			}
		}
		SolicitudDTO solicitudNuevoDTO = ConversorHelper.convertir(SolicitudDTO.class, solicitudNuevo);
		return solicitudNuevoDTO;
	}	
	
	@SuppressWarnings("rawtypes")
	public RespuestaDTO procesarRevision(SolicitudDTO solicitud) throws Exception {
		Map<Object, Object> parametros = new HashMap<>();
		RespuestaDTO respuesta = null;
		try {
			if ((solicitud.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																									  .filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
																									  .findFirst()
																									  .get()
																									  .getIdMaestro())
				||
				solicitud.getTipoSituacionRevisionSig().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																										  .filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
																										  .findFirst()
																										  .get()
																										  .getIdMaestro()))
				&&
				(StringUtil.isNotNullOrBlank(solicitud.getTipoSituacionRevision().getIdMaestro().toString())
						&& StringUtil.isNotNullOrBlank(solicitud.getTipoSituacionRevisionSig().getIdMaestro().toString()))
				&& 
				(!solicitud.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																										.filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.INICIO.getKey()))
																										.findFirst()
																										.get()
																										.getIdMaestro()) 
				&& !solicitud.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																											.filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
																											.findFirst()
																											.get()
																											.getIdMaestro())
				&& !solicitud.getTipoSituacionRevisionSig().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																											.filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.INICIO.getKey()))
																											.findFirst()
																											.get()
																											.getIdMaestro()) 
				&& !solicitud.getTipoSituacionRevisionSig().getIdMaestro().equals(listaTipoSituacionRevision.stream()
																											.filter(p-> p.getCodigoItem().equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
																											.findFirst()
																											.get()
																											.getIdMaestro()))){
				respuesta = gestionAccionSolicitud(listaTipoSituacionSolicitud.stream()
																			  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()
																			  .toString(), 
												   solicitud.getCodSolicitud(), 
												   null, 
												   solicitud.getCodigoModular(), 
												   null,
												   null);
			}
		} catch (Exception e) {			
			parametros.put("succes", false);
			parametros.put("solicitudActualizada", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema con el servicio.']", parametros);
			return respuesta;
		}
		return respuesta;
	}
	
	//imendoza 20170202 inicio
	private Documento buscarResolucion(Documento doc){
		List<Documento> listDoc;
		Documento docum = null;
		//Buscamos si la resolucion ya existe
		String numeroDocumento = doc.getNroDocumento().replaceFirst ("^0*", "");
		listDoc  = documentoDAO.buscarDocumento(doc.getCodooii(), 
														doc.getTipoDocumentoResolucion().getIdMaestro().toString(), 
														numeroDocumento, 
														FechaUtil.obtenerFechaFormatoPersonalizado(doc.getFechaDocumento(), "yyyy-MM-dd"));
		//Verificamos si hubo resultado, resolucion existente
		if(Objects.nonNull(listDoc) && !listDoc.isEmpty()){
			listDoc = listDoc.stream()
							 .filter(p-> p.getEstado().equals(EstadoState.ACTIVO.getValue()))
							 .collect(Collectors.toList());
			docum = listDoc.get(0);//Si existe la resolucion
		}	
		return docum;
	}
	
	private SolicitudDocumento buscarSolicitudDocumentoByTipo(Long idSolicitud, String tipoDocumentoType){
		SolicitudDocumento solDoc = null;
		List<SolicitudDocumento> listSolDoc = null;
		listSolDoc = solicitudDocumentoDAO.findByIdSolicitud(idSolicitud);
		Long idMaestro = listaTipoDocumento.stream()
											.filter(d-> d.getCodigoItem()
													.equals(tipoDocumentoType))
													.findFirst()
													.get()
													.getIdMaestro();		
		try {
			solDoc = listSolDoc.stream()
								.filter(p-> (p.getDocumento().getTipoDocumento().getIdMaestro().equals(idMaestro)) &&  
											(p.getEstado().equals(EstadoState.ACTIVO.getValue())))
								.findFirst()
								.get();
		} catch (NoSuchElementException e) {
		}		
		return solDoc;
	}
	
	private SolicitudDocumento buscarSolicitudDocumentoByTipoYDocumento(Long idSolicitud, String tipoDocumentoType, Documento documento){
		SolicitudDocumento solDoc = null;
		List<SolicitudDocumento> listSolDoc = new ArrayList<SolicitudDocumento>();
		//listSolDoc = solicitudDocumentoDAO.findByIdSolicitud(idSolicitud);
		solDoc = solicitudDocumentoDAO.findById(new SolicitudDocumentoPK(documento.getIdDocumento(), idSolicitud));
		if (Objects.nonNull(solDoc)) {
			listSolDoc.add(solDoc);
		}
		Long idMaestro = listaTipoDocumento.stream()
											.filter(d-> d.getCodigoItem()
													.equals(tipoDocumentoType))
													.findFirst()
													.get()
													.getIdMaestro();		
		try {
			solDoc = listSolDoc.stream()
								.filter(p-> (p.getDocumento().getTipoDocumento().getIdMaestro().equals(idMaestro)) &&  
											(p.getDocumento().getIdDocumento().equals(documento.getIdDocumento()))
										)
								.findFirst()
								.get();
		} catch (NoSuchElementException e) {
		}		
		return solDoc;
	}



@Override
public SolicitudDTO verificaSituacSolicitud(String codSolicitud, String tipo) throws Exception {
	Map<Object, Object> parametros=new HashMap<>();
	List<SolicitudDTO> solicitudes=new ArrayList<SolicitudDTO>();
	SolicitudDTO solicituddto=new SolicitudDTO();
	
	try{
//		if (StringUtil.isNotNullOrBlank(codSolicitud))parametros.put("codigoModular", codSolicitud.toUpperCase(ResourceUtil.obtenerLocaleSession()));	
		if (StringUtil.isNotNullOrBlank(codSolicitud))parametros.put("codSolicitud", codSolicitud.toUpperCase(ResourceUtil.obtenerLocaleSession()));
//		if (StringUtil.isNotNullOrBlank(codigoModular))parametros.put("codigoModular", codigoModular);
		parametros.put("estado", EstadoState.ACTIVO.getValue());
		solicitudes=listarSolicitudFirstMaxResultParametros(100, 0, parametros);
		
		/// Modificacion de solicitudes
		if(tipo.trim().equals("120023")){
			if ((solicitudes.get(0).getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
															  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey()))
															  .findFirst()
															  .get()
															  .getIdMaestro())))
					{
						
										solicituddto.setCorrecto("ok");
					}else{
										solicituddto.setCorrecto("error");
					}
		}
		
		///sustentacion de solicitudes
		if(tipo.trim().equals("120012")){
				if ((solicitudes.get(0).getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
																  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey()))
																  .findFirst()
																  .get()
																  .getIdMaestro())))
						{
							
										solicituddto.setCorrecto("ok");
						}else{
										solicituddto.setCorrecto("error");
							}
				
		}
	
		/// Anulación de solicitudes
		if(tipo.trim().equals("120001")){
			if ((solicitudes.get(0).getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
															  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.REVISION.getKey()))
															  .findFirst()
															  .get()
															  .getIdMaestro())))
					{
						
										solicituddto.setCorrecto("error");//EN REVISION, NO SE PUEDE ANULAR
					}else{
										solicituddto.setCorrecto("ok");
					}
		}
		
	}catch(Exception e){
		
		
	}
	return solicituddto;
}


@Override
public SolicitudDTO verificaCierreDeshacerAprobacionSolicitud(String codSolicitud, String tipo, String tipoSolicitud) throws Exception {
	Map<Object, Object> parametros=new HashMap<>();
	List<SolicitudDTO> solicitudes=new ArrayList<SolicitudDTO>();
	SolicitudDTO solicituddto=new SolicitudDTO();
	
	try{
		if (StringUtil.isNotNullOrBlank(codSolicitud))parametros.put("codSolicitud", codSolicitud.toUpperCase(ResourceUtil.obtenerLocaleSession()));	
				
		parametros.put("estado", EstadoState.ACTIVO.getValue());
		solicitudes=listarSolicitudFirstMaxResultParametros(100, 0, parametros);
		boolean isAprobado=false;
		///solicitudes aprobadas
		if(tipo.trim().equals("true")){
				if ((solicitudes.get(0).getTipoSituacionSolicitud().getIdMaestro().equals(listaTipoSituacionSolicitud.stream()
																  .filter(p-> p.getCodigoItem().equals(TipoSituacionSolicitudType.APROBADO.getKey()))
																  .findFirst()
																  .get()
																  .getIdMaestro())))
						{
										isAprobado=true;
										
						}else{
										isAprobado=false;
							}
				
		}else{
//			solicituddto.setCorrecto("ok");
			solicituddto.setCorrecto("ohpositivo");
		}
		//SolicitudDTO solicitud
		if(isAprobado){
			// solicitud de cierre
			MaestroDTO madto=new MaestroDTO(Long.valueOf(tipoSolicitud));
			solicituddto.setTipoSolicitud(madto);
			  if (solicituddto.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
					  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey().toString()))
					  .findFirst()
					  .get().getIdMaestro())) {
				  				solicituddto.setCorrecto("ohnegativo");
							}
			 
			  if (!solicituddto.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
					  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey().toString()))
					  .findFirst()
					  .get().getIdMaestro())) {
				  				solicituddto.setCorrecto("ohpositivo");
							}
			  
		}
	
	}catch(Exception e){
		
		
	}
	return solicituddto;
}

	
}
