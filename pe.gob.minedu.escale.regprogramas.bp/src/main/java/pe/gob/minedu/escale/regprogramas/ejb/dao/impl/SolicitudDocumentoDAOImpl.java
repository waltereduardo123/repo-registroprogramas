package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumento;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumentoPK;

@Stateless
public class SolicitudDocumentoDAOImpl 	extends AbstractJtaStatelessCRUDServices<SolicitudDocumentoPK, SolicitudDocumento>	implements SolicitudDocumentoDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate
	
	public EntityManager getEntityManager() {
		return em;
	}
	
	public SolicitudDocumentoDAOImpl() {
	}

	@Override
	public void save(SolicitudDocumento entity) {
		em.persist(entity);
		em.flush();
	}

	@Override
	public void update(SolicitudDocumento entity) {
		inciarSessionFactory();
		SolicitudDocumento sol = getSess().get(SolicitudDocumento.class, entity.getSolicitudDocumentoPK());
		try {
			ConversorHelper.fusionaPropiedades(entity, sol);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(sol);
	}

	@Override
	public void delete(SolicitudDocumento entity) {
		em.remove(entity);
	}

	@Override
	public SolicitudDocumento registrarSolicitudDocumentoNuevo(SolicitudDocumento entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@Override
	public SolicitudDocumento findById(SolicitudDocumentoPK id) {
		return em.find(SolicitudDocumento.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<SolicitudDocumento> findByIdSolicitud(Long id) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM det_regpro_sol_doc m ");
		sbQuery.append("WHERE m.N_ID_SOLICITUD = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), SolicitudDocumento.class);

		query.setParameter(1, id);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	//mejorar codigo y generalizarlo
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	//mejorar codigo y generalizarlo
}
