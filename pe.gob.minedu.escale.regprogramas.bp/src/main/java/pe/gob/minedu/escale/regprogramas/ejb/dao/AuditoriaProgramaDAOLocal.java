package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.AuditoriaPrograma;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface AuditoriaProgramaDAOLocal extends StatelessCRUDServices<Long, AuditoriaPrograma> {

	void save(AuditoriaPrograma entity);
	void update(AuditoriaPrograma entity);
	void delete(AuditoriaPrograma entity);
	AuditoriaPrograma registrar(AuditoriaPrograma entity); 
	AuditoriaPrograma registrarNoFlush(AuditoriaPrograma entity); //
	AuditoriaPrograma findById(Long id);
	List<AuditoriaPrograma> buscarProgramasxCodooii(char mander);
	List<AuditoriaPrograma> buscarProgramasxNombre(String nombrePrograma);
	List<AuditoriaPrograma> buscarProgramasxEstado(String estado);
	List<AuditoriaPrograma> findAll();
	List<AuditoriaPrograma> findByFirstMaxResult(int batchSize, int index);
	@SuppressWarnings("rawtypes")
	List<AuditoriaPrograma> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
	List<AuditoriaPrograma> buscarCodigoModular(String codigoModular);
	List<AuditoriaPrograma> buscarProgramasxCodigoModular(String codigoModular);
}
