package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprograma.business.exception.DocumentoNoActualizadoException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoActualizadaException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.DocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.DocumentoServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;

@PermitAll
@Stateless(name = "DocumentoService", mappedName = "ejb/DocumentoService")
public class DocumentoServiceImpl implements DocumentoServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private DocumentoDAOLocal documentoDAO;

	@SuppressWarnings("unchecked")
	public List<DocumentoDTO> listarDocumento() throws Exception {
		List<Documento> lista = documentoDAO.findAll();
		List<DocumentoDTO> listDocumento = ConversorHelper.convertirTodo(DocumentoDTO.class, lista);
		return listDocumento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoDTO> buscarDocumentoxId(Long idDocumento) throws Exception {
		List<Documento> lista = documentoDAO.findByIdDocumento(idDocumento);
		List<DocumentoDTO> listDocumento = ConversorHelper.convertirTodo(DocumentoDTO.class, lista);
		return listDocumento;
	}

	@Override
	public DocumentoDTO registrarDocumentoNuevo(DocumentoDTO documento) throws Exception {
		Documento documentoNuevo = ConversorHelper.convertir(Documento.class, documento);
		documentoNuevo = documentoDAO.registrar(documentoNuevo);
		DocumentoDTO documentoNuevoDTO = ConversorHelper.convertir(DocumentoDTO.class, documentoNuevo);
		return documentoNuevoDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoDTO> buscarDocumento(String codooii, String tipoResolucion, String numDocumento,
			String fechaDocumento) throws Exception {
		List<Documento> lista = documentoDAO.buscarDocumento(codooii, tipoResolucion, numDocumento, fechaDocumento);
		List<DocumentoDTO> listDocumento = ConversorHelper.convertirTodo(DocumentoDTO.class, lista);
		return listDocumento;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<DocumentoDTO> listarDocumentoFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<Documento> listaDocumento = documentoDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<DocumentoDTO> listaDocumentoDTO = ConversorHelper.convertirTodo(DocumentoDTO.class, listaDocumento);
		return listaDocumentoDTO;
	}
	
	@Override
	public DocumentoDTO actualizar(DocumentoDTO solicitud) throws Exception, DocumentoNoActualizadoException {
		Documento entity = ConversorHelper.convertir(Documento.class, solicitud);
		try {
			documentoDAO.update(entity);
		} catch (Exception e) {
			throw new SolicitudNoActualizadaException();
		}		
		DocumentoDTO entityDTO = ConversorHelper.convertir(DocumentoDTO.class, entity);
		return entityDTO;
	}
}
