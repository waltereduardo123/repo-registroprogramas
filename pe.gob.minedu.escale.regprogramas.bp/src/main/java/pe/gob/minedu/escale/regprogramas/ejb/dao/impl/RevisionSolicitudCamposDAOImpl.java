package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.RevisionSolicitudCamposDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.RevisionSolicitudCampos;

@Stateless
public class RevisionSolicitudCamposDAOImpl extends AbstractJtaStatelessCRUDServices<Long, RevisionSolicitudCampos>
		implements RevisionSolicitudCamposDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate
	public EntityManager getEntityManager() {
		return em;
	}

	public RevisionSolicitudCamposDAOImpl() {
	}

	public void save(RevisionSolicitudCampos entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(RevisionSolicitudCampos entity) {
		inciarSessionFactory();
		RevisionSolicitudCampos sol = getSess().get(RevisionSolicitudCampos.class, entity.getIdSolcam());
		try {
			ConversorHelper.fusionaPropiedades(entity, sol);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(sol);
	}

	public void delete(RevisionSolicitudCampos entity) {
		em.remove(entity);
	}

	@SuppressWarnings("unchecked")
	public List<RevisionSolicitudCampos> findByIdRevisionSolicitudCampos(Long id) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM det_regpro_solcam m ");
		sbQuery.append("WHERE m.N_ID_SOLCAM = ?1  ");
		Query query = em.createNativeQuery(sbQuery.toString(), RevisionSolicitudCampos.class);

		query.setParameter(1, id);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<RevisionSolicitudCampos> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM det_regpro_solcam");
			query = em.createNativeQuery(sbQuery.toString(), RevisionSolicitudCampos.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}

	public RevisionSolicitudCampos registrar(RevisionSolicitudCampos entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<RevisionSolicitudCampos> findByIdSolicitud(Long idSolicitud) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM det_regpro_solcam m ");
		sbQuery.append("WHERE m.N_ID_SOLICITUD = ?1  ");
//		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), RevisionSolicitudCampos.class);

		query.setParameter(1, idSolicitud);
//		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@SuppressWarnings("rawtypes")
	public List<RevisionSolicitudCampos> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new RevisionSolicitudCampos(s.idSolcam, s.estadoEvaluacion, s.solicitud.idSolicitud,"
				+ " s.campos.idCampos, s.usuarioCreacion, s.estado)" + " FROM RevisionSolicitudCampos s ");
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND s.estado = :estado ");
		}

		List<RevisionSolicitudCampos> lista = synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		return lista;
	}

	// mejorar codigo y generalizarlo
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	// mejorar codigo y generalizarlo

}
