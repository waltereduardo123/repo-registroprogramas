package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.DreGeo;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface DreGeoDAOLocal extends StatelessCRUDServices<String, DreGeo> {

	void save(DreGeo entity);
	void update(DreGeo entity);
	void delete(DreGeo entity);
	DreGeo registrar(DreGeo entity);
//	List<DreGeo> findById(String idDreUgel, String idDistrito);
	List<DreGeo> findByIdDreUgel(String idDreUgel);
//	List<DreGeo> buscarMaestrosxPadre(Long id);
//	List<DreGeo> buscarMaestrosxGrupo(String codigoAgrupacion);
//	List<DreGeo> buscarMaestrosxNombre(String nombreMaestro);
	List<DreGeo> findAll();
}
