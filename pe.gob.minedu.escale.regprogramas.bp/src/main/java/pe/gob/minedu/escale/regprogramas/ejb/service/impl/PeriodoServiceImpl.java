package pe.gob.minedu.escale.regprogramas.ejb.service.impl;



import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprograma.business.exception.PeriodoNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoActualizadoException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.PeriodoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.PeriodoServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.PeriodoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.PeriodoBandejaDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Periodo;



@PermitAll
@Stateless(name = "PeriodoService", mappedName = "ejb/PeriodoService")
public class PeriodoServiceImpl implements PeriodoServiceLocal {

	@EJB
	private PeriodoDAOLocal periodoDAO;
	
	
	
	@Override
	public PeriodoDTO registrarNuevoPeriodo(PeriodoDTO periodo) throws Exception {
		try {	
			Periodo entity = ConversorHelper.convertir(Periodo.class, periodo);
			entity = periodoDAO.registrarPeriodo(entity);
			periodo = ConversorHelper.convertir(PeriodoDTO.class, entity);
		} catch (Exception e) {
			throw new PeriodoNoRegistradoException( periodo.getCodGrupo() );
		}
		return periodo;
	}

	@Override
	public PeriodoDTO actalizarPeriodo(PeriodoDTO periodo) throws Exception {
		Periodo entity = ConversorHelper.convertir(Periodo.class, periodo);
		try {
			periodoDAO.actualizarPerodo(entity);
		} catch (Exception e) {
			throw new ProgramaNoActualizadoException(entity.getCodGrupo());
		}		
		PeriodoDTO entityDTO = ConversorHelper.convertir(PeriodoDTO.class, entity);
		return periodo;		
	}

	@SuppressWarnings("unchecked")
	public List<PeriodoDTO> buscarPeriodoxNombreItem(String nombrePeriodo, String item) throws Exception {
			List<Periodo> lista = periodoDAO.buscarPeriodoxNombreItem(nombrePeriodo, item);
			List<PeriodoDTO> listPeriodo = ConversorHelper.convertirTodo(PeriodoDTO.class, lista);
			return listPeriodo;
		
	}

	@SuppressWarnings("unchecked")
	public List<PeriodoDTO> listPeriodo() throws Exception {
		List<Periodo> lista = periodoDAO.listPeriodo();
		List<PeriodoDTO> listPeriodo = ConversorHelper.convertirTodo(PeriodoDTO.class, lista);
		return listPeriodo;
	}

	
	@SuppressWarnings("unchecked")
	public List<PeriodoDTO> listPeriodoActivo() throws Exception {
		List<Periodo> lista = periodoDAO.listPeriodoActivo();
		List<PeriodoDTO> listPeriodo = ConversorHelper.convertirTodo(PeriodoDTO.class, lista);
		return listPeriodo;
	}
	
	public List<PeriodoBandejaDTO> grillaPeriodo(int index, int batchSize, String jpqlRules, String jpqlOrder) throws Exception {
		List<PeriodoBandejaDTO> listaRetorno=new ArrayList<PeriodoBandejaDTO>();
		List<Periodo> lista = new ArrayList<Periodo>();
		lista =	periodoDAO.grillaPeriodo(index,batchSize,  jpqlRules, jpqlOrder);
		for(Periodo per: lista){
			PeriodoBandejaDTO perdto=new PeriodoBandejaDTO();
			perdto.setIdPeriodo(per.getIdPeriodo());
			perdto.setNamePeriodo(per.getNamePeriodo());
			perdto.setFechaInicioPeriodo(per.getFechaInicioPeriodo());
			perdto.setFechaFinPeriodo(per.getFechaFinPeriodo());
			perdto.setFechaFinAnio(per.getFechaFinAnio());
			perdto.setEstado(per.getEstado());
			perdto.setCodItem(per.getCodItem());
			listaRetorno.add(perdto);
		}
		return listaRetorno;
	}

	
	

}
