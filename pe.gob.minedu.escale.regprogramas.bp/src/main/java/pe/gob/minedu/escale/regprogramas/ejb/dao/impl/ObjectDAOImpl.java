package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ObjectDAOLocal;

@Stateless
public class ObjectDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Object> implements ObjectDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	public ObjectDAOImpl() {
	}

	public void save(Object entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(Object entity) {
		em.merge(entity);
	}

	public void delete(Object entity) {
		em.remove(entity);
	}

	public Object registrar(Object entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public List<Object> findByIdObject(Long idObject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> findAll() {
		
		return null;
	}

	@Override
	public List<Object> findByFirstMaxResult(int batchSize, int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Object> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {

		return null;
	}

	
	@Override
	public List<Object> findByFirstMaxResultJson(String jpql, String jpqlRules, String jpqlTable, String jpqlOrder,
			int batchSize, int index) {
		List<Object> lista = new ArrayList<Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select " + jpql + " from " + jpqlTable);
		sbQuery.append(" WHERE 1=1 ");
		if (StringUtil.isNotNullOrBlank(jpqlRules)) {
			sbQuery.append(" AND " + jpqlRules);
		}
		if (StringUtil.isNotNullOrBlank(jpqlOrder)) {
			sbQuery.append(" ORDER BY " + jpqlOrder);
		}

		lista = synchronizedFind(sbQuery.toString(), batchSize, index, null);

		int count = synchronizedFind(sbQuery.toString()).size();
		lista.add(count);  
		return lista;
	}

	
	
	
	
	
	
	
	
	
	
	@Override
	public List<Object[]> findByFirstMaxResultGenerico(String jpql, String jpqlRules, String jpqlTable,
			String jpqlOrder, int batchSize, int index) {
		List<Object[]> lista = new ArrayList<Object[]>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select " + jpql + " from " + jpqlTable);
		sbQuery.append(" WHERE 1=1 ");
		if (StringUtil.isNotNullOrBlank(jpqlRules)) {
			sbQuery.append(" AND " + jpqlRules);
		}
		if (StringUtil.isNotNullOrBlank(jpqlOrder)) {
			sbQuery.append(" ORDER BY " + jpqlOrder);
		}

		lista = synchronizedFindGenerico(sbQuery.toString(), batchSize, index, null);

		return lista;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Object> controlDeAvance(Map parameters, String tipo) {
		List<Object> lista = new ArrayList<Object>();

		
		StringBuilder sbQuery = new StringBuilder();
		if (StringUtil.isNotNullOrBlank(tipo)) {
			switch (tipo) {
			case "1":
			case "2":
				sbQuery.append(" SELECT m.C_CODITEM, m.C_NOM,tm.CREACION,tm.MODIFICACION,tm.RENOVACION,tm.CIERRE FROM  ");
				sbQuery.append(" tbl_regpro_maestro m ");
				sbQuery.append(" LEFT JOIN ( ");
				sbQuery.append("	SELECT 	a.N_ID_TIPSITU,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?1,1,0)) AS CREACION,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?2,1,0)) AS MODIFICACION,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?3,1,0)) AS RENOVACION,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?4,1,0)) AS CIERRE");
				sbQuery.append(
						"	FROM tbl_regpro_solicitud a LEFT JOIN tbl_regpro_maestro ma ON a.N_ID_TIPSOLI=ma.N_ID_MAESTRO");
				sbQuery.append("	WHERE LEFT(CODIGO,6) = ?5");
			   if(StringUtil.isNotNullOrBlank(parameters.get(6))){
					if (!parameters.get(6).equals("0")) {
						sbQuery.append(" 	AND a.N_ID_TIPOPERIODO = ?6");					
						}
			   }	
				sbQuery.append("	AND ma.C_CODGRUP = 27");
				sbQuery.append("	GROUP BY a.N_ID_TIPSITU");
				sbQuery.append(" ) AS tm ON m.N_ID_MAESTRO = tm.N_ID_TIPSITU ");
				sbQuery.append(" WHERE 1=1 ");
				if (StringUtil.isNotNullOrBlank(tipo)) {
					if (tipo.equals("1")) {
						sbQuery.append(" AND m.C_CODGRUP = 28 AND m.C_CODITEM IN (2801,2802,2803,2806) ");
					} else if (tipo.equals("2")) {
						//sbQuery.append(" AND m.C_CODGRUP = 28 AND m.C_CODITEM IN (2804,2805,2807) ");
						sbQuery.append(" AND m.C_CODGRUP = 28 AND m.C_CODITEM IN (2804,2805,2807) ");
					}
				}
//				if (StringUtil.isNotNullOrBlank(parameters.get(6))) {
//					parameters.replace(6, parameters.get(6).toString().concat("%"));					
//				}
				break;
			case "3":
				sbQuery.append(" SELECT m.C_CODITEM, m.C_NOM,tm.CICLO_I_ENTORNO_FAMILIAR,tm.CICLO_I_ENTORNO_COMUNITARIO,");
				sbQuery.append("	tm.CICLO_I_SET,tm.CICLO_II_ENTORNO_FAMILIAR,tm.CICLO_II_ENTORNO_COMUNITARIO,tm.TIPOS_ANTIGUO ");
				sbQuery.append("    ,SUM(tm.CICLO_I_ENTORNO_FAMILIAR+ ");
				sbQuery.append("		tm.CICLO_I_ENTORNO_COMUNITARIO+ ");
				sbQuery.append("		tm.CICLO_I_SET+");
				sbQuery.append("		tm.CICLO_II_ENTORNO_FAMILIAR+ ");
				sbQuery.append("		tm.CICLO_II_ENTORNO_COMUNITARIO+ ");
				sbQuery.append("		tm.TIPOS_ANTIGUO) AS TOTAL ");
				sbQuery.append(" FROM tbl_regpro_maestro m ");
				sbQuery.append("  LEFT JOIN ( " );
				sbQuery.append(" SELECT 	a.N_ID_TIPSITU, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?1 ,1,0)) AS CICLO_I_ENTORNO_FAMILIAR, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?2 ,1,0)) AS CICLO_I_ENTORNO_COMUNITARIO, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?3 ,1,0)) AS CICLO_I_SET, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?4 ,1,0)) AS CICLO_II_ENTORNO_FAMILIAR, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?5 ,1,0)) AS CICLO_II_ENTORNO_COMUNITARIO, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM IN ('1','2','3','4','5','6','7','8','9'),1,0)) AS TIPOS_ANTIGUO ");
				sbQuery.append(" 	FROM tbl_regpro_programa a LEFT JOIN tbl_regpro_maestro ma ON a.N_ID_TIPPROG=ma.N_ID_MAESTRO ");
				sbQuery.append(" 	WHERE CODOOII = ?6 ");
				sbQuery.append(" 	AND ma.C_CODGRUP = 1 ");
				sbQuery.append(" 	GROUP BY a.N_ID_TIPSITU ");
				sbQuery.append("  ) AS tm ON m.N_ID_MAESTRO = tm.N_ID_TIPSITU  ");
				sbQuery.append("  WHERE 1=1 ");
				sbQuery.append("   AND m.C_CODGRUP = 31 AND m.C_CODITEM IN ('3101','3102','3103','3104','3105','3106') /*tipo situacion programa */");
				sbQuery.append(" GROUP BY m.C_CODITEM, m.C_NOM,tm.CICLO_I_ENTORNO_FAMILIAR,tm.CICLO_I_ENTORNO_COMUNITARIO, tm.CICLO_I_SET,tm.CICLO_II_ENTORNO_FAMILIAR,tm.CICLO_II_ENTORNO_COMUNITARIO,tm.TIPOS_ANTIGUO");
				break;
			case "4":
				sbQuery.append(" SELECT m.C_CODITEM, m.C_NOM,tm.CICLO_I_ENTORNO_FAMILIAR,tm.CICLO_I_ENTORNO_COMUNITARIO,");
				sbQuery.append("	tm.CICLO_I_SET,tm.CICLO_II_ENTORNO_FAMILIAR,tm.CICLO_II_ENTORNO_COMUNITARIO,tm.TIPOS_ANTIGUO ");
				sbQuery.append("    ,SUM(tm.CICLO_I_ENTORNO_FAMILIAR+ ");
				sbQuery.append("		tm.CICLO_I_ENTORNO_COMUNITARIO+ ");
				sbQuery.append("		tm.CICLO_I_SET+");
				sbQuery.append("		tm.CICLO_II_ENTORNO_FAMILIAR+ ");
				sbQuery.append("		tm.CICLO_II_ENTORNO_COMUNITARIO+ ");
				sbQuery.append("		tm.TIPOS_ANTIGUO) AS TOTAL ");
				sbQuery.append(" FROM tbl_regpro_maestro m ");
				sbQuery.append("  LEFT JOIN ( " );
				sbQuery.append(" SELECT 	a.N_ID_TIPSITU, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?1 ,1,0)) AS CICLO_I_ENTORNO_FAMILIAR, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?2 ,1,0)) AS CICLO_I_ENTORNO_COMUNITARIO, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?3 ,1,0)) AS CICLO_I_SET, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?4 ,1,0)) AS CICLO_II_ENTORNO_FAMILIAR, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?5 ,1,0)) AS CICLO_II_ENTORNO_COMUNITARIO, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM IN ('1','2','3','4','5','6','7','8','9'),1,0)) AS TIPOS_ANTIGUO ");
				sbQuery.append(" 	FROM tbl_regpro_programa a LEFT JOIN tbl_regpro_maestro ma ON a.N_ID_TIPPROG=ma.N_ID_MAESTRO ");
				sbQuery.append(" 	WHERE ma.C_CODGRUP = 1 ");
				sbQuery.append(" 	GROUP BY a.N_ID_TIPSITU ");
				sbQuery.append("  ) AS tm ON m.N_ID_MAESTRO = tm.N_ID_TIPSITU  ");
				sbQuery.append("  WHERE 1=1 ");
				sbQuery.append("   AND m.C_CODGRUP = 31 AND m.C_CODITEM IN ('3101','3102','3103','3104','3105','3106') /*tipo situacion programa */");
				sbQuery.append(" GROUP BY m.C_CODITEM, m.C_NOM,tm.CICLO_I_ENTORNO_FAMILIAR,tm.CICLO_I_ENTORNO_COMUNITARIO, tm.CICLO_I_SET,tm.CICLO_II_ENTORNO_FAMILIAR,tm.CICLO_II_ENTORNO_COMUNITARIO,tm.TIPOS_ANTIGUO");
				break;
			//imendoza 20170206 inicio
			case "5":
				sbQuery.append(" SELECT m.C_CODITEM, m.C_NOM,tm.CICLO_I_ENTORNO_FAMILIAR,tm.CICLO_I_ENTORNO_COMUNITARIO,");
				sbQuery.append("	tm.CICLO_I_SET,tm.CICLO_II_ENTORNO_FAMILIAR,tm.CICLO_II_ENTORNO_COMUNITARIO,tm.TIPOS_ANTIGUO ");
				sbQuery.append("    ,SUM(tm.CICLO_I_ENTORNO_FAMILIAR+ ");
				sbQuery.append("		tm.CICLO_I_ENTORNO_COMUNITARIO+ ");
				sbQuery.append("		tm.CICLO_I_SET+");
				sbQuery.append("		tm.CICLO_II_ENTORNO_FAMILIAR+ ");
				sbQuery.append("		tm.CICLO_II_ENTORNO_COMUNITARIO+ ");
				sbQuery.append("		tm.TIPOS_ANTIGUO) AS TOTAL ");
				sbQuery.append(" FROM tbl_regpro_maestro m ");
				sbQuery.append("  LEFT JOIN ( " );
				sbQuery.append(" SELECT 	a.N_ID_TIPSITU, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?1 ,1,0)) AS CICLO_I_ENTORNO_FAMILIAR, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?2 ,1,0)) AS CICLO_I_ENTORNO_COMUNITARIO, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?3 ,1,0)) AS CICLO_I_SET, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?4 ,1,0)) AS CICLO_II_ENTORNO_FAMILIAR, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM = ?5 ,1,0)) AS CICLO_II_ENTORNO_COMUNITARIO, ");
				sbQuery.append(" 	SUM(IF(ma.C_CODITEM IN ('1','2','3','4','5','6','7','8','9'),1,0)) AS TIPOS_ANTIGUO ");
				sbQuery.append(" 	FROM tbl_regpro_programa a LEFT JOIN tbl_regpro_maestro ma ON a.N_ID_TIPPROG=ma.N_ID_MAESTRO ");
				sbQuery.append(" 	WHERE CODOOII LIKE (?6) ");
				sbQuery.append(" 	AND ma.C_CODGRUP = 1 ");
				sbQuery.append(" 	GROUP BY a.N_ID_TIPSITU ");
				sbQuery.append("  ) AS tm ON m.N_ID_MAESTRO = tm.N_ID_TIPSITU  ");
				sbQuery.append("  WHERE 1=1 ");
				sbQuery.append("   AND m.C_CODGRUP = 31 AND m.C_CODITEM IN ('3101','3102','3103','3104','3105','3106') /*tipo situacion programa */");
				sbQuery.append(" GROUP BY m.C_CODITEM, m.C_NOM,tm.CICLO_I_ENTORNO_FAMILIAR,tm.CICLO_I_ENTORNO_COMUNITARIO, tm.CICLO_I_SET,tm.CICLO_II_ENTORNO_FAMILIAR,tm.CICLO_II_ENTORNO_COMUNITARIO,tm.TIPOS_ANTIGUO");
				if (StringUtil.isNotNullOrBlank(parameters.get(6))) {
					parameters.replace(6, parameters.get(6).toString().concat("%"));					
				}
				break;
			case "6":
			case "7":
				sbQuery.append(" SELECT m.C_CODITEM, m.C_NOM,tm.CREACION,tm.MODIFICACION,tm.RENOVACION,tm.CIERRE FROM  ");
				sbQuery.append(" tbl_regpro_maestro m ");
				sbQuery.append(" LEFT JOIN ( ");
				sbQuery.append("	SELECT 	a.N_ID_TIPSITU,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?1,1,0)) AS CREACION,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?2,1,0)) AS MODIFICACION,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?3,1,0)) AS RENOVACION,");
				sbQuery.append("	SUM(IF(ma.C_CODITEM =?4,1,0)) AS CIERRE");
				sbQuery.append(
						"	FROM tbl_regpro_solicitud a LEFT JOIN tbl_regpro_maestro ma ON a.N_ID_TIPSOLI=ma.N_ID_MAESTRO");
				sbQuery.append("	WHERE LEFT(CODIGO,4) = ?5 ");
			//	sbQuery.append("	WHERE left(CODIGO,4) = ?5 ");
				if(StringUtil.isNotNullOrBlank(parameters.get(6))){
						if (!parameters.get(6).equals("0")) {
							sbQuery.append(" 	AND a.N_ID_TIPOPERIODO = ?6");					
							}
				   }	
				sbQuery.append("	AND ma.C_CODGRUP = 27");
				sbQuery.append("	GROUP BY a.N_ID_TIPSITU");
				sbQuery.append(" ) AS tm ON m.N_ID_MAESTRO = tm.N_ID_TIPSITU ");
				sbQuery.append(" WHERE 1=1 ");
				if (StringUtil.isNotNullOrBlank(tipo)) {
					if (tipo.equals("6")) {
						sbQuery.append(" AND m.C_CODGRUP = 28 AND m.C_CODITEM IN (2801,2802,2803,2806) ");
					} else if (tipo.equals("7")) {//Historial
						sbQuery.append(" AND m.C_CODGRUP = 28 AND m.C_CODITEM IN (2804,2805,2807) ");
						//sbQuery.append(" AND m.C_CODGRUP = 28 AND m.C_CODITEM IN (2804,2805,2807) ");
					}
				}
				
				
//				if (StringUtil.isNotNullOrBlank(parameters.get(5))) {
//					parameters.replace(5, parameters.get(5).toString().concat("%"));					
//				}
								
				break;
			//imendoza 20170206 fin
			default:
				break;
			}
			
		}
		

		lista = synchronizedNativeFind(sbQuery.toString(), parameters);

		return lista;
	}
}
