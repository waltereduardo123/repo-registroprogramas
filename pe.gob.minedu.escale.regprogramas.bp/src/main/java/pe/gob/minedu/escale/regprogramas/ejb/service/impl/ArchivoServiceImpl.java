package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ArchivoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ArchivoServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Archivo;

@PermitAll
@Stateless(name = "ArchivoService", mappedName = "ejb/ArchivoService")
public class ArchivoServiceImpl implements ArchivoServiceLocal {
	
	@EJB
	private ArchivoDAOLocal archivoDAO;

	@SuppressWarnings("unchecked")
	public List<ArchivoDTO> listarArchivo() throws Exception {
		List<Archivo> lista = archivoDAO.findAll();
		List<ArchivoDTO> listArchivo = ConversorHelper.convertirTodo(ArchivoDTO.class, lista);
		return listArchivo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ArchivoDTO> buscarArchivoxId(Long idArchivo) throws Exception {
		List<Archivo> lista = archivoDAO.findByIdArchivo(idArchivo);
		List<ArchivoDTO> listArchivo = ConversorHelper.convertirTodo(ArchivoDTO.class, lista);
		return listArchivo;
	}

	@Override
	public ArchivoDTO registrarArchivoNuevo(ArchivoDTO Archivo) throws Exception {
		Archivo ArchivoNuevo = ConversorHelper.convertir(Archivo.class, Archivo);
		ArchivoNuevo = archivoDAO.registrar(ArchivoNuevo);
		ArchivoDTO ArchivoNuevoDTO = ConversorHelper.convertir(ArchivoDTO.class, ArchivoNuevo);
		return ArchivoNuevoDTO;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ArchivoDTO> buscarArchivoxIdDocumento(Long idDocumento) throws Exception {
		List<Archivo> lista = archivoDAO.buscarArchivo(idDocumento);
		List<ArchivoDTO> listArchivo = ConversorHelper.convertirTodo(ArchivoDTO.class, lista);
		return listArchivo;
	}

	

}
