package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.RevisionSolicitudCampos;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface RevisionSolicitudCamposDAOLocal extends StatelessCRUDServices<Long, RevisionSolicitudCampos> {

	void save(RevisionSolicitudCampos entity);
	void update(RevisionSolicitudCampos entity);
	void delete(RevisionSolicitudCampos entity);
	RevisionSolicitudCampos registrar(RevisionSolicitudCampos entity);
	List<RevisionSolicitudCampos> findByIdRevisionSolicitudCampos(Long id);
	List<RevisionSolicitudCampos> findByIdSolicitud(Long idSolicitud);
	List<RevisionSolicitudCampos> findAll();
	@SuppressWarnings("rawtypes")
	List<RevisionSolicitudCampos> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
}
