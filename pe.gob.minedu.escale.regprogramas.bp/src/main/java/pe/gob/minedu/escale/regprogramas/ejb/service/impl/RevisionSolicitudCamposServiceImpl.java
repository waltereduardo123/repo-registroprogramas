package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprograma.business.exception.RevisionCamposSolicitudNoActualizadoException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.RevisionSolicitudCamposDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.RevisionSolicitudCamposServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.RevisionSolicitudCamposDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.RevisionSolicitudCampos;

@PermitAll
@Stateless(name = "RevisionSolicitudCamposService", mappedName = "ejb/RevisionSolicitudCamposService")
public class RevisionSolicitudCamposServiceImpl implements RevisionSolicitudCamposServiceLocal {

	@EJB
	private RevisionSolicitudCamposDAOLocal revisionSolicitudCamposDAO;
	

	
	@SuppressWarnings("unchecked")
	public List<RevisionSolicitudCamposDTO> listarRevisionSolicitudCampos() throws Exception {
		List<RevisionSolicitudCampos> lista = revisionSolicitudCamposDAO.findAll();
		List<RevisionSolicitudCamposDTO> list = ConversorHelper.convertirTodo(RevisionSolicitudCamposDTO.class, lista);
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RevisionSolicitudCamposDTO> buscarSolicitudCamposxIdSolicitud(Long idSolicitud) throws Exception {
		List<RevisionSolicitudCampos> lista = revisionSolicitudCamposDAO.findByIdSolicitud(idSolicitud);
		List<RevisionSolicitudCamposDTO> listSolicitud = ConversorHelper.convertirTodo(RevisionSolicitudCamposDTO.class, lista);
		return listSolicitud;
	}

	@Override
	public RevisionSolicitudCamposDTO registrar(RevisionSolicitudCamposDTO RevisionSolicitudCampos) throws Exception {
		RevisionSolicitudCampos entity = ConversorHelper.convertir(RevisionSolicitudCampos.class, RevisionSolicitudCampos);
		entity = revisionSolicitudCamposDAO.registrar(entity);
		RevisionSolicitudCamposDTO entityDTO = ConversorHelper.convertir(RevisionSolicitudCamposDTO.class, entity);
		return entityDTO;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<RevisionSolicitudCamposDTO> listarFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<RevisionSolicitudCampos> listaSolicitud = revisionSolicitudCamposDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCamposDTO = ConversorHelper.convertirTodo(RevisionSolicitudCamposDTO.class, listaSolicitud);
		return listaRevisionSolicitudCamposDTO;
	}

	@Override
	public List<RevisionSolicitudCamposDTO> listarByFirstMaxResult(int batchSize, int index) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public RevisionSolicitudCamposDTO actualizar(RevisionSolicitudCamposDTO revisision) throws Exception,RevisionCamposSolicitudNoActualizadoException {
		RevisionSolicitudCampos entity = ConversorHelper.convertir(RevisionSolicitudCampos.class, revisision);
		try {
			revisionSolicitudCamposDAO.update(entity);
		} catch (Exception e) {
			throw new RevisionCamposSolicitudNoActualizadoException();
		}		
		RevisionSolicitudCamposDTO entityDTO = ConversorHelper.convertir(RevisionSolicitudCamposDTO.class, entity);
		return entityDTO;
	}

}
