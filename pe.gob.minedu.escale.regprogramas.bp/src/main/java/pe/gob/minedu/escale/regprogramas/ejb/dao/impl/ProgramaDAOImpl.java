package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Programa;

@Stateless
public class ProgramaDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Programa> implements ProgramaDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate

	public EntityManager getEntityManager() {
		return em;
	}

	public ProgramaDAOImpl() {
	}

	public void save(Programa entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(Programa entity) {
		inciarSessionFactory();
		Programa pro = getSess().get(Programa.class, entity.getIdPrograma());
		try {
			ConversorHelper.fusionaPropiedades(entity, pro);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(pro);
	}

	public void delete(Programa entity) {
		em.remove(entity);
	}

	public Programa findById(Long id) {
		return em.find(Programa.class, id);
	}

	public Programa registrar(Programa entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<Programa> buscarProgramasxCodooii(char mander) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_programa m ");
		sbQuery.append("WHERE m.CODOOII = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class);

		query.setParameter(1, mander);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Programa> buscarProgramasxCodigoModular(String codigoModular) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_programa m ");
		sbQuery.append("WHERE m.C_CODMOD = ?1  ");
//		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class);

		query.setParameter(1, codigoModular);
//		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Programa> buscarProgramasCodigoModular(String codigoModular) {
		StringBuilder sbQuery = new StringBuilder();   

		sbQuery.append(" SELECT COUNT(*) ");
		sbQuery.append(" FROM tbl_regpro_programa m ");
		sbQuery.append(" WHERE m.C_CODMOD = ?1  ");
	//	Query query = em.createQuery(sbQuery.toString(), Programa.class);
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter(1, codigoModular);
		return query.getResultList();
	}
	
	@Override
	public List<Programa> buscarProgramasSituacionxCodigoModular(String codigoModular) {

		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_programa p  INNER JOIN tbl_regpro_maestro m ON p.N_ID_TIPSITU = m.N_ID_MAESTRO ");
		sbQuery.append("WHERE p.C_CODMOD = ?1  ");
//		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class);

		query.setParameter(1, codigoModular);
//		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
		
	}
	

	@SuppressWarnings("unchecked")
	public List<Programa> buscarProgramasxNombre(String nombrePrograma) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				"select new Programa(idPrograma,codigoModular, tipoPrograma, nombrePrograma, latitudPrograma, longitudPrograma, ");
		sbQuery.append(
				" tipoGestion,  tipoDependencia, tipoGestionEducativa, codooii, tipoTurno, tipoContinuidadJornadaEscolar,  ");
		sbQuery.append(" codgeo, marcoCensal, fechaCreacion, estado) ");
		sbQuery.append(" FROM Programa ");
		sbQuery.append(" where upper(nombrePrograma) like :nombrePrograma ");

		Query query = em.createQuery(sbQuery.toString(), Programa.class);

		query.setParameter("nombrePrograma", "%".concat(nombrePrograma.toUpperCase()).concat("%"));

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Programa> buscarProgramasxEstado(String estado) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				"select new Programa(idPrograma,codigoModular, tipoPrograma, nombrePrograma, latitudPrograma, longitudPrograma, ");
		sbQuery.append(
				" tipoGestion,  tipoDependencia, tipoGestionEducativa, codooii, tipoTurno, tipoContinuidadJornadaEscolar,  ");
		sbQuery.append(" codgeo, marcoCensal, fechaCreacion, estado) ");
		sbQuery.append(" FROM Programa  ");
		sbQuery.append(" WHERE estado = :estado ");

		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("estado", estado);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Programa> findAll() {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_programa m ");
		sbQuery.append("WHERE m.C_ESTADO = ?1 ");

		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class);

		query.setParameter(1, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@Override
	public List<Programa> findByFirstMaxResult(int batchSize, int index) {
		List<Programa> listaPrograma = findAll(Programa.class, batchSize, index);
		return listaPrograma;
	}

	
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<Programa> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(
				"select new Programa(p.idPrograma,p.codigoModular, p.nombrePrograma, p.tipoSituacion.idMaestro, p.tipoSituacion.nombreMaestro, p.tipoSituacion.codigoItem, "); //Se agrega codigo de item, por item del maestro werr  08112017
		sbQuery.append(
				" p.dreUgel.codigo, p.fechaCreacionPrograma,p.fechaCierrePrograma,p.fechaRenovacionPrograma,p.estado) ");
		sbQuery.append(" FROM Programa as p ");
		// sbQuery.append(" LEFT JOIN p.tipoSituacion as m ");
		// if(parameters.containsKey("tipoSituacion")){
		// sbQuery.append(" LEFT JOIN p.tipoSituacion as m ");
		// }
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("nombrePrograma") && StringUtil.isNotNullOrBlank(parameters.get("nombrePrograma"))) {
			sbQuery.append(" AND upper(p.nombrePrograma) = :nombrePrograma ");
		}
		if (parameters.containsKey("idPrograma")) {
			sbQuery.append(" AND p.idPrograma = :idPrograma ");
		}
		if (parameters.containsKey("codooii")) {
			sbQuery.append(" AND p.dreUgel.codigo = :codooii ");
		}
		if (parameters.containsKey("codigoModular")) {
			sbQuery.append(" AND p.codigoModular = :codigoModular ");
		}
		// if(parameters.containsKey("tipoSituacion")){
		// sbQuery.append(" AND m.codigoItem = :tipoSituacion");
		// }
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND p.estado = :estado ");
		}

		List<Programa> listaPrograma = synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		return listaPrograma;
	}
	
	@Override
public List<Programa> findByResultProgramasReinicio(Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		//sbQuery.append("SELECT  N_ID_PROGRAMA, C_CODMOD, N_ID_TIPPROG, C_NOMPROG, N_PROLAT, N_PROLON , N_ID_TIPGEST, N_ID_TIPDEPE, N_ID_TIPEGES, CODOOII, N_ID_TIPTURN, N_ID_TIPCJES, N_ID_TIPVIA, C_DIRNOMVIA,C_DIRNUMVIA, C_DIRMZ, C_DIRLOTE,N_ID_TIPLOCD, C_DIRETAP, C_DIRSECT, C_DIRZONA,C_DIROTRO,C_DIRREFE,CODGEO,C_CODCCPP,C_NOMCCPP,C_CSEMC,C_NOMSEMC,N_ID_TIPPRAG,C_OTRPRAGUA,C_SUMAGUA,N_ID_TIPPREN,C_OTRPRENER  FROM tbl_regpro_programa m ");
		sbQuery.append("SELECT * FROM tbl_regpro_programa m ");
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND m.C_ESTADO = :estado ");
		}
		//List<Programa> listaPrograma = synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		
		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class);

		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND m.C_ESTADO = :estado ");
			query.setParameter("estado", parameters.get("estado"));
		}

		
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<Programa> buscarCodigoModular(String codigoModular) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_programa m ");
		sbQuery.append("WHERE m.C_CODMOD = ?1  ");		
		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class);

		query.setParameter(1, codigoModular);		

		return query.getResultList();
	}
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	
	@Override
	public List<String> findByResultProgramasSincroniza(Map parameters) {
		List<String> lista = new ArrayList<String>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT DISTINCT C_CODCCPP FROM tbl_regpro_programa m ");
		sbQuery.append(" WHERE 1=1 ");
		sbQuery.append(" AND m.C_ESTADO IN  ( ?1, ?2 ) AND m.C_CODCCPP != '' AND m.C_CODCCPP IS NOT NULL ");
//		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class );
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter(1, parameters.get("estado"));
		query.setParameter(2, parameters.get("estadotwo"));
		lista = query.getResultList();
		return lista;
		
	}
	
	@Override
	public List<Programa> findByResultProgramasSincronizaUpdate(Map parameters) {
		List<Programa> lista = new ArrayList<Programa>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT * FROM tbl_regpro_programa m ");
		sbQuery.append(" WHERE 1=1 ");
		sbQuery.append(" AND m.C_CODCCPP = ?1 ");
		sbQuery.append(" AND m.C_ESTADO IN  ( ?2, ?3 ) AND m.C_CODCCPP != '' AND m.C_CODCCPP IS NOT NULL ");
//		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class ); 
		Query query = em.createNativeQuery(sbQuery.toString(), Programa.class );
		query.setParameter(1, parameters.get("centropoblado"));
		query.setParameter(2, parameters.get("estado"));
		query.setParameter(3, parameters.get("estadotwo"));
		lista = query.getResultList();
		return lista;
		
	}
	
	
	
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object> findProgramasByCentroPoblado(Map parameters) {
		List<Object> lista = new ArrayList<Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT C_CODMOD , N_ID_PROGRAMA FROM tbl_regpro_programa m ");
		sbQuery.append(" WHERE 1=1 ");
		sbQuery.append(" AND m.C_CODCCPP = ?1 ");
		sbQuery.append(" AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter(1, parameters.get("estado"));
		query.setParameter(2, parameters.get("centropoblado"));
		lista = query.getResultList();
		return lista;
	}




}
