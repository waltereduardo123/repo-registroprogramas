package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Maestro;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface MaestroDAOLocal extends StatelessCRUDServices<Long, Maestro> {

	void save(Maestro entity);
	void update(Maestro entity);
	void delete(Maestro entity);
	Maestro registrar(Maestro entity);
	Maestro findById(Long id);
	List<Maestro> buscarMaestrosxPadre(Long id);
	List<Maestro> buscarMaestrosxGrupo(String codigoAgrupacion);
	List<Maestro> buscarMaestrosxGrupoItem(String codigoAgrupacion, String item );
	List<Maestro> buscarMaestrosxNombre(String nombreMaestro);
	List<Maestro> buscarMaestrosxEstado(String estado);
	List<Maestro> findAll();
	String getMaxItemporGrupo(String codigoAgrupacion);
}
