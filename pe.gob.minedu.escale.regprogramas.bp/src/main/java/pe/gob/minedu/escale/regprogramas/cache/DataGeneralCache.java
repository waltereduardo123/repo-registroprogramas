package pe.gob.minedu.escale.regprogramas.cache;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.log4j.Logger;

import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;

@Singleton
@LocalBean
@Startup
public class DataGeneralCache implements Serializable {

	private static final long serialVersionUID = 6102811121955256443L;	

	private ConcurrentHashMap<Long, MaestroDTO> mapaMaestro;

	private ConcurrentHashMap<Long, MaestroDTO> mapaTipoSituacionRevision;
	
	@SuppressWarnings("unused")
	private ConcurrentHashMap<Long, MaestroDTO> mapaTipoEstadoPrograma;

	private static final Logger log = Logger.getLogger(DataGeneralCache.class);

	/**
	 * Fecha en la que se realiza la ultima sincronizacion del cache.
	 */
	private Date fechaUltimaSincronizacion;

	private List<MaestroDTO> listaMaestro;

	private List<MaestroDTO> listaTipoSituacionRevision;

	@EJB
	private MaestroServiceLocal maestroServiceLocal;

	@PostConstruct
	public void inicializar() {
		sincronizarData();
	}

	/* Cada hora de todos los dias */
	public void sincronizarData() {
		sincronizarDataMaestro();
	}

	private void sincronizarDataMaestro() {
		try {
			listaMaestro = maestroServiceLocal.listarMaestros();
			mapaMaestro = new ConcurrentHashMap<Long, MaestroDTO>();
			mapaTipoSituacionRevision = new ConcurrentHashMap<Long, MaestroDTO>();
			if (!Objects.isNull(listaMaestro) && !listaMaestro.isEmpty()) {
				for (MaestroDTO maestro : listaMaestro) {
					mapaMaestro.put(maestro.getIdMaestro(), maestro);
				}

//				listaTipoSituacionRevision = listaMaestro.stream().filter(p -> p.getCodigoAgrupacion().equals("33"))
//						.collect(Collectors.toList());
//
//				for (MaestroDTO m1 : listaTipoSituacionRevision) {
//					mapaTipoSituacionRevision.put(m1.getIdMaestro(), m1);
//				}
//				
//				for (MaestroDTO m2 : listaMaestro.stream().filter(p -> p.getCodigoAgrupacion().equals("32")).collect(Collectors.toList())) {
//					mapaTipoEstadoPrograma.put(m2.getIdMaestro(), m2);
//				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.warn(e);
		}
	}

	@Lock(LockType.READ)
	public List<MaestroDTO> getMaestroDTOxCodigoAgrupacion(String id) {
		return listaMaestro.stream().filter(p -> p.getCodigoAgrupacion().equals(id)).collect(Collectors.toList());
		 
	}
	
	@Lock(LockType.READ)
	public ConcurrentHashMap<Long, MaestroDTO> getMapaMaestro() {
		return mapaMaestro;
	}

	public void setMapaMaestro(ConcurrentHashMap<Long, MaestroDTO> mapaMaestro) {
		this.mapaMaestro = mapaMaestro;
	}

	@Lock(LockType.READ)
	public ConcurrentHashMap<Long, MaestroDTO> getMapaTipoSituacionRevision() {
		return mapaTipoSituacionRevision;
	}

	public void setMapaTipoSituacionRevision(ConcurrentHashMap<Long, MaestroDTO> mapaTipoSituacionRevision) {
		this.mapaTipoSituacionRevision = mapaTipoSituacionRevision;
	}

	public List<MaestroDTO> getListaTipoSituacionRevision() {
		return listaTipoSituacionRevision;
	}

	public void setListaTipoSituacionRevision(List<MaestroDTO> listaTipoSituacionRevision) {
		this.listaTipoSituacionRevision = listaTipoSituacionRevision;
	}

	public MaestroServiceLocal getMaestroServiceLocal() {
		return maestroServiceLocal;
	}

	public void setMaestroServiceLocal(MaestroServiceLocal maestroServiceLocal) {
		this.maestroServiceLocal = maestroServiceLocal;
	}

	public Date getFechaUltimaSincronizacion() {
		return fechaUltimaSincronizacion;
	}

	public void setFechaUltimaSincronizacion(Date fechaUltimaSincronizacion) {
		this.fechaUltimaSincronizacion = fechaUltimaSincronizacion;
	}

	@Lock(LockType.READ)
	public List<MaestroDTO> getListaMaestro() {
		return listaMaestro;
	}

	public void setListaMaestro(List<MaestroDTO> listaMaestro) {
		this.listaMaestro = listaMaestro;
	}

}
