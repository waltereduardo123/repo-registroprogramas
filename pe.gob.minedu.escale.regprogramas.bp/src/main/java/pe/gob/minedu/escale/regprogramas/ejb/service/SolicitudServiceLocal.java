package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ObservacionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.SolicitudBandejaDTO;


public interface SolicitudServiceLocal {

	List<SolicitudDTO> buscarSolicitudxId(Long idSolicitud) throws Exception;
	List<SolicitudDTO> listarSolicitud() throws Exception;
	List<SolicitudDTO> buscarSolicitud(String nombrePrograma, String codooii, String codigoModular, String tipoSolicitud) throws Exception;
	List<SolicitudDTO> buscarSolicitudCarga(String nombrePrograma, String codigoModular, String tipoSolicitud) throws Exception;//werr
	SolicitudDTO registrarSolicitudNuevo(SolicitudDTO solicitud, DocumentoDTO documento) throws Exception;
	List<SolicitudDTO> listarSolicitudByFirstMaxResult(int batchSize, int index) throws Exception;
	@SuppressWarnings("rawtypes")
	List<SolicitudDTO> listarSolicitudFirstMaxResultParametros(int batchSize, int index, Map parameters) throws Exception;
	SolicitudDTO actualizar(SolicitudDTO solicitud) throws Exception;
	SolicitudDTO obtenerSolicitudById(Long id) throws Exception;
	@SuppressWarnings("rawtypes")
//	RespuestaDTO gestionAccionSolicitud(String tipoSituacionSolicitud,String codSolicitud, String observacion,String codigoModular, SolicitudDTO solicitud) throws Exception;
	RespuestaDTO gestionAccionSolicitud(String tipoSituacionSolicitud,String codSolicitud, ObservacionSolicitudDTO observacionSolicitud,String codigoModular, SolicitudDTO solicitud, Boolean indAnularSolicitudxCierre) throws Exception;
	List<SolicitudBandejaDTO> bandejaRevision(int batchSize, int index, String jpqlRules, String jpqlOrder) throws Exception;
	SolicitudDTO registrarSolicitudModificacion(SolicitudDTO solicitud, DocumentoDTO documento) throws Exception;
	@SuppressWarnings("rawtypes")
	RespuestaDTO procesarRevision(SolicitudDTO solicitud) throws Exception;
	SolicitudDTO verificaSituacSolicitud(String codSolicitud, String tipo)throws Exception; //verificar la situacion actual de la solicitud, antes de ser enviada al servidor  WERR 05/03/2018

	SolicitudDTO verificaCierreDeshacerAprobacionSolicitud(String codModular, String tipo, String tipoSolicitud)throws Exception; //verificar que la solicitud aprobada no este en ciierre  WERR 07/05/2018
// por carga del programa
//	List<SolicitudDTO> buscarSolictudporCodModulr()throws Exception;
	
}
