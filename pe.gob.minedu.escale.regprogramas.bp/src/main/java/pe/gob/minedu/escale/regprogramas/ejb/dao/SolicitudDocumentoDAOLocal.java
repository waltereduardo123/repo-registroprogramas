package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumento;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumentoPK;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface SolicitudDocumentoDAOLocal extends StatelessCRUDServices<SolicitudDocumentoPK, SolicitudDocumento> {

	void save(SolicitudDocumento entity);
	void update(SolicitudDocumento entity);
	void delete(SolicitudDocumento entity);
	SolicitudDocumento registrarSolicitudDocumentoNuevo(SolicitudDocumento entity);
	SolicitudDocumento findById(SolicitudDocumentoPK id);
	public List<SolicitudDocumento> findByIdSolicitud(Long id);
}
