package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprograma.business.exception.MaestroNoRegistradoException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.MaestroDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Maestro;


@PermitAll
@Stateless(name = "MaestroService", mappedName = "ejb/MaestroService")
public class MaestroServiceImpl implements MaestroServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private MaestroDAOLocal maestroDAO;

	@SuppressWarnings("unchecked")
	public List<MaestroDTO> buscarMaestrosxGrupo(String codigoAgrupacion) throws Exception {
		List<Maestro> lista = maestroDAO.buscarMaestrosxGrupo(codigoAgrupacion);
		List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		return listMaestro;
	}

	@SuppressWarnings("unchecked")
	public List<MaestroDTO> buscarMaestrosxGrupoItem(String codigoAgrupacion, String item) throws Exception {
		List<Maestro> lista = maestroDAO.buscarMaestrosxGrupoItem(codigoAgrupacion, item);
		List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		return listMaestro;
	}
	
	@SuppressWarnings("unchecked")
	public List<MaestroDTO> buscarMaestrosxEstado(String estado) throws Exception {
		List<Maestro> lista = maestroDAO.buscarMaestrosxEstado(estado);
		List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		return listMaestro;
	}

	@SuppressWarnings("unchecked")
	public List<MaestroDTO> buscarMaestrosxPadre(Long id) throws Exception {
		List<Maestro> lista = maestroDAO.buscarMaestrosxPadre(id);
		List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		return listMaestro;
	}

	@SuppressWarnings("unchecked")
	public List<MaestroDTO> buscarMaestrosxNombre(String nombreMaestro) throws Exception {
		List<Maestro> lista = maestroDAO.buscarMaestrosxNombre(nombreMaestro);
		List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		return listMaestro;
	}

	@SuppressWarnings("unchecked")
	public List<MaestroDTO> listarMaestros() throws Exception {
		List<Maestro> lista = maestroDAO.findAll();
		List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		return listMaestro;
	}
	
	public List<Maestro> listarMaestrosJPA() throws Exception {
		List<Maestro> lista = maestroDAO.findAll();
		return lista;
	}

	public MaestroDTO registrarMaestroNuevo(MaestroDTO maestro) throws Exception {
		try {
			Maestro entity = ConversorHelper.convertir(Maestro.class, maestro);
			entity = maestroDAO.registrar(entity);
			maestro = ConversorHelper.convertir(MaestroDTO.class, entity);
		} catch (Exception e) {
			throw new MaestroNoRegistradoException(maestro.getCodigoAgrupacion());
		}
		return maestro;
	}

	@Override
	public MaestroDTO getMaxItemGrupoMaestro(String codigoAgrupacion) throws Exception {
		MaestroDTO nm = new MaestroDTO();
		String periodomaximo = maestroDAO.getMaxItemporGrupo(codigoAgrupacion);
		nm.setCodigoItem(periodomaximo);
		//List<Maestro> lista = maestroDAO.getMaxItemporGrupo(codigoAgrupacion);
		//List<MaestroDTO> listMaestro = ConversorHelper.convertirTodo(MaestroDTO.class, lista);
		
		return nm;
	}



}
