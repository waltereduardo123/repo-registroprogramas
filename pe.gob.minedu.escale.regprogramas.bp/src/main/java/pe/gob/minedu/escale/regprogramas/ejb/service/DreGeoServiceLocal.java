package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;

import pe.gob.minedu.escale.regprogramas.model.dto.DreGeoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.DreGeoUgelDistritoDTO;

public interface DreGeoServiceLocal {

	List<DreGeoUgelDistritoDTO> buscarDreGeoxUgel(String idDreUgel) throws Exception;

	List<DreGeoDTO> listarDreGeo() throws Exception;

	DreGeoDTO registrarDreGeoNuevo(DreGeoDTO dreGeo) throws Exception;

}
