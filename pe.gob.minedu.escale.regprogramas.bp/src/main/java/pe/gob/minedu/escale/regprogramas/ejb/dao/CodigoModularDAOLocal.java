package pe.gob.minedu.escale.regprogramas.ejb.dao;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.CodigoModular;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface CodigoModularDAOLocal extends StatelessCRUDServices<Long, CodigoModular> {

	void update(CodigoModular entity);

	CodigoModular obtenerCodigoModularDisponible();

}
