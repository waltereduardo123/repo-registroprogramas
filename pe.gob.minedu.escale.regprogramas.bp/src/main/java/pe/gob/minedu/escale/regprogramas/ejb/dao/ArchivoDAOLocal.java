package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Archivo;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface ArchivoDAOLocal extends StatelessCRUDServices<Long, Archivo> {

	void save(Archivo entity);
	void update(Archivo entity);
	void delete(Archivo entity);
	Archivo registrar(Archivo entity);
	List<Archivo> findByIdArchivo(Long idArchivo);
	List<Archivo> findAll();
	
	@SuppressWarnings("rawtypes")
	List<Archivo> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
	List<Archivo> buscarArchivo(Long idDocumento);

}
