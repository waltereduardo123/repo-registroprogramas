package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface DocumentoDAOLocal extends StatelessCRUDServices<Long, Documento> {

	void save(Documento entity);
	void update(Documento entity);
	void delete(Documento entity);
	Documento registrar(Documento entity);
	Documento registrarDocumentoConArchivo(Documento entity);
	List<Documento> findByIdDocumento(Long idDocumento);
	List<Documento> findAll();
	List<Documento> findByFirstMaxResult(int batchSize, int index);
	@SuppressWarnings("rawtypes")
	List<Documento> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
	List<Documento> buscarDocumento(String codooii,String tipoResolucion,String numDocumento,String fechaDocumento);
}
