package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

public interface ObjectServiceLocal {

	List<Object> findByFirstMaxResultJson(String jpql, String jpqlRules, String jpqlTable, String jpqlOrder, int batchSize, int index);
	List<Object[]> findByFirstMaxResultGenerico(String jpql, String jpqlRules, String jpqlTable, String jpqlOrder, int batchSize, int index);
	@SuppressWarnings("rawtypes")
	List<Object> controlDeAvance(Map parametros, String tipo);
}
