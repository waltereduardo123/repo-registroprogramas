package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;

import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoPKDTO;

public interface SolicitudDocumentoServiceLocal {

	SolicitudDocumentoDTO buscarSolicitudxId(SolicitudDocumentoPKDTO idSolicitud) throws Exception;

	SolicitudDTO registrarSolicitudNuevo(SolicitudDocumentoDTO solicitud) throws Exception;

	public List<SolicitudDocumentoDTO> findByIdSolicitud(Long id) throws Exception;
}
