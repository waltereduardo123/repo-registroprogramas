package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AccionSolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.AccionSolicitud;

@Stateless
public class AccionSolicitudDAOImpl extends AbstractJtaStatelessCRUDServices<Long, AccionSolicitud> implements AccionSolicitudDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	public AccionSolicitudDAOImpl() {
	}

	public void save(AccionSolicitud entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(AccionSolicitud entity) {
		em.merge(entity);
	}

	public void delete(AccionSolicitud entity) {
		em.remove(entity);
	}

	@SuppressWarnings("unchecked")
	public List<AccionSolicitud> findByIdAccionSolicitud(String idAccionSolicitud) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_accsoli m ");
		sbQuery.append("WHERE m.N_ID_ACCIONSOLI = ?1  ");
		Query query = em.createNativeQuery(sbQuery.toString(), AccionSolicitud.class);

		query.setParameter(1, idAccionSolicitud);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<AccionSolicitud> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM tbl_regpro_accsoli");
			query = em.createNativeQuery(sbQuery.toString(), AccionSolicitud.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}

	public AccionSolicitud registrar(AccionSolicitud entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<AccionSolicitud> findByIdSolicitud(Long idSolicitud) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("SELECT * FROM tbl_regpro_accsoli m ");
		sbQuery.append("WHERE m.N_ID_SOLICITUD = ?1  ");
		Query query = em.createNativeQuery(sbQuery.toString(), AccionSolicitud.class);

		query.setParameter(1, idSolicitud);
		
		return query.getResultList();
	}

	@SuppressWarnings("rawtypes")
	public List<AccionSolicitud> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new AccionSolicitud(s.idAccionSolicitud,"
				+ " s.observacion,s.fechaCreacion, s.usuarioCreacion, s.estado, "
				+ " s.tipoSituacionSolicitud.idMaestro, s.tipoSituacionSolicitud.nombreMaestro )"
				+ " FROM AccionSolicitud s ");
		sbQuery.append(" WHERE 1=1 ");		
		if (parameters.containsKey("idSolicitud")) {
			sbQuery.append(" AND s.solicitud.idSolicitud = :idSolicitud ");
		}
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND s.estado = :estado ");
		}
		
		

		List<AccionSolicitud> lista= synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		return lista;
	}
}
