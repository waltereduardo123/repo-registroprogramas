package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ArchivoDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Archivo;

@Stateless
public class ArchivoDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Archivo> implements ArchivoDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate

	public ArchivoDAOImpl() {
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public void save(Archivo entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(Archivo entity) {
		inciarSessionFactory();
		Archivo arch = getSess().get(Archivo.class, entity.getIdArchivo());
		try {
			ConversorHelper.fusionaPropiedades(entity, arch);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(arch);
	}

	public void delete(Archivo entity) {
		em.remove(entity);
	}

	@SuppressWarnings("unchecked")
	public List<Archivo> findByIdArchivo(Long idArchivo) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_archivo m ");
		sbQuery.append("WHERE m.N_ID_ARCHIVO = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Archivo.class);

		query.setParameter(1, idArchivo);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Archivo> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM tbl_regpro_archivo");
			query = em.createNativeQuery(sbQuery.toString(), Archivo.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}

	public Archivo registrar(Archivo entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("rawtypes")
	public List<Archivo> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new Archivo(s.idArchivo,s.nombreArchivo,s.codigoArchivo,s.fechaCreacion,"
				+ " s.usuarioCreacion,s.estado) " + " FROM Archivo s ");
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("nombreArchivo")) {
			sbQuery.append(" AND upper(s.nombreArchivo) = :nombreArchivo ");
		}
		if (parameters.containsKey("codigoArchivo")) {
			sbQuery.append(" AND upper(s.codigoArchivo) = :codigoArchivo ");
		}
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND s.estado = :estado ");
		}

		List<Archivo> listaArchivo = find(sbQuery.toString(), batchSize, index, parameters);
		return listaArchivo;
	}

	@SuppressWarnings("unchecked")
	public List<Archivo> buscarArchivo(Long idDocumento) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" Select * from tbl_regpro_archivo d ");
		sbQuery.append(" WHERE d.N_ID_DOCUMENTO=?1 ");

		Map<Integer, Long> parametros = new HashMap<Integer, Long>();

		parametros.put(1, idDocumento);

		List<Archivo> listaArchivo = nativeFind(sbQuery.toString(), parametros, Archivo.class);

		return listaArchivo;
	}

	// mejorar codigo y generalizarlo
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	// mejorar codigo y generalizarlo
}
