package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.AccionSolicitud;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface AccionSolicitudDAOLocal extends StatelessCRUDServices<Long, AccionSolicitud> {

	void save(AccionSolicitud entity);
	void update(AccionSolicitud entity);
	void delete(AccionSolicitud entity);
	AccionSolicitud registrar(AccionSolicitud entity);
	List<AccionSolicitud> findByIdAccionSolicitud(String idAccionSolicitud);
	List<AccionSolicitud> findByIdSolicitud(Long idSolicitud);
	List<AccionSolicitud> findAll();
	@SuppressWarnings("rawtypes")
	List<AccionSolicitud> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
}
