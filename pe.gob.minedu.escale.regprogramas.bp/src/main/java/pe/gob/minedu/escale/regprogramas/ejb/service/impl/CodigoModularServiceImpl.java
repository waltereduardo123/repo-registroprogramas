package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.regprograma.business.exception.CodigoModularNoRegistradoException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.CodigoModularDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.CodigoModularServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.CodigoModularDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.CodigoModular;

@PermitAll
@Stateless(name = "CodigoModularService", mappedName = "ejb/CodigoModularService")
public class CodigoModularServiceImpl implements CodigoModularServiceLocal {

	@EJB
	private CodigoModularDAOLocal codigoModularDAO;

	@Override
	public CodigoModularDTO obtenerCodigoModular() throws Exception {
		CodigoModularDTO codigoModular = new CodigoModularDTO();
		CodigoModular codigo = new CodigoModular();
		try {
			codigo = codigoModularDAO.obtenerCodigoModularDisponible();
			codigoModular = ConversorHelper.convertir(CodigoModularDTO.class, codigo);
		} catch (Exception e) {
			throw new ConversorException();
		}			
		return codigoModular;
	}

	@Override
	public CodigoModularDTO actualizar(CodigoModularDTO codigoModular) throws Exception {
		CodigoModular entity = ConversorHelper.convertir(CodigoModular.class, codigoModular);
		try {
			codigoModularDAO.update(entity);
		} catch (Exception e) {
			throw new CodigoModularNoRegistradoException(codigoModular.getCodigoModular());
		}		
		
		return codigoModular;
	}

}
