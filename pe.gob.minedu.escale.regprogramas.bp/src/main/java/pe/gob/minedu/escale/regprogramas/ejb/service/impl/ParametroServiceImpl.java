package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ParametroDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ParametroServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.ParametroDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Parametro;

@PermitAll
@Stateless(name = "ParametroService", mappedName = "ejb/ParametroService")
public class ParametroServiceImpl implements ParametroServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private ParametroDAOLocal parametroDAO;

	@SuppressWarnings("unchecked")
	public List<ParametroDTO> listarParametro() throws Exception {
		List<Parametro> lista = parametroDAO.findAll();
		List<ParametroDTO> listParametro = ConversorHelper.convertirTodo(ParametroDTO.class, lista);
		return listParametro;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ParametroDTO> buscarParametroxId(Long idParametro) throws Exception {
		List<Parametro> lista = parametroDAO.findByIdParametro(idParametro);
		List<ParametroDTO> listParametro = ConversorHelper.convertirTodo(ParametroDTO.class, lista);
		return listParametro;
	}

	@Override
	public ParametroDTO registrarParametroNuevo(ParametroDTO Parametro) throws Exception {
		Parametro parametroNuevo = ConversorHelper.convertir(Parametro.class, Parametro);
		try {

			parametroNuevo = parametroDAO.registrar(parametroNuevo);
			// registro accionsolciitud

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<ParametroDTO> listarParametroByFirstMaxResult(int batchSize, int index) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<ParametroDTO> listarParametroFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<Parametro> listaParametro = parametroDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<ParametroDTO> listaParametroDTO = ConversorHelper.convertirTodo(ParametroDTO.class, listaParametro);
		return listaParametroDTO;
	}

}
