package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;

public interface AccionSolicitudServiceLocal {

	List<AccionSolicitudDTO> buscarAccionSolicitudxIdSolicitud(Long idSolicitud) throws Exception;
	List<AccionSolicitudDTO> listarAccionSolicitud() throws Exception;	
	AccionSolicitudDTO registrar(AccionSolicitudDTO solicitud) throws Exception;
	List<AccionSolicitudDTO> listarByFirstMaxResult(int batchSize, int index) throws Exception;
	@SuppressWarnings("rawtypes")
	List<AccionSolicitudDTO> listarFirstMaxResultParametros(int batchSize, int index, Map parameters) throws Exception;
	
}
