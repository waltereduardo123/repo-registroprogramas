package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprogramas.ejb.dao.DreGeoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.DreGeoServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.DreGeoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.DreGeoUgelDistritoDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.DreGeo;

@PermitAll
@Stateless(name = "DreGeoService", mappedName = "ejb/DreGeoService")
public class DreGeoServiceImpl implements DreGeoServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private DreGeoDAOLocal DreGeoDAO;

	@SuppressWarnings("unchecked")
	public List<DreGeoDTO> listarDreGeo() throws Exception {
		List<DreGeo> lista = DreGeoDAO.findAll();
		List<DreGeoDTO> listDreGeo = ConversorHelper.convertirTodo(DreGeoDTO.class, lista);
		return listDreGeo;
	}

	@SuppressWarnings("unchecked")
	public List<DreGeoUgelDistritoDTO> buscarDreGeoxUgel(String idDreUgel) throws Exception {
		List<DreGeo> lista = DreGeoDAO.findByIdDreUgel(idDreUgel);
		List<DreGeoDTO> listDreGeo = ConversorHelper.convertirTodo(DreGeoDTO.class, lista);
		List<DreGeoUgelDistritoDTO> listaDreGeoUgelDistritoDTO = new  ArrayList<DreGeoUgelDistritoDTO>();
		for (DreGeoDTO dreGeoDTO : listDreGeo) {
			DreGeoUgelDistritoDTO dreGeo = new DreGeoUgelDistritoDTO(dreGeoDTO.getDreUgel().getCodigo(), 
					dreGeoDTO.getDreUgel().getNombreoi(),
					dreGeoDTO.getDistritos().getIdDistrito(),
					dreGeoDTO.getDistritos().getDistrito(),
					dreGeoDTO.getDistritos().getPointX(),
					dreGeoDTO.getDistritos().getPointY(),
					dreGeoDTO.getDistritos().getZoom(),
					dreGeoDTO.getDistritos().getProvincias().getRegiones().getIdRegion(),
					dreGeoDTO.getDistritos().getProvincias().getRegiones().getRegion(),
					dreGeoDTO.getDistritos().getProvincias().getRegiones().getPointX(),
					dreGeoDTO.getDistritos().getProvincias().getRegiones().getPointY(),
					dreGeoDTO.getDistritos().getProvincias().getRegiones().getZoom(),
					dreGeoDTO.getDistritos().getProvincias().getIdProvincia(),
					dreGeoDTO.getDistritos().getProvincias().getProvincia(),
					dreGeoDTO.getDistritos().getProvincias().getPointX(),
					dreGeoDTO.getDistritos().getProvincias().getPointY(),
					dreGeoDTO.getDistritos().getProvincias().getZoom());
			listaDreGeoUgelDistritoDTO.add(dreGeo);
		}
		return listaDreGeoUgelDistritoDTO;
	}

	public DreGeoDTO registrarDreGeoNuevo(DreGeoDTO DreGeo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
