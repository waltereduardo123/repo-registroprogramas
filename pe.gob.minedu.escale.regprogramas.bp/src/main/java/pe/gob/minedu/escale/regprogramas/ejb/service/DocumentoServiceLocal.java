package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;

public interface DocumentoServiceLocal {

	List<DocumentoDTO> buscarDocumentoxId(Long idDocumento) throws Exception;

	List<DocumentoDTO> listarDocumento() throws Exception;

	DocumentoDTO registrarDocumentoNuevo(DocumentoDTO Documento) throws Exception;

	List<DocumentoDTO> buscarDocumento(String codooii, String tipoResolucion, String numDocumento,
			String fechaDocumento) throws Exception;

	@SuppressWarnings("rawtypes")
	List<DocumentoDTO> listarDocumentoFirstMaxResultParametros(int batchSize, int index, Map parameters) throws Exception;
	
	DocumentoDTO actualizar(DocumentoDTO documento) throws Exception;
}
