package pe.gob.minedu.escale.regprogramas.ejb.service;


import java.util.List;
import java.util.Map;



import pe.gob.minedu.escale.regprogramas.model.dto.CorteDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Programa;

public interface ProgramaServiceLocal {

	ProgramaDTO registrarProgramaNuevo(ProgramaDTO Programa) throws Exception;
	ProgramaDTO findById(Long id) throws Exception;
	List<ProgramaDTO> buscarProgramasxCodooii(char mander);
	List<ProgramaDTO> buscarProgramasxNombre(String nombrePrograma) throws Exception;
	List<ProgramaDTO> buscarProgramasxEstado(String estado) throws Exception;
	List<ProgramaDTO> listarProgramas() throws Exception;
	List<ProgramaDTO> listarProgramasByFirstMaxResult(int batchSize, int index) throws Exception;
	@SuppressWarnings("rawtypes")
	List<ProgramaDTO> listarProgramasFirstMaxResultParametros(int batchSize, int index, Map parameters) throws Exception;
	List<Programa> listarProgramasFirstMaxResultParametrosResetPrograma(int batchSize, int index, Map parameters) throws Exception;//por reinicio de programas werr 15112017
	List<String> listCentroPobladoActivo(Map parameters) throws Exception;//por sincronizacion de programas werr 04122017
//	ProgramaDTO listCentroPobladoActivoUpdate(Map parameters) throws Exception;//por sincronizacion de programas werr 04122017
	List<ProgramaDTO> listCentroPobladoActivoUpdate(Map parameters) throws Exception;//por sincronizacion de programas werr 04122017
	List<Object> listCodModularProgramaActivo(Map parameters) throws Exception;//por sincronizacion de programas werr 04122017
	List<Programa> listarProgramasActivos(Map parameters) throws Exception;//por sincronizacion de programas werr 04122017  
	Integer actualizarSituacPrograma(Map parameters, MaestroDTO maestrost,MaestroDTO maestroestado,MaestroDTO maestroperiodo,List<Long> listasituaciones,Long idSituaSol,CorteDTO corte) throws Exception; //por reinicio de programas werr 15112017
	Integer actualizarSituacPrograma(Map parameters, MaestroDTO maestrost,MaestroDTO maestroestado,MaestroDTO maestroperiodo,List<Long> listasituaciones,Long idSituaSol) throws Exception; //por reinicio con gestion de periodos werr 18102018
	ProgramaDTO actualizar(ProgramaDTO programa) throws Exception;
	ProgramaDTO buscarProgramaxCodigoModular(String codigoModular) throws Exception;
	ProgramaDTO buscarProgramaCodigoModular(String codigoModular) throws Exception;//werr
	ProgramaDTO buscarProgramasSituacionxCodigoModular(String codigoModular) throws Exception;
	
}
