package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.DreGeoDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.DreGeo;

@Stateless
public class DreGeoDAOImpl extends AbstractJtaStatelessCRUDServices<String, DreGeo> implements DreGeoDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	public DreGeoDAOImpl() {
	}

	public void save(DreGeo entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(DreGeo entity) {
		em.merge(entity);
	}

	public void delete(DreGeo entity) {
		em.remove(entity);
	}

	// public List<DreGeo> findById(String idDreUgel, String idDistrito) {
	// DreGeoPK idDreGeo = new DreGeoPK(idDreUgel, idDistrito);
	// return em.find(DreGeo.class, idDreGeo);
	// }
//---Se comenta por caso de dregeo con cuatro caracteres iniciados en 0 werr
//	@SuppressWarnings("unchecked")
//	public List<DreGeo> findByIdDreUgel(String idDreUgel) {
//		StringBuilder sbQuery = new StringBuilder();
//
//		sbQuery.append("SELECT * FROM dre_geo m ");
//		sbQuery.append("WHERE m.CODIGO = ?1  ");
//		sbQuery.append("AND m.C_ESTADO = ?2 ");
//		Query query = em.createNativeQuery(sbQuery.toString(), DreGeo.class);
//
//		query.setParameter(1, idDreUgel);
//		query.setParameter(2, EstadoState.ACTIVO.getValue());
//
//		return query.getResultList();
//	}

	@SuppressWarnings("unchecked")
	public List<DreGeo> findByIdDreUgel(String idDreUgel) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM dre_geo m ");
		
		if(idDreUgel.length() == 6){
			sbQuery.append("WHERE m.CODIGO = ?1  ");
		}else{
			sbQuery.append("WHERE left(m.CODIGO,4) = ?1  ");
		}
		
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), DreGeo.class);

		query.setParameter(1, idDreUgel);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	
	
	// @SuppressWarnings("unchecked")
	// public List<DreGeo> buscarDreGeosxPadre(Long id) {
	// StringBuilder sbQuery = new StringBuilder();
	//
	// sbQuery.append("SELECT * FROM tbl_regpro_maestro m ");
	// sbQuery.append("WHERE m.N_ID_MAESTROPADRE = ?1 ");
	// sbQuery.append("AND m.C_ESTADO = ?2 ");
	// Query query = em.createNativeQuery(sbQuery.toString(),DreGeo.class);
	//
	// query.setParameter(1, id);
	// query.setParameter(2, EstadoState.ACTIVO.getValue());
	//
	//
	// return query.getResultList();
	// }

	// @SuppressWarnings("unchecked")
	// public List<DreGeo> buscarDreGeosxGrupo(String codigoAgrupacion) {
	// StringBuilder sbQuery = new StringBuilder();
	//
	// sbQuery.append("select new DreGeo(m.idDreGeo,m.codigoAgrupacion,
	// m.nombreDreGeo, m.descripcionDreGeo, m.codigoItem) ");
	// sbQuery.append("FROM DreGeo m ");
	// sbQuery.append("WHERE m.estado = :estado ");
	// sbQuery.append("AND m.codigoAgrupacion = :codigoAgrupacion ");
	//
	// Query query = em.createQuery(sbQuery.toString(),DreGeo.class);
	//
	// query.setParameter("estado", EstadoState.ACTIVO.getValue());
	// query.setParameter("codigoAgrupacion", codigoAgrupacion);
	//
	// return query.getResultList();
	// }
	//
	// @SuppressWarnings("unchecked")
	// public List<DreGeo> buscarDreGeosxNombre(String nombreDreGeo) {
	// StringBuilder sbQuery = new StringBuilder();
	//
	// sbQuery.append("select new DreGeo(m.idDreGeo,m.codigoAgrupacion,
	// m.nombreDreGeo, m.descripcionDreGeo, m.codigoItem) ");
	// sbQuery.append("FROM DreGeo m ");
	// sbQuery.append(" where upper(m.nombreDreGeo) like :nombreDreGeo ");
	//
	// Query query = em.createQuery(sbQuery.toString(),DreGeo.class);
	//
	// query.setParameter("nombreDreGeo",
	// "%".concat(nombreDreGeo.toUpperCase()).concat("%"));
	//
	// return query.getResultList();
	// }

	@SuppressWarnings("unchecked")
	public List<DreGeo> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM dre_geo");
			query = em.createNativeQuery(sbQuery.toString(), DreGeo.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}

	public DreGeo registrar(DreGeo entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	public EntityManager getEntityManager() {
		return em;
	}

}
