package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;

import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;


public interface SolicitudCargaServiceLocal {

	//Se agrega para el registro de la solicitud de carga. werr
	SolicitudDTO registrarSolicitudCargaExc(SolicitudDTO solicitud,List<Documento> documentos) throws Exception; //carga excel
	SolicitudDTO registrarSolicitudCarga(SolicitudDTO solicitud,List<Documento> documentos) throws Exception;
	List<Documento> registrarDocumentoArchivo(DocumentoDTO documento)throws Exception; //carga excel
	List<Documento> registrarDocumentoArchivoDTO(DocumentoDTO documento)throws Exception;
	//SolicitudDocumento registrarSolicitudDocumento()throws Exception;
}
