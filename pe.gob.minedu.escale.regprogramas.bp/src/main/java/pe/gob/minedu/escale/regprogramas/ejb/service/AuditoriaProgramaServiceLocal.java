package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO;

public interface AuditoriaProgramaServiceLocal {

	AuditoriaProgramaDTO registrarProgramaNuevo(AuditoriaProgramaDTO Programa) throws Exception;
	AuditoriaProgramaDTO findById(Long id) throws Exception;
	List<AuditoriaProgramaDTO> buscarProgramasxCodooii(char mander);
	List<AuditoriaProgramaDTO> buscarProgramasxNombre(String nombrePrograma) throws Exception;
	List<AuditoriaProgramaDTO> buscarProgramasxEstado(String estado) throws Exception;
	List<AuditoriaProgramaDTO> listarProgramas() throws Exception;
	List<AuditoriaProgramaDTO> listarProgramasByFirstMaxResult(int batchSize, int index) throws Exception;
	@SuppressWarnings("rawtypes")
	List<AuditoriaProgramaDTO> listarProgramasFirstMaxResultParametros(int batchSize, int index, Map parameters) throws Exception;
	AuditoriaProgramaDTO actualizar(AuditoriaProgramaDTO programa) throws Exception;
	AuditoriaProgramaDTO buscarProgramaxCodigoModular(String codigoModular) throws Exception;
}
