package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.regprogramas.ejb.dao.ObjectDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ObjectServiceLocal;

@PermitAll
@Stateless(name = "ObjectService", mappedName = "ejb/ObjectService")
public class ObjectServiceImpl implements ObjectServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private ObjectDAOLocal objectDAO;

	@Override
	public List<Object> findByFirstMaxResultJson(String jpql, String jpqlRules, String jpqlTable, String jpqlOrder, int batchSize,
			int index) {
		List<Object> lista = new ArrayList<Object>();
		lista = objectDAO.findByFirstMaxResultJson(jpql, jpqlRules, jpqlTable, jpqlOrder, batchSize, index);
		return lista;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public List<Object> controlDeAvance(Map parametros, String tipo) {
		List<Object> lista = new ArrayList<Object>();
		lista = objectDAO.controlDeAvance(parametros, tipo);	
		return lista;
	}

	@Override
	public List<Object[]> findByFirstMaxResultGenerico(String jpql, String jpqlRules, String jpqlTable, String jpqlOrder,
			int batchSize, int index) {
		List<Object[]> lista = new ArrayList<Object[]>();
		lista = objectDAO.findByFirstMaxResultGenerico(jpql, jpqlRules, jpqlTable, jpqlOrder, batchSize, index);
		return lista;
	}
}
