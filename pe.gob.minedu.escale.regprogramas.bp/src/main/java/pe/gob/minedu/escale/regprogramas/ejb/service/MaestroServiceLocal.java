package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;

import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Maestro;

public interface MaestroServiceLocal {

	
	List<MaestroDTO> buscarMaestrosxGrupo(String codigoAgrupacion) throws Exception;
	List<MaestroDTO> buscarMaestrosxGrupoItem(String codigoAgrupacion, String item ) throws Exception;
	List<MaestroDTO> buscarMaestrosxEstado(String estado) throws Exception;	
	List<MaestroDTO> buscarMaestrosxPadre(Long id) throws Exception;
	List<MaestroDTO> buscarMaestrosxNombre(String nombreMaestro) throws Exception;	
	List<MaestroDTO> listarMaestros() throws Exception;
	MaestroDTO getMaxItemGrupoMaestro(String codigoAgrupacion) throws Exception;
	List<Maestro> listarMaestrosJPA() throws Exception;
	MaestroDTO registrarMaestroNuevo(MaestroDTO maestro) throws Exception;
	
}
