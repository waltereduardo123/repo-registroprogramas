package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;

import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;

public interface ArchivoServiceLocal {

	List<ArchivoDTO> buscarArchivoxId(Long idArchivo) throws Exception;

	List<ArchivoDTO> listarArchivo() throws Exception;

	ArchivoDTO registrarArchivoNuevo(ArchivoDTO archivo) throws Exception;

	List<ArchivoDTO> buscarArchivoxIdDocumento(Long idDocumento) throws Exception;
}
