package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.regprogramas.model.dto.ParametroDTO;

public interface ParametroServiceLocal {

	List<ParametroDTO> buscarParametroxId(Long idParametro) throws Exception;
	List<ParametroDTO> listarParametro() throws Exception;	
	ParametroDTO registrarParametroNuevo(ParametroDTO Parametro) throws Exception;
	List<ParametroDTO> listarParametroByFirstMaxResult(int batchSize, int index) throws Exception;
	@SuppressWarnings("rawtypes")
	List<ParametroDTO> listarParametroFirstMaxResultParametros(int batchSize, int index, Map parameters) throws Exception;
}
