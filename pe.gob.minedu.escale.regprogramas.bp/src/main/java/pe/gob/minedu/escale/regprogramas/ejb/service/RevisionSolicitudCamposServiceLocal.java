package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;
import java.util.Map;

import pe.gob.minedu.escale.regprogramas.model.dto.RevisionSolicitudCamposDTO;

public interface RevisionSolicitudCamposServiceLocal {

	List<RevisionSolicitudCamposDTO> buscarSolicitudCamposxIdSolicitud(Long idSolicitud) throws Exception;

	List<RevisionSolicitudCamposDTO> listarRevisionSolicitudCampos() throws Exception;

	RevisionSolicitudCamposDTO registrar(RevisionSolicitudCamposDTO solicitudCampos) throws Exception;

	RevisionSolicitudCamposDTO actualizar(RevisionSolicitudCamposDTO revision) throws Exception;

	List<RevisionSolicitudCamposDTO> listarByFirstMaxResult(int batchSize, int index) throws Exception;

	@SuppressWarnings("rawtypes")
	List<RevisionSolicitudCamposDTO> listarFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception;

}
