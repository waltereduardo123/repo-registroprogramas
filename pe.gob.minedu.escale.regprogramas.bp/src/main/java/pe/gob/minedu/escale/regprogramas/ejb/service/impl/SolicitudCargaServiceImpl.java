package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.SerializationUtils;

import pe.gob.minedu.escale.adm.business.type.TipoArchivoType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.business.type.TipoPeriodoType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AccionSolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ArchivoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.DocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.RevisionSolicitudCamposDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDocumentoDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudCargaServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.AccionSolicitud;
import pe.gob.minedu.escale.regprogramas.model.jpa.Archivo;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;
import pe.gob.minedu.escale.regprogramas.model.jpa.Solicitud;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumento;
import pe.gob.minedu.escale.regprogramas.model.jpa.SolicitudDocumentoPK;


@PermitAll
@Stateless(name = "SolicitudCargaService", mappedName = "ejb/SolicitudCargaService")
public class SolicitudCargaServiceImpl implements SolicitudCargaServiceLocal {

	private static final String AGRUP_MAESTRO_PERIODO = "34";
	

	/** El servicio usuario dao. */
	@EJB
	private ArchivoDAOLocal archivoDAO;

	@EJB
	private DocumentoDAOLocal documentoDAO;

	@EJB
	private SolicitudDAOLocal solicitudDAO;

	@EJB
	private AccionSolicitudDAOLocal accionSolicitudDAO;

	@EJB
	private SolicitudDocumentoDAOLocal solicitudDocumentoDAO;
	
	@EJB
	private ProgramaDAOLocal programaDAO;	
	
	@EJB
	private RevisionSolicitudCamposDAOLocal  revisionSolicitudCamposDAO;
	
	@EJB
	private DataGeneralCache dataGeneralCache;
	
	@EJB(beanName="MaestroService")
	private transient MaestroServiceLocal maestroService;
	
	private List<MaestroDTO> listaTipoSolicitud = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionPrograma = new ArrayList<MaestroDTO>();
	
	private List<MaestroDTO> listaTipoDocumento;
	
	private List<MaestroDTO> listaTipoSituacionSolicitud;
	
	private List<MaestroDTO> listaTipoPrograma;
	
	private List<MaestroDTO> listaTipoPeriodo = new ArrayList<MaestroDTO>();
	
	//private List<MaestroDTO> listaTipoSituacionRevision;
	
	private List<MaestroDTO> listaTipoArchivo = new ArrayList<MaestroDTO>();
	
	
	
	
	@PostConstruct
	public void iniciar() {
		listaTipoSolicitud = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());//CARGA
		listaTipoSituacionSolicitud = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey()); //APROBADO
		listaTipoDocumento = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoDocumentoType.CODIGO_AGRUPACION.getKey()); //DOCUMENTO UE
		listaTipoArchivo = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoArchivoType.CODIGO_AGRUPACION.getKey()); //pdf
//		listaTipoPrograma=dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoProgramaType.OTRO.getKey());
		listaTipoSituacionPrograma = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoSituacionProgramaType.CODIGO_AGRUPACION.getKey());//creado
		listaTipoPeriodo=dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoPeriodoType.CODIGO_AGRUPACION.getKey());
		
	}
	
	
	@Override
	public List<Documento> registrarDocumentoArchivoDTO(DocumentoDTO documento)throws Exception{
//	public List<Documento> registrarDocumentoArchivo(DocumentoDTO documento)throws Exception{
//		  //1: registro del documento
//					Documento documentoNuevo = ConversorHelper.convertir(Documento.class, documento);
//					Documento documentoViejo = null;		
//					List<Documento> listaDocumento = new ArrayList<Documento>();
//							if(documento.getIndCambioArchivos().equals("2")) { //Nuevo registro
//								documentoNuevo = documentoDAO.registrar(documentoNuevo);					
//							}
//											
//							
//						listaDocumento.add(documentoNuevo);
//			//2: registro de los archivos
//					try {
//						for (ArchivoDTO archivoDTO : documento.getListaArchivo()) {
//								Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
//								if (StringUtil.isNotNullOrBlank(documento.getIndCambioArchivos())){
////									if (documento.getIndCambioArchivos().equals(EstadoState.ACTIVO.getValue())) {
////										for(Archivo ar : documentoViejo.getListaArchivo()){
////											ar.setEstado(EstadoState.INACTIVO.getValue());
////											archivoDAO.update(ar);
////										}
////									}
//									if(!documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())){
//										archivo.setDocumento(documentoNuevo);
//										archivoDAO.registrar(archivo);
//									}
//								}												
////							}
//						}
//					} catch (Exception e) {
//						System.out.println(e.getMessage());
//						e.printStackTrace();
//					}
		
		// registro Documento
		Documento documentoNuevo = ConversorHelper.convertir(Documento.class, documento);
		Documento documentoViejo = null;		
		List<Documento> listaDocumento = new ArrayList<Documento>();
		String numeroDocumento = documentoNuevo.getNroDocumento().replaceFirst ("^0*", "");
		if(documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())) {		
			listaDocumento  = documentoDAO.buscarDocumento(documentoNuevo.getCodooii(), 
					documentoNuevo.getTipoDocumentoResolucion().getIdMaestro().toString(), 
					numeroDocumento, 
					FechaUtil.obtenerFechaFormatoPersonalizado(documentoNuevo.getFechaDocumento(), "yyyy-MM-dd"));
			documentoNuevo = listaDocumento.get(0);
			listaDocumento.clear();
		}else		
			if(documento.getIndCambioArchivos().equals(EstadoState.ACTIVO.getValue())) {
				listaDocumento = documentoDAO.buscarDocumento(documentoNuevo.getCodooii(), 
						documentoNuevo.getTipoDocumentoResolucion().getIdMaestro().toString(), 
						numeroDocumento, 
						FechaUtil.obtenerFechaFormatoPersonalizado(documentoNuevo.getFechaDocumento(), "yyyy-MM-dd"));
				documentoViejo = listaDocumento.get(0);				
				documentoNuevo = listaDocumento.get(0);
				listaDocumento.clear();
			}else
				if(documento.getIndCambioArchivos().equals("2")) { //Nuevo registro
					documentoNuevo = documentoDAO.registrar(documentoNuevo);					
				}
			listaDocumento.add(documentoNuevo);
		
		
		
		// registro archivo
		try {
			for (ArchivoDTO archivoDTO : documento.getListaArchivo()) {
//				if (archivoDTO.getTipoArchivo().getIdMaestro().longValue() == Long
//						.valueOf(listaTipoDocumento.stream()
//								  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
//								  .findFirst()
//								  .get()
//								  .getIdMaestro()).longValue()) {
				if (archivoDTO.getTipoArchivo().getIdMaestro().equals(listaTipoDocumento.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro())) {			
					DocumentoDTO documentoCroquis = new DocumentoDTO();
					documentoCroquis = (DocumentoDTO) SerializationUtils.clone(documento);
					documentoCroquis.getListaArchivo().clear();
					documentoCroquis.getListaArchivo().add(archivoDTO);
					documentoCroquis.setCodigoDocumento(
							TipoDocumentoType.CROQUIS.getDescription(ResourceUtil.obtenerLocaleSession()));
					documentoCroquis.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
																						  .filter(p-> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
																						  .findFirst()
																						  .get()
																						  .getIdMaestro()));
					documentoCroquis.setTipoDocumentoResolucion(new MaestroDTO(0L));
					Documento documentoNuevoCroquis = ConversorHelper.convertir(Documento.class, documentoCroquis);
					documentoNuevoCroquis = documentoDAO.registrar(documentoNuevoCroquis);
					listaDocumento.add(documentoNuevoCroquis);
					archivoDTO.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
																			  .filter(p-> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
					Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
					archivo.setDocumento(documentoNuevoCroquis);
					archivoDAO.registrar(archivo);

				} else {
					Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
					if (StringUtil.isNotNullOrBlank(documento.getIndCambioArchivos())){
						if (documento.getIndCambioArchivos().equals(EstadoState.ACTIVO.getValue())) {
							for(Archivo ar : documentoViejo.getListaArchivo()){
								ar.setEstado(EstadoState.INACTIVO.getValue());
								archivoDAO.update(ar);
							}
						}
						if(!documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())){
							archivo.setDocumento(documentoNuevo);
							archivoDAO.registrar(archivo);
						}
					}												
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
				
		
					DocumentoDTO documentoDTO = ConversorHelper.convertir(DocumentoDTO.class, documentoNuevo);
					
//					return documentoDTO;
					return listaDocumento;
	}
	
	@Override
	public SolicitudDTO registrarSolicitudCarga(SolicitudDTO solicitud,List<Documento> documentos) throws Exception {
		Solicitud solicitudNuevaCarga=null;		
		//1: Es una solicitud de Carga
					 if (solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
							  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CARGA.getKey().toString()))
							  .findFirst()
							  .get()
							  .getIdMaestro())) {
						 	
							//Se agrega el periodo de la solicitud werr 20112017
//							solicitud.setPeriodoRegistrado(new MaestroDTO(listaTipoPeriodo.stream()
//									  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2018.getKey().toString()))
//									  .findFirst()
//									  .get().getIdMaestro()));
							//obtener le item de la agrupacion
							String item=maestroService.getMaxItemGrupoMaestro(AGRUP_MAESTRO_PERIODO).getCodigoItem();
							//buscar por codigo de grupo y codigo de item
							MaestroDTO mdto=new MaestroDTO();
							mdto.setIdMaestro(maestroService.buscarMaestrosxGrupoItem(AGRUP_MAESTRO_PERIODO, item).get(0).getIdMaestro());
							solicitud.setPeriodoRegistrado(mdto);
						 
							// registro Solicitud		
						  solicitudNuevaCarga = ConversorHelper.convertir(Solicitud.class, solicitud);
						  solicitudNuevaCarga = solicitudDAO.registrar(solicitudNuevaCarga);
						}
					 
		//2: registro accionsolciitud
						try {
							for (AccionSolicitudDTO accionSolicitudDTO : solicitud.getListaAccionSolicitud()) {
								AccionSolicitud accionSolicitud = ConversorHelper.convertir(AccionSolicitud.class, accionSolicitudDTO);
								accionSolicitud.setSolicitud(solicitudNuevaCarga);
								accionSolicitudDAO.registrar(accionSolicitud);
							}
						} catch (Exception e) {
							System.out.println(e.getMessage());
							e.printStackTrace();
						}
			 
	   //3: registro SolicitudDocumento
							for (Documento doc : documentos) {
								SolicitudDocumentoPK pk = new SolicitudDocumentoPK(doc.getIdDocumento(), solicitudNuevaCarga.getIdSolicitud());
								SolicitudDocumento solicitudDocumento = new SolicitudDocumento();
								solicitudDocumento.setSolicitudDocumentoPK(pk);
								solicitudDocumento.setFechaCreacion(FechaUtil.obtenerFechaActual());
								solicitudDocumento.setUsuarioCreacion(solicitud.getUsuarioCreacion());
								solicitudDocumento.setEstado(EstadoState.ACTIVO.getValue());
								solicitudDocumento = solicitudDocumentoDAO.registrarSolicitudDocumentoNuevo(solicitudDocumento);
							}
			 
				   	
							
							
					 	SolicitudDTO solicitudNuevoDTO = ConversorHelper.convertir(SolicitudDTO.class, solicitudNuevaCarga);
						return solicitudNuevoDTO;
	}
	
	private Documento buscarResolucion(Documento doc){
		List<Documento> listDoc;
		Documento docum = null;
		//Buscamos si la resolucion ya existe
		String numeroDocumento = doc.getNroDocumento().replaceFirst ("^0*", "");
		listDoc  = documentoDAO.buscarDocumento(doc.getCodooii(), 
														doc.getTipoDocumentoResolucion().getIdMaestro().toString(), 
														numeroDocumento, 
														FechaUtil.obtenerFechaFormatoPersonalizado(doc.getFechaDocumento(), "yyyy-MM-dd"));
		//Verificamos si hubo resultado, resolucion existente
		if(Objects.nonNull(listDoc) && !listDoc.isEmpty()){
			listDoc = listDoc.stream()
							 .filter(p-> p.getEstado().equals(EstadoState.ACTIVO.getValue()))
							 .collect(Collectors.toList());
			docum = listDoc.get(0);//Si existe la resolucion
		}	
		return docum;
	}
	
	private SolicitudDocumento buscarSolicitudDocumentoByTipo(Long idSolicitud, String tipoDocumentoType){
		SolicitudDocumento solDoc = null;
		List<SolicitudDocumento> listSolDoc = null;
		listSolDoc = solicitudDocumentoDAO.findByIdSolicitud(idSolicitud);
		Long idMaestro = listaTipoDocumento.stream()
											.filter(d-> d.getCodigoItem()
													.equals(tipoDocumentoType))
													.findFirst()
													.get()
													.getIdMaestro();		
		try {
			solDoc = listSolDoc.stream()
								.filter(p-> (p.getDocumento().getTipoDocumento().getIdMaestro().equals(idMaestro)) &&  
											(p.getEstado().equals(EstadoState.ACTIVO.getValue())))
								.findFirst()
								.get();
		} catch (NoSuchElementException e) {
		}		
		return solDoc;
	}


	@Override
	public List<Documento> registrarDocumentoArchivo(DocumentoDTO documento)throws Exception{
	  //1: registro del documento
				Documento documentoNuevo = ConversorHelper.convertir(Documento.class, documento);
				Documento documentoViejo = null;		
				List<Documento> listaDocumento = new ArrayList<Documento>();
						if(documento.getIndCambioArchivos().equals("2")) { //Nuevo registro
							documentoNuevo = documentoDAO.registrar(documentoNuevo);					
						}
										
						
					listaDocumento.add(documentoNuevo);
		//2: registro de los archivos
				try {
					for (ArchivoDTO archivoDTO : documento.getListaArchivo()) {
							Archivo archivo = ConversorHelper.convertir(Archivo.class, archivoDTO);
							if (StringUtil.isNotNullOrBlank(documento.getIndCambioArchivos())){
//								if (documento.getIndCambioArchivos().equals(EstadoState.ACTIVO.getValue())) {
//									for(Archivo ar : documentoViejo.getListaArchivo()){
//										ar.setEstado(EstadoState.INACTIVO.getValue());
//										archivoDAO.update(ar);
//									}
//								}
								if(!documento.getIndCambioArchivos().equals(EstadoState.INACTIVO.getValue())){
									archivo.setDocumento(documentoNuevo);
									archivoDAO.registrar(archivo);
								}
							}												
//						}
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				return listaDocumento;
	}

	@Override
	public SolicitudDTO registrarSolicitudCargaExc(SolicitudDTO solicitud, List<Documento> documentos) throws Exception {
		Solicitud solicitudNuevaCarga=null;		
		//1: Es una solicitud de Carga
					 if (solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
							  .filter(p-> p.getCodigoItem().equals(TipoSolicitudType.CARGA.getKey().toString()))
							  .findFirst()
							  .get()
							  .getIdMaestro())) {
						 	
						//Se agrega el periodo de la solicitud werr 20112017
//							solicitud.setPeriodoRegistrado(new MaestroDTO(listaTipoPeriodo.stream()
//									  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2018.getKey().toString()))
//									  .findFirst()
//									  .get().getIdMaestro()));
						 
							
							
							//obtener le item de la agrupacion
								String item=maestroService.getMaxItemGrupoMaestro(AGRUP_MAESTRO_PERIODO).getCodigoItem();
								//buscar por codigo de grupo y codigo de item
								MaestroDTO mdto=new MaestroDTO();
								mdto.setIdMaestro(maestroService.buscarMaestrosxGrupoItem(AGRUP_MAESTRO_PERIODO, item).get(0).getIdMaestro());
								solicitud.setPeriodoRegistrado(mdto);
							
							// registro Solicitud		
						  solicitudNuevaCarga = ConversorHelper.convertir(Solicitud.class, solicitud);
						  solicitudNuevaCarga = solicitudDAO.registrar(solicitudNuevaCarga);

						  
						}
					 
		//2: registro accionsolciitud
						try {
							for (AccionSolicitudDTO accionSolicitudDTO : solicitud.getListaAccionSolicitud()) {
								AccionSolicitud accionSolicitud = ConversorHelper.convertir(AccionSolicitud.class, accionSolicitudDTO);
								accionSolicitud.setSolicitud(solicitudNuevaCarga);
								accionSolicitudDAO.registrar(accionSolicitud);
							}
						} catch (Exception e) {
							System.out.println(e.getMessage());
							e.printStackTrace();
						}
			 
	   //3: registro SolicitudDocumento
							for (Documento doc : documentos) {
								SolicitudDocumentoPK pk = new SolicitudDocumentoPK(doc.getIdDocumento(), solicitudNuevaCarga.getIdSolicitud());
								SolicitudDocumento solicitudDocumento = new SolicitudDocumento();
								solicitudDocumento.setSolicitudDocumentoPK(pk);
								solicitudDocumento.setFechaCreacion(FechaUtil.obtenerFechaActual());
								solicitudDocumento.setUsuarioCreacion(solicitud.getUsuarioCreacion());
								solicitudDocumento.setEstado(EstadoState.ACTIVO.getValue());
								solicitudDocumento = solicitudDocumentoDAO.registrarSolicitudDocumentoNuevo(solicitudDocumento);
							}
			
							
					 	SolicitudDTO solicitudNuevoDTO = ConversorHelper.convertir(SolicitudDTO.class, solicitudNuevaCarga);
						return solicitudNuevoDTO;
	}

}
