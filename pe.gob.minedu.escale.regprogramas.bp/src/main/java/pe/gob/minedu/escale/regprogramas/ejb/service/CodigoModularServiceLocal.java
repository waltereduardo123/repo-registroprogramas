package pe.gob.minedu.escale.regprogramas.ejb.service;

import pe.gob.minedu.escale.regprogramas.model.dto.CodigoModularDTO;

public interface CodigoModularServiceLocal {

	CodigoModularDTO obtenerCodigoModular() throws Exception;

	CodigoModularDTO actualizar(CodigoModularDTO codigoModular) throws Exception;
}
