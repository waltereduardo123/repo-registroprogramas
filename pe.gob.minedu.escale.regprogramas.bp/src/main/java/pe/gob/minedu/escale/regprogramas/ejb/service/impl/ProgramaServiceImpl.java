package pe.gob.minedu.escale.regprogramas.ejb.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;







import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.business.state.EstadoState;

import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaCodigoModularRepetidoException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoActualizadoException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoSincronizadoException;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AuditoriaProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.CorteDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.CorteDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.AuditoriaPrograma;
import pe.gob.minedu.escale.regprogramas.model.jpa.Corte;
import pe.gob.minedu.escale.regprogramas.model.jpa.Maestro;
import pe.gob.minedu.escale.regprogramas.model.jpa.Programa;


@PermitAll
@Stateless(name = "ProgramaService", mappedName = "ejb/ProgramaService")
public class ProgramaServiceImpl implements ProgramaServiceLocal {

	/** El servicio usuario dao. */
	@EJB
	private ProgramaDAOLocal programaDAO;
	
	@EJB
	private AuditoriaProgramaDAOLocal auditoriaProgramaDAO;
	
	@EJB
	private SolicitudDAOLocal solictudDAO;
	
	@EJB
	private CorteDAOLocal corteDAO;

	@Override
	public ProgramaDTO registrarProgramaNuevo(ProgramaDTO programa) throws Exception {
		try {
			Programa entity = ConversorHelper.convertir(Programa.class, programa);
			entity = programaDAO.registrar(entity);
			programa = ConversorHelper.convertir(ProgramaDTO.class, entity);
		} catch (Exception e) {
			throw new ProgramaNoRegistradoException(programa.getCodigoModular());
		}
		return programa;
	}

	@Override
	public ProgramaDTO findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProgramaDTO> buscarProgramasxCodooii(char mander) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProgramaDTO> buscarProgramasxNombre(String nombrePrograma) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProgramaDTO> buscarProgramasxEstado(String estado) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProgramaDTO> listarProgramas() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProgramaDTO> listarProgramasByFirstMaxResult(int batchSize, int index) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<ProgramaDTO> listarProgramasFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<Programa> listaPrograma = programaDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<ProgramaDTO> listaProgramaDTO = ConversorHelper.convertirTodo(ProgramaDTO.class, listaPrograma);
		return listaProgramaDTO;
	}
	
	@Override
	public List<Programa> listarProgramasFirstMaxResultParametrosResetPrograma(int batchSize, int index, Map parameters)
			throws Exception {
		List<Programa> listaPrograma = programaDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		return listaPrograma;
	}

	@Override
	public ProgramaDTO actualizar(ProgramaDTO programa) throws Exception {
		Programa entity = ConversorHelper.convertir(Programa.class, programa);
		try {
			programaDAO.update(entity);
		} catch (Exception e) {
			throw new ProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity.getCodigoModular())?entity.getCodigoModular():"No hay codigo modular" );
		}		
		ProgramaDTO entityDTO = ConversorHelper.convertir(ProgramaDTO.class, entity);
		return entityDTO;		
	}

	@Override
	public Integer actualizarSituacPrograma(Map parameters, MaestroDTO maestroSituacion, MaestroDTO maestroEstado,MaestroDTO maestroPeriodo,List<Long> listasituaciones,Long idSituacionRevAnt,CorteDTO corte) throws Exception {
		Integer can = 0;//cantidad de registros actualizados
		int cantidaSol = 0;
		List<Programa> listaPrograma = programaDAO.findByResultProgramasReinicio(parameters);
		Maestro maestroEntySituacion = ConversorHelper.convertir(Maestro.class, maestroSituacion);
		Maestro maestroEntyEstado = ConversorHelper.convertir(Maestro.class, maestroEstado);
		//Maestro maestroTemp;
		AuditoriaPrograma aud; 
		Corte corteEntity=ConversorHelper.convertir(Corte.class, corte);
		try {
			
			for (Programa pr : listaPrograma) {
				//Programa prTem=new Programa();
				//INSERCION DE AUDITORIA
				aud = new AuditoriaPrograma();
				ConversorHelper.fusionaPropiedades(pr, aud);
				aud.setIdPrograma(null);
				
				auditoriaProgramaDAO.registrarNoFlush(aud);
				
				//PROGRAMA
				if(pr.getTipoSituacion().getIdMaestro().equals(maestroEntySituacion.getIdMaestro())){//Seteo de ESTADO 9
					//System.out.println("OBVIAR");
					pr.setTipoEstado(maestroEntyEstado);
					pr.setEstado(EstadoState.NORENOVADO.getValue());
					
				}else{//Seteo POR ACTUALIZAR
					pr.setTipoSituacion(maestroEntySituacion);
				}
								
				//ACTUALIZACIÓN DEL PROGRAMA
				//pr.setTipoSituacion(maestroEntySituacion);
				programaDAO.update(pr);
				
				can++;
			}
			
			// ACTUALIZACION DE LAS SOLICITUDES
			cantidaSol =  solictudDAO.reiniciarSituacionDeSolicitudes(listasituaciones, idSituacionRevAnt,maestroPeriodo.getIdMaestro());
			if(cantidaSol>0){
				//registramos el corte
				//corteDAO.registrar(corteEntity);
			System.out.println("YA NO SE REGISTRA EL CORTE.");
			}
	    		
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
//    			throw new ProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity)?entity.getCodigoModular():"No hay codigo modular" );
    	}	
	    return can;
	}
	
	@Override
	public Integer actualizarSituacPrograma(Map parameters, MaestroDTO maestroSituacion, MaestroDTO maestroEstado,MaestroDTO maestroPeriodo,List<Long> listasituaciones,Long idSituacionRevAnt) throws Exception {
		Integer can = 0;//cantidad de registros actualizados
		int cantidaSol = 0;
		List<Programa> listaPrograma = programaDAO.findByResultProgramasReinicio(parameters);
		Maestro maestroEntySituacion = ConversorHelper.convertir(Maestro.class, maestroSituacion);
		Maestro maestroEntyEstado = ConversorHelper.convertir(Maestro.class, maestroEstado);
		//Maestro maestroTemp;
		AuditoriaPrograma aud; 
//		Corte corteEntity=ConversorHelper.convertir(Corte.class, corte);
		try {
			
			for (Programa pr : listaPrograma) {
				//Programa prTem=new Programa();
				//INSERCION DE AUDITORIA
				aud = new AuditoriaPrograma();
				ConversorHelper.fusionaPropiedades(pr, aud);
				aud.setIdPrograma(null);
				
				auditoriaProgramaDAO.registrarNoFlush(aud);
				
				//PROGRAMA
				if(pr.getTipoSituacion().getIdMaestro().equals(maestroEntySituacion.getIdMaestro())){//Seteo de ESTADO 9
					//System.out.println("OBVIAR");
					pr.setTipoEstado(maestroEntyEstado);
					pr.setEstado(EstadoState.NORENOVADO.getValue());
					
				}else{//Seteo POR ACTUALIZAR
					pr.setTipoSituacion(maestroEntySituacion);
				}
								
				//ACTUALIZACIÓN DEL PROGRAMA
				//pr.setTipoSituacion(maestroEntySituacion);
				programaDAO.update(pr);
				
				can++;
			}
			
			// ACTUALIZACION DE LAS SOLICITUDES
			cantidaSol =  solictudDAO.reiniciarSituacionDeSolicitudes(listasituaciones, idSituacionRevAnt,maestroPeriodo.getIdMaestro());
//			if(cantidaSol>0){
//				//registramos el corte
//				//corteDAO.registrar(corteEntity);
//			System.out.println("YA NO SE REGISTRA EL CORTE.");
//			}
	    		
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
//    			throw new ProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity)?entity.getCodigoModular():"No hay codigo modular" );
    	}	
	    return can;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ProgramaDTO buscarProgramaxCodigoModular(String codigoModular) throws Exception {
		List<Programa> listaPrograma = new ArrayList<Programa>();
		List<ProgramaDTO> lista  = new ArrayList<ProgramaDTO>();
		listaPrograma = programaDAO.buscarProgramasxCodigoModular(codigoModular);
		if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
			if (!(listaPrograma.size()>1)) {
				lista = ConversorHelper.convertirTodo(ProgramaDTO.class, listaPrograma);
			}else{
				throw new ProgramaCodigoModularRepetidoException(StringUtil.isNotNullOrBlank(codigoModular)?String.valueOf(codigoModular):"No hay codigo modular" );
			}
		}else{
			throw new ProgramaNoExisteException();
		}		
		return lista.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ProgramaDTO buscarProgramaCodigoModular(String codigoModular) throws Exception {
		List<Programa> listaPrograma = new ArrayList<Programa>();
		List<ProgramaDTO> lista  = new ArrayList<ProgramaDTO>();
		listaPrograma = programaDAO.buscarProgramasCodigoModular(codigoModular);
		if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
			if (!(listaPrograma.size()>1)) {
				lista = ConversorHelper.convertirTodo(ProgramaDTO.class, listaPrograma);
			}else{
				throw new ProgramaCodigoModularRepetidoException(StringUtil.isNotNullOrBlank(codigoModular)?String.valueOf(codigoModular):"No hay codigo modular" );
			}
		}		
		return lista.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ProgramaDTO buscarProgramasSituacionxCodigoModular(String codigoModular) throws Exception {
		List<Programa> listaPrograma = new ArrayList<Programa>();
		List<ProgramaDTO> lista  = new ArrayList<ProgramaDTO>();
		listaPrograma = programaDAO.buscarProgramasSituacionxCodigoModular(codigoModular);
		if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
			if (!(listaPrograma.size()>1)) {
				lista = ConversorHelper.convertirTodo(ProgramaDTO.class, listaPrograma);
			}else{
				throw new ProgramaCodigoModularRepetidoException(StringUtil.isNotNullOrBlank(codigoModular)?String.valueOf(codigoModular):"No hay codigo modular" );
			}
		}else{
			throw new ProgramaNoExisteException();
		}		
		return lista.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Programa> listarProgramasActivos(Map parameters) throws Exception {
		List<Programa> listaPrograma = null;
		try {
			
			 listaPrograma = programaDAO.findByResultProgramasReinicio(parameters);

    	} catch (Exception e) {
    		System.out.println(e.getMessage());
//    			throw new ProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity)?entity.getCodigoModular():"No hay codigo modular" );
    	}	
	    return listaPrograma;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<String> listCentroPobladoActivo(Map parameters) throws Exception {
		List<String> listaPrograma = new ArrayList<String>();
		List<ProgramaDTO> lista  = new ArrayList<ProgramaDTO>();
		try {
			listaPrograma = programaDAO.findByResultProgramasSincroniza(parameters);
//			if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
//				//if (!(listaPrograma.size()>1)) {
//						lista = ConversorHelper.convertirTodo(ProgramaDTO.class, listaPrograma);
//				//}
//			}
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
//    			throw new ProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity)?entity.getCodigoModular():"No hay codigo modular" );
    	}	
	    return listaPrograma;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProgramaDTO> listCentroPobladoActivoUpdate(Map parameters) throws Exception {
//	public ProgramaDTO listCentroPobladoActivoUpdate(Map parameters) throws Exception {
		List<Programa> listaPrograma = new ArrayList<Programa>();
		List<ProgramaDTO> lista  = new ArrayList<ProgramaDTO>();
		ProgramaDTO listaTemp=new ProgramaDTO();
	//	try {
			listaPrograma = programaDAO.findByResultProgramasSincronizaUpdate(parameters);
	
			if (Objects.nonNull(listaPrograma) && !listaPrograma.isEmpty()) {
//				if (!(listaPrograma.size()>1)) {
				if (listaPrograma.size()>0) {
					lista = ConversorHelper.convertirTodo(ProgramaDTO.class, listaPrograma);
					listaTemp=lista.get(0);
				}else{
					listaTemp.setCodigoCentroPoblado("error");
					throw new ProgramaNoSincronizadoException(StringUtil.isNotNullOrBlank(parameters.get("centropoblado"))?String.valueOf(parameters.get("centropoblado")):"No hay centro poblado" );
					
				}
			}else{
				listaTemp.setCodigoCentroPoblado("error");
				throw new ProgramaNoSincronizadoException(StringUtil.isNotNullOrBlank(parameters.get("centropoblado"))?String.valueOf(parameters.get("centropoblado")):"No hay centro poblado" );
			}		
			return lista;
	}
	
	

	
	
	
	@Override
	public List<Object> listCodModularProgramaActivo(Map parameters) throws Exception {
		List<Object> listaPrograma = null;
		try {
			
			 listaPrograma = programaDAO.findProgramasByCentroPoblado(parameters);

    	} catch (Exception e) {
    		System.out.println(e.getMessage());
//    			throw new ProgramaNoActualizadoException(StringUtil.isNotNullOrBlank(entity)?entity.getCodigoModular():"No hay codigo modular" );
    	}	
	    return listaPrograma;
	}
	
}
