package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Periodo;


@Local
public interface PeriodoDAOLocal extends StatelessCRUDServices<Long, Periodo> {
	
	public void save(Periodo entity);
	Periodo registrarPeriodo(Periodo entity);
	void actualizarPerodo(Periodo entity);
	List<Periodo> buscarPeriodoxNombreItem(String nombrePeriodo, String item );
	List<Periodo> listPeriodo();
	List<Periodo> listPeriodoActivo();
	List<Periodo> grillaPeriodo(int batchSize, int index, String jpqlRules, String jpqlOrder);
}
