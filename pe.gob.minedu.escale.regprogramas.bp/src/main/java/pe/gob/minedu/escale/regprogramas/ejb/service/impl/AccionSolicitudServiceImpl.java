package pe.gob.minedu.escale.regprogramas.ejb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AccionSolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.AccionSolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.AccionSolicitud;

@PermitAll
@Stateless(name = "AccionSolicitudService", mappedName = "ejb/AccionSolicitudService")
public class AccionSolicitudServiceImpl implements AccionSolicitudServiceLocal {

	@EJB
	private AccionSolicitudDAOLocal accionSolicitudDAO;
	
	@EJB
	private SolicitudDAOLocal solicitudDAO;

	// AccionSolicitud

	@SuppressWarnings("unchecked")
	public List<AccionSolicitudDTO> listarAccionSolicitud() throws Exception {
		List<AccionSolicitud> lista = accionSolicitudDAO.findAll();
		List<AccionSolicitudDTO> list = ConversorHelper.convertirTodo(AccionSolicitudDTO.class, lista);
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccionSolicitudDTO> buscarAccionSolicitudxIdSolicitud(Long idSolicitud) throws Exception {
		List<AccionSolicitud> lista = accionSolicitudDAO.findByIdSolicitud(idSolicitud);
		List<AccionSolicitudDTO> listSolicitud = ConversorHelper.convertirTodo(AccionSolicitudDTO.class, lista);
		return listSolicitud;
	}

	@Override
	public AccionSolicitudDTO registrar(AccionSolicitudDTO accionSolicitud) throws Exception {
		AccionSolicitud entity = ConversorHelper.convertir(AccionSolicitud.class, accionSolicitud);
		entity = accionSolicitudDAO.registrar(entity);
		AccionSolicitudDTO entityDTO = ConversorHelper.convertir(AccionSolicitudDTO.class, entity);
		return entityDTO;
	}

	@Override
	public List<AccionSolicitudDTO> listarByFirstMaxResult(int batchSize, int index) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<AccionSolicitudDTO> listarFirstMaxResultParametros(int batchSize, int index, Map parameters)
			throws Exception {
		List<AccionSolicitud> listaSolicitud = accionSolicitudDAO.findByFirstMaxResultParametros(batchSize, index, parameters);
		List<AccionSolicitudDTO> listaAccionSolicitudDTO = ConversorHelper.convertirTodo(AccionSolicitudDTO.class, listaSolicitud);
		return listaAccionSolicitudDTO;
	}

	

}
