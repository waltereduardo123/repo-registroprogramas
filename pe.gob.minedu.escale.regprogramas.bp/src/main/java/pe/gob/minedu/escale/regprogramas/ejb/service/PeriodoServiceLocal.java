package pe.gob.minedu.escale.regprogramas.ejb.service;

import java.util.List;


import pe.gob.minedu.escale.regprogramas.model.dto.PeriodoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.PeriodoBandejaDTO;


public interface PeriodoServiceLocal {
  PeriodoDTO registrarNuevoPeriodo(PeriodoDTO periodo) throws Exception;
  PeriodoDTO actalizarPeriodo(PeriodoDTO periodo) throws Exception;
  List<PeriodoDTO> buscarPeriodoxNombreItem(String nombrePeriodo, String item ) throws Exception;
  List<PeriodoDTO> listPeriodo() throws Exception;
  List<PeriodoDTO> listPeriodoActivo() throws Exception;
  List<PeriodoBandejaDTO> grillaPeriodo(int batchSize, int index, String jpqlRules, String jpqlOrder) throws Exception;
}
