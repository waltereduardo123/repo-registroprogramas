package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.ParametroDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Parametro;

@Stateless
public class ParametroDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Parametro> implements ParametroDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	public ParametroDAOImpl() {
	}

	public void save(Parametro entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(Parametro entity) {
		em.merge(entity);
	}

	public void delete(Parametro entity) {
		em.remove(entity);
	}

	public Parametro registrar(Parametro entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	public EntityManager getEntityManager() {
		return em;
	}
	

	@SuppressWarnings("unchecked")
	public List<Parametro> findByIdParametro(Long idParametro) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_parametro m ");
		sbQuery.append("WHERE m.N_ID_PARAMETRO = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Parametro.class);

		query.setParameter(1, idParametro);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Parametro> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM tbl_regpro_parametro");
			query = em.createNativeQuery(sbQuery.toString(), Parametro.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}


	@Override
	public List<Parametro> findByFirstMaxResult(int batchSize, int index) {
		List<Parametro> listaParametro = findAll(Parametro.class, batchSize, index);
		return listaParametro;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Parametro> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new Parametro(s.idParametro,s.codigoParametro,"
				+ " s.descripcionParametro,s.valorLetra, s.valorNumerico, s.valorFecha, s.estado )"
				+ " FROM Parametro s ");
		sbQuery.append(" WHERE 1=1 ");		
		if (parameters.containsKey("descripcionParametro")) {
			sbQuery.append(" AND upper(s.descripcionParametro) = :descripcionParametro ");
		}
		if (parameters.containsKey("codigoParametro")) {
			sbQuery.append(" AND upper(s.codigoParametro) = :codigoParametro ");
		}
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND s.estado = :estado ");
		}

		List<Parametro> listaParametro = synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		return listaParametro;
	}



}
