package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Solicitud;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface SolicitudDAOLocal extends StatelessCRUDServices<Long, Solicitud> {

	void save(Solicitud entity);
	void update(Solicitud entity);
	void delete(Solicitud entity);
	Solicitud registrar(Solicitud entity);
	Solicitud registrarSolicitudConDocumento(Solicitud entity);
	List<Solicitud> findByIdSolicitud(Long idSolicitud);
	Solicitud findById(Long idSolicitud);
	List<Solicitud> findByNombreProgramaCodooii(String nombrePrograma, String codooii, String codigoModular, String tipoSolicitud);
	List<Solicitud> findByNameProgramaCodooii(String nombrePrograma, String codigoModular, String tipoSolicitud);//Solcitudes de carga werr
	List<Solicitud> findAll();
	List<Solicitud> findByFirstMaxResult(int batchSize, int index);
	@SuppressWarnings("rawtypes")
	List<Solicitud> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
	List<Solicitud> bandejaRevision(int batchSize, int index, String jpqlRules, String jpqlOrder);
	int reiniciarSituacionDeSolicitudes(List<Long> liSituacion,Long idProcesoAnt,Long idPerido);
	
}
