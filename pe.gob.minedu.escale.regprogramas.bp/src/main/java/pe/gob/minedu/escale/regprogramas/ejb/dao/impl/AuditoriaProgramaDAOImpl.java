package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoProgramaState;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.ejb.dao.AuditoriaProgramaDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.AuditoriaPrograma;

@Stateless
public class AuditoriaProgramaDAOImpl extends AbstractJtaStatelessCRUDServices<Long, AuditoriaPrograma> implements AuditoriaProgramaDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate

	public EntityManager getEntityManager() {
		return em;
	}

	public AuditoriaProgramaDAOImpl() {
	}

	public void save(AuditoriaPrograma entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(AuditoriaPrograma entity) {
		inciarSessionFactory();
		AuditoriaPrograma pro = getSess().get(AuditoriaPrograma.class, entity.getIdPrograma());
		try {
			ConversorHelper.fusionaPropiedades(entity, pro);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(pro);
	}

	public void delete(AuditoriaPrograma entity) {
		em.remove(entity);
	}

	public AuditoriaPrograma findById(Long id) {
		return em.find(AuditoriaPrograma.class, id);
	}

	public AuditoriaPrograma registrar(AuditoriaPrograma entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@Override
	public AuditoriaPrograma registrarNoFlush(AuditoriaPrograma entity) {
		em.persist(entity);
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	public List<AuditoriaPrograma> buscarProgramasxCodooii(char mander) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM aud_regpro_programa m ");
		sbQuery.append("WHERE m.CODOOII = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), AuditoriaPrograma.class);

		query.setParameter(1, mander);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<AuditoriaPrograma> buscarProgramasxCodigoModular(String codigoModular) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM aud_regpro_programa m ");
		sbQuery.append("WHERE m.C_CODMOD = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), AuditoriaPrograma.class);

		query.setParameter(1, codigoModular);
		query.setParameter(2, EstadoProgramaState.ACTIVO.getValue());

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<AuditoriaPrograma> buscarProgramasxNombre(String nombrePrograma) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				"select new Programa(idPrograma,codigoModular, tipoPrograma, nombrePrograma, latitudPrograma, longitudPrograma, ");
		sbQuery.append(
				" tipoGestion,  tipoDependencia, tipoGestionEducativa, codooii, tipoTurno, tipoContinuidadJornadaEscolar,  ");
		sbQuery.append(" codgeo, marcoCensal, fechaCreacion, estado) ");
		sbQuery.append(" FROM Programa ");
		sbQuery.append(" where upper(nombrePrograma) like :nombrePrograma ");

		Query query = em.createQuery(sbQuery.toString(), AuditoriaPrograma.class);

		query.setParameter("nombrePrograma", "%".concat(nombrePrograma.toUpperCase()).concat("%"));

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<AuditoriaPrograma> buscarProgramasxEstado(String estado) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				"select new Programa(idPrograma,codigoModular, tipoPrograma, nombrePrograma, latitudPrograma, longitudPrograma, ");
		sbQuery.append(
				" tipoGestion,  tipoDependencia, tipoGestionEducativa, codooii, tipoTurno, tipoContinuidadJornadaEscolar,  ");
		sbQuery.append(" codgeo, marcoCensal, fechaCreacion, estado) ");
		sbQuery.append(" FROM AuditoriaPrograma  ");
		sbQuery.append(" WHERE estado = :estado ");

		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("estado", estado);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<AuditoriaPrograma> findAll() {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM aud_regpro_programa m ");
		sbQuery.append("WHERE m.C_ESTADO = ?1 ");

		Query query = em.createNativeQuery(sbQuery.toString(), AuditoriaPrograma.class);

		query.setParameter(1, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@Override
	public List<AuditoriaPrograma> findByFirstMaxResult(int batchSize, int index) {
		List<AuditoriaPrograma> listaPrograma = findAll(AuditoriaPrograma.class, batchSize, index);
		return listaPrograma;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<AuditoriaPrograma> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(
				"select new AuditoriaPrograma(p.idPrograma,p.codigoModular, p.nombrePrograma, p.tipoSituacion.idMaestro, p.tipoSituacion.nombreMaestro,");
		sbQuery.append(
				" p.dreUgel.codigo, p.fechaCreacionPrograma,p.fechaCierrePrograma,p.fechaRenovacionPrograma,p.estado) ");
		sbQuery.append(" FROM Programa as p ");
		// sbQuery.append(" LEFT JOIN p.tipoSituacion as m ");
		// if(parameters.containsKey("tipoSituacion")){
		// sbQuery.append(" LEFT JOIN p.tipoSituacion as m ");
		// }
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("nombrePrograma") && StringUtil.isNotNullOrBlank(parameters.get("nombrePrograma"))) {
			sbQuery.append(" AND upper(p.nombrePrograma) = :nombrePrograma ");
		}
		if (parameters.containsKey("idPrograma")) {
			sbQuery.append(" AND p.idPrograma = :idPrograma ");
		}
		if (parameters.containsKey("codooii")) {
			sbQuery.append(" AND p.dreUgel.codigo = :codooii ");
		}
		if (parameters.containsKey("codigoModular")) {
			sbQuery.append(" AND p.codigoModular = :codigoModular ");
		}
		// if(parameters.containsKey("tipoSituacion")){
		// sbQuery.append(" AND m.codigoItem = :tipoSituacion");
		// }
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND p.estado = :estado ");
		}

		List<AuditoriaPrograma> listaPrograma = synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		return listaPrograma;
	}

	@SuppressWarnings("unchecked")
	public List<AuditoriaPrograma> buscarCodigoModular(String codigoModular) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM aud_regpro_programa m ");
		sbQuery.append("WHERE m.C_CODMOD = ?1  ");		
		Query query = em.createNativeQuery(sbQuery.toString(), AuditoriaPrograma.class);

		query.setParameter(1, codigoModular);		

		return query.getResultList();
	}
	
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}


}
