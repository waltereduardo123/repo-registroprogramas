package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.CondicionState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.regprogramas.ejb.dao.CodigoModularDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.CodigoModular;

@Stateless
public class CodigoModularDAOImpl extends AbstractJtaStatelessCRUDServices<Long, CodigoModular>	implements CodigoModularDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate

	public CodigoModularDAOImpl() {
	}

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	public void save(CodigoModular entity) {
		em.persist(entity);
		em.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CodigoModular obtenerCodigoModularDisponible() {
		StringBuilder sbQuery = new StringBuilder();
		StringBuilder sbQueryUpdate = new StringBuilder();
		
		sbQuery.append(" SELECT a.id,");
		sbQuery.append(" CAST(AES_DECRYPT(UNHEX(a.codmod),SHA1('iniadmin')) AS CHAR(7)) codmod, ");
		sbQuery.append(" a.asignado, a.codooii_asig, a.usuario_asig, a.fecha_asig");
		sbQuery.append(" FROM codmod_asig a ");
		sbQuery.append(" WHERE a.id = (SELECT MAX(b.id)+1 FROM codmod_asig b WHERE b.asignado= ?1 ) ");

		Query query = em.createNativeQuery(sbQuery.toString(), CodigoModular.class);
		
		query.setParameter(1, CondicionState.SI.getDescription(ResourceUtil.obtenerLocaleSession()));

		List<CodigoModular> lista = query.getResultList();

		sbQueryUpdate.append("UPDATE codmod_asig ");
		sbQueryUpdate.append("SET asignado=?1 ");
		sbQueryUpdate.append("WHERE id=?2");
		
		Query query2 = em.createNativeQuery(sbQueryUpdate.toString(), CodigoModular.class);
		
		query2.setParameter(1, CondicionState.SI.getDescription(ResourceUtil.obtenerLocaleSession()));
		query2.setParameter(2, lista.get(0).getId());
		
		@SuppressWarnings("unused")
		int actualizado = query2.executeUpdate();
		
		return lista.get(0);
	}

	public void update(CodigoModular entity) {
		inciarSessionFactory();
		CodigoModular co = getSess().get(CodigoModular.class, entity.getId());
		try {
			ConversorHelper.fusionaPropiedades(entity, co);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(co);
	}

	public void delete(CodigoModular entity) {
		em.remove(entity);
	}

	// mejorar codigo y generalizarlo
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	// mejorar codigo y generalizarlo

}
