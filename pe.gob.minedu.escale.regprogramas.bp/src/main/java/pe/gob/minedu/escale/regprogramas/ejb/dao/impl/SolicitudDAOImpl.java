package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.ejb.dao.SolicitudDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Solicitud;

@Stateless
public class SolicitudDAOImpl extends AbstractJtaStatelessCRUDServices<Long, Solicitud> implements SolicitudDAOLocal{

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}

	// Fin Factoria Hibernate

	public EntityManager getEntityManager() {
		return em;
	}

	public void save(Solicitud entity) {
		em.persist(entity);
		em.flush();
	}

	public void update(Solicitud entity) {
		inciarSessionFactory();
		Solicitud sol = getSess().get(Solicitud.class, entity.getIdSolicitud());
		try {
			ConversorHelper.fusionaPropiedades(entity, sol);
		} catch (Exception e) {
			throw new ConversorException();
		}
		getSess().update(sol);
	}

	public void delete(Solicitud entity) {
		em.remove(entity);
	}

	public Solicitud registrar(Solicitud entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	public Solicitud findById(Long id) {
		Solicitud entity = em.find(Solicitud.class, id);
		return entity;
	}

	// Negocio
	@SuppressWarnings("unchecked")
	public List<Solicitud> findByIdSolicitud(Long idSolicitud) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append("SELECT * FROM tbl_regpro_solicitud m ");
		sbQuery.append("WHERE m.N_ID_SOLICITUD = ?1  ");
		sbQuery.append("AND m.C_ESTADO = ?2 ");
		Query query = em.createNativeQuery(sbQuery.toString(), Solicitud.class);

		query.setParameter(1, idSolicitud);
		query.setParameter(2, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Solicitud> findAll() {
		StringBuilder sbQuery = new StringBuilder();
		Query query = null;
		try {
			sbQuery.append("SELECT * FROM tbl_regpro_solicitud");
			query = em.createNativeQuery(sbQuery.toString(), Solicitud.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return query.getResultList();
	}

	public Solicitud registrarSolicitudConDocumento(Solicitud entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Solicitud> findByFirstMaxResult(int batchSize, int index) {
		List<Solicitud> listaSolicitud = findAll(Solicitud.class, batchSize, index);
		return listaSolicitud;
	}

	@SuppressWarnings("rawtypes")
	public List<Solicitud> findByFirstMaxResultParametros(int batchSize, int index, Map parameters) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT new Solicitud(s.idSolicitud,s.codSolicitud,"
				+ " s.codigoModular,s.nombrePrograma, s.tipoSolicitud.nombreMaestro, s.tipoSolicitud.idMaestro, s.tipoSituacionSolicitud.nombreMaestro, "
				+ " s.fechaCreacion,s.usuarioCreacion,s.estado, s.tipoSituacionPrograma.idMaestro, s.indicadorUltimo, s.tipoSituacionSolicitud.idMaestro, s.tipoSituacionSolicitud.codigoItem ,"
				+ " s.tipoSituacionRevision.idMaestro, s.tipoSituacionRevisionSig.idMaestro )"
				+ " FROM Solicitud s ");
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("nombrePrograma")) {
			sbQuery.append(" AND upper(s.nombrePrograma) = :nombrePrograma ");
		}
		if (parameters.containsKey("codSolicitud")) {
			sbQuery.append(" AND upper(s.codSolicitud) = :codSolicitud ");
		}
		if (parameters.containsKey("codigoModular")) {
			sbQuery.append(" AND s.codigoModular = :codigoModular ");
		}
		if (parameters.containsKey("tipoSituacionSolicitud")) {
			sbQuery.append(" AND s.tipoSituacionSolicitud.idMaestro = :tipoSituacionSolicitud ");
		}
		if (parameters.containsKey("estado")) {
			sbQuery.append(" AND s.estado = :estado ");
		}

		List<Solicitud> listaSolicitud = synchronizedFind(sbQuery.toString(), batchSize, index, parameters);
		return listaSolicitud;
	}

	@SuppressWarnings("unchecked")
	public List<Solicitud> findByNombreProgramaCodooii(String nombrePrograma, String codooii, String codigoModular,
			String tipoSolicitud) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				"Select s.* from tbl_regpro_solicitud s INNER JOIN tbl_regpro_maestro m ON s.N_ID_TIPSOLI= m.N_ID_MAESTRO");
		sbQuery.append(" WHERE 1=1 ");
		if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
			sbQuery.append(" AND s.C_NOMPROG = ?1 ");
		}
		if (StringUtil.isNotNullOrBlank(codooii)) {
			sbQuery.append(" AND s.CODIGO = ?2 ");
		}
		if (StringUtil.isNotNullOrBlank(codigoModular)) {
			sbQuery.append(" AND s.C_CODMOD = ?3 ");
		}
		sbQuery.append(" AND s.C_ESTADO = ?4");
		if (StringUtil.isNotNullOrBlank(tipoSolicitud)) {
			if (tipoSolicitud.equals(TipoSolicitudType.CREACION.getKey())) {
				sbQuery.append(" AND m.C_CODITEM IN (\"1201\",\"1101\")");
			}
			if (!StringUtil.isNotNullOrBlank(codigoModular)) {
				if (tipoSolicitud.equals(TipoSolicitudType.MODIFICACION.getKey())) {
					sbQuery.append(" AND m.C_CODITEM IN (\"1201\",\"1101\")");
				}
			}
//			if (tipoSolicitud.equals(TipoSolicitudType.RENOVACION.getKey())
//					|| tipoSolicitud.equals(TipoSolicitudType.MODIFICACION.getKey())) {
//				sbQuery.append(" AND m.C_CODITEM IN (\"1100\",\"1101\",\"1700\")");
//				sbQuery.append(" AND s.N_ID_TIPSITU IN (" + Long.valueOf(TipoSituacionSolicitudType.APROBADO.getKey())
//						+ "," + Long.valueOf(TipoSituacionSolicitudType.PENDIENTE.getKey()) + ","
//						+ Long.valueOf(TipoSituacionSolicitudType.REVISION.getKey()) + ","
//						+ Long.valueOf(TipoSituacionSolicitudType.SUSTENTADO.getKey()) + ","
//						+ Long.valueOf(TipoSituacionSolicitudType.OBSERVADO.getKey())+")");
//			}
//			if (tipoSolicitud.equals(TipoSolicitudType.CIERRE.getKey())) {
//				sbQuery.append(" AND m.C_CODITEM IN (\""+TipoSolicitudType.CIERRE.getKey()+"\",\""+TipoSolicitudType.MODIFICACION.getKey()+"\",\""+TipoSolicitudType.RENOVACION.getKey()+"\")");
//				sbQuery.append(" AND s.N_ID_TIPSITU IN (" +Long.valueOf(TipoSituacionSolicitudType.PENDIENTE.getKey()) + ","
//				+ Long.valueOf(TipoSituacionSolicitudType.REVISION.getKey()) + ","
//				+ Long.valueOf(TipoSituacionSolicitudType.SUSTENTADO.getKey()) + ")");
//			}
			
			
			if (tipoSolicitud.equals(TipoSolicitudType.RENOVACION.getKey())
					|| tipoSolicitud.equals(TipoSolicitudType.MODIFICACION.getKey())) {
				sbQuery.append(" AND m.C_CODITEM IN (\"1100\",\"1101\",\"1700\")");
				sbQuery.append(" AND s.N_ID_TIPSITU IN (" + Long.valueOf("276")
						+ "," + Long.valueOf("273") + ","
						+ Long.valueOf("274") + ","
						+ Long.valueOf("278") + ","
						+ Long.valueOf("275")+")");
			}
			if (tipoSolicitud.equals(TipoSolicitudType.CIERRE.getKey())) {
				sbQuery.append(" AND m.C_CODITEM IN (\""+TipoSolicitudType.CIERRE.getKey()+"\",\""+TipoSolicitudType.MODIFICACION.getKey()+"\",\""+TipoSolicitudType.RENOVACION.getKey()+"\")");
				sbQuery.append(" AND s.N_ID_TIPSITU IN (" +Long.valueOf("273") + ","
				+ Long.valueOf("274") + ","
				+ Long.valueOf("278") + ")");
			}
			
		}
		sbQuery.append(" AND s.C_ESTADO = ?4");
		Query query = em.createNativeQuery(sbQuery.toString(), Solicitud.class);

		if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
			query.setParameter(1, nombrePrograma);
		}
		if (StringUtil.isNotNullOrBlank(codooii)) {
			query.setParameter(2, codooii);
		}
		if (StringUtil.isNotNullOrBlank(codigoModular)) {
			query.setParameter(3, codigoModular);
		}

		query.setParameter(4, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Solicitud> findByNameProgramaCodooii(String nombrePrograma, String codigoModular,
			String tipoSolicitud) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				"Select s.* from tbl_regpro_solicitud s INNER JOIN tbl_regpro_maestro m ON s.N_ID_TIPSOLI= m.N_ID_MAESTRO");
		sbQuery.append(" WHERE 1=1 ");
		if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
			sbQuery.append(" AND s.C_NOMPROG = ?1 ");
		}
		if (StringUtil.isNotNullOrBlank(codigoModular)) {
			sbQuery.append(" AND s.C_CODMOD = ?3 ");
		}
		sbQuery.append(" AND s.C_ESTADO = ?4");
		if (StringUtil.isNotNullOrBlank(tipoSolicitud)) {
			if (tipoSolicitud.equals(TipoSolicitudType.CARGA.getKey())) {
				sbQuery.append(" AND m.C_CODITEM IN (\"1111\")");
			}
		}
		Query query = em.createNativeQuery(sbQuery.toString(), Solicitud.class);

		if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
			query.setParameter(1, nombrePrograma);
		}
		if (StringUtil.isNotNullOrBlank(codigoModular)) {
			query.setParameter(3, codigoModular);
		}

		query.setParameter(4, EstadoState.ACTIVO.getValue());

		return query.getResultList();
	}
	
	
	@SuppressWarnings("rawtypes")
	public int anularSolicitudxCierre(Map parameters){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" UPDATE Solicitud s");
		sbQuery.append(" SET s ");
		sbQuery.append(" s.tipoSituacionSolicitud.idMaestro = "+Long.valueOf(TipoSituacionSolicitudType.ANULADO.getKey()));
		sbQuery.append(" WHERE 1=1 ");
		if (parameters.containsKey("codigoModular")) {
			sbQuery.append(" AND s.codigoModular = :codigoModular ");
		}
		return update(sbQuery.toString(), parameters);
	}
	
	@SuppressWarnings("rawtypes")
	public int reiniciarSituacionDeSolicitudes(List<Long> liSituacion,Long idProcesoAnt,Long idPeriodo){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" UPDATE Solicitud s");
		sbQuery.append(" SET ");
		sbQuery.append(" s.tipoSituacionSolicitud.idMaestro = "+idProcesoAnt + ", s.periodoRegistrado.idMaestro = "+idPeriodo);
		sbQuery.append(" WHERE 1=1 ");
		/*if (parameters.containsKey("codigoModular")) {
			sbQuery.append(" AND s.codigoModular = :codigoModular ");
		}*/
		
		if(liSituacion.size() > 0){
			sbQuery.append(" AND s.tipoSituacionSolicitud.idMaestro IN :situaList ");
		}
		
		Query query = em.createQuery(sbQuery.toString());

		if (liSituacion.size() > 0) {
			query.setParameter("situaList", liSituacion);
		}
		
		return query.executeUpdate();
			
		//return update(sbQuery.toString());
	}

	
	@SuppressWarnings({"unchecked" })
	public List<Solicitud> bandejaRevision(int index, int batchSize, String jpqlRules, String jpqlOrder) {
		StringBuilder sbQuery = new StringBuilder();
		List<Solicitud> sol;
		sbQuery.append(" SELECT new Solicitud(s.idSolicitud,s.dreUgel.codigo,"
				+ " s.fechaCreacion, s.tipoSolicitud.nombreMaestro ,s.nombrePrograma, "
				+ " s.tipoPrograma.nombreMaestro, s.codigoModular, s.tipoSituacionSolicitud.nombreMaestro, "
				+ " s.nombreUsuarioRevision, s.nombreUsuarioRevisionSig, s.usuarioRevision, s.usuarioRevisionSig, "
				+ " s.tipoSituacionRevisionSig.nombreMaestro, s.tipoSituacionRevision.nombreMaestro, s.indicadorUltimo, s.fechaModificacion) "
				+ " FROM Solicitud s ");
		sbQuery.append(" WHERE 1=1 ");
		if (StringUtil.isNotNullOrBlank(jpqlRules)) {
			sbQuery.append(" AND " + jpqlRules);
		}
		sbQuery.append(" GROUP BY s.idSolicitud ");
		
		if (StringUtil.isNotNullOrBlank(jpqlOrder)) {
			sbQuery.append(" ORDER BY " + jpqlOrder);
		}
		
		Query query = em.createQuery(sbQuery.toString(), Solicitud.class);
		sol = query.setMaxResults(batchSize).setFirstResult(index).getResultList();
		
		int count = synchronizedFind(sbQuery.toString()).size();
		
		sol.stream().forEach(p-> p.setCount(count));		
		
		return sol;
		
//		List<Solicitud> sol = query.getResultList();
//		return sol;
	}
	
	
	
	
	
	// @Override
	// public List<Object> findByFirstMaxResultJson(String jpql, String
	// jpqlRules , String jpqlTable, int batchSize, int index) {
	// List<Object> lista = new ArrayList<Object>();
	// StringBuilder sbQuery = new StringBuilder();
	// sbQuery.append(" select "+jpql + " from " + );
	// sbQuery.append(" WHERE 1=1 ");
	// sbQuery.append(jpqlRules);
	//
	// List<Solicitud> listaSolicitud = synchronizedFind(sbQuery.toString(),
	// batchSize, index, null);
	// for (Solicitud solicitud : listaSolicitud) {
	// Object objeto = (Object) solicitud;
	// lista.add(objeto);
	// }
	// return lista;
	// }
	//mejorar codigo y generalizarlo
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}
	//mejorar codigo y generalizarlo

	
}
