package pe.gob.minedu.escale.regprogramas.ejb.dao.impl;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.jpa.jta.AbstractJtaStatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.ejb.dao.PeriodoDAOLocal;
import pe.gob.minedu.escale.regprogramas.model.jpa.Periodo;


@Stateless
public class PeriodoDAOImpl extends AbstractJtaStatelessCRUDServices<Long,Periodo> implements PeriodoDAOLocal {

	@PersistenceContext(unitName = "EJBRegProgramaPUNIT")
	private EntityManager em;

	
	// InicioFactoria Hibernate //mejorar codigo y generalizarlo
	private Session sess;

	private SessionFactory sessionFactoria;

	public Session getSess() {
		return sess;
	}

	public void setSess(Session sess) {
		this.sess = sess;
	}

	public SessionFactory getSessionFactoria() {
		return sessionFactoria;
	}

	public void setSessionFactoria(SessionFactory sessionFactoria) {
		this.sessionFactoria = sessionFactoria;
	}
	
	public EntityManager getEntityManager() {
		return em;
	}

	// Fin Factoria Hibernate
	
	@Override
	public void save(Periodo entity) {
		// TODO Auto-generated method stub
		em.persist(entity);
	}

	@Override
	public Periodo registrarPeriodo(Periodo entity) {
		// TODO Auto-generated method stub
		em.persist(entity);
		em.flush();
		return entity;
	}

	@Override
	public void actualizarPerodo(Periodo entity) {
		// TODO Auto-generated method stub
		inciarSessionFactory();
		Periodo per = getSess().get(Periodo.class, entity.getIdPeriodo());
		try{
			ConversorHelper.fusionaPropiedades(entity, per );
			
		}catch(Exception e){
			throw new ConversorException();
		}
		
		getSess().update(per);
	}
	
	@SuppressWarnings("unchecked")
	public List<Periodo> buscarPeriodoxNombreItem(String nombrePeriodo, String item) {
		StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("select new Periodo(p.idPeriodo,p.namePeriodo, p.fechaInicioPeriodo, p.fechaFinPeriodo, p.fechaFinAnio, p.codGrupo , p.codItem , p.estado ) ");
	        sbQuery.append("FROM Periodo p ");
	        sbQuery.append("WHERE p.estado = :estado ");
	        sbQuery.append("AND p.namePeriodo = :nombrePeriodo ");
	        sbQuery.append("AND p.codItem = :item ");
	        
	        Query query = em.createQuery(sbQuery.toString(),Periodo.class);

	        query.setParameter("estado", EstadoState.ACTIVO.getValue());
	        query.setParameter("nombrePeriodo", nombrePeriodo);
	        query.setParameter("item", item);
	        
	        return query.getResultList();
		
	}

	@SuppressWarnings("unchecked")
	public List<Periodo> listPeriodo() {
		StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("select new Periodo(p.idPeriodo,p.namePeriodo, p.fechaInicioPeriodo, p.fechaFinPeriodo, p.fechaFinAnio, p.codGrupo , p.codItem , p.estado ) ");
	        sbQuery.append("FROM Periodo p ");
	        Query query = em.createQuery(sbQuery.toString(),Periodo.class);	        
	        return query.getResultList();
	}
	
	@Override
	public List<Periodo> listPeriodoActivo() {
		StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("select new Periodo(p.idPeriodo,p.namePeriodo, p.fechaInicioPeriodo, p.fechaFinPeriodo, p.fechaFinAnio, p.codGrupo , p.codItem , p.estado ) ");
	        sbQuery.append("FROM Periodo p ");
	        sbQuery.append("WHERE p.estado = :estado ");
	        
	        Query query = em.createQuery(sbQuery.toString(),Periodo.class);	 
	        
	        query.setParameter("estado", EstadoState.ACTIVO.getValue());
        return query.getResultList();
	}
	
	public void inciarSessionFactory() {
		EntityManager em = getEntityManager();
		setSess(em.unwrap(Session.class));
		setSessionFactoria(getSess().getSessionFactory());
	}

	@SuppressWarnings({"unchecked" })
	public List<Periodo> grillaPeriodo(int index, int batchSize, String jpqlRules, String jpqlOrder) {
		StringBuilder sbQuery = new StringBuilder();
		List<Periodo> per;
		sbQuery.append(" SELECT new Periodo(p.idPeriodo,p.namePeriodo, p.fechaInicioPeriodo, p.fechaFinPeriodo, p.fechaFinAnio, p.codGrupo , p.codItem , p.estado ) ");
	    sbQuery.append("FROM Periodo p ");
	    sbQuery.append(" ORDER BY p.estado  DESC , p.codItem DESC ");
		
		
		/*
	    sbQuery.append(" WHERE 1=1 ");
		if (StringUtil.isNotNullOrBlank(jpqlRules)) {
			sbQuery.append(" AND " + jpqlRules);
		}
		sbQuery.append(" GROUP BY p.idPeriodo ");
		
		if (StringUtil.isNotNullOrBlank(jpqlOrder)) {
			sbQuery.append(" ORDER BY " + jpqlOrder);
		}
		*/
		Query query = em.createQuery(sbQuery.toString(), Periodo.class);
		per = query.setMaxResults(batchSize).setFirstResult(index).getResultList();
		
		int count = synchronizedFind(sbQuery.toString()).size();
		
		per.stream().forEach(p-> p.setCount(count));		
		
		return per;
	}



	

}
