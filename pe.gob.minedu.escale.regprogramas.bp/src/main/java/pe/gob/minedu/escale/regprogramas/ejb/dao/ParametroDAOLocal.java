package pe.gob.minedu.escale.regprogramas.ejb.dao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.gob.minedu.escale.common.jpa.StatelessCRUDServices;
import pe.gob.minedu.escale.regprogramas.model.jpa.Parametro;

/**
 * @author IMENDOZA
 *
 */

@Local
public interface ParametroDAOLocal extends StatelessCRUDServices<Long, Parametro> {

	void save(Parametro entity);
	void update(Parametro entity);
	void delete(Parametro entity);
	Parametro registrar(Parametro entity);	
	List<Parametro> findByIdParametro(Long idParametro);	
	List<Parametro> findAll();
	List<Parametro> findByFirstMaxResult(int batchSize, int index);
	@SuppressWarnings("rawtypes")
	List<Parametro> findByFirstMaxResultParametros(int batchSize, int index, Map parameters);
}
