package pe.gob.minedu.escale.adm.model.ws.dto;

import java.io.Serializable;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class NewUsuarioCompareDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 787472789972534263L;
	   
	private Long idUsuario;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombreCompleto;
	private String email;
	private String emailInstitucional;
	private String celular;
	private String tipoDocumento;
	private String documento;
	private String codoid;
	private String estado;
	private Integer indclt;
	private String usuarioCreacion;
	private int indactmgr;
	private long idOrgan;
	private long idRol;
	private String nrocelins;
	private String nrotflins;
	private String nroanxins;
	private String cargo;
	private int indadm;
	private String fechaRegistro;
	private String fechaModif;
	private String archivo;
	private Integer situacion;
	private Integer tiposolicitud;
	private long idPerfil;
	private String indExisteUsuario;
	private String indModificacion;
	private String estadoOrganizacion;
	private long idOrgPrf;
	private String clave;
   
   
   
   
   
   
	public NewUsuarioCompareDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCodoid() {
		return codoid;
	}

	public void setCodoid(String codoid) {
		this.codoid = codoid;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}



	public String getNrocelins() {
		return nrocelins;
	}

	public void setNrocelins(String nrocelins) {
		this.nrocelins = nrocelins;
	}

	public String getEmailInstitucional() {
		return emailInstitucional;
	}

	public void setEmailInstitucional(String emailInstitucional) {
		this.emailInstitucional = emailInstitucional;
	}

	public String getNrotflins() {
		return nrotflins;
	}

	public void setNrotflins(String nrotflins) {
		this.nrotflins = nrotflins;
	}

	public String getNroanxins() {
		return nroanxins;
	}

	public void setNroanxins(String nroanxins) {
		this.nroanxins = nroanxins;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}



	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	public String getIndExisteUsuario() {
		return indExisteUsuario;
	}

	public void setIndExisteUsuario(String indExisteUsuario) {
		this.indExisteUsuario = indExisteUsuario;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getIndclt() {
		return indclt;
	}

	public void setIndclt(Integer indclt) {
		this.indclt = indclt;
	}

	public int getIndactmgr() {
		return indactmgr;
	}

	public void setIndactmgr(int indactmgr) {
		this.indactmgr = indactmgr;
	}

	public long getIdOrgan() {
		return idOrgan;
	}

	public void setIdOrgan(long idOrgan) {
		this.idOrgan = idOrgan;
	}

	public long getIdRol() {
		return idRol;
	}

	public void setIdRol(long idRol) {
		this.idRol = idRol;
	}

	public int getIndadm() {
		return indadm;
	}

	public void setIndadm(int indadm) {
		this.indadm = indadm;
	}

	public String getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(String fechaModif) {
		this.fechaModif = fechaModif;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public Integer getSituacion() {
		return situacion;
	}

	public void setSituacion(Integer situacion) {
		this.situacion = situacion;
	}

	public Integer getTiposolicitud() {
		return tiposolicitud;
	}

	public void setTiposolicitud(Integer tiposolicitud) {
		this.tiposolicitud = tiposolicitud;
	}

	public long getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(long idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getIndModificacion() {
		return indModificacion;
	}

	public void setIndModificacion(String indModificacion) {
		this.indModificacion = indModificacion;
	}

	public String getEstadoOrganizacion() {
		return estadoOrganizacion;
	}

	public void setEstadoOrganizacion(String estadoOrganizacion) {
		this.estadoOrganizacion = estadoOrganizacion;
	}

	public long getIdOrgPrf() {
		return idOrgPrf;
	}

	public void setIdOrgPrf(long idOrgPrf) {
		this.idOrgPrf = idOrgPrf;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellidoMaterno == null) ? 0 : apellidoMaterno.hashCode());
		result = prime * result + ((apellidoPaterno == null) ? 0 : apellidoPaterno.hashCode());
		result = prime * result + ((archivo == null) ? 0 : archivo.hashCode());
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result + ((celular == null) ? 0 : celular.hashCode());
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		result = prime * result + ((codoid == null) ? 0 : codoid.hashCode());
		result = prime * result + ((documento == null) ? 0 : documento.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((emailInstitucional == null) ? 0 : emailInstitucional.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((estadoOrganizacion == null) ? 0 : estadoOrganizacion.hashCode());
		result = prime * result + ((fechaModif == null) ? 0 : fechaModif.hashCode());
		result = prime * result + ((fechaRegistro == null) ? 0 : fechaRegistro.hashCode());
		result = prime * result + (int) (idOrgPrf ^ (idOrgPrf >>> 32));
		result = prime * result + (int) (idOrgan ^ (idOrgan >>> 32));
		result = prime * result + (int) (idPerfil ^ (idPerfil >>> 32));
		result = prime * result + (int) (idRol ^ (idRol >>> 32));
		result = prime * result + ((idUsuario == null) ? 0 : idUsuario.hashCode());
		result = prime * result + ((indExisteUsuario == null) ? 0 : indExisteUsuario.hashCode());
		result = prime * result + ((indModificacion == null) ? 0 : indModificacion.hashCode());
		result = prime * result + indactmgr;
		result = prime * result + indadm;
		result = prime * result + ((indclt == null) ? 0 : indclt.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((nombreCompleto == null) ? 0 : nombreCompleto.hashCode());
		result = prime * result + ((nroanxins == null) ? 0 : nroanxins.hashCode());
		result = prime * result + ((nrocelins == null) ? 0 : nrocelins.hashCode());
		result = prime * result + ((nrotflins == null) ? 0 : nrotflins.hashCode());
		result = prime * result + ((situacion == null) ? 0 : situacion.hashCode());
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		result = prime * result + ((tiposolicitud == null) ? 0 : tiposolicitud.hashCode());
		result = prime * result + ((usuarioCreacion == null) ? 0 : usuarioCreacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewUsuarioCompareDTO other = (NewUsuarioCompareDTO) obj;
		if (apellidoMaterno == null) {
			if (other.apellidoMaterno != null)
				return false;
		} else if (!apellidoMaterno.equals(other.apellidoMaterno))
			return false;
		if (apellidoPaterno == null) {
			if (other.apellidoPaterno != null)
				return false;
		} else if (!apellidoPaterno.equals(other.apellidoPaterno))
			return false;
		if (archivo == null) {
			if (other.archivo != null)
				return false;
		} else if (!archivo.equals(other.archivo))
			return false;
		if (cargo == null) {
			if (other.cargo != null)
				return false;
		} else if (!cargo.equals(other.cargo))
			return false;
		if (celular == null) {
			if (other.celular != null)
				return false;
		} else if (!celular.equals(other.celular))
			return false;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		if (codoid == null) {
			if (other.codoid != null)
				return false;
		} else if (!codoid.equals(other.codoid))
			return false;
		if (documento == null) {
			if (other.documento != null)
				return false;
		} else if (!documento.equals(other.documento))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (emailInstitucional == null) {
			if (other.emailInstitucional != null)
				return false;
		} else if (!emailInstitucional.equals(other.emailInstitucional))
			return false;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (estadoOrganizacion == null) {
			if (other.estadoOrganizacion != null)
				return false;
		} else if (!estadoOrganizacion.equals(other.estadoOrganizacion))
			return false;
		if (fechaModif == null) {
			if (other.fechaModif != null)
				return false;
		} else if (!fechaModif.equals(other.fechaModif))
			return false;
		if (fechaRegistro == null) {
			if (other.fechaRegistro != null)
				return false;
		} else if (!fechaRegistro.equals(other.fechaRegistro))
			return false;
		if (idOrgPrf != other.idOrgPrf)
			return false;
		if (idOrgan != other.idOrgan)
			return false;
		if (idPerfil != other.idPerfil)
			return false;
		if (idRol != other.idRol)
			return false;
		if (idUsuario == null) {
			if (other.idUsuario != null)
				return false;
		} else if (!idUsuario.equals(other.idUsuario))
			return false;
		if (indExisteUsuario == null) {
			if (other.indExisteUsuario != null)
				return false;
		} else if (!indExisteUsuario.equals(other.indExisteUsuario))
			return false;
		if (indModificacion == null) {
			if (other.indModificacion != null)
				return false;
		} else if (!indModificacion.equals(other.indModificacion))
			return false;
		if (indactmgr != other.indactmgr)
			return false;
		if (indadm != other.indadm)
			return false;
		if (indclt == null) {
			if (other.indclt != null)
				return false;
		} else if (!indclt.equals(other.indclt))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (nombreCompleto == null) {
			if (other.nombreCompleto != null)
				return false;
		} else if (!nombreCompleto.equals(other.nombreCompleto))
			return false;
		if (nroanxins == null) {
			if (other.nroanxins != null)
				return false;
		} else if (!nroanxins.equals(other.nroanxins))
			return false;
		if (nrocelins == null) {
			if (other.nrocelins != null)
				return false;
		} else if (!nrocelins.equals(other.nrocelins))
			return false;
		if (nrotflins == null) {
			if (other.nrotflins != null)
				return false;
		} else if (!nrotflins.equals(other.nrotflins))
			return false;
		if (situacion == null) {
			if (other.situacion != null)
				return false;
		} else if (!situacion.equals(other.situacion))
			return false;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null)
				return false;
		} else if (!tipoDocumento.equals(other.tipoDocumento))
			return false;
		if (tiposolicitud == null) {
			if (other.tiposolicitud != null)
				return false;
		} else if (!tiposolicitud.equals(other.tiposolicitud))
			return false;
		if (usuarioCreacion == null) {
			if (other.usuarioCreacion != null)
				return false;
		} else if (!usuarioCreacion.equals(other.usuarioCreacion))
			return false;
		return true;
	}
   
	
	

	
	
}
