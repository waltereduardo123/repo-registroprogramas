package pe.gob.minedu.escale.adm.model.dto;

import java.util.Date;



import pe.gob.minedu.escale.common.util.DtoUtil;

public class AccesoDTO extends DtoUtil {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long idSesion;
	private String navegador;
	private String usuario;
	private String token;
	private Date fecIniSesion;
	private Date fecFinSesion;
	private String hostcliente;
	private String usuarioCreacion;
	private Date fechaCreacion;
	private String estado;
	private String indAcc;
	
	private String keyUser;
	
	public AccesoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public String getKeyUser() {
		return keyUser;
	}


	public void setKeyUser(String keyUser) {
		this.keyUser = keyUser;
	}


	public Long getIdSesion() {
		return idSesion;
	}


	public void setIdSesion(Long idSesion) {
		this.idSesion = idSesion;
	}


	public String getNavegador() {
		return navegador;
	}


	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public Date getFecIniSesion() {
		return fecIniSesion;
	}


	public void setFecIniSesion(Date fecIniSesion) {
		this.fecIniSesion = fecIniSesion;
	}


	public Date getFecFinSesion() {
		return fecFinSesion;
	}


	public void setFecFinSesion(Date fecFinSesion) {
		this.fecFinSesion = fecFinSesion;
	}


	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}


	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}


	public Date getFechaCreacion() {
		return fechaCreacion;
	}


	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getHostcliente() {
		return hostcliente;
	}


	public void setHostcliente(String hostcliente) {
		this.hostcliente = hostcliente;
	}


	public String getIndAcc() {
		return indAcc;
	}


	public void setIndAcc(String indAcc) {
		this.indAcc = indAcc;
	}


	public AccesoDTO(Long idSesion, String navegador, String usuario, String token, Date fecIniSesion,
			Date fecFinSesion, String hostcliente, String usuarioCreacion, Date fechaCreacion, String estado,
			String indAcc, String keyUser) {
		super();
		this.idSesion = idSesion;
		this.navegador = navegador;
		this.usuario = usuario;
		this.token = token;
		this.fecIniSesion = fecIniSesion;
		this.fecFinSesion = fecFinSesion;
		this.hostcliente = hostcliente;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaCreacion = fechaCreacion;
		this.estado = estado;
		this.indAcc = indAcc;
		this.keyUser = keyUser;
	}






	
}
