package pe.gob.minedu.escale.adm.model.dto;

import java.util.Date;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class ClaveDTO extends DtoUtil {

	private Long idClave;
	private String codigo;
	private String clave;
	private Integer tiempoVigencia;
	private String usuCrea;
	private Date fecCrea;
	
	private String keyClave;
	
	
	public ClaveDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getKeyClave() {
		return keyClave;
	}


	public void setKeyClave(String keyClave) {
		this.keyClave = keyClave;
	}


	public Long getIdClave() {
		return idClave;
	}


	public void setIdClave(Long idClave) {
		this.idClave = idClave;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public String getUsuCrea() {
		return usuCrea;
	}


	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}


	public Date getFecCrea() {
		return fecCrea;
	}


	public void setFecCrea(Date fecCrea) {
		this.fecCrea = fecCrea;
	}


	public Integer getTiempoVigencia() {
		return tiempoVigencia;
	}


	public void setTiempoVigencia(Integer tiempoVigencia) {
		this.tiempoVigencia = tiempoVigencia;
	}
	
	
	
	
}
