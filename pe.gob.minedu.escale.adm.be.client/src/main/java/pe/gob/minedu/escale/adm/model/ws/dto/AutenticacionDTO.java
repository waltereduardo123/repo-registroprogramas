package pe.gob.minedu.escale.adm.model.ws.dto;

import java.io.Serializable;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class AutenticacionDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = -8194431045342536692L;

	private String idCodoId;

	private String ipHostRemoto;

	private String idEstado;

	private String pass;
	
	private String url;
	
	private String browse;
	
	private String token;
	
	private String perfil;

	

	


	public AutenticacionDTO(String idCodoId, String ipHostRemoto, String idEstado, String pass, String url,
			String browse, String token, String perfil) {
		super();
		this.idCodoId = idCodoId;
		this.ipHostRemoto = ipHostRemoto;
		this.idEstado = idEstado;
		this.pass = pass;
		this.url = url;
		this.browse = browse;
		this.token = token;
		this.perfil = perfil;
	}

	public AutenticacionDTO() {
		super();
	}

	public String getIdCodoId() {
		return idCodoId;
	}

	public void setIdCodoId(String idCodoId) {
		this.idCodoId = idCodoId;
	}

	public String getIpHostRemoto() {
		return ipHostRemoto;
	}

	public void setIpHostRemoto(String ipHostRemoto) {
		this.ipHostRemoto = ipHostRemoto;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBrowse() {
		return browse;
	}

	public void setBrowse(String browse) {
		this.browse = browse;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	

}
