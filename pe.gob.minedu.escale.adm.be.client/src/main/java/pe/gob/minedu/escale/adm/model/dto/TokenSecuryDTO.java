package pe.gob.minedu.escale.adm.model.dto;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class TokenSecuryDTO extends DtoUtil {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4301310524440537861L;

	private Long idToken;
	private String token;

	public TokenSecuryDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TokenSecuryDTO(Long idToken, String token) {
		super();
		this.idToken = idToken;
		this.token = token;
	}

	public Long getIdToken() {
		return idToken;
	}

	public void setIdToken(Long idToken) {
		this.idToken = idToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
