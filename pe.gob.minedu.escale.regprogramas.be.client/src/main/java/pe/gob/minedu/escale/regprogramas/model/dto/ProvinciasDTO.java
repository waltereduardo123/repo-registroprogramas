package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class ProvinciasDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idProvincia;

	private String provincia;

	private Short indVerna;

	private BigDecimal pointX;

	private BigDecimal pointY;

	private Short zoom;

	private RegionesDTO regionesDTO;

	private List<DistritosDTO> listaDistrito;

	public ProvinciasDTO() {
	}

	public ProvinciasDTO(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public Short getIndVerna() {
		return indVerna;
	}

	public void setIndVerna(Short indVerna) {
		this.indVerna = indVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	public RegionesDTO getRegiones() {
		return regionesDTO;
	}

	public void setRegiones(RegionesDTO regionesDTO) {
		this.regionesDTO = regionesDTO;
	}

	public List<DistritosDTO> getListaDistrito() {
		return listaDistrito;
	}

	public void setListaDistrito(List<DistritosDTO> listaDistrito) {
		this.listaDistrito = listaDistrito;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idProvincia != null ? idProvincia.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ProvinciasDTO)) {
			return false;
		}
		ProvinciasDTO other = (ProvinciasDTO) object;
		if ((this.idProvincia == null && other.idProvincia != null)
				|| (this.idProvincia != null && !this.idProvincia.equals(other.idProvincia))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.ProvinciasDTO[ idProvincia=" + idProvincia + " ]";
	}

}
