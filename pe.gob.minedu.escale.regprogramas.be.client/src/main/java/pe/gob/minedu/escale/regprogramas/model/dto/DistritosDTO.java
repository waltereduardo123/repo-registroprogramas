package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class DistritosDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idDistrito;

	private String distrito;

	private Short municip;

	private Short indVerna;

	private BigDecimal pointX;

	private BigDecimal pointY;

	private Short zoom;

	private List<DreGeoDTO> listaDreGeo;

	private ProvinciasDTO provincias;

	private List<SolicitudDTO> listaSolicitud;
	
	public DistritosDTO() {
		
	}

	public DistritosDTO(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public Short getMunicip() {
		return municip;
	}

	public void setMunicip(Short municip) {
		this.municip = municip;
	}

	public Short getIndVerna() {
		return indVerna;
	}

	public void setIndVerna(Short indVerna) {
		this.indVerna = indVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	public List<DreGeoDTO> getListaDreGeo() {
		return listaDreGeo;
	}

	public void setListaDreGeo(List<DreGeoDTO> listaDreGeo) {
		this.listaDreGeo = listaDreGeo;
	}	

	public ProvinciasDTO getProvincias() {
		return provincias;
	}


	public void setProvincias(ProvinciasDTO provincias) {
		this.provincias = provincias;
	}


	public List<SolicitudDTO> getListaSolicitud() {
		return listaSolicitud;
	}

	public void setListaSolicitud(List<SolicitudDTO> listaSolicitud) {
		this.listaSolicitud = listaSolicitud;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idDistrito != null ? idDistrito.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DistritosDTO)) {
			return false;
		}
		DistritosDTO other = (DistritosDTO) object;
		if ((this.idDistrito == null && other.idDistrito != null)
				|| (this.idDistrito != null && !this.idDistrito.equals(other.idDistrito))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.DistritosDTO[ idDistrito=" + idDistrito + " ]";
	}

}
