package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class AccionSolicitudDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idAccionSolicitud;

	private MaestroDTO tipoSituacionSolicitud;

	private String observacion;

	private String perfilRevisor;
	
	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaultimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private SolicitudDTO solicitud;

	public AccionSolicitudDTO() {
	}

	public AccionSolicitudDTO(Long idAccionSolicitud) {
		this.idAccionSolicitud = idAccionSolicitud;
	}

	public AccionSolicitudDTO(Long idAccionSolicitud, MaestroDTO nIdTipaccs, Date dFeccre, String cUsucre,
			String cEstado) {
		this.idAccionSolicitud = idAccionSolicitud;
		this.tipoSituacionSolicitud = nIdTipaccs;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
	}

	public AccionSolicitudDTO(Long idAccionSolicitud, MaestroDTO tipoSituacionSolicitud, String observacion,
			Date fechaCreacion, String usuarioCreacion, Date fechaultimaModificacion, String usuarioUltimaModificacion,
			String estado, SolicitudDTO solicitud) {
		super();
		this.idAccionSolicitud = idAccionSolicitud;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.observacion = observacion;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaultimaModificacion = fechaultimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.solicitud = solicitud;
	}

	public Long getIdAccionSolicitud() {
		return idAccionSolicitud;
	}

	public void setIdAccionSolicitud(Long idAccionSolicitud) {
		this.idAccionSolicitud = idAccionSolicitud;
	}

	public MaestroDTO getTipoSituacionSolicitud() {
		return tipoSituacionSolicitud;
	}

	public void setTipoSituacionSolicitud(MaestroDTO tipoSituacionSolicitud) {
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaultimaModificacion() {
		return fechaultimaModificacion;
	}

	public void setFechaultimaModificacion(Date fechaultimaModificacion) {
		this.fechaultimaModificacion = fechaultimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudDTO solicitudDTO) {
		this.solicitud = solicitudDTO;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}	

	public String getPerfilRevisor() {
		return perfilRevisor;
	}

	public void setPerfilRevisor(String perfilRevisor) {
		this.perfilRevisor = perfilRevisor;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idAccionSolicitud != null ? idAccionSolicitud.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AccionSolicitudDTO)) {
			return false;
		}
		AccionSolicitudDTO other = (AccionSolicitudDTO) object;
		if ((this.idAccionSolicitud == null && other.idAccionSolicitud != null)
				|| (this.idAccionSolicitud != null && !this.idAccionSolicitud.equals(other.idAccionSolicitud))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO[ idAccionSolicitud=" + idAccionSolicitud
				+ " ]";
	}

}
