package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class RegionesDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idRegion;

	private String region;

	private Short indVerna;

	private BigDecimal pointX;

	private BigDecimal pointY;

	private Short zoom;

	private List<ProveedorDepartamentoDTO> listaProveedorDepartamento;

	private List<ProvinciasDTO> listaProvincia;

	public RegionesDTO() {
	}

	public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Short getIndVerna() {
		return indVerna;
	}

	public void setIndVerna(Short indVerna) {
		this.indVerna = indVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	public List<ProvinciasDTO> getListaProvincia() {
		return listaProvincia;
	}

	public void setListaProvincia(List<ProvinciasDTO> listaProvincia) {
		this.listaProvincia = listaProvincia;
	}

	public List<ProveedorDepartamentoDTO> getListaProveedorDepartamento() {
		return listaProveedorDepartamento;
	}

	public void setListaProveedorDepartamento(List<ProveedorDepartamentoDTO> listaProveedorDepartamento) {
		this.listaProveedorDepartamento = listaProveedorDepartamento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idRegion != null ? idRegion.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof RegionesDTO)) {
			return false;
		}
		RegionesDTO other = (RegionesDTO) object;
		if ((this.idRegion == null && other.idRegion != null)
				|| (this.idRegion != null && !this.idRegion.equals(other.idRegion))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.RegionesDTO[ idRegion=" + idRegion + " ]";
	}

}
