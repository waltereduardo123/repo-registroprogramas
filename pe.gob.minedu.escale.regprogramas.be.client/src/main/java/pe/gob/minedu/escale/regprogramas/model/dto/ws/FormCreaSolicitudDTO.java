package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 * Template para el control de la seguridad en el formulario Creacion de la
 * Solicitud del perfil REGISTRADOR
 * 
 * @author WARODRIGUEZ
 *
 */
public class FormCreaSolicitudDTO extends DtoUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String x100;
	private String x101;
	private String x102;
	private String x103;
	private String x104;
	private String x105;
	private String x106;
	private String x107;
	private String x108;
	private String x109;
	private String x110;
	private String x111;
	private String x112;
	private String x113;
	private String x114;
	private String x115;
	private String x116;
	private String x117;
	private String x118;
	private String x119;
	private String x120;
	private String x121;
	private String x122;
	private String x123;
	private String x124;
	private String x125;
	private String x126;
	private String x127;
	private String x128;
	private String x129;
	private String x130;
	private String x131;
	private String x132;
	private String x133;
	private String x134;
	private String x135;
	private String x136;
	private String x137;
	private String x138;
	private String x139;
	private String x140;
	private String x141;
	private String x142;
	private String x143;
	private String x144;
	private String x145;
	private String usuarioCreacion;
	private String perfil;
	private String init;
	private String inexus;
	private String jodexus;

	
	
	public FormCreaSolicitudDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FormCreaSolicitudDTO(String x100, String x101, String x102, String x103, String x104, String x105,
			String x106, String x107, String x108, String x109, String x110, String x111, String x112, String x113,
			String x114, String x115, String x116, String x117, String x118, String x119, String x120, String x121,
			String x122, String x123, String x124, String x125, String x126, String x127, String x128, String x129,
			String x130, String x131, String x132, String x133, String x134, String x135, String x136, String x137,
			String x138, String x139, String x140, String x141, String x142, String x143, String x144, String x145,
			String usuarioCreacion, String perfil, String init, String inexus, String jodexus) {
		super();
		this.x100 = x100;
		this.x101 = x101;
		this.x102 = x102;
		this.x103 = x103;
		this.x104 = x104;
		this.x105 = x105;
		this.x106 = x106;
		this.x107 = x107;
		this.x108 = x108;
		this.x109 = x109;
		this.x110 = x110;
		this.x111 = x111;
		this.x112 = x112;
		this.x113 = x113;
		this.x114 = x114;
		this.x115 = x115;
		this.x116 = x116;
		this.x117 = x117;
		this.x118 = x118;
		this.x119 = x119;
		this.x120 = x120;
		this.x121 = x121;
		this.x122 = x122;
		this.x123 = x123;
		this.x124 = x124;
		this.x125 = x125;
		this.x126 = x126;
		this.x127 = x127;
		this.x128 = x128;
		this.x129 = x129;
		this.x130 = x130;
		this.x131 = x131;
		this.x132 = x132;
		this.x133 = x133;
		this.x134 = x134;
		this.x135 = x135;
		this.x136 = x136;
		this.x137 = x137;
		this.x138 = x138;
		this.x139 = x139;
		this.x140 = x140;
		this.x141 = x141;
		this.x142 = x142;
		this.x143 = x143;
		this.x144 = x144;
		this.x145 = x145;
		this.usuarioCreacion = usuarioCreacion;
		this.perfil = perfil;
		this.init = init;
		this.inexus = inexus;
		this.jodexus = jodexus;
	}

	public FormCreaSolicitudDTO(String x100, String x101, String x102, String x103, String x104, String x105,
			String x106, String x107, String x108, String x109, String x110, String x111, String x112, String x113,
			String x114, String x115, String x116, String x117, String x118, String x119, String x120, String x121,
			String x122, String x123, String x124, String x125, String x126, String x127, String x128, String x129,
			String x130, String x131, String x132, String x133, String x134, String x135, String x136, String x137,
			String x138, String x139, String x140, String x141, String x142, String x143, String x144, String x145,
			String usuarioCreacion, String perfil, String init) {
		super();
		this.x100 = x100;
		this.x101 = x101;
		this.x102 = x102;
		this.x103 = x103;
		this.x104 = x104;
		this.x105 = x105;
		this.x106 = x106;
		this.x107 = x107;
		this.x108 = x108;
		this.x109 = x109;
		this.x110 = x110;
		this.x111 = x111;
		this.x112 = x112;
		this.x113 = x113;
		this.x114 = x114;
		this.x115 = x115;
		this.x116 = x116;
		this.x117 = x117;
		this.x118 = x118;
		this.x119 = x119;
		this.x120 = x120;
		this.x121 = x121;
		this.x122 = x122;
		this.x123 = x123;
		this.x124 = x124;
		this.x125 = x125;
		this.x126 = x126;
		this.x127 = x127;
		this.x128 = x128;
		this.x129 = x129;
		this.x130 = x130;
		this.x131 = x131;
		this.x132 = x132;
		this.x133 = x133;
		this.x134 = x134;
		this.x135 = x135;
		this.x136 = x136;
		this.x137 = x137;
		this.x138 = x138;
		this.x139 = x139;
		this.x140 = x140;
		this.x141 = x141;
		this.x142 = x142;
		this.x143 = x143;
		this.x144 = x144;
		this.x145 = x145;
		this.usuarioCreacion = usuarioCreacion;
		this.perfil = perfil;
		this.init = init;
	}

	public String getX100() {
		return x100;
	}

	public void setX100(String x100) {
		this.x100 = x100;
	}

	public String getX101() {
		return x101;
	}

	public void setX101(String x101) {
		this.x101 = x101;
	}

	public String getX102() {
		return x102;
	}

	public void setX102(String x102) {
		this.x102 = x102;
	}

	public String getX103() {
		return x103;
	}

	public void setX103(String x103) {
		this.x103 = x103;
	}

	public String getX104() {
		return x104;
	}

	public void setX104(String x104) {
		this.x104 = x104;
	}

	public String getX105() {
		return x105;
	}

	public void setX105(String x105) {
		this.x105 = x105;
	}

	public String getX106() {
		return x106;
	}

	public void setX106(String x106) {
		this.x106 = x106;
	}

	public String getX107() {
		return x107;
	}

	public void setX107(String x107) {
		this.x107 = x107;
	}

	public String getX108() {
		return x108;
	}

	public void setX108(String x108) {
		this.x108 = x108;
	}

	public String getX109() {
		return x109;
	}

	public void setX109(String x109) {
		this.x109 = x109;
	}

	public String getX110() {
		return x110;
	}

	public void setX110(String x110) {
		this.x110 = x110;
	}

	public String getX111() {
		return x111;
	}

	public void setX111(String x111) {
		this.x111 = x111;
	}

	public String getX112() {
		return x112;
	}

	public void setX112(String x112) {
		this.x112 = x112;
	}

	public String getX113() {
		return x113;
	}

	public void setX113(String x113) {
		this.x113 = x113;
	}

	public String getX114() {
		return x114;
	}

	public void setX114(String x114) {
		this.x114 = x114;
	}

	public String getX115() {
		return x115;
	}

	public void setX115(String x115) {
		this.x115 = x115;
	}

	public String getX116() {
		return x116;
	}

	public void setX116(String x116) {
		this.x116 = x116;
	}

	public String getX117() {
		return x117;
	}

	public void setX117(String x117) {
		this.x117 = x117;
	}

	public String getX118() {
		return x118;
	}

	public void setX118(String x118) {
		this.x118 = x118;
	}

	public String getX119() {
		return x119;
	}

	public void setX119(String x119) {
		this.x119 = x119;
	}

	public String getX120() {
		return x120;
	}

	public void setX120(String x120) {
		this.x120 = x120;
	}

	public String getX121() {
		return x121;
	}

	public void setX121(String x121) {
		this.x121 = x121;
	}

	public String getX122() {
		return x122;
	}

	public void setX122(String x122) {
		this.x122 = x122;
	}

	public String getX123() {
		return x123;
	}

	public void setX123(String x123) {
		this.x123 = x123;
	}

	public String getX124() {
		return x124;
	}

	public void setX124(String x124) {
		this.x124 = x124;
	}

	public String getX125() {
		return x125;
	}

	public void setX125(String x125) {
		this.x125 = x125;
	}

	public String getX126() {
		return x126;
	}

	public void setX126(String x126) {
		this.x126 = x126;
	}

	public String getX127() {
		return x127;
	}

	public void setX127(String x127) {
		this.x127 = x127;
	}

	public String getX128() {
		return x128;
	}

	public void setX128(String x128) {
		this.x128 = x128;
	}

	public String getX129() {
		return x129;
	}

	public void setX129(String x129) {
		this.x129 = x129;
	}

	public String getX130() {
		return x130;
	}

	public void setX130(String x130) {
		this.x130 = x130;
	}

	public String getX131() {
		return x131;
	}

	public void setX131(String x131) {
		this.x131 = x131;
	}

	public String getX132() {
		return x132;
	}

	public void setX132(String x132) {
		this.x132 = x132;
	}

	public String getX133() {
		return x133;
	}

	public void setX133(String x133) {
		this.x133 = x133;
	}

	public String getX134() {
		return x134;
	}

	public void setX134(String x134) {
		this.x134 = x134;
	}

	public String getX135() {
		return x135;
	}

	public void setX135(String x135) {
		this.x135 = x135;
	}

	public String getX136() {
		return x136;
	}

	public void setX136(String x136) {
		this.x136 = x136;
	}

	public String getX137() {
		return x137;
	}

	public void setX137(String x137) {
		this.x137 = x137;
	}

	public String getX138() {
		return x138;
	}

	public void setX138(String x138) {
		this.x138 = x138;
	}

	public String getX139() {
		return x139;
	}

	public void setX139(String x139) {
		this.x139 = x139;
	}

	public String getX140() {
		return x140;
	}

	public void setX140(String x140) {
		this.x140 = x140;
	}

	public String getX141() {
		return x141;
	}

	public void setX141(String x141) {
		this.x141 = x141;
	}

	public String getX142() {
		return x142;
	}

	public void setX142(String x142) {
		this.x142 = x142;
	}

	public String getX143() {
		return x143;
	}

	public void setX143(String x143) {
		this.x143 = x143;
	}

	public String getX144() {
		return x144;
	}

	public void setX144(String x144) {
		this.x144 = x144;
	}

	public String getX145() {
		return x145;
	}

	public void setX145(String x145) {
		this.x145 = x145;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public String getInexus() {
		return inexus;
	}

	public void setInexus(String inexus) {
		this.inexus = inexus;
	}

	public String getJodexus() {
		return jodexus;
	}

	public void setJodexus(String jodexus) {
		this.jodexus = jodexus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inexus == null) ? 0 : inexus.hashCode());
		result = prime * result + ((init == null) ? 0 : init.hashCode());
		result = prime * result + ((jodexus == null) ? 0 : jodexus.hashCode());
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result + ((usuarioCreacion == null) ? 0 : usuarioCreacion.hashCode());
		result = prime * result + ((x100 == null) ? 0 : x100.hashCode());
		result = prime * result + ((x101 == null) ? 0 : x101.hashCode());
		result = prime * result + ((x102 == null) ? 0 : x102.hashCode());
		result = prime * result + ((x103 == null) ? 0 : x103.hashCode());
		result = prime * result + ((x104 == null) ? 0 : x104.hashCode());
		result = prime * result + ((x105 == null) ? 0 : x105.hashCode());
		result = prime * result + ((x106 == null) ? 0 : x106.hashCode());
		result = prime * result + ((x107 == null) ? 0 : x107.hashCode());
		result = prime * result + ((x108 == null) ? 0 : x108.hashCode());
		result = prime * result + ((x109 == null) ? 0 : x109.hashCode());
		result = prime * result + ((x110 == null) ? 0 : x110.hashCode());
		result = prime * result + ((x111 == null) ? 0 : x111.hashCode());
		result = prime * result + ((x112 == null) ? 0 : x112.hashCode());
		result = prime * result + ((x113 == null) ? 0 : x113.hashCode());
		result = prime * result + ((x114 == null) ? 0 : x114.hashCode());
		result = prime * result + ((x115 == null) ? 0 : x115.hashCode());
		result = prime * result + ((x116 == null) ? 0 : x116.hashCode());
		result = prime * result + ((x117 == null) ? 0 : x117.hashCode());
		result = prime * result + ((x118 == null) ? 0 : x118.hashCode());
		result = prime * result + ((x119 == null) ? 0 : x119.hashCode());
		result = prime * result + ((x120 == null) ? 0 : x120.hashCode());
		result = prime * result + ((x121 == null) ? 0 : x121.hashCode());
		result = prime * result + ((x122 == null) ? 0 : x122.hashCode());
		result = prime * result + ((x123 == null) ? 0 : x123.hashCode());
		result = prime * result + ((x124 == null) ? 0 : x124.hashCode());
		result = prime * result + ((x125 == null) ? 0 : x125.hashCode());
		result = prime * result + ((x126 == null) ? 0 : x126.hashCode());
		result = prime * result + ((x127 == null) ? 0 : x127.hashCode());
		result = prime * result + ((x128 == null) ? 0 : x128.hashCode());
		result = prime * result + ((x129 == null) ? 0 : x129.hashCode());
		result = prime * result + ((x130 == null) ? 0 : x130.hashCode());
		result = prime * result + ((x131 == null) ? 0 : x131.hashCode());
		result = prime * result + ((x132 == null) ? 0 : x132.hashCode());
		result = prime * result + ((x133 == null) ? 0 : x133.hashCode());
		result = prime * result + ((x134 == null) ? 0 : x134.hashCode());
		result = prime * result + ((x135 == null) ? 0 : x135.hashCode());
		result = prime * result + ((x136 == null) ? 0 : x136.hashCode());
		result = prime * result + ((x137 == null) ? 0 : x137.hashCode());
		result = prime * result + ((x138 == null) ? 0 : x138.hashCode());
		result = prime * result + ((x139 == null) ? 0 : x139.hashCode());
		result = prime * result + ((x140 == null) ? 0 : x140.hashCode());
		result = prime * result + ((x141 == null) ? 0 : x141.hashCode());
		result = prime * result + ((x142 == null) ? 0 : x142.hashCode());
		result = prime * result + ((x143 == null) ? 0 : x143.hashCode());
		result = prime * result + ((x144 == null) ? 0 : x144.hashCode());
		result = prime * result + ((x145 == null) ? 0 : x145.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormCreaSolicitudDTO other = (FormCreaSolicitudDTO) obj;
		if (inexus == null) {
			if (other.inexus != null)
				return false;
		} else if (!inexus.equals(other.inexus))
			return false;
		if (init == null) {
			if (other.init != null)
				return false;
		} else if (!init.equals(other.init))
			return false;
		if (jodexus == null) {
			if (other.jodexus != null)
				return false;
		} else if (!jodexus.equals(other.jodexus))
			return false;
		if (perfil == null) {
			if (other.perfil != null)
				return false;
		} else if (!perfil.equals(other.perfil))
			return false;
		if (usuarioCreacion == null) {
			if (other.usuarioCreacion != null)
				return false;
		} else if (!usuarioCreacion.equals(other.usuarioCreacion))
			return false;
		if (x100 == null) {
			if (other.x100 != null)
				return false;
		} else if (!x100.equals(other.x100))
			return false;
		if (x101 == null) {
			if (other.x101 != null)
				return false;
		} else if (!x101.equals(other.x101))
			return false;
		if (x102 == null) {
			if (other.x102 != null)
				return false;
		} else if (!x102.equals(other.x102))
			return false;
		if (x103 == null) {
			if (other.x103 != null)
				return false;
		} else if (!x103.equals(other.x103))
			return false;
		if (x104 == null) {
			if (other.x104 != null)
				return false;
		} else if (!x104.equals(other.x104))
			return false;
		if (x105 == null) {
			if (other.x105 != null)
				return false;
		} else if (!x105.equals(other.x105))
			return false;
		if (x106 == null) {
			if (other.x106 != null)
				return false;
		} else if (!x106.equals(other.x106))
			return false;
		if (x107 == null) {
			if (other.x107 != null)
				return false;
		} else if (!x107.equals(other.x107))
			return false;
		if (x108 == null) {
			if (other.x108 != null)
				return false;
		} else if (!x108.equals(other.x108))
			return false;
		if (x109 == null) {
			if (other.x109 != null)
				return false;
		} else if (!x109.equals(other.x109))
			return false;
		if (x110 == null) {
			if (other.x110 != null)
				return false;
		} else if (!x110.equals(other.x110))
			return false;
		if (x111 == null) {
			if (other.x111 != null)
				return false;
		} else if (!x111.equals(other.x111))
			return false;
		if (x112 == null) {
			if (other.x112 != null)
				return false;
		} else if (!x112.equals(other.x112))
			return false;
		if (x113 == null) {
			if (other.x113 != null)
				return false;
		} else if (!x113.equals(other.x113))
			return false;
		if (x114 == null) {
			if (other.x114 != null)
				return false;
		} else if (!x114.equals(other.x114))
			return false;
		if (x115 == null) {
			if (other.x115 != null)
				return false;
		} else if (!x115.equals(other.x115))
			return false;
		if (x116 == null) {
			if (other.x116 != null)
				return false;
		} else if (!x116.equals(other.x116))
			return false;
		if (x117 == null) {
			if (other.x117 != null)
				return false;
		} else if (!x117.equals(other.x117))
			return false;
		if (x118 == null) {
			if (other.x118 != null)
				return false;
		} else if (!x118.equals(other.x118))
			return false;
		if (x119 == null) {
			if (other.x119 != null)
				return false;
		} else if (!x119.equals(other.x119))
			return false;
		if (x120 == null) {
			if (other.x120 != null)
				return false;
		} else if (!x120.equals(other.x120))
			return false;
		if (x121 == null) {
			if (other.x121 != null)
				return false;
		} else if (!x121.equals(other.x121))
			return false;
		if (x122 == null) {
			if (other.x122 != null)
				return false;
		} else if (!x122.equals(other.x122))
			return false;
		if (x123 == null) {
			if (other.x123 != null)
				return false;
		} else if (!x123.equals(other.x123))
			return false;
		if (x124 == null) {
			if (other.x124 != null)
				return false;
		} else if (!x124.equals(other.x124))
			return false;
		if (x125 == null) {
			if (other.x125 != null)
				return false;
		} else if (!x125.equals(other.x125))
			return false;
		if (x126 == null) {
			if (other.x126 != null)
				return false;
		} else if (!x126.equals(other.x126))
			return false;
		if (x127 == null) {
			if (other.x127 != null)
				return false;
		} else if (!x127.equals(other.x127))
			return false;
		if (x128 == null) {
			if (other.x128 != null)
				return false;
		} else if (!x128.equals(other.x128))
			return false;
		if (x129 == null) {
			if (other.x129 != null)
				return false;
		} else if (!x129.equals(other.x129))
			return false;
		if (x130 == null) {
			if (other.x130 != null)
				return false;
		} else if (!x130.equals(other.x130))
			return false;
		if (x131 == null) {
			if (other.x131 != null)
				return false;
		} else if (!x131.equals(other.x131))
			return false;
		if (x132 == null) {
			if (other.x132 != null)
				return false;
		} else if (!x132.equals(other.x132))
			return false;
		if (x133 == null) {
			if (other.x133 != null)
				return false;
		} else if (!x133.equals(other.x133))
			return false;
		if (x134 == null) {
			if (other.x134 != null)
				return false;
		} else if (!x134.equals(other.x134))
			return false;
		if (x135 == null) {
			if (other.x135 != null)
				return false;
		} else if (!x135.equals(other.x135))
			return false;
		if (x136 == null) {
			if (other.x136 != null)
				return false;
		} else if (!x136.equals(other.x136))
			return false;
		if (x137 == null) {
			if (other.x137 != null)
				return false;
		} else if (!x137.equals(other.x137))
			return false;
		if (x138 == null) {
			if (other.x138 != null)
				return false;
		} else if (!x138.equals(other.x138))
			return false;
		if (x139 == null) {
			if (other.x139 != null)
				return false;
		} else if (!x139.equals(other.x139))
			return false;
		if (x140 == null) {
			if (other.x140 != null)
				return false;
		} else if (!x140.equals(other.x140))
			return false;
		if (x141 == null) {
			if (other.x141 != null)
				return false;
		} else if (!x141.equals(other.x141))
			return false;
		if (x142 == null) {
			if (other.x142 != null)
				return false;
		} else if (!x142.equals(other.x142))
			return false;
		if (x143 == null) {
			if (other.x143 != null)
				return false;
		} else if (!x143.equals(other.x143))
			return false;
		if (x144 == null) {
			if (other.x144 != null)
				return false;
		} else if (!x144.equals(other.x144))
			return false;
		if (x145 == null) {
			if (other.x145 != null)
				return false;
		} else if (!x145.equals(other.x145))
			return false;
		return true;
	}

}