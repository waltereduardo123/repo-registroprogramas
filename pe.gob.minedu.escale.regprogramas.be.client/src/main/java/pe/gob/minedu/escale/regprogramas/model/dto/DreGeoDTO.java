package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class DreGeoDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	protected DreGeoPKDTO dreGeoPKDTO;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private DistritosDTO distritosDTO;

	private DreUgelDTO dreUgelDTO;

	public DreGeoDTO() {
	}

	public DreGeoDTO(DreGeoPKDTO dreGeoPKDTO) {
		this.dreGeoPKDTO = dreGeoPKDTO;
	}

	public DreGeoDTO(DreGeoPKDTO dreGeoPKDTO, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado, DistritosDTO distritosDTO, DreUgelDTO dreUgelDTO) {
		super();
		this.dreGeoPKDTO = dreGeoPKDTO;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.distritosDTO = distritosDTO;
		this.dreUgelDTO = dreUgelDTO;
	}

	public DreGeoPKDTO getDreGeoPK() {
		return dreGeoPKDTO;
	}

	public void setDreGeoPK(DreGeoPKDTO dreGeoPKDTO) {
		this.dreGeoPKDTO = dreGeoPKDTO;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public DistritosDTO getDistritos() {
		return distritosDTO;
	}

	public void setDistritos(DistritosDTO distritosDTO) {
		this.distritosDTO = distritosDTO;
	}

	public DreUgelDTO getDreUgel() {
		return dreUgelDTO;
	}

	public void setDreUgel(DreUgelDTO dreUgelDTO) {
		this.dreUgelDTO = dreUgelDTO;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (dreGeoPKDTO != null ? dreGeoPKDTO.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DreGeoDTO)) {
			return false;
		}
		DreGeoDTO other = (DreGeoDTO) object;
		if ((this.dreGeoPKDTO == null && other.dreGeoPKDTO != null)
				|| (this.dreGeoPKDTO != null && !this.dreGeoPKDTO.equals(other.dreGeoPKDTO))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.DreGeoDTO[ dreGeoPK=" + dreGeoPKDTO + " ]";
	}

}
