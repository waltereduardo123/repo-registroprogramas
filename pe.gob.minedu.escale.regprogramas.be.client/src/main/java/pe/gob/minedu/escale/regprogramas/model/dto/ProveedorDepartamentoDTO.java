package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class ProveedorDepartamentoDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	protected ProveedorDepartamentoPKDTO proveedorDepartamentoPKDTO;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private RegionesDTO regionesDTO;

	private MaestroDTO maestroDTO;

	public ProveedorDepartamentoPKDTO getProveedorDepartamentoPK() {
		return proveedorDepartamentoPKDTO;
	}

	public void setProveedorDepartamentoPK(ProveedorDepartamentoPKDTO proveedorDepartamentoPKDTO) {
		this.proveedorDepartamentoPKDTO = proveedorDepartamentoPKDTO;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public RegionesDTO getRegiones() {
		return regionesDTO;
	}

	public void setRegiones(RegionesDTO regionesDTO) {
		this.regionesDTO = regionesDTO;
	}

	public MaestroDTO getMaestro() {
		return maestroDTO;
	}

	public void setMaestro(MaestroDTO maestroDTO) {
		this.maestroDTO = maestroDTO;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (proveedorDepartamentoPKDTO != null ? proveedorDepartamentoPKDTO.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ProveedorDepartamentoDTO)) {
			return false;
		}
		ProveedorDepartamentoDTO other = (ProveedorDepartamentoDTO) object;
		if ((this.proveedorDepartamentoPKDTO == null && other.proveedorDepartamentoPKDTO != null)
				|| (this.proveedorDepartamentoPKDTO != null
						&& !this.proveedorDepartamentoPKDTO.equals(other.proveedorDepartamentoPKDTO))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.ProveedorDepartamentoDTO[ proveedorDepartamentoPK="
				+ proveedorDepartamentoPKDTO + " ]";
	}

}
