package pe.gob.minedu.escale.regprogramas.model.dto;

public class SincronizarProgramaDTO {

	private String idProgramaSincronizar;
	private String codModular;
	private String rows;
	private String codcp;
	private String borrado;
	private String area;
	private String areaSig;
	private String codcpReemp;
	private String areaReemp;
	private String areaSigReemp;
	
	
	public SincronizarProgramaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SincronizarProgramaDTO(String codcp) {
		super();
		this.codcp = codcp;
	}

	public SincronizarProgramaDTO(String rows, String codcp, String borrado, String area, String areaSig,
			String codcpReemp, String areaReemp, String areaSigReemp) {
		super();
		this.rows = rows;
		this.codcp = codcp;
		this.borrado = borrado;
		this.area = area;
		this.areaSig = areaSig;
		this.codcpReemp = codcpReemp;
		this.areaReemp = areaReemp;
		this.areaSigReemp = areaSigReemp;
	}
	public String getRows() {
		return rows;
	}
	public void setRows(String rows) {
		this.rows = rows;
	}
	public String getCodcp() {
		return codcp;
	}
	public void setCodcp(String codcp) {
		this.codcp = codcp;
	}
	public String getBorrado() {
		return borrado;
	}
	public void setBorrado(String borrado) {
		this.borrado = borrado;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getAreaSig() {
		return areaSig;
	}
	public void setAreaSig(String areaSig) {
		this.areaSig = areaSig;
	}
	public String getCodcpReemp() {
		return codcpReemp;
	}
	public void setCodcpReemp(String codcpReemp) {
		this.codcpReemp = codcpReemp;
	}
	public String getAreaReemp() {
		return areaReemp;
	}
	public void setAreaReemp(String areaReemp) {
		this.areaReemp = areaReemp;
	}
	public String getAreaSigReemp() {
		return areaSigReemp;
	}
	public void setAreaSigReemp(String areaSigReemp) {
		this.areaSigReemp = areaSigReemp;
	}
	public String getIdProgramaSincronizar() {
		return idProgramaSincronizar;
	}
	public void setIdProgramaSincronizar(String idProgramaSincronizar) {
		this.idProgramaSincronizar = idProgramaSincronizar;
	}
	public String getCodModular() {
		return codModular;
	}
	public void setCodModular(String codModular) {
		this.codModular = codModular;
	}	
	
	
}
