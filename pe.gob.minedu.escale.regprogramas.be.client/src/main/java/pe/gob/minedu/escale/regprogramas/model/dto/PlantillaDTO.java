package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class PlantillaDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idPlantilla;

	private String nombreplantilla;

	private MaestroDTO tipoPlantilla;

	private String descripcionPlantilla;

	private String codigoArchivo;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	public PlantillaDTO() {
	}

	public PlantillaDTO(Long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public PlantillaDTO(Long idPlantilla, String nombreplantilla, MaestroDTO tipoPlantilla, String descripcionPlantilla,
			String codigoArchivo, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado) {
		super();
		this.idPlantilla = idPlantilla;
		this.nombreplantilla = nombreplantilla;
		this.tipoPlantilla = tipoPlantilla;
		this.descripcionPlantilla = descripcionPlantilla;
		this.codigoArchivo = codigoArchivo;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
	}

	public Long getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(Long idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public String getNombreplantilla() {
		return nombreplantilla;
	}

	public void setNombreplantilla(String nombreplantilla) {
		this.nombreplantilla = nombreplantilla;
	}

	public MaestroDTO getTipoPlantilla() {
		return tipoPlantilla;
	}

	public void setTipoPlantilla(MaestroDTO tipoPlantilla) {
		this.tipoPlantilla = tipoPlantilla;
	}

	public String getDescripcionPlantilla() {
		return descripcionPlantilla;
	}

	public void setDescripcionPlantilla(String descripcionPlantilla) {
		this.descripcionPlantilla = descripcionPlantilla;
	}

	public String getCodigoArchivo() {
		return codigoArchivo;
	}

	public void setCodigoArchivo(String codigoArchivo) {
		this.codigoArchivo = codigoArchivo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idPlantilla != null ? idPlantilla.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof PlantillaDTO)) {
			return false;
		}
		PlantillaDTO other = (PlantillaDTO) object;
		if ((this.idPlantilla == null && other.idPlantilla != null)
				|| (this.idPlantilla != null && !this.idPlantilla.equals(other.idPlantilla))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.PlantillaDTO[ idPlantilla=" + idPlantilla + " ]";
	}

}
