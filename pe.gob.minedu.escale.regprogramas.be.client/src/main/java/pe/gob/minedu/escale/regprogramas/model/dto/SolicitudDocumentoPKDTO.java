package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class SolicitudDocumentoPKDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idDocumento;

	private long idSolicitud;

	public SolicitudDocumentoPKDTO() {
	}

	public SolicitudDocumentoPKDTO(long idDocumento, long idSolicitud) {
		this.idDocumento = idDocumento;
		this.idSolicitud = idSolicitud;
	}

	public long getidDocumento() {
		return idDocumento;
	}

	public void setidDocumento(long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public long getidSolicitud() {
		return idSolicitud;
	}

	public void setidSolicitud(long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (int) idDocumento;
		hash += (int) idSolicitud;
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SolicitudDocumentoPKDTO)) {
			return false;
		}
		SolicitudDocumentoPKDTO other = (SolicitudDocumentoPKDTO) object;
		if (this.idDocumento != other.idDocumento) {
			return false;
		}
		if (this.idSolicitud != other.idSolicitud) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoPKDTO[ idDocumento=" + idDocumento
				+ ", idSolicitud=" + idSolicitud + " ]";
	}

}
