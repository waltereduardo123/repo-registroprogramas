package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class DocumentoDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idDocumento;

	private MaestroDTO tipoDocumento;

	private MaestroDTO tipoDocumentoResolucion;

	private String nroDocumento;

	private Date fechaDocumento;

	private String codigoDocumento;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private String codooii;

	private List<SolicitudDocumentoDTO> listaSolicitudDocumento;

	private List<ArchivoDTO> listaArchivo;

	private String indCambioArchivos;

	private String indCambioArchivoCroquis;

	private String indCambioDocumentoExistente;

	public DocumentoDTO() {
	}

	public DocumentoDTO(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public DocumentoDTO(Long idDocumento, Date fechaCreacion, String usuarioCreacion, String estado, String codoii,
			String codigoDocumento) {
		this.idDocumento = idDocumento;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		this.codooii = codoii;
		this.codigoDocumento = codigoDocumento;
	}

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public MaestroDTO getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(MaestroDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodooii() {
		return codooii;
	}

	public void setCodooii(String codooii) {
		this.codooii = codooii;
	}

	public MaestroDTO getTipoDocumentoResolucion() {
		return tipoDocumentoResolucion;
	}

	public void setTipoDocumentoResolucion(MaestroDTO tipoDocumentoResolucion) {
		this.tipoDocumentoResolucion = tipoDocumentoResolucion;
	}

	public List<SolicitudDocumentoDTO> getListaSolicitudDocumento() {
		return listaSolicitudDocumento;
	}

	public void setListaSolicitudDocumento(List<SolicitudDocumentoDTO> listaSolicitudDocumento) {
		this.listaSolicitudDocumento = listaSolicitudDocumento;
	}

	public List<ArchivoDTO> getListaArchivo() {
		return listaArchivo;
	}

	public void setListaArchivo(List<ArchivoDTO> listaArchivo) {
		this.listaArchivo = listaArchivo;
	}

	public String getIndCambioArchivos() {
		return indCambioArchivos;
	}

	public void setIndCambioArchivos(String indCambioArchivos) {
		this.indCambioArchivos = indCambioArchivos;
	}

	public String getIndCambioArchivoCroquis() {
		return indCambioArchivoCroquis;
	}

	public void setIndCambioArchivoCroquis(String indCambioArchivoCroquis) {
		this.indCambioArchivoCroquis = indCambioArchivoCroquis;
	}

	public String getIndCambioDocumentoExistente() {
		return indCambioDocumentoExistente;
	}

	public void setIndCambioDocumentoExistente(String indCambioDocumentoExistente) {
		this.indCambioDocumentoExistente = indCambioDocumentoExistente;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idDocumento != null ? idDocumento.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DocumentoDTO)) {
			return false;
		}
		DocumentoDTO other = (DocumentoDTO) object;
		if ((this.idDocumento == null && other.idDocumento != null)
				|| (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO[ idDocumento=" + idDocumento + " ]";
	}

}
