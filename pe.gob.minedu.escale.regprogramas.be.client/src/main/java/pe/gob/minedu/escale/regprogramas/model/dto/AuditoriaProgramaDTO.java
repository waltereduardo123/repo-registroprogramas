package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class AuditoriaProgramaDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idPrograma;

	private String codigoModular;

	private MaestroDTO tipoPrograma;

	private String nombrePrograma;

	private BigDecimal latitudPrograma;

	private BigDecimal longitudPrograma;

	private MaestroDTO tipoGestion;

	private MaestroDTO tipoDependencia;

	private MaestroDTO tipoGestionEducativa;

	private DreUgelDTO dreUgel;

	private MaestroDTO tipoTurno;

	private MaestroDTO tipoContinuidadJornadaEscolar;

	private MaestroDTO tipoVia;

	private String nombreVia;

	private String numeroVia;

	private String manzana;

	private String lote;

	private MaestroDTO tipoLocalidad;

	private String localidad;

	private String etapa;

	private String sector;

	private String zona;

	private String otraDireccion;

	private String referenciaDireccion;

	private DistritosDTO distrito;

	private String codigoCentroPoblado;

	private String nombreCentroPoblado;

	private BigDecimal latitudCentroPoblado;

	private BigDecimal longitudCentroPoblado;

	private String codigoServicioEduMasCercano;

	private String nombreServicioEduMasCercano;

	private BigDecimal latitudServicioEduMasCercano;

	private BigDecimal longitudServicioEduMasCercano;

	private MaestroDTO tipoProveedorAgua;

	private String otroProveedorAgua;

	private String suministroAgua;

	private MaestroDTO tipoProveedorEnergia;

	private String otroProveedorEnergia;

	private String suministroEnergia;

	private String area;

	private String codigoAreaSig;

	private Long zoom;

	private String marcoCensal;

	private String marcocd;

	private MaestroDTO tipoSituacion;

	private Date fechaCreacionPrograma;

	private Date fechaCierrePrograma;

	private Date fechaRenovacionPrograma;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private MaestroDTO tipoEstado;

	private String geoHash;

	private MaestroDTO tipoResolucion;

	private String numeroDocumento;

	private Date fechaDocumento;

	private String codigoDocumento;

	public AuditoriaProgramaDTO() {
	}

	public AuditoriaProgramaDTO(Long idPrograma) {
		this.idPrograma = idPrograma;
	}

	public AuditoriaProgramaDTO(Long idPrograma, String codigoModular, MaestroDTO tipoPrograma, String nombrePrograma,
			BigDecimal latitudPrograma, BigDecimal longitudPrograma, MaestroDTO tipoGestion, MaestroDTO tipoDependencia,
			MaestroDTO tipoGestionEducativa, DreUgelDTO dreUgel, MaestroDTO tipoTurno,
			MaestroDTO tipoContinuidadJornadaEscolar, MaestroDTO tipoVia, String nombreVia, String numeroVia,
			String manzana, String lote, MaestroDTO tipoLocalidad, String localidad, String etapa, String sector,
			String zona, String otraDireccion, String referenciaDireccion, DistritosDTO distrito,
			String codigoCentroPoblado, String nombreCentroPoblado, BigDecimal latitudCentroPoblado,
			BigDecimal longitudCentroPoblado, String codigoServicioEduMasCercano, String nombreServicioEduMasCercano,
			BigDecimal latitudServicioEduMasCercano, BigDecimal longitudServicioEduMasCercano,
			MaestroDTO tipoProveedorAgua, String otroProveedorAgua, String suministroAgua,
			MaestroDTO tipoProveedorEnergia, String otroProveedorEnergia, String suministroEnergia, String area,
			String codigoAreaSig, Long zoom, String marcoCensal, String marcocd, MaestroDTO tipoSituacion,
			Date fechaCreacionPrograma, Date fechaCierrePrograma, Date fechaRenovacionPrograma, Date fechaCreacion,
			String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado,
			MaestroDTO tipoEstado, String geoHash, MaestroDTO tipoResolucion, String numeroDocumento,
			Date fechaDocumento, String codigoDocumento) {
		super();
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.latitudPrograma = latitudPrograma;
		this.longitudPrograma = longitudPrograma;
		this.tipoGestion = tipoGestion;
		this.tipoDependencia = tipoDependencia;
		this.tipoGestionEducativa = tipoGestionEducativa;
		this.dreUgel = dreUgel;
		this.tipoTurno = tipoTurno;
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
		this.tipoVia = tipoVia;
		this.nombreVia = nombreVia;
		this.numeroVia = numeroVia;
		this.manzana = manzana;
		this.lote = lote;
		this.tipoLocalidad = tipoLocalidad;
		this.localidad = localidad;
		this.etapa = etapa;
		this.sector = sector;
		this.zona = zona;
		this.otraDireccion = otraDireccion;
		this.referenciaDireccion = referenciaDireccion;
		this.distrito = distrito;
		this.codigoCentroPoblado = codigoCentroPoblado;
		this.nombreCentroPoblado = nombreCentroPoblado;
		this.latitudCentroPoblado = latitudCentroPoblado;
		this.longitudCentroPoblado = longitudCentroPoblado;
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
		this.tipoProveedorAgua = tipoProveedorAgua;
		this.otroProveedorAgua = otroProveedorAgua;
		this.suministroAgua = suministroAgua;
		this.tipoProveedorEnergia = tipoProveedorEnergia;
		this.otroProveedorEnergia = otroProveedorEnergia;
		this.suministroEnergia = suministroEnergia;
		this.area = area;
		this.codigoAreaSig = codigoAreaSig;
		this.zoom = zoom;
		this.marcoCensal = marcoCensal;
		this.marcocd = marcocd;
		this.tipoSituacion = tipoSituacion;
		this.fechaCreacionPrograma = fechaCreacionPrograma;
		this.fechaCierrePrograma = fechaCierrePrograma;
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.tipoEstado = tipoEstado;
		this.geoHash = geoHash;
		this.tipoResolucion = tipoResolucion;
		this.numeroDocumento = numeroDocumento;
		this.fechaDocumento = fechaDocumento;
		this.codigoDocumento = codigoDocumento;
	}

	public AuditoriaProgramaDTO(Long idPrograma, String codigoModular, MaestroDTO tipoPrograma, String nombrePrograma,
			BigDecimal latitudPrograma, BigDecimal longitudPrograma, MaestroDTO tipoGestion, MaestroDTO tipoDependencia,
			MaestroDTO tipoGestionEducativa, MaestroDTO tipoTurno, MaestroDTO tipoContinuidadJornadaEscolar,
			String marcoCensal, Date fechaCreacion, String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.latitudPrograma = latitudPrograma;
		this.longitudPrograma = longitudPrograma;
		this.tipoGestion = tipoGestion;
		this.tipoDependencia = tipoDependencia;
		this.tipoGestionEducativa = tipoGestionEducativa;
		this.tipoTurno = tipoTurno;
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
		this.marcoCensal = marcoCensal;
		this.fechaCreacion = fechaCreacion;
		this.estado = estado;
	}

	public AuditoriaProgramaDTO(Long idPrograma, String codigoModular, String nombrePrograma, String usuarioCreacion,
			String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public AuditoriaProgramaDTO(Long idPrograma, String codigoModular, String nombrePrograma,
			Date fechaCreacionPrograma, Date fechaCierrePrograma, Date fechaRenovacionPrograma, String estado) {
		this.idPrograma = idPrograma;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.fechaCreacionPrograma = fechaCreacionPrograma;
		this.fechaCierrePrograma = fechaCierrePrograma;
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
		this.estado = estado;
	}

	public Long getIdPrograma() {
		return idPrograma;
	}

	public void setIdPrograma(Long idPrograma) {
		this.idPrograma = idPrograma;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public BigDecimal getLatitudPrograma() {
		return latitudPrograma;
	}

	public void setLatitudPrograma(BigDecimal latitudPrograma) {
		this.latitudPrograma = latitudPrograma;
	}

	public BigDecimal getLongitudPrograma() {
		return longitudPrograma;
	}

	public void setLongitudPrograma(BigDecimal longitudPrograma) {
		this.longitudPrograma = longitudPrograma;
	}

	public DreUgelDTO getDreUgel() {
		return dreUgel;
	}

	public void setDreUgel(DreUgelDTO dreUgel) {
		this.dreUgel = dreUgel;
	}

	public String getNombreVia() {
		return nombreVia;
	}

	public void setNombreVia(String nombreVia) {
		this.nombreVia = nombreVia;
	}

	public String getNumeroVia() {
		return numeroVia;
	}

	public void setNumeroVia(String numeroVia) {
		this.numeroVia = numeroVia;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getOtraDireccion() {
		return otraDireccion;
	}

	public void setOtraDireccion(String otraDireccion) {
		this.otraDireccion = otraDireccion;
	}

	public String getReferenciaDireccion() {
		return referenciaDireccion;
	}

	public void setReferenciaDireccion(String referenciaDireccion) {
		this.referenciaDireccion = referenciaDireccion;
	}

	public DistritosDTO getDistrito() {
		return distrito;
	}

	public void setDistrito(DistritosDTO distrito) {
		this.distrito = distrito;
	}

	public String getCodigoCentroPoblado() {
		return codigoCentroPoblado;
	}

	public void setCodigoCentroPoblado(String codigoCentroPoblado) {
		this.codigoCentroPoblado = codigoCentroPoblado;
	}

	public String getNombreCentroPoblado() {
		return nombreCentroPoblado;
	}

	public void setNombreCentroPoblado(String nombreCentroPoblado) {
		this.nombreCentroPoblado = nombreCentroPoblado;
	}

	public String getCodigoServicioEduMasCercano() {
		return codigoServicioEduMasCercano;
	}

	public void setCodigoServicioEduMasCercano(String codigoServicioEduMasCercano) {
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
	}

	public String getNombreServicioEduMasCercano() {
		return nombreServicioEduMasCercano;
	}

	public void setNombreServicioEduMasCercano(String nombreServicioEduMasCercano) {
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
	}

	public String getOtroProveedorAgua() {
		return otroProveedorAgua;
	}

	public void setOtroProveedorAgua(String otroProveedorAgua) {
		this.otroProveedorAgua = otroProveedorAgua;
	}

	public String getSuministroAgua() {
		return suministroAgua;
	}

	public void setSuministroAgua(String suministroAgua) {
		this.suministroAgua = suministroAgua;
	}

	public String getOtroProveedorEnergia() {
		return otroProveedorEnergia;
	}

	public void setOtroProveedorEnergia(String otroProveedorEnergia) {
		this.otroProveedorEnergia = otroProveedorEnergia;
	}

	public String getSuministroEnergia() {
		return suministroEnergia;
	}

	public void setSuministroEnergia(String suministroEnergia) {
		this.suministroEnergia = suministroEnergia;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCodigoAreaSig() {
		return codigoAreaSig;
	}

	public void setCodigoAreaSig(String codigoAreaSig) {
		this.codigoAreaSig = codigoAreaSig;
	}

	public Long getZoom() {
		return zoom;
	}

	public void setZoom(Long zoom) {
		this.zoom = zoom;
	}

	public String getMarcoCensal() {
		return marcoCensal;
	}

	public void setMarcoCensal(String marcoCensal) {
		this.marcoCensal = marcoCensal;
	}

	public String getMarcocd() {
		return marcocd;
	}

	public void setMarcocd(String marcocd) {
		this.marcocd = marcocd;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public MaestroDTO getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(MaestroDTO tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public MaestroDTO getTipoGestion() {
		return tipoGestion;
	}

	public void setTipoGestion(MaestroDTO tipoGestion) {
		this.tipoGestion = tipoGestion;
	}

	public MaestroDTO getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(MaestroDTO tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}

	public MaestroDTO getTipoGestionEducativa() {
		return tipoGestionEducativa;
	}

	public void setTipoGestionEducativa(MaestroDTO tipoGestionEducativa) {
		this.tipoGestionEducativa = tipoGestionEducativa;
	}

	public MaestroDTO getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(MaestroDTO tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	public MaestroDTO getTipoContinuidadJornadaEscolar() {
		return tipoContinuidadJornadaEscolar;
	}

	public void setTipoContinuidadJornadaEscolar(MaestroDTO tipoContinuidadJornadaEscolar) {
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
	}

	public MaestroDTO getTipoVia() {
		return tipoVia;
	}

	public void setTipoVia(MaestroDTO tipoVia) {
		this.tipoVia = tipoVia;
	}

	public MaestroDTO getTipoLocalidad() {
		return tipoLocalidad;
	}

	public void setTipoLocalidad(MaestroDTO tipoLocalidad) {
		this.tipoLocalidad = tipoLocalidad;
	}

	public MaestroDTO getTipoProveedorAgua() {
		return tipoProveedorAgua;
	}

	public void setTipoProveedorAgua(MaestroDTO tipoProveedorAgua) {
		this.tipoProveedorAgua = tipoProveedorAgua;
	}

	public MaestroDTO getTipoProveedorEnergia() {
		return tipoProveedorEnergia;
	}

	public void setTipoProveedorEnergia(MaestroDTO tipoProveedorEnergia) {
		this.tipoProveedorEnergia = tipoProveedorEnergia;
	}

	public MaestroDTO getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(MaestroDTO tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public Date getFechaCreacionPrograma() {
		return fechaCreacionPrograma;
	}

	public void setFechaCreacionPrograma(Date fechaCreacionPrograma) {
		this.fechaCreacionPrograma = fechaCreacionPrograma;
	}

	public Date getFechaCierrePrograma() {
		return fechaCierrePrograma;
	}

	public void setFechaCierrePrograma(Date fechaCierrePrograma) {
		this.fechaCierrePrograma = fechaCierrePrograma;
	}

	public Date getFechaRenovacionPrograma() {
		return fechaRenovacionPrograma;
	}

	public void setFechaRenovacionPrograma(Date fechaRenovacionPrograma) {
		this.fechaRenovacionPrograma = fechaRenovacionPrograma;
	}

	public BigDecimal getLatitudCentroPoblado() {
		return latitudCentroPoblado;
	}

	public void setLatitudCentroPoblado(BigDecimal latitudCentroPoblado) {
		this.latitudCentroPoblado = latitudCentroPoblado;
	}

	public BigDecimal getLongitudCentroPoblado() {
		return longitudCentroPoblado;
	}

	public void setLongitudCentroPoblado(BigDecimal longitudCentroPoblado) {
		this.longitudCentroPoblado = longitudCentroPoblado;
	}

	public BigDecimal getLatitudServicioEduMasCercano() {
		return latitudServicioEduMasCercano;
	}

	public void setLatitudServicioEduMasCercano(BigDecimal latitudServicioEduMasCercano) {
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
	}

	public BigDecimal getLongitudServicioEduMasCercano() {
		return longitudServicioEduMasCercano;
	}

	public void setLongitudServicioEduMasCercano(BigDecimal longitudServicioEduMasCercano) {
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
	}

	public String getGeoHash() {
		return geoHash;
	}

	public void setGeoHash(String geoHash) {
		this.geoHash = geoHash;
	}

	public MaestroDTO getTipoResolucion() {
		return tipoResolucion;
	}

	public void setTipoResolucion(MaestroDTO tipoResolucion) {
		this.tipoResolucion = tipoResolucion;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public MaestroDTO getTipoEstado() {
		return tipoEstado;
	}

	public void setTipoEstado(MaestroDTO tipoEstado) {
		this.tipoEstado = tipoEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idPrograma != null ? idPrograma.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AuditoriaProgramaDTO)) {
			return false;
		}
		AuditoriaProgramaDTO other = (AuditoriaProgramaDTO) object;
		if ((this.idPrograma == null && other.idPrograma != null)
				|| (this.idPrograma != null && !this.idPrograma.equals(other.idPrograma))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO[ idPrograma=" + idPrograma + " ]";
	}

}
