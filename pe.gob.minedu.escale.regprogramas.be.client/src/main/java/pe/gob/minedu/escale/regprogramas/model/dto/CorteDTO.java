package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;



public class CorteDTO implements Serializable {

	private Integer idCorte;
	private String 	namePeriodo;
	private Date 	fechaInicioPeriodo;
	private Date 	fechaFinPeriodo;
	private String	userCrea;
	private Date 	fecCrea;
	private String 	estado;
	
	



	
	public CorteDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	

	public CorteDTO(Integer idCorte, String namePeriodo, Date fechaInicioPeriodo, Date fechaFinPeriodo, String userCrea,
			Date fecCrea, String estado) {
		super();
		this.idCorte = idCorte;
		this.namePeriodo = namePeriodo;
		this.fechaInicioPeriodo = fechaInicioPeriodo;
		this.fechaFinPeriodo = fechaFinPeriodo;
		this.userCrea = userCrea;
		this.fecCrea = fecCrea;
		this.estado = estado;
	}




	public Integer getIdCorte() 											{		return idCorte;}
	public void setIdCorte(Integer idCorte) 								{		this.idCorte = idCorte;}
	public String getNamePeriodo() 											{		return namePeriodo;}
	public void setNamePeriodo(String namePeriodo) 							{		this.namePeriodo = namePeriodo;}
	public Date getFechaInicioPeriodo() 									{		return fechaInicioPeriodo;}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) 				{		this.fechaInicioPeriodo = fechaInicioPeriodo;}
	public Date getFechaFinPeriodo() 										{		return fechaFinPeriodo;}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) 					{		this.fechaFinPeriodo = fechaFinPeriodo;}
	public String getUserCrea() 											{		return userCrea;}
	public void setUserCrea(String userCrea) 								{		this.userCrea = userCrea;}
	public Date getFecCrea() 												{		return fecCrea;}
	public void setFecCrea(Date fecCrea) 									{		this.fecCrea = fecCrea;}
	public String getEstado() 												{		return estado;}
	public void setEstado(String estado) 									{		this.estado = estado;}
	

	
	
}

