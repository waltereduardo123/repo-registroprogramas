package pe.gob.minedu.escale.regprogramas.model.dto;

import java.util.Date;





public class PeriodoDTO {

	private Integer idPeriodo;
	private String namePeriodo;
	private Date fechaInicioPeriodo;
	private Date fechaFinPeriodo;
	private Date fechaFinAnio;
	private String codGrupo;
	private String codItem;
	private String userCrea;
	private Date fecCrea;
	private String estado;
	private int count;
	private String anioTope;
	
	




	public PeriodoDTO() 			{
											super();}
	
	

	public Integer getIdPeriodo() 																{		return idPeriodo;}
	public void setIdPeriodo(Integer idPeriodo) 												{		this.idPeriodo = idPeriodo;}
	public String getNamePeriodo() 																{		return namePeriodo;}
	public void setNamePeriodo(String namePeriodo) 												{		this.namePeriodo = namePeriodo;}
	public Date getFechaInicioPeriodo() 														{		return fechaInicioPeriodo;}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) 									{		this.fechaInicioPeriodo = fechaInicioPeriodo;}
	public Date getFechaFinPeriodo() 															{		return fechaFinPeriodo;}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) 										{		this.fechaFinPeriodo = fechaFinPeriodo;}
	public Date getFechaFinAnio() 																{		return fechaFinAnio;}
	public void setFechaFinAnio(Date fechaFinAnio) 												{		this.fechaFinAnio = fechaFinAnio;}
	public String getCodGrupo() 																{		return codGrupo;}
	public void setCodGrupo(String codGrupo) 													{		this.codGrupo = codGrupo;}
	public String getCodItem() 																	{		return codItem;}
	public void setCodItem(String codItem) 														{		this.codItem = codItem;}
	public String getUserCrea() 																{		return userCrea;}
	public void setUserCrea(String userCrea) 													{		this.userCrea = userCrea;}
	public Date getFecCrea() 																	{		return fecCrea;}
	public void setFecCrea(Date fecCrea) 														{		this.fecCrea = fecCrea;}
	public String getEstado() 																	{		return estado;}
	public void setEstado(String estado) 														{		this.estado = estado;}
	public int getCount() 																		{		return count;}
	public void setCount(int count) 															{		this.count = count;}
	public String getAnioTope() 																{		return anioTope;}
	public void setAnioTope(String anioTope) 													{		this.anioTope = anioTope;}
	
}
