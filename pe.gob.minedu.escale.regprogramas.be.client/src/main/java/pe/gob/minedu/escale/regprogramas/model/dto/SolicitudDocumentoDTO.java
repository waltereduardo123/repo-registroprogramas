package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class SolicitudDocumentoDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	protected SolicitudDocumentoPKDTO solicitudDocumentoPKDTO;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private SolicitudDTO solicitud;

	private DocumentoDTO documento;

	public SolicitudDocumentoDTO() {
	}

	public SolicitudDocumentoDTO(SolicitudDocumentoPKDTO solicitudDocumentoPKDTO) {
		this.solicitudDocumentoPKDTO = solicitudDocumentoPKDTO;
	}

	public SolicitudDocumentoDTO(SolicitudDocumentoPKDTO solicitudDocumentoPKDTO, Date fechaCreacion,
			String usuarioCreacion, String estado) {
		this.solicitudDocumentoPKDTO = solicitudDocumentoPKDTO;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public SolicitudDocumentoDTO(long nIdDocumento, long nIdSolicitud) {
		this.solicitudDocumentoPKDTO = new SolicitudDocumentoPKDTO(nIdDocumento, nIdSolicitud);
	}

	public SolicitudDocumentoPKDTO getSolicitudDocumentoPK() {
		return solicitudDocumentoPKDTO;
	}

	public void setSolicitudDocumentoPK(SolicitudDocumentoPKDTO solicitudDocumentoPKDTO) {
		this.solicitudDocumentoPKDTO = solicitudDocumentoPKDTO;
	}

	public SolicitudDocumentoPKDTO getSolicitudDocumentoPKDTO() {
		return solicitudDocumentoPKDTO;
	}

	public void setSolicitudDocumentoPKDTO(SolicitudDocumentoPKDTO solicitudDocumentoPKDTO) {
		this.solicitudDocumentoPKDTO = solicitudDocumentoPKDTO;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public DocumentoDTO getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoDTO documento) {
		this.documento = documento;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (solicitudDocumentoPKDTO != null ? solicitudDocumentoPKDTO.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SolicitudDocumentoDTO)) {
			return false;
		}
		SolicitudDocumentoDTO other = (SolicitudDocumentoDTO) object;
		if ((this.solicitudDocumentoPKDTO == null && other.solicitudDocumentoPKDTO != null)
				|| (this.solicitudDocumentoPKDTO != null
						&& !this.solicitudDocumentoPKDTO.equals(other.solicitudDocumentoPKDTO))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoDTO[ solicitudDocumentoPK="
				+ solicitudDocumentoPKDTO + " ]";
	}

}
