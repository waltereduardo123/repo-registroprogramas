package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class MaestroDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idMaestro;

	private String codigoAgrupacion;

	private String nombreMaestro;

	private String descripcionMaestro;

	private String codigoItem;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private List<ProveedorDepartamentoDTO> proveedorDepartamentoDTO;

	private List<MaestroDTO> listaMaestro;

	private MaestroDTO maestroPadre;

	public MaestroDTO() {
	}

	public MaestroDTO(Long idMaestro) {
		this.idMaestro = idMaestro;
	}

	public MaestroDTO(Long idMaestro, String codigoAgrupacion, String nombreMaestro, String descripcionMaestro,
			String codigoItem, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion,
			String usuarioUltimaModificacion, String estado, List<ProveedorDepartamentoDTO> proveedorDepartamentoDTO,
			List<MaestroDTO> listaMaestro, MaestroDTO maestroPadre) {
		super();
		this.idMaestro = idMaestro;
		this.codigoAgrupacion = codigoAgrupacion;
		this.nombreMaestro = nombreMaestro;
		this.descripcionMaestro = descripcionMaestro;
		this.codigoItem = codigoItem;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.proveedorDepartamentoDTO = proveedorDepartamentoDTO;
		this.listaMaestro = listaMaestro;
		this.maestroPadre = maestroPadre;
	}

	public Long getIdMaestro() {
		return idMaestro;
	}

	public void setIdMaestro(Long idMaestro) {
		this.idMaestro = idMaestro;
	}

	public String getCodigoAgrupacion() {
		return codigoAgrupacion;
	}

	public void setCodigoAgrupacion(String codigoAgrupacion) {
		this.codigoAgrupacion = codigoAgrupacion;
	}

	public String getNombreMaestro() {
		return nombreMaestro;
	}

	public void setNombreMaestro(String nombreMaestro) {
		this.nombreMaestro = nombreMaestro;
	}

	public String getDescripcionMaestro() {
		return descripcionMaestro;
	}

	public void setDescripcionMaestro(String descripcionMaestro) {
		this.descripcionMaestro = descripcionMaestro;
	}

	public String getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<ProveedorDepartamentoDTO> getProveedorDepartamento() {
		return proveedorDepartamentoDTO;
	}

	public void setProveedorDepartamento(List<ProveedorDepartamentoDTO> proveedorDepartamentoDTO) {
		this.proveedorDepartamentoDTO = proveedorDepartamentoDTO;
	}

	public List<MaestroDTO> getListaMaestro() {
		return listaMaestro;
	}

	public void setListaMaestro(List<MaestroDTO> listaMaestro) {
		this.listaMaestro = listaMaestro;
	}

	public MaestroDTO getMaestroPadre() {
		return maestroPadre;
	}

	public void setMaestroPadre(MaestroDTO maestroPadre) {
		this.maestroPadre = maestroPadre;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idMaestro != null ? idMaestro.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof MaestroDTO)) {
			return false;
		}
		MaestroDTO other = (MaestroDTO) object;
		if ((this.idMaestro == null && other.idMaestro != null)
				|| (this.idMaestro != null && !this.idMaestro.equals(other.idMaestro))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO[ idMaestro=" + idMaestro + " ]";
	}

}
