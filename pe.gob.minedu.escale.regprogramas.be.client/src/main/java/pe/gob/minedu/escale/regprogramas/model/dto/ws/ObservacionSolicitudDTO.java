package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;
import java.util.List;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class ObservacionSolicitudDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 787472789972534263L;

	private Long idSolicitud;
	private List<Long> listaCampos;
	private String observacion;
	private String usuarioCreacion;
	private String perfil;
	private String codigoModular;
	private String idbtn;
	private String inexus;
	private String jodexus;
	
	public ObservacionSolicitudDTO() {
	}

	public ObservacionSolicitudDTO(Long idSolicitud, List<Long> listaCampos, String observacion,
			String usuarioCreacion) {
		this.idSolicitud = idSolicitud;
		this.listaCampos = listaCampos;
		this.observacion = observacion;
		this.usuarioCreacion = usuarioCreacion;
	}

	public ObservacionSolicitudDTO(Long idSolicitud, List<Long> listaCampos, String observacion, String usuarioCreacion,
			String perfil) {
		this.idSolicitud = idSolicitud;
		this.listaCampos = listaCampos;
		this.observacion = observacion;
		this.usuarioCreacion = usuarioCreacion;
		this.perfil = perfil;
	}

	public ObservacionSolicitudDTO(Long idSolicitud, List<Long> listaCampos, String observacion, String usuarioCreacion,
			String perfil, String codigoModular) {
		this.idSolicitud = idSolicitud;
		this.listaCampos = listaCampos;
		this.observacion = observacion;
		this.usuarioCreacion = usuarioCreacion;
		this.perfil = perfil;
		this.codigoModular = codigoModular;
	}

	
	
	public ObservacionSolicitudDTO(Long idSolicitud, List<Long> listaCampos, String observacion, String usuarioCreacion,
			String perfil, String codigoModular, String idbtn, String inexus, String jodexus) {
		super();
		this.idSolicitud = idSolicitud;
		this.listaCampos = listaCampos;
		this.observacion = observacion;
		this.usuarioCreacion = usuarioCreacion;
		this.perfil = perfil;
		this.codigoModular = codigoModular;
		this.idbtn = idbtn;
		this.inexus = inexus;
		this.jodexus = jodexus;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public List<Long> getListaCampos() {
		return listaCampos;
	}

	public void setListaCampos(List<Long> listaCampos) {
		this.listaCampos = listaCampos;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getIdbtn() {
		return idbtn;
	}

	public void setIdbtn(String idbtn) {
		this.idbtn = idbtn;
	}

	public String getInexus() {
		return inexus;
	}

	public void setInexus(String inexus) {
		this.inexus = inexus;
	}

	public String getJodexus() {
		return jodexus;
	}

	public void setJodexus(String jodexus) {
		this.jodexus = jodexus;
	}
	
	

}
