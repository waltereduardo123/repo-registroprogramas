package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

@XmlRootElement
public class SolicitudBandejaDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = -3109811418637526872L;

	private Long idSolicitud;
	private String dreUgelCodigo;
	private String tipoSolicitud;
	private String nombrePrograma;
	private String tipoPrograma;
	private String codigoModular;
	private String tipoSituacionSolicitud;
	private String nombreUsuarioRevision;
	private String nombreUsuarioRevisionSig;
	private String usuarioRevision;
	private String usuarioRevisionSig;
	private String tipoSituacionRevisionSig;
	private String tipoSituacionRevision;
	private Long idDocumento;
	private String codigoDocumento;
	private Date fechaCreacion;
	private ArchivosBandejaDTO archivoCroquis;
	private List<ArchivosBandejaDTO> listaArchivoResoluciones;
	private int count;
	private String indicadorUltimo;
	private Date fechaModificacion;//imendoza 20170215

	public SolicitudBandejaDTO() {

	}

	public SolicitudBandejaDTO(Long idSolicitud, String dreUgelCodigo, Date fechaCreacion, String tipoSolicitud,
			String nombrePrograma, String tipoPrograma, String codigoModular, String tipoSituacionSolicitud,
			String nombreUsuarioRevision, String nombreUsuarioRevisionSig, Long idDocumento, String codigoDocumento,
			ArchivosBandejaDTO archivoCroquis, List<ArchivosBandejaDTO> listaArchivoResoluciones,
			String usuarioRevision, String usuarioRevisionSig, int count) {
		super();
		this.idSolicitud = idSolicitud;
		this.dreUgelCodigo = dreUgelCodigo;
		this.fechaCreacion = fechaCreacion;
		this.tipoSolicitud = tipoSolicitud;
		this.nombrePrograma = nombrePrograma;
		this.tipoPrograma = tipoPrograma;
		this.codigoModular = codigoModular;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.nombreUsuarioRevision = nombreUsuarioRevision;
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
		this.idDocumento = idDocumento;
		this.codigoDocumento = codigoDocumento;
		this.archivoCroquis = archivoCroquis;
		this.listaArchivoResoluciones = listaArchivoResoluciones;
		this.usuarioRevision = usuarioRevision;
		this.usuarioRevisionSig = usuarioRevisionSig;
		this.count = count;
	}

	public SolicitudBandejaDTO(Long idSolicitud, String dreUgelCodigo, Date fechaCreacion, String tipoSolicitud,
			String nombrePrograma, String tipoPrograma, String codigoModular, String tipoSituacionSolicitud,
			String nombreUsuarioRevision, String nombreUsuarioRevisionSig, Long idDocumento, String codigoDocumento,
			ArchivosBandejaDTO archivoCroquis, List<ArchivosBandejaDTO> listaArchivoResoluciones,
			String usuarioRevision, String usuarioRevisionSig, String tipoSituacionRevisionSig,
			String tipoSituacionRevision, int count) {
		super();
		this.idSolicitud = idSolicitud;
		this.dreUgelCodigo = dreUgelCodigo;
		this.fechaCreacion = fechaCreacion;
		this.tipoSolicitud = tipoSolicitud;
		this.nombrePrograma = nombrePrograma;
		this.tipoPrograma = tipoPrograma;
		this.codigoModular = codigoModular;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.nombreUsuarioRevision = nombreUsuarioRevision;
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
		this.idDocumento = idDocumento;
		this.codigoDocumento = codigoDocumento;
		this.archivoCroquis = archivoCroquis;
		this.listaArchivoResoluciones = listaArchivoResoluciones;
		this.usuarioRevision = usuarioRevision;
		this.usuarioRevisionSig = usuarioRevisionSig;
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
		this.tipoSituacionRevision = tipoSituacionRevision;
		this.count = count;
	}

	public SolicitudBandejaDTO(Long idSolicitud, String dreUgelCodigo, Date fechaCreacion, String tipoSolicitud,
			String nombrePrograma, String tipoPrograma, String codigoModular, String tipoSituacionSolicitud,
			String nombreUsuarioRevision, String nombreUsuarioRevisionSig, Long idDocumento, String codigoDocumento,
			ArchivosBandejaDTO archivoCroquis, List<ArchivosBandejaDTO> listaArchivoResoluciones,
			String usuarioRevision, String usuarioRevisionSig, String tipoSituacionRevisionSig,
			String tipoSituacionRevision, int count, String indicadorUltimo, Date fechaModificacion) {
		super();
		this.idSolicitud = idSolicitud;
		this.dreUgelCodigo = dreUgelCodigo;
		this.fechaCreacion = fechaCreacion;
		this.tipoSolicitud = tipoSolicitud;
		this.nombrePrograma = nombrePrograma;
		this.tipoPrograma = tipoPrograma;
		this.codigoModular = codigoModular;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.nombreUsuarioRevision = nombreUsuarioRevision;
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
		this.idDocumento = idDocumento;
		this.codigoDocumento = codigoDocumento;
		this.archivoCroquis = archivoCroquis;
		this.listaArchivoResoluciones = listaArchivoResoluciones;
		this.usuarioRevision = usuarioRevision;
		this.usuarioRevisionSig = usuarioRevisionSig;
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
		this.tipoSituacionRevision = tipoSituacionRevision;
		this.count = count;
		this.indicadorUltimo = indicadorUltimo;
		this.fechaModificacion = fechaModificacion;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getDreUgelCodigo() {
		return dreUgelCodigo;
	}

	public void setDreUgelCodigo(String dreUgelCodigo) {
		this.dreUgelCodigo = dreUgelCodigo;
	}

	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public String getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(String tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getTipoSituacionSolicitud() {
		return tipoSituacionSolicitud;
	}

	public void setTipoSituacionSolicitud(String tipoSituacionSolicitud) {
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
	}

	public String getNombreUsuarioRevision() {
		return nombreUsuarioRevision;
	}

	public void setNombreUsuarioRevision(String nombreUsuarioRevision) {
		this.nombreUsuarioRevision = nombreUsuarioRevision;
	}

	public String getNombreUsuarioRevisionSig() {
		return nombreUsuarioRevisionSig;
	}

	public void setNombreUsuarioRevisionSig(String nombreUsuarioRevisionSig) {
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
	}

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	public ArchivosBandejaDTO getArchivoCroquis() {
		return archivoCroquis;
	}

	public void setArchivoCroquis(ArchivosBandejaDTO archivoCroquis) {
		this.archivoCroquis = archivoCroquis;
	}

	public List<ArchivosBandejaDTO> getListaArchivoResoluciones() {
		return listaArchivoResoluciones;
	}

	public void setListaArchivoResoluciones(List<ArchivosBandejaDTO> listaArchivoResoluciones) {
		this.listaArchivoResoluciones = listaArchivoResoluciones;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioRevision() {
		return usuarioRevision;
	}

	public void setUsuarioRevision(String usuarioRevision) {
		this.usuarioRevision = usuarioRevision;
	}

	public String getUsuarioRevisionSig() {
		return usuarioRevisionSig;
	}

	public void setUsuarioRevisionSig(String usuarioRevisionSig) {
		this.usuarioRevisionSig = usuarioRevisionSig;
	}

	public String getTipoSituacionRevisionSig() {
		return tipoSituacionRevisionSig;
	}

	public void setTipoSituacionRevisionSig(String tipoSituacionRevisionSig) {
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
	}

	public String getTipoSituacionRevision() {
		return tipoSituacionRevision;
	}

	public void setTipoSituacionRevision(String tipoSituacionRevision) {
		this.tipoSituacionRevision = tipoSituacionRevision;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getIndicadorUltimo() {
		return indicadorUltimo;
	}

	public void setIndicadorUltimo(String indicadorUltimo) {
		this.indicadorUltimo = indicadorUltimo;
	}

	public String codigoDocumento() {
		return codigoDocumento;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
