package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;

public class DeshacerObservacionCompareDTO implements Serializable {
	private static final long serialVersionUID = 787472789972534263L;
	
	private Long idSolicitud;
    private String usuarioCreacion;
    private String perfil;
    private String idbtn;
	private String init; // privilegio que le corresponde al perfil del usuario
    
	public DeshacerObservacionCompareDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getIdbtn() {
		return idbtn;
	}

	public void setIdbtn(String idbtn) {
		this.idbtn = idbtn;
	}
	
	
	

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSolicitud == null) ? 0 : idSolicitud.hashCode());
		result = prime * result + ((idbtn == null) ? 0 : idbtn.hashCode());
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result + ((usuarioCreacion == null) ? 0 : usuarioCreacion.hashCode());
		result = prime * result + ((init == null) ? 0 : init.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeshacerObservacionCompareDTO other = (DeshacerObservacionCompareDTO) obj;
		if (idSolicitud == null) {
			if (other.idSolicitud != null)
				return false;
		} else if (!idSolicitud.equals(other.idSolicitud))
			return false;
		if (idbtn == null) {
			if (other.idbtn != null)
				return false;
		} else if (!idbtn.equals(other.idbtn))
			return false;
		if (perfil == null) {
			if (other.perfil != null)
				return false;
		} else if (!perfil.equals(other.perfil)) //init
			return false;
		if (usuarioCreacion == null) {
			if (other.usuarioCreacion != null)
				return false;
		} else if (!usuarioCreacion.equals(other.usuarioCreacion))
			return false;
		if (init == null) {
			if (other.init != null)
				return false;
		} else if (!init.equals(other.init))
			return false;
		return true;
	}


    
    
	
}
