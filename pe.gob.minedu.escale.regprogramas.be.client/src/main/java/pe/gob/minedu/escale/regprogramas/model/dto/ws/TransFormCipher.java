package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;


import pe.gob.minedu.escale.common.util.DtoUtil;

public class TransFormCipher extends DtoUtil implements Serializable {
	
	private static final long serialVersionUID = 787472789972534263L;

	private String kenvio;
	private String jodexus;
	
	private String objInexus;
	private String objJodexus;
	private String inexus;


	public TransFormCipher(String kenvio, String jodexus, String objInexus, String objJodexus, String inexus) {
		super();
		this.kenvio = kenvio;
		this.jodexus = jodexus;
		this.objInexus = objInexus;
		this.objJodexus = objJodexus;
		this.inexus = inexus;
	}

	public String getObjInexus() {
		return objInexus;
	}

	public void setObjInexus(String objInexus) {
		this.objInexus = objInexus;
	}

	public String getObjJodexus() {
		return objJodexus;
	}

	public void setObjJodexus(String objJodexus) {
		this.objJodexus = objJodexus;
	}

	public String getInexus() {
		return inexus;
	}

	public void setInexus(String inexus) {
		this.inexus = inexus;
	}

	public TransFormCipher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getKenvio() {
		return kenvio;
	}

	public void setKenvio(String kenvio) {
		this.kenvio = kenvio;
	}

	public String getJodexus() {
		return jodexus;
	}

	public void setJodexus(String jodexus) {
		this.jodexus = jodexus;
	}
	
	
	

}
