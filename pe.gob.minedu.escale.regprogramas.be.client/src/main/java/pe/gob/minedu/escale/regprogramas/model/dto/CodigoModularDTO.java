package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
public class CodigoModularDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String codigoModular;

	private String estadoAsignado;

	private String codoiiAsignado;

	private String usuarioAsignacion;

	private Date fechaAsignacion;

	public CodigoModularDTO() {
	}

	public CodigoModularDTO(Long id) {
		this.id = id;
	}

	public CodigoModularDTO(Long id, String codigoModular, String estadoAsignado, String codoiiAsignado,
			String usuarioAsignacion, Date fechaAsignacion) {
		super();
		this.id = id;
		this.codigoModular = codigoModular;
		this.estadoAsignado = estadoAsignado;
		this.codoiiAsignado = codoiiAsignado;
		this.usuarioAsignacion = usuarioAsignacion;
		this.fechaAsignacion = fechaAsignacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public String getEstadoAsignado() {
		return estadoAsignado;
	}

	public void setEstadoAsignado(String estadoAsignado) {
		this.estadoAsignado = estadoAsignado;
	}

	public String getCodoiiAsignado() {
		return codoiiAsignado;
	}

	public void setCodoiiAsignado(String codoiiAsignado) {
		this.codoiiAsignado = codoiiAsignado;
	}

	public String getUsuarioAsignacion() {
		return usuarioAsignacion;
	}

	public void setUsuarioAsignacion(String usuarioAsignacion) {
		this.usuarioAsignacion = usuarioAsignacion;
	}

	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodigoModularDTO other = (CodigoModularDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.CodigoModularDTO[ id	=" + id + " ]";
	}

}
