package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class ProveedorDepartamentoPKDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private String idRegion;

	private long idMaestro;

	public ProveedorDepartamentoPKDTO() {

	}

	public ProveedorDepartamentoPKDTO(String idRegion, long idMaestro) {
		super();
		this.idRegion = idRegion;
		this.idMaestro = idMaestro;
	}

	public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public long getIdMaestro() {
		return idMaestro;
	}

	public void setIdMaestro(long idMaestro) {
		this.idMaestro = idMaestro;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idRegion != null ? idRegion.hashCode() : 0);
		hash += (int) idMaestro;
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ProveedorDepartamentoPKDTO)) {
			return false;
		}
		ProveedorDepartamentoPKDTO other = (ProveedorDepartamentoPKDTO) object;
		if ((this.idRegion == null && other.idRegion != null)
				|| (this.idRegion != null && !this.idRegion.equals(other.idRegion))) {
			return false;
		}
		if (this.idMaestro != other.idMaestro) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.ProveedorDepartamentoPKDTO[ idRegion=" + idRegion
				+ ", idMaestro=" + idMaestro + " ]";
	}

}
