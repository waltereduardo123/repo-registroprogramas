package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class DreUgelDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;

	private String nombreoi;

	private String tipo;

	private Short inVerna;

	private BigDecimal pointX;

	private BigDecimal pointY;

	private Short zoom;

	private List<CoordinadorDreUgelDTO> listaCoordinadorDreUgel;

	private List<SolicitudDTO> listaSolicitud;

	private List<DreGeoDTO> listaDreGeo;

	public DreUgelDTO() {
	}

	public DreUgelDTO(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombreoi() {
		return nombreoi;
	}

	public void setNombreoi(String nombreoi) {
		this.nombreoi = nombreoi;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Short getInVerna() {
		return inVerna;
	}

	public void setInVerna(Short inVerna) {
		this.inVerna = inVerna;
	}

	public BigDecimal getPointX() {
		return pointX;
	}

	public void setPointX(BigDecimal pointX) {
		this.pointX = pointX;
	}

	public BigDecimal getPointY() {
		return pointY;
	}

	public void setPointY(BigDecimal pointY) {
		this.pointY = pointY;
	}

	public Short getZoom() {
		return zoom;
	}

	public void setZoom(Short zoom) {
		this.zoom = zoom;
	}

	public List<CoordinadorDreUgelDTO> getListaCoordinadorDreUgel() {
		return listaCoordinadorDreUgel;
	}

	public void setListaCoordinadorDreUgel(List<CoordinadorDreUgelDTO> listaCoordinadorDreUgel) {
		this.listaCoordinadorDreUgel = listaCoordinadorDreUgel;
	}

	public List<SolicitudDTO> getListaSolicitud() {
		return listaSolicitud;
	}

	public void setListaSolicitud(List<SolicitudDTO> listaSolicitud) {
		this.listaSolicitud = listaSolicitud;
	}

	public List<DreGeoDTO> getListaDreGeo() {
		return listaDreGeo;
	}

	public void setListaDreGeo(List<DreGeoDTO> listaDreGeo) {
		this.listaDreGeo = listaDreGeo;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DreUgelDTO)) {
			return false;
		}
		DreUgelDTO other = (DreUgelDTO) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.DreUgelDTO[ codigo=" + codigo + " ]";
	}

}
