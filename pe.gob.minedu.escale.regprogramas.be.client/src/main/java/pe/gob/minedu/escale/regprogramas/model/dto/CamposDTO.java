package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class CamposDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idCampos;

	private String nombreCampo;

	private String codigoCampo;

	private Boolean obligatorio;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos;

	public CamposDTO() {
	}

	public CamposDTO(Long idCampos) {
		this.idCampos = idCampos;
	}

	public CamposDTO(Long idCampos, Date dFeccre, String cUsucre, String cEstado) {
		this.idCampos = idCampos;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
	}

	public CamposDTO(Long idCampos, String nombreCampo, String codigoCampo, Boolean obligatorio, String estado) {
		this.idCampos = idCampos;
		this.nombreCampo = nombreCampo;
		this.codigoCampo = codigoCampo;
		this.obligatorio = obligatorio;
		this.estado = estado;
	}

	public Long getIdCampos() {
		return idCampos;
	}

	public void setIdCampos(Long idCampos) {
		this.idCampos = idCampos;
	}

	public String getNombreCampo() {
		return nombreCampo;
	}

	public void setNombreCampo(String nombreCampo) {
		this.nombreCampo = nombreCampo;
	}

	public Boolean getObligatorio() {
		return obligatorio;
	}

	public void setObligatorio(Boolean obligatorio) {
		this.obligatorio = obligatorio;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoCampo() {
		return codigoCampo;
	}

	public void setCodigoCampo(String codigoCampo) {
		this.codigoCampo = codigoCampo;
	}

	public List<RevisionSolicitudCamposDTO> getListaRevisionSolicitudCampos() {
		return listaRevisionSolicitudCampos;
	}

	public void setListaRevisionSolicitudCampos(List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos) {
		this.listaRevisionSolicitudCampos = listaRevisionSolicitudCampos;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idCampos != null ? idCampos.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof CamposDTO)) {
			return false;
		}
		CamposDTO other = (CamposDTO) object;
		if ((this.idCampos == null && other.idCampos != null)
				|| (this.idCampos != null && !this.idCampos.equals(other.idCampos))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.CamposDTO[ idCampos=" + idCampos + " ]";
	}

}
