package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class DreGeoPKDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;

	private String idDistrito;

	public DreGeoPKDTO() {
	}

	public DreGeoPKDTO(String codigo, String idDistrito) {
		this.codigo = codigo;
		this.idDistrito = idDistrito;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (codigo != null ? codigo.hashCode() : 0);
		hash += (idDistrito != null ? idDistrito.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DreGeoPKDTO)) {
			return false;
		}
		DreGeoPKDTO other = (DreGeoPKDTO) object;
		if ((this.codigo == null && other.codigo != null)
				|| (this.codigo != null && !this.codigo.equals(other.codigo))) {
			return false;
		}
		if ((this.idDistrito == null && other.idDistrito != null)
				|| (this.idDistrito != null && !this.idDistrito.equals(other.idDistrito))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.DreGeoPKDTO[ codigo=" + codigo + ", idDistrito=" + idDistrito + " ]";
	}

}
