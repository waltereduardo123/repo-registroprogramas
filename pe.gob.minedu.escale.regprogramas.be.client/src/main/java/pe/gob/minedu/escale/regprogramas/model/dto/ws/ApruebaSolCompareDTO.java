package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class ApruebaSolCompareDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 787472789972534263L;

     
 	private String codSolicitud;
	private String obsSolicitud;
	private String perfilUsuario;
	private String codArea;
	private String codCPoblado;
	private String codAreaSig;
	
	public ApruebaSolCompareDTO() {
		super();
	}

	public ApruebaSolCompareDTO(String codSolicitud, String obsSolicitud, String perfilUsuario, String codArea,
			String codCPoblado, String codAreaSig) {
		super();
		this.codSolicitud = codSolicitud;
		this.obsSolicitud = obsSolicitud;
		this.perfilUsuario = perfilUsuario;
		this.codArea = codArea;
		this.codCPoblado = codCPoblado;
		this.codAreaSig = codAreaSig;
	}

	public String getCodSolicitud() {
		return codSolicitud;
	}

	public void setCodSolicitud(String codSolicitud) {
		this.codSolicitud = codSolicitud;
	}

	public String getObsSolicitud() {
		return obsSolicitud;
	}

	public void setObsSolicitud(String obsSolicitud) {
		this.obsSolicitud = obsSolicitud;
	}

	public String getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public String getCodArea() {
		return codArea;
	}

	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}

	public String getCodCPoblado() {
		return codCPoblado;
	}

	public void setCodCPoblado(String codCPoblado) {
		this.codCPoblado = codCPoblado;
	}

	public String getCodAreaSig() {
		return codAreaSig;
	}

	public void setCodAreaSig(String codAreaSig) {
		this.codAreaSig = codAreaSig;
	}
	

	
	@Override
 	public boolean equals(Object obj) {
 		if(((ApruebaSolCompareDTO)obj).getCodSolicitud().equals(codSolicitud)&&((ApruebaSolCompareDTO)obj).getObsSolicitud().equals(obsSolicitud) 
 				&&((ApruebaSolCompareDTO)obj).getPerfilUsuario().equals(perfilUsuario)&&((ApruebaSolCompareDTO)obj).getCodArea().equals(codArea)
 				&&((ApruebaSolCompareDTO)obj).getCodCPoblado().equals(codCPoblado)&&((ApruebaSolCompareDTO)obj).getCodAreaSig().equals(codAreaSig)  ){
 			return true;
 		}else{
 			return false;
 		}
 		
 		
 	}
	
}
