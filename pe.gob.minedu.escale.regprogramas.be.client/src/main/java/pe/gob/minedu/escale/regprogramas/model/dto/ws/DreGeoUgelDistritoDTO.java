package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

@XmlRootElement
public class DreGeoUgelDistritoDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 6911610325599748255L;

	private String idDreUgel;

	private String nombreoi;
	
	private String idDistrito;
	
	private String nombreDistrito;
	
	private BigDecimal pointXDistrito;
	
	private BigDecimal pointYDistrito;
	
	private Short zoomDistrito;
	
	private String idRegion;
	
	private String nombreRegion;
	
	private BigDecimal pointXRegion;
	
	private BigDecimal pointYRegion;
	
	private Short zoomRegion;

	private String idProvincia;
	
	private String nombreProvincia;
	
	private BigDecimal pointXProvincia;
	
	private BigDecimal pointYProvincia;
	
	private Short zoomProvincia;
	


	public DreGeoUgelDistritoDTO(String idDreUgel, String nombreoi, String idDistrito, String nombreDistrito,
			BigDecimal pointXDistrito, BigDecimal pointYDistrito, Short zoomDistrito, String idRegion,
			String nombreRegion, BigDecimal pointXRegion, BigDecimal pointYRegion, Short zoomRegion, String idProvincia,
			String nombreProvincia, BigDecimal pointXProvincia, BigDecimal pointYProvincia, Short zoomProvincia) {		
		this.idDreUgel = idDreUgel;
		this.nombreoi = nombreoi;
		this.idDistrito = idDistrito;
		this.nombreDistrito = nombreDistrito;
		this.pointXDistrito = pointXDistrito;
		this.pointYDistrito = pointYDistrito;
		this.zoomDistrito = zoomDistrito;
		this.idRegion = idRegion;
		this.nombreRegion = nombreRegion;
		this.pointXRegion = pointXRegion;
		this.pointYRegion = pointYRegion;
		this.zoomRegion = zoomRegion;
		this.idProvincia = idProvincia;
		this.nombreProvincia = nombreProvincia;
		this.pointXProvincia = pointXProvincia;
		this.pointYProvincia = pointYProvincia;
		this.zoomProvincia = zoomProvincia;
	}

	public String getIdDreUgel() {
		return idDreUgel;
	}

	public void setIdDreUgel(String idDreUgel) {
		this.idDreUgel = idDreUgel;
	}

	public String getNombreoi() {
		return nombreoi;
	}

	public void setNombreoi(String nombreoi) {
		this.nombreoi = nombreoi;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getNombreDistrito() {
		return nombreDistrito;
	}

	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}

	public BigDecimal getPointXDistrito() {
		return pointXDistrito;
	}

	public void setPointXDistrito(BigDecimal pointXDistrito) {
		this.pointXDistrito = pointXDistrito;
	}

	public BigDecimal getPointYDistrito() {
		return pointYDistrito;
	}

	public void setPointYDistrito(BigDecimal pointYDistrito) {
		this.pointYDistrito = pointYDistrito;
	}

	public Short getZoomDistrito() {
		return zoomDistrito;
	}

	public void setZoomDistrito(Short zoomDistrito) {
		this.zoomDistrito = zoomDistrito;
	}

	public String getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	public String getNombreRegion() {
		return nombreRegion;
	}

	public void setNombreRegion(String nombreRegion) {
		this.nombreRegion = nombreRegion;
	}

	public BigDecimal getPointXRegion() {
		return pointXRegion;
	}

	public void setPointXRegion(BigDecimal pointXRegion) {
		this.pointXRegion = pointXRegion;
	}

	public BigDecimal getPointYRegion() {
		return pointYRegion;
	}

	public void setPointYRegion(BigDecimal pointYRegion) {
		this.pointYRegion = pointYRegion;
	}

	public Short getZoomRegion() {
		return zoomRegion;
	}

	public void setZoomRegion(Short zoomRegion) {
		this.zoomRegion = zoomRegion;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getNombreProvincia() {
		return nombreProvincia;
	}

	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}

	public BigDecimal getPointXProvincia() {
		return pointXProvincia;
	}

	public void setPointXProvincia(BigDecimal pointXProvincia) {
		this.pointXProvincia = pointXProvincia;
	}

	public BigDecimal getPointYProvincia() {
		return pointYProvincia;
	}

	public void setPointYProvincia(BigDecimal pointYProvincia) {
		this.pointYProvincia = pointYProvincia;
	}

	public Short getZoomProvincia() {
		return zoomProvincia;
	}

	public void setZoomProvincia(Short zoomProvincia) {
		this.zoomProvincia = zoomProvincia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDistrito == null) ? 0 : idDistrito.hashCode());
		result = prime * result + ((idDreUgel == null) ? 0 : idDreUgel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DreGeoUgelDistritoDTO other = (DreGeoUgelDistritoDTO) obj;
		if (idDistrito == null) {
			if (other.idDistrito != null)
				return false;
		} else if (!idDistrito.equals(other.idDistrito))
			return false;
		if (idDreUgel == null) {
			if (other.idDreUgel != null)
				return false;
		} else if (!idDreUgel.equals(other.idDreUgel))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DreGeoUgelDistritoDTO [idDreUgel=" + idDreUgel + ", idDistrito=" + idDistrito + "]";
	}

}
