package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class ArchivoDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idArchivo;

	private MaestroDTO tipoArchivo;

	private String nombreArchivo;

	private String codigoArchivo;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private DocumentoDTO documentoDTO;

	public ArchivoDTO() {
	}

	public ArchivoDTO(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public ArchivoDTO(Long idArchivo, MaestroDTO tipoArchivo, String nombreArchivo, Date fechaCreacion,
			String usuarioCreacion, String estado) {
		this.idArchivo = idArchivo;
		this.tipoArchivo = tipoArchivo;
		this.nombreArchivo = nombreArchivo;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public ArchivoDTO(Long idArchivo, String nombreArchivo,String codigoArchivo, Date fechaCreacion,
			String usuarioCreacion, String estado) {
		this.idArchivo = idArchivo;
		this.nombreArchivo = nombreArchivo;
		this.codigoArchivo = codigoArchivo;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}
	
	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public MaestroDTO getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(MaestroDTO tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getCodigoArchivo() {
		return codigoArchivo;
	}

	public void setCodigoArchivo(String codigoArchivo) {
		this.codigoArchivo = codigoArchivo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public DocumentoDTO getDocumento() {
		return documentoDTO;
	}

	public void setDocumento(DocumentoDTO documentoDTO) {
		this.documentoDTO = documentoDTO;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idArchivo != null ? idArchivo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ArchivoDTO)) {
			return false;
		}
		ArchivoDTO other = (ArchivoDTO) object;
		if ((this.idArchivo == null && other.idArchivo != null)
				|| (this.idArchivo != null && !this.idArchivo.equals(other.idArchivo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO[ idArchivo=" + idArchivo + " ]";
	}

}
