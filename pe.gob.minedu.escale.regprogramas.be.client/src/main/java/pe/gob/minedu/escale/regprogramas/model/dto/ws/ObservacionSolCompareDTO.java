package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;
import java.util.List;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class ObservacionSolCompareDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 787472789972534263L;
	
	
	private Long idSolicitud;
	private List<Long> listaCampos;
	private String observacion;
	private String usuarioCreacion;
	private String perfil;
	private String init; // privilegio que le corresponde al perfil del usuario
	
	
	public ObservacionSolCompareDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public List<Long> getListaCampos() {
		return listaCampos;
	}
	public void setListaCampos(List<Long> listaCampos) {
		this.listaCampos = listaCampos;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	
	
	public String getInit() {
		return init;
	}
	public void setInit(String init) {
		this.init = init;
	}
	@Override
	public boolean equals(Object objt) {
		
//		if(this == arg0){
//			return true;
//		}
//		
//		if(arg0 == null || getClass() != arg0.getClass() ){
//			return false;
//		}
//		
//		
//		return super.equals(arg0);
		
		if(
				((ObservacionSolCompareDTO)objt).getIdSolicitud().equals(idSolicitud)&&
				((ObservacionSolCompareDTO)objt).getListaCampos().equals(listaCampos)&&
				((ObservacionSolCompareDTO)objt).getObservacion().equals(observacion)&&
				((ObservacionSolCompareDTO)objt).getUsuarioCreacion().equals(usuarioCreacion)&&
				((ObservacionSolCompareDTO)objt).getPerfil().equals(perfil)&&
				((ObservacionSolCompareDTO)objt).getInit().equals(init) ){
					return true;
				}else{
					return false;
				}
	}
	
	
	
	@Override
	public String toString() {
		return "ObservacionSolCompareDTO [idSolicitud=" + idSolicitud + ", listaCampos=" + listaCampos
				+ ", observacion=" + observacion + ", usuarioCreacion=" + usuarioCreacion + ", perfil=" + perfil + ", init=" + init + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSolicitud == null) ? 0 : idSolicitud.hashCode());
		result = prime * result + ((init == null) ? 0 : init.hashCode());
		result = prime * result + ((listaCampos == null) ? 0 : listaCampos.hashCode());
		result = prime * result + ((observacion == null) ? 0 : observacion.hashCode());
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result + ((usuarioCreacion == null) ? 0 : usuarioCreacion.hashCode());
		return result;
	}

	
	
	
}
