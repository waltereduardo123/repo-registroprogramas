package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import pe.gob.minedu.escale.common.util.EntidadUtil;

/**
 *
 * @author IMENDOZA
 */
public class ParametroDTO extends EntidadUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idParametro;

	private String codigoParametro;

	private String descripcionParametro;

	private String valorLetra;

	private BigDecimal valorNumerico;

	private Date valorFecha;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	public ParametroDTO() {
	}

	public ParametroDTO(Long idParametro) {
		this.idParametro = idParametro;
	}

	public ParametroDTO(Long idParametro, String codigoParametro, String descripcionParametro, String valorLetra,
			BigDecimal valorNumerico, Date valorFecha, Date fechaCreacion, String usuarioCreacion,
			Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado) {
		super();
		this.idParametro = idParametro;
		this.codigoParametro = codigoParametro;
		this.descripcionParametro = descripcionParametro;
		this.valorLetra = valorLetra;
		this.valorNumerico = valorNumerico;
		this.valorFecha = valorFecha;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
	}

	public ParametroDTO(Long idParametro, String codigoParametro, String descripcionParametro, String valorLetra,
			BigDecimal valorNumerico, Date valorFecha, String estado) {
		this.idParametro = idParametro;
		this.codigoParametro = codigoParametro;
		this.descripcionParametro = descripcionParametro;
		this.valorLetra = valorLetra;
		this.valorNumerico = valorNumerico;
		this.valorFecha = valorFecha;
		this.estado = estado;
	}

	public Long getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public String getCodigoParametro() {
		return codigoParametro;
	}

	public void setCodigoParametro(String codigoParametro) {
		this.codigoParametro = codigoParametro;
	}

	public String getDescripcionParametro() {
		return descripcionParametro;
	}

	public void setDescripcionParametro(String descripcionParametro) {
		this.descripcionParametro = descripcionParametro;
	}

	public String getValorLetra() {
		return valorLetra;
	}

	public void setValorLetra(String valorLetra) {
		this.valorLetra = valorLetra;
	}

	public BigDecimal getValorNumerico() {
		return valorNumerico;
	}

	public void setValorNumerico(BigDecimal valorNumerico) {
		this.valorNumerico = valorNumerico;
	}

	public Date getValorFecha() {
		return valorFecha;
	}

	public void setValorFecha(Date valorFecha) {
		this.valorFecha = valorFecha;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idParametro != null ? idParametro.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ParametroDTO)) {
			return false;
		}
		ParametroDTO other = (ParametroDTO) object;
		if ((this.idParametro == null && other.idParametro != null)
				|| (this.idParametro != null && !this.idParametro.equals(other.idParametro))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.Paramnetro[ idParametro=" + idParametro + " ]";
	}

}
