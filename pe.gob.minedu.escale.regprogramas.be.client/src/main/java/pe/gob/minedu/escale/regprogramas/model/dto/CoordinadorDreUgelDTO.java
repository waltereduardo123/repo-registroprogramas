package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class CoordinadorDreUgelDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idCoord;

	private String usuarioCoordinador;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private DreUgelDTO dreUgelDTO;

	public CoordinadorDreUgelDTO() {
	}

	public CoordinadorDreUgelDTO(Long idCoord) {
		this.idCoord = idCoord;
	}

	public CoordinadorDreUgelDTO(Long idCoord, String usuarioCoordinador, Date fechaCreacion, String usuarioCreacion,
			Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado, DreUgelDTO dreUgelDTO) {
		super();
		this.idCoord = idCoord;
		this.usuarioCoordinador = usuarioCoordinador;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.dreUgelDTO = dreUgelDTO;
	}

	public Long getIdCoord() {
		return idCoord;
	}

	public void setIdCoord(Long idCoord) {
		this.idCoord = idCoord;
	}

	public String getUsuarioCoordinador() {
		return usuarioCoordinador;
	}

	public void setUsuarioCoordinador(String usuarioCoordinador) {
		this.usuarioCoordinador = usuarioCoordinador;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public DreUgelDTO getDreUgel() {
		return dreUgelDTO;
	}

	public void setDreUgel(DreUgelDTO dreUgelDTO) {
		this.dreUgelDTO = dreUgelDTO;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idCoord != null ? idCoord.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof CoordinadorDreUgelDTO)) {
			return false;
		}
		CoordinadorDreUgelDTO other = (CoordinadorDreUgelDTO) object;
		if ((this.idCoord == null && other.idCoord != null)
				|| (this.idCoord != null && !this.idCoord.equals(other.idCoord))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.CoordinadorDreUgelDTO[ idCoord=" + idCoord + " ]";
	}

}
