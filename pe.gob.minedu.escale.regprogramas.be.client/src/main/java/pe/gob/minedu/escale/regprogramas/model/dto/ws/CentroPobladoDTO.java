package pe.gob.minedu.escale.regprogramas.model.dto.ws;

public class CentroPobladoDTO {

	private String ubigeo;
	private String codcp;
	private String denominacion;
	private String longitud_dec;
	private String latitud_dec;
	private String area;
	private String area_sig;
	private String iddist;
	private String nomb_ubigeo;
	private String original;
	private String borrado;
	private String iddup;
	private String codcpdup;
	private String fecha;
	
	public String getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
	public String getCodcp() {
		return codcp;
	}
	public void setCodcp(String codcp) {
		this.codcp = codcp;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getLongitud_dec() {
		return longitud_dec;
	}
	public void setLongitud_dec(String longitud_dec) {
		this.longitud_dec = longitud_dec;
	}
	public String getLatitud_dec() {
		return latitud_dec;
	}
	public void setLatitud_dec(String latitud_dec) {
		this.latitud_dec = latitud_dec;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getArea_sig() {
		return area_sig;
	}
	public void setArea_sig(String area_sig) {
		this.area_sig = area_sig;
	}
	public String getIddist() {
		return iddist;
	}
	public void setIddist(String iddist) {
		this.iddist = iddist;
	}
	public String getNomb_ubigeo() {
		return nomb_ubigeo;
	}
	public void setNomb_ubigeo(String nomb_ubigeo) {
		this.nomb_ubigeo = nomb_ubigeo;
	}
	public String getOriginal() {
		return original;
	}
	public void setOriginal(String original) {
		this.original = original;
	}
	public String getBorrado() {
		return borrado;
	}
	public void setBorrado(String borrado) {
		this.borrado = borrado;
	}
	public String getIddup() {
		return iddup;
	}
	public void setIddup(String iddup) {
		this.iddup = iddup;
	}
	public String getCodcpdup() {
		return codcpdup;
	}
	public void setCodcpdup(String codcpdup) {
		this.codcpdup = codcpdup;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	
}
