package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class RevisionSolicitudCamposDTO extends DtoUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idSolcam;

	private String estadoEvaluacion;

	private String perfilRevisor;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioModificacion;

	private String estado;

	private SolicitudDTO solicitud;

	private CamposDTO campos;

	public RevisionSolicitudCamposDTO() {
	}

	public RevisionSolicitudCamposDTO(Long nIdSolcam) {
		this.idSolcam = nIdSolcam;
	}

	public RevisionSolicitudCamposDTO(Long idSolcam, Date dFeccre, String cUsucre, String cEstado) {
		this.idSolcam = idSolcam;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
	}

	public RevisionSolicitudCamposDTO(Long idSolicitud, Long idCampos, String estadoEvaluacion, Date dFeccre,
			String cUsucre, String cEstado) {
		this.solicitud = Objects.isNull(solicitud) ? new SolicitudDTO(idSolicitud) : solicitud;
		this.campos = Objects.isNull(campos) ? new CamposDTO(idCampos) : campos;
		this.estadoEvaluacion = estadoEvaluacion;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
	}

	public RevisionSolicitudCamposDTO(Long idSolicitud, Long idCampos, String estadoEvaluacion, Date dFeccre,
			String cUsucre, String cEstado, String perfilRevisor) {
		this.solicitud = Objects.isNull(solicitud) ? new SolicitudDTO(idSolicitud) : solicitud;
		this.campos = Objects.isNull(campos) ? new CamposDTO(idCampos) : campos;
		this.estadoEvaluacion = estadoEvaluacion;
		this.fechaCreacion = dFeccre;
		this.usuarioCreacion = cUsucre;
		this.estado = cEstado;
		this.perfilRevisor = perfilRevisor;
	}

	public Long getIdSolcam() {
		return idSolcam;
	}

	public void setIdSolcam(Long idSolcam) {
		this.idSolcam = idSolcam;
	}

	public String getEstadoEvaluacion() {
		return estadoEvaluacion;
	}

	public void setEstadoEvaluacion(String estadoEvaluacion) {
		this.estadoEvaluacion = estadoEvaluacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public CamposDTO getCampos() {
		return campos;
	}

	public void setCampos(CamposDTO campos) {
		this.campos = campos;
	}

	public String getPerfilRevisor() {
		return perfilRevisor;
	}

	public void setPerfilRevisor(String perfilRevisor) {
		this.perfilRevisor = perfilRevisor;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idSolcam != null ? idSolcam.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof RevisionSolicitudCamposDTO)) {
			return false;
		}
		RevisionSolicitudCamposDTO other = (RevisionSolicitudCamposDTO) object;
		if ((this.idSolcam == null && other.idSolcam != null)
				|| (this.idSolcam != null && !this.idSolcam.equals(other.idSolcam))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "pe.gob.minedu.escale.regprogramas.model.dto.RevisionSolicitudCamposDTO[ idSolcam=" + idSolcam + " ]";
	}

}
