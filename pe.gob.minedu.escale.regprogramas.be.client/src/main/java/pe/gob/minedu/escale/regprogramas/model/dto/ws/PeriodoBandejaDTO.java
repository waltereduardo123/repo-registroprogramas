package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PeriodoBandejaDTO implements Serializable  {
	private static final long serialVersionUID = -3109811418637526872L;
	
	private Integer idPeriodo;
	private String 	namePeriodo;
	private Date 	fechaInicioPeriodo;
	private Date 	fechaFinPeriodo;
	private Date 	fechaFinAnio;
	private String 	codGrupo;
	private String 	codItem;
	private String 	userCrea;
	private Date 	fecCrea;
	private String 	estado;
	
	public PeriodoBandejaDTO() 			{
		super();}


	
	

	public PeriodoBandejaDTO(Integer idPeriodo, String namePeriodo, Date fechaInicioPeriodo, Date fechaFinPeriodo,
			Date fechaFinAnio, String codGrupo, String codItem, String userCrea, Date fecCrea, String estado) {
		super();
		this.idPeriodo = idPeriodo;
		this.namePeriodo = namePeriodo;
		this.fechaInicioPeriodo = fechaInicioPeriodo;
		this.fechaFinPeriodo = fechaFinPeriodo;
		this.fechaFinAnio = fechaFinAnio;
		this.codGrupo = codGrupo;
		this.codItem = codItem;
		this.userCrea = userCrea;
		this.fecCrea = fecCrea;
		this.estado = estado;
	}





	public Integer getIdPeriodo() 																{		return idPeriodo;}
	public void setIdPeriodo(Integer idPeriodo) 												{		this.idPeriodo = idPeriodo;}
	public String getNamePeriodo() 																{		return namePeriodo;}
	public void setNamePeriodo(String namePeriodo) 												{		this.namePeriodo = namePeriodo;}
	public Date getFechaInicioPeriodo() 														{		return fechaInicioPeriodo;}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) 									{		this.fechaInicioPeriodo = fechaInicioPeriodo;}
	public Date getFechaFinPeriodo() 															{		return fechaFinPeriodo;}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) 										{		this.fechaFinPeriodo = fechaFinPeriodo;}
	public Date getFechaFinAnio() 																{		return fechaFinAnio;}
	public void setFechaFinAnio(Date fechaFinAnio) 												{		this.fechaFinAnio = fechaFinAnio;}
	public String getCodGrupo() 																{		return codGrupo;}
	public void setCodGrupo(String codGrupo) 													{		this.codGrupo = codGrupo;}
	public String getCodItem() 																	{		return codItem;}
	public void setCodItem(String codItem) 														{		this.codItem = codItem;}
	public String getUserCrea() 																{		return userCrea;}
	public void setUserCrea(String userCrea) 													{		this.userCrea = userCrea;}
	public Date getFecCrea() 																	{		return fecCrea;}
	public void setFecCrea(Date fecCrea) 														{		this.fecCrea = fecCrea;}
	public String getEstado() 																	{		return estado;}
	public void setEstado(String estado) 														{		this.estado = estado;}

	
}
