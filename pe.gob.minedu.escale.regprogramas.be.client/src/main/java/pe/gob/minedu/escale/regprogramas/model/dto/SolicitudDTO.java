package pe.gob.minedu.escale.regprogramas.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import pe.gob.minedu.escale.common.util.DtoUtil;

/**
 *
 * @author IMENDOZA
 */
@XmlRootElement
public class SolicitudDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idSolicitud;

	private String codSolicitud;

	private MaestroDTO tipoSolicitud;

	private String codigoModular;

	private MaestroDTO tipoPrograma;

	private String nombrePrograma;

	private BigDecimal latitudPrograma;

	private BigDecimal longitudPrograma;

	private MaestroDTO tipoGestion;

	private MaestroDTO tipoDependencia;

	private MaestroDTO tipoGestionEducativa;

	private MaestroDTO tipoTurno;

	private MaestroDTO tipoContinuidadJornadaEscolar;

	private MaestroDTO tipoVia;
	
	private String nombreVia;

	private String numeroVia;

	private String manzana;

	private String lote;

	private MaestroDTO tipoLocalidad;

	private String localidad;

	private String etapa;

	private String sector;

	private String zona;

	private String otraDireccion;

	private String referenciaDireccion;

	private String codigoCentroPoblado;

	private String nombreCentroPoblado;

	private String codigoArea;
	
	private String codigoAreaSig;

	private BigDecimal latitudCentroPoblado;

	private BigDecimal longitudCentroPoblado;

	private String codigoServicioEduMasCercano;

	private String nombreServicioEduMasCercano;

	private BigDecimal latitudServicioEduMasCercano;

	private BigDecimal longitudServicioEduMasCercano;

	private MaestroDTO tipoProveedorAgua;

	private String otroProveedorAgua;

	private String suministroAgua;

	private MaestroDTO tipoProveedorEnergia;

	private String otroProveedorEnergia;

	private String suministroEnergia;

	private MaestroDTO tipoSituacionSolicitud;

	private String geoHash;

	private String usuarioEnvio;

	private String nombreUsuarioEnvio;

	private Date fechaEnvio;

	private String usuarioModificacion;

	private String nombreUsuarioModificacion;

	private Date fechaModificacion;

	private String usuarioRevision;

	private String nombreUsuarioRevision;

	private Date fechaRevision;

	private String usuarioRevisionSig;

	private String nombreUsuarioRevisionSig;

	private Date fechaRevisionSig;

	private Date fechaAtencion;

	private MaestroDTO tipoSituacionRevision;

	private MaestroDTO tipoSituacionRevisionSig;

	private Date fechaCreacion;

	private String usuarioCreacion;

	private Date fechaUltimaModificacion;

	private String usuarioUltimaModificacion;

	private String estado;

	private MaestroDTO tipoSituacionPrograma;

	private List<SolicitudDocumentoDTO> listaSolicitudDocumento;

	private List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos;

	private DreUgelDTO dreUgel;

	private DistritosDTO distrito;

	private List<AccionSolicitudDTO> listaAccionSolicitud;

	private String indicadorUltimo;
	
	private MaestroDTO periodoRegistrado;
	
	private String correcto;  

	private int count;

	public SolicitudDTO() {
	}

	public SolicitudDTO(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	
	


	public SolicitudDTO(Long idSolicitud, String codSolicitud, MaestroDTO tipoSolicitud, String codigoModular,
			MaestroDTO tipoPrograma, String nombrePrograma, BigDecimal latitudPrograma, BigDecimal longitudPrograma,
			MaestroDTO tipoGestion, MaestroDTO tipoDependencia, MaestroDTO tipoGestionEducativa, MaestroDTO tipoTurno,
			MaestroDTO tipoContinuidadJornadaEscolar, MaestroDTO tipoVia, String nombreVia, String numeroVia,
			String manzana, String lote, MaestroDTO tipoLocalidad, String localidad, String etapa, String sector,
			String zona, String otraDireccion, String referenciaDireccion, String codigoCentroPoblado,
			String nombreCentroPoblado, String codigoArea, String codigoAreaSig, BigDecimal latitudCentroPoblado,
			BigDecimal longitudCentroPoblado, String codigoServicioEduMasCercano, String nombreServicioEduMasCercano,
			BigDecimal latitudServicioEduMasCercano, BigDecimal longitudServicioEduMasCercano,
			MaestroDTO tipoProveedorAgua, String otroProveedorAgua, String suministroAgua,
			MaestroDTO tipoProveedorEnergia, String otroProveedorEnergia, String suministroEnergia,
			MaestroDTO tipoSituacionSolicitud, String geoHash, String usuarioEnvio, String nombreUsuarioEnvio,
			Date fechaEnvio, String usuarioModificacion, String nombreUsuarioModificacion, Date fechaModificacion,
			String usuarioRevision, String nombreUsuarioRevision, Date fechaRevision, String usuarioRevisionSig,
			String nombreUsuarioRevisionSig, Date fechaRevisionSig, Date fechaAtencion,
			MaestroDTO tipoSituacionRevision, MaestroDTO tipoSituacionRevisionSig, Date fechaCreacion,
			String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String estado,
			MaestroDTO tipoSituacionPrograma, List<SolicitudDocumentoDTO> listaSolicitudDocumento,
			List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos, DreUgelDTO dreUgel, DistritosDTO distrito,
			List<AccionSolicitudDTO> listaAccionSolicitud, int count) {
		super();
		this.idSolicitud = idSolicitud;
		this.codSolicitud = codSolicitud;
		this.tipoSolicitud = tipoSolicitud;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.latitudPrograma = latitudPrograma;
		this.longitudPrograma = longitudPrograma;
		this.tipoGestion = tipoGestion;
		this.tipoDependencia = tipoDependencia;
		this.tipoGestionEducativa = tipoGestionEducativa;
		this.tipoTurno = tipoTurno;
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
		this.tipoVia = tipoVia;
		this.nombreVia = nombreVia;
		this.numeroVia = numeroVia;
		this.manzana = manzana;
		this.lote = lote;
		this.tipoLocalidad = tipoLocalidad;
		this.localidad = localidad;
		this.etapa = etapa;
		this.sector = sector;
		this.zona = zona;
		this.otraDireccion = otraDireccion;
		this.referenciaDireccion = referenciaDireccion;
		this.codigoCentroPoblado = codigoCentroPoblado;
		this.nombreCentroPoblado = nombreCentroPoblado;
		this.codigoArea = codigoArea;
		this.codigoAreaSig = codigoAreaSig;
		this.latitudCentroPoblado = latitudCentroPoblado;
		this.longitudCentroPoblado = longitudCentroPoblado;
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
		this.tipoProveedorAgua = tipoProveedorAgua;
		this.otroProveedorAgua = otroProveedorAgua;
		this.suministroAgua = suministroAgua;
		this.tipoProveedorEnergia = tipoProveedorEnergia;
		this.otroProveedorEnergia = otroProveedorEnergia;
		this.suministroEnergia = suministroEnergia;
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
		this.geoHash = geoHash;
		this.usuarioEnvio = usuarioEnvio;
		this.nombreUsuarioEnvio = nombreUsuarioEnvio;
		this.fechaEnvio = fechaEnvio;
		this.usuarioModificacion = usuarioModificacion;
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
		this.fechaModificacion = fechaModificacion;
		this.usuarioRevision = usuarioRevision;
		this.nombreUsuarioRevision = nombreUsuarioRevision;
		this.fechaRevision = fechaRevision;
		this.usuarioRevisionSig = usuarioRevisionSig;
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
		this.fechaRevisionSig = fechaRevisionSig;
		this.fechaAtencion = fechaAtencion;
		this.tipoSituacionRevision = tipoSituacionRevision;
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
		this.estado = estado;
		this.tipoSituacionPrograma = tipoSituacionPrograma;
		this.listaSolicitudDocumento = listaSolicitudDocumento;
		this.listaRevisionSolicitudCampos = listaRevisionSolicitudCampos;
		this.dreUgel = dreUgel;
		this.distrito = distrito;
		this.listaAccionSolicitud = listaAccionSolicitud;
		this.count = count;
	}

	public SolicitudDTO(Long idSolicitud, String codSolicitud, MaestroDTO tipoSolicitud, String codigoModular,
			MaestroDTO tipoPrograma, String nombrePrograma, Date fechaCreacion, String usuarioCreacion, String estado,
			DreUgelDTO dreUgel) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = codSolicitud;
		this.tipoSolicitud = tipoSolicitud;
		this.codigoModular = codigoModular;
		this.tipoPrograma = tipoPrograma;
		this.nombrePrograma = nombrePrograma;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
		this.dreUgel = dreUgel;
	}

	public SolicitudDTO(Long idSolicitud, String codSolicitud, String codigoModular, String nombrePrograma,
			Date fechaCreacion, String usuarioCreacion, String estado) {
		this.idSolicitud = idSolicitud;
		this.codSolicitud = codSolicitud;
		this.codigoModular = codigoModular;
		this.nombrePrograma = nombrePrograma;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.estado = estado;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getCodSolicitud() {
		return codSolicitud;
	}

	public void setCodSolicitud(String codSolicitud) {
		this.codSolicitud = codSolicitud;
	}

	public MaestroDTO getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(MaestroDTO tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public String getCodigoModular() {
		return codigoModular;
	}

	public void setCodigoModular(String codigoModular) {
		this.codigoModular = codigoModular;
	}

	public MaestroDTO getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(MaestroDTO tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public BigDecimal getLatitudPrograma() {
		return latitudPrograma;
	}

	public void setLatitudPrograma(BigDecimal latitudPrograma) {
		this.latitudPrograma = latitudPrograma;
	}

	public BigDecimal getLongitudPrograma() {
		return longitudPrograma;
	}

	public void setLongitudPrograma(BigDecimal longitudPrograma) {
		this.longitudPrograma = longitudPrograma;
	}

	public MaestroDTO getTipoGestion() {
		return tipoGestion;
	}

	public void setTipoGestion(MaestroDTO tipoGestion) {
		this.tipoGestion = tipoGestion;
	}

	public MaestroDTO getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(MaestroDTO tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}

	public MaestroDTO getTipoGestionEducativa() {
		return tipoGestionEducativa;
	}

	public void setTipoGestionEducativa(MaestroDTO tipoGestionEducativa) {
		this.tipoGestionEducativa = tipoGestionEducativa;
	}

	public MaestroDTO getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(MaestroDTO tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	public MaestroDTO getTipoContinuidadJornadaEscolar() {
		return tipoContinuidadJornadaEscolar;
	}

	public void setTipoContinuidadJornadaEscolar(MaestroDTO tipoContinuidadJornadaEscolar) {
		this.tipoContinuidadJornadaEscolar = tipoContinuidadJornadaEscolar;
	}

	public MaestroDTO getTipoVia() {
		return tipoVia;
	}

	public void setTipoVia(MaestroDTO tipoVia) {
		this.tipoVia = tipoVia;
	}

	public String getNombreVia() {
		return nombreVia;
	}

	public void setNombreVia(String nombreVia) {
		this.nombreVia = nombreVia;
	}

	public String getNumeroVia() {
		return numeroVia;
	}

	public void setNumeroVia(String numeroVia) {
		this.numeroVia = numeroVia;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public MaestroDTO getTipoLocalidad() {
		return tipoLocalidad;
	}

	public void setTipoLocalidad(MaestroDTO tipoLocalidad) {
		this.tipoLocalidad = tipoLocalidad;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getOtraDireccion() {
		return otraDireccion;
	}

	public void setOtraDireccion(String otraDireccion) {
		this.otraDireccion = otraDireccion;
	}

	public String getReferenciaDireccion() {
		return referenciaDireccion;
	}

	public void setReferenciaDireccion(String referenciaDireccion) {
		this.referenciaDireccion = referenciaDireccion;
	}

	public String getCodigoCentroPoblado() {
		return codigoCentroPoblado;
	}

	public void setCodigoCentroPoblado(String codigoCentroPoblado) {
		this.codigoCentroPoblado = codigoCentroPoblado;
	}

	public String getNombreCentroPoblado() {
		return nombreCentroPoblado;
	}

	public void setNombreCentroPoblado(String nombreCentroPoblado) {
		this.nombreCentroPoblado = nombreCentroPoblado;
	}

	public BigDecimal getLatitudCentroPoblado() {
		return latitudCentroPoblado;
	}

	public void setLatitudCentroPoblado(BigDecimal latitudCentroPoblado) {
		this.latitudCentroPoblado = latitudCentroPoblado;
	}

	public BigDecimal getLongitudCentroPoblado() {
		return longitudCentroPoblado;
	}

	public void setLongitudCentroPoblado(BigDecimal longitudCentroPoblado) {
		this.longitudCentroPoblado = longitudCentroPoblado;
	}

	public String getCodigoServicioEduMasCercano() {
		return codigoServicioEduMasCercano;
	}

	public void setCodigoServicioEduMasCercano(String codigoServicioEduMasCercano) {
		this.codigoServicioEduMasCercano = codigoServicioEduMasCercano;
	}

	public String getNombreServicioEduMasCercano() {
		return nombreServicioEduMasCercano;
	}

	public void setNombreServicioEduMasCercano(String nombreServicioEduMasCercano) {
		this.nombreServicioEduMasCercano = nombreServicioEduMasCercano;
	}

	public BigDecimal getLatitudServicioEduMasCercano() {
		return latitudServicioEduMasCercano;
	}

	public void setLatitudServicioEduMasCercano(BigDecimal latitudServicioEduMasCercano) {
		this.latitudServicioEduMasCercano = latitudServicioEduMasCercano;
	}

	public BigDecimal getLongitudServicioEduMasCercano() {
		return longitudServicioEduMasCercano;
	}

	public void setLongitudServicioEduMasCercano(BigDecimal longitudServicioEduMasCercano) {
		this.longitudServicioEduMasCercano = longitudServicioEduMasCercano;
	}

	public MaestroDTO getTipoProveedorAgua() {
		return tipoProveedorAgua;
	}

	public void setTipoProveedorAgua(MaestroDTO tipoProveedorAgua) {
		this.tipoProveedorAgua = tipoProveedorAgua;
	}

	public String getOtroProveedorAgua() {
		return otroProveedorAgua;
	}

	public void setOtroProveedorAgua(String otroProveedorAgua) {
		this.otroProveedorAgua = otroProveedorAgua;
	}

	public String getSuministroAgua() {
		return suministroAgua;
	}

	public void setSuministroAgua(String suministroAgua) {
		this.suministroAgua = suministroAgua;
	}

	public MaestroDTO getTipoProveedorEnergia() {
		return tipoProveedorEnergia;
	}

	public void setTipoProveedorEnergia(MaestroDTO tipoProveedorEnergia) {
		this.tipoProveedorEnergia = tipoProveedorEnergia;
	}

	public String getOtroProveedorEnergia() {
		return otroProveedorEnergia;
	}

	public void setOtroProveedorEnergia(String otroProveedorEnergia) {
		this.otroProveedorEnergia = otroProveedorEnergia;
	}

	public String getSuministroEnergia() {
		return suministroEnergia;
	}

	public void setSuministroEnergia(String suministroEnergia) {
		this.suministroEnergia = suministroEnergia;
	}

	public MaestroDTO getTipoSituacionSolicitud() {
		return tipoSituacionSolicitud;
	}

	public void setTipoSituacionSolicitud(MaestroDTO tipoSituacionSolicitud) {
		this.tipoSituacionSolicitud = tipoSituacionSolicitud;
	}

	public String getUsuarioEnvio() {
		return usuarioEnvio;
	}

	public void setUsuarioEnvio(String usuarioEnvio) {
		this.usuarioEnvio = usuarioEnvio;
	}

	public String getNombreUsuarioEnvio() {
		return nombreUsuarioEnvio;
	}

	public void setNombreUsuarioEnvio(String nombreUsuarioEnvio) {
		this.nombreUsuarioEnvio = nombreUsuarioEnvio;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getNombreUsuarioModificacion() {
		return nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioRevision() {
		return usuarioRevision;
	}

	public void setUsuarioRevision(String usuarioRevision) {
		this.usuarioRevision = usuarioRevision;
	}

	public String getNombreUsuarioRevision() {
		return nombreUsuarioRevision;
	}

	public void setNombreUsuarioRevision(String nombreUsuarioRevision) {
		this.nombreUsuarioRevision = nombreUsuarioRevision;
	}

	public Date getFechaRevision() {
		return fechaRevision;
	}

	public void setFechaRevision(Date fechaRevision) {
		this.fechaRevision = fechaRevision;
	}

	public String getUsuarioRevisionSig() {
		return usuarioRevisionSig;
	}

	public void setUsuarioRevisionSig(String usuarioRevisionSig) {
		this.usuarioRevisionSig = usuarioRevisionSig;
	}

	public String getNombreUsuarioRevisionSig() {
		return nombreUsuarioRevisionSig;
	}

	public void setNombreUsuarioRevisionSig(String nombreUsuarioRevisionSig) {
		this.nombreUsuarioRevisionSig = nombreUsuarioRevisionSig;
	}

	public Date getFechaRevisionSig() {
		return fechaRevisionSig;
	}

	public void setFechaRevisionSig(Date fechaRevisionSig) {
		this.fechaRevisionSig = fechaRevisionSig;
	}

	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getUsuarioUltimaModificacion() {
		return usuarioUltimaModificacion;
	}

	public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
		this.usuarioUltimaModificacion = usuarioUltimaModificacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<SolicitudDocumentoDTO> getListaSolicitudDocumento() {
		return listaSolicitudDocumento;
	}

	public void setListaSolicitudDocumento(List<SolicitudDocumentoDTO> listaSolicitudDocumento) {
		this.listaSolicitudDocumento = listaSolicitudDocumento;
	}

	public List<RevisionSolicitudCamposDTO> getListaRevisionSolicitudCampos() {
		return listaRevisionSolicitudCampos;
	}

	public void setListaRevisionSolicitudCampos(List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos) {
		this.listaRevisionSolicitudCampos = listaRevisionSolicitudCampos;
	}

	public DreUgelDTO getDreUgel() {
		return dreUgel;
	}

	public void setDreUgel(DreUgelDTO dreUgel) {
		this.dreUgel = dreUgel;
	}

	public DistritosDTO getDistrito() {
		return distrito;
	}

	public void setDistrito(DistritosDTO distrito) {
		this.distrito = distrito;
	}

	public List<AccionSolicitudDTO> getListaAccionSolicitud() {
		return listaAccionSolicitud;
	}

	public void setListaAccionSolicitud(List<AccionSolicitudDTO> listaAccionSolicitud) {
		this.listaAccionSolicitud = listaAccionSolicitud;
	}

	public String getGeoHash() {
		return geoHash;
	}

	public void setGeoHash(String geoHash) {
		this.geoHash = geoHash;
	}

	public String getCodigoArea() {
		return codigoArea;
	}

	public void setCodigoArea(String codigoArea) {
		this.codigoArea = codigoArea;
	}	

	public String getCodigoAreaSig() {
		return codigoAreaSig;
	}

	public void setCodigoAreaSig(String codigoAreaSig) {
		this.codigoAreaSig = codigoAreaSig;
	}

	public MaestroDTO getTipoSituacionPrograma() {
		return tipoSituacionPrograma;
	}

	public void setTipoSituacionPrograma(MaestroDTO tipoSituacionPrograma) {
		this.tipoSituacionPrograma = tipoSituacionPrograma;
	}

	public MaestroDTO getTipoSituacionRevision() {
		return tipoSituacionRevision;
	}

	public void setTipoSituacionRevision(MaestroDTO tipoSituacionRevision) {
		this.tipoSituacionRevision = tipoSituacionRevision;
	}

	public MaestroDTO getTipoSituacionRevisionSig() {
		return tipoSituacionRevisionSig;
	}

	public void setTipoSituacionRevisionSig(MaestroDTO tipoSituacionRevisionSig) {
		this.tipoSituacionRevisionSig = tipoSituacionRevisionSig;
	}

	public String getIndicadorUltimo() {
		return indicadorUltimo;
	}

	public void setIndicadorUltimo(String indicadorUltimo) {
		this.indicadorUltimo = indicadorUltimo;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	
	
	
	public MaestroDTO getPeriodoRegistrado() {
		return periodoRegistrado;
	}

	public void setPeriodoRegistrado(MaestroDTO periodoRegistrado) {
		this.periodoRegistrado = periodoRegistrado;
	}

	

	public String getCorrecto() {
		return correcto;
	}

	public void setCorrecto(String correcto) {
		this.correcto = correcto;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idSolicitud != null ? idSolicitud.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SolicitudDTO)) {
			return false;
		}
		SolicitudDTO other = (SolicitudDTO) object;
		if ((this.idSolicitud == null && other.idSolicitud != null)
				|| (this.idSolicitud != null && !this.idSolicitud.equals(other.idSolicitud))) {
			return false;
		}
		return true;
	}
//
////	@Override
//	public String toString() {
//		return "pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO[ idSolicitud=" + idSolicitud + " ]";
//	}

	@Override
	public String toString() {
		return "SolicitudDTO [idSolicitud=" + idSolicitud + ", codSolicitud=" + codSolicitud + ", tipoSolicitud="
				+ tipoSolicitud + ", codigoModular=" + codigoModular + ", tipoPrograma=" + tipoPrograma
				+ ", nombrePrograma=" + nombrePrograma + ", latitudPrograma=" + latitudPrograma + ", longitudPrograma="
				+ longitudPrograma + ", tipoGestion=" + tipoGestion + ", tipoDependencia=" + tipoDependencia
				+ ", tipoGestionEducativa=" + tipoGestionEducativa + ", tipoTurno=" + tipoTurno
				+ ", tipoContinuidadJornadaEscolar=" + tipoContinuidadJornadaEscolar + ", tipoVia=" + tipoVia
				+ ", nombreVia=" + nombreVia + ", numeroVia=" + numeroVia + ", manzana=" + manzana + ", lote=" + lote
				+ ", tipoLocalidad=" + tipoLocalidad + ", localidad=" + localidad + ", etapa=" + etapa + ", sector="
				+ sector + ", zona=" + zona + ", otraDireccion=" + otraDireccion + ", referenciaDireccion="
				+ referenciaDireccion + ", codigoCentroPoblado=" + codigoCentroPoblado + ", nombreCentroPoblado="
				+ nombreCentroPoblado + ", codigoArea=" + codigoArea + ", codigoAreaSig=" + codigoAreaSig
				+ ", latitudCentroPoblado=" + latitudCentroPoblado + ", longitudCentroPoblado=" + longitudCentroPoblado
				+ ", codigoServicioEduMasCercano=" + codigoServicioEduMasCercano + ", nombreServicioEduMasCercano="
				+ nombreServicioEduMasCercano + ", latitudServicioEduMasCercano=" + latitudServicioEduMasCercano
				+ ", longitudServicioEduMasCercano=" + longitudServicioEduMasCercano + ", tipoProveedorAgua="
				+ tipoProveedorAgua + ", otroProveedorAgua=" + otroProveedorAgua + ", suministroAgua=" + suministroAgua
				+ ", tipoProveedorEnergia=" + tipoProveedorEnergia + ", otroProveedorEnergia=" + otroProveedorEnergia
				+ ", suministroEnergia=" + suministroEnergia + ", tipoSituacionSolicitud=" + tipoSituacionSolicitud
				+ ", geoHash=" + geoHash + ", usuarioEnvio=" + usuarioEnvio + ", nombreUsuarioEnvio="
				+ nombreUsuarioEnvio + ", fechaEnvio=" + fechaEnvio + ", usuarioModificacion=" + usuarioModificacion
				+ ", nombreUsuarioModificacion=" + nombreUsuarioModificacion + ", fechaModificacion="
				+ fechaModificacion + ", usuarioRevision=" + usuarioRevision + ", nombreUsuarioRevision="
				+ nombreUsuarioRevision + ", fechaRevision=" + fechaRevision + ", usuarioRevisionSig="
				+ usuarioRevisionSig + ", nombreUsuarioRevisionSig=" + nombreUsuarioRevisionSig + ", fechaRevisionSig="
				+ fechaRevisionSig + ", fechaAtencion=" + fechaAtencion + ", tipoSituacionRevision="
				+ tipoSituacionRevision + ", tipoSituacionRevisionSig=" + tipoSituacionRevisionSig + ", fechaCreacion="
				+ fechaCreacion + ", usuarioCreacion=" + usuarioCreacion + ", fechaUltimaModificacion="
				+ fechaUltimaModificacion + ", usuarioUltimaModificacion=" + usuarioUltimaModificacion + ", estado="
				+ estado + ", tipoSituacionPrograma=" + tipoSituacionPrograma + ", listaSolicitudDocumento="
				+ listaSolicitudDocumento + ", listaRevisionSolicitudCampos=" + listaRevisionSolicitudCampos
				+ ", dreUgel=" + dreUgel + ", distrito=" + distrito + ", listaAccionSolicitud=" + listaAccionSolicitud
				+ ", indicadorUltimo=" + indicadorUltimo + ", count=" + count + "]";
	}
	
	

}
