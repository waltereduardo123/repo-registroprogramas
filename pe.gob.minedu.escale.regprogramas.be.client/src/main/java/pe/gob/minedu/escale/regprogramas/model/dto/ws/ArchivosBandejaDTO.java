package pe.gob.minedu.escale.regprogramas.model.dto.ws;

import java.io.Serializable;

import pe.gob.minedu.escale.common.util.DtoUtil;

public class ArchivosBandejaDTO extends DtoUtil implements Serializable {

	private static final long serialVersionUID = 787472052472534263L;

	private Long idArchivo;
	private String nombreArchivo;
	private String codigoArchivo;
	private String estado;

	public ArchivosBandejaDTO(Long idArchivo, String nombreArchivo, String codigoArchivo, String estado) {
		super();
		this.idArchivo = idArchivo;
		this.nombreArchivo = nombreArchivo;
		this.codigoArchivo = codigoArchivo;
		this.estado = estado;
	}

	public ArchivosBandejaDTO() {
		super();
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getCodigoArchivo() {
		return codigoArchivo;
	}

	public void setCodigoArchivo(String codigoArchivo) {
		this.codigoArchivo = codigoArchivo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
