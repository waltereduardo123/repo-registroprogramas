package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;

/**
 * Servlet implementation class inMoon
 */
@WebServlet("/svt/inMoon")
@MultipartConfig
public class inMoon extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String mensajeErrorArchivo  ="";
	private String mensaje = "";
	
	
	private Gson gson = new Gson();
	private ConcurrentHashMap<Object, Object> parametros;
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public inMoon() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Boolean sucess=true;
		parametros = new ConcurrentHashMap<>();
		
		try{
            String remoteAddress = request.getRemoteAddr();
            String forwardedFor = request.getHeader("X-Forwarded-For");
            String realIP = request.getHeader("X-Real-IP");
            String remoteHost = request.getRemoteHost();
            
        
            if( realIP == null )
                realIP = forwardedFor;
            if( realIP == null )
                realIP = remoteAddress;
            if( realIP == null )
                realIP = remoteHost;

	        
	      
			parametros.put("getIndex", true);
						
//			respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
			respuesta = new RespuestaDTO(sucess, "['']", parametros);

			response.addHeader("x-frame-options", "DENY");
			//response.addHeader("x-frame-options","SAMEORIGIN");
			response.addHeader("X-XSS-Protection", "1; mode=block");
			response.addHeader("X-Content-Type-Options", "nosniff");
						
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");	   
			response.getWriter().write(gson.toJson(respuesta).toString());
			
		}catch(Exception e){
			System.out.println("ERROR PROPAGADO--> " + e.getMessage() );
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
