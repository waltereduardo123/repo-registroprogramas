package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import pe.gob.minedu.escale.common.dto.rest.ReporteGenerico;
import pe.gob.minedu.escale.common.util.ExportExcelUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.ejb.service.ObjectServiceLocal;


/**
 * Servlet implementation class reportes
 */
@WebServlet("/svt/reporteGenerico")
public class ReportesGenericosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Gson gson = new Gson();

	@EJB
	private transient ObjectServiceLocal objectService = lookup(ObjectServiceLocal.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportesGenericosServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String consulta = (StringUtil.isNotNullOrBlank(request.getParameter("consultaJson")))
				? request.getParameter("consultaJson").toString() : null;

		ReporteGenerico reporteGenerico = gson.fromJson(consulta, ReporteGenerico.class);

		List<Object[]> lista = new ArrayList<Object[]>();

		String valor = StringUtil.isNotNullOrBlank(reporteGenerico.getConsultaJson().getGroupFilter())
				? reporteGenerico.getConsultaJson().getGroupFilter().toString() : "";

		lista = objectService.findByFirstMaxResultGenerico(reporteGenerico.getConsultaJson().obtenerFields(), valor,
				reporteGenerico.getConsultaJson().getTable(), reporteGenerico.getConsultaJson().getOrder(),
				reporteGenerico.getConsultaJson().getBatchSize(), reporteGenerico.getConsultaJson().getIndex());

		if (!lista.isEmpty()) {
			ExportExcelUtil export = new ExportExcelUtil(lista, reporteGenerico.getListaCabecera(),
					reporteGenerico.getNombreReporte(), response);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",
					"attachment; filename=" + reporteGenerico.getNombreReporte().toString() + ".xls");
			export.doExport(lista,"user");
			response = export.getResponse();

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			Context c = new InitialContext();
			return (T) c.lookup(
					"java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/" + nombreSimple.replace("Local", "")
							+ "!pe.gob.minedu.escale.regprogramas.ejb.service." + nombreSimple);
		} catch (NamingException ne) {
			throw new RuntimeException(ne);
		}
	}

}
