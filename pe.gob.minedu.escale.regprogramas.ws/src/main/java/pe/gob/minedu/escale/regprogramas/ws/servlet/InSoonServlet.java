package pe.gob.minedu.escale.regprogramas.ws.servlet;


import java.io.IOException;



import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;


import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.LogUtil;



@WebServlet("/svt/inSoon")
@MultipartConfig
public class InSoonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String mensajeErrorArchivo  ="";
	private String mensaje = "";
	
	
	private Gson gson = new Gson();
	private ConcurrentHashMap<Object, Object> parametros;

		
	private static LogUtil log = new LogUtil(InSoonServlet.class.getName());
	

	
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
    public InSoonServlet() {
        super();
    }

    @PostConstruct
	public void iniciar() {
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Boolean sucess=true;
		parametros = new ConcurrentHashMap<>();
//		Object nonce=request.getAttribute("FFL1");
		try{
            String remoteAddress = request.getRemoteAddr();
            String forwardedFor = request.getHeader("X-Forwarded-For");
            String realIP = request.getHeader("X-Real-IP");
            String remoteHost = request.getRemoteHost();
            
        
            if( realIP == null )
                realIP = forwardedFor;
            if( realIP == null )
                realIP = remoteAddress;
            if( realIP == null )
                realIP = remoteHost;

	        
	      
			parametros.put("getSoon", realIP);
						
//			respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
			respuesta = new RespuestaDTO(sucess, "['']", parametros);

//			response.addHeader("x-frame-options", "DENY");
//			//response.addHeader("x-frame-options","SAMEORIGIN");
//			response.addHeader("X-XSS-Protection", "1; mode=block");
//			response.addHeader("X-Content-Type-Options", "nosniff");
//			HttpSession ses=request.getSession();
//			ses.setAttribute("FFL1", nonce);
			
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");	   
			response.getWriter().write(gson.toJson(respuesta).toString());
			
		}catch(Exception e){
			System.out.println("ERROR PROPAGADO--> " + e.getMessage() );
			
		}
		
		
	}

	

	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				            
	}



	public String getMensajeErrorArchivo() {
		return mensajeErrorArchivo;
	}

	public void setMensajeErrorArchivo(String mensajeErrorArchivo) {
		this.mensajeErrorArchivo = mensajeErrorArchivo;
	}				
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	

	

}
