package pe.gob.minedu.escale.regprogramas.ws.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class TestUrl {
	
	public static void main(String[] args) {
			  try {
			
				  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				  SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
				  
				  Date dateMoment = formatter.parse("31/12/2018");
				  Date dateMoment1 = formatter1.parse("28/02/2019");
				  
		            System.out.println("IP OBTENIDO : " + calcularMesesAFecha(dateMoment,dateMoment1 ) );
		            
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

	public static int calcularMesesAFecha(Date fechaInicio, Date fechaFin) {
        try {
            //Fecha inicio en objeto Calendar
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(fechaInicio);
            //Fecha finalización en objeto Calendar
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(fechaFin);
            //Cálculo de meses para las fechas de inicio y finalización
            int startMes = (startCalendar.get(Calendar.YEAR) * 12) + startCalendar.get(Calendar.MONTH);
            int endMes = (endCalendar.get(Calendar.YEAR) * 12) + endCalendar.get(Calendar.MONTH);
            //Diferencia en meses entre las dos fechas
            int diffMonth = endMes - startMes;
            return diffMonth;
        } catch (Exception e) {
            return 0;
        }
 }

}
