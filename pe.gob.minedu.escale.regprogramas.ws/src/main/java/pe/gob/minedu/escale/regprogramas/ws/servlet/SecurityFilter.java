package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.joda.time.DateTime;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;


//import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
//import pe.gob.minedu.escale.adm.model.dto.RolDTO;
//import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.common.rest.auth.util.AuthUtils;



//@Priority(Priorities.AUTHENTICATION)
@Provider
public class SecurityFilter implements ContainerRequestFilter, ContainerResponseFilter {

	
//	@EJB
//	private transient AdministracionServiceLocal administracionServiceLocal = lookup(AdministracionServiceLocal.class);

	/** Used for Script Nonce */
	private SecureRandom prng = null;
	
	@SuppressWarnings("unused")
	private static final String EXPIRE_ERROR_MSG = "El token a expirado", JWT_ERROR_MSG = "No se puede analizar el JWT",
			JWT_INVALID_MSG = "Invalido JWT token";

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext response) throws IOException {
		
		try {
			this.prng = SecureRandom.getInstance("SHA1PRNG");
	
			// Add Script Nonce CSP Policy
			// --Generate a random number
		//String randomNum = new Integer(this.prng.nextInt()).toString();
		String randomNumext=Integer.toHexString(new Integer(this.prng.nextInt()));
		//String randomNum =new Double(this.prng.nextGaussian()).toString();	
		
		
		
		String csp =  "frame-src 'self' https://www.google.com/recaptcha/ ;"+
		"style-src 'self' 'unsafe-inline' ;"+
		"font-src 'self'; "+
		"img-src 'self'; "+
		"media-src; "+
		"base-uri 'none'; "+
		"default-src 'nonce-"+ randomNumext +"' 'unsafe-inline' 'strict-dynamic' https: http:; "+
		"report-uri *://*.google.com https://www.bing.com/api/maps/mapcontrol?branch=release https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "+
		"connect-src 'self' ; "+
		"object-src 'self' 'unsafe-inline' ; "+
		"script-src 'self' https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "
		;
				/*
				"default-src 'self';  font-src ; "+ 
				"frame-src; img-src; media-src; "+
				"object-src 'none'; base-uri 'none'; "+
				"report-uri https://www.google.com/recaptcha/api.js https://www.bing.com/api/maps/mapcontrol?branch=release https://ipinfo.io/json; "+ 
				"sandbox; script-src; "+
				"script-src 'strict-dynamic'"
				+"'nonce-"+ randomNumext +"'"
				+ "'unsafe-inline' http: https: http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc  https://ipinfo.io/json; "+
				"style-src; upgrade-insecure-requests; "+ 
				"connect-src 'none'; "+
				"upgrade-insecure-requests";*/
//	response.getHeaders().add( "X-FRAME-OPTIONS", "DENY" );
//	response.getHeaders().add( "X-Content-Type-Options", "nosniff" );
//	response.getHeaders().add( "Content-Security-Policy", "default-src 'self'; font-src; frame-src; img-src; media-src; object-src; report-uri; sandbox; script-src; style-src; upgrade-insecure-requests; connect-src 'none'; upgrade-insecure-requests" );
	response.getHeaders().add( "Content-Security-Policy", csp );
//	response.getHeaders().add( "X-XSS-Protection", "1; mode=block" );
	response.getHeaders().add( "Set-Cookie", "key=value; HttpOnly; SameSite=strict" );
		
	

	
	
	
	
	
	
//		SecurityContext originalContext = requestContext.getSecurityContext();
//		String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
//
//		if (authHeader == null || authHeader.isEmpty() || authHeader.split(" ").length != 2) {
////			Authorizer authorizer = new Authorizer(new ArrayList<RolDTO>(), "", originalContext.isSecure());
////			requestContext.setSecurityContext(authorizer);
//		} else {
//			JWTClaimsSet claimSet;
//			try {
//				claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
//				if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
//					response.setStatusInfo(StatusRegProgramas.TOKEN_EXPIRED);
////					throw new IOException(EXPIRE_ERROR_MSG);
//				}
//			} catch (ParseException e) {
//				response.setStatusInfo(StatusRegProgramas.TOKEN_CONFLICT);		
////				throw new IOException(JWT_ERROR_MSG);				
//			} catch (JOSEException e) {
////				response.setStatusInfo(Response.Status.FORBIDDEN);
////				throw new IOException(JWT_ERROR_MSG);
////				response.setStatusInfo(new CustomReasonPhraseExceptionStatusType(Response.Status.FORBIDDEN,"Token no válido"));
////				response.setStatus(Response.Status.FORBIDDEN.getStatusCode());
////				response.setEntity("Token no válido");
//
////				throw new IOException(JWT_INVALID_MSG);
////				response.setEntity(JWT_INVALID_MSG);
//				response.setStatusInfo(Response.Status.FORBIDDEN);
////				requestContext.abortWith(
////				Response.status(Response.Status.FORBIDDEN).
////			      build());
////				response.getHeaders().add("Access-Control-Allow-Origin", "*");
////				response.getHeaders().add("Access-Control-Allow-Credentials", "true");
////				response.getHeaders().add("Access-Control-Allow-Methods",
////		                "GET, POST, DELETE, PUT");
////				response.getHeaders().add("Access-Control-Allow-Headers",
////		                "Content-Type, Accept");
//			}
//
//			
//			// Asegúrese de que el token no ha expirado
////			if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
////				response.setStatusInfo(Response.Status.FORBIDDEN);
//////				throw new IOException(EXPIRE_ERROR_MSG);
////			} else {								
//			
////			}
//		}
////		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
////		response.getHeaders().putSingle("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
////		response.getHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type, Authorization");
	
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}

	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		@SuppressWarnings("unused")
		SecurityContext originalContext = requestContext.getSecurityContext();
		String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
//		Boolean prueba = originalContext.isSecure();
		
		if (authHeader == null || authHeader.isEmpty() || authHeader.split(" ").length != 2) {
			requestContext.abortWith(
					Response.status(Response.Status.FORBIDDEN).
					build());			
//			Authorizer authorizer = new Authorizer(new ArrayList<RolDTO>(), "", originalContext.isSecure());
//			requestContext.setSecurityContext(authorizer);
		} else {
			JWTClaimsSet claimSet;
			try {
				claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
				
			System.out.println("CLAIM OBTENIDO : " + claimSet.toJSONObject() );	
				
//				String bodyHeader = requestContext.getHeaderString(HttpHeaders.CONTENT_TYPE);
//				if (bodyHeader.equals("")) {
////					OutputStream stream = new LoggingStream(b, requestContext.getEntityStream());
//				}
//				
				if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
					requestContext.abortWith(
									Response.status(Response.Status.FORBIDDEN).
									build());
				}
			} catch (ParseException e) {
				requestContext.abortWith(
								Response.status(Response.Status.FORBIDDEN).
								build());			
			} catch (JOSEException e) {
				requestContext.abortWith(
								Response.status(Response.Status.FORBIDDEN).
								build());
			}
		}
	}


//	public static class Authorizer implements SecurityContext {
//
//		List<RolDTO> roles;// Muchos a muchos Roles - Usuarios
//		// Roles rol; //Uno a muchos Roles - Usuarios
//		String username;
//		boolean isSecure;
//
//		public Authorizer(List<RolDTO> roles, String username, boolean isSecure) {
//			this.roles = roles;
//			this.username = username;
//			this.isSecure = isSecure;
//		}
//
//		@Override
//		public Principal getUserPrincipal() {
//			return new User(username);
//		}
//
//		@Override
//		public boolean isUserInRole(String role) {
//			// return rol.equals(role); Uno a muchos
//			return roles.contains(new RolDTO(Long.valueOf(role)));// Muchos a muchos			
//		}
//
//		@Override
//		public boolean isSecure() {
//			return isSecure;
//		}
//
//		@Override
//		public String getAuthenticationScheme() {
//			return "JWT Authentication";
//		}
//	}
//
//	public static class User implements Principal {
//
//		String name;
//
//		public User(String name) {
//			this.name = name;
//		}
//
//		@Override
//		public String getName() {
//			return name;
//		}
//	}
//	
//	@SuppressWarnings({ "unchecked" })
//	private <T> T lookup(Class<T> clase) {
//		try {
//			String nombreSimple = clase.getSimpleName();
//			Context c = new InitialContext();
//			return (T) c.lookup(
//					"java:global/pe.gob.minedu.escale.adm.ws-1.0.0-PRO/" + nombreSimple.replace("Local", "")
//							+ "!pe.gob.minedu.escale.adm.ejb.service." + nombreSimple);
//		} catch (NamingException ne) {
//			throw new RuntimeException(ne);
//		}
//	}
}
