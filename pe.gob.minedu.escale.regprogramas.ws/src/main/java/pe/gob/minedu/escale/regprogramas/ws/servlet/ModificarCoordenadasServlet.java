package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.google.gson.Gson;
import pe.gob.minedu.escale.adm.business.type.TipoArchivoType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoResolucionType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.business.type.TipoEstadoProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionRevisionType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.business.exception.FormatoInvalidoException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.service.AuditoriaProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudCargaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;

/**
 * Servlet implementation class ModificarCoordenadasServlet
 */
@WebServlet("/svt/modificarCoordenadas")
@MultipartConfig
public class ModificarCoordenadasServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String mensajeErrorArchivo = "";
	private String mensaje = "";
	private String mensajePdf = "";

	private Map<Object, Object> parametros;
	private ConcurrentHashMap<Object, Object> paramsQuery;
	private Gson gson = new Gson();

	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;

	/** El objeto ErrorService. */
	private BaseBean baseBean;

	private String[] retornos;

	List<MaestroDTO> listaTipoSolicitud = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoSituacionSolicitud = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoSituacionRevision = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoArchivo = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoDocumento = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoDocumentoResolucion = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionPrograma;

	private List<MaestroDTO> listaTipoEstadoPrograma;

	@EJB
	private DataGeneralCache dataGeneralCache = lookup(DataGeneralCache.class);

	// @EJB
	// private transient SolicitudServiceLocal solicitudService =
	// lookup(SolicitudServiceLocal.class);

	@EJB
	private transient ProgramaServiceLocal programaService = lookup(ProgramaServiceLocal.class);

	@EJB
	private transient SolicitudCargaServiceLocal solicitudCargaService = lookup(SolicitudCargaServiceLocal.class);

	@EJB
	private transient SolicitudServiceLocal solicitudService = lookup(SolicitudServiceLocal.class);

	@EJB
	private AuditoriaProgramaServiceLocal auditoriaProgramaService = lookup(AuditoriaProgramaServiceLocal.class);

	private static final String SITU_PROG_CERRADO = "299";
	private static final String SITU_PROG_ENREVISION = "297";
	private static final String SITU_PROG_MODIFICADO = "298";
	private static final String COD_DREUGEL_PROGRAMA = "UE0001";

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	// private File destinationDir="D://files";
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModificarCoordenadasServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	@PostConstruct
	public void iniciar() {
		// listado necesarios para la carga

		listaTipoSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionRevision = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionRevisionType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey());// pendiente,
																										// revision,observado,aprobado(2804),anulado,sustentado
		listaTipoArchivo = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoArchivoType.CODIGO_AGRUPACION.getKey()); // pdf(1),doc,docx,xls,xlsx,ppt,pptx,zip,otro
		listaTipoDocumento = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoDocumentoType.CODIGO_AGRUPACION.getKey()); // resolucion,
																								// croquis,instructivo,documento
																								// ue(2904)
		listaTipoDocumentoResolucion = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoDocumentoResolucionType.CODIGO_AGRUPACION.getKey()); // resolucion,
																											// croquis,instructivo,documento
																											// ue(2904)
		listaTipoEstadoPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoEstadoProgramaType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionProgramaType.CODIGO_AGRUPACION.getKey());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Boolean sucess = null;
		request.setCharacterEncoding("UTF-8");
		parametros = new HashMap<>();
		List<ProgramaDTO> listPrograma = new ArrayList<ProgramaDTO>();
		String[] headsDocument = { "COD_MOD", "LATITUD", "LONGITUD", "CODCP", "AREA_2000", "AREA_SIG" };
		String[] headsDocumentTemp = new String[6];
		String[] headsDocumentTempMens = new String[6];
		paramsQuery = new ConcurrentHashMap<>();
		String extensionpdf = "pdf";
		boolean showResponseTexto = false;
		boolean showResponseExcel = false;
		List<String> listaFileNombreFinal = new ArrayList<String>();

		String menasejeError = null;
		// SolicitudDTO solicitud = new SolicitudDTO();
		DocumentoDTO documentoCarga = new DocumentoDTO();
		AuditoriaProgramaDTO aud = new AuditoriaProgramaDTO();
		boolean errorRow = false; 
		boolean errorRowCelda = false;
		boolean errorFileIncorrecto = false;
		boolean errorFileIncorrectoOtro = false;
		boolean errorFileXlsExtIncorrecto = false;
		Collection<Part> listRequest=null;
		HSSFWorkbook hssfWorkbook =null;
		boolean errorRowNull = false;
		HSSFRow rowj = null;
		try {
//			HSSFWorkbook hssfWorkbook = null;
			/* Obtener la lista de archivos */
				
		
				listRequest=request.getParts();	
		
			// listRequest.add(request.getParts());
			String[] fileNombre = null;
			InputStream fileItem = null;
//			HSSFWorkbook hssfWorkbook = null;
			HSSFSheet hssfSheet = null;
			HSSFCell hssfCellTmp = null;
			HSSFCell hssfCellTmpHeadMod = null;
			HSSFCell hssfCellTmpHeadLat = null;
			HSSFCell hssfCellTmpHeadLon = null;
			HSSFCell hssfCellTmpHeadCP = null;
			HSSFCell hssfCellTmpHeadA2 = null;
			HSSFCell hssfCellTmpHeadASig = null;
			HSSFCell hssfCellTmpMod = null;
			HSSFCell hssfCellTmpLat = null;
			HSSFCell hssfCellTmpLon = null;
			HSSFCell hssfCellTmpCP = null;
			HSSFCell hssfCellTmpA2 = null;
			HSSFCell hssfCellTmpASig = null;
			HSSFCell hssfCellResultado = null;
			int rows = 0;
			int filaInicio = 2;
			int numColum = 0;
//			HSSFRow rowj = null;
			int columnsObligaria = 6;
			String mensajeExcelError = "-";
			boolean iniciarProcesamiento = false;
			boolean isOkHead = false;
			HSSFRow row;
			HSSFCell cell;

			/*********************/
			boolean mensajeValExcel = false;
			List<Documento> documentos = null;
			
			for (Part part : listRequest) {
				// obtencion del name del Archivo y de la lista
				fileNombre = getFileName(part);
				if (StringUtil.isNotNullOrBlank(fileNombre) && fileNombre[0].contains(".xls")) {
					fileItem = part.getInputStream();
						
						hssfWorkbook = new HSSFWorkbook(fileItem);
					
				}
			}
		
				
			if (hssfWorkbook != null) {
//				hssfSheet = hssfWorkbook.getSheetAt(0);


				hssfSheet = hssfWorkbook.getSheet("coordenadas");
				// Obteenr el numero de columnas en la hoja
				row = hssfSheet.getRow(0);
				if (row != null) {
					short num = row.getLastCellNum();
					numColum = (int) num;

					rows = hssfSheet.getLastRowNum(); // numero de filas fisicas
														// de la hoja
//
//					for (int k = 0; k < headsDocument.length; k++) {
//						if (row.getCell(0) == null) {
//							errorRowCelda = true;
//						}
//						if (row.getCell(1) == null) {
//							errorRowCelda = true;
//						}
//						if (row.getCell(2) == null) {
//							errorRowCelda = true;
//						}
//						if (row.getCell(3) == null) {
//							errorRowCelda = true;
//						}
//						if (row.getCell(4) == null) {
//							errorRowCelda = true;
//						}
//						if (row.getCell(5) == null) {
////							errorRowCelda = true;
//							errorRowCelda = false;
//						}
//
//					}

					menasejeError = alertCellEmptyRow(hssfSheet, filaInicio, numColum, headsDocument,
							headsDocumentTempMens);

				} else {

					numColum = 0;
					errorRow = true;

				}
				// numColum=(int)hssfSheet.getRow(0).getLastCellNum();

				// short colum = hssfSheet.getRow(0).getLastCellNum();

				// int numColum = (int) hssfSheet.getRow(0).getLastCellNum();

				// rows = hssfSheet.getLastRowNum(); // numero de filas fisicas
				// de
				// // la hoja

				// menasejeError = alertCellEmptyRow(hssfSheet, filaInicio,
				// numColum);

				int contErr = 0;

				// obtener y validar los datos de la cabecera

				rowj = hssfSheet.getRow(0);
				hssfCellTmpHeadMod = rowj.getCell(0);
				hssfCellTmpHeadLat = rowj.getCell(1);
				hssfCellTmpHeadLon = rowj.getCell(2);
				hssfCellTmpHeadCP = rowj.getCell(3);
				hssfCellTmpHeadA2 = rowj.getCell(4);
				hssfCellTmpHeadASig = rowj.getCell(5);

				if (hssfCellTmpHeadMod != null && hssfCellTmpHeadLat != null && hssfCellTmpHeadLon != null
						&& hssfCellTmpHeadCP != null && hssfCellTmpHeadA2 != null && hssfCellTmpHeadASig != null) {
					String codModHead = hssfCellTmpHeadMod.getStringCellValue();
					String latHead = hssfCellTmpHeadLat.getStringCellValue();
					String lonHead = hssfCellTmpHeadLon.getStringCellValue();
					String codcpHead = hssfCellTmpHeadCP.getStringCellValue();
					String area2Head = hssfCellTmpHeadA2.getStringCellValue();
					String areaSigHead = hssfCellTmpHeadASig.getStringCellValue();

					for (int k = 0; k < headsDocumentTemp.length; k++) {
						headsDocumentTemp[0] = codModHead;
						headsDocumentTemp[1] = latHead;
						headsDocumentTemp[2] = lonHead;
						headsDocumentTemp[3] = codcpHead;
						headsDocumentTemp[4] = area2Head;
						headsDocumentTemp[5] = areaSigHead;
					}
					// headsDocumentTemp={codModHead,latHead,lonHead,codcpHead,area2Head,areaSigHead};

					// System.out.println("CABECERAS¨:"+codModHead+"//"+latHead+"//"+lonHead+"//"+codcpHead+"//"+area2Head+"//"+areaSigHead);

					

					// if(headsDocument.equals(headsDocumentTemp)){
					// isOkHead=true;
					// }

					for (int i = 0; i < headsDocument.length; i++) {
						// for(int j=0;j<headsDocumentTemp.length;j++){
						// temIput=headsDocumentTemp[i];
						if (headsDocument[i].equals(headsDocumentTemp[i])) {
							isOkHead = true;
							// break;
						} else {
							isOkHead = false;
							contErr++;
							// break;
						}
						// }

					}

				}

				if (contErr > 0) {
					// if(!isOkHead){
					menasejeError = "Existen columnas incorrectamente definidas, los columnas permitidas para el formato de carga de datos son: COD_MOD, LATITUD, LONGITUD, CODCP, AREA_2000, AREA_SIG ";
					showResponseExcel = false;

				}

				if (!StringUtils.isEmpty(menasejeError)) {
					// if(!isOkHead){
					// menasejeError="Existe una columna, mal definida,
					// verifique por favor.";
					// }

					// Hubo errores de forma la carga de archivos. Mostrar
					// advertencia alert
					sucess = true;
					parametros.put("hayErrorExcel", mensajeValExcel);
					parametros.put("mensajeVerificacion", menasejeError);
					respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
					showResponseTexto = true;
					showResponseExcel = false;
				}

				else

				{
					// Sin errores en la carga de archivos. Siempre descargar el
					// excel.
					// ---------------inicio tratamiento de los
					// pdf-------------------------------------------------------
					Properties propiedades = new Properties();
					InputStream in = getClass().getResourceAsStream(
							"/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
					propiedades.load(in);
					String pathFile = propiedades.getProperty("directorio.path");
					String codigoDocumento = "";
					String fecha = "";
					String diaMes = "";
					String organizacion = "";
					String prefijo = "";
					String nuevoNombre = "";
					int contFile = 1;
					in.close();
					String[] fileNombrePdf = null;
					String fileNombreFinal = null;
					/* INCIO Verificar archivo valido PDF */
					boolean isPDF = true;
					for (Part part : request.getParts()) {
						fileNombrePdf = getFileName(part);
						if (StringUtil.isNotNullOrBlank(fileNombrePdf) && fileNombrePdf[0].contains(".pdf")) {
							PDFParser parser = new PDFParser(part.getInputStream());
							try {
								parser.parse();
							} catch (IOException e) {
								isPDF = false;
								errorFileIncorrecto = true;
								throw new FormatoInvalidoException();// Abortar
																		// servlet
							}
						}
						if (!isPDF) {
							break;
						}

					}

					/* FIN Verificar archivo valido PDF */
					// Guardar Archivos
					for (Part part : request.getParts()) {
						fileNombrePdf = getFileName(part);

						if (StringUtil.isNotNullOrBlank(fileNombrePdf) && fileNombrePdf[0].contains(".pdf")) {

							codigoDocumento = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
									"ddMMyyyy");
							fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
									"yyyyMMddHHmmssSSS");
							diaMes = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM");
							organizacion = StringUtil.isNotNullOrBlank(request.getParameter("tipoOrganizacion"))
									? request.getParameter("tipoOrganizacion").toString() : null;
							prefijo = StringUtil.isNotNullOrBlank(request.getParameter("prefijo"))
									? request.getParameter("prefijo").toString() : null;
							nuevoNombre = pathFile + organizacion + "-" + prefijo + "-" + diaMes + "-" + codigoDocumento
									+ "-" + fecha + "-" + contFile + "." + extensionpdf;

							// fileNombreFinal =
							// fileNamePolicypdf.rename(fileNombrePdf);
							fileNombreFinal = nuevoNombre;
							part.write(fileNombreFinal);
							listaFileNombreFinal.add(fileNombreFinal);
							contFile++;

						}
					}

					// ---------------finaliza tratamiento de los
					// pdf-------------------------------------------------------
					// ---------------inicia registro de documento y
					// archivos-------------------------------------------------------
					documentoCarga.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
							.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.RESOLUCION.getKey().toString()))
							.findFirst().get().getIdMaestro()));
					documentoCarga.setTipoDocumentoResolucion(new MaestroDTO(listaTipoDocumentoResolucion.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoDocumentoResolucionType.OTROUE.getKey().toString()))
							.findFirst().get().getIdMaestro()));
					documentoCarga.setNroDocumento(
							FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM"));
					documentoCarga.setFechaDocumento(FechaUtil.obtenerFechaActual());
					documentoCarga.setCodigoDocumento(request.getParameter("tipoOrganizacion") + "-"
							+ request.getParameter("prefijo") + "-"
							+ FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM") + "-"
							+ FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMMyyyy"));
					documentoCarga.setFechaCreacion(FechaUtil.obtenerFechaActual());
					documentoCarga.setUsuarioCreacion("user");
					documentoCarga.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
					documentoCarga.setUsuarioUltimaModificacion("user");
					documentoCarga.setEstado("1");
					documentoCarga.setCodooii(request.getParameter("tipoOrganizacion"));
					documentoCarga.setIndCambioArchivos(
							(StringUtil.isNotNullOrBlank(request.getParameter("indCambioArchivo")))
									? request.getParameter("indCambioArchivo").toString()
									: EstadoState.INACTIVO.getValue());
					List<ArchivoDTO> listaArchivo = new ArrayList<ArchivoDTO>();
					for (String file : listaFileNombreFinal) {
						ArchivoDTO archivoCarga = new ArchivoDTO();
						char mander = file.charAt(file.length() - 5);
						switch (mander) {
						case 's':
							archivoCarga.setTipoArchivo(new MaestroDTO(listaTipoDocumento.stream()
									.filter(p -> p.getCodigoItem()
											.equals(TipoDocumentoType.DOCUMENTOUE.getKey().toString()))
									.findFirst().get().getIdMaestro()));
							break;
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
							archivoCarga.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
									.filter(p -> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString()))
									.findFirst().get().getIdMaestro()));
							break;
						default:
							break;
						}
						String nom = "";
						int startIndex = file.lastIndexOf('\\');
						int startIndexLinux = file.lastIndexOf('/');
						if (startIndex != -1) {
							nom = file.substring(startIndex + 1, file.length());
						}
						if (startIndexLinux != -1) {
							nom = file.substring(startIndexLinux + 1, file.length());
						}
						archivoCarga.setNombreArchivo(nom);
						archivoCarga.setCodigoArchivo(nom.substring(0, nom.length() - 4));
						archivoCarga.setFechaCreacion(FechaUtil.obtenerFechaActual());
						archivoCarga.setUsuarioCreacion("user");
						archivoCarga.setEstado(EstadoState.ACTIVO.getValue());
						listaArchivo.add(archivoCarga);
					}
					documentoCarga.setListaArchivo(listaArchivo);
					documentos = solicitudCargaService.registrarDocumentoArchivo(documentoCarga);
					// ---------------finaliza registro de documento y
					// archivos-------------------------------------------------------

					// Iniciamos el recorrido del excel y el procesamiento de la
					// información

					// ---------------recorrido del
					// workbook-------------------------------------------------------
					String codModular = "", centroPob, area2000, areaSIG = "";
					double latitudP, longitudP;
					for (int j = (filaInicio - 1); j <= rows; j++) {
						ProgramaDTO prBean = new ProgramaDTO();
						ProgramaDTO prBeanUpd = new ProgramaDTO();
						SolicitudDTO solicitud = new SolicitudDTO();
						AccionSolicitudDTO accionSolicitudDTO = new AccionSolicitudDTO();
						rowj = hssfSheet.getRow(j);
						System.out.println("Columnas rowj[]:" + rowj);
						for (int i = 0; i < columnsObligaria; i++) {
							// get Codigo Modular
							hssfCellTmpMod = rowj.getCell(0);
							// set necesarios para up del programa
							hssfCellTmpLat = rowj.getCell(1);
							hssfCellTmpLon = rowj.getCell(2);
							hssfCellTmpCP = rowj.getCell(3);
							hssfCellTmpA2 = rowj.getCell(4);
							hssfCellTmpASig = rowj.getCell(5);

						}

						// validar la columna de codigo modular, para numerico o
						// texto
						if (hssfCellTmpMod.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							codModular = String.valueOf((int) hssfCellTmpMod.getNumericCellValue());
						}
						if (hssfCellTmpMod.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							codModular = hssfCellTmpMod.getStringCellValue();
						}

						// set necesarios para up del programa
						latitudP = hssfCellTmpLat.getNumericCellValue();
						longitudP = hssfCellTmpLon.getNumericCellValue();
						centroPob = String.valueOf((int) hssfCellTmpCP.getNumericCellValue());
						area2000 = String.valueOf((int) hssfCellTmpA2.getNumericCellValue());
						if(hssfCellTmpASig!=null){
							areaSIG = String.valueOf((int) hssfCellTmpASig.getNumericCellValue());
						}else{
							areaSIG=null;
						}
						
						
						// BigDecimal.valueOf(Double.parseDouble(prBean.getLatitudServicioEduMasCercano().toEngineeringString()))
						prBeanUpd.setCodigoModular(codModular);
						prBeanUpd.setLatitudPrograma(BigDecimal.valueOf(latitudP));
						prBeanUpd.setLongitudPrograma(BigDecimal.valueOf(longitudP));
						prBeanUpd.setCodigoCentroPoblado(centroPob);
						prBeanUpd.setArea(area2000);
						prBeanUpd.setCodigoAreaSig(areaSIG);

						// buscamos en el programa, el codigo modular.
						try {
							prBean = programaService.buscarProgramaxCodigoModular(codModular.trim());
					

							if (Objects.nonNull(prBean)) { // codigo modular
															// registrado en el
															// programa
															// Setear los datos
															// del programa a la
															// solicitud
								solicitud.setCodSolicitud(FechaUtil.obtenerFechaFormatoPersonalizado(
										FechaUtil.obtenerFechaActual(), "yyyyMMddHHmmssSSS")
										+ ((StringUtil.isNotNullOrBlank(prBean.getDreUgel().getCodigo()))
												? prBean.getDreUgel().getCodigo().toString() : 0));
								solicitud.setDreUgel(prBean.getDreUgel());
								solicitud.setDistrito(prBean.getDistrito());
								solicitud.setTipoSolicitud(new MaestroDTO(listaTipoSolicitud.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSolicitudType.CARGA.getKey().toString()))
										.findFirst().get().getIdMaestro()));
								solicitud.setCodigoModular(prBean.getCodigoModular());
								solicitud.setTipoPrograma(prBean.getTipoPrograma());
								solicitud.setNombrePrograma(prBean.getNombrePrograma());
								solicitud.setLatitudPrograma(prBean.getLatitudPrograma());
								solicitud.setLongitudPrograma(prBean.getLongitudPrograma());
								solicitud.setTipoGestion(prBean.getTipoGestion());
								solicitud.setTipoDependencia(prBean.getTipoDependencia());
								solicitud.setTipoGestionEducativa(prBean.getTipoGestionEducativa());
								solicitud.setTipoTurno(prBean.getTipoTurno());
								solicitud.setTipoContinuidadJornadaEscolar(prBean.getTipoContinuidadJornadaEscolar());
								solicitud.setTipoVia(prBean.getTipoVia());
								solicitud.setNombreVia((StringUtil.isNotNullOrBlank(prBean.getNombreVia()))
										? prBean.getNombreVia().toString() : null);
								solicitud.setNumeroVia((StringUtil.isNotNullOrBlank(prBean.getNumeroVia()))
										? prBean.getNumeroVia().toString() : null);
								solicitud.setManzana((StringUtil.isNotNullOrBlank(prBean.getManzana()))
										? prBean.getManzana().toString() : null);
								solicitud.setLote((StringUtil.isNotNullOrBlank(prBean.getLote()))
										? prBean.getLote().toString() : null);
								solicitud.setTipoLocalidad(prBean.getTipoLocalidad());
								solicitud.setLocalidad(prBean.getLocalidad());
								solicitud.setEtapa((StringUtil.isNotNullOrBlank(prBean.getEtapa()))
										? prBean.getEtapa().toString() : null);
								solicitud.setSector((StringUtil.isNotNullOrBlank(prBean.getSector()))
										? prBean.getSector().toString() : null);
								solicitud.setZona((StringUtil.isNotNullOrBlank(prBean.getZona()))
										? prBean.getZona().toString() : null);
								solicitud.setOtraDireccion((StringUtil.isNotNullOrBlank(prBean.getOtraDireccion()))
										? prBean.getOtraDireccion().toString() : null);
								solicitud.setReferenciaDireccion(prBean.getReferenciaDireccion());
								solicitud.setCodigoCentroPoblado(prBean.getCodigoCentroPoblado());
								solicitud.setNombreCentroPoblado(prBean.getNombreCentroPoblado());
								solicitud.setCodigoArea(prBean.getArea());
								solicitud.setCodigoAreaSig(prBean.getCodigoAreaSig());
								solicitud.setLatitudCentroPoblado(prBean.getLatitudCentroPoblado());
								solicitud.setLongitudCentroPoblado(prBean.getLongitudCentroPoblado());
								solicitud.setCodigoServicioEduMasCercano(
										(StringUtil.isNotNullOrBlank(prBean.getCodigoServicioEduMasCercano()))
												? prBean.getCodigoServicioEduMasCercano().toString() : null);
								solicitud.setNombreServicioEduMasCercano(
										(StringUtil.isNotNullOrBlank(prBean.getNombreServicioEduMasCercano()))
												? prBean.getNombreServicioEduMasCercano().toString() : null);
								solicitud.setLatitudServicioEduMasCercano(
										(StringUtil.isNotNullOrBlank(prBean.getLatitudServicioEduMasCercano()))
												? (BigDecimal.valueOf(Double.parseDouble(prBean
														.getLatitudServicioEduMasCercano().toEngineeringString())))
												: null);
								solicitud.setLongitudServicioEduMasCercano(
										(StringUtil.isNotNullOrBlank(prBean.getLongitudServicioEduMasCercano()))
												? (BigDecimal.valueOf(Double.parseDouble(prBean
														.getLongitudServicioEduMasCercano().toEngineeringString())))
												: null);
								solicitud.setTipoProveedorAgua(prBean.getTipoProveedorAgua());
								solicitud.setOtroProveedorAgua(
										(StringUtil.isNotNullOrBlank(prBean.getOtroProveedorAgua()))
												? prBean.getOtroProveedorAgua().toString() : null);
								solicitud.setSuministroAgua((StringUtil.isNotNullOrBlank(prBean.getSuministroAgua()))
										? prBean.getSuministroAgua().toString() : null);
								solicitud.setTipoProveedorEnergia(prBean.getTipoProveedorEnergia());
								solicitud.setOtroProveedorEnergia(
										(StringUtil.isNotNullOrBlank(prBean.getOtroProveedorEnergia()))
												? prBean.getOtroProveedorEnergia().toString() : null);
								solicitud.setSuministroEnergia(
										(StringUtil.isNotNullOrBlank(prBean.getSuministroEnergia()))
												? prBean.getSuministroEnergia().toString() : null);
								solicitud.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionSolicitudType.APROBADO.getKey().toString()))
										.findFirst().get().getIdMaestro()));
								solicitud.setGeoHash(prBean.getGeoHash());
								solicitud.setUsuarioEnvio(
										(StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
												? request.getParameter("usuarioCreacion").toString() : null);
								solicitud.setNombreUsuarioEnvio(
										(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevision")))
												? request.getParameter("nombreUsuarioRevision").toString() : null);
								solicitud.setFechaEnvio(FechaUtil.obtenerFechaActual());
								solicitud.setUsuarioModificacion(request.getParameter("usuarioCreacion"));
								solicitud.setNombreUsuarioModificacion(request.getParameter("nombreUsuarioRevision"));
								solicitud.setFechaModificacion(FechaUtil.obtenerFechaActual());
								solicitud.setUsuarioRevision(
										(StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
												? request.getParameter("usuarioCreacion").toString() : null);
								solicitud.setNombreUsuarioRevision(
										(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevision")))
												? request.getParameter("nombreUsuarioRevision").toString() : null);
								solicitud.setUsuarioRevisionSig(
										(StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
												? request.getParameter("usuarioCreacion").toString() : null);
								solicitud.setNombreUsuarioRevisionSig(
										(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevision")))
												? request.getParameter("nombreUsuarioRevision").toString() : null);
								solicitud.setFechaRevisionSig(FechaUtil.obtenerFechaActual());
								solicitud.setFechaAtencion(FechaUtil.obtenerFechaActual());
								solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionRevisionType.APROBADO.getKey().toString()))
										.findFirst().get().getIdMaestro()));
								solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionRevisionType.APROBADO.getKey().toString()))
										.findFirst().get().getIdMaestro()));
								solicitud.setFechaCreacion(FechaUtil.obtenerFechaActual());
								solicitud.setUsuarioCreacion(
										(StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
												? request.getParameter("usuarioCreacion").toString() : null);
								solicitud.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
								solicitud.setUsuarioUltimaModificacion(
										(StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
												? request.getParameter("usuarioCreacion").toString() : null);

								// solicitud.setEstado(prBean.getEstado());

								solicitud.setEstado(EstadoState.ACTIVO.getValue());

								solicitud.setTipoSituacionPrograma(prBean.getTipoSituacion());
								solicitud.setIndicadorUltimo(EstadoState.ACTIVO.getValue());
								// Las acciones de la solicitud
								accionSolicitudDTO.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud
										.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionSolicitudType.APROBADO.getKey().toString()))
										.findFirst().get().getIdMaestro()));// APROBADO
								accionSolicitudDTO.setFechaCreacion(FechaUtil.obtenerFechaActual());
								accionSolicitudDTO.setUsuarioCreacion(
										(StringUtil.isNotNullOrBlank("admin")) ? "admin".toString() : null);
								accionSolicitudDTO.setEstado(EstadoState.ACTIVO.getValue());
								List<AccionSolicitudDTO> listaAccionSolicitudDTO = new ArrayList<AccionSolicitudDTO>();
								listaAccionSolicitudDTO.add(accionSolicitudDTO);
								solicitud.setListaAccionSolicitud(listaAccionSolicitudDTO);
								paramsQuery.clear();
								paramsQuery.put("codigoModular", prBean.getCodigoModular().toString());
								listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0,
										paramsQuery);
								
								if (listPrograma != null) {
									if (!listPrograma.isEmpty()) {
										for (ProgramaDTO programa : listPrograma) {
											if (StringUtil
													.isNotNullOrBlank(programa.getTipoSituacion().getIdMaestro())) {
												// validar situacion del
												// programa revisado
												mensajeExcelError = mensajeSituacionPrograma(
														String.valueOf(programa.getTipoSituacion().getIdMaestro()));
												if (mensajeExcelError.equals("true".trim())) {
													mensajeExcelError = "Actualizado correctamente.";
													iniciarProcesamiento = true;
												} else {
													iniciarProcesamiento = false;
												}

											}
										}
									}
								}

								// incrementar el Workbook

								rowj = hssfSheet.getRow(0);
								rowj.createCell(columnsObligaria);
								hssfCellResultado = rowj.getCell(6);
								hssfCellResultado.setCellValue("RESULTADO");

								for (int p = (filaInicio - 1); p <= rows; p++) {
									// En la fila j, verificar si hay nulos
									rowj = hssfSheet.getRow(j);
									for (int i = 0; i <= columnsObligaria; i++) {
										hssfCellTmp = rowj.createCell(columnsObligaria);
										if (i == columnsObligaria) {
											hssfCellTmp.setCellValue(mensajeExcelError);
										}
									}

								}

								showResponseExcel = true;// Siempre se descarga
								// el excel

								// Se inicia el procesamiento.
								if (iniciarProcesamiento) {
									// Registro de la solicitud
									solicitud = solicitudCargaService.registrarSolicitudCargaExc(solicitud, documentos);
									System.out.println("solicitud--> " + solicitud );
									// actualizar el programa
									prBeanUpd.setIdPrograma(prBean.getIdPrograma());
									prBeanUpd.setDreUgel(prBean.getDreUgel());// codigoDocumento
									// prBeanUpd.setDreUgel(new
									// DreUgelDTO(COD_DREUGEL_PROGRAMA.trim()));
									prBeanUpd.setTipoResolucion(new MaestroDTO(listaTipoDocumentoResolucion.stream()
											.filter(p -> p.getCodigoItem()
													.equals(TipoDocumentoResolucionType.OTROUE.getKey().toString()))
											.findFirst().get().getIdMaestro()));
									prBeanUpd.setNumeroDocumento(FechaUtil
											.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM"));
									prBeanUpd.setFechaDocumento(FechaUtil.obtenerFechaActual());
									prBeanUpd.setCodigoDocumento(request.getParameter("tipoOrganizacion") + "-"
											+ request.getParameter("prefijo") + "-"
											+ FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
													"ddMM")
											+ "-" + FechaUtil.obtenerFechaFormatoPersonalizado(
													FechaUtil.obtenerFechaActual(), "ddMMyyyy"));

									/*
									 * prBeanUpd.setTipoEstado(new
									 * MaestroDTO(listaTipoEstadoPrograma.stream
									 * () .filter(p-> p.getCodigoItem().equals(
									 * TipoEstadoProgramaType.ACTIVO.getKey()))
									 * .findFirst() .get().getIdMaestro()));
									 */

									prBeanUpd.setTipoSituacion(prBean.getTipoSituacion());
									// prBeanUpd.setTipoSituacion(new
									// MaestroDTO(Long.parseLong(SITU_PROG_MODIFICADO.trim())));
									// //Se comenta por inconsistencia con el
									// reinicio de los programas werr
									/*
									 * prBeanUpd.setTipoSituacion(new
									 * MaestroDTO(listaTipoSituacionPrograma.
									 * stream() .filter(p->
									 * p.getCodigoItem().equals(
									 * TipoSituacionProgramaType.MODIFICADO.
									 * getKey())) .findFirst() .get()
									 * .getIdMaestro())); //Se comenta por
									 * inconsistencia con el reinicio de los
									 * programas werr
									 * 
									 */

									prBeanUpd.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
									prBeanUpd.setUsuarioUltimaModificacion(
											(StringUtil.isNotNullOrBlank(request.getParameter("usuarioRevision")))
													? request.getParameter("usuarioRevision").toString() : null);

									prBeanUpd = programaService.actualizar(prBeanUpd);
									System.out.println("prBeanUpd-->" +prBeanUpd );
									// registrar la auditoria del programa
									// prBeanAud =
									// programaService.buscarProgramaxCodigoModular(solicitud.getCodigoModular());
									// ConversorHelper.fusionaPropiedades(prBeanAud,
									// aud);
									ConversorHelper.fusionaPropiedades(prBean, aud);
									aud.setIdPrograma(null);
									aud = auditoriaProgramaService.registrarProgramaNuevo(aud);
									System.out.println("aud--> " + aud);
								}
							}
						} catch (Exception e) {
						
							
							iniciarProcesamiento = false;
							mensajeExcelError = "";
							for (int p = (filaInicio - 1); p <= rows; p++) {
								// En la fila j, verificar si hay nulos
								rowj = hssfSheet.getRow(j);
								for (int i = 0; i <= columnsObligaria; i++) {
									hssfCellTmp = rowj.createCell(columnsObligaria);
									if (i == columnsObligaria) {
										// hssfCellTmp.setCellValue("No se
										// actualizo. Código Modular no
										// Existe.");
										hssfCellTmp.setCellValue("El valor del campo COD_MOD no existe.");
									}
								}

							}
							/////************//////////////////////////AGREGADO////////////////////////////////////////////
							sucess = true;
							parametros.put("hayErrorExcel", mensajeValExcel);
							parametros.put("mensajeVerificacion", "El valor del campo COD_MOD no existe");
							respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

							/////************//////////////////////////////////////////////////////////////////////
							
						}

					}

				}

			} 
			else {
		
				errorFileXlsExtIncorrecto=true;
				showResponseExcel=false;
				showResponseTexto=false;
				
				throw new NullPointerException();

				
				
				
			
				
////				showResponseExcel = false;
//				errorFileIncorrectoOtro=true;
//				System.out.println("EL WORKBOOK ES NULL");
//				sucess = true;
//				parametros.put("hayErrorExcel", "sadasd");
//				parametros.put("mensajeVerificacion", "En el registro Archivo XLS se debe adjuntar archivo de extensión XLS");
			
//				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
				
//				errorFileXlsExtIncorrecto=true;

				
		}
			
		
			//////////////////////////////////////////////////////////////////////////
	//	}	

			if (showResponseExcel) {
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-disposition", "attachment; filename=test.xls");
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSEXCEL.getKey().toString()));

				OutputStream output = response.getOutputStream();

				hssfWorkbook.write(output);
				output.flush();
				output.close();

			}

			if (showResponseTexto) {

				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				//response.getWriter().write(gson.toJson(respuesta).toString()); //////////////////////////////////////////// CAMBIO ////////////
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSTEXTO.getKey().toString()));
			}
		/////************//////////////////////////AGREGADO////////////////////////////////////////////
			if(!iniciarProcesamiento){
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				response.getWriter().write(gson.toJson(respuesta).toString());
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSTEXTO.getKey().toString()));
			}
		/////************//////////////////////////////////////////////////////////////////////
			
			
//			/////////////////////////////////////////////////////////////////////////
//			if(errorFileXlsExtIncorrecto){
//				response.setCharacterEncoding("UTF-8");
//				response.setContentType("text/html; charset=UTF-8");
//				response.getWriter().write(gson.toJson(respuesta).toString());
//				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSDOCTIPOXLS.getKey().toString()));
//				
//			} 
//			//////////////////////////////////////////////////////////////////////////
		
		} catch (Exception e) {
			System.out.println("ERROR--> " + e.getMessage() );
			if(listRequest==null||listRequest.size()<=0){
				errorFileIncorrectoOtro = true;
				
			}
			if(hssfWorkbook==null){
				showResponseExcel = false;
				errorRowNull=false;
				errorRowCelda=false;
				errorRow=false;
			}
			if(rowj==null){
				errorRowNull=true;
				errorRowCelda=false;
				errorRow=false;
			}
			if (errorFileIncorrecto) {
				errorRowCelda = false;
				// mensajePdf = getBaseBean().getErrorService().getErrorFor(e,
				// ResourceUtil.obtenerLocaleSession()).getDefaultMessage();//////////////////
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html");
				sucess = true;
				parametros.put("hayErrorPdf", true);
				parametros.put("mensajeVerificacion", "El archivo PDF que intenta subir es incorrecto"); // inv\u00e1lido
				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

				response.getWriter().write(gson.toJson(respuesta).toString());
				// response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSDOCTIPO.getKey().toString()));
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSDOCTIPO.getKey().toString()));
			} else {
				System.out.println("errorFileIncorrecto -->" +errorFileIncorrecto );
			}
			
			if(errorFileXlsExtIncorrecto){
				showResponseExcel=true;
				showResponseTexto=false;
				errorFileIncorrectoOtro=false;
				errorFileIncorrecto=false;
				errorRowCelda=false;
				errorRow=false;
				errorRowNull=false;
				
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html");
				sucess = true;
				parametros.put("hayErrorExcel", true);
				parametros.put("mensajeVerificacion", "En el registro Archivo XLS se debe adjuntar archivo de extensión XLS");
				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
				
				response.getWriter().write(gson.toJson(respuesta).toString());
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSDOCTIPOXLS.getKey().toString()));
				
			}  else {
				System.out.println("errorFileIncorrecto -->" +errorFileXlsExtIncorrecto );
			}

			if (errorRow) {  
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				sucess = true;
				parametros.put("hayErrorExcel", true);
				parametros.put("mensajeVerificacion", "El archivo subido no coincide con el formato de carga de datos");
				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

				response.getWriter().write(gson.toJson(respuesta).toString());
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSTEXTO.getKey().toString()));
			} else {
				System.out.println("errorRow-->" +errorRow );
			}
			if (errorRowCelda) {
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				sucess = true;
				parametros.put("hayErrorExcel", true);
				parametros.put("mensajeVerificacion", "El archivo subido no coincide con el formato de carga de datos");
				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

				response.getWriter().write(gson.toJson(respuesta).toString());
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSTEXTO.getKey().toString()));
			} else {
				System.out.println("errorRowCelda -->" +errorRowCelda );
			}
			if(errorFileIncorrectoOtro||!showResponseExcel||errorRowNull){
				errorRowCelda = false;
				// mensajePdf = getBaseBean().getErrorService().getErrorFor(e,
				// ResourceUtil.obtenerLocaleSession()).getDefaultMessage();//////////////////
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				sucess = true;
				parametros.put("hayErrorExcel", true);
				parametros.put("mensajeVerificacionOtro", "El archivo excel que intenta subir es incorrecto, no cumple con el formato definido"); // inv\u00e1lido
				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

				response.getWriter().write(gson.toJson(respuesta).toString());
				// response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSDOCTIPO.getKey().toString()));
				response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSDOCTIPOOTRO.getKey().toString()));
			}
			else {
				System.out.println("errorFileIncorrectoOtro showResponseExcel errorRowNull--> " + errorFileIncorrectoOtro+ "//" + showResponseExcel + "//" + errorRowNull );
			}
		}

	}

	private String mensajeSituacionPrograma(String idProgramaMaestro) {

		String mensajeMostrar = "true";

		// se obtiene el mensaje a mostrar, según la situacion del programa en
		// el maestro
		// if(idProgramaMaestro.equals(SITU_PROG_CERRADO.trim())){
		// mensajeMostrar="No se actualiza el programa inactivo.";
		// }
		if (idProgramaMaestro.equals(SITU_PROG_ENREVISION.trim())) {
			// mensajeMostrar="No se actualizo, por solicitud en proceso.";
			mensajeMostrar = "No se actualizó, hay una solicitud en proceso.";
		}
		if (idProgramaMaestro.equals(SITU_PROG_CERRADO.trim())) {
			// mensajeMostrar="No se actualiza el programa inactivo.";
			mensajeMostrar = "No se actualizó el programa se encuentra inactivo.";
		}
		return mensajeMostrar.trim();
	}

	private String[] getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				retornos = new String[2];
				retornos[0] = token.substring(token.indexOf("=") + 2, token.length() - 1);
				retornos[1] = part.getName();
				if (StringUtil.isNotNullOrBlank(retornos[0])) {
					return retornos;
				}
			}
		}
		return null;
	}

	private boolean checkIfRowIsEmpty(HSSFRow row) {
		if (row == null) {
			return true;
		}
		if (row.getLastCellNum() <= 0) {
			return true;
		}
		for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
			HSSFCell cell = row.getCell(cellNum);
			if (cell != null && cell.getCellType() != HSSFCell.CELL_TYPE_BLANK
					&& StringUtil.isNotNullOrBlank(cell.toString())) {
				return false;
			}
		}
		return true;
	}

	// verificar si existen campos vacios dentro de la filas obligatoria.
	private String alertCellEmptyRow(HSSFSheet sheet, int filaInicio, int numColumnas0, String[] headsDocument,
			String[] headsDocumentTempMens) {
		String mensajeError = "";
		HSSFRow rowj = null;
		HSSFCell cell = null;
//		int columnsObligaria = 5;
		int columnsObligaria = 6;
		boolean esvacio = false;
		int typeDatoCell;
		double valTypeDatoCell;
		String valTypeDatoCellSt;
//		boolean numColumnasHead = true;
		boolean numColumnasHead = false;
		boolean numColumnasHeadCero = true;
		boolean isOkHead = false;
		int contErr = 0;
		Pattern pattern1;
		Matcher matcher1;
		boolean contrCMod=false;
		// if(numColumnas0<6){
		if (numColumnas0 < 7 && numColumnas0 > 0) {
			// verificar los encabezados 
//			for (int i = 0; i < headsDocument.length; i++) {
//				// for(int j=0;j<headsDocumentTemp.length;j++){
//				// temIput=headsDocumentTemp[i];
//				if (headsDocument[i].equals(headsDocumentTempMens[i])) {
//					isOkHead = true;
//					// break;
//				} else {
//					isOkHead = false;
////					contErr++;
//					// break;
//				}
//				// }
//
//			}

			numColumnasHead = true;
			// if(contErr==0){
			// numColumnasHead = false;
			// }

		}
		if (numColumnas0 == 0) {
			numColumnasHeadCero = false;
		}
		if (numColumnasHead) {

			int rows = sheet.getLastRowNum(); // cantidad de filas. Incluida las
												// que estan en blanco
			// recorrer a partir de la segunda fila del excel. No considerar las
			// filas vacias
//			for (int j = (filaInicio - 1); j < rows; j++) {
			for (int j = (filaInicio - 1); j <= rows ; j++) {
			// En la fila j, verificar si hay nulos
				esvacio = false;
				rowj = sheet.getRow(j);
				if (!checkIfRowIsEmpty(rowj)) {
					for (int i = 0; i < columnsObligaria; i++) {
						cell = rowj.getCell(i);

						if (cell == null || cell.getCellType() == HSSFCell.CELL_TYPE_BLANK
								|| StringUtil.isEmptyString(cell.toString())) {
								if(i==5){
									esvacio = false;
								}else{
									esvacio = true;
								}
							break;
						} else {
							//validacion de celdas por columnas. [0-5]
							if (i == 0) {
								valTypeDatoCellSt = cell.getStringCellValue();
								pattern1 = Pattern.compile("^[0-9]*$");
								matcher1 = pattern1.matcher(valTypeDatoCellSt);

								   if(matcher1.find()) {
									   contrCMod=true;
								   }else{
									   contrCMod=false;
								   }
								if (valTypeDatoCellSt.length()!=7 || !contrCMod ) {
									esvacio = false;
									mensajeError = mensajeError + " No se tiene el valor correcto en el campo COD_MOD en la fila "
											+ (j+1)+ " .";;
									break;
								}
							}
							if (i == 1) {
								valTypeDatoCell = cell.getNumericCellValue();
								// valTypeDatoCellSt=cell.getStringCellValue();
								if ( (valTypeDatoCell > -0.04) || (valTypeDatoCell <  -19.0)) {
									// esvacio = true;
									esvacio = false;
									mensajeError = mensajeError + " La latitud esta fuera del rango permitido en la fila "
											+ (j+1)+ " .";;
									break;
								}
							}
							if (i == 2) {
								valTypeDatoCell = cell.getNumericCellValue();
								if ((valTypeDatoCell > -68.0) || (valTypeDatoCell < -82.0)) {
									// esvacio = true;
									esvacio = false;
									mensajeError = mensajeError + " La longitud esta fuera del rango permitido en la fila "
											+ (j+1) + " .";;
									break;
								}
							}
							if (i == 3) {
								valTypeDatoCellSt = String.valueOf(cell.getNumericCellValue());
								valTypeDatoCellSt=valTypeDatoCellSt.substring(0, valTypeDatoCellSt.indexOf("."));
								pattern1 = Pattern.compile("^[0-9]*$");
								matcher1 = pattern1.matcher(valTypeDatoCellSt);

								   if(matcher1.find()) {
									   contrCMod=true;
								   }else{
									   contrCMod=false;
								   }
								
								if (valTypeDatoCellSt.length()!=6 || !contrCMod ) {
									esvacio = false;
									mensajeError = mensajeError + " No se tiene el valor correcto en el campo CODCP en la fila "
											+ (j+1)+ " .";
									break;
								}
							}
							if (i == 4) { //no hay null
								valTypeDatoCellSt = String.valueOf(cell.getNumericCellValue());
								valTypeDatoCellSt=valTypeDatoCellSt.substring(0, valTypeDatoCellSt.indexOf("."));
								pattern1 = Pattern.compile("^[12]*$");
								matcher1 = pattern1.matcher(valTypeDatoCellSt);

								   if(matcher1.find()) {
									   contrCMod=true;
								   }else{
									   contrCMod=false;
								   }
								   //formateo
								 
								  
								if (valTypeDatoCellSt.length()!=1 ||!contrCMod ) {
									esvacio = false;
									mensajeError = mensajeError + " No se tiene el valor correcto en el campo AREA_2000 en la fila "
											+ (j+1)+ " .";;
									break;
								}
							}
							if (i == 5) { // hay null
								valTypeDatoCellSt = String.valueOf(cell.getNumericCellValue());
								valTypeDatoCellSt=valTypeDatoCellSt.substring(0, valTypeDatoCellSt.indexOf("."));
								pattern1 = Pattern.compile("^[12]*$");
								matcher1 = pattern1.matcher(valTypeDatoCellSt);

								   if(matcher1.find()) {
									   contrCMod=true;
								   }else{
									   contrCMod=false;
								   }
								  
								if (valTypeDatoCellSt.length()!=1 ||!contrCMod || valTypeDatoCellSt==null ) {
									esvacio = false;
									mensajeError = mensajeError + " No se tiene el valor correcto en el campo AREA_SIG en la fila "
											+ (j+1)+ " .";;
									break;
								}
							}

						}
					}
				}
				if (esvacio) {
					mensajeError = mensajeError + " Faltan completar datos en la fila " + (j+1)  + " .";;
					break;
				}

			}

		} else {

			// mensajeError = mensajeError + " Faltan completar la cantidad de
			// columnas, se requieren 6 columnas en el formato excel para la
			// carga de datos "; 

			boolean flagMensajeInvalido = false;
			boolean flagMensajeCantColumna = false;
			for (int i = 0; i < headsDocument.length; i++) {
				// for(int j=0;j<headsDocumentTemp.length;j++){
				// temIput=headsDocumentTemp[i];
//				if (!headsDocument[i].equals(headsDocumentTempMens[i])) {
				if (headsDocument[i].equals(headsDocumentTempMens[i])) {
					flagMensajeInvalido = true;
				} else {
					flagMensajeCantColumna = true;

				}
				// }

			}
			if (flagMensajeInvalido) {
				mensajeError = mensajeError + " El archivo subido no coincide con el formato de carga de datos";
			}
			if (numColumnas0 != 6) {
				mensajeError = mensajeError
						+ " Faltan completar la cantidad de columnas en el archivo subido, el formato de carga de datos requiere 6 columnas: COD_MOD, LATITUD, LONGITUD, CODCP, AREA_2000, AREA_SIG ";
			}

			// mensajeError = mensajeError
			// + " Faltan completar la cantidad de columnas en el archivo
			// subido, el formato de carga de datos requiere 6 columnas:
			// COD_MOD, LATITUD, LONGITUD, CODCP, AREA_2000, AREA_SIG ";

		}

		if (!numColumnasHeadCero) {
			mensajeError = mensajeError + " El archivo subido no coincide con el formato de carga de datos";
		}
		
		if(mensajeError.length()>0)
			mensajeError = mensajeError.substring(0, mensajeError.length()-1);

		return mensajeError;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup("java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/"
					+ nombreSimple.replace("Local", "") + "!" + nombreLargo);
		} catch (NamingException ne) {
			throw new RuntimeException(ne);
		}
	}

	public void Merge(Cell cell, String item) {
		if (item.equals("item1")) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue("Fabian Montoya");
		} else if (item.equals("item3")) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue("Excel_GO");
		} else {
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue("Item_not_found");
		}
	}

	public BaseBean getBaseBean() {
		if (baseBean == null) {
			baseBean = new BaseBean();
		}
		return baseBean;
	}

}
