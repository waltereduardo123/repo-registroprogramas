package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

public class Conexion implements Runnable {
    private Socket sock;
    private AtomicInteger count;

    public Conexion(Socket socket, AtomicInteger cuenta) {
            sock = socket;
            count = cuenta;
    }

    public void run() {
            try {
                    //Leemos el encabezado de datos
                    byte[] buf = new byte[2];
                    if (sock.getInputStream().read(buf) == 2) {
                            //Decodificamos la longitud
                            int largo = ((buf[0] & 0xff) << 8) | (buf[1] & 0xff);
                            //Creamos un buffer de la longitud indicada
                            buf = new byte[largo];
                            //Vamos leyendo bloques hasta tener el mensaje completo
                            int cuantos = sock.getInputStream().read(buf);
                            while (cuantos < buf.length) {
                                    cuantos += sock.getInputStream().read(buf, cuantos, buf.length - cuantos);
                            }
                            //Simplemente contestamos lo mismo que leimos
                            sock.getOutputStream().write(largo >> 8);
                            sock.getOutputStream().write(largo & 0xff);
                            sock.getOutputStream().write(buf);
                            sock.getOutputStream().flush();
                    }
            } catch (IOException ex) {
                    //TODO manejar error
            } finally {
                    try {
                            sock.close();
                    } catch (IOException ex) {
                            //TODO manejar error
                    }
                    //Decrementar el contador global de conexiones
                    count.decrementAndGet();
            }
    }
}
