package pe.gob.minedu.escale.regprogramas.ws.configuracion;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import pe.gob.minedu.escale.regprogramas.ws.servlet.SecurityFilter;

@ApplicationPath("/svt")
public class ServicioConfiguracionSvt extends Application {

	private Set<Object> singletons = new HashSet<>();

	public ServicioConfiguracionSvt() {
		singletons.add(new SecurityFilter());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

}

