package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdfparser.PDFParser;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import pe.gob.minedu.escale.adm.business.type.TipoArchivoType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionRevisionType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.ejb.service.SecuryTokenServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.TokenSecuryDTO;
import pe.gob.minedu.escale.adm.model.jpa.Usuario;
import pe.gob.minedu.escale.common.business.exception.FormatoInvalidoException;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.ConsultaJson;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.rest.auth.util.AuthUtils;
import pe.gob.minedu.escale.common.rest.auth.util.Token;
import pe.gob.minedu.escale.common.rest.util.AesUtil;
import pe.gob.minedu.escale.common.rest.util.QueryParamURL;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.FileRenamePolicy;
import pe.gob.minedu.escale.common.util.LogUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.CadenaInvalidaException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoRegistradaException;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DistritosDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DreUgelDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.FormCreaSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ObservacionSolCompareDTO;

@WebServlet("/svt/creacionSolicitud")
@MultipartConfig
public class CreacionSolicitudServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String mensajeErrorArchivo = "";
	private String mensaje = "";

	private Map<Object, Object> parametros;
	private Gson gson = new Gson();

	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;

	/** El objeto ErrorService. */
	private BaseBean baseBean;

	private String param, salt, iv;

	private int keySize = 128, iterationCount = 1000;

	private String passphrase = "0015975320171234";

	private AesUtil aesUtil;

	ConsultaJson consultaJson = new ConsultaJson();

	@EJB
	private transient SolicitudServiceLocal solicitudService = lookup(SolicitudServiceLocal.class);

	@EJB(beanName = "MaestroService")
	private transient MaestroServiceLocal maestroService;

	@EJB
	private DataGeneralCache dataGeneralCache = lookup(DataGeneralCache.class);

	@EJB
	private transient UsuarioServiceLocal usuarioServiceLocal = lookup(UsuarioServiceLocal.class);
	
    @EJB
    private transient SecuryTokenServiceLocal securyTokenServiceLocal = lookup(SecuryTokenServiceLocal.class);

	private String[] retornos;

	private List<MaestroDTO> listaTipoSolicitud = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionSolicitud = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionRevision = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoDocumento = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoArchivo = new ArrayList<MaestroDTO>();

	private static LogUtil log = new LogUtil(CreacionSolicitudServlet.class.getName());

	String respEncryp = "";

	public CreacionSolicitudServlet() {
		super();
	}

	@PostConstruct
	public void iniciar() {
		listaTipoSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionRevision = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionRevisionType.CODIGO_AGRUPACION.getKey());
		listaTipoArchivo = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoArchivoType.CODIGO_AGRUPACION.getKey());
		listaTipoDocumento = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoDocumentoType.CODIGO_AGRUPACION.getKey());
	}

	@SuppressWarnings("rawtypes")
	protected void doGet(HttpServletRequest request, HttpServletResponse response, String jsonEncrypted)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		SolicitudDTO solicitud = new SolicitudDTO();
		parametros = new HashMap<>();
		Boolean sucess = null;
		FormCreaSolicitudDTO formCreaSolicitud = null;
		String jsonEncrypteddcInexus = "";
		String jsonEncrypteddcJodexus = "";
		FormCreaSolicitudDTO objInexus = null;
		FormCreaSolicitudDTO objJodexus = null;

		Test1 tes = new Test1();
		aesUtil = new AesUtil(keySize, iterationCount);
		Token token;
		boolean isOkTk=false;
		try {

//			
			String dataEnvio = StringUtil.isNotNullOrBlank(request.getParameter("dataEnvio"))
					? request.getParameter("dataEnvio").toString()
					: null;
			System.out.println("DATA RECIBIDA : " + dataEnvio);

			// data token inicio
			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
			
			String dataEnvioOne = StringUtil.isNotNullOrBlank(request.getParameter("dataEnvioOne"))
					? request.getParameter("dataEnvioOne").toString()
					: null;
			System.out.println("DATA RECIBIDA ONE: " + dataEnvioOne);

			String dataEnvioTwo = StringUtil.isNotNullOrBlank(request.getParameter("dataEnvioTwo"))
					? request.getParameter("dataEnvioTwo").toString()
					: null;
			System.out.println("DATA RECIBIDA TWO: " + dataEnvioTwo);

			parametros.put("perfil", dataEnvioOne);
			parametros.put("codid", dataEnvioTwo); //
			parametros.put("identificador", passphrase);
			parametros.put("Mensajes", mensajes);

//			token = AuthUtils.createTokenValFormularios("127.0.0.1", parametros); ///////////////////////
		
				
			
			/////////////////////

//			
			param = QueryParamURL.getParam(dataEnvio, "jsonEncrypted");
			salt = QueryParamURL.getParam(dataEnvio, "salt");
			iv = QueryParamURL.getParam(dataEnvio, "iv");

////			
			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			System.out.println("DATA desencriptada : " + objJsonString);
//			
//		
			try {
				formCreaSolicitud = gson.fromJson(objJsonString, FormCreaSolicitudDTO.class);
				System.out.println(
						"OBJECTO BASE : " + formCreaSolicitud.getInexus() + " /// " + formCreaSolicitud.getJodexus());
			} catch (JsonSyntaxException je) {
				System.out.println("ERROR :  " + je);
			}

			try {
				jsonEncrypteddcInexus = aesUtil.descifrarBase64(formCreaSolicitud.getInexus().substring(9));

				objInexus = gson.fromJson(jsonEncrypteddcInexus, FormCreaSolicitudDTO.class);

				jsonEncrypteddcJodexus = aesUtil.descifrarBase64(formCreaSolicitud.getJodexus());

				objJodexus = gson.fromJson(jsonEncrypteddcJodexus, FormCreaSolicitudDTO.class);

				System.out.println("OBJETOS RECUPERADOS : " + objInexus + " ////  " + objJodexus);

			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}
			
			Usuario userValido = usuarioServiceLocal.verificarUsuario(objInexus.getUsuarioCreacion());
			if (StringUtils.isEmpty(userValido.getCodigoOID())) {
				throw new CadenaInvalidaException();
			}
			
			
	
			
			
			SimpleDateFormat formatter1=new SimpleDateFormat("dd/MM/yyyy"); 
			Date dateServ=new Date();
			String fechDocu=objInexus.getX135();
			Date dateFecDoc=formatter1.parse(fechDocu); 
			
			if(dateFecDoc.after(dateServ))throw new CadenaInvalidaException();
			
			
			
            String idGestionX=objInexus.getX102();
            Long idGestion = Long.parseLong(idGestionX);
            
            String idDependenciaX=objInexus.getX103();
            Long idDependencia = Long.parseLong(idDependenciaX);
            List<MaestroDTO> listaMaestroHijosDeGestion = maestroService.buscarMaestrosxPadre(idGestion);
            
            if(listaMaestroHijosDeGestion.isEmpty()) { throw new CadenaInvalidaException();}
            else {
                  boolean idDependenciaExiste = false;
                idDependenciaExiste = listaMaestroHijosDeGestion.stream().anyMatch(
                        (p) -> p.getIdMaestro().equals(idDependencia));
                  if(!idDependenciaExiste) throw new CadenaInvalidaException();
            }
            
                            
            // tbl_regpro_maestro 
            String idGestoraX=objInexus.getX104();
            Long idGestora = Long.parseLong(idGestoraX);
            List<MaestroDTO> listaMaestroHijosDeDependencia = maestroService.buscarMaestrosxPadre(idDependencia);
            
            if(listaMaestroHijosDeDependencia.isEmpty()) { throw new CadenaInvalidaException();}
            else {
                  boolean idGestoraExiste = false;
                  idGestoraExiste = listaMaestroHijosDeDependencia.stream().anyMatch(
                             (p) -> p.getIdMaestro().equals(idGestora));            
                  if(!idGestoraExiste) throw new CadenaInvalidaException();
            }

            
            
            

			

			List<Object[]> listPrivilegios = usuarioServiceLocal.validarUserPerfilPriv(
					formCreaSolicitud.getUsuarioCreacion(), formCreaSolicitud.getPerfil(), objInexus.getInit());
			if (listPrivilegios == null || listPrivilegios.size() == 0) {
				throw new CadenaInvalidaException();
			}
			
			

			FormCreaSolicitudDTO objCampos = new FormCreaSolicitudDTO();
			objCampos.setX100(objInexus.getX100());
			objCampos.setX101(objInexus.getX101());
			objCampos.setX102(objInexus.getX102());
			objCampos.setX103(objInexus.getX103());
			objCampos.setX104(objInexus.getX104());
			objCampos.setX105(objInexus.getX105());
			objCampos.setX106(objInexus.getX106());
			objCampos.setX107(objInexus.getX107());
			objCampos.setX108(objInexus.getX108());
			objCampos.setX109(objInexus.getX109());
			objCampos.setX110(objInexus.getX110());
			objCampos.setX111(objInexus.getX111());
			objCampos.setX112(objInexus.getX112());
			objCampos.setX113(objInexus.getX113());
			objCampos.setX114(objInexus.getX114());
			objCampos.setX115(objInexus.getX115());
			objCampos.setX116(objInexus.getX116());
			objCampos.setX117(objInexus.getX117());
			objCampos.setX118(objInexus.getX118());
			objCampos.setX119(objInexus.getX119());
			objCampos.setX120(objInexus.getX120());
			objCampos.setX121(objInexus.getX121());
			objCampos.setX122(objInexus.getX122());
			objCampos.setX123(objInexus.getX123());
			objCampos.setX124(objInexus.getX124());
			objCampos.setX125(objInexus.getX125());
			objCampos.setX126(objInexus.getX126());
			objCampos.setX127(objInexus.getX127());
			objCampos.setX128(objInexus.getX128());
			objCampos.setX129(objInexus.getX129());
			objCampos.setX130(objInexus.getX130());
			objCampos.setX131(objInexus.getX131());
			objCampos.setX132(objInexus.getX132());
			objCampos.setX133(objInexus.getX133());
			objCampos.setX134(objInexus.getX134());
			objCampos.setX135(objInexus.getX135());
			objCampos.setX136(objInexus.getX136());
			objCampos.setX137(objInexus.getX137());
			objCampos.setX138(objInexus.getX138());
			objCampos.setX139(objInexus.getX139());
			objCampos.setX140(objInexus.getX140());
			objCampos.setX141(objInexus.getX141());
			objCampos.setX142(objInexus.getX142());
			objCampos.setX143(objInexus.getX143());
			objCampos.setX144(objInexus.getX144());
			objCampos.setX145(objInexus.getX145());
			objCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
			objCampos.setPerfil(objInexus.getPerfil());
			objCampos.setInit(objInexus.getInit());

			/////////////////////////////////////////////////////////////

//			
//			if(!jsonEncrypteddcJodexus.equals(jsonEncrypteddcInexus))
//			{
//				throw new CadenaInvalidaException();
//			}

			if (!objInexus.equals(objJodexus) || !objInexus.equals(objCampos) || !objJodexus.equals(objCampos)) {
//			if (!objInexus.equals(objJodexus)) {
				throw new CadenaInvalidaException();
			} else {
				/*
				parametros.put("xtu", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			
			*/
			
				

				// subida de archivo
				String extensions = "pdf";
//				int cantidadMBMaxima = 3;
//				FileProperties filePropiedades = new FileProperties(extensions,cantidadMBMaxima);		
				Properties propiedades = new Properties();
				InputStream in = getClass()
						.getResourceAsStream("/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
				propiedades.load(in);
				String pathFile = propiedades.getProperty("directorio.path");
				in.close();
				FileRenamePolicy fileName = new FileRenamePolicy() {
					@Override
					public File rename(File f, InputStream content) {
						String fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
								"ddMMyyyyHHmmss");
						String extension = FilenameUtils.getExtension(f.getName());
						String nombreFile = FilenameUtils.getBaseName(f.getName());
						File f1 = new File(fecha + nombreFile + extension);
						f.renameTo(f1);
						return f;
					}

					@Override
					public File rename(File f, String nombreBase) {
						String fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
								"yyyyMMddHHmmssSSS");
						String extension = FilenameUtils.getExtension(f.getName());
						String nuevoNombre = pathFile + fecha + "-" + nombreBase + "." + extension;
						File fileNuevo = new File(nuevoNombre);
						return fileNuevo;
					}

					public String rename(String[] nombreBase) {
						String codigoDocumento = StringUtil.isNotNullOrBlank(request.getParameter("codigoDocumento"))
								? request.getParameter("codigoDocumento").toString()
								: null;
						String fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
								"yyyyMMddHHmmssSSS");
						String nuevoNombre = pathFile + codigoDocumento + "-" + fecha + "-"
								+ nombreBase[1].replaceAll("file", "") + "." + extensions;
						return nuevoNombre;
					}
				};
				String[] fileNombre = null;
				String fileNombreFinal = null;
				List<String> listaFileNombreFinal = new ArrayList<String>();

				/* INCIO Verificar archivo valido PDF */
				boolean isPDF = true;
				for (Part part : request.getParts()) {
					fileNombre = getFileName(part);
					if (StringUtil.isNotNullOrBlank(fileNombre)) {
						PDFParser parser = new PDFParser(part.getInputStream());
						try {
							parser.parse();
						} catch (IOException e) {
							isPDF = false;
//						    e.printStackTrace();					    
							throw new FormatoInvalidoException();// Abortar servlet
						}
					}
					if (!isPDF) {
						break;
					}
				}
				/* FIN Verificar archivo valido PDF */
				// Guardar Archivos
				for (Part part : request.getParts()) {
					fileNombre = getFileName(part);
					if (StringUtil.isNotNullOrBlank(fileNombre)) {
						fileNombreFinal = fileName.rename(fileNombre);
						part.write(fileNombreFinal);
						listaFileNombreFinal.add(fileNombreFinal);
					}
				}

				// INICIO SOLICITUD
				solicitud.setTipoSolicitud((StringUtil.isNotNullOrBlank(request.getParameter("tipoSolicitud")))
						? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSolicitud").toString())))
						: new MaestroDTO(0L));
				
				solicitud.setCodigoModular((StringUtil.isNotNullOrBlank(request.getParameter("codigoModular")))
						? request.getParameter("codigoModular").toString()
						: null);
				//TIPO DE PROGRAMA
				solicitud.setTipoPrograma((StringUtil.isNotNullOrBlank(objInexus.getX101()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX101())))
						: new MaestroDTO(0L));
				//NOMBRE DE PROGRAMAS
				solicitud.setNombrePrograma((StringUtil.isNotNullOrBlank(objInexus.getX100()))
						? objInexus.getX100().toString().toUpperCase()
						: null);
				//TIPO DE GESTION
				solicitud.setTipoGestion((StringUtil.isNotNullOrBlank(objInexus.getX102()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX102().toString())))
						: new MaestroDTO(0L));
				
		
				
				solicitud.setLatitudPrograma((StringUtil.isNotNullOrBlank(objInexus.getX114()))
						? (BigDecimal.valueOf(Double.parseDouble(objInexus.getX114())))
						: null);
				solicitud.setLongitudPrograma((StringUtil.isNotNullOrBlank(objInexus.getX115()))
						? (BigDecimal.valueOf(Double.parseDouble(objInexus.getX115())))
						: null);

				solicitud.setTipoDependencia((StringUtil.isNotNullOrBlank(objInexus.getX103()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX103().toString())))
						: new MaestroDTO(0L));
				solicitud
						.setTipoGestionEducativa((StringUtil.isNotNullOrBlank(objInexus.getX104()))
								? (new MaestroDTO(Long.valueOf(objInexus.getX104().toString())))
								: new MaestroDTO(0L));
				solicitud.setTipoTurno((StringUtil.isNotNullOrBlank(objInexus.getX137()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX137().toString())))
						: new MaestroDTO(0L));
				solicitud.setTipoContinuidadJornadaEscolar(
						(StringUtil.isNotNullOrBlank(objInexus.getX138()))
								? (new MaestroDTO(
										Long.valueOf(objInexus.getX138().toString())))
								: new MaestroDTO(0L));
				solicitud.setTipoVia((StringUtil.isNotNullOrBlank(objInexus.getX116()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX116().toString())))
						: new MaestroDTO(0L));
				solicitud.setNombreVia((StringUtil.isNotNullOrBlank(objInexus.getX117()))
						? objInexus.getX117().toString()
						: null);
				solicitud.setNumeroVia((StringUtil.isNotNullOrBlank(objInexus.getX118()))
						? objInexus.getX118().toString()
						: null);
				solicitud.setManzana((StringUtil.isNotNullOrBlank(objInexus.getX119()))
						? objInexus.getX119().toString()
						: null);
				solicitud.setLote((StringUtil.isNotNullOrBlank(objInexus.getX120()))
						? objInexus.getX120().toString()
						: null);
				solicitud.setTipoLocalidad((StringUtil.isNotNullOrBlank(objInexus.getX121()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX121().toString())))
						: new MaestroDTO(0L));
				solicitud.setLocalidad((StringUtil.isNotNullOrBlank(objInexus.getX122()))
						? objInexus.getX122().toString()
						: null);
				solicitud.setEtapa((StringUtil.isNotNullOrBlank(objInexus.getX123()))
						? objInexus.getX123().toString()
						: null);
				solicitud.setSector((StringUtil.isNotNullOrBlank(objInexus.getX124()))
						? objInexus.getX124().toString()
						: null);
				solicitud.setZona((StringUtil.isNotNullOrBlank(objInexus.getX125()))
						? objInexus.getX125().toString()
						: null);
				solicitud.setOtraDireccion((StringUtil.isNotNullOrBlank(objInexus.getX126()))
						? objInexus.getX126().toString()
						: null);
				solicitud.setReferenciaDireccion((StringUtil.isNotNullOrBlank(objInexus.getX127()))
						? objInexus.getX127().toString()
						: null);
				solicitud.setCodigoCentroPoblado((StringUtil.isNotNullOrBlank(objInexus.getX108()))
						? objInexus.getX108().toString()
						: null);
				solicitud.setNombreCentroPoblado((StringUtil.isNotNullOrBlank(objInexus.getX109()))
						? objInexus.getX109().toString()
						: null);
				// InicioJL
				solicitud.setCodigoArea((StringUtil.isNotNullOrBlank(objInexus.getX112()))
						? objInexus.getX112().toString()
						: null);
				solicitud.setCodigoAreaSig((StringUtil.isNotNullOrBlank(objInexus.getX113()))
						? objInexus.getX113().toString()
						: null);
				// InicioJL
				solicitud
						.setLatitudCentroPoblado((StringUtil.isNotNullOrBlank(objInexus.getX110()))
								? (BigDecimal.valueOf(
										Double.parseDouble(objInexus.getX110().toString())))
								: null);
				solicitud.setLongitudCentroPoblado(
						(StringUtil.isNotNullOrBlank(objInexus.getX111()))
								? (BigDecimal.valueOf(
										Double.parseDouble(objInexus.getX111().toString())))
								: null);
				solicitud.setCodigoServicioEduMasCercano(
						(StringUtil.isNotNullOrBlank(objInexus.getX128()))
								? objInexus.getX128().toString()
								: null);
				solicitud.setNombreServicioEduMasCercano(
						(StringUtil.isNotNullOrBlank(objInexus.getX129()))
								? objInexus.getX129().toString()
								: null);
				solicitud.setLatitudServicioEduMasCercano(
						(StringUtil.isNotNullOrBlank(objInexus.getX130()))
								? (BigDecimal.valueOf(Double
										.parseDouble(objInexus.getX130().toString())))
								: null);
				solicitud.setLongitudServicioEduMasCercano(
						(StringUtil.isNotNullOrBlank(objInexus.getX131()))
								? (BigDecimal.valueOf(Double
										.parseDouble(objInexus.getX131().toString())))
								: null);
				solicitud.setTipoProveedorAgua((StringUtil.isNotNullOrBlank(objInexus.getX139()))
						? (new MaestroDTO(Long.valueOf(objInexus.getX139().toString())))
						: new MaestroDTO(0L));
				solicitud.setOtroProveedorAgua((StringUtil.isNotNullOrBlank(objInexus.getX140()))
						? objInexus.getX140().toString()
						: null);
				solicitud.setSuministroAgua((StringUtil.isNotNullOrBlank(objInexus.getX141()))
						? objInexus.getX141().toString()
						: null);
				solicitud
						.setTipoProveedorEnergia((StringUtil.isNotNullOrBlank(objInexus.getX142()))
								? (new MaestroDTO(Long.valueOf(objInexus.getX142().toString())))
								: new MaestroDTO(0L));
				solicitud
						.setOtroProveedorEnergia((StringUtil.isNotNullOrBlank(objInexus.getX143()))
								? objInexus.getX143().toString()
								: null);
				solicitud.setSuministroEnergia((StringUtil.isNotNullOrBlank(objInexus.getX144()))
						? objInexus.getX144().toString()
						: null);
				solicitud.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString()))
						.findFirst().get().getIdMaestro()));
				solicitud.setGeoHash((StringUtil.isNotNullOrBlank(request.getParameter("geoHash")))
						? request.getParameter("geoHash").toString()
						: null);
				solicitud.setUsuarioEnvio((StringUtil.isNotNullOrBlank(objInexus.getUsuarioCreacion()))
						? objInexus.getUsuarioCreacion().toString()
						: null);
				solicitud.setNombreUsuarioEnvio((StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioEnvio")))
						? request.getParameter("nombreUsuarioEnvio").toString()
						: null);
				solicitud.setCodSolicitud(
						FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "yyyyMMddHHmmssSSS")
								+ ((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
										? request.getParameter("dreUgel").toString()
										: 0));
				solicitud.setFechaEnvio(FechaUtil.obtenerFechaActual());
				solicitud.setUsuarioModificacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
						? request.getParameter("usuarioModificacion").toString()
						: null);
				solicitud.setNombreUsuarioModificacion(
						(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioModificacion")))
								? request.getParameter("nombreUsuarioModificacion").toString()
								: null);
				solicitud.setUsuarioRevision((StringUtil.isNotNullOrBlank(request.getParameter("usuarioRevision")))
						? request.getParameter("usuarioRevision").toString()
						: null);
				solicitud.setNombreUsuarioRevision(
						(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevision")))
								? request.getParameter("nombreUsuarioRevision").toString()
								: null);
				solicitud.setUsuarioRevisionSig((StringUtil.isNotNullOrBlank(request.getParameter("usuarioRevisionSig")))
						? request.getParameter("usuarioRevisionSig").toString()
						: null);
				solicitud.setNombreUsuarioRevisionSig(
						(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevisionSig")))
								? request.getParameter("nombreUsuarioRevisionSig").toString()
								: null);
				solicitud.setFechaCreacion(FechaUtil.obtenerFechaActual());
				solicitud.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
						? request.getParameter("usuarioCreacion").toString()
						: null);// Reemplazar con request de cliente
				solicitud.setEstado((StringUtil.isNotNullOrBlank(request.getParameter("estado")))
						? request.getParameter("estado").toString()
						: EstadoState.ACTIVO.getValue());
				solicitud.setTipoSituacionPrograma(
						(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionPrograma")))
								? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSituacionPrograma").toString())))
								: new MaestroDTO(0L));// Si es creacion de programa es nulo
				solicitud.setTipoSituacionRevision(
						(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionRevision")))
								? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSituacionRevision").toString())))
								: new MaestroDTO(
										listaTipoSituacionRevision
												.stream().filter(
														p -> p.getCodigoItem()
																.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION
																		.getKey().toString()))
												.findFirst().get().getIdMaestro()));
				solicitud
						.setTipoSituacionRevisionSig(
								(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionRevisionSig")))
										? (new MaestroDTO(
												Long.valueOf(request.getParameter("tipoSituacionRevisionSig").toString())))
										: new MaestroDTO(listaTipoSituacionRevision
												.stream().filter(
														p -> p.getCodigoItem()
																.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION
																		.getKey().toString()))
												.findFirst().get().getIdMaestro()));
				solicitud.setIndicadorUltimo(EstadoState.INACTIVO.getValue());
				// INCIO DOCUMENTO
				DocumentoDTO documento = new DocumentoDTO();
				documento.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
						.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.RESOLUCION.getKey().toString())).findFirst()
						.get().getIdMaestro()));

				documento.setTipoDocumentoResolucion(
						(StringUtil.isNotNullOrBlank(objInexus.getX133()))
								? (new MaestroDTO(Long.valueOf(objInexus.getX133().toString())))
								: new MaestroDTO(0L));
				documento.setNroDocumento((StringUtil.isNotNullOrBlank(objInexus.getX134()))
						? objInexus.getX134().toString()
						: null);
				try {
					Date fechaDocumento = FechaUtil
							.obtenerFecha(StringUtil.isNotNullOrBlank(objInexus.getX135())
									? objInexus.getX135().toString()
									: null);
					documento.setFechaDocumento(fechaDocumento);
				} catch (ParseException e1) {
					documento.setFechaDocumento(null);
					e1.printStackTrace();
				}
				documento.setCodigoDocumento((StringUtil.isNotNullOrBlank(objInexus.getX136()))
						? objInexus.getX136().toString()
						: null);
				documento.setCodooii((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
						? request.getParameter("dreUgel").toString()
						: null);
				documento.setFechaCreacion(FechaUtil.obtenerFechaActual());
				documento.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
						? request.getParameter("usuarioCreacion").toString()
						: null);// Reemplazar con request de cliente
				documento.setEstado((StringUtil.isNotNullOrBlank(request.getParameter("estado")))
						? request.getParameter("estado").toString()
						: EstadoState.ACTIVO.getValue());
				documento.setIndCambioArchivos((StringUtil.isNotNullOrBlank(request.getParameter("indCambioArchivo")))
						? request.getParameter("indCambioArchivo").toString()
						: EstadoState.INACTIVO.getValue());
				// INCIO ARCHIVO
				// INICIO SUBIDA ARCHIVO
				List<ArchivoDTO> listaArchivo = new ArrayList<ArchivoDTO>();
				for (String file : listaFileNombreFinal) {
					ArchivoDTO archivo = new ArchivoDTO();
					char mander = file.charAt(file.length() - 5);
					switch (mander) {
					case 's':
						archivo.setTipoArchivo(new MaestroDTO(listaTipoDocumento.stream()
								.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
								.findFirst().get().getIdMaestro()));// TipoArchivoCroquis, artificio para luego crear otro
																	// documento tipo croquis
						break;
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
						archivo.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
								.filter(p -> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString())).findFirst()
								.get().getIdMaestro()));
						break;
					default:
						break;
					}
					// Datos genericos en archivos
					String nom = "";
					int startIndex = file.lastIndexOf('\\');
					int startIndexLinux = file.lastIndexOf('/');
					if (startIndex != -1) {
						nom = file.substring(startIndex + 1, file.length());
					}
					if (startIndexLinux != -1) {
						nom = file.substring(startIndexLinux + 1, file.length());
					}
					archivo.setNombreArchivo(nom);
					archivo.setCodigoArchivo(nom.substring(0, nom.length() - 4));
					archivo.setFechaCreacion(FechaUtil.obtenerFechaActual());
					archivo.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
							? request.getParameter("usuarioCreacion").toString()
							: null);// Reemplazar con request de cliente
					archivo.setEstado(EstadoState.ACTIVO.getValue());
					listaArchivo.add(archivo); // Agregar archivo a una lista Archivo
				}
				// FIN ARCHIVO
				documento.setListaArchivo(listaArchivo);
				// FIN DOCUMENTO
				// Guardar en tabla SOLICITUD
				solicitud.setDistrito((StringUtil.isNotNullOrBlank(objInexus.getX107()))
						? (new DistritosDTO(objInexus.getX107().toString()))
						: null);
				// setear DreUgel
//				String dreUgel = request.getParameter("dreUgel");
				solicitud.setDreUgel((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
						? (new DreUgelDTO(request.getParameter("dreUgel").toString()))
						: null);
				AccionSolicitudDTO accionSolicitudDTO = new AccionSolicitudDTO();
				// creacionDuro
				accionSolicitudDTO.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString()))
						.findFirst().get().getIdMaestro()));// PENDIENTE
				accionSolicitudDTO.setFechaCreacion(FechaUtil.obtenerFechaActual());
				accionSolicitudDTO.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
						? request.getParameter("usuarioCreacion").toString()
						: null);
				accionSolicitudDTO.setEstado(EstadoState.ACTIVO.getValue());
				List<AccionSolicitudDTO> listaAccionSolicitudDTO = new ArrayList<AccionSolicitudDTO>();
				listaAccionSolicitudDTO.add(accionSolicitudDTO);
				solicitud.setListaAccionSolicitud(listaAccionSolicitudDTO);
				try {
					
			//		param = QueryParamURL.getParam(token.getToken(), "uescalet");
//					System.out.println("TOKRN : " + token.getToken() );
					// leer el token registrado
					List<TokenSecuryDTO> listk=new ArrayList<TokenSecuryDTO>();
//					TokenSecuryDTO del=new TokenSecuryDTO();
					listk=securyTokenServiceLocal.listVerificarTokenForm();
					//if(listk.size()>0) {
					String tkform=listk.get(0).getToken();
					System.out.println("EL TOKEN : " + tkform );
					
					isOkTk=AuthUtils.validarTokenFormulario(tkform);
//					isOkTk=AuthUtils.validarTokenFormulario(token.getToken().toString());
					
					if(isOkTk) {

						
					
								solicitud = solicitudService.registrarSolicitudNuevo(solicitud, documento);
								//ordenar la eliminacion token registrado
								sucess = true;
								parametros.put("clean", true);
//								securyTokenServiceLocal.eliminarTokenForm(del);
						
					}else {
						//verificacion del token
						sucess = false;
						parametros.put("clean", false);
//						parametros.put("xtk", isOkTk);
//						respuesta = new RespuestaDTO(true, "['']", parametros);
//						respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
//								+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
						throw new SolicitudNoRegistradaException();
//						
					}
					
				//	}
					
					
					
				} catch (Exception e) {
					sucess = false;
					parametros.put("clean", false);
					throw new SolicitudNoRegistradaException();
				}
				sucess = true;
				parametros.put("solicitudRegistrada", true);
//				parametros.put("solicitud", solicitud);////////////////////////////////////////CAMBIO/////////////////////////////////////////////
//				if (solicitud.getTipoSolicitud().getIdMaestro().equals(271L)){
				String prue = request.getParameter("tipoSolicitud");
//				if (prue.equals("271")){	//solicitudCierre
				if (prue.equals(listaTipoSolicitud.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey().toString())).findFirst()
						.get().getIdMaestro().toString())) { // solicitudCierre
					Map<Object, Object> parametro = new HashMap<>();
					parametro.put("codigoModular", solicitud.getCodigoModular());
					parametro.put("tipoSituacionSolicitud", listaTipoSituacionSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
							.findFirst().get().getIdMaestro());
					parametros.put("solicitudAnuladas",
							solicitudService.listarSolicitudFirstMaxResultParametros(1000, 0, parametro));
				}

//				setMensajeExito("1");//Solicitud registrada
			}
		}
			catch (Exception e) {
				sucess = false;
				mensaje = getBaseBean().getErrorService().getErrorFor(e, ResourceUtil.obtenerLocaleSession())
						.getDefaultMessage();
				parametros.put("solicitudRegistrada", false);

				if (log.isHabilitadoError()) {
					log.error(e);
				}
			}

			finally {
				respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

				response.addHeader("x-frame-options", "DENY");
				// response.addHeader("x-frame-options","SAMEORIGIN");
				response.addHeader("X-XSS-Protection", "1; mode=block");
				response.addHeader("X-Content-Type-Options", "nosniff");

				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html");

				// respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt,
				// iv, passphrase, gson.toJson(respuesta));
				// response.getWriter().write(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString());

				response.getWriter().write(gson.toJson(respuesta).toString());

				/// return
				/// Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

			}
				
				
				
				
				
				
				
				
			
			
			
			
			
			
			}


//		
//
//			// subida de archivo
//			String extensions = "pdf";
////			int cantidadMBMaxima = 3;
////			FileProperties filePropiedades = new FileProperties(extensions,cantidadMBMaxima);		
//			Properties propiedades = new Properties();
//			InputStream in = getClass()
//					.getResourceAsStream("/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
//			propiedades.load(in);
//			String pathFile = propiedades.getProperty("directorio.path");
//			in.close();
//			FileRenamePolicy fileName = new FileRenamePolicy() {
//				@Override
//				public File rename(File f, InputStream content) {
//					String fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
//							"ddMMyyyyHHmmss");
//					String extension = FilenameUtils.getExtension(f.getName());
//					String nombreFile = FilenameUtils.getBaseName(f.getName());
//					File f1 = new File(fecha + nombreFile + extension);
//					f.renameTo(f1);
//					return f;
//				}
//
//				@Override
//				public File rename(File f, String nombreBase) {
//					String fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
//							"yyyyMMddHHmmssSSS");
//					String extension = FilenameUtils.getExtension(f.getName());
//					String nuevoNombre = pathFile + fecha + "-" + nombreBase + "." + extension;
//					File fileNuevo = new File(nuevoNombre);
//					return fileNuevo;
//				}
//
//				public String rename(String[] nombreBase) {
//					String codigoDocumento = StringUtil.isNotNullOrBlank(request.getParameter("codigoDocumento"))
//							? request.getParameter("codigoDocumento").toString()
//							: null;
//					String fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
//							"yyyyMMddHHmmssSSS");
//					String nuevoNombre = pathFile + codigoDocumento + "-" + fecha + "-"
//							+ nombreBase[1].replaceAll("file", "") + "." + extensions;
//					return nuevoNombre;
//				}
//			};
//			String[] fileNombre = null;
//			String fileNombreFinal = null;
//			List<String> listaFileNombreFinal = new ArrayList<String>();
//
//			/* INCIO Verificar archivo valido PDF */
//			boolean isPDF = true;
//			for (Part part : request.getParts()) {
//				fileNombre = getFileName(part);
//				if (StringUtil.isNotNullOrBlank(fileNombre)) {
//					PDFParser parser = new PDFParser(part.getInputStream());
//					try {
//						parser.parse();
//					} catch (IOException e) {
//						isPDF = false;
////					    e.printStackTrace();					    
//						throw new FormatoInvalidoException();// Abortar servlet
//					}
//				}
//				if (!isPDF) {
//					break;
//				}
//			}
//			/* FIN Verificar archivo valido PDF */
//			// Guardar Archivos
//			for (Part part : request.getParts()) {
//				fileNombre = getFileName(part);
//				if (StringUtil.isNotNullOrBlank(fileNombre)) {
//					fileNombreFinal = fileName.rename(fileNombre);
//					part.write(fileNombreFinal);
//					listaFileNombreFinal.add(fileNombreFinal);
//				}
//			}
//
//			// INICIO SOLICITUD
//			solicitud.setTipoSolicitud((StringUtil.isNotNullOrBlank(request.getParameter("tipoSolicitud")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSolicitud").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setCodigoModular((StringUtil.isNotNullOrBlank(request.getParameter("codigoModular")))
//					? request.getParameter("codigoModular").toString()
//					: null);
//			solicitud.setTipoPrograma((StringUtil.isNotNullOrBlank(request.getParameter("tipoPrograma")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoPrograma").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setNombrePrograma((StringUtil.isNotNullOrBlank(request.getParameter("nombrePrograma")))
//					? request.getParameter("nombrePrograma").toString().toUpperCase()
//					: null);
//			solicitud.setLatitudPrograma((StringUtil.isNotNullOrBlank(request.getParameter("latitudPrograma")))
//					? (BigDecimal.valueOf(Double.parseDouble(request.getParameter("latitudPrograma"))))
//					: null);
//			solicitud.setLongitudPrograma((StringUtil.isNotNullOrBlank(request.getParameter("longitudPrograma")))
//					? (BigDecimal.valueOf(Double.parseDouble(request.getParameter("longitudPrograma"))))
//					: null);
//			solicitud.setTipoGestion((StringUtil.isNotNullOrBlank(request.getParameter("tipoGestion")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoGestion").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setTipoDependencia((StringUtil.isNotNullOrBlank(request.getParameter("tipoDependencia")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoDependencia").toString())))
//					: new MaestroDTO(0L));
//			solicitud
//					.setTipoGestionEducativa((StringUtil.isNotNullOrBlank(request.getParameter("tipoGestionEducativa")))
//							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoGestionEducativa").toString())))
//							: new MaestroDTO(0L));
//			solicitud.setTipoTurno((StringUtil.isNotNullOrBlank(request.getParameter("tipoTurno")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoTurno").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setTipoContinuidadJornadaEscolar(
//					(StringUtil.isNotNullOrBlank(request.getParameter("tipoContinuidadJornadaEscolar")))
//							? (new MaestroDTO(
//									Long.valueOf(request.getParameter("tipoContinuidadJornadaEscolar").toString())))
//							: new MaestroDTO(0L));
//			solicitud.setTipoVia((StringUtil.isNotNullOrBlank(request.getParameter("tipoVia")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoVia").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setNombreVia((StringUtil.isNotNullOrBlank(request.getParameter("nombreVia")))
//					? request.getParameter("nombreVia").toString()
//					: null);
//			solicitud.setNumeroVia((StringUtil.isNotNullOrBlank(request.getParameter("numeroVia")))
//					? request.getParameter("numeroVia").toString()
//					: null);
//			solicitud.setManzana((StringUtil.isNotNullOrBlank(request.getParameter("manzana")))
//					? request.getParameter("manzana").toString()
//					: null);
//			solicitud.setLote((StringUtil.isNotNullOrBlank(request.getParameter("lote")))
//					? request.getParameter("lote").toString()
//					: null);
//			solicitud.setTipoLocalidad((StringUtil.isNotNullOrBlank(request.getParameter("tipoLocalidad")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoLocalidad").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setLocalidad((StringUtil.isNotNullOrBlank(request.getParameter("localidad")))
//					? request.getParameter("localidad").toString()
//					: null);
//			solicitud.setEtapa((StringUtil.isNotNullOrBlank(request.getParameter("etapa")))
//					? request.getParameter("etapa").toString()
//					: null);
//			solicitud.setSector((StringUtil.isNotNullOrBlank(request.getParameter("sector")))
//					? request.getParameter("sector").toString()
//					: null);
//			solicitud.setZona((StringUtil.isNotNullOrBlank(request.getParameter("zona")))
//					? request.getParameter("zona").toString()
//					: null);
//			solicitud.setOtraDireccion((StringUtil.isNotNullOrBlank(request.getParameter("otraDireccion")))
//					? request.getParameter("otraDireccion").toString()
//					: null);
//			solicitud.setReferenciaDireccion((StringUtil.isNotNullOrBlank(request.getParameter("referenciaDireccion")))
//					? request.getParameter("referenciaDireccion").toString()
//					: null);
//			solicitud.setCodigoCentroPoblado((StringUtil.isNotNullOrBlank(request.getParameter("codigoCentroPoblado")))
//					? request.getParameter("codigoCentroPoblado").toString()
//					: null);
//			solicitud.setNombreCentroPoblado((StringUtil.isNotNullOrBlank(request.getParameter("nombreCentroPoblado")))
//					? request.getParameter("nombreCentroPoblado").toString()
//					: null);
//			// InicioJL
//			solicitud.setCodigoArea((StringUtil.isNotNullOrBlank(request.getParameter("areaCentroPoblado")))
//					? request.getParameter("areaCentroPoblado").toString()
//					: null);
//			solicitud.setCodigoAreaSig((StringUtil.isNotNullOrBlank(request.getParameter("codigoAreaSig")))
//					? request.getParameter("codigoAreaSig").toString()
//					: null);
//			// InicioJL
//			solicitud
//					.setLatitudCentroPoblado((StringUtil.isNotNullOrBlank(request.getParameter("latitudCentroPoblado")))
//							? (BigDecimal.valueOf(
//									Double.parseDouble(request.getParameter("latitudCentroPoblado").toString())))
//							: null);
//			solicitud.setLongitudCentroPoblado(
//					(StringUtil.isNotNullOrBlank(request.getParameter("longitudCentroPoblado")))
//							? (BigDecimal.valueOf(
//									Double.parseDouble(request.getParameter("longitudCentroPoblado").toString())))
//							: null);
//			solicitud.setCodigoServicioEduMasCercano(
//					(StringUtil.isNotNullOrBlank(request.getParameter("codigoServicioEduMasCercano")))
//							? request.getParameter("codigoServicioEduMasCercano").toString()
//							: null);
//			solicitud.setNombreServicioEduMasCercano(
//					(StringUtil.isNotNullOrBlank(request.getParameter("nombreServicioEduMasCercano")))
//							? request.getParameter("nombreServicioEduMasCercano").toString()
//							: null);
//			solicitud.setLatitudServicioEduMasCercano(
//					(StringUtil.isNotNullOrBlank(request.getParameter("latitudServicioEduMasCercano")))
//							? (BigDecimal.valueOf(Double
//									.parseDouble(request.getParameter("latitudServicioEduMasCercano").toString())))
//							: null);
//			solicitud.setLongitudServicioEduMasCercano(
//					(StringUtil.isNotNullOrBlank(request.getParameter("longitudServicioEduMasCercano")))
//							? (BigDecimal.valueOf(Double
//									.parseDouble(request.getParameter("longitudServicioEduMasCercano").toString())))
//							: null);
//			solicitud.setTipoProveedorAgua((StringUtil.isNotNullOrBlank(request.getParameter("tipoProveedorAgua")))
//					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoProveedorAgua").toString())))
//					: new MaestroDTO(0L));
//			solicitud.setOtroProveedorAgua((StringUtil.isNotNullOrBlank(request.getParameter("otroProveedorAgua")))
//					? request.getParameter("otroProveedorAgua").toString()
//					: null);
//			solicitud.setSuministroAgua((StringUtil.isNotNullOrBlank(request.getParameter("suministroAgua")))
//					? request.getParameter("suministroAgua").toString()
//					: null);
//			solicitud
//					.setTipoProveedorEnergia((StringUtil.isNotNullOrBlank(request.getParameter("tipoProveedorEnergia")))
//							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoProveedorEnergia").toString())))
//							: new MaestroDTO(0L));
//			solicitud
//					.setOtroProveedorEnergia((StringUtil.isNotNullOrBlank(request.getParameter("otroProveedorEnergia")))
//							? request.getParameter("otroProveedorEnergia").toString()
//							: null);
//			solicitud.setSuministroEnergia((StringUtil.isNotNullOrBlank(request.getParameter("suministroEnergia")))
//					? request.getParameter("suministroEnergia").toString()
//					: null);
//			solicitud.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
//					.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString()))
//					.findFirst().get().getIdMaestro()));
//			solicitud.setGeoHash((StringUtil.isNotNullOrBlank(request.getParameter("geoHash")))
//					? request.getParameter("geoHash").toString()
//					: null);
//			solicitud.setUsuarioEnvio((StringUtil.isNotNullOrBlank(request.getParameter("usuarioEnvio")))
//					? request.getParameter("usuarioEnvio").toString()
//					: null);
//			solicitud.setNombreUsuarioEnvio((StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioEnvio")))
//					? request.getParameter("nombreUsuarioEnvio").toString()
//					: null);
//			solicitud.setCodSolicitud(
//					FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "yyyyMMddHHmmssSSS")
//							+ ((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
//									? request.getParameter("dreUgel").toString()
//									: 0));
//			solicitud.setFechaEnvio(FechaUtil.obtenerFechaActual());
//			solicitud.setUsuarioModificacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
//					? request.getParameter("usuarioModificacion").toString()
//					: null);
//			solicitud.setNombreUsuarioModificacion(
//					(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioModificacion")))
//							? request.getParameter("nombreUsuarioModificacion").toString()
//							: null);
//			solicitud.setUsuarioRevision((StringUtil.isNotNullOrBlank(request.getParameter("usuarioRevision")))
//					? request.getParameter("usuarioRevision").toString()
//					: null);
//			solicitud.setNombreUsuarioRevision(
//					(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevision")))
//							? request.getParameter("nombreUsuarioRevision").toString()
//							: null);
//			solicitud.setUsuarioRevisionSig((StringUtil.isNotNullOrBlank(request.getParameter("usuarioRevisionSig")))
//					? request.getParameter("usuarioRevisionSig").toString()
//					: null);
//			solicitud.setNombreUsuarioRevisionSig(
//					(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevisionSig")))
//							? request.getParameter("nombreUsuarioRevisionSig").toString()
//							: null);
//			solicitud.setFechaCreacion(FechaUtil.obtenerFechaActual());
//			solicitud.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
//					? request.getParameter("usuarioCreacion").toString()
//					: null);// Reemplazar con request de cliente
//			solicitud.setEstado((StringUtil.isNotNullOrBlank(request.getParameter("estado")))
//					? request.getParameter("estado").toString()
//					: EstadoState.ACTIVO.getValue());
//			solicitud.setTipoSituacionPrograma(
//					(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionPrograma")))
//							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSituacionPrograma").toString())))
//							: new MaestroDTO(0L));// Si es creacion de programa es nulo
//			solicitud.setTipoSituacionRevision(
//					(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionRevision")))
//							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSituacionRevision").toString())))
//							: new MaestroDTO(
//									listaTipoSituacionRevision
//											.stream().filter(
//													p -> p.getCodigoItem()
//															.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION
//																	.getKey().toString()))
//											.findFirst().get().getIdMaestro()));
//			solicitud
//					.setTipoSituacionRevisionSig(
//							(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionRevisionSig")))
//									? (new MaestroDTO(
//											Long.valueOf(request.getParameter("tipoSituacionRevisionSig").toString())))
//									: new MaestroDTO(listaTipoSituacionRevision
//											.stream().filter(
//													p -> p.getCodigoItem()
//															.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION
//																	.getKey().toString()))
//											.findFirst().get().getIdMaestro()));
//			solicitud.setIndicadorUltimo(EstadoState.INACTIVO.getValue());
//			// INCIO DOCUMENTO
//			DocumentoDTO documento = new DocumentoDTO();
//			documento.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
//					.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.RESOLUCION.getKey().toString())).findFirst()
//					.get().getIdMaestro()));
//
//			documento.setTipoDocumentoResolucion(
//					(StringUtil.isNotNullOrBlank(request.getParameter("tipoDocumentoResolucion")))
//							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoDocumentoResolucion").toString())))
//							: new MaestroDTO(0L));
//			documento.setNroDocumento((StringUtil.isNotNullOrBlank(request.getParameter("nroDocumento")))
//					? request.getParameter("nroDocumento").toString()
//					: null);
//			try {
//				Date fechaDocumento = FechaUtil
//						.obtenerFecha(StringUtil.isNotNullOrBlank(request.getParameter("fechaDocumento"))
//								? request.getParameter("fechaDocumento").toString()
//								: null);
//				documento.setFechaDocumento(fechaDocumento);
//			} catch (ParseException e1) {
//				documento.setFechaDocumento(null);
//				e1.printStackTrace();
//			}
//			documento.setCodigoDocumento((StringUtil.isNotNullOrBlank(request.getParameter("codigoDocumento")))
//					? request.getParameter("codigoDocumento").toString()
//					: null);
//			documento.setCodooii((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
//					? request.getParameter("dreUgel").toString()
//					: null);
//			documento.setFechaCreacion(FechaUtil.obtenerFechaActual());
//			documento.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
//					? request.getParameter("usuarioCreacion").toString()
//					: null);// Reemplazar con request de cliente
//			documento.setEstado((StringUtil.isNotNullOrBlank(request.getParameter("estado")))
//					? request.getParameter("estado").toString()
//					: EstadoState.ACTIVO.getValue());
//			documento.setIndCambioArchivos((StringUtil.isNotNullOrBlank(request.getParameter("indCambioArchivo")))
//					? request.getParameter("indCambioArchivo").toString()
//					: EstadoState.INACTIVO.getValue());
//			// INCIO ARCHIVO
//			// INICIO SUBIDA ARCHIVO
//			List<ArchivoDTO> listaArchivo = new ArrayList<ArchivoDTO>();
//			for (String file : listaFileNombreFinal) {
//				ArchivoDTO archivo = new ArchivoDTO();
//				char mander = file.charAt(file.length() - 5);
//				switch (mander) {
//				case 's':
//					archivo.setTipoArchivo(new MaestroDTO(listaTipoDocumento.stream()
//							.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
//							.findFirst().get().getIdMaestro()));// TipoArchivoCroquis, artificio para luego crear otro
//																// documento tipo croquis
//					break;
//				case '1':
//				case '2':
//				case '3':
//				case '4':
//				case '5':
//					archivo.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
//							.filter(p -> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString())).findFirst()
//							.get().getIdMaestro()));
//					break;
//				default:
//					break;
//				}
//				// Datos genericos en archivos
//				String nom = "";
//				int startIndex = file.lastIndexOf('\\');
//				int startIndexLinux = file.lastIndexOf('/');
//				if (startIndex != -1) {
//					nom = file.substring(startIndex + 1, file.length());
//				}
//				if (startIndexLinux != -1) {
//					nom = file.substring(startIndexLinux + 1, file.length());
//				}
//				archivo.setNombreArchivo(nom);
//				archivo.setCodigoArchivo(nom.substring(0, nom.length() - 4));
//				archivo.setFechaCreacion(FechaUtil.obtenerFechaActual());
//				archivo.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
//						? request.getParameter("usuarioCreacion").toString()
//						: null);// Reemplazar con request de cliente
//				archivo.setEstado(EstadoState.ACTIVO.getValue());
//				listaArchivo.add(archivo); // Agregar archivo a una lista Archivo
//			}
//			// FIN ARCHIVO
//			documento.setListaArchivo(listaArchivo);
//			// FIN DOCUMENTO
//			// Guardar en tabla SOLICITUD
//			solicitud.setDistrito((StringUtil.isNotNullOrBlank(request.getParameter("distrito")))
//					? (new DistritosDTO(request.getParameter("distrito").toString()))
//					: null);
//			// setear DreUgel
////			String dreUgel = request.getParameter("dreUgel");
//			solicitud.setDreUgel((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
//					? (new DreUgelDTO(request.getParameter("dreUgel").toString()))
//					: null);
//			AccionSolicitudDTO accionSolicitudDTO = new AccionSolicitudDTO();
//			// creacionDuro
//			accionSolicitudDTO.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
//					.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString()))
//					.findFirst().get().getIdMaestro()));// PENDIENTE
//			accionSolicitudDTO.setFechaCreacion(FechaUtil.obtenerFechaActual());
//			accionSolicitudDTO.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioCreacion")))
//					? request.getParameter("usuarioCreacion").toString()
//					: null);
//			accionSolicitudDTO.setEstado(EstadoState.ACTIVO.getValue());
//			List<AccionSolicitudDTO> listaAccionSolicitudDTO = new ArrayList<AccionSolicitudDTO>();
//			listaAccionSolicitudDTO.add(accionSolicitudDTO);
//			solicitud.setListaAccionSolicitud(listaAccionSolicitudDTO);
//			try {
//				
//		//		param = QueryParamURL.getParam(token.getToken(), "uescalet");
////				System.out.println("TOKRN : " + token.getToken() );
////				isOkTk=AuthUtils.validarTokenFormulario(token.getToken().toString());
////				if(isOkTk) {
//				solicitud = solicitudService.registrarSolicitudNuevo(solicitud, documento);
////				}else {
////					//verificacion del token
////					sucess = false;
//////					parametros.put("xtk", isOkTk);
//////					respuesta = new RespuestaDTO(true, "['']", parametros);
//////					respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
//////							+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
////					throw new SolicitudNoRegistradaException();
////					
////				}
//				
//				
//				
//				
//				
//			} catch (Exception e) {
//				sucess = false;
//				throw new SolicitudNoRegistradaException();
//			}
//			sucess = true;
//			parametros.put("solicitudRegistrada", true);
////			parametros.put("solicitud", solicitud);////////////////////////////////////////CAMBIO/////////////////////////////////////////////
////			if (solicitud.getTipoSolicitud().getIdMaestro().equals(271L)){
//			String prue = request.getParameter("tipoSolicitud");
////			if (prue.equals("271")){	//solicitudCierre
//			if (prue.equals(listaTipoSolicitud.stream()
//					.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey().toString())).findFirst()
//					.get().getIdMaestro().toString())) { // solicitudCierre
//				Map<Object, Object> parametro = new HashMap<>();
//				parametro.put("codigoModular", solicitud.getCodigoModular());
//				parametro.put("tipoSituacionSolicitud", listaTipoSituacionSolicitud.stream()
//						.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
//						.findFirst().get().getIdMaestro());
//				parametros.put("solicitudAnuladas",
//						solicitudService.listarSolicitudFirstMaxResultParametros(1000, 0, parametro));
//			}
//
////			setMensajeExito("1");//Solicitud registrada
//		} catch (Exception e) {
//			sucess = false;
//			mensaje = getBaseBean().getErrorService().getErrorFor(e, ResourceUtil.obtenerLocaleSession())
//					.getDefaultMessage();
//			parametros.put("solicitudRegistrada", false);
//
//			if (log.isHabilitadoError()) {
//				log.error(e);
//			}
//		}
//
//		finally {
//			respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
//
//			response.addHeader("x-frame-options", "DENY");
//			// response.addHeader("x-frame-options","SAMEORIGIN");
//			response.addHeader("X-XSS-Protection", "1; mode=block");
//			response.addHeader("X-Content-Type-Options", "nosniff");
//
//			response.setCharacterEncoding("UTF-8");
//			response.setContentType("text/html");
//
//			// respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt,
//			// iv, passphrase, gson.toJson(respuesta));
//			// response.getWriter().write(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString());
//
//			response.getWriter().write(gson.toJson(respuesta).toString());
//
//			/// return
//			/// Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
//
//		}
//	}


	public String getMensajeErrorArchivo() {
		return mensajeErrorArchivo;
	}

	public void setMensajeErrorArchivo(String mensajeErrorArchivo) {
		this.mensajeErrorArchivo = mensajeErrorArchivo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public BaseBean getBaseBean() {
		if (baseBean == null) {
			baseBean = new BaseBean();
		}
		return baseBean;
	}

	public void setBaseBean(BaseBean baseBean) {
		this.baseBean = baseBean;
	}

	// [0] Devuelve nomrbe archivo, [1] Nombre del parametro: ejem: fileCroquis
	private String[] getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				retornos = new String[2];
				retornos[0] = token.substring(token.indexOf("=") + 2, token.length() - 1);
				retornos[1] = part.getName();
				if (StringUtil.isNotNullOrBlank(retornos[0])) {
					return retornos;
				}
			}
		}
		return null;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup("java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/"
					+ nombreSimple.replace("Local", "") + "!" + nombreLargo);
		} catch (NamingException ne) {
			throw new RuntimeException(ne);
		}
	}
}
