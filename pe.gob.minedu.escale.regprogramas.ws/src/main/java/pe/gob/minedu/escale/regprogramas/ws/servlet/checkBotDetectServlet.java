package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

//import javax.json.stream.JsonParser;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.captcha.botdetect.web.servlet.SimpleCaptcha;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import pe.gob.minedu.escale.common.dto.rest.ExampleValidationResult;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.LogUtil;



@WebServlet("/svt/checkBotDetectServlet")
public class checkBotDetectServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String mensajeErrorArchivo  ="";
	private String mensaje = "";
	

	
	private Gson gson = new Gson();
	private ConcurrentHashMap<Object, Object> parametros;

		
	private static LogUtil log = new LogUtil(checkBotDetectServlet.class.getName());
	

	
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public checkBotDetectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
	      throws ServletException, IOException { 
	   
		Boolean sucess = null;
	    PrintWriter out = response.getWriter(); 
	    Gson gson = new Gson(); 
	    ExampleValidationResult validationResult = new ExampleValidationResult(); 
//	    PASAR EL SERVLET A UN SERVICIO.
//	    HACER LA SIGUIENTE SECUENCIA EN EL LOGIN:
//	    	PRIMERO VALIDA POR SERVICIOS Y AL FUINAL ENVIA EL FORM AL SERVLET
	    response.setContentType("application/json; charset=utf-8"); 
	    
	    JsonParser parser = new JsonParser(); 
	    JsonObject formDataObj = (JsonObject) parser.parse(request.getReader()); 
	    
	    String captchaId = formDataObj.get("captchaId").getAsString(); 
//	    String captchaCode = formDataObj.get("captchaCode").getAsString(); 
//	    String captchaId = formDataObj.get("captchaId").toString(); 
	    String captchaCode = formDataObj.get("captchaCode").toString(); 
	    
	  
	    // validate captcha 
//	    SimpleCaptcha captcha = SimpleCaptcha.load(request); 
	    
	    SimpleCaptcha captcha = SimpleCaptcha.load(request, "angularFormCaptcha");
	    
	    boolean isHuman = captcha.validate(captchaCode, captchaId); 
	    
	    if (isHuman) { 
	      // Captcha validation passed 
	      // TODO: do whatever you want here 
//	    	 validationResult.setSuccess(isHuman); 
	    	 validationResult.setIndel(0xFF12D001);
	    }
	    
	    try { 
	      // write the validation result as json string for sending it back to client 
	      out.write(gson.toJson(validationResult)); 
	    } catch(Exception ex) { 
	      out.write(ex.getMessage()); 
	    } finally { 
	      out.close(); 
	    } 
	  }



	public String getMensajeErrorArchivo() {
		return mensajeErrorArchivo;
	}

	public void setMensajeErrorArchivo(String mensajeErrorArchivo) {
		this.mensajeErrorArchivo = mensajeErrorArchivo;
	}				
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	
}
