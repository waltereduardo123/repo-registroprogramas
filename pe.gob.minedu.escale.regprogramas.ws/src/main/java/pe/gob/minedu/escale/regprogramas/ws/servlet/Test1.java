package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

public class Test1 {


		public void metodoOne() throws IOException {
		/////////////////////////////////////////////////////////////////////////////////////////////////
		//https://www.javamexico.org/blogs/ezamudio/ejemplo_de_ataque_de_negacion_de_servicio
		ServerSocket server = new ServerSocket(9123);
		//Este es un contador para saber cuantas conexiones tenemos abiertas
		//Aqui lo incrementamos y las conexiones lo decrementan
		AtomicInteger count = new AtomicInteger();
		//Ciclo interminable
		while (true) {
		        //Aceptamos una nueva conexion
		        Socket sock = server.accept();
		        //Incrementamos el contador, imprimiendo cuantas van
		        System.out.println(String.format("Nueva conexion, van %d", count.incrementAndGet()));
		        //Arrancamos la nueva conexion en un thread.
		        new Thread(new Conexion(sock, count)).start();
		}
		
		}
		
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////
		
	

}
