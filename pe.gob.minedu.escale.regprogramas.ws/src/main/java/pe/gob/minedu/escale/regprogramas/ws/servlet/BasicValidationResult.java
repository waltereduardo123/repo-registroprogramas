package pe.gob.minedu.escale.regprogramas.ws.servlet;

public class BasicValidationResult {
    private boolean success;

    public BasicValidationResult() {
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
