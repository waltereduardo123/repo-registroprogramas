package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;


import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;



import com.google.gson.Gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
//import com.sun.jersey.api.client.Client;
//import com.sun.jersey.api.client.ClientResponse;



import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.LogUtil;

import pe.gob.minedu.escale.regprogramas.ejb.service.AuditoriaProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SincronizarProgramaDTO;





/**
 * Servlet implementation class SincronizarProgramaServlet
 */
@WebServlet("/svt/sincronizarPrograma")
@MultipartConfig
public class SincronizarProgramaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
//	private static final String BASE_URI = "http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc/ccpp5k?codcp=112249";
//	private static final String BASE_URI = "http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc/ccpp5k";
	
	private static final String REST_URI ="http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc/ccpp5k";
	private Client client = ClientBuilder.newClient();
	
	private Gson gson = new Gson();
	
	private ConcurrentHashMap<Object, Object> parametros;
	private static LogUtil log = new LogUtil(SincronizarProgramaServlet.class.getName()); 
	@EJB
	private ProgramaServiceLocal programaService = lookup(ProgramaServiceLocal.class);
	
	@EJB
	private AuditoriaProgramaServiceLocal auditoriaProgramaService = lookup(AuditoriaProgramaServiceLocal.class);
	
	private String mensaje = "";
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public SincronizarProgramaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub/
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {			
		List<String> centroPobladoActivo=null;
		//		ProgramaDTO programasSincronizar=null;
		List<ProgramaDTO> programasSincronizar=null;
		AuditoriaProgramaDTO aud = new AuditoriaProgramaDTO();
	    Boolean sucess = null;
	    Boolean servicioDetenido=false;
	    Boolean cantProgramUpdate=false;
	    List<SincronizarProgramaDTO> listProgramaSincroniza=null;
	try {
	    
		parametros = new ConcurrentHashMap<>();
	    parametros.put("estado", EstadoState.ACTIVO.getValue());
	    parametros.put("estadotwo", EstadoState.NORENOVADO.getValue());
	    centroPobladoActivo= programaService.listCentroPobladoActivo(parametros);
	    
	    ProgramaDTO prBean=new ProgramaDTO();
	    int cantProgramaActualizado = 0;

	 //centros poblados recuperados para ser sincronizados.
	    listProgramaSincroniza= listProgramaSincroniza(centroPobladoActivo);
	    
if (!listProgramaSincroniza.isEmpty()) {
		 
		 for(SincronizarProgramaDTO sdto: listProgramaSincroniza){
				  ProgramaDTO prdto=new ProgramaDTO();
				  	parametros.put("estado", EstadoState.ACTIVO.getValue());
					parametros.put("estadotwo", EstadoState.NORENOVADO.getValue());
					parametros.put("centropoblado", sdto.getCodcp());
												
					programasSincronizar=programaService.listCentroPobladoActivoUpdate(parametros);
						//Registrar en auditoria
		 	    	   for(ProgramaDTO dtop:programasSincronizar){
		 	    		    prBean = programaService.buscarProgramaxCodigoModular(dtop.getCodigoModular());
							ConversorHelper.fusionaPropiedades(prBean, aud);
			 				aud.setIdPrograma(null);
			 				aud = auditoriaProgramaService.registrarProgramaNuevo(aud);  
			 				
							//--SINCRONIZAR LOS CENTROS POBLADOS	
							  prdto.setIdPrograma(prBean.getIdPrograma());
							  prdto.setDreUgel(prBean.getDreUgel());
							  prdto.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
							  
							
							  
							  prdto.setCodigoCentroPoblado(sdto.getCodcpReemp());
							  prdto.setArea(sdto.getArea());
							  prdto.setCodigoAreaSig(sdto.getAreaSig());
							  
							  prdto=programaService.actualizar(prdto);
							  cantProgramUpdate=true;
								//cantidad de centros poblados a ser sincronizados
								cantProgramaActualizado++;
		 	    	   }
						  			  
		  }		 
	 }else{

		 cantProgramUpdate=false;
	 }
 
		sucess = true;
		parametros.put("exito", true);
		parametros.put("listado", listProgramaSincroniza);	
		
//		parametros.put("listadoFilter", centrosSincronizarUpdate);
		if(!cantProgramUpdate){
			parametros.put("noSincroniza", true);
			//parametros.put("cantidad", cantProgramaActualizado);
		}else{
			parametros.put("noSincroniza", false);
			parametros.put("cantidad", cantProgramaActualizado); /////////////////////////////////////////////////////
		}
		if(servicioDetenido){
			parametros.put("servStop", true);
		}

		respuesta = new RespuestaDTO(sucess, "['" + "" + "']", parametros);
	    
		response.addHeader("x-frame-options", "DENY");
		//response.addHeader("x-frame-options","SAMEORIGIN");
		response.addHeader("X-XSS-Protection", "1; mode=block");
		response.addHeader("X-Content-Type-Options", "nosniff");
		
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		response.getWriter().write(gson.toJson(respuesta).toString());
//		response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSTEXTO.getKey().toString()));
	  }catch (Exception e){  

		  	sucess = false;
		  	parametros.put("errorGen", true);
		  	respuesta = new RespuestaDTO(sucess, "['" + "" + "']", parametros);
		  	
		  	response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html; charset=UTF-8");
			response.getWriter().write(gson.toJson(respuesta).toString());
		  
		  
	  }	
	}
		

private List<SincronizarProgramaDTO> listProgramaSincroniza(List<String> programaActivo){ 
//		HttpURLConnection conn = null; 

		JsonElement elementFilterRec = null;
        List<SincronizarProgramaDTO> programaSincronizar = new ArrayList<SincronizarProgramaDTO>();
		try{
			
			String cpobladotemp="";
//			programaSincronizarTemp=ConversorHelper.convertirTodo(ProgramaDTO.class, programaActivo);
			
			for(String spdto: programaActivo){
				cpobladotemp=spdto;
				elementFilterRec = obtenerReemplazo(spdto);
				if(elementFilterRec != null){
					
					if(!spdto.equals(elementFilterRec.getAsJsonObject().get("CODCP").getAsString())){//SI EXISTIO REEMPLAZO AL SER CODIGOS DIFERENTES EL ENVIADO Y EL RETORNADO POR EL METODO
						SincronizarProgramaDTO sdto=new SincronizarProgramaDTO();
						sdto.setCodcpReemp(elementFilterRec.getAsJsonObject().get("CODCP").getAsString());
						//sdto.setBorrado(elementFilterRec.getAsJsonObject().get("BORRADO").getAsString());
//						sdto.setCodcp(elementFilter.getAsJsonObject().get("CODCP").getAsString());	
						sdto.setCodcp(cpobladotemp);	
						sdto.setArea(elementFilterRec.getAsJsonObject().get("AREA").getAsString());
						sdto.setAreaSig(elementFilterRec.getAsJsonObject().get("AREA_SIG").getAsString());
						programaSincronizar.add(sdto);
						
					}
					
				}
				
			}

		}catch(Throwable t){
			t.printStackTrace(); 
		}
//		return programaSincronizar;
		System.out.println("TAMANO DE LOS SINCROIZADOS-->" + programaSincronizar.size() );
		return programaSincronizar;
	}

	private JsonElement obtenerReemplazo(String spdto) {
		
		String path= "";
		String line = null;
        String borrado="";
        String stService="";
        JSONObject stServiceObj = null;	
        JSONObject stObj = null;
        JsonParser parserOnFilter = null;
        JsonElement elementFilter =null;
        JSONArray arrayElemet=null;
		URL url  = null;
		
		do {
			
			try {
				path = "http://sigmed.minedu.gob.pe/servicios/rest/service/restsig.svc/ccpp5k?codcp="+spdto ;
				url = new URL(path);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setUseCaches(false);
		        InputStream is = conn.getInputStream();
		        InputStreamReader isr = new InputStreamReader(is);
		        BufferedReader brq = new BufferedReader(isr);
		        while ( (line = brq.readLine()) != null)
		        {
		        	stService=line;
		            stService = stService.substring(1,stService.length()-1);
		        }
				stService = stService.replaceAll("\\\\", ""); 
		        brq.close();
		        stServiceObj = new JSONObject(stService.toString());
		        
		        if(stServiceObj.getJSONArray("Rows") != null){
		        	
		        	//stObj = new JSONObject(stServiceObj.getJSONArray("Rows").get(0).toString());
		        	arrayElemet=stServiceObj.getJSONArray("Rows");
		        	for(int i=0; i<arrayElemet.length();i++){
		        		stObj = new JSONObject(stServiceObj.getJSONArray("Rows").get(i).toString());
			        	parserOnFilter = new JsonParser();
						elementFilter = parserOnFilter.parse(stObj.toString());
						borrado = elementFilter.getAsJsonObject().get("BORRADO").getAsString();
						if(!borrado.equals("0")){
							spdto = elementFilter.getAsJsonObject().get("CODCPDUP").getAsString();
						}
		        	}
		        

				
//					spdto = elementFilter.getAsJsonObject().get("CODCPDUP").getAsString();
//					if(!elementFilter.getAsJsonObject().get("CODCPDUP").getAsString().equals("null") && !elementFilter.getAsJsonObject().get("CODCPDUP").getAsString().trim().isEmpty()){
					

					
		       
		        }else{
		          System.out.println("dsdsdsdsdsd" + path );
		        }
		        
		        conn.disconnect();
		        
		        
			} catch(Throwable t){
				t.printStackTrace(); 
			}
			
		} while (borrado.equals("1"));
				
		return elementFilter;
	}
	
	

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup(
					"java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/" + nombreSimple.replace("Local", "")
							+ "!"+nombreLargo);
		} catch (NamingException ne) {
			if (log.isHabilitadoError()) log.error(ne);
			throw new RuntimeException(ne);
		}
	}
	
}
