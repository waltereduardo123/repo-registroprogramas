package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class Test {

	public static void main(String[] args) {


//		
//		GsonBuilder builder = new GsonBuilder();
//		builder.setVersion(2.0);
//		Gson gson = builder.create();
//		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
//		String fec="10/11/2015 12:12:34";
//		String fec="10/11/2015";
		Date fechaActual = new Date();
		String fechaSistemaInicio="05/02/2019";
		String fechaSistemaFin="30/12/2018";
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formateador1 = new SimpleDateFormat("dd/MM/yyyy");
       
        try {
			Date tesInicio=formateador.parse(fechaSistemaInicio);
			Date tesFin=formateador1.parse(fechaSistemaFin);
			
			
			int y=calcularRangoValidoFecha(tesInicio,tesFin );
			System.out.println("EL TOPE  :  " + y);
			
        } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}
	
	public static int calcularRangoValidoFecha(Date fechaInicio, Date fechaFin) {
        try {
            //Fecha inicio en objeto Calendar
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(fechaInicio);
            //Fecha finalización en objeto Calendar
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(fechaFin);
            //Cálculo de meses para las fechas de inicio y finalización
            int startMes = (startCalendar.get(Calendar.YEAR) * 12) + startCalendar.get(Calendar.MONTH);
            int endMes = (endCalendar.get(Calendar.YEAR) * 12) + endCalendar.get(Calendar.MONTH);
            //Diferencia en meses entre las dos fechas
            int diffMonth = endMes - startMes;
            return diffMonth;
        } catch (Exception e) {
            return 0;
        }
 }
	
	public static boolean verificaTopeInicio(Date fechaPeriodo, String periodo){
		boolean verifica=false;
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		String periodoFecha="01/01/"+periodo;
		
		Date periodoSel;
		try {
			Calendar calen=Calendar.getInstance();
			calen.setTime(sdf.parse(periodoFecha));
			calen.add(Calendar.DAY_OF_MONTH,-92);
			Date fechPeriodo=calen.getTime();
					
			if(fechaPeriodo.before(fechPeriodo)){
				verifica=true;
				System.out.println("NO SE LE DEJA QUE REGISTRE");
			}else{
				System.out.println("PUEDE CONTINUAR CON EL REGISTRO");
				verifica=false;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return verifica;
		
			
		}
	


}
