package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.apache.pdfbox.pdfparser.PDFParser;

import org.apache.poi.hssf.usermodel.HSSFRow;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import pe.gob.minedu.escale.adm.business.type.TipoArchivoType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoResolucionType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.business.type.TipoEstadoProgramaType;

import pe.gob.minedu.escale.adm.business.type.TipoSituacionProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionRevisionType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.ejb.service.SecuryTokenServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.TokenSecuryDTO;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.rest.auth.util.AuthUtils;
import pe.gob.minedu.escale.common.rest.auth.util.Token;
import pe.gob.minedu.escale.common.rest.util.AesUtil;
import pe.gob.minedu.escale.common.rest.util.QueryParamURL;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.LogUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.CadenaInvalidaException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoRegistradaException;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.service.AuditoriaProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudCargaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DistritosDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DreUgelDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.FormCreaSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.jpa.Documento;

/**
 * Servlet implementation class CreacionSolicitudCargaServlet
 */
@WebServlet("/svt/creacionSolicitudCarga")
@MultipartConfig
public class CreacionSolicitudCargaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ConcurrentHashMap<Object, Object> paramsQuery;

	private String mensajeErrorArchivo = "";
	private String mensaje = "";

	private Map<Object, Object> parametros;
	private Gson gson = new Gson();

	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;

	/** El objeto ErrorService. */
	private BaseBean baseBean;
	


	private String param, salt, iv;

	private int keySize = 128, iterationCount = 1000;

	private String passphrase = "0015975320171234";

	private AesUtil aesUtil;
	
	
	

	

	@EJB
	private transient SolicitudServiceLocal solicitudService = lookup(SolicitudServiceLocal.class);

	@EJB(beanName = "MaestroService")
	private transient MaestroServiceLocal maestroService;

	@EJB
	private transient SolicitudCargaServiceLocal solicitudCargaService = lookup(SolicitudCargaServiceLocal.class);

	@EJB
	private DataGeneralCache dataGeneralCache = lookup(DataGeneralCache.class);

	@EJB
	private transient ProgramaServiceLocal programaService = lookup(ProgramaServiceLocal.class);

	@EJB
	private AuditoriaProgramaServiceLocal auditoriaProgramaService = lookup(AuditoriaProgramaServiceLocal.class);

	@EJB
	private transient UsuarioServiceLocal usuarioServiceLocal = lookup(UsuarioServiceLocal.class);
	
	@EJB
	private transient SecuryTokenServiceLocal securyTokenServiceLocal = lookup(SecuryTokenServiceLocal.class);

	private static final String SITU_PROG_CERRADO = "299";
	private static final String SITU_PROG_ENREVISION = "297";
	private static final String SITU_PROG_MODIFICADO = "298";
	private static final String COD_DREUGEL_PROGRAMA = "UE0001";

	private String[] retornos;

	private List<MaestroDTO> listaTipoSolicitud = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionSolicitud = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionRevision = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoDocumento = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoArchivo = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoEstadoPrograma;

	private List<MaestroDTO> listaTipoSituacionPrograma;

	List<MaestroDTO> listaTipoDocumentoResolucion = new ArrayList<MaestroDTO>();

	private static LogUtil log = new LogUtil(CreacionSolicitudServlet.class.getName());

	public CreacionSolicitudCargaServlet() {
		super();
	}

	@PostConstruct
	public void iniciar() {
		// listado necesarios para la carga

		listaTipoSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionRevision = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionRevisionType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey());// pendiente,
																										// revision,observado,aprobado(2804),anulado,sustentado
		listaTipoArchivo = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoArchivoType.CODIGO_AGRUPACION.getKey()); // pdf(1),doc,docx,xls,xlsx,ppt,pptx,zip,otro
		listaTipoDocumento = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoDocumentoType.CODIGO_AGRUPACION.getKey()); // resolucion,
																								// croquis,instructivo,documento
																								// ue(2904)
		listaTipoDocumentoResolucion = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoDocumentoResolucionType.CODIGO_AGRUPACION.getKey()); // resolucion,
																											// croquis,instructivo,documento
																											// ue(2904)
		listaTipoEstadoPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoEstadoProgramaType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionProgramaType.CODIGO_AGRUPACION.getKey());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Boolean sucess = null;
		request.setCharacterEncoding("UTF-8");
		parametros = new HashMap<>();
		List<SolicitudDTO> listCarga = new ArrayList<SolicitudDTO>();
		List<SolicitudDTO> listCargaTmp = new ArrayList<SolicitudDTO>();

		List<ProgramaDTO> listPrograma = new ArrayList<ProgramaDTO>();
		paramsQuery = new ConcurrentHashMap<>();
		boolean enrevision = false;
		boolean codModularInexistente = false;
		boolean verMensajeCodModuInex = false;

		DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
		ServletFileUpload uploadHandler = null;

		String extensionpdf = "pdf";
		boolean showResponseTexto = false;
		boolean showResponseExcel = false;
		List<String> listaFileNombreFinal = new ArrayList<String>();

		String menasejeError = null;
//				SolicitudDTO solicitud = new SolicitudDTO();
		DocumentoDTO documentoCarga = new DocumentoDTO();
		AuditoriaProgramaDTO aud = new AuditoriaProgramaDTO();
		
		FormCreaSolicitudDTO formCreaSolicitud = null;
		
		String jsonEncrypteddcInexus = "";
		String jsonEncrypteddcJodexus = "";
		FormCreaSolicitudDTO objInexus = null;
		FormCreaSolicitudDTO objJodexus = null;
		
		Test1 tes = new Test1();
		aesUtil = new AesUtil(keySize, iterationCount);
		
		Token token;
		boolean isOkTk=false;

		try {
			
			String dataEnvio = StringUtil.isNotNullOrBlank(request.getParameter("dataEnvio"))
					? request.getParameter("dataEnvio").toString()
					: null;
			System.out.println("DATA RECIBIDA : " + dataEnvio);

			// data token inicio
			Map<String,Object> mensajes = new HashMap<>();
			mensajes.put("msgPorTk", "Intento fallido");	
			
			String dataEnvioOne = StringUtil.isNotNullOrBlank(request.getParameter("dataEnvioOne"))
					? request.getParameter("dataEnvioOne").toString()
					: null;
			System.out.println("DATA RECIBIDA ONE: " + dataEnvioOne);

			String dataEnvioTwo = StringUtil.isNotNullOrBlank(request.getParameter("dataEnvioTwo"))
					? request.getParameter("dataEnvioTwo").toString()
					: null;
			System.out.println("DATA RECIBIDA TWO: " + dataEnvioTwo);

			parametros.put("perfil", dataEnvioOne);
			parametros.put("codid", dataEnvioTwo); //
			parametros.put("identificador", passphrase);
			parametros.put("Mensajes", mensajes);

//			token = AuthUtils.createTokenValFormularios("127.0.0.1", parametros); /////////////////
		
				
			
			/////////////////////

//			
			param = QueryParamURL.getParam(dataEnvio, "jsonEncrypted");
			salt = QueryParamURL.getParam(dataEnvio, "salt");
			iv = QueryParamURL.getParam(dataEnvio, "iv");

////			
			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			System.out.println("DATA desencriptada : " + objJsonString);
//			
//		
			try {
				formCreaSolicitud = gson.fromJson(objJsonString, FormCreaSolicitudDTO.class);
				System.out.println(
						"OBJECTO BASE : " + formCreaSolicitud.getInexus() + " /// " + formCreaSolicitud.getJodexus());
			} catch (JsonSyntaxException je) {
				System.out.println("ERROR :  " + je);
			}

			try {
				jsonEncrypteddcInexus = aesUtil.descifrarBase64(formCreaSolicitud.getInexus().substring(9));

				objInexus = gson.fromJson(jsonEncrypteddcInexus, FormCreaSolicitudDTO.class);

				jsonEncrypteddcJodexus = aesUtil.descifrarBase64(formCreaSolicitud.getJodexus());

				objJodexus = gson.fromJson(jsonEncrypteddcJodexus, FormCreaSolicitudDTO.class);

				System.out.println("OBJETOS RECUPERADOS : " + objInexus + " ////  " + objJodexus);

			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}

            // tbl_regpro_maestro
            String idGestionX=objInexus.getX102();
            Long idGestion = Long.parseLong(idGestionX);
            
            String idDependenciaX=objInexus.getX103();
            Long idDependencia = Long.parseLong(idDependenciaX);
            List<MaestroDTO> listaMaestroHijosDeGestion = maestroService.buscarMaestrosxPadre(idGestion);
            
            if(listaMaestroHijosDeGestion.isEmpty()) { throw new CadenaInvalidaException();}
            else {
                  boolean idDependenciaExiste = false;
                idDependenciaExiste = listaMaestroHijosDeGestion.stream().anyMatch(
                        (p) -> p.getIdMaestro().equals(idDependencia));
                  if(!idDependenciaExiste) throw new CadenaInvalidaException();
            }
            
                            
            // tbl_regpro_maestro 
            String idGestoraX=objInexus.getX104();
            Long idGestora = Long.parseLong(idGestoraX);
            List<MaestroDTO> listaMaestroHijosDeDependencia = maestroService.buscarMaestrosxPadre(idDependencia);
            
            if(listaMaestroHijosDeDependencia.isEmpty()) { throw new CadenaInvalidaException();}
            else {
                  boolean idGestoraExiste = false;
                  idGestoraExiste = listaMaestroHijosDeDependencia.stream().anyMatch(
                             (p) -> p.getIdMaestro().equals(idGestora));            
                  if(!idGestoraExiste) throw new CadenaInvalidaException();
            }

			
			
			
			List<Object[]> listPrivilegios = usuarioServiceLocal.validarUserPerfilPriv(
					formCreaSolicitud.getUsuarioCreacion(), formCreaSolicitud.getPerfil(), objInexus.getInit());
			if (listPrivilegios == null || listPrivilegios.size() == 0) {
				throw new CadenaInvalidaException();
			}

			FormCreaSolicitudDTO objCampos = new FormCreaSolicitudDTO();
			objCampos.setX100(objInexus.getX100());
			objCampos.setX101(objInexus.getX101());
			objCampos.setX102(objInexus.getX102());
			objCampos.setX103(objInexus.getX103());
			objCampos.setX104(objInexus.getX104());
			objCampos.setX105(objInexus.getX105());
			objCampos.setX106(objInexus.getX106());
			objCampos.setX107(objInexus.getX107());
			objCampos.setX108(objInexus.getX108());
			objCampos.setX109(objInexus.getX109());
			objCampos.setX110(objInexus.getX110());
			objCampos.setX111(objInexus.getX111());
			objCampos.setX112(objInexus.getX112());
			objCampos.setX113(objInexus.getX113());
			objCampos.setX114(objInexus.getX114());
			objCampos.setX115(objInexus.getX115());
			objCampos.setX116(objInexus.getX116());
			objCampos.setX117(objInexus.getX117());
			objCampos.setX118(objInexus.getX118());
			objCampos.setX119(objInexus.getX119());
			objCampos.setX120(objInexus.getX120());
			objCampos.setX121(objInexus.getX121());
			objCampos.setX122(objInexus.getX122());
			objCampos.setX123(objInexus.getX123());
			objCampos.setX124(objInexus.getX124());
			objCampos.setX125(objInexus.getX125());
			objCampos.setX126(objInexus.getX126());
			objCampos.setX127(objInexus.getX127());
			objCampos.setX128(objInexus.getX128());
			objCampos.setX129(objInexus.getX129());
			objCampos.setX130(objInexus.getX130());
			objCampos.setX131(objInexus.getX131());
			objCampos.setX132(objInexus.getX132());
			objCampos.setX133(objInexus.getX133());
			objCampos.setX134(objInexus.getX134());
			objCampos.setX135(objInexus.getX135());
			objCampos.setX136(objInexus.getX136());
			objCampos.setX137(objInexus.getX137());
			objCampos.setX138(objInexus.getX138());
			objCampos.setX139(objInexus.getX139());
			objCampos.setX140(objInexus.getX140());
			objCampos.setX141(objInexus.getX141());
			objCampos.setX142(objInexus.getX142());
			objCampos.setX143(objInexus.getX143());
			objCampos.setX144(objInexus.getX144());
			objCampos.setX145(objInexus.getX145());
			objCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
			objCampos.setPerfil(objInexus.getPerfil());
			objCampos.setInit(objInexus.getInit());

			/////////////////////////////////////////////////////////////

//			
//			if(!jsonEncrypteddcJodexus.equals(jsonEncrypteddcInexus))
//			{
//				throw new CadenaInvalidaException();
//			}

			if (!objInexus.equals(objJodexus) || !objInexus.equals(objCampos) || !objJodexus.equals(objCampos)) {
//			if (!objInexus.equals(objJodexus)) {
				throw new CadenaInvalidaException();
			} else {
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			

			/* Obtener la lista de archivos */
			Collection<Part> listRequest = request.getParts();

//					listRequest.add(request.getParts());
			String[] fileNombre = null;
			String[] fileNameExcel = null;
			InputStream fileItem = null;
			int rows = 0;
			int filaInicio = 2;
			HSSFRow rowj = null;
			int columnsObligaria = 6;
			String mensajeExcelError = "-";
			boolean iniciarProcesamiento = false;
			/*********************/
			boolean mensajeValExcel = false;
			List<Documento> documentos = null;
			DocumentoDTO documentosdto = null;

			// ---------------inicio tratamiento de los
			// pdf-------------------------------------------------------
			Properties propiedades = new Properties();
			InputStream in = getClass()
					.getResourceAsStream("/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
			propiedades.load(in);
			String pathFile = propiedades.getProperty("directorio.path");
			String codigoDocumento = "";
			String fecha = "";
			String diaMes = "";
			String organizacion = "";
			String prefijo = "";
			String nuevoNombre = "";
			int contFile = 1;
			in.close();
			String[] fileNombrePdf = null;
			String fileNombreFinal = null;
			/* INCIO Verificar archivo valido PDF */
			boolean isPDF = true;
			// boolean isXSL=true;
			for (Part part : request.getParts()) {
				fileNombrePdf = getFileName(part);
				if (StringUtil.isNotNullOrBlank(fileNombrePdf) && fileNombrePdf[0].contains(".pdf")) {
					PDFParser parser = new PDFParser(part.getInputStream());
					try {
						parser.parse();
					} catch (IOException e) {
						isPDF = false;
					}
				}
				if (!isPDF) {
					break;
				}

			}

			String codigoDocumentoOrganizacion = request.getParameter("codigoDocumento").trim();
			String[] codoiidocu = codigoDocumentoOrganizacion.split("-");

			for (int i = 0; i < codoiidocu.length; i++) {
				prefijo = codoiidocu[1];
			}
			/* FIN Verificar archivo valido PDF */
			// Guardar Archivos
			for (Part part : request.getParts()) {
				fileNombrePdf = getFileName(part);
				if (StringUtil.isNotNullOrBlank(fileNombrePdf) && fileNombrePdf[0].contains(".pdf")) {
					codigoDocumento = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
							"ddMMyyyy");
					fecha = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
							"yyyyMMddHHmmssSSS");
					diaMes = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM");
					organizacion = StringUtil.isNotNullOrBlank(request.getParameter("tipoOrganizacion"))
							? request.getParameter("tipoOrganizacion").toString()
							: null;
					// prefijo=StringUtil.isNotNullOrBlank(request.getParameter("prefijo"))?request.getParameter("prefijo").toString():null;
					if (fileNombrePdf[1].contains("croquis")) {
						nuevoNombre = pathFile + organizacion + "-" + prefijo + "-" + codoiidocu[2] + "-"
								+ codigoDocumento + "-" + fecha + "-" + contFile + "-" + "croquis" + "." + extensionpdf;
					} else {
						nuevoNombre = pathFile + organizacion + "-" + prefijo + "-" + codoiidocu[2] + "-"
								+ codigoDocumento + "-" + fecha + "-" + contFile + "." + extensionpdf;
					}
					fileNombreFinal = nuevoNombre;
					part.write(fileNombreFinal);
					listaFileNombreFinal.add(fileNombreFinal);
					contFile++;
				}
			}
			// ---------------finaliza tratamiento de los
			// pdf-------------------------------------------------------
			// ---------------inicia registro de documento y
			// archivos-------------------------------------------------------

			documentoCarga.setTipoDocumento(new MaestroDTO(listaTipoDocumento.stream()
					.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.RESOLUCION.getKey().toString())).findFirst()
					.get().getIdMaestro()));
			documentoCarga.setTipoDocumentoResolucion(
					(StringUtil.isNotNullOrBlank(request.getParameter("tipoDocumentoResolucion")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoDocumentoResolucion").toString())))
							: null);

			try {
				Date fechaDocumento = FechaUtil
						.obtenerFecha(StringUtil.isNotNullOrBlank(request.getParameter("fechaDocumento"))
								? request.getParameter("fechaDocumento").toString()
								: null);
				documentoCarga.setFechaDocumento(fechaDocumento);
			} catch (ParseException e1) {
				documentoCarga.setFechaDocumento(null);
				e1.printStackTrace();
			}
			// documentoCarga.setFechaDocumento(FechaUtil.obtenerFechaActual());
			//////////// verificar
//			    				documentoCarga.setCodigoDocumento(request.getParameter("tipoOrganizacion")+"-"+request.getParameter("prefijo")+"-"+FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM")+"-"+ FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMMyyyy"));
//			    				documentoCarga.setCodigoDocumento((StringUtil.isNotNullOrBlank(request.getParameter("codigoDocumento")))?request.getParameter("codigoDocumento").toString():null);

			documentoCarga.setCodigoDocumento((StringUtil.isNotNullOrBlank(request.getParameter("codigoDocumento")))
					? request.getParameter("codigoDocumento").toString()
					: null);

			documentoCarga.setFechaCreacion(FechaUtil.obtenerFechaActual());
			documentoCarga.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
					? request.getParameter("usuarioModificacion").toString()
					: null);
			documentoCarga.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
			documentoCarga.setUsuarioUltimaModificacion("user");
			documentoCarga.setEstado("1");

//			    				documentoCarga.setCodooii((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))?request.getParameter("dreUgel").toString():null);
			documentoCarga.setCodooii(codoiidocu[0]);

			documentoCarga.setNroDocumento(codoiidocu[2]);

			documentoCarga.setIndCambioArchivos((StringUtil.isNotNullOrBlank(request.getParameter("indCambioArchivo")))
					? request.getParameter("indCambioArchivo").toString()
					: EstadoState.INACTIVO.getValue());
			documentoCarga.setIndCambioArchivoCroquis(
					(StringUtil.isNotNullOrBlank(request.getParameter("indCambioArchivoCroquis")))
							? request.getParameter("indCambioArchivoCroquis").toString()
							: EstadoState.INACTIVO.getValue());
			List<ArchivoDTO> listaArchivo = new ArrayList<ArchivoDTO>();
			for (String file : listaFileNombreFinal) {
				ArchivoDTO archivoCarga = new ArchivoDTO();
				char mander = file.charAt(file.length() - 5);
				switch (mander) { // tipoArchivo
				case 's':
					archivoCarga.setTipoArchivo(new MaestroDTO(listaTipoDocumento.stream()
							.filter(p -> p.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey().toString()))
							.findFirst().get().getIdMaestro()));

//			    						archivoCarga.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
//												  .filter(p-> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString()))
//												  .findFirst()
//												  .get()
//												  .getIdMaestro()));

					break;
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
					archivoCarga.setTipoArchivo(new MaestroDTO(listaTipoArchivo.stream()
							.filter(p -> p.getCodigoItem().equals(TipoArchivoType.PDF.getKey().toString())).findFirst()
							.get().getIdMaestro()));
					break;
				default:
					break;
				}
				String nom = "";
				int startIndex = file.lastIndexOf('\\');
				int startIndexLinux = file.lastIndexOf('/');
				if (startIndex != -1) {
					nom = file.substring(startIndex + 1, file.length());
				}
				if (startIndexLinux != -1) {
					nom = file.substring(startIndexLinux + 1, file.length());
				}
				archivoCarga.setNombreArchivo(nom);
				archivoCarga.setCodigoArchivo(nom.substring(0, nom.length() - 4));
				archivoCarga.setFechaCreacion(FechaUtil.obtenerFechaActual());
				archivoCarga
						.setUsuarioCreacion((StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
								? request.getParameter("usuarioModificacion").toString()
								: null);
				archivoCarga.setEstado(EstadoState.ACTIVO.getValue());
				listaArchivo.add(archivoCarga);
			}
			documentoCarga.setListaArchivo(listaArchivo);
//			    				documentosdto=solicitudCargaService.registrarDocumentoArchivoDTO(documentoCarga);
			documentos = solicitudCargaService.registrarDocumentoArchivoDTO(documentoCarga);
			// ---------------finaliza registro de documento y
			// archivos-------------------------------------------------------

			// Iniciamos el recorrido del excel y el procesamiento de la información

			String codModular = "", centroPob, area2000, areaSIG;
			double latitudP, longitudP;

			ProgramaDTO prBean = new ProgramaDTO();
			ProgramaDTO prBeanAud = new ProgramaDTO();
			ProgramaDTO prBeanUpd = new ProgramaDTO();
			SolicitudDTO solicitud = new SolicitudDTO();
			AccionSolicitudDTO accionSolicitudDTO = new AccionSolicitudDTO();

			// set necesarios para up del programa
			prBeanUpd.setNombrePrograma((Objects.nonNull(request.getParameter("nombrePrograma")))
					? request.getParameter("nombrePrograma").toString().toUpperCase()
					: null);
			prBeanUpd.setTipoPrograma((Objects.nonNull(request.getParameter("tipoPrograma")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoPrograma").toString())))
					: null);
			prBeanUpd.setTipoGestion((Objects.nonNull(request.getParameter("tipoGestion")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoGestion").toString())))
					: null);
			prBeanUpd.setTipoDependencia((Objects.nonNull(request.getParameter("tipoDependencia")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoDependencia").toString())))
					: null);
			prBeanUpd.setTipoGestionEducativa((Objects.nonNull(request.getParameter("tipoGestionEducativa")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoGestionEducativa").toString())))
					: null);
			prBeanUpd.setCodigoCentroPoblado((StringUtil.isNotNullOrBlank(request.getParameter("codigoCentroPoblado")))
					? request.getParameter("codigoCentroPoblado").toString()
					: null);
			prBeanUpd.setNombreCentroPoblado((StringUtil.isNotNullOrBlank(request.getParameter("nombreCentroPoblado")))
					? request.getParameter("nombreCentroPoblado").toString()
					: null);
			prBeanUpd.setLatitudPrograma(
					BigDecimal.valueOf(Double.parseDouble(request.getParameter("latitudPrograma"))));
			prBeanUpd.setLongitudPrograma(
					BigDecimal.valueOf(Double.parseDouble(request.getParameter("longitudPrograma"))));
			prBeanUpd.setArea((StringUtil.isNotNullOrBlank(request.getParameter("area")))
					? request.getParameter("area").toString()
					: null);
			prBeanUpd.setCodigoAreaSig((StringUtil.isNotNullOrBlank(request.getParameter("areaSig")))
					? request.getParameter("areaSig").toString()
					: null);
			prBeanUpd.setTipoVia((Objects.nonNull(request.getParameter("tipoVia")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoVia").toString())))
					: null);
			prBeanUpd.setNombreVia(
					(Objects.nonNull(request.getParameter("nombreVia"))) ? request.getParameter("nombreVia").toString()
							: null);
			prBeanUpd.setNumeroVia(
					(Objects.nonNull(request.getParameter("numeroVia"))) ? request.getParameter("numeroVia").toString()
							: null);
			prBeanUpd.setManzana(
					(Objects.nonNull(request.getParameter("manzana"))) ? request.getParameter("manzana").toString()
							: null);
			prBeanUpd.setLote(
					(Objects.nonNull(request.getParameter("lote"))) ? request.getParameter("lote").toString() : null);
			prBeanUpd.setTipoLocalidad((Objects.nonNull(request.getParameter("tipoLocalidad")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoLocalidad").toString())))
					: null);
			prBeanUpd.setLocalidad(
					(Objects.nonNull(request.getParameter("localidad"))) ? request.getParameter("localidad").toString()
							: null);
			prBeanUpd.setEtapa(
					(Objects.nonNull(request.getParameter("etapa"))) ? request.getParameter("etapa").toString() : null);
			prBeanUpd.setSector(
					(Objects.nonNull(request.getParameter("sector"))) ? request.getParameter("sector").toString()
							: null);
			prBeanUpd.setZona(
					(Objects.nonNull(request.getParameter("zona"))) ? request.getParameter("zona").toString() : null);
			prBeanUpd.setOtraDireccion((Objects.nonNull(request.getParameter("otraDireccion")))
					? request.getParameter("otraDireccion").toString()
					: null);
			prBeanUpd.setReferenciaDireccion((Objects.nonNull(request.getParameter("referenciaDireccion")))
					? request.getParameter("referenciaDireccion").toString()
					: null);
			prBeanUpd.setCodigoServicioEduMasCercano(
					(Objects.nonNull(request.getParameter("codigoServicioEduMasCercano")))
							? request.getParameter("codigoServicioEduMasCercano").toString()
							: null);
			prBeanUpd.setNombreServicioEduMasCercano(
					(Objects.nonNull(request.getParameter("nombreServicioEduMasCercano")))
							? request.getParameter("nombreServicioEduMasCercano").toString()
							: null);
			prBeanUpd.setLatitudServicioEduMasCercano(
					(Objects.nonNull(request.getParameter("latitudServicioEduMasCercano")))
							? (BigDecimal.valueOf(Double
									.parseDouble(request.getParameter("latitudServicioEduMasCercano").toString())))
							: null);
			prBeanUpd.setLongitudServicioEduMasCercano(
					(Objects.nonNull(request.getParameter("longitudServicioEduMasCercano")))
							? (BigDecimal.valueOf(Double
									.parseDouble(request.getParameter("longitudServicioEduMasCercano").toString())))
							: null);
			prBeanUpd.setTipoProveedorAgua((Objects.nonNull(request.getParameter("tipoProveedorAgua")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoProveedorAgua").toString())))
					: null);
			prBeanUpd.setOtroProveedorAgua((Objects.nonNull(request.getParameter("otroProveedorAgua")))
					? request.getParameter("otroProveedorAgua").toString()
					: null);
			prBeanUpd.setSuministroAgua((Objects.nonNull(request.getParameter("suministroAgua")))
					? request.getParameter("suministroAgua").toString()
					: null);
			prBeanUpd.setTipoProveedorEnergia((Objects.nonNull(request.getParameter("tipoProveedorEnergia")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoProveedorEnergia").toString())))
					: null);
			prBeanUpd.setOtroProveedorEnergia((Objects.nonNull(request.getParameter("otroProveedorEnergia")))
					? request.getParameter("otroProveedorEnergia").toString()
					: null);
			prBeanUpd.setSuministroEnergia((Objects.nonNull(request.getParameter("suministroEnergia")))
					? request.getParameter("suministroEnergia").toString()
					: null);
			prBeanUpd.setTipoTurno((Objects.nonNull(request.getParameter("tipoTurno")))
					? (new MaestroDTO(Long.valueOf(request.getParameter("tipoTurno").toString())))
					: null);
			prBeanUpd.setTipoContinuidadJornadaEscolar(
					(Objects.nonNull(request.getParameter("tipoContinuidadJornadaEscolar")))
							? (new MaestroDTO(
									Long.valueOf(request.getParameter("tipoContinuidadJornadaEscolar").toString())))
							: null);

			// buscamos en el programa, el codigo modular.
			try {
				prBean = programaService.buscarProgramaxCodigoModular(
						(StringUtil.isNotNullOrBlank(request.getParameter("codigoModular")))
								? request.getParameter("codigoModular").toString()
								: null);
//												  System.out.println("el errro[]"+prBean);

//												  if(codModularInexistente){
////						    	 						mensajeExcelError="No se actualizo. Código Modular no Existe.";
//						    	 						verMensajeCodModuInex=verMensajeCodModuInex&&codModularInexistente;
////						    	 						iniciarProcesamiento=false;
//												  			}

				if (Objects.nonNull(prBean)) { // codigo modular registrado en el programa
												// Setear los datos del programa a la solicitud
					solicitud.setCodSolicitud(FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
							"yyyyMMddHHmmssSSS")
							+ ((StringUtil.isNotNullOrBlank(prBean.getDreUgel().getCodigo()))
									? prBean.getDreUgel().getCodigo().toString()
									: 0));
					solicitud.setCodigoModular((StringUtil.isNotNullOrBlank(request.getParameter("codigoModular")))
							? request.getParameter("codigoModular").toString()
							: null);
					solicitud.setDreUgel((StringUtil.isNotNullOrBlank(request.getParameter("dreUgel")))
							? (new DreUgelDTO(request.getParameter("dreUgel").toString()))
							: null);
					solicitud.setDistrito((StringUtil.isNotNullOrBlank(request.getParameter("distrito")))
							? (new DistritosDTO(request.getParameter("distrito").toString()))
							: null);
					solicitud.setTipoSolicitud(new MaestroDTO(listaTipoSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CARGA.getKey().toString()))
							.findFirst().get().getIdMaestro()));
					solicitud.setTipoPrograma((Objects.nonNull(request.getParameter("tipoPrograma")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoPrograma").toString())))
							: null);
					solicitud.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionSolicitudType.APROBADO.getKey().toString()))
							.findFirst().get().getIdMaestro()));
					solicitud.setNombrePrograma((Objects.nonNull(request.getParameter("nombrePrograma")))
							? request.getParameter("nombrePrograma").toString().toUpperCase()
							: null);
					solicitud.setLatitudPrograma((Objects.nonNull(request.getParameter("latitudPrograma")))
							? (BigDecimal.valueOf(Double.parseDouble(request.getParameter("latitudPrograma"))))
							: null);
					solicitud.setLongitudPrograma((Objects.nonNull(request.getParameter("longitudPrograma")))
							? (BigDecimal.valueOf(Double.parseDouble(request.getParameter("longitudPrograma"))))
							: null);
					solicitud.setTipoGestion((Objects.nonNull(request.getParameter("tipoGestion")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoGestion").toString())))
							: null);
					solicitud.setTipoDependencia((Objects.nonNull(request.getParameter("tipoDependencia")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoDependencia").toString())))
							: null);
					solicitud.setTipoGestionEducativa((Objects.nonNull(request.getParameter("tipoGestionEducativa")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoGestionEducativa").toString())))
							: null);
					solicitud.setTipoTurno((Objects.nonNull(request.getParameter("tipoTurno")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoTurno").toString())))
							: null);
					solicitud.setTipoContinuidadJornadaEscolar(
							(Objects.nonNull(request.getParameter("tipoContinuidadJornadaEscolar")))
									? (new MaestroDTO(Long
											.valueOf(request.getParameter("tipoContinuidadJornadaEscolar").toString())))
									: null);
					solicitud.setTipoVia((Objects.nonNull(request.getParameter("tipoVia")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoVia").toString())))
							: null);
					solicitud.setNombreVia((Objects.nonNull(request.getParameter("nombreVia")))
							? request.getParameter("nombreVia").toString()
							: null);
					solicitud.setNumeroVia((Objects.nonNull(request.getParameter("numeroVia")))
							? request.getParameter("numeroVia").toString()
							: null);
					solicitud.setManzana((Objects.nonNull(request.getParameter("manzana")))
							? request.getParameter("manzana").toString()
							: null);
					solicitud.setLote(
							(Objects.nonNull(request.getParameter("lote"))) ? request.getParameter("lote").toString()
									: null);
					solicitud.setTipoLocalidad((Objects.nonNull(request.getParameter("tipoLocalidad")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoLocalidad").toString())))
							: null);
					solicitud.setLocalidad((Objects.nonNull(request.getParameter("localidad")))
							? request.getParameter("localidad").toString()
							: null);
					solicitud.setEtapa(
							(Objects.nonNull(request.getParameter("etapa"))) ? request.getParameter("etapa").toString()
									: null);
					solicitud.setSector((Objects.nonNull(request.getParameter("sector")))
							? request.getParameter("sector").toString()
							: null);
					solicitud.setZona(
							(Objects.nonNull(request.getParameter("zona"))) ? request.getParameter("zona").toString()
									: null);
					solicitud.setOtraDireccion((Objects.nonNull(request.getParameter("otraDireccion")))
							? request.getParameter("otraDireccion").toString()
							: null);
					solicitud.setReferenciaDireccion((Objects.nonNull(request.getParameter("referenciaDireccion")))
							? request.getParameter("referenciaDireccion").toString()
							: null);
					solicitud.setCodigoCentroPoblado((Objects.nonNull(request.getParameter("codigoCentroPoblado")))
							? request.getParameter("codigoCentroPoblado").toString()
							: null);
					solicitud.setNombreCentroPoblado((Objects.nonNull(request.getParameter("nombreCentroPoblado")))
							? request.getParameter("nombreCentroPoblado").toString()
							: null);
					solicitud.setCodigoArea((Objects.nonNull(request.getParameter("areaCentroPoblado")))
							? request.getParameter("areaCentroPoblado").toString()
							: null);
					solicitud.setCodigoAreaSig((Objects.nonNull(request.getParameter("codigoAreaSig")))
							? request.getParameter("codigoAreaSig").toString()
							: null);
					solicitud.setLatitudCentroPoblado((Objects.nonNull(request.getParameter("latitudCentroPoblado")))
							? (BigDecimal.valueOf(
									Double.parseDouble(request.getParameter("latitudCentroPoblado").toString())))
							: null);
					solicitud.setLongitudCentroPoblado((Objects.nonNull(request.getParameter("longitudCentroPoblado")))
							? (BigDecimal.valueOf(
									Double.parseDouble(request.getParameter("longitudCentroPoblado").toString())))
							: null);
					solicitud.setCodigoServicioEduMasCercano(
							(Objects.nonNull(request.getParameter("codigoServicioEduMasCercano")))
									? request.getParameter("codigoServicioEduMasCercano").toString()
									: null);
					solicitud.setNombreServicioEduMasCercano(
							(Objects.nonNull(request.getParameter("nombreServicioEduMasCercano")))
									? request.getParameter("nombreServicioEduMasCercano").toString()
									: null);
					solicitud
							.setLatitudServicioEduMasCercano(
									(Objects.nonNull(request.getParameter("latitudServicioEduMasCercano")))
											? (BigDecimal.valueOf(Double.parseDouble(
													request.getParameter("latitudServicioEduMasCercano").toString())))
											: null);
					solicitud
							.setLongitudServicioEduMasCercano(
									(Objects.nonNull(request.getParameter("longitudServicioEduMasCercano")))
											? (BigDecimal.valueOf(Double.parseDouble(
													request.getParameter("longitudServicioEduMasCercano").toString())))
											: null);
					solicitud.setTipoProveedorAgua((Objects.nonNull(request.getParameter("tipoProveedorAgua")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoProveedorAgua").toString())))
							: null);
					solicitud.setOtroProveedorAgua((Objects.nonNull(request.getParameter("otroProveedorAgua")))
							? request.getParameter("otroProveedorAgua").toString()
							: null);
					solicitud.setSuministroAgua((Objects.nonNull(request.getParameter("suministroAgua")))
							? request.getParameter("suministroAgua").toString()
							: null);
					solicitud.setTipoProveedorEnergia((Objects.nonNull(request.getParameter("tipoProveedorEnergia")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoProveedorEnergia").toString())))
							: null);
					solicitud.setOtroProveedorEnergia((Objects.nonNull(request.getParameter("otroProveedorEnergia")))
							? request.getParameter("otroProveedorEnergia").toString()
							: null);
					solicitud.setSuministroEnergia((Objects.nonNull(request.getParameter("suministroEnergia")))
							? request.getParameter("suministroEnergia").toString()
							: null);
					solicitud.setGeoHash((Objects.nonNull(request.getParameter("geoHash")))
							? request.getParameter("geoHash").toString()
							: null);
					solicitud.setUsuarioModificacion(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);
					solicitud.setNombreUsuarioModificacion(
							(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioModificacion")))
									? request.getParameter("nombreUsuarioModificacion").toString()
									: null);
					solicitud.setFechaModificacion(FechaUtil.obtenerFechaActual());
					solicitud.setEstado((StringUtil.isNotNullOrBlank(request.getParameter("estado")))
							? request.getParameter("estado").toString()
							: EstadoState.ACTIVO.getValue());
					solicitud.setTipoSituacionPrograma(
							(StringUtil.isNotNullOrBlank(request.getParameter("tipoSituacionPrograma")))
									? (new MaestroDTO(
											Long.valueOf(request.getParameter("tipoSituacionPrograma").toString())))
									: null);
					solicitud.setIndicadorUltimo(EstadoState.INACTIVO.getValue());

					solicitud.setUsuarioEnvio((StringUtil.isNotNullOrBlank(request.getParameter("usuarioRevision")))
							? request.getParameter("usuarioRevision").toString()
							: null);
					solicitud.setNombreUsuarioEnvio(
							(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioRevision")))
									? request.getParameter("nombreUsuarioRevision").toString()
									: null);
					solicitud.setFechaEnvio(FechaUtil.obtenerFechaActual());
//													   		solicitud.setUsuarioModificacion(request.getParameter("usuarioRevision"));
//													   		solicitud.setNombreUsuarioModificacion(request.getParameter("usuarioRevision"));
					solicitud.setUsuarioRevision(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);
					solicitud.setNombreUsuarioRevision(
							(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioModificacion")))
									? request.getParameter("nombreUsuarioModificacion").toString()
									: null);
					solicitud.setUsuarioRevisionSig(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);
					solicitud.setNombreUsuarioRevisionSig(
							(StringUtil.isNotNullOrBlank(request.getParameter("nombreUsuarioModificacion")))
									? request.getParameter("nombreUsuarioModificacion").toString()
									: null);
					solicitud.setFechaRevisionSig(FechaUtil.obtenerFechaActual());
					solicitud.setFechaAtencion(FechaUtil.obtenerFechaActual());
					solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.APROBADO.getKey().toString()))
							.findFirst().get().getIdMaestro()));
					solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.APROBADO.getKey().toString()))
							.findFirst().get().getIdMaestro()));
					solicitud.setFechaCreacion(FechaUtil.obtenerFechaActual());
					solicitud.setUsuarioCreacion(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);
					solicitud.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
					solicitud.setUsuarioUltimaModificacion(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);

					// Las acciones de la solicitud
					accionSolicitudDTO.setTipoSituacionSolicitud(new MaestroDTO(listaTipoSituacionSolicitud.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionSolicitudType.APROBADO.getKey().toString()))
							.findFirst().get().getIdMaestro()));// APROBADO
					accionSolicitudDTO.setFechaCreacion(FechaUtil.obtenerFechaActual());
					accionSolicitudDTO.setUsuarioCreacion(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);
					accionSolicitudDTO.setEstado(EstadoState.ACTIVO.getValue());
					List<AccionSolicitudDTO> listaAccionSolicitudDTO = new ArrayList<AccionSolicitudDTO>();
					listaAccionSolicitudDTO.add(accionSolicitudDTO);
					solicitud.setListaAccionSolicitud(listaAccionSolicitudDTO);

					// Finalizar los seteaos los datos del programa a la solicitud

					showResponseExcel = true;// Siempre se descarga el excel
					// Se inicia el procesamiento.
//															if(iniciarProcesamiento){
					// Registro de la solicitud
					solicitud = solicitudCargaService.registrarSolicitudCarga(solicitud, documentos);
					// actualizar el programa
					prBeanUpd.setIdPrograma(prBean.getIdPrograma());
					prBeanUpd.setDreUgel(prBean.getDreUgel());// codigoDocumento
//														   			   prBeanUpd.setDreUgel(new DreUgelDTO(COD_DREUGEL_PROGRAMA.trim()));

//														   			   prBeanUpd.setTipoResolucion(new MaestroDTO(listaTipoDocumentoResolucion.stream()
//														   					   	.filter(p-> p.getCodigoItem().equals(TipoDocumentoResolucionType.OTROUE.getKey().toString())).findFirst().get().getIdMaestro()));

					prBeanUpd.setTipoResolucion(
							(StringUtil.isNotNullOrBlank(request.getParameter("tipoDocumentoResolucion")))
									? (new MaestroDTO(
											Long.valueOf(request.getParameter("tipoDocumentoResolucion").toString())))
									: null);

					//

					prBeanUpd.setNumeroDocumento(
							FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM"));

//														   			   prBeanUpd.setFechaDocumento(FechaUtil.obtenerFechaActual());

					try {
						Date fechaDocumento = FechaUtil
								.obtenerFecha(StringUtil.isNotNullOrBlank(request.getParameter("fechaDocumento"))
										? request.getParameter("fechaDocumento").toString()
										: null);
						prBeanUpd.setFechaDocumento(fechaDocumento);
					} catch (ParseException e1) {
						prBeanUpd.setFechaDocumento(null);
						e1.printStackTrace();
					}

//														   			   prBeanUpd.setCodigoDocumento(request.getParameter("tipoOrganizacion")+"-"+request.getParameter("prefijo")+"-"+FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM")+"-"+ FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMMyyyy"));

					prBeanUpd.setCodigoDocumento((StringUtil.isNotNullOrBlank(request.getParameter("codigoDocumento")))
							? request.getParameter("codigoDocumento").toString()
							: null);

//														   			   prBeanUpd.setCodigoDocumento(request.getParameter("tipoOrganizacion")+"-"+request.getParameter("prefijo")+"-"+FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMM")+"-"+ FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "ddMMyyyy"));

//														   			   prBeanUpd.setTipoEstado(new MaestroDTO(listaTipoEstadoPrograma.stream()
//																			  .filter(p-> p.getCodigoItem().equals(TipoEstadoProgramaType.ACTIVO.getKey()))
//																			  .findFirst()
//																			  .get().getIdMaestro()));

					// prBeanUpd.setTipoSituacion(prBean.getTipoSituacion()); //Se comenta por
					// asignacion de edicion al administrador.

					prBeanUpd.setTipoSituacion((Objects.nonNull(request.getParameter("tipoSituacion")))
							? (new MaestroDTO(Long.valueOf(request.getParameter("tipoSituacion").toString())))
							: null);

//														   			   prBeanUpd.setTipoSituacion(new MaestroDTO(Long.parseLong(SITU_PROG_MODIFICADO.trim()))); //Se comenta por inconsistencia con el reinicio de los programas werr

					/*
					 * prBeanUpd.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
					 * .filter(p->
					 * p.getCodigoItem().equals(TipoSituacionProgramaType.MODIFICADO.getKey()))
					 * .findFirst() .get() .getIdMaestro())); //Se comenta por inconsistencia con el
					 * reinicio de los programas werr
					 * 
					 */

					prBeanUpd.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
					prBeanUpd.setUsuarioUltimaModificacion(
							(StringUtil.isNotNullOrBlank(request.getParameter("usuarioModificacion")))
									? request.getParameter("usuarioModificacion").toString()
									: null);
					
					
					
					try {

						// leer el token registrado
						List<TokenSecuryDTO> listk=new ArrayList<TokenSecuryDTO>();
//						TokenSecuryDTO del=new TokenSecuryDTO();
						listk=securyTokenServiceLocal.listVerificarTokenForm();
						//if(listk.size()>0) {
						String tkform=listk.get(0).getToken();
						System.out.println("EL TOKEN : " + tkform );
						
						isOkTk=AuthUtils.validarTokenFormulario(tkform);
//						isOkTk=AuthUtils.validarTokenFormulario(token.getToken().toString());
						if(isOkTk) {
						
								prBeanUpd = programaService.actualizar(prBeanUpd);
								sucess = true;
								parametros.put("clean", true);
						}else {
							//verificacion del token
							sucess = false;
							parametros.put("clean", false);

							throw new SolicitudNoRegistradaException();
//							
						}	

					}catch(Exception e) {
						sucess = false;
						parametros.put("clean", false);
						throw new SolicitudNoRegistradaException();
						
					}
					
					
					
					
					
					
					
					// registrar la auditoria del programa
//														   			prBeanAud = programaService.buscarProgramaxCodigoModular(solicitud.getCodigoModular());
//																	ConversorHelper.fusionaPropiedades(prBeanAud, aud);
					ConversorHelper.fusionaPropiedades(prBean, aud);
					aud.setIdPrograma(null);
					aud = auditoriaProgramaService.registrarProgramaNuevo(aud);

					sucess = true;
				}
//													   }
			} catch (Exception e)

			{
				sucess = false;
//													  codModularInexistente=true;
//													  verMensajeCodModuInex=true;
//													  iniciarProcesamiento=false;
//													  mensajeExcelError="";

			}

//					if(showResponseExcel){
//						response.setContentType("application/vnd.ms-excel");
//				        response.setHeader("Content-disposition", "attachment; filename=test.xls");
//				        response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSEXCEL.getKey().toString()));
//			
//			
//				   
//				        
//					}
//					
//					if(showResponseTexto){
//						
//						response.setCharacterEncoding("UTF-8");
//						response.setContentType("text/html");	                        
//						response.getWriter().write(gson.toJson(respuesta).toString());
//						response.setStatus(Integer.parseInt(TipoDocumentoType.STATUSTEXTO.getKey().toString()));
//				}
			}

		} catch (Exception e) {
			sucess = false;
			mensaje = getBaseBean().getErrorService().getErrorFor(e, ResourceUtil.obtenerLocaleSession())
					.getDefaultMessage();
			parametros.put("solicitudRegistrada", false);
			if (log.isHabilitadoError()) {
				log.error(e);
			}
		} finally {
			respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);

			response.addHeader("x-frame-options", "DENY");
			// response.addHeader("x-frame-options","SAMEORIGIN");
			response.addHeader("X-XSS-Protection", "1; mode=block");
			response.addHeader("X-Content-Type-Options", "nosniff");

			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");
			response.getWriter().write(gson.toJson(respuesta).toString());
		}

	}

	public String getMensajeErrorArchivo() {
		return mensajeErrorArchivo;
	}

	public void setMensajeErrorArchivo(String mensajeErrorArchivo) {
		this.mensajeErrorArchivo = mensajeErrorArchivo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public BaseBean getBaseBean() {
		if (baseBean == null) {
			baseBean = new BaseBean();
		}
		return baseBean;
	}

	public void setBaseBean(BaseBean baseBean) {
		this.baseBean = baseBean;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup("java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/"
					+ nombreSimple.replace("Local", "") + "!" + nombreLargo);
		} catch (NamingException ne) {
			throw new RuntimeException(ne);
		}

	}

	// [0] Devuelve nomrbe archivo, [1] Nombre del parametro: ejem: fileCroquis
	private String[] getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				retornos = new String[2];
				retornos[0] = token.substring(token.indexOf("=") + 2, token.length() - 1);
				retornos[1] = part.getName();
				if (StringUtil.isNotNullOrBlank(retornos[0])) {
					return retornos;
				}
			}
		}
		return null;
	}

}
