package pe.gob.minedu.escale.regprogramas.ws.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class ConverterFile {

	public ConverterFile() {
		super();
		// TODO Auto-generated constructor stub Existen mas de
	} 

//	public List flujoAprobadoresXlsxToList(File fileNameXlsx,Map dbean) {
	public List flujoAprobadoresXlsxToList(InputStream fileNameXlsx,Map dbean) {		
		List fileDataList = new ArrayList();
//		FileInputStream fileInputStream = null; //FileInputStream
		Workbook workBook = null;
		
		try {
//			fileInputStream = new FileInputStream(fileNameXlsx);
//			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
			HSSFWorkbook workbook = new HSSFWorkbook(fileNameXlsx);
			HSSFSheet hssfSheet = workbook.getSheetAt(0);
			String[] cabecera = (String[])dbean.get("cabeceraXlsx");
			int rowIniData = ((Integer) dbean.get("rowIniData")).intValue();
			int numFilas = hssfSheet.getLastRowNum() + 1;
			int numColums= 0;
			HashMap hmFila=null;	
			//Segun el formato la data se contiene desde la fila 2
			for(int y=rowIniData-1;y<numFilas;y++){
				HSSFRow hssfRow =  hssfSheet.getRow(y);
				
				hmFila= new HashMap();
				numColums = cabecera.length;
				System.out.println("numColums[]"+numColums);
				for(int x=0;x<numColums;x++) {
					HSSFCell hssfCell = (HSSFCell) hssfRow.getCell(x);
					hmFila.put(cabecera[x],hssfCell==null?"": hssfCell.toString().trim());					
				}
			//	hmFila.put("estado", "OK");
				fileDataList.add(hmFila);
			}
		} catch (Exception e) {
			System.out.println("Error en la conversion del excel []"+e);		
		}finally{
			if(fileNameXlsx != null){
			//	fileInputStream.close();				
			}
		}
		return fileDataList;
	}	
	
}
//
//dbean.put("rowIniData", Integer.valueOf(String.valueOf(2)));
//dbean.put("nameExcel", fileItem.getName());
//dbean.put("tamanio", fileItem.getSize());
//dbean.put("cabeceraXlsx", cabecera);