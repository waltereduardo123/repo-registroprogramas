package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.poi.ss.usermodel.Cell;


import com.google.gson.Gson;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import pe.gob.minedu.escale.adm.business.type.TipoEstadoProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoPeriodoType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionRevisionType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.PeriodoServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.PeriodoDTO;

/**
 * Servlet implementation class GestionarPeriodosServlet 
 */
@WebServlet("/svt/gestionarPeriodos")
@MultipartConfig
public class GestionarPeriodosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String mensaje = "";
	private ConcurrentHashMap<Object, Object> parametros;
	private Gson gson = new Gson();
	private static final String GRUPO_PERIODO_MAESTRO = "34";
    
    List<MaestroDTO> listaTipoSolicitud = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoSituacionSolicitud = new ArrayList<MaestroDTO>();

	List<MaestroDTO> listaTipoSituacionRevision = new ArrayList<MaestroDTO>();

	private List<MaestroDTO> listaTipoSituacionPrograma;

	private List<MaestroDTO> listaTipoEstadoPrograma;
	
	private List<MaestroDTO> listaTipoPeriodo;
	
	@EJB
	private transient PeriodoServiceLocal periodoService = lookup(PeriodoServiceLocal.class);
	
	@EJB
	private transient MaestroServiceLocal maestroService = lookup(MaestroServiceLocal.class); 
	
	@EJB
	private DataGeneralCache dataGeneralCache = lookup(DataGeneralCache.class);
	
	@EJB
	private ProgramaServiceLocal programaService = lookup(ProgramaServiceLocal.class);
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
    public GestionarPeriodosServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	@PostConstruct
	public void iniciar() {
		// listado necesarios para la carga

		listaTipoSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionRevision = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionRevisionType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey());


		listaTipoEstadoPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoEstadoProgramaType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionProgramaType.CODIGO_AGRUPACION.getKey());
		
		listaTipoPeriodo=dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoPeriodoType.CODIGO_AGRUPACION.getKey());
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Boolean sucess=null;
		boolean perRegisExis=false;
		boolean haySolapamiento=false;
		boolean fechaAnioIncorrecto=false;
		boolean fechaInicioPeriodoIncorrecto=false;
		boolean recargaPagina=false;
		boolean fechaInicioIncorrecto=false;
		boolean iniciaReinicio=false;
		boolean inReinicio=false;
		boolean initRueda=false;
		parametros = new ConcurrentHashMap<>();
		String namePeriodo="";
		String periodo="";
		String fecIniPeriodo="";
		String fecFinPeriodo="";
		String usuCrea="";
		Collection<Part> listRequest=null;
		Integer cantReinicia = 0;
		
		PeriodoDTO perdto=new PeriodoDTO();
		PeriodoDTO perdtobd=new PeriodoDTO();
		MaestroDTO maesdto=new MaestroDTO();
		
		String grupo="";
		String item="";
		
		
		try{		
			
//			listRequest=request.getParts();	
			
			
			
//			for (Part part : request.getParts()) {
			
	  //obtencion de los request necesarios
			namePeriodo=StringUtil.isNotNullOrBlank(request.getParameter("namePeriodo"))? request.getParameter("namePeriodo").toString():null;
			periodo=StringUtil.isNotNullOrBlank(request.getParameter("periodo"))? request.getParameter("periodo").toString():null;
			System.out.println("LLEGAMOS AL SERVLET DOPOST "  + namePeriodo + "//"  + periodo );
			fecIniPeriodo=StringUtil.isNotNullOrBlank(request.getParameter("fecIniPeriodo"))? request.getParameter("fecIniPeriodo").toString():null;
//			fecIniPeriodo=request.getParameter("fecIniPeriodo");
			SimpleDateFormat sdfinicio=new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdffin=new SimpleDateFormat("dd/MM/yyyy");
			fecFinPeriodo=StringUtil.isNotNullOrBlank(request.getParameter("fecFinPeriodo"))? request.getParameter("fecFinPeriodo").toString():null;
			usuCrea=StringUtil.isNotNullOrBlank(request.getParameter("usuCrea"))? request.getParameter("usuCrea").toString():null;
					
			Calendar cal= Calendar.getInstance();
			int year= cal.get(Calendar.YEAR);
			String anioFin="31-12-"+year;
			SimpleDateFormat sdfi1=new SimpleDateFormat("dd-MM-yyyy");
			
			
			namePeriodo="PERIODO " +  periodo;
			//Registro del periodo
			perdto.setNamePeriodo(namePeriodo);
					


			
			Date fechtoday=new Date();
			
				perdto.setFechaInicioPeriodo(sdfinicio.parse(fecIniPeriodo));
				perdto.setFechaFinPeriodo(sdffin.parse(fecFinPeriodo));
				perdto.setFechaFinAnio(sdfi1.parse(anioFin));
				perdto.setCodGrupo(GRUPO_PERIODO_MAESTRO);
				perdto.setCodItem(String.valueOf(periodo));
				perdto.setUserCrea(usuCrea);
				perdto.setFecCrea(FechaUtil.obtenerFechaActual());
				perdto.setEstado(EstadoState.ACTIVO.getValue());
				
				//Verificacion de fechas correspondiente al periodo ingresado
//				if(obtenerAnio(perdto.getFechaInicioPeriodo())!=Integer.parseInt(periodo)||obtenerAnio(perdto.getFechaFinPeriodo())!=Integer.parseInt(periodo)){
//				if(obtenerAnio(perdto.getFechaFinPeriodo())!=Integer.parseInt(periodo)){
				if(verificaTopeInicio(perdto.getFechaInicioPeriodo(),periodo)){
					fechaInicioPeriodoIncorrecto=true;
					fechaAnioIncorrecto=false;
				}else if(comparaFecha(fechtoday,fecIniPeriodo)<0){
					//comparaFecha(fechtoday,fecIniPeriodo)!=0||
							fechaInicioIncorrecto=true;
					
				}else if(obtenerAnio(perdto.getFechaFinPeriodo())!=Integer.parseInt(periodo)){
					fechaAnioIncorrecto=true;
				}
				else{
					fechaAnioIncorrecto=false;
					
					//verificar solapamiento de fechas
					//1. BUSCAR ULTIMO PERIODO REGISTRADO SEGUN SELECION DE USUARIO
					List<PeriodoDTO> lisper=periodoService.buscarPeriodoxNombreItem(perdto.getNamePeriodo(), perdto.getCodItem() );
					System.out.println("listado--> " + lisper );
					
					if(lisper.size()==1){
						System.out.println("YA EXISTE UN REGISTRO EN ESTE PERIODO " + lisper );
						perRegisExis=true;
					}else if(lisper.size()==0){
						// MANDAR A VALIDAR LOS SOLAPAMIENTOS 
						// BUSCAR ULTIMO PERIODO REGISTRADO EN BD 
//						namePeriodo="PERIODO " +  year;
						namePeriodo="PERIODO " +  periodo;
						perdtobd.setNamePeriodo(namePeriodo);
//						perdtobd.setCodItem(String.valueOf(year));
						perdtobd.setCodItem(String.valueOf(periodo));
						List<PeriodoDTO> lisperbd=periodoService.buscarPeriodoxNombreItem(perdtobd.getNamePeriodo(), perdtobd.getCodItem() );
						System.out.println("listado--> " + lisperbd );
						
//								if(verificaNoSolapamiento( lisperbd.get(0).getFechaInicioPeriodo(), lisperbd.get(0).getFechaFinPeriodo(), perdto.getFechaInicioPeriodo(), perdto.getFechaFinPeriodo() )){
						
							//EN CASO EXISTA ALGUN PERIODO ACTIVO.
						int periodoAnt=Integer.parseInt(periodo)-1;
						String namePeriodoAnt="PERIODO " +  String.valueOf(periodoAnt);
						PeriodoDTO perdtoAnt=new PeriodoDTO();
						perdtoAnt.setNamePeriodo(namePeriodoAnt);
						perdtoAnt.setCodItem(String.valueOf(periodoAnt));
						
						List<PeriodoDTO> lisperuP=periodoService.buscarPeriodoxNombreItem(perdtoAnt.getNamePeriodo(), perdtoAnt.getCodItem() );

						// ACTUALIZAR LOS ESTADOS DE LOS DEMAS PERIODOS, EXCEPTO EL ULTIMO REGISTRADO
									int idPeriod=0;
									int idPeriodoTem=0;
									//get ultimo registrado
									for(int j=0;j<lisperuP.size();j++){
										idPeriodoTem=lisperuP.get(j).getIdPeriodo();
										PeriodoDTO perUpdt=new PeriodoDTO();
										perUpdt.setIdPeriodo(idPeriodoTem);
										perUpdt.setEstado(EstadoState.INACTIVO.getValue());
										
										perUpdt=periodoService.actalizarPeriodo(perUpdt);
									///////////////////////////////////////	recargaPagina=true;
									}
															
						   /////////////////
								perdto=periodoService.registrarNuevoPeriodo(perdto);
				
								System.out.println("PERIODO REGISTRADO --> " +perdto );
								recargaPagina=true;
								//Registro del maestro
								maesdto.setCodigoAgrupacion(GRUPO_PERIODO_MAESTRO);
								maesdto.setNombreMaestro(String.valueOf(periodo));//year
								maesdto.setDescripcionMaestro("PERIODO " + periodo );//year
								maesdto.setCodigoItem(perdto.getCodItem());
								maesdto.setFechaCreacion(perdto.getFecCrea());
								maesdto.setUsuarioCreacion(perdto.getUserCrea());
								maesdto.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
								maesdto.setUsuarioUltimaModificacion(perdto.getUserCrea());
								maesdto.setEstado(EstadoState.ACTIVO.getValue());
								
								//VERIFICACION DE EXISTENCIA DE REGISTRO EN MAESTRO
								
								if(maestroService.buscarMaestrosxGrupoItem(maesdto.getCodigoAgrupacion(), maesdto.getCodigoItem()).size()==0){
									maesdto=maestroService.registrarMaestroNuevo(maesdto);
								////////////////////////////////////////////////////////////////	recargaPagina=true;
									parametros.put("finanio", perdto.getFechaFinAnio());
									iniciaReinicio=true;
								}else{
									System.out.println("YA EXISTE UN REGISTRO PARA ESE ITEM Y ESE GRUPO --> " +maesdto );
									//recargaPagina=false;
								}
								
								
								
								//// INICIA EL REINICIO DE LOS PROGRAMAS
							if(iniciaReinicio){
								//buscar los programas que no esten cerrados o deshecho
								
								initRueda=true;
								parametros.put("estado", EstadoState.ACTIVO.getValue());
								MaestroDTO maestroSituacion=new MaestroDTO(listaTipoSituacionPrograma.stream().filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.PORACTUALIZAR.getKey()))
												.findFirst().get().getIdMaestro());
								
								MaestroDTO maestroEstado=new MaestroDTO(listaTipoEstadoPrograma.stream()
										  .filter(p-> p.getCodigoItem().equals(TipoEstadoProgramaType.NORENOVADO.getKey()))
										  .findFirst()
										  .get()
										  .getIdMaestro());
								
								//RESCATAR EL ITEM Y GRUPO DEL PERIODO ANTERIOR
								
								for(int j=0;j<lisperuP.size();j++){
									grupo=lisperuP.get(j).getCodGrupo();
									item=lisperuP.get(j).getCodItem();	
								}
							
//								MaestroDTO maestroPeriodo=new MaestroDTO(listaTipoPeriodo.stream()
//										  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2017.getKey()))
//										  .findFirst()
//										  .get()
//										  .getIdMaestro());
								
								List<MaestroDTO> Maestroregistrado=maestroService.buscarMaestrosxGrupoItem(grupo,item );
								MaestroDTO maestroPeriodo=new MaestroDTO();
								maestroPeriodo.setIdMaestro(Maestroregistrado.get(0).getIdMaestro());
								
								
								
									 // Tipo de solicitudes a restaura
									List<Long> listSituaciones = new ArrayList<Long>();
									listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString())).findFirst().get().getIdMaestro());
									listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISION.getKey().toString())).findFirst().get().getIdMaestro());
									listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey().toString())).findFirst().get().getIdMaestro());
									listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.SUSTENTADO.getKey().toString())).findFirst().get().getIdMaestro());
									 
									 //Reiniciar los programas
													
									cantReinicia=programaService.actualizarSituacPrograma(parametros,maestroSituacion,maestroEstado,maestroPeriodo,listSituaciones,listaTipoSituacionSolicitud.stream()
											  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISIONPROCESOANT.getKey().toString()))
											  .findFirst()
											  .get()
											  .getIdMaestro());
									
									inReinicio=true;	
//								parametros.put("numeroReinicios", cantReinicia);
//								
//								respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
								
							}
								
								///////////////////////////////////////////////////////////
								
								
								
								
//							}else{
//								System.out.println("HAY SOLAPAMIENTO DE FECHAS REGISTRADAS --> " +maesdto );
//								haySolapamiento=true;
//							}
								
					}else{
						// ACTUALIZAR LOS ESTADOS DE LOS DEMAS PERIODOS, EXCEPTO EL ULTIMO REGISTRADO
						perRegisExis=false;
						int idPeriodo=0;
						int idPeriodoTemp=0;
						//get ultimo registrado
						for(int j=0;j<lisper.size();j++){
							idPeriodo=lisper.get(j).getIdPeriodo();
						}
						
						do{	
							for(int j=0;j<lisper.size()-1;j++){
								idPeriodoTemp=lisper.get(j).getIdPeriodo();
								PeriodoDTO perUpd=new PeriodoDTO();
								perUpd.setIdPeriodo(idPeriodoTemp);
								perUpd.setEstado(EstadoState.INACTIVO.getValue());
								
								perUpd=periodoService.actalizarPeriodo(perUpd);
							}
							break;
						}while(idPeriodoTemp<idPeriodo);
					}
					
					
				}
				
				
//				fechaInicioIncorrecto=false;
		
//		if(comparaFecha(fechtoday,fecIniPeriodo)==0||comparaFecha(fechtoday,fecIniPeriodo)>0){			
//			}else{
//			
//				fechaInicioIncorrecto=true;
//			
//			
//			}
			
		
			
			
			
			

			
//			}
			sucess=true;
			if(perRegisExis){
				parametros.put("existeperiodo", true);
			}
			if(haySolapamiento){
				parametros.put("existecruce", true);
			}
			if(fechaAnioIncorrecto){
				parametros.put("noesanio", true);
			}
			if(recargaPagina){
				parametros.put("numeroReinicios", cantReinicia);
				parametros.put("reload", true);
			
			}
			if(fechaInicioIncorrecto){
				parametros.put("noesinicio", true);
			}
			if(inReinicio){
				parametros.put("iscantidad", true);
//				parametros.put("numeroReinicios", cantReinicia);
			}
			if(fechaInicioPeriodoIncorrecto){
				parametros.put("noesInicioCorrecto", true);
			}			
			respuesta = new RespuestaDTO(sucess, "['" + mensaje + "']", parametros);
			
			response.addHeader("x-frame-options", "DENY");
			//response.addHeader("x-frame-options","SAMEORIGIN");
			response.addHeader("X-XSS-Protection", "1; mode=block");
			response.addHeader("X-Content-Type-Options", "nosniff");
	
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html; charset=UTF-8");
			response.getWriter().write(gson.toJson(respuesta).toString());

			
		}catch(Exception e){
			System.out.println("ERROR PROPAGADO--> " + e.getMessage() );
		}
		
	
	}
@SuppressWarnings({ "unchecked" })
private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup("java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/"
					+ nombreSimple.replace("Local", "") + "!" + nombreLargo);
		} catch (NamingException ne) {
			throw new RuntimeException(ne);
		}
	}
	
public void Merge(Cell cell, String item) {
	if (item.equals("item1")) {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue("Fabian Montoya");
	} else if (item.equals("item3")) {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue("Excel_GO");
	} else {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue("Item_not_found");
	}
}

/*
 * Metodo que obtiene el tamaño del bloque. Resta las dos fechas involucradas
 * 
 * */

private  int getTamanoRango(Date fechaInicio, Date fechaFin){
		FechaUtil futil = null;
		int numDiasRango = 0;
		try{
			numDiasRango=futil.restarFechas(fechaInicio, fechaFin);
		}catch(Exception e){			
		}
			return numDiasRango;	
	}
private int comparaFecha(Date fechaSistema, String fechInicio){
	
	int indicaComparacion=0;
    SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
    String fechSistema=formateador.format(fechaSistema);
	   try {
		Date fechaDate1 = formateador.parse(fechSistema);
		Date fechaDate2 = formateador.parse(fechInicio);
		if(fechaDate2.after(fechaDate1)){
			System.out.println("LA FECHA DEL SISTEMA ES MENOR");
			indicaComparacion= 1;
		}else if(fechaDate2.before(fechaDate1)){
			System.out.println("LA FECHA DEL SISTEMA ES MAYOR");
			indicaComparacion= -1;
		}else{
			System.out.println("LAS FECHAS SON IGUALES");
			indicaComparacion=0;
		}
		
	} catch (ParseException e) {
		e.printStackTrace();
	}
	return indicaComparacion;
	
}

/*
 * Metodo que verifica si existe solapamiento entre dos periodos de fechas.
 * Return true si no existe solapamiento
 */
private  boolean verificaNoSolapamiento(Date fechaInicio,Date fechaFin,Date fechaInicioPost, Date fechaFinPost){
		int tamPeriodoOne=0;
		int tamPeriodoTwo=0;
		boolean isOk=false;
		boolean isOkBloquePost=false;
		try{
		
			//1. Obtener tamaños de rango
			tamPeriodoOne=getTamanoRango(fechaInicio, fechaFin);
			tamPeriodoTwo=getTamanoRango(fechaInicioPost, fechaFinPost);
			//2. verificar si bloque dos esta despues de bloque uno
			if(fechaInicioPost.after(fechaFin)&&(fechaInicioPost.compareTo(fechaFin)>0||fechaInicioPost.compareTo(fechaFin)<0)){ 
				System.out.println("PRIMER FILTRO OK");
				isOk=true;
				isOkBloquePost=false;
			}else{
				System.out.println("PRIMER FILTRO PELIGRO");
				isOk=false;
				isOkBloquePost=true;
			}
			//3. verificar si fecha de inicio de bloque 2 esta antes de fecha fin de bloque 1
			if(isOkBloquePost){
				System.out.println("LA FECHA DE INICIO DEL BLOQUE DOS, ESTA ANTES DE LA FECHA DE FIN DEL BLOQUE UNO");
				if(fechaInicioPost.before(fechaFin)){
				System.out.println("ENTRAMOS EN TRUE");
				//La fecha de fin del bloque dos, debe de estar antes de la fecha de inicio del bloque uno
						if(fechaFinPost.before(fechaInicio)&&(fechaFinPost.compareTo(fechaInicio)>0||fechaFinPost.compareTo(fechaInicio)<0) ){
							isOk=true;
						}else{
							System.out.println("ENTRAMOS EN FALSE POR FECHA FIN BLOQUE DOS IGUAL A FECHA INICIO BLOQUE UNO");
							isOk=false;
						}
				
//					isOk=true;
				
				}else{
					System.out.println("ENTRAMOS EN FALSE");
					isOk=false;
				}
			}	
				
			
		}catch(Exception e){
			
		}
		
		return isOk;
	}
	private int obtenerAnio(Date date){
		String formato="";
		if (null == date){
	
		return 0;
	
		}
		else{
	
		formato="yyyy";
		SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
		return Integer.parseInt(dateFormat.format(date));
	
			}
		}

	private boolean verificaTopeInicio(Date fechaIniPeriodo, String periodo){
		boolean verifica=false;
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		String periodoFecha="01/01/"+periodo;
		
		try {
			Calendar calen=Calendar.getInstance();
			calen.setTime(sdf.parse(periodoFecha));
			calen.add(Calendar.DAY_OF_MONTH,-92);
			Date fechPeriodo=calen.getTime();
					
			if(fechaIniPeriodo.before(fechPeriodo)){
				verifica=true;
				System.out.println("NO SE LE DEJA QUE REGISTRE");
			}else{
				System.out.println("PUEDE CONTINUAR CON EL REGISTRO");
				verifica=false;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return verifica;
		
			
		}

}
