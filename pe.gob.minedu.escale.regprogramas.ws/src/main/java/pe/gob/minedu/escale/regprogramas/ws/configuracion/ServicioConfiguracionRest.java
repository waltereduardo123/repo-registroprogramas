package pe.gob.minedu.escale.regprogramas.ws.configuracion;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import pe.gob.minedu.escale.regprogramas.ws.DreGeoWs;
import pe.gob.minedu.escale.regprogramas.ws.MaestroWs;
import pe.gob.minedu.escale.regprogramas.ws.ProgramaWs;
import pe.gob.minedu.escale.regprogramas.ws.servlet.SecurityFilter;

@ApplicationPath("/rest")
public class ServicioConfiguracionRest extends Application {

	private Set<Object> singletons = new HashSet<>();

	public ServicioConfiguracionRest() {
		singletons.add(new MaestroWs());
		singletons.add(new DreGeoWs());
		singletons.add(new ProgramaWs());
		singletons.add(new SecurityFilter());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

}

