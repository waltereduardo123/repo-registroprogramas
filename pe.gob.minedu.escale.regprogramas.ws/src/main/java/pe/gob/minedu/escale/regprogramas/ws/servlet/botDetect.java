package pe.gob.minedu.escale.regprogramas.ws.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.captcha.botdetect.web.servlet.SimpleCaptcha;
import com.google.gson.Gson;

import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.util.LogUtil;



@WebServlet("/svt/botDetect")
@MultipartConfig
public class botDetect extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String mensajeErrorArchivo  ="";
	private String mensaje = "";
	
	
	private Gson gson = new Gson();
	private ConcurrentHashMap<Object, Object> parametros;

		
	private static LogUtil log = new LogUtil(botDetect.class.getName());
	

	
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public botDetect() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Boolean sucess=true;
		parametros = new ConcurrentHashMap<>();
		PrintWriter out = response.getWriter();
		try{
     
			SimpleCaptcha captcha = SimpleCaptcha.load(request, "exampleCaptcha");
			String captchaHtml = captcha.getHtml();
			
			//out.write(captchaHtml);
			parametros.put("xwurs", captchaHtml);
			respuesta = new RespuestaDTO(sucess, "['']", parametros);
			
			response.addHeader("x-frame-options", "DENY");
			//response.addHeader("x-frame-options","SAMEORIGIN");
			response.addHeader("X-XSS-Protection", "1; mode=block");
			response.addHeader("X-Content-Type-Options", "nosniff");
						
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");	   
			response.getWriter().write(gson.toJson(respuesta).toString());
			
		}catch(Exception e){
			System.out.println("ERROR PROPAGADO--> " + e.getMessage() );
			
		}
	}

	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				            
	}



	public String getMensajeErrorArchivo() {
		return mensajeErrorArchivo;
	}

	public void setMensajeErrorArchivo(String mensajeErrorArchivo) {
		this.mensajeErrorArchivo = mensajeErrorArchivo;
	}				
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	
}
