package pe.gob.minedu.escale.regprogramas.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


import com.google.gson.Gson;

import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.rest.util.AesUtil;
import pe.gob.minedu.escale.common.rest.util.QueryParamURL;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.service.MaestroServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;

@Path("/maestro")
@Stateless
public class MaestroWs {

	
	@EJB
	private transient MaestroServiceLocal maestroService = lookup(MaestroServiceLocal.class);
	
	@EJB
	private DataGeneralCache dataGeneralCache = lookup(DataGeneralCache.class);
	
	private Gson gson = new Gson();
	
	private String json = "";
	
	//cambio werr
	private String param,salt,iv;
	
	private int keySize = 128, iterationCount = 1000;
	
	private String passphrase= "0015975320171234";
	
	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);	
	
	private ConcurrentHashMap<Object, Object> parametros;
	
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
	public MaestroWs() {
		
	}
	
//	@POST
//	@Path("/getMaestros")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getMaestros(String url){
//		List<MaestroDTO> listMaestro = new ArrayList<MaestroDTO>();		
//		try {
////			listMaestro = maestroService.listarMaestros();
//			listMaestro = dataGeneralCache.getListaMaestro();
//			if (StringUtil.isNotNullOrBlank(listMaestro)) {
//				json = gson.toJson(listMaestro);
//			}
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			json = "No se encuentra maestros: estado-> ";
//			return Response.status(Status.CONFLICT).entity(json).build();
//		}
//		return Response.status(Status.OK).entity(json).build();
//	}
//Se modifica por cifrado de datos  werr 08022018	
	@SuppressWarnings("rawtypes")
	@POST
	@Path("/getMaestros")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMaestros(final String jsonEncrypted){ 
		List<MaestroDTO> listMaestro = new ArrayList<MaestroDTO>();		
			aesUtil = new AesUtil(keySize, iterationCount);		
			parametros = new ConcurrentHashMap<>();
		try {		
				if(StringUtil.isNotNullOrBlank(jsonEncrypted) && jsonEncrypted.charAt(0)!='{' ){
					
					listMaestro = maestroService.listarMaestros();
					if (StringUtil.isNotNullOrBlank(listMaestro)) {
					param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
					salt = QueryParamURL.getParam(jsonEncrypted, "salt");
					iv = QueryParamURL.getParam(jsonEncrypted, "iv");
					parametros.put("salt", salt);
					parametros.put("iv", iv);
					parametros.put("error", false);
					parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(listMaestro)));
					respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
					return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
				}else{
					parametros.put("error", true);
					parametros.put("mensaje", "Error en la carga del maestro");
					respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
					return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
					}		
				}
			else{
				parametros.put("error", true);
				parametros.put("mensaje", "Error General.");
				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
				return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra maestros: estado-> ";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
	
	}
	
	@POST
	@Path("/getMaestroxPadre/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMaestroxPadre(@PathParam("id") String id){
		List<MaestroDTO> listMaestro = new ArrayList<MaestroDTO>();		
		try {
			listMaestro = maestroService.buscarMaestrosxPadre(Long.parseLong(id));				
			if (StringUtil.isNotNullOrBlank(listMaestro)) {
				json = gson.toJson(listMaestro);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra maestro";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
		return Response.status(Status.OK).entity(json).build();
	}
	
	@POST
	@Path("/getMaestroxGrupo/{codigoAgrupacion}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMaestroxGrupo(@PathParam("codigoAgrupacion") final String codigoAgrupacion){
		List<MaestroDTO> listMaestro = new ArrayList<MaestroDTO>();		
		try {
//			listMaestro = maestroService.buscarMaestrosxGrupo(codigoAgrupacion);
			listMaestro = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(codigoAgrupacion);
			if (StringUtil.isNotNullOrBlank(listMaestro)) {
				json = gson.toJson(listMaestro);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra maestro";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
		return Response.status(Status.OK).entity(json).build();
	}
	
	@POST
	@Path("/getMaestroxNombre/{nombreMaestro}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMaestroxNombre(@PathParam("nombreMaestro") final String nombreMaestro){
		List<MaestroDTO> listMaestro = new ArrayList<MaestroDTO>();		
		try {
			listMaestro = maestroService.buscarMaestrosxNombre(nombreMaestro);	
			if (StringUtil.isNotNullOrBlank(listMaestro)) {
				json = gson.toJson(listMaestro);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra maestro";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
		return Response.status(Status.OK).entity(json).build();
	}

	
	@POST
	@Path("/getMaestroxEstado/{estado}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMaestroxEstado(@PathParam("estado") final String estado){
		List<MaestroDTO> listMaestro = new ArrayList<MaestroDTO>();
		try {
			listMaestro = maestroService.buscarMaestrosxEstado(estado);	
			if (StringUtil.isNotNullOrBlank(listMaestro)) {
				json = gson.toJson(listMaestro);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra maestro";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
		return Response.status(Status.OK).entity(json).build();
	}
	
	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup(
					"java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/" + nombreSimple.replace("Local", "")
							+ "!"+nombreLargo);
		} catch (NamingException ne) {
			throw new RuntimeException(ne);
		}
	}
	
}
