package pe.gob.minedu.escale.regprogramas.ws;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
//import org.apache.log4j.spi.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import pe.gob.minedu.escale.adm.business.type.TipoAgrupadorSeguridad;
import pe.gob.minedu.escale.adm.business.type.TipoArchivoType;
import pe.gob.minedu.escale.adm.business.type.TipoDocumentoType;
import pe.gob.minedu.escale.adm.business.type.TipoEstadoProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoPeriodoType;
import pe.gob.minedu.escale.adm.business.type.TipoProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionProgramaType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionRevisionType;
import pe.gob.minedu.escale.adm.business.type.TipoSituacionSolicitudType;
import pe.gob.minedu.escale.adm.business.type.TipoSolicitudType;
import pe.gob.minedu.escale.adm.ejb.service.AdministracionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.SesionServiceLocal;
import pe.gob.minedu.escale.adm.ejb.service.UsuarioServiceLocal;
import pe.gob.minedu.escale.adm.model.dto.AccesoDTO;
import pe.gob.minedu.escale.adm.model.dto.PerfilDTO;
import pe.gob.minedu.escale.adm.model.dto.UsuarioDTO;
import pe.gob.minedu.escale.adm.model.jpa.Usuario;
import pe.gob.minedu.escale.adm.utils.helper.ConversorHelper;
import pe.gob.minedu.escale.adm.vo.UsuarioLoginResultadoVO;
import pe.gob.minedu.escale.common.application.exception.ConversorException;
import pe.gob.minedu.escale.common.business.state.CondicionState;
import pe.gob.minedu.escale.common.business.state.EstadoProgramaState;
import pe.gob.minedu.escale.common.business.state.EstadoState;
import pe.gob.minedu.escale.common.dto.rest.ConsultaJson;
import pe.gob.minedu.escale.common.dto.rest.ControlAvanceFilter;
import pe.gob.minedu.escale.common.dto.rest.ReporteGenerico;
import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.dto.rest.TipoSolicitudFilter;
import pe.gob.minedu.escale.common.rest.auth.util.AuthUtils;
import pe.gob.minedu.escale.common.rest.util.AesUtil;
import pe.gob.minedu.escale.common.rest.util.QueryParamURL;
import pe.gob.minedu.escale.common.util.BaseBean;
import pe.gob.minedu.escale.common.util.CollectionUtil;
import pe.gob.minedu.escale.common.util.ExportExcelUtil;
import pe.gob.minedu.escale.common.util.FechaUtil;
import pe.gob.minedu.escale.common.util.ResourceUtil;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprograma.business.exception.AccionSolicitudNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.AuditoriaProgramaNoActualizadoException;
import pe.gob.minedu.escale.regprograma.business.exception.AuditoriaProgramaNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.AuditoriaProgramaNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.CadenaInvalidaException;
import pe.gob.minedu.escale.regprograma.business.exception.CodigoModularNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.CodigoSolicitudRepetidoException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoActualizadoException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.ProgramaNoRegistradoException;
import pe.gob.minedu.escale.regprograma.business.exception.RevisionCamposIdSolicitudNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.RevisionCamposSolicitudNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.RevisionCamposSolicitudNoExistePerfilException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoActualizadaException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoExisteException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudNoRegistradaException;
import pe.gob.minedu.escale.regprograma.business.exception.SolicitudRevisadaException;
import pe.gob.minedu.escale.regprograma.business.exception.TipoSolicitudNoExisteException;
import pe.gob.minedu.escale.regprogramas.cache.DataGeneralCache;
import pe.gob.minedu.escale.regprogramas.ejb.service.AccionSolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ArchivoServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.AuditoriaProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.CodigoModularServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.DocumentoServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ObjectServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ParametroServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.PeriodoServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.ProgramaServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.RevisionSolicitudCamposServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudDocumentoServiceLocal;
import pe.gob.minedu.escale.regprogramas.ejb.service.SolicitudServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.AccionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ArchivoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.AuditoriaProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.CodigoModularDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DistritosDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.DreUgelDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.MaestroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ParametroDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.PeriodoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ProgramaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.RevisionSolicitudCamposDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.SolicitudDocumentoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ApruebaSolCompareDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.DeshacerObservacionCompareDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.DeshacerSolCompareDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.FormCreaSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ObservacionSolCompareDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.ObservacionSolicitudDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.PeriodoBandejaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.SolicitudBandejaDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.TransFormCipher;

@Path("/programa")
@Stateless
public class ProgramaWs extends BaseBean {
	private static final Logger LOG = LoggerFactory.getLogger(ProgramaWs.class);
	private static final long serialVersionUID = 992902449014292913L;

	@EJB
	private ProgramaServiceLocal programaService = lookup(ProgramaServiceLocal.class);

	@EJB
	private SolicitudServiceLocal solicitudService = lookup(SolicitudServiceLocal.class);

	@EJB
	private DocumentoServiceLocal documentoService = lookup(DocumentoServiceLocal.class);

	@EJB
	private ArchivoServiceLocal archivoService = lookup(ArchivoServiceLocal.class);

	@EJB
	private ObjectServiceLocal objectService = lookup(ObjectServiceLocal.class);

	@EJB
	private ParametroServiceLocal parametroService = lookup(ParametroServiceLocal.class);

	@EJB
	private AccionSolicitudServiceLocal accionSolicitudService = lookup(AccionSolicitudServiceLocal.class);

	@EJB
	private CodigoModularServiceLocal codigoModularService = lookup(CodigoModularServiceLocal.class);

	@EJB
	private SolicitudDocumentoServiceLocal solicitudDocumentoService = lookup(SolicitudDocumentoServiceLocal.class);

	@EJB
	private RevisionSolicitudCamposServiceLocal revisionSolicitudCamposService = lookup(
			RevisionSolicitudCamposServiceLocal.class);

	@EJB
	private AuditoriaProgramaServiceLocal auditoriaProgramaService = lookup(AuditoriaProgramaServiceLocal.class);

	@EJB
	private DataGeneralCache dataGeneralCache = lookup(DataGeneralCache.class);

	@EJB
	private PeriodoServiceLocal periodoService = lookup(PeriodoServiceLocal.class);

	@EJB
	private transient SesionServiceLocal sesionServiceLocal = lookup(SesionServiceLocal.class);

	@EJB
	private transient UsuarioServiceLocal usuarioServiceLocal = lookup(UsuarioServiceLocal.class);

	private Gson gson = new Gson();

	/** El servicio de administracion service local. */
	@EJB
	private transient AdministracionServiceLocal administracionServiceLocal = lookup(AdministracionServiceLocal.class);

	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;

	private ConcurrentHashMap<Object, Object> parametros;

	private final static String ERROR_SERVICIO_GENERAL = "Ocurrio un problema en el servicio";

	private List<MaestroDTO> listaTipoSituacionRevision;

	private List<MaestroDTO> listaTipoSituacionSolicitud;

	private List<MaestroDTO> listaTipoSolicitud;

	private List<MaestroDTO> listaTipoSituacionPrograma;

	private List<MaestroDTO> listaTipoEstadoPrograma;

	private List<MaestroDTO> listaTipoDocumento;

	private List<MaestroDTO> listaTipoPeriodo;

	private boolean success;

	private String param, salt, iv;

	private int keySize = 128, iterationCount = 1000;

	private String passphrase = "0015975320171234";

	private AesUtil aesUtil;

//	private static LogUtil log = new LogUtil(ProgramaWs.class.getName());

	public ProgramaWs() {
		iniciarMaestro();
	}

	@PostConstruct
	public void iniciar() {

	}

	public void iniciarMaestro() {
		listaTipoSituacionRevision = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionRevisionType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSolicitud = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSolicitudType.CODIGO_AGRUPACION.getKey());
		listaTipoSituacionPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoSituacionProgramaType.CODIGO_AGRUPACION.getKey());
		listaTipoEstadoPrograma = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoEstadoProgramaType.CODIGO_AGRUPACION.getKey());
		listaTipoDocumento = dataGeneralCache
				.getMaestroDTOxCodigoAgrupacion(TipoDocumentoType.CODIGO_AGRUPACION.getKey());
		listaTipoPeriodo = dataGeneralCache.getMaestroDTOxCodigoAgrupacion(TipoPeriodoType.CODIGO_AGRUPACION.getKey());
		LOG.info("LISTADO DE PERIODOS : " + listaTipoPeriodo);
		LOG.info("LISTADO DE ESTADOS : " + listaTipoEstadoPrograma);
	}

	@SuppressWarnings({ "rawtypes" })
	@POST
	@Path("/obtenerDatos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerDatos(String jsonEncrypted) {
		if (LOG.isDebugEnabled()) {
			LOG.info("input in obetenerDatos");
		}
		List<Object> lista = new ArrayList<Object>();
		ConsultaJson consultaJson = new ConsultaJson();
		aesUtil = new AesUtil(keySize, iterationCount);

		parametros = new ConcurrentHashMap<>();
		String respEncryp = "";
		try {
			// INICIO desencriptar
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			consultaJson = gson.fromJson(objJsonString, ConsultaJson.class);

			// FIN desencriptar

			//////////////////////////
			String tkMaster;
			List<AccesoDTO> listAceesos = new ArrayList<AccesoDTO>();
			AccesoDTO accdto = new AccesoDTO();
			accdto.setUsuario(consultaJson.getPivot());
			listAceesos = sesionServiceLocal.verificarNevegacionUserTupla(accdto);
			if (listAceesos != null) {
				tkMaster = listAceesos.get(0).getToken();
				if (tkMaster.equals(consultaJson.getShow())) {
					String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
							? consultaJson.getGroupFilter().toString()
							: "";
					lista = objectService.findByFirstMaxResultJson(consultaJson.obtenerFields(), valor,
							consultaJson.getTable(), consultaJson.getOrder(), consultaJson.getBatchSize(),
							consultaJson.getIndex());
					if (!lista.isEmpty()) {
						parametros.put("salt", salt);
						parametros.put("iv", iv);
						parametros.put("rare", false);
						// parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase,
						// gson.toJson(lista)));
						parametros.put("lista", lista);
						respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
						// respuesta = new RespuestaDTO(true, "[]", lista);
						respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
								+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
					} else if (lista.isEmpty()) {
						respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
						// respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt,
						// iv, passphrase, gson.toJson(respuesta));
					}

				} else {
					try {
						throw new CadenaInvalidaException();
					} catch (CadenaInvalidaException ex) {
						if (LOG.isErrorEnabled())
							LOG.error("error producido" + ex);
						parametros.put("rare", true);
						respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
						// return
						// Response.status(Status.NOT_ACCEPTABLE).entity(gson.toJson(respuesta).toString()).build();
						respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
								+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
						return Response.status(Status.NOT_ACCEPTABLE)
								.entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
						// return
						// Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
					}

				}
			}
////////////////////			 

		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", lista);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} finally {
			parametros.clear();
		}
		// return
		// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

	@SuppressWarnings({ "rawtypes" })
	@POST
	@Path("/existeResolucion/{codooii}/{tipoResolucion}/{numDocumento}/{fechaDocumento}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response existeResolucion(@PathParam("codooii") String codooii,
			@PathParam("tipoResolucion") String tipoResolucion, @PathParam("numDocumento") String numDocumento,
			@PathParam("fechaDocumento") String fechaDocumento) {
		List<DocumentoDTO> listDocumento = new ArrayList<DocumentoDTO>();
		parametros = new ConcurrentHashMap<>();
		try {
			numDocumento = numDocumento.replaceFirst("^0*", "");
			listDocumento = documentoService.buscarDocumento(codooii, tipoResolucion, numDocumento, fechaDocumento);
			if (!listDocumento.isEmpty()) {
				parametros.clear();
				parametros.put("existeResolucion", true);
				parametros.put("idDocumento", listDocumento.get(0).getIdDocumento().intValue());
				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
			} else if (listDocumento.isEmpty()) {
				parametros.clear();
				parametros.put("existeResolucion", false);
				parametros.put("idDocumento", 0);
				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
			}
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Erros producido" + e);
			}
			parametros.clear();
			parametros.put("existeResolucion", false);
			parametros.put("idDocumento", 0);
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']",
					new ConcurrentHashMap<>(parametros));
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings({ "rawtypes" })
	@POST
	@Path("/existeCroquis/{idSolicitud}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response existeCroquis(@PathParam("idSolicitud") Long idSolicitud) {
		List<SolicitudDocumentoDTO> listaSolicitudDocumento = new ArrayList<SolicitudDocumentoDTO>();
		List<ArchivoDTO> listaArch = new ArrayList<ArchivoDTO>();
		parametros = new ConcurrentHashMap<>();
		SolicitudDocumentoDTO doc = null;
		Long conteo = null;
		try {
			listaSolicitudDocumento = solicitudDocumentoService.findByIdSolicitud(idSolicitud);
			Long idMaestro = listaTipoDocumento.stream()
					.filter(d -> d.getCodigoItem().equals(TipoDocumentoType.CROQUIS.getKey())).findFirst().get()
					.getIdMaestro();

			conteo = listaSolicitudDocumento.stream()
					.filter(p -> p.getDocumento().getTipoDocumento().getIdMaestro().equals(idMaestro)).count();
			if (Objects.nonNull(conteo) && conteo != 0L) {
				doc = listaSolicitudDocumento.stream()
						.filter(p -> p.getDocumento().getTipoDocumento().getIdMaestro().equals(idMaestro)).findFirst()
						.get();
				listaArch = archivoService.buscarArchivoxIdDocumento(doc.getDocumento().getIdDocumento());
				ArchivoDTO arch = new ArchivoDTO();
				arch = listaArch.stream().filter(p -> p.getEstado().equals(EstadoState.ACTIVO.getValue())).findFirst()
						.get();
				// nombre Archivo
				if (Objects.nonNull(doc.getDocumento())) {
					parametros.clear();
					parametros.put("existeCroquis", true);
					parametros.put("archivo", arch);
					respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
				} else {
					parametros.clear();
					parametros.put("existeCroquis", false);
					parametros.put("archivo", 0);
					respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
				}
			} else {
				parametros.clear();
				parametros.put("existeCroquis", false);
				parametros.put("archivo", 0);
				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
			}
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido :" + e);
			}
			parametros.clear();
			parametros.put("existeCroquis", false);
			parametros.put("idDocumento", 0);
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']",
					new ConcurrentHashMap<>(parametros));
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@POST
	@Path("/obtenerArchivosResol/{idResolucion}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerArchivosResol(@PathParam("idResolucion") String idDocumento) {
		List<ArchivoDTO> listArchivo = new ArrayList<ArchivoDTO>();
		try {
			listArchivo = archivoService.buscarArchivoxIdDocumento(Long.valueOf(idDocumento));
			if (!listArchivo.isEmpty()) {
				respuesta = new RespuestaDTO<ArchivoDTO>(true, "[]", listArchivo);
			} else if (listArchivo.isEmpty()) {
				respuesta = new RespuestaDTO<ArchivoDTO>(true, "['No existe registro']", listArchivo);
			}
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido" + e);
			}
			respuesta = new RespuestaDTO<ArchivoDTO>(false, "" + ERROR_SERVICIO_GENERAL + "", listArchivo);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerSolicitud/{index}/{cantidad}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerSolicitud(@PathParam("index") int index, @PathParam("cantidad") int cantidad) {
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		parametros = new ConcurrentHashMap<>();
		try {
			listSolicitud = solicitudService.listarSolicitudFirstMaxResultParametros(cantidad, index, parametros);
			if (!listSolicitud.isEmpty()) {
				respuesta = new RespuestaDTO(true, "[]", listSolicitud);
			} else if (listSolicitud.isEmpty()) {
				respuesta = new RespuestaDTO(true, "['No hay resultados']", listSolicitud);
			}
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", listSolicitud);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	
	@SuppressWarnings({ "rawtypes" })
	@POST
	@Path("/validarPrograma/{tipoSolicitud}/{codooii}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validarPrograma(@PathParam("tipoSolicitud") String tipoSolicitud,
			@PathParam("codooii") String codooii, @QueryParam("codigoModular") String codigoModular,
			@QueryParam("nombrePrograma") String nombrePrograma) {
		List<ProgramaDTO> listPrograma = new ArrayList<ProgramaDTO>();
		//List<ProgramaDTO> listPrograma1 = new ArrayList<ProgramaDTO>();
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		List<ParametroDTO> listParametro = new ArrayList<ParametroDTO>();
		parametros = new ConcurrentHashMap<>();
		Boolean existeSolicitudAnulada = null; //imendoza 20170103
		Boolean existeSolicitudPendiente = null; //imendoza 20170103
		Boolean existeSolicitudRevision = null; //Ana
		Boolean existeSolicitudObservada=null; //Ana
		Boolean existeSolicitudSustentada= null;//Ana
		Boolean existeSolicitudProceso = false; //Ana
		String solicitudCrear= null;
		String nombreTipoSolicitud= null;
		String nombreSituacionSolicitud = null;
		try {
//			parametros.put("estado", "1");
			
			parametros.put("codooii", codooii);
			if (StringUtil.isNotNullOrBlank(codigoModular)) {
				parametros.put("codigoModular", codigoModular);
			} else {
				parametros.put("codigoModular", "");
			}
			if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
				parametros.put("nombrePrograma", StringUtil.upperCaseObject(nombrePrograma).toString());
			} else {
				parametros.put("nombrePrograma", "");
			}
			listSolicitud = solicitudService.buscarSolicitud(parametros.get("nombrePrograma").toString(),
					parametros.get("codooii").toString(), parametros.get("codigoModular").toString(), tipoSolicitud);
			//imendoza 20170103
			existeSolicitudAnulada =
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			existeSolicitudPendiente = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			//Ana
			existeSolicitudRevision = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISION.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			
			//Ana
			existeSolicitudObservada = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			//Ana
			existeSolicitudSustentada = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.SUSTENTADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
	
			//SI EXISTE ALGUNA SOLICITUD EN PROCESO (REVISIÓN, PENDIENTE, OBSERVADA, SUSTENTADA) CON EL CÓDIGO MODULAR INGRESADO	//Ana
			//parametros.put("existeSolicitud", (existeSolicitudPendiente || existeSolicitudRevision || existeSolicitudObservada || existeSolicitudSustentada ));
			
			if ((existeSolicitudPendiente || existeSolicitudRevision || existeSolicitudObservada || existeSolicitudSustentada )) 
			{		
				existeSolicitudProceso = true;
				
				nombreTipoSolicitud = (listSolicitud.stream().anyMatch(
						(p) -> p.getTipoSolicitud().getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))?"CIERRE": (				
													listSolicitud.stream().anyMatch(
						(p) -> p.getTipoSolicitud().getCodigoItem().equals(TipoSolicitudType.MODIFICACION.getKey()))?"MODIFICACION": (
													listSolicitud.stream().anyMatch(
						(p) -> p.getTipoSolicitud().getCodigoItem().equals(TipoSolicitudType.RENOVACION.getKey()))?"RENOVACION":("")
						 )));
				nombreSituacionSolicitud =(existeSolicitudPendiente? "PENDIENTE":(existeSolicitudRevision?"REVISION":(existeSolicitudObservada?"OBSERVADA":(existeSolicitudSustentada?"SUSTENTADA":""))));
			}
			
			//VALIDACIONES DEPENDIENDO DEL TIPO DE SOLICITUD QUE SE DESEA CREAR PARA EL CODIGO MODULAR INGRESADO
			//solicitudCrear= TipoSolicitudType.valueOf(tipoSolicitud).name().toString();
			if(tipoSolicitud.equals(TipoSolicitudType.RENOVACION.getKey())|| tipoSolicitud.equals(TipoSolicitudType.MODIFICACION.getKey()) || tipoSolicitud.equals(TipoSolicitudType.CIERRE.getKey()))
			{
				parametros.keySet().removeIf(e -> (e == "nombrePrograma")); 
				if (StringUtil.isNotNullOrBlank(codigoModular)) {
					parametros.put("codigoModular", codigoModular);
					listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
				} else {
					listPrograma = null;
				}				
				//listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros); // 1
				parametros.keySet().removeIf(e -> (e == "codooii" || e == "tipoSituacion") || e == "codigoModular");
				parametros.put("codigoParametro", "FECFINPERREN");
				listParametro = parametroService.listarParametroFirstMaxResultParametros(5, 0, parametros); // 1
				int estaFueraDePeriodo = 0;
				
				if (!listParametro.isEmpty()) {
					for (ParametroDTO parametro : listParametro) {
						estaFueraDePeriodo = FechaUtil.restarFechas(FechaUtil.obtenerFechaActual(),
								parametro.getValorFecha());
						
						
						if (estaFueraDePeriodo <= -1) {
							parametros.put("fueraPeriodoRenovacion", true);
							break;
						} else {
							parametros.put("fueraPeriodoRenovacion", false);
						}
						
					}
				}				
				
				/////
				if ((listPrograma != null)&&(!listPrograma.isEmpty())) {		
					Boolean puedeModificar= false;
					Boolean puedeRenovar= false;
					Boolean puedeCerrar = false;
					for (ProgramaDTO programa : listPrograma) {
						if (StringUtil.isNotNullOrBlank(programa.getTipoSituacion().getDescripcionMaestro())) {
							//SI LA SOLICITUD QUE SE DESEA CREAR ES RENOVACIÓN
							if (tipoSolicitud.equals(TipoSolicitudType.RENOVACION.getKey())) 
							{
								solicitudCrear = "RENOVACION";
							puedeRenovar =
										(programa.getTipoSituacion().getIdMaestro()
												.equals(listaTipoSituacionPrograma.stream()
														  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.PORACTUALIZAR.getKey()))
														  .findFirst()
														  .get()
										
														  .getIdMaestro()) ? true: false);
							
								//break;
							};
							
							//SI LA SOLICITUD QUE SE DESEA CREAR ES DE MODIFICACIÓN
							if (tipoSolicitud.equals(TipoSolicitudType.MODIFICACION.getKey())) {
							solicitudCrear = "MODIFICACION";
							puedeModificar =
									((
										programa.getTipoSituacion().getIdMaestro()
											.equals(listaTipoSituacionPrograma.stream()
													.filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.RENOVADO.getKey()))
													.findFirst()
													.get()
													.getIdMaestro()
													)
									|| programa.getTipoSituacion().getIdMaestro()
											.equals(listaTipoSituacionPrograma.stream()
													  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.CREADO.getKey()))
													  .findFirst()
													  .get()
													  .getIdMaestro()
													 )
									|| programa.getTipoSituacion().getIdMaestro()
											.equals(listaTipoSituacionPrograma.stream()
													  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.MODIFICADO.getKey()))
													  .findFirst()
													  .get()
													  .getIdMaestro()
													 )
									)? true : false
								);							
							};
							//SI LA SOLICITUD QUE SE DESEA CREAR ES DE CIERRE	
							if (tipoSolicitud.equals(TipoSolicitudType.CIERRE.getKey())) {
							solicitudCrear = "CIERRE";
							puedeCerrar =
									((
										programa.getTipoSituacion().getIdMaestro()
											.equals(listaTipoSituacionPrograma.stream()
													.filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.RENOVADO.getKey()))
													.findFirst()
													.get()
													.getIdMaestro()
													)
									|| programa.getTipoSituacion().getIdMaestro()
											.equals(listaTipoSituacionPrograma.stream()
													  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.CREADO.getKey()))
													  .findFirst()
													  .get()
													  .getIdMaestro()
													 )
									|| programa.getTipoSituacion().getIdMaestro()
										.equals(listaTipoSituacionPrograma.stream()
											  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.PORACTUALIZAR.getKey()))
											  .findFirst()
											  .get()
											  .getIdMaestro()
											 )
									|| programa.getTipoSituacion().getIdMaestro()
										.equals(listaTipoSituacionPrograma.stream()
											  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.MODIFICADO.getKey()))
											  .findFirst()
											  .get()
											  .getIdMaestro()
											 )
									)? true : false
								);
							//break;
							};
							//MENSAJE  //Ana
							String situacion = listaTipoSituacionPrograma.stream().filter(p->p.getIdMaestro().equals(programa.getTipoSituacion().getIdMaestro())).findFirst().get().getDescripcionMaestro().toString();						
							parametros.put("TipoSituacion", situacion);
							String mensaje = "No puede crear una solicitud de " + solicitudCrear + " para un programa en situación " + situacion;
							
							if (!puedeModificar || !puedeRenovar ||!puedeCerrar)	
							{
											if(programa.getTipoSituacion().getIdMaestro()
													.equals(listaTipoSituacionPrograma.stream()
															.filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.ENREVISION.getKey()))
															  .findFirst()
															  .get()
															  .getIdMaestro()																
															)?true:false
													)
												{
								 				 parametros.put("Mensaje", "Existe ya una solicitud de "+ nombreTipoSolicitud + ", en proceso (" + nombreSituacionSolicitud + "), para el código modular ingresado");
												}else
												{ parametros.put("Mensaje", mensaje);
												}
							}							
						}
					};
					
					parametros.put("puedeModificar",puedeModificar);
					parametros.put("puedeCerrar", puedeCerrar);
					parametros.put("puedeRenovar", puedeRenovar);
				} //SI LA SOLICITUD QUE SE DESEA CREAR ES DE CREACIÓN
				
			}else if (tipoSolicitud.equals(TipoSolicitudType.CREACION.getKey()) ){
				//imendoza 20170103
				/*imendoza 20170106 Esta omision es solo hasta el 20 de enero,segun una direccion
				if (Objects.nonNull(existeSolicitudAnulada) && Objects.nonNull(existeSolicitudPendiente)) {
					if (!existeSolicitudPendiente) {					
						if (existeSolicitudAnulada) {
							listSolicitud.clear();
						}
					}
				}		
				*/
				listSolicitud.clear();
				//imendoza 20170103
				parametros.keySet().removeIf(e -> (e == "codigoModular"));
				listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
			} 
			//Este parámetro ya no debería usarse sin el existeSolicitudEnProceso
			parametros.put("existeSolicitud", !listSolicitud.isEmpty());		
			parametros.put("existeSolicitudProceso", existeSolicitudProceso);
			//parametros.put("nombreTipoSolicitud", nombreTipoSolicitud);
			//parametros.put("nombreSituacionSolicitud", nombreSituacionSolicitud);
		
			//SI EXISTE O NO EL PROGRAMA
			if (listPrograma != null){
				parametros.put("existePrograma", !listPrograma.isEmpty());
				if(listPrograma.size()>0)
					parametros.put("esCerrado", listPrograma.get(0).getEstado().equals("1") || listPrograma.get(0).getEstado().equals("9") ? false:true);
			}
			
			
			
//				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(listPrograma.size()>0)
//				parametros.put("esCerrado", listPrograma.get(0).getEstado().equals("1") || listPrograma.get(0).getEstado().equals("9") ? false:true);
//
//			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
			List<String> eliminar = Arrays.asList("codooii", "nombrePrograma", "estado", "codigoParametro",
					"codigoModular");
			parametros.keySet().removeIf(e -> (eliminar.contains(e)));
			
			//if(parametros.get("fueraPeriodoModificacion")!=null)
			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
		
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " +e);
			}
//			parametros.keySet().removeIf(d -> (d == "codooii" || d == "nombrePrograma" || d == "estado"
//					|| d == "codigoParametro" || d == "codigoModular")); /////////////////////////////////////////CAMBIOOOOOOOOOOOOOOOOOOOOOO////////////////////////////////////
			parametros.put("existeSolicitud", false);
			parametros.put("existePrograma", false);
			if(parametros.get("fueraPeriodoModificacion")!=null)respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", new ConcurrentHashMap<>(parametros));
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}finally {			
			parametros.clear();//imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}
	
	@SuppressWarnings({ "rawtypes" })
	@POST
	@Path("/validarProgramanuevosFomrs/{tipoSolicitud}/{codooii}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validarProgramanuevosFomrs(@PathParam("tipoSolicitud") String tipoSolicitud,
			@PathParam("codooii") String codooii, @QueryParam("codigoModular") String codigoModular,
			@QueryParam("nombrePrograma") String nombrePrograma) {
		List<ProgramaDTO> listPrograma = new ArrayList<ProgramaDTO>();
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		List<ParametroDTO> listParametro = new ArrayList<ParametroDTO>();
		parametros = new ConcurrentHashMap<>();
		Boolean existeSolicitudAnulada = null; //imendoza 20170103
		Boolean existeSolicitudPendiente = null; //imendoza 20170103
		Boolean existeSolicitudRevision = null; //Ana
		Boolean existeSolicitudObservada=null; //Ana
		Boolean existeSolicitudSustentada= null;//Ana
		Boolean existeSolicitudProceso = false; //Ana
		try {
			//parametros.put("estado", "1"); walter 02/10/2018
			parametros.put("codooii", codooii);
			if (StringUtil.isNotNullOrBlank(codigoModular)) {
				parametros.put("codigoModular", codigoModular);
			} else {
				parametros.put("codigoModular", "");
			}
			if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
				parametros.put("nombrePrograma", StringUtil.upperCaseObject(nombrePrograma).toString());
			} else {
				parametros.put("nombrePrograma", "");
			}
			listSolicitud = solicitudService.buscarSolicitud(parametros.get("nombrePrograma").toString(),
					parametros.get("codooii").toString(), parametros.get("codigoModular").toString(), tipoSolicitud);
			//imendoza 20170103
			existeSolicitudAnulada =
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			existeSolicitudPendiente = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			//Ana
			existeSolicitudRevision = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISION.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			
			//Ana
			existeSolicitudObservada = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			//Ana
			existeSolicitudSustentada = 
			listSolicitud.stream()
						 .anyMatch(p-> p.getTipoSituacionSolicitud()
								 		.getIdMaestro()
								 		.equals(listaTipoSituacionSolicitud.stream()
																			  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.SUSTENTADO.getKey().toString()))
																			  .findFirst()
																			  .get()
																			  .getIdMaestro()));
			
			if ((existeSolicitudPendiente || existeSolicitudRevision || existeSolicitudObservada || existeSolicitudSustentada )) 
			{existeSolicitudProceso = true;	}
			//imendoza 20170103
			if (tipoSolicitud.equals(TipoSolicitudType.RENOVACION.getKey())) {
				parametros.keySet().removeIf(e -> (e == "nombrePrograma"));
				listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
				parametros.keySet().removeIf(e -> (e == "codooii" || e == "tipoSituacion") || e == "codigoModular");
				parametros.put("codigoParametro", "FECFINPERREN");
				listParametro = parametroService.listarParametroFirstMaxResultParametros(5, 0, parametros);
				int estaFueraDePeriodo = 0;
				if (!listParametro.isEmpty()) {
					for (ParametroDTO parametro : listParametro) {
						estaFueraDePeriodo = FechaUtil.restarFechas(FechaUtil.obtenerFechaActual(),
								parametro.getValorFecha());
						
						
						if (estaFueraDePeriodo <= -1) {
							parametros.put("fueraPeriodoRenovacion", true);
							break;
						} else {
							parametros.put("fueraPeriodoRenovacion", false);
						}
						
					}
				}
				if (!listPrograma.isEmpty()) {
					for (ProgramaDTO programa : listPrograma) {
						if (StringUtil.isNotNullOrBlank(programa.getTipoSituacion().getDescripcionMaestro())) {
							parametros.put("puedeRenovar",
									programa.getTipoSituacion().getIdMaestro()
											.equals(listaTipoSituacionPrograma.stream()
													  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.PORACTUALIZAR.getKey()))
													  .findFirst()
													  .get()
													  .getIdMaestro()) ? true: false);
							break;
						}
					}
				}
			} else if (tipoSolicitud.equals(TipoSolicitudType.CREACION.getKey())) {
				//imendoza 20170103
				/*imendoza 20170106 Esta omision es solo hasta el 20 de enero,segun una direccion
				if (Objects.nonNull(existeSolicitudAnulada) && Objects.nonNull(existeSolicitudPendiente)) {
					if (!existeSolicitudPendiente) {					
						if (existeSolicitudAnulada) {
							listSolicitud.clear();
						}
					}
				}		
				*/
				listSolicitud.clear();
				//imendoza 20170103
				parametros.keySet().removeIf(e -> (e == "codigoModular"));
				listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
			} else if (tipoSolicitud.equals(TipoSolicitudType.MODIFICACION.getKey())) {
				//imendoza 20170103
				if (Objects.nonNull(existeSolicitudAnulada) && Objects.nonNull(existeSolicitudPendiente)) {
					if (!existeSolicitudPendiente) {					
						if (existeSolicitudAnulada) {
							listSolicitud.clear();
						}
					}
				}				
				//imendoza 20170103
				parametros.keySet().removeIf(e -> (e == "codooii" || e == "nombrePrograma"));
				if (StringUtil.isNotNullOrBlank(codigoModular)) {
					parametros.put("codigoModular", codigoModular);
					listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
				} else {
					listPrograma = null;
				}
				parametros.keySet().removeIf(e -> (e == "codooii" || e == "codigoModular"));
				parametros.put("codigoParametro", "FECFINPERMOD");
				listParametro = parametroService.listarParametroFirstMaxResultParametros(5, 0, parametros);
				int estaFueraDePeriodo = 0;
				if (!listParametro.isEmpty()) {
					for (ParametroDTO parametro : listParametro) {
						estaFueraDePeriodo = FechaUtil.restarFechas(FechaUtil.obtenerFechaActual(),
								parametro.getValorFecha());
						if (estaFueraDePeriodo <= -1) {
							parametros.put("fueraPeriodoModificacion", true);
							break;
						} else {
							parametros.put("fueraPeriodoModificacion", false);
						}
					}
				}
				if (listPrograma != null) {
					if (!listPrograma.isEmpty()) {
						for (ProgramaDTO programa : listPrograma) {
							if (StringUtil.isNotNullOrBlank(programa.getTipoSituacion().getIdMaestro())) {
								parametros.put("puedeModificar",
										(programa.getTipoSituacion().getIdMaestro()
												.equals(listaTipoSituacionPrograma.stream()
														  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.RENOVADO.getKey()))
														  .findFirst()
														  .get()
														  .getIdMaestro()))
												|| programa.getTipoSituacion().getIdMaestro()
														.equals(listaTipoSituacionPrograma.stream()
																  .filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.CREADO.getKey()))
																  .findFirst()
																  .get()
																  .getIdMaestro())
																		? true : false);
								break;
							}
						}
					}
				}
			} else if (tipoSolicitud.equals(TipoSolicitudType.CIERRE.getKey())) {
				parametros.keySet().removeIf("nombrePrograma"::equals);
				listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
				parametros.put("existeSolicitudCierre", listSolicitud.stream().anyMatch(
						(p) -> p.getTipoSolicitud().getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey())));
				parametros.put("existeSolicitudModificacion", listSolicitud.stream().anyMatch(
						(p) -> p.getTipoSolicitud().getCodigoItem().equals(TipoSolicitudType.MODIFICACION.getKey())));
				parametros.put("existeSolicitudRenovacion", listSolicitud.stream().anyMatch(
						(p) -> p.getTipoSolicitud().getCodigoItem().equals(TipoSolicitudType.RENOVACION.getKey())));
			}
			parametros.put("existeSolicitud", !listSolicitud.isEmpty());
			parametros.put("existeSolicitudProceso", existeSolicitudProceso);
			/*if (listPrograma != null) //walter 02/10/2018
				parametros.put("existePrograma", !listPrograma.isEmpty());*/
			if (listPrograma != null){
				parametros.put("existePrograma", !listPrograma.isEmpty());
				if(listPrograma.size()>0)
					parametros.put("esCerrado", listPrograma.get(0).getEstado().equals("1") || listPrograma.get(0).getEstado().equals("9") ? false:true);
			}
			
			List<String> eliminar = Arrays.asList("codooii", "nombrePrograma", "estado", "codigoParametro",
					"codigoModular");
			parametros.keySet().removeIf(e -> (eliminar.contains(e)));
			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " +e);
			}
			parametros.keySet().removeIf(d -> (d == "codooii" || d == "nombrePrograma" || d == "estado"
					|| d == "codigoParametro" || d == "codigoModular"));
			parametros.put("existeSolicitud", false);
			parametros.put("existePrograma", false);
			respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", new ConcurrentHashMap<>(parametros));
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}finally {
			parametros.clear();//imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}


	
	@SuppressWarnings("rawtypes")
	@POST
	@Path("/controlDeAvance")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response controlDeAvance(String jsonEncrypted) {
		List<Object> lista = new ArrayList<Object>();
		ControlAvanceFilter consultaJson = new ControlAvanceFilter();
		aesUtil = new AesUtil(keySize, iterationCount);

		parametros = new ConcurrentHashMap<>();
		String respEncryp = "";

		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			consultaJson = gson.fromJson(objJsonString, ControlAvanceFilter.class);

//			MaestroDTO maestroPeriodo=new MaestroDTO(listaTipoPeriodo.stream()
//					  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2018.getKey()))
//					  .findFirst()
//					  .get()
//					  .getIdMaestro());

			if (StringUtil.isNotNullOrBlank(consultaJson.getTipo())) {
				switch (consultaJson.getTipo()) {
				case "1":// control de solicitudes
				case "2":
				case "6":// Indicador para Dre //imendoza 20170206 inicio
				case "7":// Indicador para Dre Historial//imendoza 20170206 inicio
					parametros.put(1, TipoSolicitudType.CREACION.getKey());
					parametros.put(2, TipoSolicitudType.MODIFICACION.getKey());
					parametros.put(3, TipoSolicitudType.RENOVACION.getKey());
					parametros.put(4, TipoSolicitudType.CIERRE.getKey());
					parametros.put(5, consultaJson.getCodoii());
					if (!consultaJson.getPeriodo().equals("0")) {
						parametros.put(6, consultaJson.getPeriodo());
					}
					break;
				case "3":// control de programas
				case "5":// Indicador para Dre //imendoza 20170206 inicio
					parametros.put(1, TipoProgramaType.CICLO_I_ENTORNO_FAMILIAR.getKey());
					parametros.put(2, TipoProgramaType.CICLO_I_ENTORNO_COMUNITARIO.getKey());
					parametros.put(3, TipoProgramaType.CICLO_I_SET.getKey());
					parametros.put(4, TipoProgramaType.CICLO_II_ENTORNO_FAMILIAR.getKey());
					parametros.put(5, TipoProgramaType.CICLO_II_ENTORNO_COMUNITARIO.getKey());
					parametros.put(6, consultaJson.getCodoii());
					break;
				case "4":// control de programas total
					parametros.put(1, TipoProgramaType.CICLO_I_ENTORNO_FAMILIAR.getKey());
					parametros.put(2, TipoProgramaType.CICLO_I_ENTORNO_COMUNITARIO.getKey());
					parametros.put(3, TipoProgramaType.CICLO_I_SET.getKey());
					parametros.put(4, TipoProgramaType.CICLO_II_ENTORNO_FAMILIAR.getKey());
					parametros.put(5, TipoProgramaType.CICLO_II_ENTORNO_COMUNITARIO.getKey());
					break;
				default:
					break;
				}
			}

			lista = objectService.controlDeAvance(parametros, consultaJson.getTipo());
			// if(lista!=null && lista.size()>0)respuesta = new RespuestaDTO(true, "[]",
			// lista);
			respuesta = new RespuestaDTO(true, "[]", lista);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		// return
		// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

//	@SuppressWarnings("rawtypes")
//	@POST
//	//@Path("/controlDeAvance/{codigoDreUgel}")
//	@Path("/controlDeAvance")
//	@Produces(MediaType.APPLICATION_JSON)
////	public Response controlDeAvance(@PathParam("codigoDreUgel") String codigo,  @QueryParam("tipo") String tipo ,@QueryParam("periodo") String periodo) {
//	public Response controlDeAvance(String jsonEncrypted) {
//		List<Object> lista = new ArrayList<Object>();
//		ControlAvanceFilter consultaJson = new ControlAvanceFilter();
//		aesUtil = new AesUtil(keySize, iterationCount);	
//		
//		parametros = new ConcurrentHashMap<>();
//		try {
//			
//			
//			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
//			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
//			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
//			
//			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
//
//			consultaJson = gson.fromJson(objJsonString, ControlAvanceFilter.class);
//			
////			MaestroDTO maestroPeriodo=new MaestroDTO(listaTipoPeriodo.stream()
////					  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2018.getKey()))
////					  .findFirst()
////					  .get()
////					  .getIdMaestro());
//			
//			if (StringUtil.isNotNullOrBlank(consultaJson.getTipo())) {
//				switch (consultaJson.getTipo()) {
//				case "1"://control de solicitudes
//				case "2":
//				case "6"://Indicador para Dre //imendoza 20170206 inicio
//				case "7"://Indicador para Dre Historial//imendoza 20170206 inicio
//					parametros.put(1, TipoSolicitudType.CREACION.getKey());
//					parametros.put(2, TipoSolicitudType.MODIFICACION.getKey());
//					parametros.put(3, TipoSolicitudType.RENOVACION.getKey());
//					parametros.put(4, TipoSolicitudType.CIERRE.getKey());
//					parametros.put(5, consultaJson.getCodoii());
//					if(!consultaJson.getPeriodo().equals("0")){
//						parametros.put(6, consultaJson.getPeriodo());
//					}
//					break;
//				case "3"://control de programas
//				case "5"://Indicador para Dre //imendoza 20170206 inicio
//					parametros.put(1, TipoProgramaType.CICLO_I_ENTORNO_FAMILIAR.getKey());
//					parametros.put(2, TipoProgramaType.CICLO_I_ENTORNO_COMUNITARIO.getKey());
//					parametros.put(3, TipoProgramaType.CICLO_I_SET.getKey());
//					parametros.put(4, TipoProgramaType.CICLO_II_ENTORNO_FAMILIAR.getKey());
//					parametros.put(5, TipoProgramaType.CICLO_II_ENTORNO_COMUNITARIO.getKey());
//					parametros.put(6, consultaJson.getCodoii());
//					break;
//				case "4"://control de programas total
//					parametros.put(1, TipoProgramaType.CICLO_I_ENTORNO_FAMILIAR.getKey());
//					parametros.put(2, TipoProgramaType.CICLO_I_ENTORNO_COMUNITARIO.getKey());
//					parametros.put(3, TipoProgramaType.CICLO_I_SET.getKey());
//					parametros.put(4, TipoProgramaType.CICLO_II_ENTORNO_FAMILIAR.getKey());
//					parametros.put(5, TipoProgramaType.CICLO_II_ENTORNO_COMUNITARIO.getKey());
//					break;
//				default:
//					break;
//				}
//			}		
//					
//			
//			lista = objectService.controlDeAvance(parametros, consultaJson.getTipo());
//		//	if(lista!=null && lista.size()>0)respuesta = new RespuestaDTO(true, "[]", lista);
//			respuesta = new RespuestaDTO(true, "[]", lista);
//		} catch (Exception e) {
//			if (log.isHabilitadoError()) {
//				log.error(e);
//			}
//			parametros.clear();
//			respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", parametros);
//			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
//		}finally {
//			parametros.clear();//imendoza 20170104
//		}
//		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
//	}

//	@SuppressWarnings("rawtypes")
//	@POST
//	@Path("/reporte")
//	@Produces(MediaType.APPLICATION_OCTET_STREAM)
//	public Response reporte(String jsonEncrypted,
//			@javax.ws.rs.core.Context HttpServletResponse response) {
//		List<Object[]> lista = new ArrayList<Object[]>();
//		ReporteGenerico reporteGenerico = new ReporteGenerico();
//		aesUtil = new AesUtil(keySize, iterationCount);	
//		UsuarioDTO usuario=new UsuarioDTO();
//		try {
//			//INICIO desencriptar			
//			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
//			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
//			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
//			//dawnlkasdnas  verificar la validacion del ebcrypt al final de la desvarga////////////////
//			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
//
//			reporteGenerico = gson.fromJson(objJsonString, ReporteGenerico.class);
//	
//			
//			
//			
//			//FIN desencriptar			
//			String valor = StringUtil.isNotNullOrBlank(reporteGenerico.getConsultaJson().getGroupFilter())
//					? reporteGenerico.getConsultaJson().getGroupFilter().toString() : "";
//			lista = objectService.findByFirstMaxResultGenerico(reporteGenerico.getConsultaJson().obtenerFields(), valor,
//					reporteGenerico.getConsultaJson().getTable(), reporteGenerico.getConsultaJson().getOrder(), 100000,
//					reporteGenerico.getConsultaJson().getIndex());
////			if (!lista.isEmpty()) {
//				ExportExcelUtil export = new ExportExcelUtil(lista, reporteGenerico.getListaCabecera(),
//						reporteGenerico.getNombreReporte(), response);
//				
//				response.addHeader("x-frame-options", "DENY");
//				//response.addHeader("x-frame-options","SAMEORIGIN");
//				response.addHeader("X-XSS-Protection", "1; mode=block");
//				response.addHeader("X-Content-Type-Options", "nosniff");
//				
//				response.setContentType("application/vnd.ms-excel");
//				response.setHeader("Content-Disposition",
//						"attachment; filename=" + reporteGenerico.getNombreReporte().toString() + "."
//								+ TipoArchivoType.XLS.getDescription(ResourceUtil.obtenerLocaleSession()));
//			///////////////////////////////////////
//			//	for(int i=0;i<lista.size();i++){
//			
//			//	}
//			//////////////////////////////////////	 
//				export.doExport(lista,usuarioCreador);
//		
//			
//		/*	
//			} else if (lista.isEmpty()) {
//				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
//
//			
//			
//			
//			
//			}
//		*/
//		} catch (Exception e) {
//			if (LOG.isErrorEnabled()) {
//				LOG.error("Error producido : " +e);
//			}
//			parametros.clear();
//			respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", parametros);
//			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
//		}finally {
//			parametros.clear();//imendoza 20170104
//		}
//		return Response.status(Status.OK).entity(gson.toJson("Exito").toString()).build();
//	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/reporte")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response reporte(String jsonEncrypted, @javax.ws.rs.core.Context HttpServletResponse response) {
		List<Object[]> lista = new ArrayList<Object[]>();
		ReporteGenerico reporteGenerico = new ReporteGenerico();
		aesUtil = new AesUtil(keySize, iterationCount);
		Boolean sucess;
		try {
			// INICIO desencriptar
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			reporteGenerico = gson.fromJson(objJsonString, ReporteGenerico.class);
			//////////////////////////////////////////////
			String usuarioCreador = reporteGenerico.getUsuarioCreador();

			sucess = AuthUtils.validarTokenRequest(reporteGenerico.getMain());
			if (sucess) {
				String valor = StringUtil.isNotNullOrBlank(reporteGenerico.getConsultaJson().getGroupFilter())
						? reporteGenerico.getConsultaJson().getGroupFilter().toString()
						: "";
				lista = objectService.findByFirstMaxResultGenerico(reporteGenerico.getConsultaJson().obtenerFields(),
						valor, reporteGenerico.getConsultaJson().getTable(),
						reporteGenerico.getConsultaJson().getOrder(), 100000,
						reporteGenerico.getConsultaJson().getIndex());
				ExportExcelUtil export = new ExportExcelUtil(lista, reporteGenerico.getListaCabecera(),
						reporteGenerico.getNombreReporte(), response);
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition",
						"attachment; filename=" + reporteGenerico.getNombreReporte().toString() + "."
								+ TipoArchivoType.XLS.getDescription(ResourceUtil.obtenerLocaleSession()));
				export.doExport(lista, usuarioCreador);

//				
//				parametros.put("param", param); //Se agrega por validacion de intromision de tramas  werr
//				parametros.put("chekstatetk", true);
//				respuesta = new RespuestaDTO(true, "['']", parametros);
			} else {
				throw new Exception();
//				parametros.put("chekstatetk", false);
//				respuesta = new RespuestaDTO(false, "['']", parametros);
			}

			//////////////////////////////////////
			// FIN desencriptar

		} catch (Exception e) {
//			if (log.isHabilitadoError()) {
//				log.error(e);
//			}
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson("Exito").toString()).build();
	}

//	@SuppressWarnings("rawtypes")
//	@POST
//	//@Path("/donwloadTemplate/{idDoc}")
//	@Path("/donwloadTemplate")
//	@Produces(MediaType.APPLICATION_OCTET_STREAM)	
//	public Response donwloadTemplate(String idDoc , @javax.ws.rs.core.Context HttpServletResponse response) {
//		Properties propiedades = new Properties();
//		InputStream in = getClass().getResourceAsStream("/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
//		HSSFWorkbook workBook = new HSSFWorkbook();
//
//		try {
//			propiedades.load(in);
//			String pathFile = propiedades.getProperty("directorio.path");
//			in.close();
//			String nombreArchivo="CoordenadasProgramas.xls";
//			String nombreArchivoOut="CoordenadasProgramas";
//			
//			ServletOutputStream stream = response.getOutputStream();
//			
////			OutputStream stream = response.getOutputStream();
//			InputStream inputStream;
//			FileInputStream input;
//			File file = new File(pathFile+nombreArchivo);
//			if(file.exists()){
//				input = new FileInputStream(pathFile+nombreArchivo);
//			}else{
//				input = new FileInputStream(pathFile+"NO_FILE.xls");
//			}
//			inputStream=input;
//			
//			   //Get the contents of an InputStream as a byte[].
//	        byte[] bytes = IOUtils.toByteArray(inputStream);
//	       
//			BufferedInputStream buf = new BufferedInputStream(input);
//	        int readBytes = 0;
//			try {
//		        while ((readBytes = buf.read()) != -1) {
//		            stream.write(readBytes);
//		        }		
//
//
//				response.setHeader("Content-Disposition",
//						"attachment; filename=" + nombreArchivoOut.toString() + "."
//								+ TipoArchivoType.XLS.getDescription(ResourceUtil.obtenerLocaleSession()));
//				
//				
//				workBook.write(stream);
//				
//	
//				response.getOutputStream().flush();
//		       // stream.flush();
//			} finally {		
//				
//		         buf.close();
//		         input.close();
//			}
//		} catch (Exception e) {
//			//parametros.clear();
//			respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", parametros);
//			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
//		}
//		finally {
//			//parametros.clear();
//		}
//		return Response.status(Status.OK).entity(gson.toJson("Exito").toString()).build();
//	}

	@SuppressWarnings("rawtypes")
	@POST
	// @Path("/donwloadTemplate/{idDoc}")
	@Path("/donwloadTemplate")
	@Produces("application/vnd.ms-excel")
	public Response donwloadTemplate(String idDoc, @javax.ws.rs.core.Context HttpServletResponse response) {
		Properties propiedades = new Properties();
		InputStream in = getClass()
				.getResourceAsStream("/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
		HSSFWorkbook workBook = new HSSFWorkbook();
		ResponseBuilder responset = null;
		try {
			propiedades.load(in);
			String pathFile = propiedades.getProperty("directorio.path");
			in.close();
			String nombreArchivo = "CoordenadasProgramas.xls";

			response.addHeader("x-frame-options", "DENY");
			// response.addHeader("x-frame-options","SAMEORIGIN");
			response.addHeader("X-XSS-Protection", "1; mode=block");
			response.addHeader("X-Content-Type-Options", "nosniff");
			response.setContentType("application/vnd.ms-excel");

			response.addHeader("Content-Disposition", "attachment; filename=CoordenadasProgramas.xls");

//			String nombreArchivoOut="CoordenadasProgramas";

			ServletOutputStream stream = response.getOutputStream();

//			OutputStream stream = response.getOutputStream();
//			InputStream inputStream;
//			FileInputStream input;
			File file = new File(pathFile + nombreArchivo);
			responset = Response.ok((Object) file);
//			responset.header("Content-Disposition",
//					"attachment; filename=CoordenadasProgramas.xls");
		} catch (Exception e) {
			System.out.println("ERROR POR : " + e);
		}
		return responset.build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/descargaArchivo/{idDoc}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response descargaArchivo(@PathParam("idDoc") String nombreArchivo,
			@javax.ws.rs.core.Context HttpServletResponse response) {
		Properties propiedades = new Properties();
		InputStream in = getClass()
				.getResourceAsStream("/pe/gob/minedu/escale/regprogramas/ws/resources/configuracion.properties");
		try {
			propiedades.load(in);
			String pathFile = propiedades.getProperty("directorio.path");
			in.close();

			response.addHeader("x-frame-options", "DENY");
			// response.addHeader("x-frame-options","SAMEORIGIN");
			response.addHeader("X-XSS-Protection", "1; mode=block");
			response.addHeader("X-Content-Type-Options", "nosniff");
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=\"" + nombreArchivo + "\"");
			ServletOutputStream stream = response.getOutputStream();
			FileInputStream input;
			File file = new File(pathFile + nombreArchivo);
			if (file.exists()) {
				input = new FileInputStream(pathFile + nombreArchivo);
			} else {
				input = new FileInputStream(pathFile + "NO_FILE.pdf");
			}

			BufferedInputStream buf = new BufferedInputStream(input);
			int readBytes = 0;
			try {
				while ((readBytes = buf.read()) != -1) {
					stream.write(readBytes);
				}
				stream.flush();
			} finally {
				buf.close();
				input.close();
			}
		} catch (Exception e) {
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson("Exito").toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/anularSolicitud/{codSolicitud}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response anularSolicitud(@PathParam("codSolicitud") String codSolicitud,
			@QueryParam("observacion") String observacion, @QueryParam("codoidUsuario") String codoidUsuario) {
		parametros = new ConcurrentHashMap<>();
		ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();
		try {
			SolicitudDTO solicitud = new SolicitudDTO();
			solicitud.setUsuarioModificacion(codoidUsuario);
			if (StringUtil.isNotNullOrBlank(observacion)) {
				observacionSolicitud.setObservacion(observacion);
			}
			respuesta = solicitudService
					.gestionAccionSolicitud(
							listaTipoSituacionSolicitud.stream()
									.filter(p -> p.getCodigoItem()
											.equals(TipoSituacionSolicitudType.ANULADO.getKey().toString()))
									.findFirst().get().getIdMaestro().toString(),
							codSolicitud, observacionSolicitud, "", solicitud, false);
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/aprobarSolicitud")
	@Produces(MediaType.APPLICATION_JSON)
	public Response aprobarSolicitud(String jsonEncrypted) {
//	public Response aprobarSolicitud(@PathParam("idSolicitud") Long idSolicitud, @QueryParam("observacion") String observacion,
//			@QueryParam("perfil") String perfil, @QueryParam("codigoCentroPoblado") String codigoCentroPoblado,
//			@QueryParam("codigoAreaCentroPoblado") String codigoAreaCentroPoblado, @QueryParam("codigoAreaSig") String codigoAreaSig) {
		TipoSolicitudFilter consultaJson = new TipoSolicitudFilter();
		aesUtil = new AesUtil(keySize, iterationCount);
		parametros = new ConcurrentHashMap<>();
		List<SolicitudDTO> listaSolicitud = new ArrayList<SolicitudDTO>();
		SolicitudDTO solicitud = new SolicitudDTO();
		ProgramaDTO programa = new ProgramaDTO();
		CodigoModularDTO codigo = new CodigoModularDTO();
		ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();
		AuditoriaProgramaDTO aud = new AuditoriaProgramaDTO();
		List<MaestroDTO> listaTipoSolValidaRevisor;
		boolean isTipoSolicitudValida = false;
		boolean isTipoSituacionRevisorSolicitudInvalida = false;
		Long idTipoSituacionRevisionSolicitud;
		Long idTipoSituacionRevisionSigSolicitud;
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			consultaJson = gson.fromJson(objJsonString, TipoSolicitudFilter.class);

			if (StringUtil.isNotNullOrBlank(consultaJson.getObsSolicitud())) {
				observacionSolicitud.setObservacion(consultaJson.getObsSolicitud());
			}
			solicitud = solicitudService.obtenerSolicitudById(Long.parseLong(consultaJson.getCodSolicitud()));
			if (solicitud != null) {
				Long idTipoSolicitud = solicitud.getTipoSolicitud().getIdMaestro();
				idTipoSituacionRevisionSolicitud = Objects.nonNull(solicitud.getTipoSituacionRevision().getIdMaestro())
						? solicitud.getTipoSituacionRevision().getIdMaestro()
						: 0L;
				idTipoSituacionRevisionSigSolicitud = Objects
						.nonNull(solicitud.getTipoSituacionRevisionSig().getIdMaestro())
								? solicitud.getTipoSituacionRevisionSig().getIdMaestro()
								: 0L;
				isTipoSolicitudValida = listaTipoSolicitud.stream()
						.anyMatch(p -> p.getIdMaestro().equals(idTipoSolicitud));
				listaTipoSolValidaRevisor = listaTipoSituacionRevision.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey())
								|| p.getCodigoItem().equals(TipoSituacionRevisionType.APROBADO.getKey()))
						.collect(Collectors.toList());

				if (StringUtil.isNotNullOrBlank(consultaJson.getPerfilUsuario())) {
//					if (consultaJson.getPerfilUsuario().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString()) ) {						
					if (consultaJson.getPerfilUsuario()
							.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_REGPRO.getValue().toString())) {
						isTipoSituacionRevisorSolicitudInvalida = listaTipoSolValidaRevisor.stream()
								.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSolicitud));
//					} else if (consultaJson.getPerfilUsuario().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
					} else if (consultaJson.getPerfilUsuario()
							.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG_REGPRO.getValue().toString())) {
						isTipoSituacionRevisorSolicitudInvalida = listaTipoSolValidaRevisor.stream()
								.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSigSolicitud));
					} else {
						throw new RevisionCamposSolicitudNoExistePerfilException();
					}
				}
				if (isTipoSolicitudValida) {
					if (!isTipoSituacionRevisorSolicitudInvalida) {
//						if (consultaJson.getPerfilUsuario().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {
						if (consultaJson.getPerfilUsuario()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_REGPRO.getValue().toString())) {
							solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.APROBADO.getKey()))
									.findFirst().get().getIdMaestro()));
							if (solicitud.getTipoSolicitud().getIdMaestro()
									.equals(listaTipoSolicitud.stream()
											.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))
											.findFirst().get().getIdMaestro())) {
								// INICIO - Solo en caso de SOL CIERRE, vasta con la aprobacion del REVISOR,
								// artificio para aprobar por SIG
								solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionRevisionType.APROBADO.getKey()))
										.findFirst().get().getIdMaestro()));
								// FIN
							}
						} else
//						 if(consultaJson.getPerfilUsuario().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())){
						if (consultaJson.getPerfilUsuario()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG_REGPRO.getValue().toString())) {
							solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.APROBADO.getKey()))
									.findFirst().get().getIdMaestro()));
							solicitud.setCodigoCentroPoblado(StringUtil.isNotNullOrBlank(consultaJson.getCodCPoblado())
									? consultaJson.getCodCPoblado()
									: null);
							solicitud.setCodigoArea(
									StringUtil.isNotNullOrBlank(consultaJson.getCodArea()) ? consultaJson.getCodArea()
											: null);
							solicitud.setCodigoAreaSig(
									Objects.nonNull(consultaJson.getCodAreaSig()) ? consultaJson.getCodAreaSig() : "");
						}
						solicitud = solicitudService.actualizar(solicitud);

						if ((solicitud.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision
								.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.APROBADO.getKey()))
								.findFirst().get().getIdMaestro()))
								&& (solicitud.getTipoSituacionRevisionSig().getIdMaestro()
										.equals(listaTipoSituacionRevision.stream()
												.filter(p -> p.getCodigoItem()
														.equals(TipoSituacionRevisionType.APROBADO.getKey()))
												.findFirst().get().getIdMaestro()))) {
							solicitud.setIndicadorUltimo(EstadoState.ACTIVO.getValue());
							if (solicitud.getTipoSolicitud().getIdMaestro()
									.equals(listaTipoSolicitud.stream()
											.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey()))
											.findFirst().get().getIdMaestro())) {

								codigo = codigoModularService.obtenerCodigoModular();
								solicitud.setCodigoModular(codigo.getCodigoModular());
								solicitud = solicitudService.actualizar(solicitud);
								parametros.clear();
								programa = convertirSolicitudAPrograma(solicitud);
								programa = programaService.registrarProgramaNuevo(programa);

								respuesta = solicitudService
										.gestionAccionSolicitud(
												listaTipoSituacionSolicitud.stream()
														.filter(p -> p.getCodigoItem()
																.equals(TipoSituacionSolicitudType.APROBADO.getKey()
																		.toString()))
														.findFirst().get().getIdMaestro().toString(),
												solicitud.getCodSolicitud(), observacionSolicitud, "", null, null);

								codigo.setEstadoAsignado(
										CondicionState.SI.getDescription(ResourceUtil.obtenerLocaleSession()));
								codigo.setFechaAsignacion(FechaUtil.obtenerFechaActual());
								codigo.setCodigoModular(null);
								codigo.setCodoiiAsignado(solicitud.getDreUgel().getCodigo());
								codigoModularService.actualizar(codigo);
							} else if ((solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.MODIFICACION.getKey()))// MODIFICADO
									.findFirst().get().getIdMaestro()))
									|| (solicitud.getTipoSolicitud().getIdMaestro()
											.equals(listaTipoSolicitud.stream()
													.filter(p -> p.getCodigoItem()
															.equals(TipoSolicitudType.RENOVACION.getKey()))// ACTUALIZADO
													.findFirst().get().getIdMaestro()))
									|| (solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
											.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))// CERRAR
											.findFirst().get().getIdMaestro()))) {
								programa = programaService.buscarProgramaxCodigoModular(solicitud.getCodigoModular());
								ConversorHelper.fusionaPropiedades(programa, aud);
								aud.setIdPrograma(null);
								aud = auditoriaProgramaService.registrarProgramaNuevo(aud);
								ConversorHelper.fusionaPropiedades(convertirSolicitudAPrograma(solicitud), programa);
								programa = programaService.actualizar(programa);
								respuesta = solicitudService
										.gestionAccionSolicitud(
												listaTipoSituacionSolicitud.stream()
														.filter(p -> p.getCodigoItem()
																.equals(TipoSituacionSolicitudType.APROBADO.getKey()
																		.toString()))
														.findFirst().get().getIdMaestro().toString(),
												solicitud.getCodSolicitud(), observacionSolicitud, "", solicitud, // actualiza
																													// la
																													// solicitud
												null);
								listaSolicitud = solicitudService.buscarSolicitud("", "", solicitud.getCodigoModular(),
										"");
								for (SolicitudDTO soli : listaSolicitud) {
									if (!soli.getIdSolicitud().equals(solicitud.getIdSolicitud())) {
										soli.setIndicadorUltimo(EstadoState.INACTIVO.getValue());
										solicitudService.actualizar(soli);
									}
								}

							} else {
								throw new TipoSolicitudNoExisteException(
										StringUtil.isNotNullOrBlank(solicitud.getTipoSolicitud().getIdMaestro())
												? solicitud.getTipoSolicitud().getIdMaestro().toString()
												: "");
							}
						} else {
							respuesta = solicitudService.procesarRevision(solicitud);
						}
					} else {
						throw new SolicitudRevisadaException();
					}
				} else {
					throw new TipoSolicitudNoExisteException();
				}
			} else {
				throw new SolicitudNoExisteException(StringUtil.isNotNullOrBlank(consultaJson.getCodSolicitud())
						? consultaJson.getCodSolicitud().toString()
						: "");
			}
		} catch (ProgramaNoRegistradoException | CodigoSolicitudRepetidoException | SolicitudNoRegistradaException
				| CodigoModularNoRegistradoException | ConversorException | SolicitudNoExisteException
				| ProgramaNoActualizadoException | AuditoriaProgramaNoRegistradoException
				| TipoSolicitudNoExisteException | SolicitudRevisadaException er) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : ");

			respuesta = new RespuestaDTO(success, "['"
					+ getErrorService().getErrorFor(er, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);
			respuesta = new RespuestaDTO(success, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	private ProgramaDTO convertirSolicitudAPrograma(SolicitudDTO solicitud) throws Exception {
		ProgramaDTO programa = new ProgramaDTO();

		List<SolicitudDocumentoDTO> listaSolicitudDocumento = solicitudDocumentoService
				.findByIdSolicitud(solicitud.getIdSolicitud());
		Long idMaestro = listaTipoDocumento.stream()
				.filter(d -> d.getCodigoItem().equals(TipoDocumentoType.RESOLUCION.getKey())).findFirst().get()
				.getIdMaestro();
		SolicitudDocumentoDTO doc = listaSolicitudDocumento.stream()
				.filter(p -> p.getDocumento().getTipoDocumento().getIdMaestro().equals(idMaestro)).findFirst().get();

		programa.setCodigoDocumento(doc.getDocumento().getCodigoDocumento());
		programa.setFechaDocumento(doc.getDocumento().getFechaDocumento());
		programa.setTipoResolucion(StringUtil.isNotNullOrBlank(doc.getDocumento().getTipoDocumentoResolucion())
				? new MaestroDTO(doc.getDocumento().getTipoDocumentoResolucion().getIdMaestro())
				: new MaestroDTO(0L));
		programa.setNumeroDocumento(doc.getDocumento().getNroDocumento());
		if (!solicitud.getTipoSolicitud().getIdMaestro()
				.equals(listaTipoSolicitud.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey())).findFirst().get()
						.getIdMaestro())) {
			programa.setLocalidad(
					StringUtil.isNotNullOrBlank(solicitud.getLocalidad()) ? solicitud.getLocalidad() : null);
			programa.setArea(StringUtil.isNotNullOrBlank(solicitud.getCodigoArea()) ? solicitud.getCodigoArea() : null);
			programa.setCodigoAreaSig(
					StringUtil.isNotNullOrBlank(solicitud.getCodigoAreaSig()) ? solicitud.getCodigoAreaSig() : null);
			programa.setCodigoCentroPoblado(
					StringUtil.isNotNullOrBlank(solicitud.getCodigoCentroPoblado()) ? solicitud.getCodigoCentroPoblado()
							: null);
			programa.setCodigoModular(
					StringUtil.isNotNullOrBlank(solicitud.getCodigoModular()) ? solicitud.getCodigoModular() : null);
			programa.setCodigoServicioEduMasCercano(
					StringUtil.isNotNullOrBlank(solicitud.getCodigoServicioEduMasCercano())
							? solicitud.getCodigoServicioEduMasCercano()
							: null);
			programa.setDistrito(new DistritosDTO(solicitud.getDistrito().getIdDistrito()));
			programa.setDreUgel(new DreUgelDTO(solicitud.getDreUgel().getCodigo()));
			programa.setEstado(EstadoProgramaState.ACTIVO.getValue());
			programa.setTipoEstado(new MaestroDTO(listaTipoEstadoPrograma.stream()
					.filter(p -> p.getCodigoItem().equals(TipoEstadoProgramaType.ACTIVO.getKey())).findFirst().get()
					.getIdMaestro()));// 308 activo, 309 inactivo
			programa.setEtapa(StringUtil.isNotNullOrBlank(solicitud.getEtapa()) ? solicitud.getEtapa() : null);
			programa.setFechaCreacion(FechaUtil.obtenerFechaActual());
			programa.setGeoHash(StringUtil.isNotNullOrBlank(solicitud.getGeoHash()) ? solicitud.getGeoHash() : "");
			programa.setLatitudCentroPoblado(StringUtil.isNotNullOrBlank(solicitud.getLatitudCentroPoblado())
					? solicitud.getLatitudCentroPoblado()
					: null);
			programa.setLongitudCentroPoblado(StringUtil.isNotNullOrBlank(solicitud.getLongitudCentroPoblado())
					? solicitud.getLongitudCentroPoblado()
					: null);
			programa.setLatitudPrograma(
					StringUtil.isNotNullOrBlank(solicitud.getLatitudPrograma()) ? solicitud.getLatitudPrograma()
							: null);
			programa.setLongitudPrograma(
					StringUtil.isNotNullOrBlank(solicitud.getLongitudPrograma()) ? solicitud.getLongitudPrograma()
							: null);
			programa.setLatitudServicioEduMasCercano(
					StringUtil.isNotNullOrBlank(solicitud.getLatitudServicioEduMasCercano())
							? solicitud.getLatitudServicioEduMasCercano()
							: null);
			programa.setLongitudServicioEduMasCercano(
					StringUtil.isNotNullOrBlank(solicitud.getLongitudServicioEduMasCercano())
							? solicitud.getLongitudServicioEduMasCercano()
							: null);
			programa.setLote(StringUtil.isNotNullOrBlank(solicitud.getLote()) ? solicitud.getLote() : null);
			programa.setManzana(StringUtil.isNotNullOrBlank(solicitud.getManzana()) ? solicitud.getManzana() : null);
			programa.setNombreCentroPoblado(
					StringUtil.isNotNullOrBlank(solicitud.getNombreCentroPoblado()) ? solicitud.getNombreCentroPoblado()
							: null);
			programa.setNombrePrograma(
					StringUtil.isNotNullOrBlank(solicitud.getNombrePrograma()) ? solicitud.getNombrePrograma() : null);
			programa.setNombreServicioEduMasCercano(
					StringUtil.isNotNullOrBlank(solicitud.getNombreServicioEduMasCercano())
							? solicitud.getNombreServicioEduMasCercano()
							: null);
			programa.setNombreVia(
					StringUtil.isNotNullOrBlank(solicitud.getNombreVia()) ? solicitud.getNombreVia() : null);
			programa.setNumeroVia(
					StringUtil.isNotNullOrBlank(solicitud.getNumeroVia()) ? solicitud.getNumeroVia() : null);
			programa.setOtraDireccion(
					StringUtil.isNotNullOrBlank(solicitud.getOtraDireccion()) ? solicitud.getOtraDireccion() : null);
			programa.setOtroProveedorAgua(
					StringUtil.isNotNullOrBlank(solicitud.getOtroProveedorAgua()) ? solicitud.getOtroProveedorAgua()
							: null);
			programa.setOtroProveedorEnergia(solicitud.getOtroProveedorEnergia());
			programa.setReferenciaDireccion(solicitud.getReferenciaDireccion());
			programa.setSector(solicitud.getSector());
			programa.setSuministroAgua(solicitud.getSuministroAgua());
			programa.setSuministroEnergia(solicitud.getSuministroEnergia());
			// Tipo inicio
			programa.setTipoContinuidadJornadaEscolar(
					StringUtil.isNotNullOrBlank(solicitud.getTipoContinuidadJornadaEscolar())
							? new MaestroDTO(solicitud.getTipoContinuidadJornadaEscolar().getIdMaestro())
							: new MaestroDTO(0L));
			programa.setTipoDependencia(StringUtil.isNotNullOrBlank(solicitud.getTipoDependencia())
					? new MaestroDTO(solicitud.getTipoDependencia().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoGestion(StringUtil.isNotNullOrBlank(solicitud.getTipoGestion())
					? new MaestroDTO(solicitud.getTipoGestion().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoGestionEducativa(StringUtil.isNotNullOrBlank(solicitud.getTipoGestionEducativa())
					? new MaestroDTO(solicitud.getTipoGestionEducativa().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoLocalidad(StringUtil.isNotNullOrBlank(solicitud.getTipoLocalidad())
					? new MaestroDTO(solicitud.getTipoLocalidad().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoPrograma(StringUtil.isNotNullOrBlank(solicitud.getTipoPrograma())
					? new MaestroDTO(solicitud.getTipoPrograma().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoProveedorAgua(StringUtil.isNotNullOrBlank(solicitud.getTipoProveedorAgua())
					? new MaestroDTO(solicitud.getTipoProveedorAgua().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoProveedorEnergia(StringUtil.isNotNullOrBlank(solicitud.getTipoProveedorEnergia())
					? new MaestroDTO(solicitud.getTipoProveedorEnergia().getIdMaestro())
					: new MaestroDTO(0L));

			if (solicitud.getTipoSolicitud().getIdMaestro()
					.equals(listaTipoSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey())).findFirst()
							.get().getIdMaestro())) {
				programa.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.CREADO.getKey())).findFirst()
						.get().getIdMaestro()));
				programa.setMarcocd("0");
				programa.setMarcoCensal("0");
			} else if (solicitud.getTipoSolicitud().getIdMaestro()
					.equals(listaTipoSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.MODIFICACION.getKey())).findFirst()
							.get().getIdMaestro())) {
				programa.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.MODIFICADO.getKey()))
						.findFirst().get().getIdMaestro()));
			} else if (solicitud.getTipoSolicitud().getIdMaestro()
					.equals(listaTipoSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.RENOVACION.getKey())).findFirst()
							.get().getIdMaestro())) {
				programa.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.RENOVADO.getKey())).findFirst()
						.get().getIdMaestro()));
			}
			programa.setTipoTurno(StringUtil.isNotNullOrBlank(solicitud.getTipoTurno())
					? new MaestroDTO(solicitud.getTipoTurno().getIdMaestro())
					: new MaestroDTO(0L));
			programa.setTipoVia(StringUtil.isNotNullOrBlank(solicitud.getTipoVia())
					? new MaestroDTO(solicitud.getTipoVia().getIdMaestro())
					: new MaestroDTO(0L));
			// Tipo Fin
			programa.setZona(solicitud.getZona());
			if (solicitud.getTipoSolicitud().getIdMaestro()
					.equals(listaTipoSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey())).findFirst()
							.get().getIdMaestro())) {
				programa.setZoom(9L);// Valor por defecto 9 รณ 10 รณ 11
				programa.setUsuarioCreacion(solicitud.getUsuarioCreacion());
				programa.setFechaCreacion(FechaUtil.obtenerFechaActual());
				programa.setFechaCreacionPrograma(FechaUtil.obtenerFechaActual());
			} else if ((solicitud.getTipoSolicitud().getIdMaestro()
					.equals(listaTipoSolicitud.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.RENOVACION.getKey())).findFirst()
							.get().getIdMaestro()))
					|| (solicitud.getTipoSolicitud().getIdMaestro()
							.equals(listaTipoSolicitud.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.MODIFICACION.getKey()))
									.findFirst().get().getIdMaestro()))) {
				programa.setUsuarioUltimaModificacion(solicitud.getUsuarioCreacion());
				programa.setFechaRenovacionPrograma(FechaUtil.obtenerFechaActual());
			}

		} else {
			programa.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
					.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.CERRADO.getKey())).findFirst().get()
					.getIdMaestro()));
			programa.setEstado(EstadoProgramaState.INACTIVO.getValue());
			programa.setTipoEstado(new MaestroDTO(listaTipoEstadoPrograma.stream()
					.filter(p -> p.getCodigoItem().equals(TipoEstadoProgramaType.INACTIVO.getKey())).findFirst().get()
					.getIdMaestro()));// 308 activo, 309 inactivo
			programa.setUsuarioUltimaModificacion(solicitud.getUsuarioCreacion());
			programa.setFechaCierrePrograma(FechaUtil.obtenerFechaActual());

		}
		return programa;
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/bandejaValidarSolicitud")
	@Produces(MediaType.APPLICATION_JSON)
	public Response bandejaValidarSolicitud(String jsonEncrypted) {
		parametros = new ConcurrentHashMap<>();
		List<SolicitudBandejaDTO> lista = new ArrayList<SolicitudBandejaDTO>();
		ConsultaJson consultaJson = new ConsultaJson();
		aesUtil = new AesUtil(keySize, iterationCount);
		try {
			// INICIO desencriptar
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			consultaJson = gson.fromJson(objJsonString, ConsultaJson.class);
			// FIN desencriptar

			//////////////////////////
			String tkMaster;
			List<AccesoDTO> listAceesos = new ArrayList<AccesoDTO>();
			AccesoDTO accdto = new AccesoDTO();
			accdto.setUsuario(consultaJson.getPivot());
			listAceesos = sesionServiceLocal.verificarNevegacionUserTupla(accdto);
			if (listAceesos != null) {
				tkMaster = listAceesos.get(0).getToken();
				if (tkMaster.equals(consultaJson.getShow())) {
					String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
							? consultaJson.getGroupFilter().toString()
							: "";
					lista = solicitudService.bandejaRevision(consultaJson.getIndex(), consultaJson.getBatchSize(),
							valor, consultaJson.getOrder());

					if (!lista.isEmpty()) {
						parametros.put("salt", salt);
						parametros.put("iv", iv);
						parametros.put("rare", false);
						parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
						respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
					} else if (lista.isEmpty()) {
						respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
					}

				} else {
					try {
						throw new CadenaInvalidaException();
					} catch (CadenaInvalidaException ex) {
						if (LOG.isErrorEnabled())
							LOG.error("error producido" + ex);
						parametros.put("rare", true);
						respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
						return Response.status(Status.NOT_ACCEPTABLE).entity(gson.toJson(respuesta).toString()).build();
					}

				}
			}
////////////////////

//			String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
//					? consultaJson.getGroupFilter().toString() : "";
//			lista = solicitudService.bandejaRevision(consultaJson.getIndex(), consultaJson.getBatchSize(), valor, consultaJson.getOrder());
//
//			if (!lista.isEmpty()) {
//				parametros.put("salt", salt);
//				parametros.put("iv", iv);
//				parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
//				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
//			} else if (lista.isEmpty()) {
//				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
//			}

		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);

			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", lista);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/inCheckChain")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response inCheckChain(String jsonEncrypted) {
		String respEncryp = "";
		TipoSolicitudFilter consultaJson = new TipoSolicitudFilter();
		String jsonEncrypteddcInexus = "";
		String jsonEncrypteddcJodexus = "";
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			consultaJson = gson.fromJson(objJsonString, TipoSolicitudFilter.class);
			try {
				jsonEncrypteddcInexus = aesUtil.descifrarBase64(consultaJson.getInexus().substring(9));
				jsonEncrypteddcJodexus = aesUtil.descifrarBase64(consultaJson.getJodexus());
			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}
			if (!jsonEncrypteddcJodexus.equals(jsonEncrypteddcInexus)) {
				throw new CadenaInvalidaException();
			} else {
				parametros.put("xtu", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			}
		} catch (CadenaInvalidaException ex) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + ex);

			parametros.put("rare", true);
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		}

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

//	@SuppressWarnings("rawtypes")
//	@POST
//	@Path("/inCheckChain")
//	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
//	public Response inCheckChain(String jsonEncrypted ){
//		String respEncryp="";
//		TipoSolicitudFilter consultaJson = new TipoSolicitudFilter();
//		String jsonEncrypteddcInexus="";
//		String jsonEncrypteddcJodexus = "";
//
//		TipoSolicitudFilter objInexus=null;
//		TipoSolicitudFilter objJodexus=null;
//		
//		try{
//			
//			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
//			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
//			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
//			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
//			consultaJson = gson.fromJson(objJsonString, TipoSolicitudFilter.class);
//			try{
//			jsonEncrypteddcInexus = aesUtil.descifrarBase64(consultaJson.getInexus().substring(9));
//			
//			objInexus=gson.fromJson(jsonEncrypteddcInexus, TipoSolicitudFilter.class);
//			
//			jsonEncrypteddcJodexus = aesUtil.descifrarBase64(consultaJson.getJodexus());
//			
//			objJodexus=gson.fromJson(jsonEncrypteddcJodexus, TipoSolicitudFilter.class);
//			
//			}catch(StringIndexOutOfBoundsException e){
//				throw new CadenaInvalidaException();
//			}catch(NullPointerException e){
//				throw new CadenaInvalidaException();
//			}
//			
//			List<Object[]> listPrivilegios=usuarioServiceLocal.validarUserPerfilPriv(consultaJson.getDniUsuario(),consultaJson.getPerfilUsuario(),consultaJson.getInit());
//			if(listPrivilegios==null||listPrivilegios.size()==0){
//				throw new CadenaInvalidaException();
//			}
//			
//			TipoSolicitudFilter objCampos = new TipoSolicitudFilter();
//			objCampos.setCodSolicitud(objInexus.getCodSolicitud());
//			objCampos.setObsSolicitud(objInexus.getObsSolicitud());
//			objCampos.setPerfilUsuario(objInexus.getPerfilUsuario());
//			objCampos.setCodArea(objInexus.getCodArea());
//			objCampos.setCodCPoblado(objInexus.getCodCPoblado());
//			objCampos.setCodAreaSig(objInexus.getCodAreaSig());
//			objCampos.setInit(objInexus.getInit());
//			
//			objCampos.setInit(objInexus.getInit()); /////////////////////////////////////////////////////////////
//			
//			
//			if(!objInexus.equals(objJodexus)||!objInexus.equals(objCampos)||!objJodexus.equals(objCampos))
//			{
//				throw new CadenaInvalidaException();
//			}else{
//				parametros.put("xtu", true);
//				respuesta = new RespuestaDTO(true, "['']", parametros);
//				respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
//			}
//		}catch(CadenaInvalidaException ex){
//			if (LOG.isErrorEnabled())LOG.error("Error producido : " +ex);
//			
//			parametros.put("rare", true);
//			respuesta = new RespuestaDTO(false, "['"+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() +"']", parametros);
//			respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
//			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
//		}
//		
//		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();	
//	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/revisarSolicitud")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response revisarSolicitud(String jsonEncrypted) {
		TipoSolicitudFilter consultaJson = new TipoSolicitudFilter();
		aesUtil = new AesUtil(keySize, iterationCount);
		parametros = new ConcurrentHashMap<>();
		SolicitudDTO solicitud = new SolicitudDTO();
		ObservacionSolicitudDTO observacion = new ObservacionSolicitudDTO();
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		List<MaestroDTO> listaTipoSolValidaRevisor;
		boolean isTipoSituacionRevisorSolicitudInvalida = false;
		String respEncryp = "";
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			consultaJson = gson.fromJson(objJsonString, TipoSolicitudFilter.class);

			if (StringUtil.isNotNullOrBlank(consultaJson.getCodSolicitud()))
				parametros.put("codSolicitud",
						consultaJson.getCodSolicitud().toUpperCase(ResourceUtil.obtenerLocaleSession()));
			parametros.put("estado", EstadoState.ACTIVO.getValue());
			listSolicitud = solicitudService.listarSolicitudFirstMaxResultParametros(100, 0, parametros);
			if (Objects.nonNull(listSolicitud) && !listSolicitud.isEmpty() && listSolicitud.size() == 1) {
				listaTipoSolValidaRevisor = listaTipoSituacionRevision.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.INICIO.getKey())
								|| p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey())
								|| p.getCodigoItem().equals(TipoSituacionRevisionType.APROBADO.getKey()))
						.collect(Collectors.toList());
				Long idTipoSituacionRevisionSolicitud = listSolicitud.get(0).getTipoSituacionRevision().getIdMaestro();
				Long idTipoSituacionRevisionSigSolicitud = listSolicitud.get(0).getTipoSituacionRevisionSig()
						.getIdMaestro();
				if (StringUtil.isNotNullOrBlank(consultaJson.getPerfilUsuario())) {
					if (consultaJson.getPerfilUsuario()
							.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {
						isTipoSituacionRevisorSolicitudInvalida = listaTipoSolValidaRevisor.stream()
								.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSolicitud));
					} else if (consultaJson.getPerfilUsuario()
							.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
						isTipoSituacionRevisorSolicitudInvalida = listaTipoSolValidaRevisor.stream()
								.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSigSolicitud));
					} else {
						throw new RevisionCamposSolicitudNoExistePerfilException();
					}
				}
			} else {
				throw new CodigoSolicitudRepetidoException();
			}
			if (!isTipoSituacionRevisorSolicitudInvalida) {
				if (StringUtil.isNotNullOrBlank(consultaJson.getNomUsuario())
						&& StringUtil.isNotNullOrBlank(consultaJson.getDniUsuario())
						&& StringUtil.isNotNullOrBlank(consultaJson.getPerfilUsuario())) {
					if (consultaJson.getPerfilUsuario()
							.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {
						solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.INICIO.getKey()))
								.findFirst().get().getIdMaestro()));
						solicitud.setUsuarioRevision(consultaJson.getDniUsuario());
						solicitud.setNombreUsuarioRevision(consultaJson.getNomUsuario());
					} else if (consultaJson.getPerfilUsuario()
							.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
						solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.INICIO.getKey()))
								.findFirst().get().getIdMaestro()));
						solicitud.setUsuarioRevisionSig(consultaJson.getDniUsuario());
						solicitud.setNombreUsuarioRevisionSig(consultaJson.getNomUsuario());
					}
				}
				observacion.setPerfil(consultaJson.getPerfilUsuario());
				solicitud.setUsuarioModificacion(consultaJson.getDniUsuario());
				solicitud.setNombreUsuarioModificacion(consultaJson.getNomUsuario());
				respuesta = solicitudService
						.gestionAccionSolicitud(
								listaTipoSituacionSolicitud.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionSolicitudType.REVISION.getKey()))
										.findFirst().get().getIdMaestro().toString(),
								consultaJson.getCodSolicitud(), observacion, "", solicitud, null);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			} else {
				throw new SolicitudRevisadaException();
			}
		} catch (CodigoSolicitudRepetidoException | RevisionCamposSolicitudNoExistePerfilException
				| SolicitudRevisadaException er) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + er);

			parametros.clear();
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(er, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);

			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		// return
		// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/inObserveChain")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response inObserveChain(String jsonEncrypted) {
		String respEncryp = "";
		ObservacionSolicitudDTO observacionSolicitud;
		String jsonEncrypteddcInexus = "";
		String jsonEncrypteddcJodexus = "";
		ObservacionSolCompareDTO objInexus = null;
		ObservacionSolCompareDTO objJodexus = null;
		try {
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			observacionSolicitud = gson.fromJson(objJsonString, ObservacionSolicitudDTO.class);

			try {
				jsonEncrypteddcInexus = aesUtil.descifrarBase64(observacionSolicitud.getInexus().substring(9));

				objInexus = gson.fromJson(jsonEncrypteddcInexus, ObservacionSolCompareDTO.class);

				jsonEncrypteddcJodexus = aesUtil.descifrarBase64(observacionSolicitud.getJodexus());

				objJodexus = gson.fromJson(jsonEncrypteddcJodexus, ObservacionSolCompareDTO.class);

			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}
		
			
			List<Object[]> listPrivilegios = usuarioServiceLocal.validarUserPerfilPriv(
					observacionSolicitud.getUsuarioCreacion(), observacionSolicitud.getPerfil(), objInexus.getInit());
			if (listPrivilegios == null || listPrivilegios.size() == 0) {
				throw new CadenaInvalidaException();
			}

			ObservacionSolCompareDTO objCampos = new ObservacionSolCompareDTO();
			objCampos.setIdSolicitud(objInexus.getIdSolicitud());
			objCampos.setListaCampos(objInexus.getListaCampos());
			objCampos.setObservacion(objInexus.getObservacion());
			objCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
			objCampos.setPerfil(objInexus.getPerfil());

			objCampos.setInit(objInexus.getInit()); /////////////////////////////////////////////////////////////

			if(StringUtils.isEmpty(objInexus.getObservacion()))throw new CadenaInvalidaException();
			if(StringUtils.isEmpty(objJodexus.getObservacion()))throw new CadenaInvalidaException();
			if(StringUtils.isEmpty(objCampos.getObservacion()))throw new CadenaInvalidaException();


			if (!objInexus.equals(objJodexus) || !objInexus.equals(objCampos) || !objJodexus.equals(objCampos)) {
				throw new CadenaInvalidaException();
			} else {
				parametros.put("xtu", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			}

			// verificacion de campos correspondientes por tipo de revisor
			String perfil = objInexus.getPerfil().trim();
			List<Long> campos = objInexus.getListaCampos();
			Long valueok = 0L;
			boolean isRevisor = false;
			boolean isRevisorSIG = false;
			for (int i = 0; i < campos.size(); i++) {
				if (perfil.equals(String.valueOf(3))) { // revisor
					System.out.println("ES REVISOR ");
					valueok = campos.get(i);
					if (valueok <= 0 || valueok > 11) {
						isRevisorSIG = true;
						break;
					} // throw new CadenaInvalidaException();
				}
				if (perfil.equals(String.valueOf(6))) { // revisor sig
					System.out.println("ES REVISOR SIG");
					valueok = campos.get(i);
					if (valueok <= 11 || valueok > 16) {
						isRevisor = true;
						break;
					} // throw new CadenaInvalidaException();
				}
			}

			if (isRevisorSIG)
				throw new CadenaInvalidaException();
			if (isRevisor)
				throw new CadenaInvalidaException();

		} catch (CadenaInvalidaException ex) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + ex);

			parametros.put("rare", true);
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		}

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/checkSecury")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response checkSecury(String jsonEncrypted) {

		String respEncryp = "";
		FormCreaSolicitudDTO formCreaSolicitud;
		String jsonEncrypteddcInexus = "";
		String jsonEncrypteddcJodexus = "";
		FormCreaSolicitudDTO objInexus = null;
		FormCreaSolicitudDTO objJodexus = null;
//		TransFormCipher transFormCipher=null;
		String objJsonString = "";

		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			System.out.println("PARAM : " + param );
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			System.out.println("SALT : " + salt );
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
			System.out.println("IV : " + iv );
			
			objJsonString = aesUtil.decrypt( salt , iv ,  passphrase , param );
			System.out.println("obteniendo : " + objJsonString);
			// throw new NullPointerException();

			formCreaSolicitud = gson.fromJson(objJsonString, FormCreaSolicitudDTO.class);

			try {
				jsonEncrypteddcInexus = aesUtil.descifrarBase64(formCreaSolicitud.getInexus().substring(9));

				objInexus = gson.fromJson(jsonEncrypteddcInexus, FormCreaSolicitudDTO.class);

				jsonEncrypteddcJodexus = aesUtil.descifrarBase64(formCreaSolicitud.getJodexus());

				objJodexus = gson.fromJson(jsonEncrypteddcJodexus, FormCreaSolicitudDTO.class);

			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}

//			List<Object[]> listPrivilegios = usuarioServiceLocal.validarUserPerfilPriv(
//					observacionSolicitud.getUsuarioCreacion(), observacionSolicitud.getPerfil(), objInexus.getInit());
//			if (listPrivilegios == null || listPrivilegios.size() == 0) {
//				throw new CadenaInvalidaException();
//			}
//
//			ObservacionSolCompareDTO objCampos = new ObservacionSolCompareDTO();
//			objCampos.setIdSolicitud(objInexus.getIdSolicitud());
//			objCampos.setListaCampos(objInexus.getListaCampos());
//			objCampos.setObservacion(objInexus.getObservacion());
//			objCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
//			objCampos.setPerfil(objInexus.getPerfil());
//
//			objCampos.setInit(objInexus.getInit()); /////////////////////////////////////////////////////////////

//				
//				if(!jsonEncrypteddcJodexus.equals(jsonEncrypteddcInexus))
//				{
//					throw new CadenaInvalidaException();
//				}

//			if (!objInexus.equals(objJodexus) || !objInexus.equals(objCampos) || !objJodexus.equals(objCampos)) {
			if (!objInexus.equals(objJodexus) || !objJodexus.equals(objInexus)) {
				throw new CadenaInvalidaException();
			} else {
				parametros.put("xtu", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			}

//			// verificacion de campos correspondientes por tipo de revisor
//			String perfil = objInexus.getPerfil().trim();
//			List<Long> campos = objInexus.getListaCampos();
//			Long valueok = 0L;
//			boolean isRevisor = false;
//			boolean isRevisorSIG = false;
//			for (int i = 0; i < campos.size(); i++) {
//				if (perfil.equals(String.valueOf(3))) { // revisor
//					System.out.println("ES REVISOR ");
//					valueok = campos.get(i);
//					if (valueok <= 0 || valueok > 11) {
//						isRevisorSIG = true;
//						break;
//					} // throw new CadenaInvalidaException();
//				}
//				if (perfil.equals(String.valueOf(6))) { // revisor sig
//					System.out.println("ES REVISOR SIG");
//					valueok = campos.get(i);
//					if (valueok <= 11 || valueok > 16) {
//						isRevisor = true;
//						break;
//					} // throw new CadenaInvalidaException();
//				}
//			}
//
//			if (isRevisorSIG)
//				throw new CadenaInvalidaException();
//			if (isRevisor)
//				throw new CadenaInvalidaException();

		} catch (CadenaInvalidaException ex) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + ex);

			parametros.put("rare", true);
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} catch (NullPointerException exnl) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + exnl);

			parametros.put("rare", true);
			respuesta = new RespuestaDTO(false,
					"['" + getErrorService().getErrorFor(exnl, ResourceUtil.obtenerLocaleSession()).getDefaultMessage()
							+ "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();

		}

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/inDeshaceChain")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response inDeshaceChain(String jsonEncrypted) {
		String respEncryp = "";
		ObservacionSolicitudDTO observacionSolicitud;
		DeshacerSolCompareDTO objCampos = new DeshacerSolCompareDTO();
//		DeshacerObservacionCompareDTO obsCampos=new DeshacerObservacionCompareDTO();
		DeshacerSolCompareDTO objInexus = null;
		DeshacerSolCompareDTO objJodexus = null;
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			observacionSolicitud = gson.fromJson(objJsonString, ObservacionSolicitudDTO.class);

			try {

				String jsonEncrypteddcInexus = aesUtil.descifrarBase64(observacionSolicitud.getInexus().substring(9));

				objInexus = gson.fromJson(jsonEncrypteddcInexus, DeshacerSolCompareDTO.class);

				String jsonEncrypteddcJodexus = aesUtil.descifrarBase64(observacionSolicitud.getJodexus());

				objJodexus = gson.fromJson(jsonEncrypteddcJodexus, DeshacerSolCompareDTO.class);

			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}

			List<Object[]> listPrivilegios = usuarioServiceLocal.validarUserPerfilPriv(
					observacionSolicitud.getUsuarioCreacion(), observacionSolicitud.getPerfil(), objInexus.getInit());
			if (listPrivilegios == null || listPrivilegios.size() == 0) {
				throw new CadenaInvalidaException();
			}

//			objCampos.setInit(objInexus.getInit()); /////////////////////////////////////////////////////////////

//			if(objInexus.getObservacion()!=null){
			// SECURITY IN DESHACER APROBACION
			objCampos.setIdSolicitud(observacionSolicitud.getIdSolicitud());
			objCampos.setUsuarioCreacion(observacionSolicitud.getUsuarioCreacion());
			objCampos.setPerfil(observacionSolicitud.getPerfil());
			objCampos.setObservacion(observacionSolicitud.getObservacion());

			objCampos.setInit(objInexus.getInit()); /////////////////////////////////////////////////////////////

			if (!objInexus.equals(objJodexus) || !objInexus.equals(objCampos) || !objJodexus.equals(objCampos)) {

				throw new CadenaInvalidaException();
			} else {
				parametros.put("xtu", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			}

//			}
//			else{
//				//SECURITY IN DESHACER OBSERVACION. (CANCELAR)
//				obsCampos.setIdSolicitud(observacionSolicitud.getIdSolicitud());
//				obsCampos.setUsuarioCreacion(observacionSolicitud.getUsuarioCreacion());
//				obsCampos.setPerfil(observacionSolicitud.getPerfil());
//				obsCampos.setIdbtn(observacionSolicitud.getIdbtn());	
//				
//				if(!objInexus.equals(objJodexus)||!objInexus.equals(obsCampos)||!objJodexus.equals(obsCampos))
//				{
//					throw new CadenaInvalidaException();
//				}
//				else{
//					parametros.put("xtu", true);
//					respuesta = new RespuestaDTO(true, "['']", parametros);
//					respEncryp = "salt="+salt+"&iv="+iv+"&jsonEncrypted="+aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
//				}
//				
//			}

		} catch (CadenaInvalidaException ex) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + ex);

			parametros.put("rare", true);
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		}

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/inDeshaceREVChain")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response inDeshaceREVChain(String jsonEncrypted) {
		String respEncryp = "";
		ObservacionSolicitudDTO observacionSolicitud;
		DeshacerObservacionCompareDTO obsCampos = new DeshacerObservacionCompareDTO();
		DeshacerObservacionCompareDTO objInexus = null;
		DeshacerObservacionCompareDTO objJodexus = null;
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			observacionSolicitud = gson.fromJson(objJsonString, ObservacionSolicitudDTO.class);

			try {

				String jsonEncrypteddcInexus = aesUtil.descifrarBase64(observacionSolicitud.getInexus().substring(9));

				objInexus = gson.fromJson(jsonEncrypteddcInexus, DeshacerObservacionCompareDTO.class);

				String jsonEncrypteddcJodexus = aesUtil.descifrarBase64(observacionSolicitud.getJodexus());

				objJodexus = gson.fromJson(jsonEncrypteddcJodexus, DeshacerObservacionCompareDTO.class);

			} catch (StringIndexOutOfBoundsException e) {
				throw new CadenaInvalidaException();
			} catch (NullPointerException e) {
				throw new CadenaInvalidaException();
			}

			List<Object[]> listPrivilegios = usuarioServiceLocal.validarUserPerfilPriv(
					observacionSolicitud.getUsuarioCreacion(), observacionSolicitud.getPerfil(), objInexus.getInit());
			if (listPrivilegios == null || listPrivilegios.size() == 0) {
				throw new CadenaInvalidaException();
			}

			// SECURITY IN DESHACER OBSERVACION. (CANCELAR)
			obsCampos.setIdSolicitud(objInexus.getIdSolicitud());
			obsCampos.setUsuarioCreacion(objInexus.getUsuarioCreacion());
			obsCampos.setPerfil(objInexus.getPerfil());
			obsCampos.setIdbtn(objInexus.getIdbtn());

			obsCampos.setInit(objInexus.getInit()); /////////////////////////////////////////////////////////////

			if (!objInexus.equals(objJodexus) || !objInexus.equals(obsCampos) || !objJodexus.equals(obsCampos)) {
				throw new CadenaInvalidaException();
			} else {
				parametros.put("xtu", true);
				respuesta = new RespuestaDTO(true, "['']", parametros);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			}

		} catch (CadenaInvalidaException ex) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + ex);

			parametros.put("rare", true);
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(ex, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		}

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/observarSolicitud")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response observarSolicitud(String jsonEncrypted) {
		parametros = new ConcurrentHashMap<>();
		List<Long> listaCampos = new ArrayList<Long>();
		SolicitudDTO solicitud = new SolicitudDTO();
		List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos = new ArrayList<RevisionSolicitudCamposDTO>();
		List<MaestroDTO> listaTipoSolValidaRevisor;
		boolean isTipoSolicitudValida = false;
		boolean isTipoSituacionRevisorSolicitudInvalida = false;
		Long idTipoSituacionRevisionSolicitud;
		Long idTipoSituacionRevisionSigSolicitud;

		ObservacionSolicitudDTO observacionSolicitud;
		String respEncryp = "";

		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			observacionSolicitud = gson.fromJson(objJsonString, ObservacionSolicitudDTO.class);

			listaCampos = observacionSolicitud.getListaCampos();

			// verificacion de campos correspondientes por tipo de revisor
			String perfil = observacionSolicitud.getPerfil().trim();
			
			
			//////////////////////////////////////////////
			
			Usuario userValido = usuarioServiceLocal.verificarUsuario(observacionSolicitud.getUsuarioCreacion());
			if (StringUtils.isEmpty(userValido.getCodigoOID())) {
				throw new CadenaInvalidaException();
			}
			
			
			
			///////////////////////////////////////
			List<Long> campos = observacionSolicitud.getListaCampos();
			Long valueok = 0L;
			boolean isRevisor = false;
			boolean isRevisorSIG = false;
			for (int i = 0; i < campos.size(); i++) {
				if (perfil.equals(String.valueOf(3))) { // revisor
					System.out.println("ES REVISOR ");
					valueok = campos.get(i);
					if (valueok <= 0 || valueok > 11) {
						isRevisorSIG = true;
						break;
					} // throw new CadenaInvalidaException();
				}
				if (perfil.equals(String.valueOf(6))) { // revisor sig
					System.out.println("ES REVISOR SIG");
					valueok = campos.get(i);
					if (valueok <= 11 || valueok > 16) {
						isRevisor = true;
						break;
					} // throw new CadenaInvalidaException();
				}
			}

			if (isRevisorSIG)
				throw new Exception();
			if (isRevisor)
				throw new Exception();

			if (Objects.nonNull(observacionSolicitud.getListaCampos())
					&& !observacionSolicitud.getListaCampos().isEmpty()) {
				if (StringUtil.isNotNullOrBlank(observacionSolicitud.getIdSolicitud())
						&& !(observacionSolicitud.getIdSolicitud() == 0)) {
					solicitud = solicitudService.obtenerSolicitudById(observacionSolicitud.getIdSolicitud());
					Long idTipoSolicitud = solicitud.getTipoSolicitud().getIdMaestro();
					idTipoSituacionRevisionSolicitud = Objects
							.nonNull(solicitud.getTipoSituacionRevision().getIdMaestro())
									? solicitud.getTipoSituacionRevision().getIdMaestro()
									: 0L;
					idTipoSituacionRevisionSigSolicitud = Objects
							.nonNull(solicitud.getTipoSituacionRevisionSig().getIdMaestro())
									? solicitud.getTipoSituacionRevisionSig().getIdMaestro()
									: 0L;
					isTipoSolicitudValida = listaTipoSolicitud.stream()
							.anyMatch(p -> p.getIdMaestro().equals(idTipoSolicitud));
					listaTipoSolValidaRevisor = listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.OBSERVADO.getKey())
									|| p.getCodigoItem().equals(TipoSituacionRevisionType.APROBADO.getKey()))
							.collect(Collectors.toList());
					if (StringUtil.isNotNullOrBlank(observacionSolicitud.getPerfil())) {
//						if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {						
						if (observacionSolicitud.getPerfil()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_REGPRO.getValue().toString())) {
							isTipoSituacionRevisorSolicitudInvalida = listaTipoSolValidaRevisor.stream()
									.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSolicitud));
//						} else if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
						} else if (observacionSolicitud.getPerfil()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG_REGPRO.getValue().toString())) {
							isTipoSituacionRevisorSolicitudInvalida = listaTipoSolValidaRevisor.stream()
									.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSigSolicitud));
						} else {
							throw new RevisionCamposSolicitudNoExistePerfilException();
						}
					}

					if (isTipoSolicitudValida) {
						if (!isTipoSituacionRevisorSolicitudInvalida) {
							if (StringUtil.isNotNullOrBlank(observacionSolicitud.getPerfil())) {
//								if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {
								if (observacionSolicitud.getPerfil()
										.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_REGPRO.getValue().toString())) {
									solicitud
											.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
													.filter(p -> p.getCodigoItem()
															.equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
													.findFirst().get().getIdMaestro()));
									if (solicitud.getTipoSolicitud().getIdMaestro().equals(listaTipoSolicitud.stream()
											.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))
											.findFirst().get().getIdMaestro())) {
										solicitud.setTipoSituacionRevisionSig(
												new MaestroDTO(listaTipoSituacionRevision.stream()
														.filter(p -> p.getCodigoItem()
																.equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
														.findFirst().get().getIdMaestro()));
									}
								} else
//									if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
								if (observacionSolicitud.getPerfil().equals(
										TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG_REGPRO.getValue().toString())) {
									solicitud.setTipoSituacionRevisionSig(
											new MaestroDTO(listaTipoSituacionRevision.stream()
													.filter(p -> p.getCodigoItem()
															.equals(TipoSituacionRevisionType.OBSERVADO.getKey()))
													.findFirst().get().getIdMaestro()));
								}
							} else {
								throw new RevisionCamposSolicitudNoExistePerfilException();
							}
							listaRevisionSolicitudCampos = revisionSolicitudCamposService
									.buscarSolicitudCamposxIdSolicitud(observacionSolicitud.getIdSolicitud());
							if (!listaRevisionSolicitudCampos.isEmpty()
									&& Objects.nonNull(listaRevisionSolicitudCampos)) {
								for (RevisionSolicitudCamposDTO revision : listaRevisionSolicitudCampos) {
									if (revision.getPerfilRevisor().equals(observacionSolicitud.getPerfil())) {
										revision.setEstado(EstadoState.INACTIVO.getValue());
										revision.setEstadoEvaluacion(EstadoState.INACTIVO.getValue());
										revision.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
										revision.setUsuarioModificacion(observacionSolicitud.getUsuarioCreacion());
										revisionSolicitudCamposService.actualizar(revision);
									}
								}
							}
						} else {
							throw new SolicitudRevisadaException();
						}
					} else {
						throw new TipoSolicitudNoExisteException();
					}
				} else {
					throw new RevisionCamposIdSolicitudNoExisteException();
				}
				for (Long idCampo : listaCampos) {
					revisionSolicitudCamposService.registrar(new RevisionSolicitudCamposDTO(
							observacionSolicitud.getIdSolicitud(), idCampo, EstadoState.ACTIVO.getValue(),
							FechaUtil.obtenerFechaActual(), observacionSolicitud.getUsuarioCreacion(),
							EstadoState.ACTIVO.getValue(), observacionSolicitud.getPerfil()));
				}

				respuesta = solicitudService.gestionAccionSolicitud(
						listaTipoSituacionSolicitud.stream()
								.filter(p -> p.getCodigoItem()
										.equals(TipoSituacionSolicitudType.REVISION.getKey().toString()))
								.findFirst().get().getIdMaestro().toString(),
						solicitud.getCodSolicitud(), observacionSolicitud, solicitud.getCodigoModular(), solicitud,
						null);
				respuesta = solicitudService.procesarRevision(solicitud);
				respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
						+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
				if (Objects.isNull(respuesta)) {
					respuesta = new RespuestaDTO(true, "['']", parametros);
					respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
							+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
				}
			} else {
				throw new RevisionCamposSolicitudNoExisteException();
			}
		} catch (CodigoSolicitudRepetidoException | SolicitudNoRegistradaException | ConversorException
				| AccionSolicitudNoRegistradoException | SolicitudNoActualizadaException | SolicitudRevisadaException
				| TipoSolicitudNoExisteException | RevisionCamposSolicitudNoExisteException er) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : ");

			// parametros.clear();
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(er, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);

			// parametros.clear();
			parametros.put("ExceptionTipo", e.toString());
			parametros.put("Exception", e.getMessage());
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();

		} finally {
			parametros.clear();// imendoza 20170104
		}
		// return
		// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();

	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/desHacerObservarSolicitud")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response desHacerObservarSolicitud(String jsonEncrypted) {
		ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();
		aesUtil = new AesUtil(keySize, iterationCount);

		parametros = new ConcurrentHashMap<>();
		SolicitudDTO solicitud = new SolicitudDTO();
		List<AccionSolicitudDTO> listaAccionSolicitud = new ArrayList<AccionSolicitudDTO>();
		AccionSolicitudDTO accionSolicUltimo;
		AccionSolicitudDTO accionSolicUltimo2;
		AccionSolicitudDTO accionSolicPrimero = new AccionSolicitudDTO();
		List<RevisionSolicitudCamposDTO> listaRevisionSolicitudCampos = new ArrayList<RevisionSolicitudCamposDTO>();
		Long idTipoSitSol = 0L;
		List<MaestroDTO> listaTipoSolValidaRevisor;
		boolean isTipoSituacionRevisorSolicitudInicio = false;
		String respEncryp = "";
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			observacionSolicitud = gson.fromJson(objJsonString, ObservacionSolicitudDTO.class);

			parametros.put("estado", "1");
			parametros.put("idSolicitud", observacionSolicitud.getIdSolicitud());
			listaAccionSolicitud = accionSolicitudService.listarFirstMaxResultParametros(100, 0, parametros);
			parametros.clear();

			// jlbedrillana inicio
			solicitud = solicitudService.obtenerSolicitudById(observacionSolicitud.getIdSolicitud());

			if (StringUtil.isNotNullOrBlank(observacionSolicitud.getIdbtn())
					&& observacionSolicitud.getIdbtn().equals("2")) {// El valor 2 indica que el evento se genero a
																		// partir del boton cancelar

				if (Objects.nonNull(solicitud)) {
					listaTipoSolValidaRevisor = listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.INICIO.getKey()))
							.collect(Collectors.toList());
					Long idTipoSituacionRevisionSolicitud = solicitud.getTipoSituacionRevision().getIdMaestro();
					Long idTipoSituacionRevisionSigSolicitud = solicitud.getTipoSituacionRevisionSig().getIdMaestro();
					if (StringUtil.isNotNullOrBlank(observacionSolicitud.getPerfil())) {
//						if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {						
						if (observacionSolicitud.getPerfil()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_REGPRO.getValue().toString())) {
							isTipoSituacionRevisorSolicitudInicio = listaTipoSolValidaRevisor.stream()
									.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSolicitud));
//						} else if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
						} else if (observacionSolicitud.getPerfil()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG_REGPRO.getValue().toString())) {
							isTipoSituacionRevisorSolicitudInicio = listaTipoSolValidaRevisor.stream()
									.anyMatch(p -> p.getIdMaestro().equals(idTipoSituacionRevisionSigSolicitud));
						}
						/*
						 * else { throw new RevisionCamposSolicitudNoExistePerfilException(); }
						 */
					}
				}

				if (!isTipoSituacionRevisorSolicitudInicio) {
					throw new AccionSolicitudNoRegistradoException();
				}

			}

			// jlbedrillana fin

			if (!listaAccionSolicitud.isEmpty() && Objects.nonNull(listaAccionSolicitud)) {
				accionSolicUltimo = listaAccionSolicitud.get(listaAccionSolicitud.size() - 1);// Cogemos el ultimo
																								// registro de la accion
				// Con la funcion sorted() realizamos un reordenamiento de la lista, asi podemos
				// recorrer del ultimo registro al primero.
				accionSolicPrimero = listaAccionSolicitud.stream()
						.sorted(Comparator.comparingLong(AccionSolicitudDTO::getIdAccionSolicitud).reversed())
						.filter(p -> !p.getTipoSituacionSolicitud().getIdMaestro()
								.equals(accionSolicUltimo.getTipoSituacionSolicitud().getIdMaestro()))
						.findFirst().get();
				// Debemos verificar que el estado a reponer no sea OBSERVADO.
				if (accionSolicPrimero.getTipoSituacionSolicitud().getIdMaestro()
						.equals(listaTipoSituacionSolicitud.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey()))
								.findFirst().get().getIdMaestro())) {
					// Buscamos otro estado distinto a OBSERVADO y al ultimo estado de la solicitud.
					// Con la funcion sorted() realizamos un reordenamiento de la lista, asi podemos
					// recorrer del ultimo registro al primero.
					accionSolicPrimero = listaAccionSolicitud.stream()
							.sorted(Comparator.comparingLong(AccionSolicitudDTO::getIdAccionSolicitud).reversed())
							.filter(p -> (!p.getTipoSituacionSolicitud().getIdMaestro()
									.equals(listaTipoSituacionSolicitud.stream()
											.filter(pa -> pa.getCodigoItem()
													.equals(TipoSituacionSolicitudType.OBSERVADO.getKey()))
											.findFirst().get().getIdMaestro())
									&& (!p.getTipoSituacionSolicitud().getIdMaestro()
											.equals(accionSolicUltimo.getTipoSituacionSolicitud().getIdMaestro()))))
							.findFirst().get();

				}
			}

			// solicitud =
			// solicitudService.obtenerSolicitudById(observacionSolicitud.getIdSolicitud());
			if (Objects.nonNull(solicitud)) {
				solicitud.setUsuarioModificacion(observacionSolicitud.getUsuarioCreacion());
//				if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {
				if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_REGPRO.getValue().toString())) {
					solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
							.findFirst().get().getIdMaestro()));
					if (solicitud.getTipoSolicitud().getIdMaestro()
							.equals(listaTipoSolicitud.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))
									.findFirst().get().getIdMaestro())) {
						solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
								.filter(p -> p.getCodigoItem()
										.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
								.findFirst().get().getIdMaestro()));
						solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
								.filter(p -> p.getCodigoItem()
										.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
								.findFirst().get().getIdMaestro()));
						solicitud.setUsuarioRevision("");
						solicitud.setNombreUsuarioRevision("");
						solicitud.setUsuarioRevisionSig("");
						solicitud.setNombreUsuarioRevisionSig("");

					} else {
						solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
								.filter(p -> p.getCodigoItem()
										.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
								.findFirst().get().getIdMaestro()));
						solicitud.setUsuarioRevision("");
						solicitud.setNombreUsuarioRevision("");
					}
				} else
//					if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
				if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG_REGPRO.getValue().toString())) {
					solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
							.findFirst().get().getIdMaestro()));
					solicitud.setUsuarioRevisionSig("");
					solicitud.setNombreUsuarioRevisionSig("");
				}
				if (solicitud.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
						.findFirst().get().getIdMaestro())
						&& solicitud.getTipoSituacionRevisionSig().getIdMaestro()
								.equals(listaTipoSituacionRevision.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
										.findFirst().get().getIdMaestro())) {
					accionSolicUltimo2 = listaAccionSolicitud.get(listaAccionSolicitud.size() - 1);// Cogemos el ultimo
																									// registro de la
																									// accion
					if (solicitud.getTipoSolicitud().getIdMaestro()
							.equals(listaTipoSolicitud.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))
									.findFirst().get().getIdMaestro())) {
						// Debemos verificar que el estado a reponer no sea OBSERVADO.
						if (accionSolicUltimo2.getTipoSituacionSolicitud().getIdMaestro()
								.equals(listaTipoSituacionSolicitud.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionSolicitudType.OBSERVADO.getKey()))
										.findFirst().get().getIdMaestro())) {
							// Buscamos otro estado distinto a OBSERVADO y al ultimo estado de la solicitud.
							// Con la funcion sorted() realizamos un reordenamiento de la lista, asi podemos
							// recorrer del ultimo registro al primero.
							Long idSitu = accionSolicPrimero.getTipoSituacionSolicitud().getIdMaestro();
							accionSolicPrimero = listaAccionSolicitud.stream()
									.sorted(Comparator.comparingLong(AccionSolicitudDTO::getIdAccionSolicitud)
											.reversed())
									.filter(p -> (!p.getTipoSituacionSolicitud().getIdMaestro()
											.equals(listaTipoSituacionSolicitud.stream()
													.filter(pa -> pa.getCodigoItem()
															.equals(TipoSituacionSolicitudType.OBSERVADO.getKey()))
													.findFirst().get().getIdMaestro())
											&& (!p.getTipoSituacionSolicitud().getIdMaestro().equals(idSitu))))
									.findFirst().get();
							idTipoSitSol = accionSolicPrimero.getTipoSituacionSolicitud().getIdMaestro();
						} else {
							idTipoSitSol = accionSolicPrimero.getTipoSituacionSolicitud().getIdMaestro();
						}
					} else {
						// Verificamos si el ultimo estado de la solicitud es SUSTENTADO
						if (accionSolicUltimo2.getTipoSituacionSolicitud().getIdMaestro()
								.equals(listaTipoSituacionSolicitud.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionSolicitudType.SUSTENTADO.getKey()))
										.findFirst().get().getIdMaestro())) {
							idTipoSitSol = accionSolicUltimo2.getTipoSituacionSolicitud().getIdMaestro();
						} else {
							idTipoSitSol = accionSolicPrimero.getTipoSituacionSolicitud().getIdMaestro();
						}
					}
					respuesta = solicitudService.gestionAccionSolicitud(idTipoSitSol.toString(),
							solicitud.getCodSolicitud(), observacionSolicitud, solicitud.getCodigoModular(), solicitud,
							null);
					respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
							+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
				}
				if (!solicitud.getTipoSituacionRevision().getIdMaestro().equals(listaTipoSituacionRevision.stream()
						.filter(p -> p.getCodigoItem().equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
						.findFirst().get().getIdMaestro())
						|| !solicitud.getTipoSituacionRevisionSig().getIdMaestro()
								.equals(listaTipoSituacionRevision.stream()
										.filter(p -> p.getCodigoItem()
												.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
										.findFirst().get().getIdMaestro())) {
					respuesta = solicitudService
							.gestionAccionSolicitud(
									listaTipoSituacionSolicitud.stream()
											.filter(p -> p.getCodigoItem()
													.equals(TipoSituacionSolicitudType.REVISION.getKey()))
											.findFirst().get().getIdMaestro().toString(),
									solicitud.getCodSolicitud(), observacionSolicitud, solicitud.getCodigoModular(),
									solicitud, null);

					respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
							+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
				}
				listaRevisionSolicitudCampos = revisionSolicitudCamposService
						.buscarSolicitudCamposxIdSolicitud(solicitud.getIdSolicitud());
				if (!listaRevisionSolicitudCampos.isEmpty() && Objects.nonNull(listaRevisionSolicitudCampos)) {
					for (RevisionSolicitudCamposDTO revision : listaRevisionSolicitudCampos) {
						if (revision.getPerfilRevisor().equals(observacionSolicitud.getPerfil())) {
							revision.setEstado(EstadoState.INACTIVO.getValue());
							revision.setEstadoEvaluacion(EstadoState.INACTIVO.getValue());
							revision.setFechaUltimaModificacion(FechaUtil.obtenerFechaActual());
							revision.setUsuarioModificacion(observacionSolicitud.getUsuarioCreacion());
							revisionSolicitudCamposService.actualizar(revision);
						}
					}
				}
			} else {
				throw new SolicitudNoExisteException();
			}

		} catch (SolicitudNoExisteException | SolicitudNoActualizadaException | CodigoSolicitudRepetidoException
				| SolicitudNoRegistradaException | ConversorException | AccionSolicitudNoRegistradoException er) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + er);

			parametros.clear();
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(er, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();

		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);

			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		// return
		// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/desHacerAprobacionSolicitud")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response desHacerAprobacionSolicitud(String jsonEncrypted) {
		ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();
		aesUtil = new AesUtil(keySize, iterationCount);
		// final ObservacionSolicitudDTO observacionSolicitud
		parametros = new ConcurrentHashMap<>();
		SolicitudDTO solicitud = new SolicitudDTO();
		AuditoriaProgramaDTO auditoriaPrograma = new AuditoriaProgramaDTO();
		ProgramaDTO pro = new ProgramaDTO();
		String codigoModular;
		String respEncryp = "";
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			observacionSolicitud = gson.fromJson(objJsonString, ObservacionSolicitudDTO.class);

			solicitud = solicitudService.obtenerSolicitudById(observacionSolicitud.getIdSolicitud());
			if (Objects.nonNull(solicitud)) {
				solicitud.setIndicadorUltimo(EstadoState.INACTIVO.getValue());
				codigoModular = solicitud.getCodigoModular();
				if (solicitud.getTipoSolicitud().getIdMaestro()
						.equals(listaTipoSolicitud.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey())).findFirst()
								.get().getIdMaestro())) {
					solicitud.setCodigoModular("");////////////////////////////////////////////////////////////////////////////////////
				}
//				if (observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_TODOS.getValue().toString()) 
				if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_TODOS_REGPRO.getValue().toString())
						|| solicitud.getTipoSolicitud().getIdMaestro()
								.equals(listaTipoSolicitud.stream()
										.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))
										.findFirst().get().getIdMaestro())) {
					solicitud.setUsuarioModificacion(observacionSolicitud.getUsuarioCreacion());
					solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
							.findFirst().get().getIdMaestro()));
					solicitud.setUsuarioRevision("");
					solicitud.setNombreUsuarioRevision("");
					solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
							.findFirst().get().getIdMaestro()));
					solicitud.setUsuarioRevisionSig("");
					solicitud.setNombreUsuarioRevisionSig("");
				} else if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())) {
					solicitud.setUsuarioModificacion(observacionSolicitud.getUsuarioCreacion());
					solicitud.setTipoSituacionRevision(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
							.findFirst().get().getIdMaestro()));
					solicitud.setUsuarioRevision("");
					solicitud.setNombreUsuarioRevision("");
				} else if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
					solicitud.setTipoSituacionRevisionSig(new MaestroDTO(listaTipoSituacionRevision.stream()
							.filter(p -> p.getCodigoItem()
									.equals(TipoSituacionRevisionType.PENDIENTE_DE_REVISION.getKey()))
							.findFirst().get().getIdMaestro()));
					solicitud.setUsuarioRevisionSig("");
					solicitud.setNombreUsuarioRevisionSig("");
				} // PERFIL_REVISOR_TODOS_REGPRO
//				if(observacionSolicitud.getPerfil().equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_TODOS.getValue().toString()) 
				if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_TODOS_REGPRO.getValue().toString())
						|| solicitud.getTipoSolicitud().getIdMaestro()
								.equals(listaTipoSolicitud.stream()
										.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CIERRE.getKey()))
										.findFirst().get().getIdMaestro())) {
					observacionSolicitud.setPerfil(null);// No guardar el perfil
					respuesta = solicitudService.gestionAccionSolicitud(
							listaTipoSituacionSolicitud.stream()
									.filter(p -> p.getCodigoItem()
											.equals(TipoSituacionSolicitudType.PENDIENTE.getKey()))
									.findFirst().get().getIdMaestro().toString(),
							solicitud.getCodSolicitud(), observacionSolicitud, codigoModular, solicitud, null);
					respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
							+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));

				} else if (observacionSolicitud.getPerfil()
						.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_PADRON.getValue().toString())
						|| observacionSolicitud.getPerfil()
								.equals(TipoAgrupadorSeguridad.PERFIL_REVISOR_SIG.getValue().toString())) {
					observacionSolicitud.setPerfil(null);// No guardar el perfil
					respuesta = solicitudService
							.gestionAccionSolicitud(
									listaTipoSituacionSolicitud.stream()
											.filter(p -> p.getCodigoItem()
													.equals(TipoSituacionSolicitudType.REVISION.getKey()))
											.findFirst().get().getIdMaestro().toString(),
									solicitud.getCodSolicitud(), observacionSolicitud, codigoModular, solicitud, null);

					respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
							+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));

				}
				pro = programaService.buscarProgramaxCodigoModular(codigoModular);/////////////////////////////
				if (Objects.nonNull(pro)) {
					if (solicitud.getTipoSolicitud().getIdMaestro()
							.equals(listaTipoSolicitud.stream()
									.filter(p -> p.getCodigoItem().equals(TipoSolicitudType.CREACION.getKey()))
									.findFirst().get().getIdMaestro())) { ///////////////////////////
						pro.setTipoEstado(new MaestroDTO(listaTipoEstadoPrograma.stream()
								.filter(p -> p.getCodigoItem().equals(TipoEstadoProgramaType.INACTIVO.getKey()))
								.findFirst().get().getIdMaestro()));
						pro.setEstado(EstadoProgramaState.INACTIVO.getValue()); ///////////////////////////////////////////
						pro.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.DESHECHO.getKey()))
								.findFirst().get().getIdMaestro()));
					} else {
						auditoriaPrograma = auditoriaProgramaService.buscarProgramaxCodigoModular(codigoModular);
						Long idPrograma = pro.getIdPrograma();
						ConversorHelper.fusionaPropiedades(auditoriaPrograma, pro);
						pro.setIdPrograma(idPrograma);
						// Inicio Situacion Programa original a retormar
						pro.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.ENREVISION.getKey()))
								.findFirst().get().getIdMaestro()));

						// Fin Situacion Programa a retormar
						// Inicio Estado
						pro.setTipoEstado(new MaestroDTO(listaTipoEstadoPrograma.stream()
								.filter(p -> p.getCodigoItem().equals(TipoEstadoProgramaType.ACTIVO.getKey()))
								.findFirst().get().getIdMaestro()));
						pro.setEstado(EstadoProgramaState.ACTIVO.getValue());
						// Fin estado
						auditoriaPrograma.setTipoEstado(new MaestroDTO(listaTipoEstadoPrograma.stream()
								.filter(p -> p.getCodigoItem().equals(TipoEstadoProgramaType.INACTIVO.getKey()))
								.findFirst().get().getIdMaestro()));
						auditoriaPrograma.setTipoSituacion(new MaestroDTO(listaTipoSituacionPrograma.stream()
								.filter(p -> p.getCodigoItem().equals(TipoSituacionProgramaType.DESHECHO.getKey()))
								.findFirst().get().getIdMaestro()));
						auditoriaPrograma.setEstado(EstadoProgramaState.INACTIVO.getValue());
						auditoriaPrograma = auditoriaProgramaService.actualizar(auditoriaPrograma);
					}
					pro = programaService.actualizar(pro); ///////////////////////////////////////
				} else {
					throw new ProgramaNoExisteException();
				}
			} else {
				throw new SolicitudNoExisteException();
			}
		} catch (SolicitudNoExisteException | SolicitudNoActualizadaException | CodigoSolicitudRepetidoException
				| ProgramaNoExisteException | ConversorException | AccionSolicitudNoRegistradoException
				| ProgramaNoActualizadoException | AuditoriaProgramaNoExisteException
				| AuditoriaProgramaNoActualizadoException er) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + er);

			parametros.clear();
			respuesta = new RespuestaDTO(false, "['"
					+ getErrorService().getErrorFor(er, ResourceUtil.obtenerLocaleSession()).getDefaultMessage() + "']",
					parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} catch (Exception e) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + e);

			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			// return
			// Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));
			return Response.status(Status.CONFLICT).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString())
					.build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		// return
		// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
	}

	// Se agrega por validacion en la carga de solicitudes werr 31/10/2017

	@SuppressWarnings({ "rawtypes" })
	@POST
	@Path("/validarProgramaCarga/{tipoSolicitud}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validarProgramaCarga(@PathParam("tipoSolicitud") String tipoSolicitud,
			@QueryParam("codigoModular") String codigoModular, @QueryParam("nombrePrograma") String nombrePrograma) {
		List<ProgramaDTO> listPrograma = new ArrayList<ProgramaDTO>();
		List<SolicitudDTO> listSolicitud = new ArrayList<SolicitudDTO>();
		List<ParametroDTO> listParametro = new ArrayList<ParametroDTO>();
		parametros = new ConcurrentHashMap<>();

		try {
			parametros.put("estado", "1");
			if (StringUtil.isNotNullOrBlank(codigoModular)) {
				parametros.put("codigoModular", codigoModular);
			} else {
				parametros.put("codigoModular", "");
			}
			if (StringUtil.isNotNullOrBlank(nombrePrograma)) {
				parametros.put("nombrePrograma", StringUtil.upperCaseObject(nombrePrograma).toString());
			} else {
				parametros.put("nombrePrograma", "");
			}
			listSolicitud = solicitudService.buscarSolicitudCarga(parametros.get("nombrePrograma").toString(),
					parametros.get("codigoModular").toString(), tipoSolicitud);

			if (tipoSolicitud.equals(TipoSolicitudType.CARGA.getKey())) {

				parametros.keySet().removeIf(e -> (e == "codooii" || e == "nombrePrograma"));
				if (StringUtil.isNotNullOrBlank(codigoModular)) {
					parametros.put("codigoModular", codigoModular);
					listPrograma = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);
				} else {
					listPrograma = null;
				}
				parametros.keySet().removeIf(e -> (e == "codooii" || e == "codigoModular"));
				parametros.put("codigoParametro", "FECFINPERMOD");
//					listParametro = parametroService.listarParametroFirstMaxResultParametros(5, 0, parametros);
//					int estaFueraDePeriodo = 0;
//					if (!listParametro.isEmpty()) {
//						for (ParametroDTO parametro : listParametro) {
//							estaFueraDePeriodo = FechaUtil.restarFechas(FechaUtil.obtenerFechaActual(),
//									parametro.getValorFecha());
//							if (estaFueraDePeriodo <= -1) {
//								parametros.put("fueraPeriodoModificacion", true);
//								break;
//							} else {
//								parametros.put("fueraPeriodoModificacion", false);
//							}
//						}
//					}

				if (listPrograma != null) {
					if (!listPrograma.isEmpty()) {
						for (ProgramaDTO programa : listPrograma) {
							if (StringUtil.isNotNullOrBlank(programa.getTipoSituacion().getIdMaestro())) {
								parametros.put("puedeModificar",
										(programa.getTipoSituacion().getIdMaestro()
												.equals(listaTipoSituacionPrograma.stream()
														.filter(p -> p.getCodigoItem()
																.equals(TipoSituacionProgramaType.CERRADO.getKey()))
														.findFirst().get().getIdMaestro()))
												|| programa.getTipoSituacion().getIdMaestro()
														.equals(listaTipoSituacionPrograma.stream()
																.filter(p -> p.getCodigoItem().equals(
																		TipoSituacionProgramaType.DESHECHO.getKey()))
																.findFirst().get().getIdMaestro()) ? false : true);
								break;
							}
						}
					}
				}

			}
			parametros.put("existeSolicitud", !listSolicitud.isEmpty());
			if (listPrograma != null)
				parametros.put("existePrograma", !listPrograma.isEmpty());
			List<String> eliminar = Arrays.asList("codooii", "nombrePrograma", "estado", "codigoParametro",
					"codigoModular");
			parametros.keySet().removeIf(e -> (eliminar.contains(e)));
			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			parametros.keySet().removeIf(d -> (d == "codooii" || d == "nombrePrograma" || d == "estado"
					|| d == "codigoParametro" || d == "codigoModular"));
			parametros.put("existeSolicitud", false);
			parametros.put("existePrograma", false);
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']",
					new ConcurrentHashMap<>(parametros));
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerPrograma/{codModular}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerPrograxCodModular(@PathParam("codModular") String codModular) {
		ProgramaDTO prBean = new ProgramaDTO();
		List<ProgramaDTO> listprBean;
		parametros = new ConcurrentHashMap<>();
		try {

			parametros.put("codigoModular", codModular.trim());
			listprBean = programaService.listarProgramasFirstMaxResultParametros(5, 0, parametros);

			// prBean=programaService.buscarProgramaxCodigoModular(codModular.trim());
//			  if(Objects.nonNull(prBean))
			if (Objects.nonNull(listprBean)) {
				prBean = listprBean.get(0);
				parametros.put("existePrograma", true);
				parametros.put("dataPrograma", prBean);

				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));

				// return
				// Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
			} else {
				parametros.put("existePrograma", false);
				respuesta = new RespuestaDTO(true, "['No hay resultados']", prBean);
			}
			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
		} catch (Exception e) {
			parametros.put("existePrograma", false);
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']",
					new ConcurrentHashMap<>(parametros));
		} finally {
			parametros.clear();
		}

		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	// Se agrega por el reinicio de programas werr 14/11/2017

//	@SuppressWarnings("rawtypes")
//	@POST
//	@Path("/reiniciarPrograma")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response reiniciarPrograma(String jsonEncrypted) {
//		TipoPeriodoFilter consultaJson=new TipoPeriodoFilter();
//		aesUtil = new AesUtil(keySize, iterationCount);		
//		parametros = new ConcurrentHashMap<>();
//	//	String namPeriodo, fechPeriodo,nombreUser;
//		CorteDTO corte=new CorteDTO();
//		try { 
//			
//			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
//			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
//			iv = QueryParamURL.getParam(jsonEncrypted, "iv");
//			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
//			consultaJson = gson.fromJson(objJsonString, TipoPeriodoFilter.class);
//			//buscar los programas que no esten cerrados o deshecho
//			Integer cantReinicia = null;
//			
//			parametros.put("estado", EstadoState.ACTIVO.getValue());
//			MaestroDTO maestroSituacion=new MaestroDTO(listaTipoSituacionPrograma.stream().filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.PORACTUALIZAR.getKey()))
//							.findFirst().get().getIdMaestro());
//			
//			MaestroDTO maestroEstado=new MaestroDTO(listaTipoEstadoPrograma.stream()
//					  .filter(p-> p.getCodigoItem().equals(TipoEstadoProgramaType.NORENOVADO.getKey()))
//					  .findFirst()
//					  .get()
//					  .getIdMaestro());
//			
//			MaestroDTO maestroPeriodo=new MaestroDTO(listaTipoPeriodo.stream()
//					  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2017.getKey()))
//					  .findFirst()
//					  .get()
//					  .getIdMaestro());
//			
//			
//				 // Tipo de solicitudes a restaura
//				List<Long> listSituaciones = new ArrayList<Long>();
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString())).findFirst().get().getIdMaestro());
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISION.getKey().toString())).findFirst().get().getIdMaestro());
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey().toString())).findFirst().get().getIdMaestro());
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.SUSTENTADO.getKey().toString())).findFirst().get().getIdMaestro());
//				 
//				 //Reiniciar los programas
//				
//				//Seteo para el registro del corte
//				corte.setIdCorte(null);
//				corte.setNamePeriodo(consultaJson.getNamePeriodo());
//				corte.setFechaInicioPeriodo(FechaUtil.obtenerFechaFormatoPersonalizado(consultaJson.getFecIni(), "dd/MM/yyyy"));
//				corte.setFechaFinPeriodo(FechaUtil.obtenerFechaFormatoPersonalizado(consultaJson.getFecFin(), "dd/MM/yyyy"));
//				corte.setUserCrea(consultaJson.getNameUser());
//				corte.setFecCrea(FechaUtil.obtenerFechaActual());
//				corte.setEstado(EstadoState.ACTIVO.getValue().toString());
//				
//				cantReinicia=programaService.actualizarSituacPrograma(parametros,maestroSituacion,maestroEstado,maestroPeriodo,listSituaciones,listaTipoSituacionSolicitud.stream()
//						  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISIONPROCESOANT.getKey().toString()))
//						  .findFirst()
//						  .get()
//						  .getIdMaestro(),corte);
//				
//				 
////				 	if (StringUtil.isNotNullOrBlank(namePeriodo)) {
////						namPeriodo=namePeriodo;
////					} else {
////						namPeriodo="";
////					}
////					if (StringUtil.isNotNullOrBlank(fechaIniPeriodo)) {
////						fechPeriodo= StringUtil.upperCaseObject(fechaIniPeriodo).toString();
////					} else {
////						fechPeriodo="";
////					}
////					if (StringUtil.isNotNullOrBlank(nombreUsuario)) {
////						nombreUser= StringUtil.upperCaseObject(nombreUsuario).toString();
////					} else {
////						nombreUser="";
////					}
//		//Registrar el corte
//					
//						
//			   
//			parametros.put("numeroReinicios", cantReinicia);
//			
//			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
//
//		} catch (Exception e) {
//			if (log.isHabilitadoError()) {
//				log.error(e);
//			}
//			parametros.keySet().removeIf(d -> (d == "fechaPeriodo" || d == "namePeriodo"));
//			parametros.put("existeSolicitud", false);
//			parametros.put("existePrograma", false);
//			respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", new ConcurrentHashMap<>(parametros));
//			//return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
//		}finally {
////			listPrograma.clear();
////			parametros.clear();
//		}
//		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
//	}

//	@SuppressWarnings("rawtypes")
//	@POST
//	@Path("/reiniciarProgramaInt")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response reiniciarProgramaInt() {	
//		parametros = new ConcurrentHashMap<>();
//	//	String namPeriodo, fechPeriodo,nombreUser;
//		try { 
//			
//			//buscar los programas que no esten cerrados o deshecho
//			Integer cantReinicia = null;
//			
//			parametros.put("estado", EstadoState.ACTIVO.getValue());
//			MaestroDTO maestroSituacion=new MaestroDTO(listaTipoSituacionPrograma.stream().filter(p-> p.getCodigoItem().equals(TipoSituacionProgramaType.PORACTUALIZAR.getKey()))
//							.findFirst().get().getIdMaestro());
//			
//			MaestroDTO maestroEstado=new MaestroDTO(listaTipoEstadoPrograma.stream()
//					  .filter(p-> p.getCodigoItem().equals(TipoEstadoProgramaType.NORENOVADO.getKey()))
//					  .findFirst()
//					  .get()
//					  .getIdMaestro());
//			
//			MaestroDTO maestroPeriodo=new MaestroDTO(listaTipoPeriodo.stream()
//					  .filter(p-> p.getCodigoItem().equals(TipoPeriodoType.PERIODO2017.getKey()))
//					  .findFirst()
//					  .get()
//					  .getIdMaestro());
//			
//			
//				 // Tipo de solicitudes a restaura
//				List<Long> listSituaciones = new ArrayList<Long>();
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.PENDIENTE.getKey().toString())).findFirst().get().getIdMaestro());
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISION.getKey().toString())).findFirst().get().getIdMaestro());
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.OBSERVADO.getKey().toString())).findFirst().get().getIdMaestro());
//				listSituaciones.add(listaTipoSituacionSolicitud.stream().filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.SUSTENTADO.getKey().toString())).findFirst().get().getIdMaestro());
//				 
//				 //Reiniciar los programas
//								
//				cantReinicia=programaService.actualizarSituacPrograma(parametros,maestroSituacion,maestroEstado,maestroPeriodo,listSituaciones,listaTipoSituacionSolicitud.stream()
//						  .filter(r-> r.getCodigoItem().equals(TipoSituacionSolicitudType.REVISIONPROCESOANT.getKey().toString()))
//						  .findFirst()
//						  .get()
//						  .getIdMaestro());
//				
//			parametros.put("numeroReinicios", cantReinicia);
//			
//			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
//
//		} catch (Exception e) {
//			if (log.isHabilitadoError()) {
//				log.error(e);
//			}
//			parametros.keySet().removeIf(d -> (d == "fechaPeriodo" || d == "namePeriodo"));
//			parametros.put("existeSolicitud", false);
//			parametros.put("existePrograma", false);
//			respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", new ConcurrentHashMap<>(parametros));
//			//return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
//		}finally {
//
//		}
//		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
//	}

//	@SuppressWarnings("rawtypes")
//	@POST
//	@Path("/verificaSituacionSolicitud/{codSolicitud}") 
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response verificaSituacionSolicitud(@PathParam("codSolicitud") String codSolicitud , @QueryParam("tipo") String tipo) {
//		parametros = new ConcurrentHashMap<>();
//		ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();
//		try {
	@SuppressWarnings("rawtypes")
	@POST
	@Path("/verificaSituacionSolicitud")
	@Produces(MediaType.APPLICATION_JSON)
	public Response verificaSituacionSolicitud(String jsonEncrypted) {
		TipoSolicitudFilter consultaJson = new TipoSolicitudFilter();
		aesUtil = new AesUtil(keySize, iterationCount);
		parametros = new ConcurrentHashMap<>();
		ObservacionSolicitudDTO observacionSolicitud = new ObservacionSolicitudDTO();

		try {
			// INICIO desencriptar
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			consultaJson = gson.fromJson(objJsonString, TipoSolicitudFilter.class);

			// SUSTENTAR SOLICITUD
			SolicitudDTO indicadordto = solicitudService.verificaSituacSolicitud(consultaJson.getCodSolicitud(),
					consultaJson.getTipoAccion());
			parametros.put("procede", indicadordto.getCorrecto());
			respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/enabledcl")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response enabledcl(String jsonEncrypted) {
		parametros = new ConcurrentHashMap<>();
		TipoSolicitudFilter consultaJson = new TipoSolicitudFilter();
		String respEncryp;
		try {
			// if(!checkSIG){
			// INICIO desencriptar
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			consultaJson = gson.fromJson(objJsonString, TipoSolicitudFilter.class);

			SolicitudDTO indicadordto = solicitudService.verificaCierreDeshacerAprobacionSolicitud(
					consultaJson.getCodSolicitud(), consultaJson.getTipoAccion(), consultaJson.getTipoSolicitud());

			parametros.put("procede", indicadordto.getCorrecto());
//				parametros.put("noprocede", consultaJson.getIsIntruso());

			respuesta = new RespuestaDTO(false, "", parametros);
			respEncryp = "salt=" + salt + "&iv=" + iv + "&jsonEncrypted="
					+ aesUtil.encrypt(salt, iv, passphrase, gson.toJson(respuesta));

//				parametros.put("procede", respEncryp);

			// }else{
			// respuesta = new RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']",
			// parametros);
			// }

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			parametros.clear();
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();
		}

		return Response.status(Status.OK).entity(gson.toJson(aesUtil.cifrarBase64(respEncryp)).toString()).build();
//		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();

	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerPeriodo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerPeriodo(String jsonEncrypted) {
		parametros = new ConcurrentHashMap<>();
		List<PeriodoBandejaDTO> listaTem = new ArrayList<PeriodoBandejaDTO>();
		List<PeriodoBandejaDTO> lista = new ArrayList<PeriodoBandejaDTO>();
		ConsultaJson consultaJson = new ConsultaJson();
		aesUtil = new AesUtil(keySize, iterationCount);
		try {
			// INICIO desencriptar
			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);
			consultaJson = gson.fromJson(objJsonString, ConsultaJson.class);
			// FIN desencriptar
			String valor = StringUtil.isNotNullOrBlank(consultaJson.getGroupFilter())
					? consultaJson.getGroupFilter().toString()
					: "";
			listaTem = periodoService.grillaPeriodo(consultaJson.getIndex(), consultaJson.getBatchSize(), valor,
					consultaJson.getOrder());
			for (PeriodoBandejaDTO pdto : listaTem) {
				if (pdto.getEstado().equals("0")) {
					pdto.setEstado("INACTIVO");
				}
				if (pdto.getEstado().equals("1")) {
					pdto.setEstado("ACTIVO");
				}
				lista.add(pdto);
			}

			if (!lista.isEmpty()) {
				parametros.put("salt", salt);
				parametros.put("iv", iv);
				parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(lista)));
				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
			} else if (lista.isEmpty()) {
				respuesta = new RespuestaDTO(true, "['No hay resultados']", lista);
			}
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			respuesta = new RespuestaDTO(false, "['" + ERROR_SERVICIO_GENERAL + "']", lista);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		} finally {
			parametros.clear();// imendoza 20170104
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/obtenerPeriodoTope")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response obtenerPeriodoTope(String jsonEncrypted) {

		aesUtil = new AesUtil(keySize, iterationCount);
		parametros = new ConcurrentHashMap<>();
		Boolean sucess;
		String paramPer = "";
		String namePeriodo = "";
		PeriodoDTO perdto = new PeriodoDTO();
		Date fecTopFinAnio;
		Date dateMoment;
		int olgura = 0;

		PeriodoDTO perdtojson = new PeriodoDTO();
		try {

			param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
			salt = QueryParamURL.getParam(jsonEncrypted, "salt");
			iv = QueryParamURL.getParam(jsonEncrypted, "iv");

			String objJsonString = aesUtil.decrypt(salt, iv, passphrase, param);

			perdtojson = gson.fromJson(objJsonString, PeriodoDTO.class);

			// --obtener el periodo activo
			List<PeriodoDTO> periodoActivo = periodoService.listPeriodoActivo();
			// --obtener la fecha tope
			fecTopFinAnio = periodoActivo.get(0).getFechaFinAnio();
			// --verificacion de olgura 2 meses antes y despues del anio tope--
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

			try {
				// fecha today
				dateMoment = formatter.parse(perdtojson.getAnioTope());

				olgura = calcularRangoValidoFecha(fecTopFinAnio, dateMoment);// tope vs hoy

			} catch (ParseException e) {
				e.printStackTrace();
			}

//			paramPer = QueryParamURL.getParam(jsonEncrypted, "perTope");	

//			namePeriodo="PERIODO " +  paramPer.substring(6);
//			namePeriodo="PERIODO " +  perdtojson.getAnioTope().substring(6); /***********/*****************/*******************/*****************
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");

			String year = sdf2.format(fecTopFinAnio);
//	        namePeriodo="PERIODO " +  fecTopFinAnio; //*****************************/****************/**************/*********************
			namePeriodo = "PERIODO " + year;
			// Registro del periodo
			// 2
			if (Math.abs(olgura) <= 2) {
				perdto.setNamePeriodo(namePeriodo);
//					perdto.setCodItem(String.valueOf(paramPer.substring(6)));
//					perdto.setCodItem(String.valueOf(perdtojson.getAnioTope().substring(6)));/*******************/*****************/******************/************
				perdto.setCodItem(year);
				List<PeriodoDTO> lisperbd = periodoService.buscarPeriodoxNombreItem(perdto.getNamePeriodo(),
						perdto.getCodItem());
				if (lisperbd != null && lisperbd.size() > 0) {
					String formato = "yyyy";
					SimpleDateFormat dft = new SimpleDateFormat(formato);
					// dft.format(lisperbd.get(0).getFechaFinAnio());
					Integer fecEnvio = Integer.parseInt(dft.format(lisperbd.get(0).getFechaFinAnio())) + 1;
					// Integer
					// fecEnvio=Integer.parseInt(dft.format(lisperbd.get(0).getFechaFinAnio()));
					parametros.put("ckTp", fecEnvio);
					respuesta = new RespuestaDTO(true, "['']", parametros);
				} else {
					parametros.put("ckTp", 0);
					respuesta = new RespuestaDTO(false, "['']", parametros);
				}
			} else {
				parametros.put("ckTp", 0);
				respuesta = new RespuestaDTO(false, "['']", parametros);
			}

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error producido : " + e);
			}
			parametros.put("ckTp", false);
			respuesta = new RespuestaDTO(false, "['Ocurrio un problema en el servicio']", parametros);
			return Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).build();
		}
		return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();

	}

	public static int calcularRangoValidoFecha(Date fechaInicio, Date fechaFin) {
		try {
			// Fecha inicio en objeto Calendar
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(fechaInicio);
			// Fecha finalización en objeto Calendar
			Calendar endCalendar = Calendar.getInstance();
			endCalendar.setTime(fechaFin);
			// Cálculo de meses para las fechas de inicio y finalización
			int startMes = (startCalendar.get(Calendar.YEAR) * 12) + startCalendar.get(Calendar.MONTH);
			int endMes = (endCalendar.get(Calendar.YEAR) * 12) + endCalendar.get(Calendar.MONTH);
			// Diferencia en meses entre las dos fechas
			int diffMonth = endMes - startMes;
			return diffMonth;
		} catch (Exception e) {
			return 0;
		}
	}

	/*
	 * @SuppressWarnings("rawtypes")
	 * 
	 * @POST
	 * 
	 * @Path("/activarCierre/{codSolicitud}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Response
	 * activarCierre(@PathParam("codSolicitud") String codSolicitud
	 * , @QueryParam("tipo") String tipo,
	 * 
	 * @QueryParam("tipoSolicitud") String tipoSolicitud ) { parametros = new
	 * ConcurrentHashMap<>(); try { // if(!checkSIG){ SolicitudDTO
	 * indicadordto=solicitudService.verificaCierreDeshacerAprobacionSolicitud(
	 * codSolicitud,tipo,tipoSolicitud); parametros.put("procede",
	 * indicadordto.getCorrecto()); respuesta = new RespuestaDTO(true, "[]", new
	 * ConcurrentHashMap<>(parametros)); //}else{ //respuesta = new
	 * RespuestaDTO(false, "['"+ERROR_SERVICIO_GENERAL+"']", parametros); // }
	 * 
	 * 
	 * } catch (Exception e) { if (log.isHabilitadoError()) log.error(e);
	 * parametros.clear(); respuesta = new RespuestaDTO(false,
	 * "['"+ERROR_SERVICIO_GENERAL+"']", parametros); return
	 * Response.status(Status.CONFLICT).entity(gson.toJson(respuesta).toString()).
	 * build(); }finally { parametros.clear(); } return
	 * Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
	 * }
	 */

	public List<MaestroDTO> getListaTipoSituacionRevision() {
		return listaTipoSituacionRevision;
	}

	public void setListaTipoSituacionRevision(List<MaestroDTO> listaTipoSituacionRevision) {
		this.listaTipoSituacionRevision = listaTipoSituacionRevision;
	}

	public List<MaestroDTO> getListaTipoSituacionSolicitud() {
		return listaTipoSituacionSolicitud;
	}

	public void setListaTipoSituacionSolicitud(List<MaestroDTO> listaTipoSituacionSolicitud) {
		this.listaTipoSituacionSolicitud = listaTipoSituacionSolicitud;
	}

	public List<MaestroDTO> getListaTipoSolicitud() {
		return listaTipoSolicitud;
	}

	public void setListaTipoSolicitud(List<MaestroDTO> listaTipoSolicitud) {
		this.listaTipoSolicitud = listaTipoSolicitud;
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T lookup(Class<T> clase) {
		try {
			String nombreSimple = clase.getSimpleName();
			String nombreLargo = clase.getName();
			Context c = new InitialContext();
			return (T) c.lookup("java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/"
					+ nombreSimple.replace("Local", "") + "!" + nombreLargo);
		} catch (NamingException ne) {
			if (LOG.isErrorEnabled())
				LOG.error("Error producido : " + ne);

			throw new RuntimeException(ne);
		}
	}

}
