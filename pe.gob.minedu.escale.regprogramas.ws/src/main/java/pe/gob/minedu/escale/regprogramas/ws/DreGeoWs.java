package pe.gob.minedu.escale.regprogramas.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;

import pe.gob.minedu.escale.common.dto.rest.RespuestaDTO;
import pe.gob.minedu.escale.common.rest.util.AesUtil;
import pe.gob.minedu.escale.common.rest.util.QueryParamURL;
import pe.gob.minedu.escale.common.util.StringUtil;
import pe.gob.minedu.escale.regprogramas.ejb.service.DreGeoServiceLocal;
import pe.gob.minedu.escale.regprogramas.model.dto.DreGeoDTO;
import pe.gob.minedu.escale.regprogramas.model.dto.ws.DreGeoUgelDistritoDTO;

@Path("/dregeo")
@Stateless
public class DreGeoWs {

	@EJB
	private transient DreGeoServiceLocal dreGeoService = lookup();
	
	private Gson gson = new Gson();
	
	private String json = "";
	
	//cambio werr
	private String param,salt,iv;
	private int keySize = 128, iterationCount = 1000;
		
	private String passphrase= "0015975320171234";
		
	private AesUtil aesUtil = new AesUtil(keySize, iterationCount);	
		
	private ConcurrentHashMap<Object, Object> parametros;
		
	@SuppressWarnings("rawtypes")
	private RespuestaDTO respuesta;
	
	public DreGeoWs() {
		
	}
	
	@POST
	@Path("/getDreGeos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdreGeos(String url){
		List<DreGeoDTO> listdreGeo = new ArrayList<DreGeoDTO>();		
		try {
			listdreGeo = dreGeoService.listarDreGeo();			
			if (StringUtil.isNotNullOrBlank(listdreGeo)) {
				json = gson.toJson(listdreGeo);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra dreGeos";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
		return Response.status(Status.OK).entity(json).build();
	}
	
//	@POST
//	@Path("/getDreGeoxUgel/{idDreUgel}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getDreGeoxUgel(@PathParam("idDreUgel") String idDreUgel){
//		List<DreGeoUgelDistritoDTO> listdreGeo = new ArrayList<DreGeoUgelDistritoDTO>();		
//		try {
//			listdreGeo = dreGeoService.buscarDreGeoxUgel(idDreUgel);	
//			if (StringUtil.isNotNullOrBlank(listdreGeo)) {
//				json = gson.toJson(listdreGeo);
//			}
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			json = "No se encuentra dreGeo";
//			return Response.status(Status.CONFLICT).entity(json).build();
//		}
//		return Response.status(Status.OK).entity(json).build();
//	}
	
//Se modifica por cifrado de datos  werr 08022018	

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/getDreGeoxUgel/{idDreUgel}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDreGeoxUgel(@PathParam("idDreUgel") String idDreUgel , String jsonEncrypted ){
		List<DreGeoUgelDistritoDTO> listdreGeo = new ArrayList<DreGeoUgelDistritoDTO>();		
		aesUtil = new AesUtil(keySize, iterationCount);		
		parametros = new ConcurrentHashMap<>();
		try {
			
			if(StringUtil.isNotNullOrBlank(jsonEncrypted) && jsonEncrypted.charAt(0)!='{' ){
				listdreGeo = dreGeoService.buscarDreGeoxUgel(idDreUgel);	
				if (StringUtil.isNotNullOrBlank(listdreGeo)) {
					param = QueryParamURL.getParam(jsonEncrypted, "jsonEncrypted");
					salt = QueryParamURL.getParam(jsonEncrypted, "salt");
					iv = QueryParamURL.getParam(jsonEncrypted, "iv");
					parametros.put("salt", salt);
					parametros.put("iv", iv);
					parametros.put("error", false);
					parametros.put("lista", aesUtil.encrypt(salt, iv, passphrase, gson.toJson(listdreGeo)));
					respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
					return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
					
				}else{
					parametros.put("error", true);
					parametros.put("mensaje", "DreUgel esta vacio");
					respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
					return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
				}
			
			}
			else{
				parametros.put("error", true);
				parametros.put("mensaje", "Error General");
				respuesta = new RespuestaDTO(true, "[]", new ConcurrentHashMap<>(parametros));
				return Response.status(Status.OK).entity(gson.toJson(respuesta).toString()).build();
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			json = "No se encuentra dreGeo";
			return Response.status(Status.CONFLICT).entity(json).build();
		}
		//return Response.status(Status.OK).entity(json).build();
	}
	
	private DreGeoServiceLocal lookup() {
        try {
            Context c = new InitialContext();
            return (DreGeoServiceLocal) c.lookup("java:global/pe.gob.minedu.escale.regprogramas.ws-1.0.0-PRO/DreGeoService!pe.gob.minedu.escale.regprogramas.ejb.service.DreGeoServiceLocal");
        } catch (NamingException ne) {
            throw new RuntimeException(ne);
        }
    }
	
}
